
#import <Foundation/Foundation.h>

@interface UserDefault : NSObject


+ (void)saveBOOLValue:(BOOL)value forKey:(NSString*)key;

+ (BOOL)retriveBOOLValueFroKey:(NSString*)key;

+ (void)saveIntValue:(int)value forKey:(NSString*)key;

+ (int)retriveIntValueFroKey:(NSString*)key;

+ (void)saveObject:(id)obj forKey:(NSString*)key;

+ (id)retriveObjectForKey:(NSString*)key;

+ (void)removeObjectforKey:(NSString*)key;

+ (void)saveDoubleValue:(double)value forKey:(NSString*)key;

+ (int)retriveDoubleValueFroKey:(NSString*)key;
    

    
@end
