//
//  ForwardMsgVC.h
//  ZoeChat
//
//  Created by macmini on 07/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForwardMsgVC : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *RecentChatTblView;
- (IBAction)clickOnCancel:(id)sender;

- (IBAction)ClickOnForwardBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *contactsTblVie;
@property (strong,nonatomic)NSString *typeString;
@property (strong,nonatomic)NSString *textString;
@property (strong,nonatomic)NSString *latitudeString;
@property (strong,nonatomic)NSString *longitudeString;
@property (strong,nonatomic)NSString *captionString;

@property (strong,nonatomic)NSString *ContactNameString;
@property (strong,nonatomic)NSString *ContactNumberString;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *forwardBtn;
@property(strong, nonatomic)NSString *strZoeChatId;
@property(strong, nonatomic)NSString *strChatRoomType;
//link
@property (strong,nonatomic)NSString *getPreviewVal;
@property (strong,nonatomic)NSString *getLinkTitle;
@property (strong,nonatomic)NSString *getLinkLogo;
@property (strong,nonatomic)NSString *getLinkDes;
@end
