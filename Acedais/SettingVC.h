//
//  SettingVC.h
//  ZoeChat
//
//  Created by macmini on 11/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingVC : UIViewController
@property (strong, nonatomic) IBOutlet UIView *profileView;
@property (strong, nonatomic) IBOutlet UIImageView *profileImgView;
@property (strong, nonatomic) IBOutlet UILabel *profileName;
@property (strong, nonatomic) IBOutlet UILabel *profileStatus;
@property (strong, nonatomic) IBOutlet UIButton *profileBtn;
- (IBAction)clickOnBackBtn:(id)sender;
- (IBAction)clickOnImgBtn:(id)sender;

- (IBAction)dataUsage:(id)sender;
- (IBAction)Privacy:(id)sender;
- (IBAction)notification:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnTheme;

@property (weak, nonatomic) IBOutlet UIView *themeView;
@property (weak, nonatomic) IBOutlet UIButton *b1;
@property (weak, nonatomic) IBOutlet UIButton *b2;
@property (weak, nonatomic) IBOutlet UIButton *b3;
@property (weak, nonatomic) IBOutlet UIButton *b4;
@property (weak, nonatomic) IBOutlet UIButton *b5;
@property (weak, nonatomic) IBOutlet UIButton *b6;
@property (weak, nonatomic) IBOutlet UIButton *b7;
@property (weak, nonatomic) IBOutlet UIButton *b8;
@property (weak, nonatomic) IBOutlet UIButton *b9;
@property (weak, nonatomic) IBOutlet UIButton *b10;
@property (weak, nonatomic) IBOutlet UIButton *b11;
@property (weak, nonatomic) IBOutlet UIButton *b12;
- (IBAction)ClickOnB1:(id)sender;
- (IBAction)ClickOnB2:(id)sender;
- (IBAction)ClickOnB3:(id)sender;
- (IBAction)ClickOnB4:(id)sender;
- (IBAction)ClickOnB5:(id)sender;
- (IBAction)ClickOnB6:(id)sender;
- (IBAction)ClickOnB7:(id)sender;
- (IBAction)ClickOnB8:(id)sender;
- (IBAction)ClickOnB9:(id)sender;
- (IBAction)ClickOnB10:(id)sender;
- (IBAction)ClickOnB11:(id)sender;
- (IBAction)ClickOnB12:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *fbBtn;
@property (strong, nonatomic) IBOutlet UIButton *instaBtn;
@property (strong, nonatomic) IBOutlet UIButton *twitterBtn;
@property (strong, nonatomic) IBOutlet UIButton *supportBtn;
- (IBAction)clickOnFB:(id)sender;
- (IBAction)clickOnInstagram:(id)sender;
- (IBAction)clickOnTwitter:(id)sender;
- (IBAction)clickOnSupport:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *fbimgView;
@property (strong, nonatomic) IBOutlet UIImageView *instaImgView;
@property (strong, nonatomic) IBOutlet UIImageView *twitterimgView;
@property (strong, nonatomic) IBOutlet UIImageView *supportImgview;
@property (strong, nonatomic) IBOutlet UILabel *InitialLbl;

@end
