//
//  PrivacySettingVC.h
//  ZoeChat
//
//  Created by macmini on 10/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivacySettingVC : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *privacyTblView;
- (IBAction)clickOnBackBtn:(id)sender;

@end
