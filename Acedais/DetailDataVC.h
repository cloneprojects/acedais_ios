//
//  DetailDataVC.h
//  ZoeChat
//
//  Created by macmini on 06/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailDataVC : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *detailDataTblView;
@property (strong, nonatomic)NSString *titleStr;
@property (strong, nonatomic)NSIndexPath *photoIndexPath;
@property (strong, nonatomic)NSIndexPath *audioIndexPath;
@property (strong, nonatomic)NSIndexPath *videoIndexPath;
@property (strong, nonatomic)NSIndexPath *documentIndexPath;

@end
