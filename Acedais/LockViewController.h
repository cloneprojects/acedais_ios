//
//  LockViewController.h
//  ZoeChat
//
//  Created by macmini on 11/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LockViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *emailTxtFld;
@property (strong, nonatomic) IBOutlet UITextField *phoneTxtFld;
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
- (IBAction)clickOnsubmitBtn:(id)sender;
- (IBAction)clickOnBackBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *passwordTxtFld;
@property (strong, nonatomic) IBOutlet UITextField *confirmPasswordTxtFld;
- (IBAction)ClickonPasswordSubmit:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *passwordView;
@property (strong, nonatomic) IBOutlet UIButton *passwordSubmitBtn;
@property (strong,nonatomic)NSString *getChatId;

@end
