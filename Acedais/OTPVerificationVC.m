//
//  OTPVerificationVC.m
//  ZoeChat
//
//  Created by Pyramidions Solution on 29/12/16.
//  Copyright © 2016 Pyramidions Solution. All rights reserved.
//

#import "OTPVerificationVC.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "AFNHelper.h"
#import "MBProgressHUD.h"

@interface OTPVerificationVC ()
{
    UITextField *currentTextView;
    UITextView *currentTextView1;
    bool keyboardIsShown;
    UITapGestureRecognizer *tapGesture;
    AppDelegate *appDelegate;
    
}

@end

@implementation OTPVerificationVC
@synthesize strPhoneNumber;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    [self.navigationController.navigationBar setTranslucent:NO];
     [self setFontFamily:FONT_NORMAL forView:self.view andSubViews:YES];
    
    _txtOTP.delegate=self;
    _txtOTP.text=appDelegate.strOTP;
    _btnNext.clipsToBounds=YES;
    _btnNext.layer.cornerRadius=4;
    [_btnNext setFont:[UIFont fontWithName:FONT_MEDIUM size:[[_btnNext font] pointSize]]];
    
    self.btnCallMe.titleEdgeInsets=UIEdgeInsetsMake(0, 5, 0, 0);
    self.btnResend.titleEdgeInsets=UIEdgeInsetsMake(0, 0, 0, 5);
    
    self.title=[NSString stringWithFormat:@"%@", strPhoneNumber];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:FONT_HEAVY size:15]}];

   
    CALayer *btnUpperBorder = [CALayer layer];
    btnUpperBorder.frame = CGRectMake(0.0f
                                       , 0.0f, _btnResend.frame.size.width+_btnCallMe.frame.size.width+10, 1.5f);
    btnUpperBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                       alpha:1.0f].CGColor;
    [_btnResend.layer addSublayer:btnUpperBorder];
    
    CALayer *btnbottomBorder = [CALayer layer];
    btnbottomBorder.frame = CGRectMake(0.0f
                                       , _btnResend.frame.size.height-1.5, _btnResend.frame.size.width+_btnCallMe.frame.size.width+10, 1.5f);
    btnbottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                        alpha:1.0f].CGColor;
    [_btnResend.layer addSublayer:btnbottomBorder];

    
    CALayer *txtbottomBorder = [CALayer layer];
    txtbottomBorder.frame = CGRectMake(0.0f, _txtOTP.frame.size.height-1.5, _txtOTP.frame.size.width, 1.5f);
    txtbottomBorder.backgroundColor = [UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0].CGColor;
    [_txtOTP.layer addSublayer:txtbottomBorder];

    
    //ScrollView moving
    
    //  _scrollView.contentSize=CGSizeMake(self.view.frame.size.width, self.btnRegister.frame.origin.y+70);
    
    
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.scrollView.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    self.scrollView.contentSize = contentRect.size;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyBoard)];
    
    [self.view addGestureRecognizer:tap];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onWrongNumber:(id)sender
{
     [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onResend:(id)sender {
}
- (IBAction)onCallMe:(id)sender {
}

- (IBAction)onNext:(id)sender
{
    if ([self.txtOTP.text isEqualToString:@""])
    {
        [self.txtOTP resignFirstResponder];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Please enter OTP"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:2];
        
        
        
        return;
    }
    else if (self.txtOTP.text.length!=6)
    {
        [self.txtOTP resignFirstResponder];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Please enter valid OTP"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:2];
        return;
    }
    else if (![self.txtOTP.text isEqualToString:appDelegate.strOTP])
    {
        [self.txtOTP resignFirstResponder];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Please enter valid OTP"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:2];
        return;
    }
    
    
    [self performSegueWithIdentifier:@"OTP_Profile" sender:self];
}

#pragma mark -
#pragma mark - Keyboard

-(void)dismissKeyBoard
{
    // [tableTexture removeFromSuperview];
    
    for(UIView *subView in self.scrollView.subviews)
    {
        if([subView isKindOfClass:[UITextField class]])
        {
            if([subView isFirstResponder])
            {
                [subView resignFirstResponder];
                break;
            }
        }
        else if([subView isKindOfClass:[UITextView class]])
        {
            if([subView isFirstResponder])
            {
                [subView resignFirstResponder];
                break;
            }
        }
        
    }
    
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
-(void) textFieldDidBeginEditing:(UITextField *)textField
{
    
    currentTextView=textField;
    [self moveScrollView:textField.frame];
    
    /* if (textField!= _txtPassword)
     {
     
     [btnShowPwd removeFromSuperview];
     _txtPassword.secureTextEntry=YES;
     } */
}


-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
    currentTextView1=textView;
    [self moveScrollView:textView.frame];
}
- (void)keyboardDidHide:(NSNotification *)n
{
    keyboardIsShown = NO;
    [self.view removeGestureRecognizer:tapGesture];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardDidShow:(NSNotification *)n
{
    if (keyboardIsShown) {
        return;
    }
    
    keyboardIsShown = YES;
    [self.scrollView addGestureRecognizer:tapGesture];
}

- (void) moveScrollView:(CGRect) rect
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    
    CGSize kbSize = CGSizeMake(32, 260); // keyboard height
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.scrollView.frame;
    aRect.size.height -= (kbSize.height+55);
    if (!CGRectContainsPoint(aRect, rect.origin) )
    {
        CGPoint scrollPoint = CGPointMake(0.0, rect.origin.y-kbSize.height+100);
        
        if(screenHeight==480)
            scrollPoint = CGPointMake(0.0, rect.origin.y-kbSize.height+100+90);
        
        if(scrollPoint.y>0)
            [self.self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

- (void) hideKeyBoard
{
    
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    //  NSLog(@"Keyboard is hidden");
    UIEdgeInsets content=UIEdgeInsetsMake(0, 0, 0, 0);
    self.scrollView.contentInset = content;
    self.scrollView.scrollIndicatorInsets = content;}


#pragma mark -
#pragma mark - Font
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)view;
        [btn setFont:[UIFont fontWithName:fontFamily size:[[btn font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *txtFld = (UITextField *)view;
        [txtFld setFont:[UIFont fontWithName:fontFamily size:[[txtFld font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextView class]])
    {
        UITextView *txtVw = (UITextView *)view;
        [txtVw setFont:[UIFont fontWithName:fontFamily size:[[txtVw font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}



@end
