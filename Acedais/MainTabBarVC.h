//
//  MainTabBarVC.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 31/12/16.
//  Copyright © 2016 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Contacts/Contacts.h>


@interface MainTabBarVC : UITabBarController<UITabBarControllerDelegate, UISearchBarDelegate,UITextFieldDelegate>
{
    
}
- (IBAction)ClickOnContactSearch:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *ContactSearch;
- (IBAction)onSearch:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnSearch;
- (IBAction)onMenu:(id)sender;
@end
