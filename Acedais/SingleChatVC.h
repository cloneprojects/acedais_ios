//
//  SingleChatVC.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 10/01/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Stickerpipe/Stickerpipe.h>

@interface SingleChatVC : UIViewController<UITableViewDataSource, UITableViewDelegate, CAAnimationDelegate, UIScrollViewDelegate>
//{
//    int rowNO;
//    NSIndexPath *lastIndexPth;
//}
@property (weak, nonatomic) IBOutlet UITableView *tableSingleChat;

@property(strong, nonatomic)NSDictionary *dictDetails;

- (IBAction)onAttachments:(id)sender;
//Audio
@property (strong, nonatomic) UIButton *PlayPauseButton;
@property (strong, nonatomic) UIButton *CancelButton;
@property (strong, nonatomic) UILabel *TimeLabel;
@property (strong, nonatomic) UILabel *AudioNameLabel;
@property(strong, nonatomic)UIView *AudioView;
@property(strong, nonatomic)UIView *WholeView;
@property(strong,nonatomic)UISlider *movingslider;
//Info
@property (strong, nonatomic) UIButton *backButton;
@property (strong, nonatomic) UILabel *statusLabel;
@property (strong, nonatomic) UILabel *msgInfoLb;
@property (strong, nonatomic) UILabel *DeliveredLabel;
@property (strong, nonatomic) UILabel *statusTimeLabel;
@property (strong, nonatomic) UILabel *DeliveredTimeLabel;
@property(strong, nonatomic)UIView *infoView;
@property(strong, nonatomic)UIView *FullView;
@property(strong,nonatomic)UIImageView *StatusImgview;
@property(strong,nonatomic)UIImageView *deliveredImgview;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *callBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *attachmentBtn;
//Link
@property (strong, nonatomic) IBOutlet UIView *linkPreviewView;
@property (strong, nonatomic) IBOutlet UIImageView *linkLogo;
@property (strong, nonatomic) IBOutlet UILabel *LinkTitlelbl;
@property (strong, nonatomic) IBOutlet UILabel *linkDescriptionLbl;
- (IBAction)CancelBtn:(id)sender;
//AddContactView
@property (strong, nonatomic) IBOutlet UIView *addContactView;
@property (strong, nonatomic) IBOutlet UIButton *addContactBtn;
- (IBAction)ClickOnAddContact:(id)sender;

@end
