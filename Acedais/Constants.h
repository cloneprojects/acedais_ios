//
//  Constants.h
//  gHealth Doctor
//
//  Created by Ramesh P on 16/11/16.
//  Copyright © 2016 provenlogic. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - Service URL
#pragma mark - --



/*
#pragma mark - Google APIs
#pragma mark - --
extern NSString *const FILE_GOOGLE_MAP_API;//Google Maps
extern NSString *const FILE_GOOGLE_MAP_DISTANCE_MATRIX_API;//Google Maps Distance Matrix API
#pragma mark - OpenTok APIs
#pragma mark - --
extern NSString *const FILE_OPENTOK_API;// OpenTok API
 
 Avenir: (
 "Avenir-Medium",
 "Avenir-HeavyOblique",
 "Avenir-Book",
 "Avenir-Light",
 "Avenir-Roman",
 "Avenir-BookOblique",
 "Avenir-MediumOblique",
 "Avenir-Black",
 "Avenir-BlackOblique",
 "Avenir-Heavy",
 "Avenir-LightOblique",
 "Avenir-Oblique"
 )

*/
// com.pyramidion.ZoeChat

#define Address_URL @"https://maps.googleapis.com/maps/api/geocode/json?"
#define AutoComplete_URL @"https://maps.googleapis.com/maps/api/place/autocomplete/json?"
#define Google_Maps_API @"AIzaSyCQQm8xcAtKf0_Ij9Wg9tbzpSGKcAtOtTk"
#define Google_Static_Map_API @"AIzaSyCQQm8xcAtKf0_Ij9Wg9tbzpSGKcAtOtTk"


//original URL
//#define SERVICE_URL @"http://139.59.25.20:3000/"
//#define SOCKET_URL @"http://139.59.25.20:3000/user"


// #define AGORA_APP_ID @"97164676bb634736a5853737882579de" // Gopal Account



//Changed URL
#define SERVICE_URL @"http://18.207.40.111:3000/"
#define SOCKET_URL @"http://18.207.40.111:3000/"

#define AGORA_APP_ID @"7798a0c34c5a4ba3911702df1e1831c2"

#pragma mark - CustomAlbum
#pragma mark - --
extern NSString * const CSAlbum ;
extern NSString * const CSAssetIdentifier ;
extern NSString * const CSAlbumIdentifier ;

#pragma mark - AWS Credentials
#pragma mark - --
extern NSString *const FILE_AWS_POOL_ID;//AWS Pool Id
extern NSString *const FILE_AWS_IMAGE_BASE_URL;//AWS IMAGE BASE URL
extern NSString *const FILE_BUCKET_NAME;//AWS BUCKET NAME

#pragma mark - Methods
#pragma mark - --
extern NSString *const FILE_GET_OTP;//get otp
extern NSString *const FILE_REGISTER;//register
extern NSString *const FILE_CONTACTS_SYNC;//contacts sync
extern NSString *const FILE_UPDATE_PROFILE;//Update Profile
extern NSString *const FILE_UPDATE_GROUP_NAME;//Update Group Name
extern NSString *const FILE_MAKE_CALL;//Make Call
extern NSString *const FILE_ACCEPT_CALL;//Accept Call
extern NSString *const FILE_END_CALL;//End Call
extern NSString *const FILE_CREATE_GROUP;//Create Group
extern NSString *const FILE_ADD_PARTICIPANTS;//Add Participants
extern NSString *const FILE_GET_PARTICIPANTS;//Get Participants
extern NSString *const FILE_PRIVACY;//Privacy
extern NSString *const FILE_RECOVERY;//Recovery
extern NSString *const FILE_REQUESTOTP;//OTP
extern NSString *const FILE_CHECKRECOVERY;//Check recovery
extern NSString *const FILE_BACKUPUPDATE;//Back Up
extern NSString *const UPDATEDEVICETOKEN;



#pragma mark - Font
#pragma mark - --
extern NSString *const FONT_NORMAL;//Normal
extern NSString *const FONT_HEAVY;// Heavy
extern NSString *const FONT_MEDIUM;//  Medium


#pragma mark - Parameters
#pragma mark - --
extern NSString *const PICTURE;


#pragma mark - User Table Parameters
#pragma mark - --
extern NSString *const MOBILE;
extern NSString *const NAME;
extern NSString *const REGISTEREDNAME;
extern NSString *const IMAGE;
extern NSString *const STATUS;
extern NSString *const SHOWINCONTACTSPAGE;
extern NSString *const ZOECHATID;

#pragma mark - Chat Table Parameters
#pragma mark - --
extern NSString *const CHATID;
extern NSString *const CHATROOMID;
extern NSString *const CHATROOMTYPE;
extern NSString *const CHATSENDER;
extern NSString *const LASTMESSAGE;
extern NSString *const LASTMESSAGESTATUS;
extern NSString *const LASTMESSAGETIME;
extern NSString *const UNREADCOUNT;

#pragma mark - User Table Parameters
#pragma mark - --
extern NSString *const CHATMESSAGEID;
extern NSString *const USERID;
extern NSString *const CONTENT;
extern NSString *const CONTENTYPE;
extern NSString *const CONTENTSTATUS;
extern NSString *const MESSAGESENDER;
extern NSString *const SENTTIME;
extern NSString *const DELIVEREDTIME;
extern NSString *const SEENTIME;
extern NSString *const CAPTION;
extern NSString *const ISDOWNLOADED;


