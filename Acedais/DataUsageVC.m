//
//  DataUsageVC.m
//  ZoeChat
//
//  Created by macmini on 06/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "DataUsageVC.h"
#import "DataUsageCell.h"
#import "DetailDataVC.h"
#import "AppDelegate.h"

@interface DataUsageVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *dataArray;
    AppDelegate *appdelegate;
    NSInteger getindexvalue;
}
@end

@implementation DataUsageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    dataArray = [[NSArray alloc]initWithObjects:@"Photo",@"Audio",@"Video",@"Reset Auto-Download Settings", nil];
    appdelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"YES" forKey:@"isDataPage"];
    [defaults synchronize];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"YES" forKey:@"isDataPage"];
    [defaults synchronize];
        [_dataTblView reloadData];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  60;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DataUsageCell *cell=(DataUsageCell *)[tableView dequeueReusableCellWithIdentifier:@"DataUsageCell"];
    if (cell==nil) {
        cell=[[DataUsageCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DataUsageCell"];
    }
    cell.nameLbl.text=[dataArray objectAtIndex:indexPath.row];
    if (indexPath.row == 0)
    {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *getStatus = [defaults objectForKey:@"photoStatus"];
    
        if (getStatus==nil)
        {
        cell.StatusLbl.text = @"Never";
    }
    else
    {
        cell.StatusLbl.text = getStatus;
    }
        

    }
    else if (indexPath.row == 1)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *getStatus = [defaults objectForKey:@"audioStatus"];
        
        if (getStatus==nil)        {
            cell.StatusLbl.text = @"Never";
        }
        else
        {
            cell.StatusLbl.text = getStatus;
        }
    }
    else if (indexPath.row == 2)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *getStatus = [defaults objectForKey:@"videoStatus"];
        if (getStatus==nil)
        {
            cell.StatusLbl.text = @"Never";
        }
        else
        {
            cell.StatusLbl.text = getStatus;
        }
        
    }
//    else if (indexPath.row == 3)
//    {
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        NSString *getStatus = [defaults objectForKey:@"documentStatus"];
//        
//        if (getStatus==nil)
//        {
//            cell.StatusLbl.text = @"Wi-fi";
//        }
//        else
//        {
//            cell.StatusLbl.text = getStatus;
//        }
//    }
    
   
   else if (indexPath.row == 3)
    {
        
        cell.nameLbl.textColor = [UIColor redColor];
        cell.StatusLbl.text = @"";
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    cell.selectionStyle=UITableViewCellSelectionStyleGray;
       return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DataUsageCell *cell=(DataUsageCell*)[tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row == 3)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"photoStatus"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"audioStatus"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"videoStatus"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"documentStatus"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"selected1"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"selected2"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"selected3"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"selected4"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isDataPage"];

        [[NSUserDefaults standardUserDefaults] synchronize];
        cell.StatusLbl.text = @"";
        [tableView reloadData];

    }
    else
    {
//    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    DetailDataVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"DetailDataVC"];
//    hme.titleStr = [dataArray objectAtIndex:indexPath.row];
//    [self.navigationController pushViewController:hme animated:YES];
        getindexvalue=indexPath.row;
        [self performSegueWithIdentifier:@"DetailData" sender:nil];
        
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"DetailData"])
    {
        DetailDataVC *Single=[segue destinationViewController];
        Single.titleStr=[dataArray objectAtIndex:getindexvalue];
    }
}


- (IBAction)clickonBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
