//
//  MessageCell.m
//  Whatsapp
//
//  Created by Rafael Castro on 7/23/15.
//  Copyright (c) 2015 HummingBird. All rights reserved.
//

#import "MessageCell.h"
#import "Constants.h"
#import "AppDelegate.h"
#import <Stickerpipe/Stickerpipe.h>
#import "UIImageView+WebCache.h"

@interface MessageCell ()
{
    CGRect tempFrame;
}
@end


@implementation MessageCell

-(CGFloat)height
{
    return _bubbleImage.frame.size.height;
}
-(void)updateMessageStatus
{
    [self buildCell];
    //Animate Transition
    _statusIcon.alpha = 0;
    [UIView animateWithDuration:.5 animations:^{
        _statusIcon.alpha = 1;
    }];
}

#pragma mark -

-(id)init
{
    self = [super init];
    if (self)
    {
        [self commonInit];
    }
    return self;
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self commonInit];
    }
    return self;
}
-(void)commonInit
{
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.accessoryType = UITableViewCellAccessoryNone;
    
    _textView = [[UITextView alloc] init];
    _bubbleImage = [[UIImageView alloc] init];
     _imgView = [[UIImageView alloc] init];
    _timeLabel = [[UILabel alloc] init];
     _nameLabel = [[UILabel alloc] init];
    _statusIcon = [[UIImageView alloc] init];
    _resendButton = [[UIButton alloc] init];
    _resendButton.hidden = YES;
    _viewContact=[[UIView alloc]init];
    
    _StarimgView = [[UIImageView alloc]init];
    _HeadertextView = [[UITextView alloc]init];
    _StickerimgView= [[UIImageView alloc] init];

    
    [self.contentView addSubview:_bubbleImage];
    [self.contentView addSubview:_textView];
     [self.contentView addSubview:_nameLabel];
    [self.contentView addSubview:_imgView];
    [self.contentView addSubview:_timeLabel];
    [self.contentView addSubview:_statusIcon];
    [self.contentView addSubview:_resendButton];
    [self.contentView addSubview:_viewContact];
    [self.contentView addSubview:_StarimgView];
    [self.contentView addSubview:_HeadertextView];
    [self.contentView addSubview:_StickerimgView];

}
-(void)prepareForReuse
{
    [super prepareForReuse];
    
    _textView.text = @"";
    _nameLabel.text=@"";
    _timeLabel.text = @"";
    _statusIcon.image = nil;
    _bubbleImage.image = nil;
    _imgView.image=nil;
    _resendButton.hidden = YES;
    _viewContact=nil;
    _StarimgView.image = nil;
    _HeadertextView.text = @"";
    _StickerimgView.image=nil;

    
    
}
-(void)setMessage:(Message *)message
{
    _message = message;
    [self buildCell];
    
    message.heigh = self.height;
}
-(void)buildCell
{
    [self setTextView];
   
    [self setNameLabel];
     [self setTimeLabel];
    [self setBubble];
    
    [self addStatusIcon];
    [self setStatusIcon];
    
    [self setFailedButton];
    [self setStaring];
//    [self setHeaderView];
    [self setNeedsLayout];
}

#pragma mark - TextView

-(void)setTextView
{
      NSLog(@"%@",_message.name);
    
    if ([_message.type isEqualToString:@"image"] || [_message.type isEqualToString:@"location"] || [_message.type isEqualToString:@"video"])
    {
      //  NSLog(@"%@",_message.caption);
       // CGFloat max_witdh = 0.7*self.contentView.frame.size.width;
         CGFloat max_witdh =224;
        _imgView.frame = CGRectMake(0, 0, max_witdh, 150);
        _imgView.backgroundColor = [UIColor clearColor];
        _imgView.userInteractionEnabled = NO;
        _imgView.image = _message.image;
        _imgView.contentMode=UIViewContentModeScaleAspectFill;
        _imgView.clipsToBounds=YES;
        _imgView.layer.cornerRadius=4;
   
        
     //   NSLog(@"%f",self.contentView.frame.size.width);
        
        
        CGFloat imgView_x;
        CGFloat imgView_y;
        CGFloat imgView_w = _imgView.frame.size.width;
        CGFloat imgView_h = _imgView.frame.size.height;
        UIViewAutoresizing autoresizing;
        
        if (_message.sender == MessageSenderMyself)
        {
            imgView_x = self.contentView.frame.size.width - imgView_w - 15;
            imgView_y = 5;
            autoresizing = UIViewAutoresizingFlexibleLeftMargin;
           // imgView_x -= [self isSingleLineCase]?65.0:0.0;
           // imgView_x -= [self isStatusFailedCase]?([self fail_delta]-15):0.0;
        }
        else
        {
            imgView_x = 20;
            imgView_y = 5;
            autoresizing = UIViewAutoresizingFlexibleRightMargin;
            
            if (_message.name.length>0)
            {
                imgView_y=17;
            }
        }
        
        _imgView.autoresizingMask = autoresizing;
        _imgView.frame = CGRectMake(imgView_x, imgView_y, imgView_w, imgView_h);
         tempFrame=_imgView.frame;
        
        NSLog(@"%@",_message.caption);
        if ([_message.caption length]!=0)
        {
            
            CGFloat max_witdh = 0.7*self.contentView.frame.size.width;
            _textView.frame = CGRectMake(0, 0, max_witdh, MAXFLOAT);
            _textView.font = [UIFont fontWithName:FONT_NORMAL size:12.0];
            _textView.backgroundColor = [UIColor clearColor];
            _textView.userInteractionEnabled = NO;
            _textView.text = _message.caption;
            
            
            [_textView sizeToFit];
            
            CGFloat textView_x;
            CGFloat textView_y;
            CGFloat textView_w = _textView.frame.size.width;
            CGFloat textView_h = _textView.frame.size.height;
            UIViewAutoresizing autoresizing;
            
           
                textView_x = imgView_x+5;
                textView_y = imgView_y+imgView_h+1;
                autoresizing = UIViewAutoresizingFlexibleRightMargin;
            
            _textView.autoresizingMask = autoresizing;
            _textView.frame = CGRectMake(textView_x, textView_y, textView_w, textView_h);
            
            tempFrame=CGRectMake(imgView_x, imgView_y, imgView_w, imgView_h+textView_h);
            
        }
        
        
    }
    else if ([_message.type isEqualToString:@"contact"])
    {
        
        CGFloat max_witdh = 0.7*self.contentView.frame.size.width;
        _viewContact.frame = CGRectMake(0, 0, max_witdh, 80);
//        _viewContact.frame = CGRectMake(0, 0, max_witdh, 60);
        _viewContact.backgroundColor = [UIColor clearColor];
        _viewContact.userInteractionEnabled = NO;
        [_viewContact sizeToFit];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0,51, max_witdh, 1)];
        lineView.backgroundColor = [UIColor lightGrayColor];
        [_viewContact addSubview:lineView];
        
        CGFloat View_x;
        CGFloat View_y;
        CGFloat View_w = _viewContact.frame.size.width;
        CGFloat View_h = _viewContact.frame.size.height;
        UIViewAutoresizing autoresizing;
        
        if (_message.sender == MessageSenderMyself)
        {
            View_x = self.contentView.frame.size.width - View_w - 20;
            View_y = 1;
            autoresizing = UIViewAutoresizingFlexibleLeftMargin;
//            BOOL succ=[self isSingleLineCase];
//            View_x -= [self isSingleLineCase]?65.0:0.0;
//            View_x -= [self isStatusFailedCase]?([self fail_delta]-15):0.0;
        }
        else
        {
            View_x = 20;
            View_y = 1;
            autoresizing = UIViewAutoresizingFlexibleRightMargin;
            
            if (_message.name.length>0)
            {
                View_y=10;
            }
        }
        
        _viewContact.autoresizingMask = autoresizing;
        _viewContact.frame = CGRectMake(View_x, View_y, View_w, View_h);
        
        tempFrame=_viewContact.frame;
        
        
        _imgContact=[[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 40, 40)];
        _imgContact.image=[UIImage imageNamed:@"clear"];
        _imgContact.layer.cornerRadius=_imgContact.frame.size.height/2;
        _imgContact.clipsToBounds=YES;
        [_viewContact addSubview:_imgContact];
        
        
        _lblContName=[[UILabel alloc]initWithFrame:CGRectMake(60, 10, _viewContact.frame.size.width-65, 20)];
        _lblContName.text=_message.contactName;
        _lblContName.font=[UIFont fontWithName:FONT_NORMAL size:15];
        [_viewContact addSubview:_lblContName];

        
        UILabel *lblimg=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, 40, 40)];
        NSString *InitialStr;
        UIColor *bgColor;
        InitialStr=[_lblContName.text substringToIndex:1];
        if ([InitialStr isEqualToString:@"A"])
        {
            bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"B"])
        {
            bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"C"])
        {
            bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"D"])
        {
            bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"E"])
        {
            bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"F"])
        {
            bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"G"])
        {
            bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"H"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"I"])
        {
            bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"J"])
        {
            bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"K"])
        {
            bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"L"])
        {
            bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"M"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"N"])
        {
            bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"O"])
        {
            bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"P"])
        {
            bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"Q"])
        {
            bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"R"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"S"])
        {
            bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"T"])
        {
            bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"U"])
        {
            bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"V"])
        {
            bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"W"])
        {
            bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"X"])
        {
            bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"Y"])
        {
            bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"Z"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
        }
        lblimg.text=InitialStr;
        lblimg.backgroundColor=bgColor;
        lblimg.textAlignment=NSTextAlignmentCenter;
        lblimg.layer.cornerRadius=lblimg.frame.size.height/2;
        lblimg.clipsToBounds=YES;
        lblimg.font=[UIFont fontWithName:FONT_NORMAL size:15];
        [_viewContact addSubview:lblimg];
        
        
        _lblContNumber=[[UILabel alloc]initWithFrame:CGRectMake(60, 30, _viewContact.frame.size.width-65, 20)];
        _lblContNumber.text=_message.contactNo;
        _lblContNumber.font=[UIFont fontWithName:FONT_NORMAL size:13];
        [_viewContact addSubview:_lblContNumber];
        
        if (_message.sender == MessageSenderMyself)
        {
            _btnInviteOrMsg=[[UIButton alloc]initWithFrame:CGRectMake(View_x, 51, _viewContact.frame.size.width, 30)];
            [_btnInviteOrMsg setTitle:@"Invite" forState:UIControlStateNormal];
            _btnInviteOrMsg.font=[UIFont fontWithName:FONT_MEDIUM size:15];
            
            NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
            UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
            if (color != nil) {
               [_btnInviteOrMsg setTitleColor:color forState:UIControlStateNormal];
                
            }
            else
            {
                [_btnInviteOrMsg setTitleColor:[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0] forState:UIControlStateNormal];
            }
            [self.contentView addSubview:_btnInviteOrMsg];
            
        }
        else
        {
            _btnInviteOrMsg=[[UIButton alloc]initWithFrame:CGRectMake(20, 51, _viewContact.frame.size.width/2, 30)];
            [_btnInviteOrMsg setTitle:@"Invite" forState:UIControlStateNormal];
            _btnInviteOrMsg.font=[UIFont fontWithName:FONT_MEDIUM size:15];
            
            NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
            UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
            if (color != nil) {
                [_btnInviteOrMsg setTitleColor:color forState:UIControlStateNormal];
                
            }
            else
            {
                [_btnInviteOrMsg setTitleColor:[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0] forState:UIControlStateNormal];
            }

            [self.contentView addSubview:_btnInviteOrMsg];

            
            _btnAddContact=[[UIButton alloc]initWithFrame:CGRectMake((_viewContact.frame.size.width/2)+20, 51, _viewContact.frame.size.width/2, 30)];
            [_btnAddContact setTitle:@"Add Contact" forState:UIControlStateNormal];
            _btnAddContact.font=[UIFont fontWithName:FONT_MEDIUM size:15];
            
                        if (color != nil) {
                [_btnAddContact setTitleColor:color forState:UIControlStateNormal];
            }
            else
            {
                [_btnAddContact setTitleColor:[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0] forState:UIControlStateNormal];
            }
            [self.contentView addSubview:_btnAddContact];

            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake((_viewContact.frame.size.width/2)-1,51, 0.75, 30)];
            lineView.backgroundColor = [UIColor lightGrayColor];
            [_viewContact addSubview:lineView];
        }
        
    }
    else if ([_message.type isEqualToString:@"audio"])
    {
        
        CGFloat max_witdh = 0.4*self.contentView.frame.size.width;
        _viewContact.frame = CGRectMake(0, 0, max_witdh, 60);
        _viewContact.backgroundColor = [UIColor clearColor];
        _viewContact.userInteractionEnabled = NO;
        [_viewContact sizeToFit];
        
        CGFloat View_x;
        CGFloat View_y;
        CGFloat View_w = _viewContact.frame.size.width;
        CGFloat View_h = _viewContact.frame.size.height;
        UIViewAutoresizing autoresizing;
        
        if (_message.sender == MessageSenderMyself)
        {
            View_x = self.contentView.frame.size.width - View_w - 20;
            View_y = 1;
            autoresizing = UIViewAutoresizingFlexibleLeftMargin;
            //            BOOL succ=[self isSingleLineCase];
            //            View_x -= [self isSingleLineCase]?65.0:0.0;
            //            View_x -= [self isStatusFailedCase]?([self fail_delta]-15):0.0;
        }
        else
        {
            View_x = 20;
            View_y = 1;
            autoresizing = UIViewAutoresizingFlexibleRightMargin;
            
            if (_message.name.length>0)
            {
                View_y=10;
            }
        }
        
        _viewContact.autoresizingMask = autoresizing;
        _viewContact.frame = CGRectMake(View_x, View_y, View_w, View_h);
        
        tempFrame=_viewContact.frame;
        
        
        if (_message.sender == MessageSenderMyself)
        {
            _imgContact=[[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 40, 40)];
            _imgContact.image=[UIImage imageNamed:@"Audi"];
            _imgContact.layer.cornerRadius=_imgContact.frame.size.height/2;
            _imgContact.clipsToBounds=YES;
            [_viewContact addSubview:_imgContact];
            
             float xPos=_viewContact.frame.origin.x + _imgContact.frame.size.width;
            
_btnAudioAction=[[UIButton alloc]initWithFrame:CGRectMake(xPos+15, 12, 30, 30)];          //  [_btnAudioAction setBackgroundImage:[UIImage imageNamed:@"PlayButton.png"] forState:UIControlStateNormal];
            [self.contentView addSubview:_btnAudioAction];
            
           

            CGRect frame = CGRectMake(xPos+45, 16.5, self.contentView.frame.size.width-(xPos+83), 20);
            _slider = [[UISlider alloc] initWithFrame:frame];
          
            [_slider setBackgroundColor:[UIColor clearColor]];
            _slider.minimumValue = 0.00;
            _slider.maximumValue = 10.00;
            _slider.continuous = YES;
            _slider.value = 0;
            _slider.minimumTrackTintColor = [UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
            //  _slider.maximumTrackTintColor = [UIColor colorWithRed:193/255.0 green:194/255.0 blue:194/255.0 alpha:1.0];
         //   _slider.thumbTintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
            
            UIImage* _sliderBarImage = [UIImage imageNamed:@"25 x 5.png"];
            _sliderBarImage=[_sliderBarImage stretchableImageWithLeftCapWidth:8.0 topCapHeight:10.0];
            [_slider setMaximumTrackImage:_sliderBarImage forState:UIControlStateNormal];
            
            UIImage *_sliderThumb=[UIImage imageNamed:@"rec.png"];
            // _sliderThumb=[_sliderThumb stretchableImageWithLeftCapWidth:8.0 topCapHeight:10.0];
            [_slider setThumbImage:_sliderThumb forState:UIControlStateNormal];
            
            
           // [self.contentView addSubview:_slider];

        }
        else
        {
 _btnAudioAction=[[UIButton alloc]initWithFrame:CGRectMake(_viewContact.frame.origin.x + 5, 12, 30, 30)];            [_btnAudioAction setBackgroundImage:[UIImage imageNamed:@"PlayButton.png"] forState:UIControlStateNormal];
            [self.contentView addSubview:_btnAudioAction];

           
            
            CGRect frame = CGRectMake(_viewContact.frame.origin.x + 35, 16.5, _viewContact.frame.size.width-97, 20);
           _slider = [[UISlider alloc] initWithFrame:frame];
            [_slider setBackgroundColor:[UIColor clearColor]];
            _slider.minimumValue = 0.00;
            _slider.maximumValue = 10.00;
            _slider.continuous = YES;
            _slider.value = 0;
            _slider.minimumTrackTintColor = [UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
            //  _slider.maximumTrackTintColor = [UIColor colorWithRed:193/255.0 green:194/255.0 blue:194/255.0 alpha:1.0];
         //   _slider.thumbTintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
            
            UIImage* _sliderBarImage = [UIImage imageNamed:@"25 x 5.png"];
            _sliderBarImage=[_sliderBarImage stretchableImageWithLeftCapWidth:8.0 topCapHeight:10.0];
            [_slider setMaximumTrackImage:_sliderBarImage forState:UIControlStateNormal];
            
            UIImage *_sliderThumb=[UIImage imageNamed:@"rec.png"];
            // _sliderThumb=[_sliderThumb stretchableImageWithLeftCapWidth:8.0 topCapHeight:10.0];
            [_slider setThumbImage:_sliderThumb forState:UIControlStateNormal];
            
            
           // [self.contentView addSubview:_slider];

            
            float xPos=_viewContact.frame.origin.x + _viewContact.frame.size.width;
            
            _imgContact=[[UIImageView alloc]initWithFrame:CGRectMake(xPos-62, 5, 40, 40)];
            _imgContact.image=[UIImage imageNamed:@"Audi"];
            _imgContact.layer.cornerRadius=_imgContact.frame.size.height/2;
            _imgContact.clipsToBounds=YES;
            [_viewContact addSubview:_imgContact];
            
        }
       
        
        
    }
    else if ([_message.type isEqualToString:@"document"])
    {
        
        CGFloat max_witdh = 0.7*self.contentView.frame.size.width;
        _viewContact.frame = CGRectMake(0, 0, max_witdh, 60);
        _viewContact.backgroundColor = [UIColor clearColor];
        _viewContact.userInteractionEnabled = NO;
        [_viewContact sizeToFit];
        
        CGFloat View_x;
        CGFloat View_y;
        CGFloat View_w = _viewContact.frame.size.width;
        CGFloat View_h = _viewContact.frame.size.height;
        UIViewAutoresizing autoresizing;
        
        if (_message.sender == MessageSenderMyself)
        {
            View_x = self.contentView.frame.size.width - View_w - 20;
            View_y = 1;
            autoresizing = UIViewAutoresizingFlexibleLeftMargin;
            //            BOOL succ=[self isSingleLineCase];
            //            View_x -= [self isSingleLineCase]?65.0:0.0;
            //            View_x -= [self isStatusFailedCase]?([self fail_delta]-15):0.0;
        }
        else
        {
            View_x = 20;
            View_y = 1;
            autoresizing = UIViewAutoresizingFlexibleRightMargin;
            
            if (_message.name.length>0)
            {
                View_y=10;
            }
        }
        
        _viewContact.autoresizingMask = autoresizing;
        _viewContact.frame = CGRectMake(View_x, View_y, View_w, View_h);
        
        tempFrame=_viewContact.frame;
        
        
        if (_message.sender == MessageSenderMyself)
        {
            float xPos=_viewContact.frame.origin.x + _documentImg.frame.size.width;

            _documentImg=[[UIImageView alloc]initWithFrame:CGRectMake(10, 18, 20, 20)];
            _documentImg.image=[UIImage imageNamed:@"forms"];
            _documentImg.layer.cornerRadius=_documentImg.frame.size.height/2;
            _documentImg.clipsToBounds=YES;
            [_viewContact addSubview:_documentImg];
            
            
            _docBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, max_witdh, 60)];
            //  [_btnAudioAction setBackgroundImage:[UIImage imageNamed:@"PlayButton.png"] forState:UIControlStateNormal];
            [self.contentView addSubview:_docBtn];
            
            
            CGRect frame = CGRectMake(xPos+45, 16.5, self.contentView.frame.size.width-(xPos+83), 20);
            _documentName = [[UILabel alloc] initWithFrame:frame];
            [_documentName setBackgroundColor:[UIColor clearColor]];
            [self.contentView addSubview:_documentName];
            
            
            
        }
        else
        {
            float xPos=_viewContact.frame.origin.x + _viewContact.frame.size.width;

            _docBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, max_witdh, 60)];
            [_docBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            [self.contentView addSubview:_docBtn];
            
            _documentImg=[[UIImageView alloc]initWithFrame:CGRectMake(_viewContact.frame.origin.x + 10, 18, 20, 20)];
            _documentImg.image = [UIImage imageNamed:@"forms"];
           
            [self.contentView addSubview:_documentImg];
            
            
            
            CGRect frame = CGRectMake(_viewContact.frame.origin.x + 35, 16.5, _viewContact.frame.size.width, 30);
            _documentName = [[UILabel alloc] initWithFrame:frame];
            [_documentName setBackgroundColor:[UIColor clearColor]];
             [self.contentView addSubview:_documentName];
            
            
//            float xPos=_viewContact.frame.origin.x + _viewContact.frame.size.width;
            
//            _docBtn=[[UIButton alloc]initWithFrame:CGRectMake(xPos-62, 5, 40, 40)];
//            [_btnAudioAction setBackgroundImage:[UIImage imageNamed:@"PlayButton.png"] forState:UIControlStateNormal];
//            [_viewContact addSubview:_docBtn];
            
            
            
        }
        
        
        
    }
    else if ([_message.type isEqualToString:@"header"])
    {
       [self setHeaderView];

    }
    else if ([_message.type isEqualToString:@"sticker"])

    {
            CGFloat max_witdh =200;
        _imgView.frame = CGRectMake(0, 0, max_witdh, 200);
        _imgView.backgroundColor = [UIColor clearColor];
        _imgView.userInteractionEnabled = NO;
//        _imgView.image = _message.image;
        [_imgView stk_cancelStickerLoading];
        _imgView.image = nil;
        [_imgView stk_cancelStickerImageLoading: _imgView];

        _imgView.contentMode=UIViewContentModeScaleAspectFill;
        _imgView.clipsToBounds=YES;
        _imgView.layer.cornerRadius=4;
        [_imgView stk_setStickerWithMessage: _message.text placeholder: nil placeholderColor: nil progress: nil completion: nil];
        
        //   NSLog(@"%f",self.contentView.frame.size.width);
        
        
        CGFloat imgView_x;
        CGFloat imgView_y;
        CGFloat imgView_w = _imgView.frame.size.width;
        CGFloat imgView_h = _imgView.frame.size.height;
        UIViewAutoresizing autoresizing;
        
        if (_message.sender == MessageSenderMyself)
        {
            imgView_x = self.contentView.frame.size.width - imgView_w - 15;
//            imgView_x = 200;
            imgView_y = 5;
            autoresizing = UIViewAutoresizingFlexibleLeftMargin;
            // imgView_x -= [self isSingleLineCase]?65.0:0.0;
            // imgView_x -= [self isStatusFailedCase]?([self fail_delta]-15):0.0;
        }
        else
        {
            imgView_x = 20;
            imgView_y = 5;
            autoresizing = UIViewAutoresizingFlexibleRightMargin;
            
            if (_message.name.length>0)
            {
                imgView_y=17;
            }
        }
        
        _imgView.autoresizingMask = autoresizing;
        _imgView.frame = CGRectMake(imgView_x, imgView_y, imgView_w, imgView_h);
        tempFrame=_imgView.frame;
        
        NSLog(@"%@",_message.caption);
        if ([_message.caption length]!=0)
        {
            
            CGFloat max_witdh = 0.7*self.contentView.frame.size.width;
            _textView.frame = CGRectMake(0, 0, max_witdh, MAXFLOAT);
            _textView.font = [UIFont fontWithName:FONT_NORMAL size:12.0];
            _textView.backgroundColor = [UIColor clearColor];
            _textView.userInteractionEnabled = NO;
            _textView.text = _message.caption;
            
            
            [_textView sizeToFit];
            
            CGFloat textView_x;
            CGFloat textView_y;
            CGFloat textView_w = _textView.frame.size.width;
            CGFloat textView_h = _textView.frame.size.height;
            UIViewAutoresizing autoresizing;
            
            
            textView_x = imgView_x+5;
            textView_y = imgView_y+imgView_h+1;
            autoresizing = UIViewAutoresizingFlexibleRightMargin;
            
            _textView.autoresizingMask = autoresizing;
            _textView.frame = CGRectMake(textView_x, textView_y, textView_w, textView_h);
            
            tempFrame=CGRectMake(imgView_x, imgView_y, imgView_w, imgView_h+textView_h);
            
        }
        
        
    }
    
    else
    {
        
        
//        NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:@"(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))" options:NSRegularExpressionCaseInsensitive error:NULL];
//        NSString *someString = _message.text;
//        NSString *match = [someString substringWithRange:[expression rangeOfFirstMatchInString:someString options:NSMatchingCompleted range:NSMakeRange(0, [someString length])]];
//        NSLog(@"%@", match);
        NSLog(@"%@",_message.showPreview);
        if ([_message.showPreview isEqualToString:@"1"])
        {
         
        if ([self validateUrl:_message.text])
        {
                   CGFloat max_witdh = 0.7*self.contentView.frame.size.width;
            _viewContact.frame = CGRectMake(0, 0, max_witdh, 100);
            _viewContact.backgroundColor = [UIColor clearColor];
            _viewContact.userInteractionEnabled = NO;
            [_viewContact sizeToFit];
            
            
            
            CGFloat View_x;
            CGFloat View_y;
            CGFloat View_w = _viewContact.frame.size.width;
            CGFloat View_h = _viewContact.frame.size.height;
            UIViewAutoresizing autoresizing;
            
            if (_message.sender == MessageSenderMyself)
            {
                View_x = self.contentView.frame.size.width - View_w - 20 ;
                View_y = 1;
                autoresizing = UIViewAutoresizingFlexibleLeftMargin;
                //            BOOL succ=[self isSingleLineCase];
                //            View_x -= [self isSingleLineCase]?65.0:0.0;
                //            View_x -= [self isStatusFailedCase]?([self fail_delta]-15):0.0;
            }
            else
            {
                View_x = 20;
                View_y = 1;
                autoresizing = UIViewAutoresizingFlexibleRightMargin;
                
                if (_message.name.length>0)
                {
                    View_y=10;
                }
            }
            
            _viewContact.autoresizingMask = autoresizing;
            _viewContact.frame = CGRectMake(View_x, View_y, View_w, View_h);
            
            tempFrame=_viewContact.frame;
            
            
            
            NSString *CheckImgstr=[_message.linkLogo substringToIndex:4];
            if ([CheckImgstr isEqualToString:@"http"])
            {
                _LogoImg=[[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 40, 50)];
                [_LogoImg sd_setImageWithURL:[NSURL URLWithString:_message.linkLogo]
                            placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
                
                [_viewContact addSubview:_LogoImg];
                
                
                
                _DescriptionLbl=[[UILabel alloc]initWithFrame:CGRectMake(60, 25, _viewContact.frame.size.width-65, 40)];
                _DescriptionLbl.text=_message.linkDescription;
                _DescriptionLbl.font=[UIFont fontWithName:FONT_NORMAL size:13];
                _DescriptionLbl.lineBreakMode = NSLineBreakByWordWrapping;
                _DescriptionLbl.numberOfLines = 0;
                
                [_viewContact addSubview:_DescriptionLbl];
                
                
                
                _TitleLbl=[[UILabel alloc]initWithFrame:CGRectMake(60, 10, _viewContact.frame.size.width-65, 20)];
                _TitleLbl.text=_message.linkTitle;
                _TitleLbl.font=[UIFont fontWithName:FONT_MEDIUM size:13];
                [_viewContact addSubview:_TitleLbl];
                
                
                
                
                _LinkView=[[UILabel alloc]initWithFrame:CGRectMake(0, 60, _viewContact.frame.size.width-65, 25)];
                _LinkView.textColor=[UIColor blueColor];
                _LinkView.text=_message.text;
                _LinkView.font=[UIFont fontWithName:FONT_NORMAL size:13];
                [_viewContact addSubview:_LinkView];
                //            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 60, _viewContact.frame.size.width-65, 1)];
                //            lineView.backgroundColor = [UIColor blueColor];
                //            [_viewContact addSubview:lineView];

            }
            else
            {
           
                
                
//                _LogoImg=[[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 40, 50)];
                _DescriptionLbl=[[UILabel alloc]initWithFrame:CGRectMake(10, 25, _viewContact.frame.size.width-45, 40)];
                _DescriptionLbl.text=_message.linkDescription;
                _DescriptionLbl.font=[UIFont fontWithName:FONT_NORMAL size:13];
                _DescriptionLbl.lineBreakMode = NSLineBreakByWordWrapping;
                _DescriptionLbl.numberOfLines = 0;
                
                [_viewContact addSubview:_DescriptionLbl];

            
          
            _TitleLbl=[[UILabel alloc]initWithFrame:CGRectMake(10, 10, _viewContact.frame.size.width-45, 20)];
            _TitleLbl.text=_message.linkTitle;
            _TitleLbl.font=[UIFont fontWithName:FONT_MEDIUM size:13];
            [_viewContact addSubview:_TitleLbl];
            
            
            
            
            _LinkView=[[UILabel alloc]initWithFrame:CGRectMake(0, 60, _viewContact.frame.size.width-65, 25)];
            _LinkView.textColor=[UIColor blueColor];
            _LinkView.text=_message.text;
            _LinkView.font=[UIFont fontWithName:FONT_NORMAL size:13];
            [_viewContact addSubview:_LinkView];
//            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 60, _viewContact.frame.size.width-65, 1)];
//            lineView.backgroundColor = [UIColor blueColor];
//            [_viewContact addSubview:lineView];
            
            }
            _LinkBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 60, _viewContact.frame.size.width, 25)];
            [self.contentView addSubview:_LinkBtn];
            
            if (_message.sender == MessageSenderMyself)
            {
                
            }
            else
            {
                
               
            }
            UIView *lineViews = [[UIView alloc] initWithFrame:CGRectMake(0,(_LinkView.frame.origin.y), (_viewContact.frame.size.width), 1)];
            lineViews.backgroundColor = [UIColor lightGrayColor];
            [_viewContact addSubview:lineViews];
            
        }
        }
        else
        {
            
            if ([self validateUrl:_message.text])
            {
                CGFloat max_witdh = 0.7*self.contentView.frame.size.width;
                _textView.frame = CGRectMake(0, 0, max_witdh, MAXFLOAT);
                _textView.font = [UIFont fontWithName:FONT_NORMAL size:15.0];
                _textView.backgroundColor = [UIColor clearColor];
                _textView.userInteractionEnabled = NO;
                _textView.text = _message.text;
                _textView.textColor=[UIColor blueColor];
                
                _LinkBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, max_witdh, MAXFLOAT)];
                [self.contentView addSubview:_LinkBtn];
//                UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, MAXFLOAT, max_witdh, 1)];
//                lineView.backgroundColor = [UIColor blueColor];
//                [_textView addSubview:lineView];
                [_textView sizeToFit];
                
                CGFloat textView_x;
                CGFloat textView_y;
                CGFloat textView_w = _textView.frame.size.width;
                CGFloat textView_h = _textView.frame.size.height;
                UIViewAutoresizing autoresizing;
                
                if (_message.sender == MessageSenderMyself)
                {
                    textView_x = self.contentView.frame.size.width - textView_w - 20;
                    textView_y = -3;
                    autoresizing = UIViewAutoresizingFlexibleLeftMargin;
                    BOOL succ=[self isSingleLineCase];
                    textView_x -= [self isSingleLineCase]?65.0:0.0;
                    textView_x -= [self isStatusFailedCase]?([self fail_delta]-15):0.0;
                }
                else
                {
                    textView_x = 20;
                    textView_y = -1;
                    
                    autoresizing = UIViewAutoresizingFlexibleRightMargin;
                    
                    if (_message.name.length>0)
                    {
                        textView_y=10;
                    }
                }
                
                _textView.autoresizingMask = autoresizing;
                _textView.frame = CGRectMake(textView_x, textView_y, textView_w, textView_h);
                
                tempFrame=_textView.frame;
            }
            else
            {
            CGFloat max_witdh = 0.7*self.contentView.frame.size.width;
            _textView.frame = CGRectMake(0, 0, max_witdh, MAXFLOAT);
                 _textView.text = _message.text;
                _textView.font = [UIFont fontWithName:FONT_NORMAL size:15.0];
              /*  NSString *myString = _message.text;
                NSMutableArray *array = [NSMutableArray array];
                
                for (int i = 0; i < [myString length]; i++) {
                    NSString *ch = [myString substringWithRange:NSMakeRange(i, 1)];
                    [array addObject:ch];
                }
 
                        for( NSString *EmojiString in array)
                        {
//                             BOOL containsEmoji = [self isEmoji:EmojiString];
//                            if (containsEmoji) {
                            if(![EmojiString canBeConvertedToEncoding:NSASCIIStringEncoding])
                            {
                               
                                _textView.font = [UIFont fontWithName:FONT_NORMAL size:30.0];
                            }
//                            }
//                            else if([EmojiString isEqualToString:@"@"""])
//                            {
//                                _textView.font = [UIFont fontWithName:FONT_NORMAL size:30.0];
//                            }
                            else
                            {
                                _textView.font = [UIFont fontWithName:FONT_NORMAL size:15.0];
                            }
                        }
                        
                */

            
            _textView.backgroundColor = [UIColor clearColor];
            _textView.userInteractionEnabled = NO;
           
            
            
            [_textView sizeToFit];
            
            CGFloat textView_x;
            CGFloat textView_y;
            CGFloat textView_w = _textView.frame.size.width;
            CGFloat textView_h = _textView.frame.size.height;
            UIViewAutoresizing autoresizing;
            
            if (_message.sender == MessageSenderMyself)
            {
                textView_x = self.contentView.frame.size.width - textView_w - 20;
                textView_y = -3;
                autoresizing = UIViewAutoresizingFlexibleLeftMargin;
                BOOL succ=[self isSingleLineCase];
                textView_x -= [self isSingleLineCase]?65.0:0.0;
                textView_x -= [self isStatusFailedCase]?([self fail_delta]-15):0.0;
            }
            else
            {
                textView_x = 20;
                textView_y = -1;
                autoresizing = UIViewAutoresizingFlexibleRightMargin;
                
                if (_message.name.length>0)
                {
                    textView_y=10;
                }
            }
            
            _textView.autoresizingMask = autoresizing;
            _textView.frame = CGRectMake(textView_x, textView_y, textView_w, textView_h);
            
            tempFrame=_textView.frame;
        }
        }
    }
}

#pragma mark - CheckStringContains Emoji
-(BOOL)isEmoji:(NSString *)character {//argument can be character or entire string
    
    UILabel *characterRender = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    characterRender.text = character;
    characterRender.backgroundColor = [UIColor blackColor];//needed to remove subpixel rendering colors
    [characterRender sizeToFit];
    
    CGRect rect = [characterRender bounds];
    UIGraphicsBeginImageContextWithOptions(rect.size,YES,0.0f);
    CGContextRef contextSnap = UIGraphicsGetCurrentContext();
    [characterRender.layer renderInContext:contextSnap];
    UIImage *capturedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CGImageRef imageRef = [capturedImage CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *rawData = (unsigned char*) calloc(height * width * 4, sizeof(unsigned char));
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
    BOOL colorPixelFound = NO;
    
    int x = 0;
    int y = 0;
    while (y < height && !colorPixelFound) {
        while (x < width && !colorPixelFound) {
            
            NSUInteger byteIndex = (bytesPerRow * y) + x * bytesPerPixel;
            
            CGFloat red = (CGFloat)rawData[byteIndex];
            CGFloat green = (CGFloat)rawData[byteIndex+1];
            CGFloat blue = (CGFloat)rawData[byteIndex+2];
            
            CGFloat h, s, b, a;
            UIColor *c = [UIColor colorWithRed:red green:green blue:blue alpha:1.0f];
            [c getHue:&h saturation:&s brightness:&b alpha:&a];
            
            b /= 255.0f;
            
            if (b > 0) {
                colorPixelFound = YES;
            }
            
            x++;
        }
        x=0;
        y++;
    }
    
    return colorPixelFound;
    
}
-(void)CheckEmoji
{
    
}
#pragma mark - HeaderView
-(void)setHeaderView
{
    [_timeLabel setHidden:YES];
    [_textView setHidden:YES];
    [_bubbleImage setHidden:YES];
    [_statusIcon setHidden:YES];
    [_nameLabel setHidden:YES];

    CGFloat max_witdh = self.contentView.frame.size.width;
    _HeadertextView.frame = CGRectMake(30, 0, max_witdh-30, MAXFLOAT);
    _HeadertextView.font = [UIFont fontWithName:FONT_NORMAL size:13.0];
    _HeadertextView.userInteractionEnabled = NO;
    _HeadertextView.text = _message.text;
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        _HeadertextView.backgroundColor = color;
        _HeadertextView.textColor=[UIColor whiteColor];
        
        
        
    }
    else
    {
        _HeadertextView.backgroundColor = [UIColor colorWithRed:207/255.0 green:220/255.0 blue:252.0/255.0 alpha:1];
    }
 
    [_HeadertextView.layer setCornerRadius:10.0f];
    [_HeadertextView.layer setMasksToBounds:YES];
    
    [_HeadertextView sizeToFit];
    
    
    
    
    
    
    CGFloat textView_x;
    CGFloat textView_y;
    CGFloat textView_w = _HeadertextView.frame.size.width;
    CGFloat textView_h = _HeadertextView.frame.size.height;
    UIViewAutoresizing autoresizing;
    
        textView_x = (self.contentView.frame.size.width - textView_w)/2;
        textView_y = 10;
        autoresizing = UIViewAutoresizingFlexibleRightMargin;
        
        if (_message.name.length>0)
        {
            textView_y=10;
        }
    
    _HeadertextView.autoresizingMask = autoresizing;
    _HeadertextView.frame = CGRectMake(textView_x, textView_y, textView_w, textView_h);
    
    
    tempFrame=_HeadertextView.frame;
}
- (BOOL) validateUrl: (NSString *) candidate {
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
}

#pragma mark - NameLabel

-(void)setNameLabel
{
    if (_message.name.length!=0)
    {
        _nameLabel.frame = CGRectMake(0, 0, 100, 14);
        _nameLabel.textColor = [UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
        UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
        if (color != nil) {
            _nameLabel.textColor = color;

            
        }
        else
        {
            _nameLabel.textColor = [UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];

        }
        _nameLabel.font = [UIFont fontWithName:FONT_NORMAL size:12.0];
        _nameLabel.userInteractionEnabled = NO;
        _nameLabel.alpha = 0.7;
        _nameLabel.textAlignment = NSTextAlignmentLeft;
        
        self.nameLabel.text = _message.name;
        
        //Set position
        CGFloat name_x;
        CGFloat name_y = 3;
        
        if (_message.sender == MessageSenderMyself)
        {
            name_x = 20;
        }
        else
        {
            name_x = 20;
        }
        
        if ([self isSingleLineCase])
        {
            name_x = 20;
            name_y = 3;
        }
        
        _nameLabel.frame = CGRectMake(name_x,
                                      name_y,
                                      _nameLabel.frame.size.width,
                                      _nameLabel.frame.size.height);
        
        if ([_message.type isEqualToString:@"image"] || [_message.type isEqualToString:@"location"] || [_message.type isEqualToString:@"video"])
        {
            _nameLabel.autoresizingMask = _imgView.autoresizingMask;
        }
        else
        {
            _nameLabel.autoresizingMask = _textView.autoresizingMask;
        }
        

    }
    
}

#pragma mark - TimeLabel

-(void)setTimeLabel
{
    NSLog(@"%@",_message.type);
    _timeLabel.frame = CGRectMake(0, 0, 52, 14);
    _timeLabel.textColor = [UIColor lightGrayColor];
    _timeLabel.font = [UIFont fontWithName:FONT_NORMAL size:10.0];
    _timeLabel.userInteractionEnabled = NO;
    _timeLabel.alpha = 0.7;
    _timeLabel.textAlignment = NSTextAlignmentRight;
   // _timeLabel.backgroundColor=[UIColor redColor];
   
    
    //Set Text to Label
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.timeStyle = NSDateFormatterShortStyle;
    df.dateStyle = NSDateFormatterNoStyle;
    df.doesRelativeDateFormatting = YES;
    self.timeLabel.text = [df stringFromDate:_message.date];
    
    //Set position
    CGFloat time_x;
    CGFloat time_y = tempFrame.origin.y+ tempFrame.size.height - 14;
    
    if (_message.sender == MessageSenderMyself)
    {
//        NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:@"(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))" options:NSRegularExpressionCaseInsensitive error:NULL];
//        NSString *someString = _message.text;
//        NSString *match = [someString substringWithRange:[expression rangeOfFirstMatchInString:someString options:NSMatchingCompleted range:NSMakeRange(0, [someString length])]];
//        NSLog(@"%@", match);

        if ([self validateUrl:_message.text])
        {
            time_x = 230;
        }
        else{
        
        time_x = tempFrame.origin.x + tempFrame.size.width - _timeLabel.frame.size.width - 20;
        }
    }
    else
    {
        if ([_message.type isEqualToString:@"image"] || [_message.type isEqualToString:@"location"] || [_message.type isEqualToString:@"video"]|| [_message.type isEqualToString:@"sticker"])
        {
            time_x = MAX(tempFrame.origin.x + tempFrame.size.width - _timeLabel.frame.size.width,
                         tempFrame.origin.x);
        }
        else if ([_message.type isEqualToString:@"audio"] || [_message.type isEqualToString:@"document"])
        {
            time_x=_imgContact.frame.origin.x-52;
        }
        else
        {
            time_x = MAX(tempFrame.origin.x + tempFrame.size.width - _timeLabel.frame.size.width,
                         _nameLabel.frame.origin.x+_nameLabel.frame.size.height-_timeLabel.frame.size.width);
           
        }
        
    }
    
    if ((![_message.type isEqualToString:@"contact"] && ![_message.type isEqualToString:@"audio"])|| [_message.type isEqualToString:@"document"])
    {
        if ([self isSingleLineCase])
        {
            if (_message.name.length==0)
            {
//                NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:@"(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))" options:NSRegularExpressionCaseInsensitive error:NULL];
//                NSString *someString = _message.text;
//                NSString *match = [someString substringWithRange:[expression rangeOfFirstMatchInString:someString options:NSMatchingCompleted range:NSMakeRange(0, [someString length])]];
//                NSLog(@"%@", match);
                
                if ([self validateUrl:_message.text])

                {
                    time_x = 230;
                    time_y -= 10;
                }
                else{
                
                time_x = tempFrame.origin.x + tempFrame.size.width - 5;
                time_y -= 10;
                }
            }
            else
            {
                time_x=MAX(tempFrame.origin.x + tempFrame.size.width - 5,
                           _nameLabel.frame.origin.x+_nameLabel.frame.size.width-_timeLabel.frame.size.width);
                time_y -= 10;
            }
        }
    }
    else
    {
        time_y=35;
        
    }
    
   
    
    _timeLabel.frame = CGRectMake(time_x,
                                  time_y,
                                  _timeLabel.frame.size.width,
                                  _timeLabel.frame.size.height);
    
    if ([_message.type isEqualToString:@"image"] || [_message.type isEqualToString:@"location"] || [_message.type isEqualToString:@"video"]|| [_message.type isEqualToString:@"sticker"])
    {
        _timeLabel.autoresizingMask = _imgView.autoresizingMask;
    }
    else if (![_message.type isEqualToString:@"contact"] || [_message.type isEqualToString:@"document"])
    {
          _timeLabel.autoresizingMask = _viewContact.autoresizingMask;
    }
    else
    {
        _timeLabel.autoresizingMask = _textView.autoresizingMask;
    }
    
    if ([_message.type isEqualToString:@"image"] || [_message.type isEqualToString:@"location"] || [_message.type isEqualToString:@"video"]|| [_message.type isEqualToString:@"sticker"])
    {
        if ([_message.caption length]==0)
        {
            _timeLabel.textColor = [UIColor whiteColor];
            _timeLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blackBG.png"]];
            [_timeLabel sizeToFit];
        }
    }
    
}
-(BOOL)isSingleLineCase
{
    if ([_message.type isEqualToString:@"image"] || [_message.type isEqualToString:@"location"] || [_message.type isEqualToString:@"video"]|| [_message.type isEqualToString:@"sticker"])
    {
        
        CGFloat delta_x = _message.sender == MessageSenderMyself?65.0:44.0;
        
        CGFloat cell_height = tempFrame.size.height;
        CGFloat cell_width = tempFrame.size.width;
        CGFloat view_width = self.contentView.frame.size.width;
        
        //Single Line Case
        return (cell_height <= 45 && cell_width + delta_x <= 0.8*view_width)?YES:NO;
    }
    else
    {
        CGFloat delta_x = _message.sender == MessageSenderMyself?65.0:44.0;
        
        CGFloat textView_height = _textView.frame.size.height;
        CGFloat textView_width = _textView.frame.size.width;
        CGFloat view_width = self.contentView.frame.size.width;
        
        //Single Line Case
        return (textView_height <= 45 && textView_width + delta_x <= 0.8*view_width)?YES:NO;
    }
    
}

-(void)setStaring
{
    {
        NSLog(@"%@",_message.type);
        _StarimgView.frame = CGRectMake(0, 0, 10, 10);
        _StarimgView.image = [UIImage imageNamed:@""];
        
       
        
        //Set position
        CGFloat time_x;
        CGFloat time_y = tempFrame.origin.y+ tempFrame.size.height - 14;
        
        if (_message.sender == MessageSenderMyself)
        {
            time_x = tempFrame.origin.x + tempFrame.size.width - _StarimgView.frame.size.width - 20;
        }
        else
        {
            if ([_message.type isEqualToString:@"image"] || [_message.type isEqualToString:@"location"] || [_message.type isEqualToString:@"video"]|| [_message.type isEqualToString:@"sticker"])
            {
                time_x = MAX(tempFrame.origin.x + tempFrame.size.width - _StarimgView.frame.size.width,
                             tempFrame.origin.x);
            }
            else if ([_message.type isEqualToString:@"audio"] || [_message.type isEqualToString:@"document"])
            {
                time_x=_imgContact.frame.origin.x-52;
            }
            else
            {
                time_x = MAX(tempFrame.origin.x + tempFrame.size.width - _StarimgView.frame.size.width,
                             _nameLabel.frame.origin.x+_nameLabel.frame.size.height-_StarimgView.frame.size.width);
                
            }
            
        }
        
        if (![_message.type isEqualToString:@"contact"] && ![_message.type isEqualToString:@"audio"])
        {
            if ([self isSingleLineCase])
            {
                if (_message.name.length==0)
                {
                    time_x = tempFrame.origin.x + tempFrame.size.width - 5;
                    time_y -= 10;
                }
                else
                {
                    time_x=MAX(tempFrame.origin.x + tempFrame.size.width - 5,
                               _nameLabel.frame.origin.x+_nameLabel.frame.size.width-_StarimgView.frame.size.width);
                    time_y -= 10;
                }
            }
        }
        else
        {
            time_y=35;
            
        }
        
        
        
        _StarimgView.frame = CGRectMake(time_x,
                                      time_y,
                                      _StarimgView.frame.size.width,
                                      _StarimgView.frame.size.height);
        
        if ([_message.type isEqualToString:@"image"] || [_message.type isEqualToString:@"location"] || [_message.type isEqualToString:@"video"]|| [_message.type isEqualToString:@"sticker"])
        {
            _StarimgView.autoresizingMask = _imgView.autoresizingMask;
        }
        else if (![_message.type isEqualToString:@"contact"] || [_message.type isEqualToString:@"document"])
        {
            _StarimgView.autoresizingMask = _viewContact.autoresizingMask;
        }
        else
        {
            _StarimgView.autoresizingMask = _textView.autoresizingMask;
        }
        
        if ([_message.type isEqualToString:@"image"] || [_message.type isEqualToString:@"location"] || [_message.type isEqualToString:@"video"]|| [_message.type isEqualToString:@"sticker"])
        {
//            if ([_message.caption length]==0)
//            {
//                _StarimgView.textColor = [UIColor whiteColor];
//                _timeLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blackBG.png"]];
//                [_timeLabel sizeToFit];
//            }
        }
        
    }
}

#pragma mark - Bubble

- (void)setBubble
{
    if (_message.name.length==0)
    {
        //Margins to Bubble
        CGFloat marginLeft = 5;
        CGFloat marginRight = 2;
        
        //Bubble positions
        CGFloat bubble_x;
        CGFloat bubble_y = 0;
        CGFloat bubble_width;
        CGFloat bubble_height;
        if ([_message.type isEqualToString:@"image"] || [_message.type isEqualToString:@"location"] || [_message.type isEqualToString:@"video"] || [_message.type isEqualToString:@"sticker"])
        {
            if ([_message.caption length]==0)
            {
                bubble_height = MAX(tempFrame.size.height + 8,
                                    _timeLabel.frame.origin.y + _timeLabel.frame.size.height + 9);
            }
            else
            {
                bubble_height = MIN(tempFrame.size.height + 8,
                                    _timeLabel.frame.origin.y + _timeLabel.frame.size.height + 6);
            }
            
        }
        else if ([_message.type isEqualToString:@"contact"])
        {
            bubble_height =tempFrame.size.height+5;
            
        }
        else
        {
            bubble_height = MIN(tempFrame.size.height + 8,
                                _timeLabel.frame.origin.y + _timeLabel.frame.size.height + 6);
        }
        
        
        if (_message.sender == MessageSenderMyself)
        {
            
            bubble_x = MIN(tempFrame.origin.x -marginLeft,_timeLabel.frame.origin.x - 2*marginLeft);
            
            _bubbleImage.image = [[self imageNamed:@"bubbleMine"]
                                  stretchableImageWithLeftCapWidth:15 topCapHeight:14];
            
            
            bubble_width = self.contentView.frame.size.width - bubble_x - marginRight;
            bubble_width -= [self isStatusFailedCase]?[self fail_delta]:0.0;
        }
        else if (_message.sender == MessageSenderSomeone)
        {
            bubble_x = marginRight;
            
            _bubbleImage.image = [[self imageNamed:@"bubbleSomeone"]
                                  stretchableImageWithLeftCapWidth:21 topCapHeight:14];
            
            bubble_width = MAX(tempFrame.origin.x + tempFrame.size.width + marginLeft,
                               _timeLabel.frame.origin.x + _timeLabel.frame.size.width + 2*marginLeft);
            
            if ([_message.type isEqualToString:@"contact"])
            {
                bubble_width =_viewContact.frame.size.width+23;
            }
            
        }
        
        _bubbleImage.frame = CGRectMake(bubble_x, bubble_y, bubble_width, bubble_height);
        if ([_message.type isEqualToString:@"image"] || [_message.type isEqualToString:@"location"] || [_message.type isEqualToString:@"video"] || [_message.type isEqualToString:@"sticker"])
        {
            _bubbleImage.autoresizingMask = _imgView.autoresizingMask;
        }
        else if ([_message.type isEqualToString:@"contact"])
        {
            _bubbleImage.autoresizingMask = _viewContact.autoresizingMask;
            
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0,50, _viewContact.frame.size.width, 0.75)];
            lineView.backgroundColor = [UIColor lightGrayColor];
//            [_viewContact addSubview:lineView];
            
        }
        else
        {
            _bubbleImage.autoresizingMask = _textView.autoresizingMask;
        }
        
    }
    else
    {
        //Margins to Bubble
        CGFloat marginLeft = 5;
        CGFloat marginRight = 2;
        
        //Bubble positions
        CGFloat bubble_x;
        CGFloat bubble_y = 0;
        CGFloat bubble_width;
        CGFloat bubble_height;
        if ([_message.type isEqualToString:@"image"] || [_message.type isEqualToString:@"location"] || [_message.type isEqualToString:@"video"] || [_message.type isEqualToString:@"sticker"])
        {
            if ([_message.caption length]==0)
            {
                bubble_height = MIN(tempFrame.size.height + _nameLabel.frame.size.height +12,
                                    _timeLabel.frame.origin.y + _timeLabel.frame.size.height + _nameLabel.frame.size.height);
            }
            else
            {
                bubble_height = MIN(tempFrame.size.height + _nameLabel.frame.size.height,
                                    _timeLabel.frame.origin.y + _timeLabel.frame.size.height + _nameLabel.frame.size.height);
            }
            
        }
        else if ([_message.type isEqualToString:@"contact"])
        {
            bubble_height =tempFrame.size.height+20;
        }
        else
        {
            if ([self isSingleLineCase])
            {
                bubble_height = MIN(tempFrame.size.height +  _nameLabel.frame.size.height-8,
                                    _timeLabel.frame.origin.y + _timeLabel.frame.size.height + _nameLabel.frame.size.height);
            }
            else
            {
                bubble_height = MIN(tempFrame.size.height +  _nameLabel.frame.size.height,
                                    _timeLabel.frame.origin.y + _timeLabel.frame.size.height + _nameLabel.frame.size.height);
            }
            
        }
        
        
        if (_message.sender == MessageSenderMyself)
        {
            
            bubble_x = MIN(tempFrame.origin.x -marginLeft,_timeLabel.frame.origin.x - 2*marginLeft);
            
            _bubbleImage.image = [[self imageNamed:@"bubbleMine"]
                                  stretchableImageWithLeftCapWidth:15 topCapHeight:14];
            
            
            bubble_width = self.contentView.frame.size.width - bubble_x - marginRight;
            bubble_width -= [self isStatusFailedCase]?[self fail_delta]:0.0;
        }
        else
        {
            bubble_x = marginRight;
            
            _bubbleImage.image = [[self imageNamed:@"bubbleSomeone"]
                                  stretchableImageWithLeftCapWidth:21 topCapHeight:14];
            
            if ([_message.type isEqualToString:@"image"] || [_message.type isEqualToString:@"location"] || [_message.type isEqualToString:@"video"] || [_message.type isEqualToString:@"sticker"])
            {
                bubble_width = MAX(tempFrame.origin.x + tempFrame.size.width + marginLeft,
                                   _timeLabel.frame.origin.x + _timeLabel.frame.size.width + 2*marginLeft);
            }
            else
            {
                bubble_width = MAX(_nameLabel.frame.origin.x + _nameLabel.frame.size.width + marginLeft,
                                   _timeLabel.frame.origin.x + _timeLabel.frame.size.width + 2*marginLeft);
            }
            
            if ([_message.type isEqualToString:@"contact"])
            {
                bubble_width =_viewContact.frame.size.width+23;
            }
            
        }
        
        _bubbleImage.frame = CGRectMake(bubble_x, bubble_y, bubble_width, bubble_height);
        if ([_message.type isEqualToString:@"image"] || [_message.type isEqualToString:@"location"] || [_message.type isEqualToString:@"video"] || [_message.type isEqualToString:@"sticker"])
        {
            _bubbleImage.autoresizingMask = _imgView.autoresizingMask;
        }
        else if ([_message.type isEqualToString:@"contact"])
        {
            _bubbleImage.autoresizingMask = _viewContact.autoresizingMask;
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0,50, _viewContact.frame.size.width, 0.75)];
            lineView.backgroundColor = [UIColor lightGrayColor];
//            [_viewContact addSubview:lineView];
            
        }
        else
        {
            _bubbleImage.autoresizingMask = _textView.autoresizingMask;
        }
        
    }
}

#pragma mark - StatusIcon

-(void)addStatusIcon
{
    CGRect time_frame = _timeLabel.frame;
    CGRect status_frame = CGRectMake(0, 0, 15, 14);
    status_frame.origin.x = time_frame.origin.x + time_frame.size.width + 5;
    status_frame.origin.y = time_frame.origin.y;
    _statusIcon.frame = status_frame;
    _statusIcon.contentMode = UIViewContentModeLeft;
    if ([_message.type isEqualToString:@"image"] || [_message.type isEqualToString:@"location"] || [_message.type isEqualToString:@"video"])
    {
        if ([_message.caption length]==0)
        {
           
          //  _statusIcon.backgroundColor = [[UIColor whiteColor]colorWithAlphaComponent:0.5];
        }

        _statusIcon.autoresizingMask = _imgView.autoresizingMask;
    }
    else
    {
        _statusIcon.autoresizingMask = _textView.autoresizingMask;
    }
    
}
-(void)setStatusIcon
{
    
    
    AppDelegate *appdel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
   if ([appdel.RoomTYPE isEqualToString:@"0"])
   {
    if (self.message.status == MessageStatusSending || self.message.status == MessageStatusCancelled || self.message.status == MessageStatusUploaded)
        _statusIcon.image = [self imageNamed:@"status_sending"];
    else if (self.message.status == MessageStatusSent)
        _statusIcon.image = [self imageNamed:@"status_sent"];
    else if (self.message.status == MessageStatusReceived)
        _statusIcon.image = [self imageNamed:@"status_notified"];
    else if (self.message.status == MessageStatusRead)
        _statusIcon.image = [self imageNamed:@"status_read"];
    if (self.message.status == MessageStatusFailed)
        _statusIcon.image = nil;
    
    _statusIcon.hidden = _message.sender == MessageSenderSomeone;
   }
    else if ([appdel.RoomTYPE isEqualToString:@"1"])
    {
        if (self.message.status == MessageStatusSending || self.message.status == MessageStatusCancelled || self.message.status == MessageStatusUploaded)
            _statusIcon.image = [self imageNamed:@"status_sending"];
        else if (self.message.status == MessageStatusSent)
            _statusIcon.image = [self imageNamed:@"status_sent"];
        else if (self.message.status == MessageStatusReceived)
//            _statusIcon.image = [self imageNamed:@"status_notified"];
            _statusIcon.image = [self imageNamed:@"status_sent"];

        else if (self.message.status == MessageStatusRead)
//            _statusIcon.image = [self imageNamed:@"status_read"];
            _statusIcon.image = [self imageNamed:@"status_sent"];

//        if (self.message.status == MessageStatusFailed)
//            _statusIcon.image = nil;
        
        _statusIcon.hidden = _message.sender == MessageSenderSomeone;
    }
    else if ([appdel.RoomTYPE isEqualToString:@"2"])
    {
        if (self.message.status == MessageStatusSending || self.message.status == MessageStatusCancelled || self.message.status == MessageStatusUploaded)
            _statusIcon.image = [self imageNamed:@"status_sending"];
        else if (self.message.status == MessageStatusSent)
            _statusIcon.image = [self imageNamed:@"status_sent"];
        else if (self.message.status == MessageStatusReceived)
            //            _statusIcon.image = [self imageNamed:@"status_notified"];
            _statusIcon.image = [self imageNamed:@"status_sent"];
        
        else if (self.message.status == MessageStatusRead)
            //            _statusIcon.image = [self imageNamed:@"status_read"];
            _statusIcon.image = [self imageNamed:@"status_sent"];
        _statusIcon.hidden = _message.sender == MessageSenderSomeone;
    }
    else
    {
        if (self.message.status == MessageStatusSending || self.message.status == MessageStatusCancelled || self.message.status == MessageStatusUploaded)
            _statusIcon.image = [self imageNamed:@"status_sending"];
        else if (self.message.status == MessageStatusSent)
            _statusIcon.image = [self imageNamed:@"status_sent"];
        else if (self.message.status == MessageStatusReceived)
            _statusIcon.image = [self imageNamed:@"status_notified"];
        else if (self.message.status == MessageStatusRead)
            _statusIcon.image = [self imageNamed:@"status_read"];
        if (self.message.status == MessageStatusFailed)
            _statusIcon.image = nil;
        
        _statusIcon.hidden = _message.sender == MessageSenderSomeone;
    }
}

#pragma mark - Failed Case

//
// This delta is how much TextView
// and Bubble should shit left
//
-(NSInteger)fail_delta
{
    return 60;
}
-(BOOL)isStatusFailedCase
{
    return self.message.status == MessageStatusFailed;
}
-(void)setFailedButton
{
    NSInteger b_size = 22;
    CGRect frame = CGRectMake(self.contentView.frame.size.width - b_size - [self fail_delta]/2 + 5,
                              (self.contentView.frame.size.height - b_size)/2,
                              b_size,
                              b_size);
    
    _resendButton.frame = frame;
    _resendButton.hidden = ![self isStatusFailedCase];
    [_resendButton setImage:[self imageNamed:@"status_failed"] forState:UIControlStateNormal];
}

#pragma mark - UIImage Helper

-(UIImage *)imageNamed:(NSString *)imageName
{
    return [UIImage imageNamed:imageName
                      inBundle:[NSBundle bundleForClass:[self class]]
 compatibleWithTraitCollection:nil];
}
//Sticker
- (void)fillWithStickerMessage: (NSString*)message downloaded: (BOOL)downloaded {
    if ([STKStickersManager isStickerMessage: message]) {
        [_imgView stk_setStickerWithMessage: message placeholder: nil placeholderColor: nil progress: nil completion: nil];
    }
    UIImage* downloadImage = [UIImage imageNamed: @"STKDownloadIcon"];
    
    UIColor* imageColor = [UIColor colorWithRed: 255.0 / 255.0 green: 87.0 / 255.0 blue: 34.0 / 255.0 alpha: 1];
    //UIImage* btnImage = [downloadImage imageWithImageTintColor: imageColor];
    
    
    
    
}
@end
