//
//  Constants.m
//  gHealth Doctor
//
//  Created by Ramesh P on 16/11/16.
//  Copyright © 2016 provenlogic. All rights reserved.
//

#import "Constants.h"

/*
#pragma mark - Google APIs
#pragma mark - --
NSString *const FILE_GOOGLE_MAP_API=@"AIzaSyCEpQKNmupriYPGMlZyvZdegfLBVm6QUoU";//Google Maps
NSString *const FILE_GOOGLE_MAP_DISTANCE_MATRIX_API=@"AIzaSyAO8u1pIQMQ5QRC7t-NRxhVwYKxA6B3spg";//Google Maps Distance Matrix API
#pragma mark - OpenTok APIs
#pragma mark - --
NSString *const FILE_OPENTOK_API=@"45736242";// OpenTok API
 
 Avenir: (
 "Avenir-Medium",
 "Avenir-HeavyOblique",
 "Avenir-Book",
 "Avenir-Light",
 "Avenir-Roman",
 "Avenir-BookOblique",
 "Avenir-MediumOblique",
 "Avenir-Black",
 "Avenir-BlackOblique",
 "Avenir-Heavy",
 "Avenir-LightOblique",
 "Avenir-Oblique"
 ) 
*/

#pragma mark - CustomAlbum
#pragma mark - --
NSString * const CSAlbum = @"ZoeChat";
NSString * const CSAssetIdentifier = @"assetIdentifier";
NSString * const CSAlbumIdentifier = @"albumIdentifier";

#pragma mark - AWS Credentials
#pragma mark - --
NSString *const FILE_AWS_POOL_ID=@"ap-northeast-2:11b9ab57-891c-439e-9435-e09f37496aaa";//AWS Pool Id
NSString *const FILE_AWS_IMAGE_BASE_URL=@"https://s3.ap-northeast-2.amazonaws.com/zoechats/";//AWS IMAGE BASE URL
NSString *const FILE_BUCKET_NAME=@"zoechats";//AWS BUCKET NAME

#pragma mark - Methods
#pragma mark - --
NSString *const FILE_GET_OTP=@"user/getOTP";//get otp  
NSString *const FILE_REGISTER=@"user/register";//register
NSString *const FILE_CONTACTS_SYNC=@"user/syncContacts";//contacts sync
NSString *const FILE_UPDATE_PROFILE=@"user/update";//Update Profile
NSString *const FILE_UPDATE_GROUP_NAME=@"group/updateName";//Update Group Name
NSString *const FILE_MAKE_CALL=@"user/voiceCall";//Make Call
NSString *const FILE_ACCEPT_CALL=@"user/acceptCall";//Accept Call
NSString *const FILE_END_CALL=@"user/endCall";//End Call
NSString *const FILE_CREATE_GROUP=@"group/createGroup";//Accept Call
NSString *const FILE_ADD_PARTICIPANTS=@"group/addParticipants";//Add Participants
NSString *const FILE_GET_PARTICIPANTS=@"group/getGroupParticipants";//Get Participants
NSString *const FILE_PRIVACY=@"user/updatePrivacySettings";//Privacy Settings
NSString *const FILE_RECOVERY=@"user/updateRecoveryDetails";//Recovery
NSString *const FILE_REQUESTOTP=@"user/requestOtp";//OTP
NSString *const FILE_CHECKRECOVERY=@"user/checkRecoveryDetails";//Recovery
NSString *const FILE_BACKUPUPDATE=@"user/driveUpdate";//Back UP
NSString *const FILE_REQUESTLIST =@"user/groupRequest";
NSString *const UPDATEDEVICETOKEN = @"user/updateDeviceToken";


#pragma mark - Font
#pragma mark - --
NSString *const FONT_NORMAL=@"Avenir";//Avenir
NSString *const FONT_HEAVY=@"Avenir-Heavy";//Avenir Heavy
NSString *const FONT_MEDIUM=@"Avenir-Medium";// Avenir Medium


#pragma mark - Parameters
#pragma mark - --

NSString *const PICTURE=@"picture"; //


#pragma mark - User Table Parameters
#pragma mark - --

NSString *const MOBILE=@"mobile";
NSString *const NAME=@"name";
NSString *const REGISTEREDNAME=@"registeredName";
NSString *const IMAGE=@"image";
NSString *const STATUS=@"status";
NSString *const SHOWINCONTACTSPAGE=@"showInContactsPage";
NSString *const ZOECHATID=@"zoeChatId";

#pragma mark - Chat Table Parameters
#pragma mark - --

NSString *const CHATID=@"id";
NSString *const CHATROOMID=@"chatRoomId";
NSString *const CHATROOMTYPE=@"chatRoomType";
NSString *const CHATSENDER=@"sender";
NSString *const LASTMESSAGE=@"lastMessage";
NSString *const LASTMESSAGESTATUS=@"lastMessageStatus";
NSString *const LASTMESSAGETIME=@"lastMessageTime";
NSString *const UNREADCOUNT=@"unReadCount";

#pragma mark - Chat Messages Table Parameters
#pragma mark - --

NSString *const CHATMESSAGEID=@"id";
NSString *const USERID=@"userId";
NSString *const CONTENT=@"content";
NSString *const CONTENTYPE=@"contentType";
NSString *const CONTENTSTATUS=@"contentStatus";
NSString *const MESSAGESENDER=@"sender";
NSString *const SENTTIME=@"sentTime";
NSString *const DELIVEREDTIME=@"deliveredTime";
NSString *const SEENTIME=@"seenTime";
NSString *const CAPTION=@"caption";
NSString *const ISDOWNLOADED=@"isDownloaded";


