//
//  CallVC.m
//  ZoeChat
//
//  Created by Pyramidions Solution on 20/04/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "CallVC.h"
#import "Constants.h"
#import "DBClass.h"
#import "CallCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface CallVC ()
{
    NSMutableArray *arrCalls, *arrForTableView;
     DBClass *db_class;
    NSString *strChatRoomId;
}
@end

@implementation CallVC

- (void)viewDidLoad {
    [super viewDidLoad];
     db_class=[[DBClass alloc]init];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self setFontFamily:FONT_NORMAL forView:self.view andSubViews:YES];
       self.title = @"Calls";
   
    self.tabBarItem.image = [[UIImage imageNamed:@"Call50.png"]
                             imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    

    
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        [[UIApplication sharedApplication] keyWindow].tintColor = color;
        self.navigationController.navigationBar.translucent = NO;
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : color }
                                                 forState:UIControlStateSelected];
        [[UITabBar appearance] setSelectedImageTintColor:color];
        
        
        
    }
    else
    {
        [[UIApplication sharedApplication] keyWindow].tintColor = [UIColor whiteColor];
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0] }
                                                 forState:UIControlStateSelected];
        [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0]];
    }

    
    
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCalls) name:@"Refresh_Calls" object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    
    [self refreshCalls];
}

-(void)refreshCalls
{
    arrCalls=[db_class getCalls];
    
    if (arrCalls.count!=0)
    {
        for (int i=0; i<arrCalls.count; i++)
        {
            NSMutableDictionary *dictSing=[arrCalls objectAtIndex:i];
            NSDictionary *dictInfo =[db_class getUserInfo:[dictSing valueForKey:@"zoeChatId"]];
            NSString *strName=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"name"]];
            NSString *strImage=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"image"]];
            [dictSing setObject:strName forKey:@"name"];
            [dictSing setObject:strImage forKey:@"image"];
            NSLog(@"%@",dictSing);
            
            [arrCalls replaceObjectAtIndex:i withObject:dictSing];
        }
    }
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"callTime" ascending:NO];
    arrCalls=[arrCalls sortedArrayUsingDescriptors:@[sort]];
    
    arrForTableView=[NSMutableArray arrayWithArray:arrCalls];
    
    [_tableCalls reloadData];
 
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(long long)convertDateToMilliseconds:(NSDate *)date
{
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    long long milliseconds=timeInSeconds*1000;
    return milliseconds;
}
-(NSDate *)convertMillisecondsToDate:(long long)milliSeconds
{
    NSDate *dat = [NSDate dateWithTimeIntervalSince1970:(milliSeconds / 1000.0)];
    return dat;
}

#pragma mark -
#pragma mark - TableView Delagate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  60;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return arrForTableView.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    CallCell *cell=(CallCell *)[tableView dequeueReusableCellWithIdentifier:@"CallCell"];
    if (cell==nil) {
        cell=[[CallCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CallCell"];
    }
    NSString *InitialStr;
    UIColor *bgColor;

    [cell.LblImg setHidden:YES];

    NSDictionary *dictArt=[arrForTableView objectAtIndex:indexPath.row];
    if (![[dictArt valueForKey:@"name"] isEqualToString:@"(null)"])
    {
        cell.CallName.text=[[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"name"]]capitalizedString];
        InitialStr=[cell.CallName.text substringToIndex:1];
        if ([InitialStr isEqualToString:@"A"])
        {
            bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"B"])
        {
            bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"C"])
        {
            bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"D"])
        {
            bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"E"])
        {
            bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"F"])
        {
            bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"G"])
        {
            bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"H"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"I"])
        {
            bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"J"])
        {
            bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"K"])
        {
            bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"L"])
        {
            bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"M"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"N"])
        {
            bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"O"])
        {
            bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"P"])
        {
            bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"Q"])
        {
            bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"R"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"S"])
        {
            bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"T"])
        {
            bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"U"])
        {
            bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"V"])
        {
            bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"W"])
        {
            bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"X"])
        {
            bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"Y"])
        {
            bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"Z"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
        }
    }
    else
    {
        cell.CallName.text=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"zoeChatId"]];
        InitialStr=[cell.CallName.text substringToIndex:1];
        if ([InitialStr isEqualToString:@"0"])
        {
            bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"1"])
        {
            bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"2"])
        {
            bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"3"])
        {
            bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"4"])
        {
            bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"5"])
        {
            bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"6"])
        {
            bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"7"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"8"])
        {
            bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"9"])
        {
            bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
        }


    }
    NSString *strImageURL1=[dictArt valueForKey:@"image"];
    cell.CallImage.image=[UIImage imageNamed:@"userImage.png"];
    if (![strImageURL1 isEqualToString:@"(null)"])
    {
        if (![strImageURL1 isEqualToString:@""])
        {
            [cell.CallImage sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
        }
    }
    else
    {
        [cell.LblImg setHidden:NO];
        
        [cell.CallImage sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
        
        cell.LblImg.text=InitialStr;
        cell.LblImg.backgroundColor=bgColor;
    }

    
    
    
    cell.CallImage.layer.cornerRadius=cell.CallImage.frame.size.height/2;
    cell.CallImage.clipsToBounds=YES;
    cell.LblImg.layer.cornerRadius=cell.CallImage.frame.size.height/2;
    cell.LblImg.clipsToBounds=YES;
    
    strChatRoomId=[dictArt valueForKey:@"zoeChatId"];
    NSDate *dat=[self convertMillisecondsToDate:[[dictArt valueForKey:@"callTime"]longLongValue]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *strTime=[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:dat]];
    
    cell.CallTime.text=strTime;
    
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
  //  NSString *strZoeChatId=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"zoeChatId"]];
   
    if ([[dictArt valueForKey:@"callType"]isEqualToString:@"voice"])
    {
        cell.ImgCallType.image=[UIImage imageNamed:@"VoiceCall.png"];
    }
    else
    {
        cell.ImgCallType.image=[UIImage imageNamed:@"VideoCall.png"];
    }
    
    if ([[dictArt valueForKey:@"callLog"]isEqualToString:@"0"])
    {
        cell.ImgStatusIcon.image=[UIImage imageNamed:@"RedArrow.png"];
    }
    else
    {
        cell.ImgStatusIcon.image=[UIImage imageNamed:@"GreenArrowUp.png"];
    }
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        
        cell.ImgCallType.image = [cell.ImgCallType.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

        cell.ImgCallType.tintColor=color;
        
        
    }
    else
    {
        
        
        
        
    }
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [ViewSearch removeFromSuperview];
//    dictToSendSingleChat=[arrChats objectAtIndex:indexPath.row];
//    [self performSegueWithIdentifier:@"Chat_SingleChat" sender:self];
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return NO;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {

        CallCell *cell = (CallCell *)[tableView cellForRowAtIndexPath:indexPath];
       NSLog(@"%@",strChatRoomId);
        NSMutableArray *arrAllChats=[db_class getcall:strChatRoomId];
        NSLog(@"%@",arrAllChats);
        NSLog(@"%ld",(long)indexPath.row);
        NSString* myNewID = [NSString stringWithFormat:@"%i", indexPath.row];
//
//        [arrAllChats removeObjectAtIndex:indexPath.row];
//        [tableView reloadData];
        BOOL Success=[db_class deleteCall:myNewID];
       
        [self refreshCalls];
        
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark - Font

-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)view;
        [btn setFont:[UIFont fontWithName:fontFamily size:[[btn font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *txtFld = (UITextField *)view;
        [txtFld setFont:[UIFont fontWithName:fontFamily size:[[txtFld font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextView class]])
    {
        UITextView *txtVw = (UITextView *)view;
        [txtVw setFont:[UIFont fontWithName:fontFamily size:[[txtVw font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
    
}
//


@end
