//
//  ShowAllContactsVC.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 28/04/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowAllContactsVC : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property(strong, nonatomic)NSString *strZoeChatId;
@property(strong, nonatomic)NSString *strChatRoomType;

@end
