//
//  GettingProfileDetailsVC.m
//  ZoeChat
//
//  Created by Pyramidions Solution on 29/12/16.
//  Copyright © 2016 Pyramidions Solution. All rights reserved.
//

#import "GettingProfileDetailsVC.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "AFNHelper.h"
#import "TOCropViewController.h"
#import "MBProgressHUD.h"
#import "AWSS3TransferUtility.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "CustomAlbum.h"
#import "DBClass.h"
#import "MainTabBarVC.h"

@import FirebaseInstanceID;

@interface GettingProfileDetailsVC ()<TOCropViewControllerDelegate>
{
    UITextField *currentTextView;
    UITextView *currentTextView1;
    bool keyboardIsShown;
    UITapGestureRecognizer *tapGesture;
    AppDelegate *appDelegate;
    NSString *isPictureAdded;
    UIPopoverController *popoverController;
    NSURL *assetURL;
    NSString* UploadFileName;
    DBClass *db_class;

}
@property (nonatomic, strong) UIImage *image;           // The image we'll be cropping
@property (nonatomic, strong) UIImageView *imageView;   // The image view to present the cropped image

@property (nonatomic, assign) TOCropViewCroppingStyle croppingStyle; //The cropping style
@property (nonatomic, assign) CGRect croppedFrame;
@property (nonatomic, assign) NSInteger angle;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
@property (nonatomic, strong) UIPopoverController *activityPopoverController;
#pragma clang diagnostic pop

- (void)showCropViewController;
- (void)sharePhoto;

- (void)layoutImageView;
- (void)didTapImageView;

- (void)updateImageViewWithImage:(UIImage *)image fromCropViewController:(TOCropViewController *)cropViewController;



@end

@implementation GettingProfileDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     db_class=[[DBClass alloc]init];
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
   // [self.navigationController.navigationBar setTranslucent:NO];
     [self setFontFamily:FONT_NORMAL forView:self.view andSubViews:YES];
    
    isPictureAdded=@"0";
    UploadFileName=@"";
    _txtName.delegate=self;
   // _txtName.text=@"Gopal";
    _btnDone.clipsToBounds=YES;
    _btnDone.layer.cornerRadius=4;
    [_btnDone setFont:[UIFont fontWithName:FONT_MEDIUM size:[[_btnDone font] pointSize]]];

    self.title=@"Edit Profile";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:FONT_HEAVY size:15]}];
    _btnProfile.clipsToBounds=YES;
    _btnProfile.layer.cornerRadius=_btnProfile.frame.size.height/2;
    _btnProfile.titleLabel.textAlignment=NSTextAlignmentCenter;
    _btnProfile.layer.borderWidth=1.0;
    _btnProfile.layer.borderColor=[UIColor blackColor].CGColor;
    CALayer *btnUpperBorder = [CALayer layer];
    btnUpperBorder.frame = CGRectMake(0.0f
                                      , 0.0f, _txtName.frame.size.width, 1.5f);
    btnUpperBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                       alpha:1.0f].CGColor;
    [_txtName.layer addSublayer:btnUpperBorder];
    
    CALayer *btnbottomBorder = [CALayer layer];
    btnbottomBorder.frame = CGRectMake(0.0f
                                       , _txtName.frame.size.height-1.5, _txtName.frame.size.width, 1.5f);
    btnbottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                        alpha:1.0f].CGColor;
    [_txtName.layer addSublayer:btnbottomBorder];

    
    //ScrollView moving
    
  //  _scrollView.contentSize=CGSizeMake(self.view.frame.size.width, self.btnRegister.frame.origin.y+70);
    
    
     CGRect contentRect = CGRectZero;
     for (UIView *view in self.scrollView.subviews) {
     contentRect = CGRectUnion(contentRect, view.frame);
     }
     self.scrollView.contentSize = contentRect.size;
    

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyBoard)];
    
    [self.view addGestureRecognizer:tap];
    
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onProfile:(id)sender
{
    NSLog(@"%@",appDelegate.strInternetMode);
    if ([appDelegate.strInternetMode isEqualToString:@"online"])
    {
        UIButton *btnProf=(UIButton * )sender;
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:appDelegate.strAlertTitle
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction* Camera = [UIAlertAction
                                 actionWithTitle:@"Take Picture"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                                     
                                     //Use camera if device has one otherwise use photo library
                                     if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                                     {
                                         [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
                                     }
                                     else
                                     {
                                         [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                                     }
                                     
                                     [imagePicker setDelegate:self];
                                     
                                     //Show image picker
                                     [self presentViewController:imagePicker animated:YES completion:nil];
                                 }];
        
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Choose From Camera Roll"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
                                 imagePickerController.delegate = self;
                                 [imagePickerController setAllowsEditing:NO];
                                 imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
                                 [self presentViewController:imagePickerController animated:YES completion:nil];
                                 
                             }];
        
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleCancel
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:Camera];
        [alert addAction:ok];
        [alert addAction:cancel];
        
        
        UIPopoverPresentationController *popPresenter1 = [alert
                                                          popoverPresentationController];
        popPresenter1.sourceView=btnProf;
        popPresenter1.sourceRect=btnProf.bounds;
        [alert setModalPresentationStyle:UIModalPresentationPopover];
        
        [self presentViewController:alert animated:YES completion:nil];
        NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
        UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
        if (color != nil) {
            alert.view.tintColor=color;
        }
        else
        {
            
            alert.view.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
            
        }

    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Please check your internet connection"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.label.adjustsFontSizeToFitWidth=YES;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:2];

    }
    
    
    
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SegueHome"]) {
        MainTabBarVC *lvc = segue.destinationViewController;
        [lvc setHidesBottomBarWhenPushed:YES];
        lvc.navigationItem.hidesBackButton = YES;
    }
}
- (IBAction)onDone:(id)sender
{
    if ([appDelegate.strInternetMode isEqualToString:@"online"])
    {
        if ([self.txtName.text isEqualToString:@""])
        {
             [self.txtName resignFirstResponder];
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            // Configure for text only and offset down
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [NSString stringWithFormat:@"Please enter your name"];
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hideAnimated:YES afterDelay:2];
           
            return;
        }
        NSString *strImageURL;
        NSString *FCMToken = [[FIRInstanceID instanceID] token];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        if ([isPictureAdded isEqualToString:@"1"])
        {
            strImageURL=[NSString stringWithFormat:@"%@%@",FILE_AWS_IMAGE_BASE_URL,UploadFileName];
            [dictParam setValue:appDelegate.strCountryCode forKey:@"countryCode"];
            [dictParam setValue:appDelegate.strMobileNumber forKey:@"mobileNumber"];
            [dictParam setValue:_txtName.text forKey:@"name"];
            [dictParam setValue:strImageURL forKey:@"image"];
            [dictParam setValue:FCMToken forKey:@"pushNotificationId"];
            [dictParam setValue:appDelegate.strTokenVOIP forKey:@"voipToken"];
            [dictParam setValue:@"ios" forKey:@"os"];
            [dictParam setValue:@"5" forKey:@"buildVersion"];
        }
        else
        {
            strImageURL=@"";
            [dictParam setValue:appDelegate.strCountryCode forKey:@"countryCode"];
            [dictParam setValue:appDelegate.strMobileNumber forKey:@"mobileNumber"];
            [dictParam setValue:_txtName.text forKey:@"name"];
            [dictParam setValue:FCMToken forKey:@"pushNotificationId"];
            [dictParam setValue:appDelegate.strTokenVOIP forKey:@"voipToken"];
            [dictParam setValue:@"ios" forKey:@"os"];
            [dictParam setValue:@"5" forKey:@"buildVersion"];
        }
        
        
        
        
        
        MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDAnimationFade;
        hud.label.text = @"Loading";
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_REGISTER withParamData:dictParam withBlock:^(id response, NSError *error)
         {
              NSLog(@"Response: %@",dictParam);
             NSLog(@"Response: %@",response);
             [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
             if (response)
             {
                 NSLog(@"Response: %@",response);
                 if ([[response valueForKey:@"error"] boolValue]==false)
                 {
                     
                     appDelegate.strCountryCode=[NSString stringWithFormat:@"%@",[response valueForKey:@"countryCode"]];
                     appDelegate.strMobileNumber=[NSString stringWithFormat:@"%@",[response valueForKey:@"mobileNumber"]];
                     appDelegate.strName=[NSString stringWithFormat:@"%@",[response valueForKey:@"name"]];
                     appDelegate.strID=[NSString stringWithFormat:@"%@",[response valueForKey:@"_id"]];
                     appDelegate.strProfilePic=[NSString stringWithFormat:@"%@",[response valueForKey:@"image"]];
                     appDelegate.strStatus=@"Hi there! Im using Acedais.";
                     appDelegate.strLoginStatus=@"LoggedIn";
                     
                    

                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setValue:appDelegate.strCountryCode forKey:@"countryCode"];
                     [defaults setValue:appDelegate.strMobileNumber forKey:@"mobileNumber"];
                     [defaults setValue:appDelegate.strName forKey:@"name"];
                     [defaults setValue:appDelegate.strID forKey:@"_id"];
                     [defaults setValue:appDelegate.strProfilePic forKey:@"image"];
                     [defaults setValue:@"Hi there! Im using Acedais" forKey:@"status"];
                     
                     [defaults setValue:appDelegate.strLoginStatus forKey:@"LoginStatus"];
                     
                     NSURL* url = [[NSURL alloc] initWithString:SOCKET_URL];
                     
//                     appDelegate.socket = [[SocketIOClient alloc] initWithSocketURL:url config:@{@"log": @YES, @"forcePolling": @YES,@"reconnects":@YES,@"reconnectAttempts":@-1,@"reconnectWait":@5}];
//                     BOOL isdefualt = [response boo boolForKey:@"in_default_group"];
                     
//                     nsbb *isdef = [response objectForKey:@"in_default_group"];

                     
                     Boolean ch = (Boolean)[response objectForKey:@"in_default_group"];
                     
                     if(ch)
                     {
                         
                     }
                     else
                     {
                         
                     }
                     
                     NSLog(@"Cuntroycode:%@",appDelegate.strCountryCode);
                      NSLog(@"mobileNumber:%@",appDelegate.strMobileNumber);
//                     NSLog(@"jgjbjbbkabkbkisdefault:",isdefualt);
                     
//                     else{
                     
                     appDelegate.socket = [[SocketIOClient alloc] initWithSocketURL:url config:@{@"log": @NO, @"forcePolling": @YES}];
                     
                     
                     [appDelegate.socket connect];
                     
                     [appDelegate.socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
                         [appDelegate.socket emit:@"online" with:@[@{@"id":appDelegate.strID}]];


                         [[NSNotificationCenter defaultCenter] postNotificationName:@"Ask_Refresh_Contacts" object:self];
                         
                         NSString *strReceiveMessage=[NSString stringWithFormat:@"%@:receiveMessage",appDelegate.strID];
                         if ([response objectForKey:@"in_default_group"])
                         {
                             
                             [[appDelegate.socket emitWithAck:@"addMe" with:@[@{@"countryCode": appDelegate.strCountryCode,@"mobileNumber":appDelegate.strMobileNumber }]] timingOutAfter:1 callback:^(NSArray* data)
                              {
                                  NSLog(@"%@",data);
                                  
                              }];
                             
                         }
                         [appDelegate.socket on:strReceiveMessage callback:^(NSArray *dataReceiveMsg, SocketAckEmitter *ack)
                          {
                              BOOL success=NO;
                              NSLog(@"%@",dataReceiveMsg);
                              NSLog(@"%@",ack);
                              
                              
                              for (int j=0; j<dataReceiveMsg.count; j++)
                              {
                                  NSDictionary *dictReceiveMsg=[dataReceiveMsg objectAtIndex:j];
                                  NSString *strChatRoomType=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"chatRoomType"]];
                                  NSString *strMsg=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"content"]];
                                  NSString *strMsgFrom=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"from"]];
                                  NSString *strMsgType=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"contentType"]];
                                  NSString *strMsgTime=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"time"]];
                                  NSString *strMsgId=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"messageId"]];
                                  NSString *strLatitude=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"latitude"]];
                                  NSString *strLogitude=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"longitude"]];
                                  NSString *strGroupId=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"groupId"]];
                                  NSString *strMsgFromUserId=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"from"]];
                                  NSString *strContactName=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"contactName"]];
                                  NSString *strContactNo=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"contactNumber"]];
                                  NSString *strToMsg=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"to"]];
                                  NSString *strShowPreview=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"showPreview"]];
                                  NSString *strPreviewTitle=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"metaTitle"]];
                                  NSString *strPreviewDescription=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"metaDescription"]];
                                  NSString *strPreviewLogo=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"metaLogo"]];
                                  
                                  if ([strLatitude isEqualToString:@"(null)"])
                                  {
                                      strLatitude=@"";
                                  }
                                  if ([strLogitude isEqualToString:@"(null)"])
                                  {
                                      strLogitude=@"";
                                  }
                                  
                                  if ([strChatRoomType isEqualToString:@"1"])
                                  {
                                      BOOL success2 = [db_class updatedeleteChatTable:strGroupId isDelete:@"0"];
                                  }
                                  else if([strChatRoomType isEqualToString:@"0"])
                                      
                                  {
                                      strMsgFrom=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"from"]];
                                      BOOL success2 = [db_class updatedeleteChatTable:strMsgFrom isDelete:@"0"];
                                  }
                                  
                                  BOOL isAvailinChatsTable=NO;
                                  if ([appDelegate.strID isEqualToString:strMsgFrom])
                                  {
                                      isAvailinChatsTable=[db_class doesChatExist:strToMsg];
                                      
                                      if (isAvailinChatsTable==NO)
                                      {
                                          success=[db_class insertChats:@"" chatRoomId:strToMsg chatRoomType:strChatRoomType sender:@"1" lastMessage:strMsg lastMessageStatus:@"delivered" lastMessagesTime:strMsgTime lastMessagesType:strMsgType unReadCount:@"1" groupName:@"" groupImage:@"" sentBy:strMsgFrom isDelete:@"0" isLock:@"0" password:@""];
                                          if (success==YES)
                                          {
                                              isAvailinChatsTable=YES;
                                          }
                                      }
                                      else
                                      {
                                          if ([strChatRoomType isEqualToString:@"1"])
                                          {
                                              NSDictionary *dictSing=[db_class getSingleChat:strGroupId];
                                              NSString *strgetCount=[NSString stringWithFormat:@"%@",[dictSing valueForKey:@"unreadCount"]];
                                              NSInteger nUnreadCount=[strgetCount integerValue];
                                              nUnreadCount=nUnreadCount+1;
                                              NSString *strUnreadCount=[NSString stringWithFormat:@"%ld",(long)nUnreadCount];
                                              
                                              success=[db_class updateLastMessage:strGroupId sender:@"1" lastMessage:strMsg lastMessageStatus:@"delivered" lastMessageTime:strMsgTime lastMessagesType:strMsgType unReadCount:strUnreadCount];
                                              //   NSLog(@"success");
                                          }
                                          else if ([strChatRoomType isEqualToString:@"0"])
                                          {
                                              
                                              if ([strMsgFrom isEqualToString:appDelegate.strID])
                                              {
                                                  
                                                  NSDictionary *dictSing=[db_class getSingleChat:strToMsg];
                                                  NSString *strgetCount=[NSString stringWithFormat:@"%@",[dictSing valueForKey:@"unreadCount"]];
                                                  NSInteger nUnreadCount=[strgetCount integerValue];
                                                  nUnreadCount=nUnreadCount+1;
                                                  NSString *strUnreadCount=[NSString stringWithFormat:@"%ld",(long)nUnreadCount];
                                                  success=[db_class updateLastMessage:strToMsg sender:@"0" lastMessage:strMsg lastMessageStatus:@"delivered" lastMessageTime:strMsgTime lastMessagesType:strMsgType unReadCount:strUnreadCount];
                                              }
                                              else
                                              {
                                                  NSDictionary *dictSing=[db_class getSingleChat:strMsgFrom];
                                                  NSString *strgetCount=[NSString stringWithFormat:@"%@",[dictSing valueForKey:@"unreadCount"]];
                                                  NSInteger nUnreadCount=[strgetCount integerValue];
                                                  nUnreadCount=nUnreadCount+1;
                                                  NSString *strUnreadCount=[NSString stringWithFormat:@"%ld",(long)nUnreadCount];
                                                  success=[db_class updateLastMessage:strMsgFrom sender:@"1" lastMessage:strMsg lastMessageStatus:@"delivered" lastMessageTime:strMsgTime lastMessagesType:strMsgType unReadCount:strUnreadCount];
                                              }
                                          }
                                      }
                                  }
                                  else
                                  {
                                      isAvailinChatsTable=[db_class doesChatExist:strMsgFrom];
                                      
                                      if (isAvailinChatsTable==NO)
                                      {
                                          success=[db_class insertChats:@"" chatRoomId:strMsgFrom chatRoomType:strChatRoomType sender:@"1" lastMessage:strMsg lastMessageStatus:@"delivered" lastMessagesTime:strMsgTime lastMessagesType:strMsgType unReadCount:@"1" groupName:@"" groupImage:@"" sentBy:strMsgFrom isDelete:@"0" isLock:@"0" password:@""];
                                          if (success==YES)
                                          {
                                              isAvailinChatsTable=YES;
                                          }
                                      }
                                      else
                                      {
                                          if ([strChatRoomType isEqualToString:@"1"])
                                          {
                                              NSDictionary *dictSing=[db_class getSingleChat:strGroupId];
                                              NSString *strgetCount=[NSString stringWithFormat:@"%@",[dictSing valueForKey:@"unreadCount"]];
                                              NSInteger nUnreadCount=[strgetCount integerValue];
                                              nUnreadCount=nUnreadCount+1;
                                              NSString *strUnreadCount=[NSString stringWithFormat:@"%ld",(long)nUnreadCount];
                                              
                                              success=[db_class updateLastMessage:strGroupId sender:@"1" lastMessage:strMsg lastMessageStatus:@"delivered" lastMessageTime:strMsgTime lastMessagesType:strMsgType unReadCount:strUnreadCount];
                                              //   NSLog(@"success");
                                          }
                                          else if ([strChatRoomType isEqualToString:@"0"])
                                          {
                                              NSDictionary *dictSing=[db_class getSingleChat:strMsgFrom];
                                              NSString *strgetCount=[NSString stringWithFormat:@"%@",[dictSing valueForKey:@"unreadCount"]];
                                              NSInteger nUnreadCount=[strgetCount integerValue];
                                              nUnreadCount=nUnreadCount+1;
                                              NSString *strUnreadCount=[NSString stringWithFormat:@"%ld",(long)nUnreadCount];
                                              if ([strMsgFrom isEqualToString:appDelegate.strID])
                                              {
                                                  
                                                  
                                                  
                                                  success=[db_class updateLastMessage:strToMsg sender:@"0" lastMessage:strMsg lastMessageStatus:@"delivered" lastMessageTime:strMsgTime lastMessagesType:strMsgType unReadCount:strUnreadCount];
                                                  
                                              }
                                              else
                                              {
                                                  
                                                  success=[db_class updateLastMessage:strMsgFrom sender:@"1" lastMessage:strMsg lastMessageStatus:@"delivered" lastMessageTime:strMsgTime lastMessagesType:strMsgType unReadCount:strUnreadCount];
                                                  
                                              }
                                          }
                                      }
                                      
                                  }
                                  
                                  long long milliSecond=[self convertDateToMilliseconds:[NSDate date]];
                                  NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSecond];
                                  
                                  
                                  if ([strMsgType isEqualToString:@"location"])
                                  {
                                      NSDate *now=[NSDate date];
                                      NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                                      [dateFormatter1 setDateFormat:@"yyyyMMddHHmmssSSS"];
                                      NSString *localDateString = [dateFormatter1 stringFromDate:now];
                                      //  NSLog(@"%@",localDateString);
                                      
                                      
                                      NSString* strExt = @"PNG";
                                      
                                      NSString *UploadFileName=[NSString stringWithFormat:@"%@.%@",localDateString,strExt];
                                      
                                      
                                      
                                      NSURL *imageURL = [NSURL URLWithString:[strMsg stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                                      
                                      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                                          NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              // Update the UI
                                              //image you want to upload
                                              // UIImage* imageToUpload = [UIImage imageWithData:imageData];
                                              UIImage *imageToUpload=[self compressImage:[UIImage imageWithData:imageData]];
                                              //convert uiimage to
                                              NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                              NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",UploadFileName]];
                                              [UIImagePNGRepresentation(imageToUpload) writeToFile:filePath atomically:YES];
                                              
                                              
                                              BOOL success1=[db_class insertChatMessages:strMsgId userId:strMsgFromUserId groupId:strGroupId chatRoomType:strChatRoomType content:UploadFileName contentType:strMsgType contentStatus:@"delivered" sender:@"1" sentTime:strMsgTime deliveredTime:strMilliSeconds seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:strLatitude longitude:strLogitude contactName:strContactName contactNumber:strContactNo checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                                              
                                              NSDictionary *theInfo =[NSDictionary dictionaryWithObjectsAndKeys:dataReceiveMsg,@"msgsArray", nil];
                                              [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_SingleChat_ReceivedMsgs"
                                                                                                  object:self
                                                                                                userInfo:theInfo];
                                              
                                          });
                                      });
                                      
                                      
                                  }
                                  else
                                  {
                                      
                                      
                                      NSLog(@"%@",appDelegate.strID);
                                      if ([appDelegate.strID isEqualToString:strMsgFrom])
                                      {
                                          NSLog(@"%@",strMsgFrom);
                                          success=[db_class insertChatMessages:strMsgId userId:strToMsg groupId:strGroupId chatRoomType:strChatRoomType content:strMsg contentType:strMsgType contentStatus:@"delivered" sender:@"0" sentTime:strMsgTime deliveredTime:strMilliSeconds seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:strLatitude longitude:strLogitude contactName:strContactName contactNumber:strContactNo checkStar:@"0" showPreview:strShowPreview linkTitle:strPreviewTitle linkLogo:strPreviewLogo linkDescription:strPreviewDescription];
                                          
                                      }
                                      else
                                      {
                                          success=[db_class insertChatMessages:strMsgId userId:strMsgFromUserId groupId:strGroupId chatRoomType:strChatRoomType content:strMsg contentType:strMsgType contentStatus:@"delivered" sender:@"1" sentTime:strMsgTime deliveredTime:strMilliSeconds seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:strLatitude longitude:strLogitude contactName:strContactName contactNumber:strContactNo checkStar:@"0" showPreview:strShowPreview linkTitle:strPreviewTitle linkLogo:strPreviewLogo linkDescription:strPreviewDescription];
                                      }
                                  }
                                  
                                  
                                  NSLog(@"%@",appDelegate.strID);
                                  if ([appDelegate.strID isEqualToString:strMsgFrom])
                                  {
                                      NSLog(@"%@",strMsgFrom);
                                      
                                  }
                                  
                                  [appDelegate.socket emit:@"sendDelivered" with:@[@{@"messageId":strMsgId,@"from":strMsgFrom,@"to": appDelegate.strID,@"time":strMilliSeconds,@"chatRoomType":strChatRoomType,@"userid":appDelegate.strID}]];
                                  
                                  [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
                                  [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_App_Badge_Count" object:self];
                                  
                                  if (![strMsgType isEqualToString:@"location"])
                                  {
                                      NSDictionary *theInfo =[NSDictionary dictionaryWithObjectsAndKeys:dataReceiveMsg,@"msgsArray", nil];
                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_SingleChat_ReceivedMsgs"
                                                                                          object:self
                                                                                        userInfo:theInfo];
                                      
                                  }
                                  
                              }
                              
                              
                              
                          }];
                         
                         NSString *strACK=[NSString stringWithFormat:@"%@:ack",appDelegate.strID];
                         [appDelegate.socket on:strACK callback:^(NSArray *dataACK, SocketAckEmitter *ack)
                          {
                              for (int j=0; j<dataACK.count; j++)
                              {
                                  NSDictionary *dictACK=[dataACK objectAtIndex:j];
                                  NSString *strMsgId=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"messageid"]];
                                  NSString *strToWhomMsgd=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"to"]];
                                  NSString *strMsgStatus=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"status"]];
                                  NSString *strMsgTime=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"time"]];
                                  NSString *strChatRoomTypeACK=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"chatRoomType"]];
                                  appDelegate.uniqueID=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"uniqueid"]];

                                  if ([strChatRoomTypeACK isEqualToString:@"1"])
                                  {
                                      strToWhomMsgd=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"to"]];
                                  }
                                  BOOL success=NO;
                                  BOOL success1=NO;
                                  success=[db_class updateContentStatus:strMsgId contentStatus:strMsgStatus];
                                  
                                  if ([strMsgStatus isEqualToString:@"delivered"])
                                  {
                                      success=[db_class updateDeliveredTime:strMsgId deliveredTime:strMsgTime];
                                  }
                                  else if ([strMsgStatus isEqualToString:@"read"])
                                  {
                                      success=[db_class updateSeenTime:strMsgId seenTime:strMsgTime];
                                  }
                                  
                                  NSDictionary *dict=[db_class getSingleChat:strToWhomMsgd];
                                  
                                  //  if ([[dict valueForKey:@"lastMessageTime"]isEqualToString:strMsgTime])
                                  // {
                                  success1=[db_class updateLastMsgStatus:strToWhomMsgd lastMsgStatus:strMsgStatus];
                                  // }
                                  
                              }
                              [appDelegate.socket emit:@"ack" with:@[@{@"userid":appDelegate.strID,@"uniqueid":appDelegate.uniqueID}]];
                              [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
                              NSDictionary *theInfo =[NSDictionary dictionaryWithObjectsAndKeys:dataACK,@"ackArray", nil];
                              [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_SingleChat_acks"
                                                                                  object:self
                                                                                userInfo:theInfo];
                              
                              
                          }];
                         
                         NSString *strGroup=[NSString stringWithFormat:@"%@:group",appDelegate.strID];
                         [appDelegate.socket on:strGroup callback:^(NSArray *dataACK, SocketAckEmitter *ack)
                          {
                              NSLog(@"%@",dataACK);
                              for (int j=0; j<dataACK.count; j++)
                              {
                                  NSDictionary *userInfo=[dataACK objectAtIndex:j];
                                  NSString *strGroupId=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"groupId"]];
                                  NSString *strTime=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"time"]];
                                  NSString *strFrom=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"createdBy"]];
                                  NSString *strGroupName=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"groupName"]];
                                  NSString *strGroupImage=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"groupImage"]];
                                  NSString *strContentType=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"contentType"]];
                                  NSString *strFromMsg=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"from"]];
                                  NSString *StrMsgg=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"content"]];
                                  NSString *PartcipantIDstr=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"participantId"]];
                                   NSString *StrOldName=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"oldName"]];
                                  appDelegate.uniqueID=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"uniqueid"]];
                                  if ([strContentType isEqualToString:@"GroupAdd"])
                                  {
                                      NSUUID *uuid=[NSUUID UUID];
                                      NSString *strMgsId=[NSString stringWithFormat:@"%@",uuid];
                                      
                                      NSString *strMess=[NSString stringWithFormat:@"added you"];
                                      NSDictionary *dictName=[db_class getUserNameForNotification:[userInfo valueForKey:@"createdBy"]];
                                      NSString *strNameToShow;
                                      
                                      if ([[dictName allKeys] containsObject:@"name"])
                                      {
                                          strNameToShow=[NSString stringWithFormat:@"%@",[dictName valueForKey:@"name"]];
                                      }
                                      else
                                      {
                                          strNameToShow=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"createdBy"]];
                                      }
                                      
                                      NSString *strContent=[NSString stringWithFormat:@"%@ %@",strNameToShow,strMess];
                                      
                                      
                                      if (![strFrom isEqualToString:appDelegate.strID])
                                      {
                                          BOOL success=[db_class insertChats:@"" chatRoomId:strGroupId chatRoomType:@"1" sender:@"" lastMessage:strContent lastMessageStatus:@"delivered" lastMessagesTime:strTime lastMessagesType:@"header" unReadCount:@"1" groupName:strGroupName groupImage:strGroupImage sentBy:strFrom isDelete:@"0" isLock:@"0" password:@""];
                                          
                                          BOOL success1=[db_class insertChatMessages:strMgsId userId:strFrom groupId:strGroupId chatRoomType:@"1" content:strContent contentType:@"header" contentStatus:@"delivered" sender:@"" sentTime:strTime deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                                          
                                          [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
                                      }
                                      
                                  }
                                  else if ([strContentType isEqualToString:@"GroupNameEdit"])
                                  {
                                      NSUUID *uuid=[NSUUID UUID];
                                      NSString *strMgsId=[NSString stringWithFormat:@"%@",uuid];
                                      
                                      NSDictionary *dictName=[db_class getUserNameForNotification:[userInfo valueForKey:@"from"]];
                                      NSString *strNameToShow;
                                      if ([strFromMsg isEqualToString:appDelegate.strID])
                                      {
                                          strNameToShow = @"You";
                                      }
                                      else{
                                      if ([[dictName allKeys] containsObject:@"name"])
                                      {
                                          strNameToShow=[NSString stringWithFormat:@"%@",[dictName valueForKey:@"name"]];
                                      }
                                      else
                                      {
                                          strNameToShow=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"from"]];
                                      }
                                      }
                                      
                                      NSString *strContent=[NSString stringWithFormat:@" %@ changed the subject from %@ to %@",strNameToShow,StrOldName,strGroupName];
                                      if (![strFrom isEqualToString:appDelegate.strID])
                                      {
                                          
                                          
                                          BOOL success2=[db_class updateGroupName:strGroupId groupName:strGroupName];
                                          BOOL isAvailinChatsTable=NO;
                                          isAvailinChatsTable=[db_class doesChatExist:strGroupId];
                                          
                                          BOOL success=NO;
                                          if (isAvailinChatsTable==NO)
                                          {
                                              success=[db_class insertChats:@"" chatRoomId:strGroupId chatRoomType:@"1" sender:@"" lastMessage:strContent lastMessageStatus:@"sending" lastMessagesTime:strTime lastMessagesType:@"header" unReadCount:@"0" groupName:strGroupName  groupImage:strGroupImage sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
                                              if (success==YES)
                                              {
                                                  isAvailinChatsTable=YES;
                                              }
                                          }
                                          else
                                          {
                                              BOOL success1 = [db_class updateLastMessage: strGroupId sender:@"" lastMessage:strContent lastMessageStatus:@"delivered" lastMessageTime:strTime lastMessagesType:@"header" unReadCount:@"1"];
                                              NSLog(@"success");
                                          }
                                          
                                          //
                                          //
                                          BOOL succe=[db_class insertChatMessages:strMgsId userId:strFromMsg groupId:strGroupId chatRoomType:@"1" content:strContent contentType:@"header" contentStatus:@"delivered" sender:@"" sentTime:strTime deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                                          
                                          [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
                                      }
                                      
                                  }
                                  
                                  else if ([strContentType isEqualToString:@"GroupImageEdit"])
                                  {
                                      NSUUID *uuid=[NSUUID UUID];
                                      NSString *strMgsId=[NSString stringWithFormat:@"%@",uuid];
                                      
                                      NSDictionary *dictName=[db_class getUserNameForNotification:[userInfo valueForKey:@"from"]];
                                      NSString *strNameToShow;
                                      if ([strFromMsg isEqualToString:appDelegate.strID])
                                      {
                                          strNameToShow = @"You";
                                      }
                                      else{
                                      if ([[dictName allKeys] containsObject:@"name"])
                                      {
                                          strNameToShow=[NSString stringWithFormat:@"%@",[dictName valueForKey:@"name"]];
                                      }
                                      else
                                      {
                                          strNameToShow=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"from"]];
                                      }
                                      }
                                      NSString *strContent=[NSString stringWithFormat:@" %@ changed the group Icon",strNameToShow];
                                      
                                      if (![strFrom isEqualToString:appDelegate.strID])
                                      {
                                          
                                          BOOL succ = [db_class updateGroupImage:strGroupId  groupImage:strGroupImage];
                                          
                                          BOOL isAvailinChatsTable=NO;
                                          isAvailinChatsTable=[db_class doesChatExist:strGroupId];
                                          BOOL success=NO;
                                          if (isAvailinChatsTable==NO)
                                          {
                                              success=[db_class insertChats:@"" chatRoomId:strGroupId chatRoomType:@"1" sender:@"" lastMessage:strContent lastMessageStatus:@"sending" lastMessagesTime:strTime lastMessagesType:@"header" unReadCount:@"0" groupName:strGroupName  groupImage:strGroupImage sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
                                              if (success==YES)
                                              {
                                                  isAvailinChatsTable=YES;
                                              }
                                          }
                                          else
                                          {
                                              BOOL success1 = [db_class updateLastMessage: strGroupId sender:@"" lastMessage:strContent lastMessageStatus:@"delivered" lastMessageTime:strTime lastMessagesType:@"header" unReadCount:@"1"];
                                              NSLog(@"success");
                                          }
                                          
                                          
                                          
                                          
                                          BOOL success2=[db_class insertChatMessages:strMgsId userId:strFromMsg groupId:strGroupId chatRoomType:@"1" content:strContent contentType:@"header" contentStatus:@"delivered" sender:@"" sentTime:strTime deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                                          
                                          [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
                                      }
                                      
                                  }
                                  
                                  else if ([strContentType isEqualToString:@"GroupExit"])
                                  {
                                      NSUUID *uuid=[NSUUID UUID];
                                      NSString *strMgsId=[NSString stringWithFormat:@"%@",uuid];
                                      //NSString *strLeftNum=[NSString stringWithFormat:@"%@",strFromMsg];
                                      NSString *strMess=[NSString stringWithFormat:@"is left from Group"];
                                      
                                      NSString *strNameToShow;
                                      
                                      
                                      NSDictionary *dictName=[db_class getUserNameForNotification:[userInfo valueForKey:@"from"]];
                                      if ([[dictName allKeys] containsObject:@"name"])
                                      {
                                          strNameToShow=[NSString stringWithFormat:@"%@",[dictName valueForKey:@"name"]];
                                      }
                                      else
                                      {
                                          strNameToShow=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"from"]];
                                      }
                                      
                                      
                                      NSString *strcontent=[NSString stringWithFormat:@"%@ %@",strNameToShow,strMess];

                                      
                                      if (![strFrom isEqualToString:appDelegate.strID])
                                      {
                                          
                                          
                                          
                                          BOOL isAvailinChatsTable=NO;
                                          isAvailinChatsTable=[db_class doesChatExist:strGroupId];
                                          BOOL success=NO;
                                          if (isAvailinChatsTable==NO)
                                          {
                                              success=[db_class insertChats:@"" chatRoomId:strGroupId chatRoomType:@"1" sender:@"" lastMessage:strcontent lastMessageStatus:@"sending" lastMessagesTime:strTime lastMessagesType:@"header" unReadCount:@"0" groupName:strGroupName  groupImage:strGroupImage sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
                                              if (success==YES)
                                              {
                                                  isAvailinChatsTable=YES;
                                              }
                                          }
                                          else
                                          {
                                              BOOL success1 = [db_class updateLastMessage: strGroupId sender:@"" lastMessage:strcontent lastMessageStatus:@"delivered" lastMessageTime:strTime lastMessagesType:@"header" unReadCount:@"1"];
                                              NSLog(@"success");
                                          }
                                          
                                          
                                          
                                          
                                          BOOL success2=[db_class insertChatMessages:strMgsId userId:strFromMsg groupId:strGroupId chatRoomType:@"1" content:strcontent contentType:@"header" contentStatus:@"delivered" sender:@"" sentTime:strTime deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                                          
                                          [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
                                      }
                                      
                                  }

                                  
                                      else if ([strContentType isEqualToString:@"PartAdd"])
                                      {
                                          NSUUID *uuid=[NSUUID UUID];
                                          NSString *strMgsId=[NSString stringWithFormat:@"%@",uuid];
                                          
                                          
                                          //                     NSString *strNameToShow1;
                                          //                     NSString *strNameToShow2;
                                          NSString *strNameToShow;
                                          NSString *strContent;
                                          NSString *strMess;
                                          NSDictionary *dictName1;
                                          NSDictionary *dictName2;
                                          
                                          
                                          dictName1=[db_class getUserNameForNotification:[userInfo valueForKey:@"addedBy"]];
                                          dictName2=[db_class getUserNameForNotification:[userInfo valueForKey:@"participantId"]];
                                          if ([[dictName1 allKeys] containsObject:@"name"])
                                          {
                                              strNameToShow=[NSString stringWithFormat:@"%@",[dictName1 valueForKey:@"name"]];
                                              strMess=[NSString stringWithFormat:@"added %@",[dictName2 valueForKey:@"name"]];
                                              if ([strMess isEqualToString:@"added (null)"])
                                              {
                                                  strNameToShow=[NSString stringWithFormat:@"%@",[dictName1 valueForKey:@"name"]];
                                                  strMess=[NSString stringWithFormat:@"added %@",PartcipantIDstr];
                                              }
                                              strContent=[NSString stringWithFormat:@"%@ %@",strNameToShow,strMess];
                                          }
                                          
                                          else
                                          {
                                              strNameToShow=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"addedBy"]];
                                              strMess=[NSString stringWithFormat:@"added %@",PartcipantIDstr];
                                              strContent=[NSString stringWithFormat:@"%@ %@",strNameToShow,strMess];
                                          }
                                          
                                          
                                          /*NSString *strNameToShow;
                                           NSString *strMess;
                                           NSDictionary *dictName;
                                           
                                           dictName=[db_class getUserNameForNotification:[userInfo valueForKey:@"addedBy"]];
                                           dictName=[db_class getUserNameForNotification:[userInfo valueForKey:@"participantId"]];
                                           if ([[dictName allKeys] containsObject:@"name"])
                                           {
                                           strNameToShow=[NSString stringWithFormat:@"%@",[dictName valueForKey:@"name"]];
                                           strMess=[NSString stringWithFormat:@"added %@",[dictName valueForKey:@"name"]];
                                           }
                                           else
                                           {
                                           strNameToShow=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"addedBy"]];
                                           strMess=[NSString stringWithFormat:@"added %@",PartcipantIDstr];
                                           }
                                           
                                           NSString *strContent=[NSString stringWithFormat:@"%@ %@",strNameToShow,strMess];
                                           
                                           */
                                          
                                          
                                          if (![strFrom isEqualToString:appDelegate.strID])
                                          {
                                              BOOL isAvailinChatsTable=NO;
                                              isAvailinChatsTable=[db_class doesChatExist:strGroupId];
                                              BOOL success=NO;
                                              if (isAvailinChatsTable==NO)
                                              {
                                                  success=[db_class insertChats:@"" chatRoomId:strGroupId chatRoomType:@"1" sender:@"" lastMessage:strContent lastMessageStatus:@"sending" lastMessagesTime:strTime lastMessagesType:@"header" unReadCount:@"0" groupName:strGroupName  groupImage:strGroupImage sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
                                                  if (success==YES)
                                                  {
                                                      isAvailinChatsTable=YES;
                                                  }
                                              }
                                              else
                                              {
                                                  BOOL success1 = [db_class updateLastMessage: strGroupId sender:@"" lastMessage:strContent lastMessageStatus:@"delivered" lastMessageTime:strTime lastMessagesType:@"header" unReadCount:@"1"];
                                                  NSLog(@"success");
                                              }
//                                                                       BOOL success2=[db_class insertChatMessages:strMgsId userId:strFromMsg groupId:strGroupId chatRoomType:@"1" content:strContent contentType:@"header" contentStatus:@"delivered" sender:@"" sentTime:strTime deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0"];

                                              
                                              [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
                                          }
                                          
                                      }
                              }
                              [appDelegate.socket emit:@"ack" with:@[@{@"userid":appDelegate.strID,@"uniqueid":appDelegate.uniqueID}]];
                          }];
                         

                         
                     }];
                     
//                     NSString *savedValue = [[NSUserDefaults standardUserDefaults]
//                                             stringForKey:@"isDriveLogin"];
//
//                     NSLog(@" %@",savedValue);
//                     if ( [savedValue isEqualToString: @"true"])
//                     {
                     if ([[response valueForKey:@"isDriveLogin"] boolValue]==false)
                     {
                         [self performSegueWithIdentifier:@"SegueHome" sender:self];
                     }
                     else
                     {
                         
                         NSString *mTime = [response valueForKey:@"time"];

                         
                         NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                         [dateFormatter1 setDateFormat:@"yyyyMMddHHmmssSSS"];
                         
                         NSDate *fromTime = [dateFormatter1 dateFromString:mTime];

                         NSDate *now=[NSDate date];
                         NSString *localDateString = [dateFormatter1 stringFromDate:now];
                         
                         NSDate *nowTime = [dateFormatter1 dateFromString:localDateString];

                         
                         
                         NSTimeInterval secondsBetween = [nowTime timeIntervalSinceDate:fromTime];
                         
                         int numberOfDays = secondsBetween / 86400;

                         
                         NSString *msgTime = [NSString stringWithFormat:@"%d days ago",numberOfDays];
            
                         
                         NSString *mCount = [response valueForKey:@"messageCount"];
                        
                         
                         [[NSUserDefaults standardUserDefaults]setObject:mCount forKey:@"messageCount"];
                         
                         [[NSUserDefaults standardUserDefaults]setObject:msgTime forKey:@"messageTime"];
                         
                         
                         [self performSegueWithIdentifier:@"SegueBackup" sender:self];
                     }
//                 }
             }
                 else
                 {
                     
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     // Configure for text only and offset down
                     hud.mode = MBProgressHUDModeText;
                     hud.label.text = [NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                     hud.margin = 10.f;
                     hud.yOffset = 150.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hideAnimated:YES afterDelay:2];
                     
                     
                 }
             }
             
         }];

    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Please check your internet connection"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.label.adjustsFontSizeToFitWidth=YES;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:2];
        
    }

}


-(long long)convertDateToMilliseconds:(NSDate *)date
{
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    long long milliseconds=timeInSeconds*1000;
    return milliseconds;
}
-(NSDate *)convertMillisecondsToDate:(long long)milliSeconds
{
    NSDate *dat = [NSDate dateWithTimeIntervalSince1970:(milliSeconds / 1000.0)];
    return dat;
}



-(NSString*)remaningTime:(NSDate*)startDate endDate:(NSDate*)endDate
{
    NSDateComponents *components;
    NSInteger days;
    NSInteger hour;
    NSInteger minutes;
    NSString *durationString;
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate: startDate toDate: endDate options: 0];
    
    days = [components day];
    hour = [components hour];
    minutes = [components minute];
    
    if(days>0)
    {
        if(days>1)
            durationString=[NSString stringWithFormat:@"%d days",days];
        else
            durationString=[NSString stringWithFormat:@"%d day",days];
        return durationString;
    }
    if(hour>0)
    {
        if(hour>1)
            durationString=[NSString stringWithFormat:@"%d hours",hour];
        else
            durationString=[NSString stringWithFormat:@"%d hour",hour];
        return durationString;
    }
    if(minutes>0)
    {
        if(minutes>1)
            durationString = [NSString stringWithFormat:@"%d minutes",minutes];
        else
            durationString = [NSString stringWithFormat:@"%d minute",minutes];
        
        return durationString;
    }
    return @"";
}



#pragma mark
#pragma mark - ImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
   
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:chosenImage];
        cropController.delegate = self;
    cropController.rotateButtonsHidden=YES;
    cropController.rotateClockwiseButtonHidden=YES;
  //  cropController.aspectRatioPickerButtonHidden=YES;
   
    // -- Uncomment these if you want to test out restoring to a previous crop setting --
    //cropController.angle = 90; // The initial angle in which the image will be rotated
    //cropController.imageCropFrame = CGRectMake(0,0,2848,4288); //The
    
    // -- Uncomment the following lines of code to test out the aspect ratio features --
    cropController.aspectRatioPreset = TOCropViewControllerAspectRatioPresetSquare; //Set the initial aspect ratio as a square
    cropController.aspectRatioLockEnabled = YES; // The crop box is locked to the aspect ratio and can't be resized away from it
    cropController.resetAspectRatioEnabled = NO; // When tapping 'reset', the aspect ratio will NOT be reset back to default
    
    // -- Uncomment this line of code to place the toolbar at the top of the view controller --
    // cropController.toolbarPosition = TOCropViewControllerToolbarPositionTop;

    
        self.image = chosenImage;
        
        //If profile picture, push onto the same navigation stack
        if (self.croppingStyle == TOCropViewCroppingStyleCircular) {
            [picker pushViewController:cropController animated:YES];
        }
        else { //otherwise dismiss, and then present from the main controller
            [picker dismissViewControllerAnimated:YES completion:^{
                [self presentViewController:cropController animated:YES completion:nil];
            }];
        }
  
    
    
    assetURL = [info objectForKey:UIImagePickerControllerReferenceURL];
    
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark
#pragma mark - Cropper Delegate -
- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    [self.btnProfile setImage:image forState:UIControlStateNormal];
    
    self.btnProfile.clipsToBounds = YES;
    self.btnProfile.layer.cornerRadius = (self.btnProfile.frame.size.width / 2);//half of the width
    self.btnProfile.layer.borderColor=[UIColor blackColor].CGColor;
    self.btnProfile.layer.borderWidth=1.0f;
    isPictureAdded=@"1";
    
    MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.btnProfile animated:YES];
   // hud.mode = MBProgressHUDAnimationFade;
   // hud.label.text = @"";
    
    self.btnDone.userInteractionEnabled=NO;
    
    __block NSString *fileName = nil;
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library assetForURL:assetURL resultBlock:^(ALAsset *asset)  {
        fileName = asset.defaultRepresentation.filename;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyyMMddHHmmssSSS"];
        NSString *localDateString = [dateFormatter stringFromDate:[NSDate date]];
       // NSLog(@"%@",localDateString);
        
        NSArray* arrExt = [fileName componentsSeparatedByString: @"."];
        NSString* strExt = [arrExt objectAtIndex: arrExt.count-1];
        
        UploadFileName=[NSString stringWithFormat:@"%@.%@",appDelegate.strID,strExt];

        
        //image you want to upload
         UIImage *imageToUpload=[self compressImage:image];
        
        //convert uiimage to
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",UploadFileName]];
        [UIImagePNGRepresentation(imageToUpload) writeToFile:filePath atomically:YES];
        
        NSURL* fileUrl = [NSURL fileURLWithPath:filePath];
        
        AWSS3TransferUtilityUploadCompletionHandlerBlock completionHandler = ^(AWSS3TransferUtilityUploadTask *task, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // Do something e.g. Alert a user for transfer completion.
                // On failed uploads, `error` contains the error object.
                [MBProgressHUD hideHUDForView:self.btnProfile animated:YES];
                self.btnDone.userInteractionEnabled=YES;
                NSLog(@"%@",error);
                [CustomAlbum addNewAssetWithImage:imageToUpload toAlbum:[CustomAlbum getMyAlbumWithName:CSAlbum] onSuccess:^(NSString *ImageId) {
                    NSLog(@"image saved");
                    
                }
                                          onError:^(NSError *error)
                 {
                     NSLog(@"probelm in saving image");
                 }];
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                
                // Configure for text only and offset down
                hud.mode = MBProgressHUDModeText;
                hud.label.text = @"Profile picture uploaded";
                hud.margin = 10.f;
                hud.yOffset = 150.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hideAnimated:YES afterDelay:2];
            });
        };
        
        AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility defaultS3TransferUtility];
        [[transferUtility uploadFile:fileUrl bucket:FILE_BUCKET_NAME key:UploadFileName contentType:@"image/png" expression:nil completionHandler:completionHandler]
        continueWithBlock:^id(AWSTask *task) {
            if (task.error) {
                [MBProgressHUD hideHUDForView:self.btnProfile animated:YES];
                NSLog(@"Error: %@", task.error);
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                
                // Configure for text only and offset down
                hud.mode = MBProgressHUDModeText;
                hud.label.text = @"Profile picture not uploaded";
                hud.margin = 10.f;
                hud.yOffset = 150.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hideAnimated:YES afterDelay:2];

            }
           
            if (task.result) {
                AWSS3TransferUtilityUploadTask *uploadTask = task.result;
                // Do something with uploadTask.
                NSLog(@"%@",uploadTask);
            }
            
            return nil;
        }];
        
    }failureBlock:^(NSError *error)
    {
        NSLog(@"%@",error);
    }];
    
     [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
   
}

- (UIImage *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth) {
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
}




#pragma mark -
#pragma mark - Keyboard

-(void)dismissKeyBoard
{
    // [tableTexture removeFromSuperview];
    
    for(UIView *subView in self.scrollView.subviews)
    {
        if([subView isKindOfClass:[UITextField class]])
        {
            if([subView isFirstResponder])
            {
                [subView resignFirstResponder];
                break;
            }
        }
        else if([subView isKindOfClass:[UITextView class]])
        {
            if([subView isFirstResponder])
            {
                [subView resignFirstResponder];
                break;
            }
        }
        
    }
    
    
    
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
-(void) textFieldDidBeginEditing:(UITextField *)textField
{
   
    currentTextView=textField;
    [self moveScrollView:textField.frame];
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
    currentTextView1=textView;
    [self moveScrollView:textView.frame];
}
- (void)keyboardDidHide:(NSNotification *)n
{
    keyboardIsShown = NO;
    [self.view removeGestureRecognizer:tapGesture];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardDidShow:(NSNotification *)n
{
    if (keyboardIsShown) {
        return;
    }
    
    keyboardIsShown = YES;
    [self.scrollView addGestureRecognizer:tapGesture];
}

- (void) moveScrollView:(CGRect) rect
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    
    CGSize kbSize = CGSizeMake(32, 260); // keyboard height
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.scrollView.frame;
    aRect.size.height -= (kbSize.height+55);
    if (!CGRectContainsPoint(aRect, rect.origin) )
    {
        CGPoint scrollPoint = CGPointMake(0.0, rect.origin.y-kbSize.height+100);
        
        if(screenHeight==480)
            scrollPoint = CGPointMake(0.0, rect.origin.y-kbSize.height+100+90);
        
        if(scrollPoint.y>0)
            [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

- (void) hideKeyBoard
{
    
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    //  NSLog(@"Keyboard is hidden");
    //  UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    UIEdgeInsets content=UIEdgeInsetsMake(0, 0, 0, 0);
    self.scrollView.contentInset = content;
    self.scrollView.scrollIndicatorInsets = content;
}


#pragma mark -
#pragma mark - Font


-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)view;
        [btn setFont:[UIFont fontWithName:fontFamily size:[[btn font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *txtFld = (UITextField *)view;
        [txtFld setFont:[UIFont fontWithName:fontFamily size:[[txtFld font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextView class]])
    {
        UITextView *txtVw = (UITextView *)view;
        [txtVw setFont:[UIFont fontWithName:fontFamily size:[[txtVw font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}


@end
