//
//  RecentChatsCell.h
//  ZoeChat
//
//  Created by macmini on 08/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecentChatsCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *profileImgView;
@property (strong, nonatomic) IBOutlet UILabel *nameLbl;
@property (strong, nonatomic) IBOutlet UILabel *RecentInitialLbl;

@end
