//
//  RequestVC.h
//  ZoeChat
//
//  Created by admin on 16/05/18.
//  Copyright © 2018 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *vwTop;
@property (weak, nonatomic) IBOutlet UITableView *requestTable;
@property NSMutableArray*datarequest;
@property NSMutableArray*location;
@end
