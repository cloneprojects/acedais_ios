//
//  ForgotPasswordVC.m
//  ZoeChat
//
//  Created by macmini on 13/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "ForgotPasswordVC.h"
#import "MainTabBarVC.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "AFNHelper.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "DBClass.h"

@interface ForgotPasswordVC ()<UITextFieldDelegate>
{
    AppDelegate *appdelegate;
    NSString *otpStr;
    DBClass *db_class;
}
@end

@implementation ForgotPasswordVC
@synthesize getChatId;

- (void)viewDidLoad {
    [super viewDidLoad];
    _submitbtn.layer.cornerRadius = 5;
    _submitbtn.clipsToBounds = YES;
    db_class=[[DBClass alloc]init];
        appdelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    _submit.layer.cornerRadius = 5;
    _submit.clipsToBounds = YES;
    self.ForgotPasswordView.hidden=YES;
    [self.ForgotPasswordView.layer setCornerRadius:5.0f];
    [self.ForgotPasswordView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.ForgotPasswordView.layer setBorderWidth:0.2f];
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        
        self.submitbtn.backgroundColor=color;
        self.otpTxtFld.tintColor=color;
        self.passwordTxt.tintColor=color;
        self.conPasswordTxt.tintColor=color;
        
        
        
    }
    else
    {
        self.submitbtn.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.otpTxtFld.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.passwordTxt.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.conPasswordTxt.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];

        
    }
    [self callRequestOTPAPI];
    
}
-(void)ShowPasswordView
{
    UIView *viewForPassword=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+self.navigationController.navigationBar.frame.size.height+50)];
    viewForPassword.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7f];
    viewForPassword.tag=9087650;
    [self.navigationController.view addSubview:viewForPassword];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onRemoveViewAudioVideo:)]; // this is the current problem like a lot of people out there...
    [viewForPassword addGestureRecognizer:tap];
    self.ForgotPasswordView.hidden=NO;
    
    [viewForPassword addSubview:self.ForgotPasswordView];
}
- (void) onRemoveViewAudioVideo: (UIGestureRecognizer *) gesture
{
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    self.ForgotPasswordView.hidden=YES;
    [viewForAudioOrVideo removeFromSuperview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)ClickOnSubmit:(id)sender {
        [_otpTxtFld resignFirstResponder];	
    if ([otpStr isEqualToString:self.otpTxtFld.text])
    {
        [self ShowPasswordView];
//        UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        MainTabBarVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainTabBarVC"];
//        [self.navigationController pushViewController:hme animated:YES];
    }
    else
    {
        
    }
    
}

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)callRequestOTPAPI
{
    
    if ([appdelegate.strInternetMode isEqualToString:@"online"])
    {
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:appdelegate.strID forKey:@"id"];
        
        MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDAnimationFade;
        hud.label.text = @"Loading";
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_REQUESTOTP withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
             if (response)
             {
                 NSLog(@"Response: %@",response);
                 if ([[response valueForKey:@"error"] boolValue]==false)
                 {
                     otpStr=[[response valueForKey:@"otp"]stringValue];
                    // NSString *str=[@(otpStr) ]
                     self.otpTxtFld.text=otpStr;
                     
                     
                 }
                 else
                 {
                     
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     // Configure for text only and offset down
                     hud.mode = MBProgressHUDModeText;
                     hud.label.text = [NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                     hud.margin = 10.f;
                     hud.yOffset = 150.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hideAnimated:YES afterDelay:2];
                 }
             }
             
         }];
        
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Please check your internet connection"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.label.adjustsFontSizeToFitWidth=YES;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:2];
        
    }
    
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)submitBtn:(id)sender {
    [_passwordTxt resignFirstResponder];
    
    [_conPasswordTxt resignFirstResponder];
    
    if ([_passwordTxt.text isEqualToString:@""] || [_conPasswordTxt.text isEqualToString:@""])
    {
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Fill all Details"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.label.adjustsFontSizeToFitWidth=YES;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:2];
    }
    
    else if ([_passwordTxt.text isEqualToString:_conPasswordTxt.text])
    {
        NSString *passwordStr=[NSString stringWithFormat:@"%@", _passwordTxt.text];
        NSLog(@"%@",getChatId);
        BOOL success1=[db_class updateLockChatTable:getChatId isLock:@"1"];
        BOOL success2=[db_class updatePasswordChatTable:getChatId password:passwordStr];
        UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
        _passwordTxt.text=@"";
        _conPasswordTxt.text=@"";
        self.ForgotPasswordView.hidden=YES;
        [viewForAudioOrVideo removeFromSuperview];
        UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MainTabBarVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainTabBarVC"];
        [self.navigationController pushViewController:hme animated:YES];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Enter Correct Password"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.label.adjustsFontSizeToFitWidth=YES;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:2];
        
    }
    
    
}
@end
