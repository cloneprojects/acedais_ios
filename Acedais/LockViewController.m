//
//  LockViewController.m
//  ZoeChat
//
//  Created by macmini on 11/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "LockViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AFNHelper.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "Constants.h"
#import "AppDelegate.h"
#import "MainTabBarVC.h"
#import "DBClass.h"
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;
@interface LockViewController ()<UITextFieldDelegate>
{
    AppDelegate *appDelegate;
    CGFloat animatedDistance;
    DBClass *db_class;

}
@end

@implementation LockViewController
@synthesize getChatId;

- (void)viewDidLoad {
    [super viewDidLoad];
    db_class=[[DBClass alloc]init];
    _submitBtn.layer.cornerRadius = 5;
    _submitBtn.clipsToBounds = YES;
    _passwordSubmitBtn.layer.cornerRadius = 5; 
    _passwordSubmitBtn.clipsToBounds = YES;
    
    self.passwordView.hidden=YES;
    [self.passwordView.layer setCornerRadius:5.0f];
    [self.passwordView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.passwordView.layer setBorderWidth:0.2f];

    appDelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        
        self.submitBtn.backgroundColor=color;
        self.passwordSubmitBtn.backgroundColor=color;
        self.emailTxtFld.tintColor=color;
        self.phoneTxtFld.tintColor=color;
        self.passwordTxtFld.tintColor=color;
        self.confirmPasswordTxtFld.tintColor=color;

        
        
        
    }
    else
    {
        self.submitBtn.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.passwordSubmitBtn.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.emailTxtFld.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.phoneTxtFld.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.passwordTxtFld.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.confirmPasswordTxtFld.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];

        
        
        
        
    }

    }
//-(void)addBottomLine:(UITextField*)textfield
//{
//    CALayer *border = [CALayer layer];
//    CGFloat borderWidth = 0.5;
//    border.borderColor = [UIColor darkGrayColor].CGColor;
//    border.frame = CGRectMake(0, textfield.frame.size.height - borderWidth, textfield.frame.size.width, textfield.frame.size.height);
//    border.borderWidth = borderWidth;
//    [textfield.layer addSublayer:border];
//    textfield.layer.masksToBounds = YES;
//}
-(void)ShowPasswordView
{
    UIView *viewForPassword=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+self.navigationController.navigationBar.frame.size.height+50)];
    viewForPassword.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7f];
    viewForPassword.tag=9087650;
    [self.navigationController.view addSubview:viewForPassword];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onRemoveViewAudioVideo:)]; // this is the current problem like a lot of people out there...
    [viewForPassword addGestureRecognizer:tap];
    self.passwordView.hidden=NO;

    [viewForPassword addSubview:self.passwordView];
}
- (void) onRemoveViewAudioVideo: (UIGestureRecognizer *) gesture
{
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    self.passwordView.hidden=YES;
    [viewForAudioOrVideo removeFromSuperview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)clickOnsubmitBtn:(id)sender {
    [_emailTxtFld resignFirstResponder];
    [_phoneTxtFld resignFirstResponder];
    
    
    
    BOOL isValidEmail=[self validateEmail:_emailTxtFld.text];
    BOOL isPhoneNumber=[self myMobileNumberValidate:_phoneTxtFld.text];
    
    
    if ([_emailTxtFld.text isEqualToString:@""] || [_phoneTxtFld.text isEqualToString:@""])
    {
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Fill all Details"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.label.adjustsFontSizeToFitWidth=YES;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:2];
    }
    
    else if (isValidEmail==NO && isPhoneNumber==NO)
    {
        //Alert
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Alert"
                                                                      message:@"Enter Valid Phonenumber and EmailId"
                                                               preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Ok"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        NSLog(@"you pressed No, thanks button");
                                    }];
        
        
        
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    else if(!isPhoneNumber)
    {
        //Alert
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Alert"
                                                                      message:@"Enter Valid Phonenumber"
                                                               preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Ok"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        NSLog(@"you pressed No, thanks button");
                                    }];
        
        
        
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if(_phoneTxtFld.text.length<10)
    {
        //Alert
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Alert"
                                                                      message:@"Enter Valid Phonenumber"
                                                               preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Ok"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        NSLog(@"you pressed No, thanks button");
                                    }];
        
        
        
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if (!isValidEmail)
    {
        //Alert
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Alert"
                                                                      message:@"Enter Valid Email-Id"
                                                               preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Ok"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        
                                    }];
        
        
        
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if (isValidEmail==YES && isPhoneNumber==YES)
    {
        [self callRecoveryAPI];
        
    }
    
    
}
- (BOOL)myMobileNumberValidate:(NSString*)number
{
    NSString *numberRegEx = @"[0-9]{10}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if ([numberTest evaluateWithObject:number] == YES)
        return TRUE;
    else
        return FALSE;
}
- (IBAction)clickOnBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)ClickonPasswordSubmit:(id)sender {
    if ([_passwordTxtFld.text isEqualToString:@""] || [_confirmPasswordTxtFld.text isEqualToString:@""])
    {
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Fill all Details"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.label.adjustsFontSizeToFitWidth=YES;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:2];
    }
   else if ([_passwordTxtFld.text isEqualToString:_confirmPasswordTxtFld.text])
    {
        NSString *passwordStr=[NSString stringWithFormat:@"%@", _passwordTxtFld.text];
        NSLog(@"%@",getChatId);
        BOOL success1=[db_class updateLockChatTable:getChatId isLock:@"1"];
        BOOL success2=[db_class updatePasswordChatTable:getChatId password:passwordStr];
        UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
        _passwordTxtFld.text=@"";
        _confirmPasswordTxtFld.text=@"";
        self.passwordView.hidden=YES;
        [viewForAudioOrVideo removeFromSuperview];
        UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MainTabBarVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainTabBarVC"];
        [self.navigationController pushViewController:hme animated:YES];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Enter Correct Password"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.label.adjustsFontSizeToFitWidth=YES;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:2];

    }
    
}
-(BOOL) validateEmail:(NSString*) emailString
{
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    NSLog(@"%i", regExMatches);
    if (regExMatches == 0) {
        return NO;
    }
    else
        return YES;
}
-(void)callRecoveryAPI
{
    NSString *emailStr=[NSString stringWithFormat:@"%@",_emailTxtFld.text];
    NSString *phoneStr=[NSString stringWithFormat:@"%@",_phoneTxtFld.text];

    if ([appDelegate.strInternetMode isEqualToString:@"online"])
    {
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setObject:appDelegate.strID forKey:@"id"];
            [dictParam setObject:emailStr forKey:@"email"];
            [dictParam setObject:phoneStr forKey:@"phone"];
        
        MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDAnimationFade;
        hud.label.text = @"Loading";
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_RECOVERY withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
             if (response)
             {
                 NSLog(@"Response: %@",response);
                 if ([[response valueForKey:@"error"] boolValue]==false)
                 {
                     
                     [self ShowPasswordView];
                     
                 }
                 else
                 {
                     
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     // Configure for text only and offset down
                     hud.mode = MBProgressHUDModeText;
                     hud.label.text = [NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                     hud.margin = 10.f;
                     hud.yOffset = 150.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hideAnimated:YES afterDelay:2];
                     
                     
                 }
             }
             
         }];
        
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Please check your internet connection"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.label.adjustsFontSizeToFitWidth=YES;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:2];
        
    }
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textfield{
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

@end
