//
//  PrivacyTblCell.h
//  ZoeChat
//
//  Created by macmini on 10/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivacyTblCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *nameLbl;
@property (strong, nonatomic) IBOutlet UILabel *statusLbl;

@end
