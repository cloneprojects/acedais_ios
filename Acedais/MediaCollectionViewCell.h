//
//  MediaCollectionViewCell.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 27/03/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MediaCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgMedia;

@end
