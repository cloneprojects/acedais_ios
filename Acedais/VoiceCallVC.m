//
//  VoiceCallVC.m
//  ZoeChat
//
//  Created by Pyramidions Solution on 30/03/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "VoiceCallVC.h"
#import "AFNHelper.h"
#import "Constants.h"
#import "VideoSession.h"
#import "VideoViewLayouter.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
#import<CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCall.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import "DBClass.h"
#import "MZTimerLabel.h"
@import CallKit;

@interface VoiceCallVC () <AgoraRtcEngineDelegate>
{
    AppDelegate *appDelegate;
    AVAudioPlayer *myAudioPlayer ;
    NSTimer *timerCall;
    DBClass *db_class;
    MZTimerLabel *stopwatch;
     BOOL isCallConnected;
}
@property (nonatomic, strong) CXCallController* cxcallcontrollerobject;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *flowViews;
@property (weak, nonatomic) IBOutlet UILabel *roomNameLabel;

@property (weak, nonatomic) IBOutlet UIView *controlView;

@property (weak, nonatomic) IBOutlet UIButton *muteVideoButton;
@property (weak, nonatomic) IBOutlet UIButton *muteAudioButton;

@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property (weak, nonatomic) IBOutlet UIButton *speakerButton;

@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *backgroundTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *backgroundDoubleTap;

@property (strong, nonatomic) AgoraRtcEngineKit *agoraKit;
@property (strong, nonatomic) NSMutableArray<VideoSession *> *videoSessions;
@property (strong, nonatomic) VideoSession *doubleClickFullSession;
@property (strong, nonatomic) VideoViewLayouter *videoViewLayouter;

@property (assign, nonatomic) BOOL shouldHideFlowViews;
@property (assign, nonatomic) BOOL audioMuted;
@property (assign, nonatomic) BOOL videoMuted;
@property (assign, nonatomic) BOOL speakerEnabled;

@end

@implementation VoiceCallVC
@synthesize strName, strimage,strZoeChatID, strInComing;
- (void)setShouldHideFlowViews:(BOOL)shouldHideFlowViews {
    _shouldHideFlowViews = shouldHideFlowViews;
    if (self.flowViews.count) {
        for (UIView *view in self.flowViews) {
            view.hidden = shouldHideFlowViews;
        }
    }
}

- (void)setDoubleClickFullSession:(VideoSession *)doubleClickFullSession {
    _doubleClickFullSession = doubleClickFullSession;
    if (self.videoSessions.count >= 3) {
        [self updateInterfaceWithSessions:self.videoSessions targetSize:self.containerView.frame.size animation:YES];
    }
}

- (VideoViewLayouter *)videoViewLayouter {
    if (!_videoViewLayouter) {
        _videoViewLayouter = [[VideoViewLayouter alloc] init];
    }
    return _videoViewLayouter;
}

- (void)setAudioMuted:(BOOL)audioMuted {
    _audioMuted = audioMuted;
    [self.muteAudioButton setBackgroundImage: [UIImage imageNamed:(audioMuted ? @"btn_mute_blue" : @"btn_mute")] forState:UIControlStateNormal];
    [self.agoraKit muteLocalAudioStream:audioMuted];
}
/*
- (void)setVideoMuted:(BOOL)videoMuted {
    _videoMuted = videoMuted;
    [self.muteVideoButton setBackgroundImage: [UIImage imageNamed:(videoMuted ? @"btn_video" : @"btn_voice")] forState:UIControlStateNormal];
    self.cameraButton.hidden = videoMuted;
    self.speakerButton.hidden = videoMuted;
    
    [self.agoraKit muteLocalVideoStream:videoMuted];
    
    [self setVideoMuted:videoMuted forUid:0];
    [self updateSelfViewVisiable];
}
*/

- (void)setSpeakerEnabled:(BOOL)speakerEnabled {
    _speakerEnabled = speakerEnabled;
    [self.speakerButton setBackgroundImage:[UIImage imageNamed:(speakerEnabled ? @"btn_speaker_blue" : @"btn_speaker")] forState:UIControlStateNormal];
    //[self.speakerButton setBackgroundImage:[UIImage imageNamed:(speakerEnabled ? @"btn_speaker" : @"btn_speaker_blue")] forState:UIControlStateHighlighted];
    
    [self.agoraKit setEnableSpeakerphone:speakerEnabled];
}

- (void)viewDidLoad {
    [super viewDidLoad];
     appDelegate.strUUID=nil;
     db_class=[[DBClass alloc]init];
    [self.lblImg setHidden:YES];
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        
        self.viewForNameStatus.backgroundColor=color;
        self.viewForPickCall.backgroundColor=color;
        self.controlView.backgroundColor=color;
        self.callImgBg.backgroundColor=color;


        
        
        
    }
    else
    {
        self.viewForNameStatus.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.viewForPickCall.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.controlView.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.view.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.callImgBg.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];

        
        
        
        
        
    }
    
     appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
     [[self navigationController] setNavigationBarHidden:YES animated:YES];
    _viewForPickCall.hidden=YES;
    self.imgProfile.layer.cornerRadius=self.imgProfile.frame.size.height/2;
    self.imgProfile.clipsToBounds=YES;
    self.lblImg.layer.cornerRadius=self.lblImg.frame.size.height/2;
    self.lblImg.clipsToBounds=YES;
    NSLog(@"%@",appDelegate.dictInComomgCall);
     NSString *strChannalId=[NSString stringWithFormat:@"%@",appDelegate.strID];
    BOOL isEmpty = ([appDelegate.dictInComomgCall count] == 0);
   
    if (!isEmpty)
    {
        strInComing=[appDelegate.dictInComomgCall valueForKey:@"inComing"];
        strName=[appDelegate.dictInComomgCall valueForKey:@"name"];
        strimage=[appDelegate.dictInComomgCall valueForKey:@"image"];
        strZoeChatID=[appDelegate.dictInComomgCall valueForKey:@"fromId"];
        strChannalId=[appDelegate.dictInComomgCall valueForKey:@"channelId"];
    }
    
    _lblName.text=strName;
    _callNumberlbl.text=[NSString stringWithFormat:@"+%@...",strZoeChatID];
   
    if ([strInComing isEqualToString:@"no"])
    {
        _lblStatus.text=@"Calling";
         [self playSound:@"RingSound" :@"mp3"];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setValue:strZoeChatID forKey:@"to"];
        [dictParam setValue:appDelegate.strID forKey:@"from"];
        [dictParam setValue:strChannalId forKey:@"channelId"];
        [dictParam setValue:@"false" forKey:@"isVideoCall"];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_MAKE_CALL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             
             if (response)
             {
                 //  NSLog(@"Response: %@",response);
                 if ([[response valueForKey:@"error"] boolValue]==false)
                 {
                     
                 }
                 else
                 {
                     NSLog(@"error: %@",error);
                     [stopwatch pause];
                     [myAudioPlayer stop];
                     [self.navigationController popViewControllerAnimated:YES];
                      isCallConnected=NO;
                     
                 }
             }
             
         }];
        NSUUID *uuid=[NSUUID UUID];
        appDelegate.strUUID=uuid;
        [timerCall invalidate];
        timerCall=nil;
        timerCall=[NSTimer scheduledTimerWithTimeInterval:45 target:self selector:@selector(roomVCNeedClose) userInfo:nil repeats:NO];

        long long milliSeconds=[self convertDateToMilliseconds:[NSDate date]];
        appDelegate.strCallTime=[NSString stringWithFormat:@"%lld",milliSeconds];
        NSLog(@"%@",appDelegate.strCallTime);
        
        
    }
    else
    {
        _lblStatus.text=@"Connecting";
    }
    
    if (![strimage isEqualToString:@"(null)"])
    {
        [_imgProfile sd_setImageWithURL:[NSURL URLWithString:strimage] placeholderImage:[UIImage imageNamed:@"clear.png"]];
    }
    else
    {
        [self.lblImg setHidden:NO];
        NSString *InitialStr;
        UIColor *bgColor;
        InitialStr=[strName substringToIndex:1];
        if ([InitialStr isEqualToString:@"A"])
        {
            bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"B"])
        {
            bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"C"])
        {
            bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"D"])
        {
            bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"E"])
        {
            bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"F"])
        {
            bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"G"])
        {
            bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"H"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"I"])
        {
            bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"J"])
        {
            bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"K"])
        {
            bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"L"])
        {
            bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"M"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"N"])
        {
            bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"O"])
        {
            bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"P"])
        {
            bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"Q"])
        {
            bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"R"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"S"])
        {
            bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"T"])
        {
            bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"U"])
        {
            bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"V"])
        {
            bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"W"])
        {
            bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"X"])
        {
            bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"Y"])
        {
            bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"Z"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
        }
        self.lblImg.text=InitialStr;
        self.lblImg.backgroundColor=bgColor;
    
    //        _imgProfile.image=[UIImage imageNamed:@"userImage.png"];
    }
    

    self.videoSessions = [[NSMutableArray alloc] init];
    self.roomName=strChannalId;
    self.roomNameLabel.text = self.roomName;
    [self.backgroundTap requireGestureRecognizerToFail:self.backgroundDoubleTap];

    [self loadAgoraKit];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onEndCall:) name:@"DisConnect_Call" object:nil];
    
}
-(void)viewWillAppear:(BOOL)animated
{
      [[self navigationController] setNavigationBarHidden:YES animated:YES];
     isCallConnected=NO;
}
-(void)viewWillDisappear:(BOOL)animated
{
     [[self navigationController] setNavigationBarHidden:NO animated:YES];
}

- (IBAction)doMuteVideoPressed:(UIButton *)sender {
    self.videoMuted = !self.videoMuted;
}

- (IBAction)doMuteAudioPressed:(UIButton *)sender {
    self.audioMuted = !self.audioMuted;
}

- (IBAction)doCameraPressed:(UIButton *)sender {
    [self.agoraKit switchCamera];
}

- (IBAction)doSpeakerPressed:(UIButton *)sender {
    self.speakerEnabled = !self.speakerEnabled;
}

- (IBAction)doClosePressed:(UIButton *)sender {
    [self leaveChannel];
}

- (IBAction)doBackTapped:(UITapGestureRecognizer *)sender {
  //  self.shouldHideFlowViews = !self.shouldHideFlowViews;
}

- (IBAction)doBackDoubleTapped:(UITapGestureRecognizer *)sender {
    if (!self.doubleClickFullSession) {
        NSInteger tappedIndex = [self.videoViewLayouter responseIndexOfLocation:[sender locationInView:self.containerView]];
        if (tappedIndex >= 0 && tappedIndex < self.videoSessions.count) {
            self.doubleClickFullSession = self.videoSessions[tappedIndex];
        }
    } else {
        self.doubleClickFullSession = nil;
    }
}

- (void)updateInterfaceWithSessions:(NSArray *)sessions targetSize:(CGSize)targetSize animation:(BOOL)animation {
    if (animation) {
        [UIView animateWithDuration:0.3 animations:^{
            [self updateInterfaceWithSessions:sessions targetSize:targetSize];
            [self.view layoutIfNeeded];
        }];
    } else {
        [self updateInterfaceWithSessions:sessions targetSize:targetSize];
    }
}

- (void)updateInterfaceWithSessions:(NSArray *)sessions targetSize:(CGSize)targetSize {
    if (!sessions.count) {
        return;
    }
    
    VideoSession *selfSession = sessions.firstObject;
    self.videoViewLayouter.selfView = selfSession.hostingView;
    self.videoViewLayouter.selfSize = selfSession.size;
    self.videoViewLayouter.targetSize = targetSize;
    
    NSMutableArray *peerVideoViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 1; i < sessions.count; ++i) {
        VideoSession *session = sessions[i];
        [peerVideoViews addObject:session.hostingView];
    }
    self.videoViewLayouter.videoViews = peerVideoViews;
    self.videoViewLayouter.fullView = self.doubleClickFullSession.hostingView;
    self.videoViewLayouter.containerView = self.containerView;
    
    [self.videoViewLayouter layoutVideoViews];
    [self updateSelfViewVisiable];
    
    if (sessions.count >= 3) {
        self.backgroundDoubleTap.enabled = YES;
    } else {
        self.backgroundDoubleTap.enabled = NO;
        self.doubleClickFullSession = nil;
    }
}

- (void)setIdleTimerActive:(BOOL)active {
    [UIApplication sharedApplication].idleTimerDisabled = !active;
}

- (void)addLocalSession {
    VideoSession *localSession = [VideoSession localSession];
    [self.videoSessions addObject:localSession];
    [self.agoraKit setupLocalVideo:localSession.canvas];
    [self updateInterfaceWithSessions:self.videoSessions targetSize:self.containerView.frame.size animation:YES];
}

- (VideoSession *)fetchSessionOfUid:(NSUInteger)uid {
    for (VideoSession *session in self.videoSessions) {
        if (session.uid == uid) {
            return session;
        }
    }
    return nil;
}

- (VideoSession *)videoSessionOfUid:(NSUInteger)uid {
    VideoSession *fetchedSession = [self fetchSessionOfUid:uid];
    if (fetchedSession) {
        return fetchedSession;
    } else {
        VideoSession *newSession = [[VideoSession alloc] initWithUid:uid];
        [self.videoSessions addObject:newSession];
        [self updateInterfaceWithSessions:self.videoSessions targetSize:self.containerView.frame.size animation:YES];
        return newSession;
    }
}

- (void)setVideoMuted:(BOOL)muted forUid:(NSUInteger)uid {
    VideoSession *fetchedSession = [self fetchSessionOfUid:uid];
    fetchedSession.isVideoMuted = muted;
}

- (void)updateSelfViewVisiable {
    UIView *selfView = self.videoSessions.firstObject.hostingView;
    if (self.videoSessions.count == 2) {
        selfView.hidden = self.videoMuted;
    } else {
        selfView.hidden = false;
    }
}

- (void)alertString:(NSString *)string {
    if (!string.length) {
        return;
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:string preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        alert.view.tintColor=color;
    }
    else
    {
        
        alert.view.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        
    }
}

- (void)leaveChannel {
    [self.agoraKit setupLocalVideo:nil];
    [self.agoraKit leaveChannel:nil];
    [self.agoraKit stopPreview];
    
    for (VideoSession *session in self.videoSessions) {
        [session.hostingView removeFromSuperview];
    }
    [self.videoSessions removeAllObjects];
    
    [self setIdleTimerActive:YES];
    
    [self roomVCNeedClose];
    
}

//MARK: - Agora Media SDK
- (void)loadAgoraKit {
    self.agoraKit = [AgoraRtcEngineKit sharedEngineWithAppId:AGORA_APP_ID delegate:self];
    [self.agoraKit setChannelProfile:AgoraRtc_ChannelProfile_Communication];
    [self.agoraKit disableVideo];
    [self.agoraKit setVideoProfile:self.videoProfile swapWidthAndHeight:NO];
    
    [self addLocalSession];
    [self.agoraKit startPreview];
    
    int code = [self.agoraKit joinChannelByKey:nil channelName:self.roomName info:nil uid:0 joinSuccess:nil];
    if (code == 0) {
        [self setIdleTimerActive:NO];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self alertString:[NSString stringWithFormat:@"Join channel failed: %d", code]];
        });
    }
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine firstRemoteVideoDecodedOfUid:(NSUInteger)uid size:(CGSize)size elapsed:(NSInteger)elapsed {
    VideoSession *userSession = [self videoSessionOfUid:uid];
    userSession.size = size;
    [self.agoraKit setupRemoteVideo:userSession.canvas];
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine firstLocalVideoFrameWithSize:(CGSize)size elapsed:(NSInteger)elapsed {
    if (self.videoSessions.count) {
        VideoSession *selfSession = self.videoSessions.firstObject;
        selfSession.size = size;
        [self updateInterfaceWithSessions:self.videoSessions targetSize:self.containerView.frame.size animation:NO];
    }
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine didOfflineOfUid:(NSUInteger)uid reason:(AgoraRtcUserOfflineReason)reason {
    VideoSession *deleteSession;
    for (VideoSession *session in self.videoSessions) {
        if (session.uid == uid) {
            deleteSession = session;
        }
    }
    
    if (deleteSession) {
        [self.videoSessions removeObject:deleteSession];
        [deleteSession.hostingView removeFromSuperview];
        [self updateInterfaceWithSessions:self.videoSessions targetSize:self.containerView.frame.size animation:YES];
        
        if (deleteSession == self.doubleClickFullSession) {
            self.doubleClickFullSession = nil;
        }
    }
    
    CXEndCallAction *endCallAction = [[CXEndCallAction alloc] initWithCallUUID:appDelegate.strUUID];
    CXTransaction *transaction = [[CXTransaction alloc] initWithAction:endCallAction];
    CXCallController *controller = [[CXCallController alloc] init];
    NSLog(@"%@ endCallRequest uuid %@",NSStringFromClass([self class]),appDelegate.strUUID.UUIDString);
    [controller requestTransaction:transaction completion:^(NSError * _Nullable error) {
        if (error) {
            NSLog(@"%@ endCallRequest error %@",NSStringFromClass([self class]),[error description]);
        }
    }];
    [self.navigationController popViewControllerAnimated:YES];
     isCallConnected=NO;
    [myAudioPlayer stop];
    [stopwatch pause];
}
-(void)rtcEngine:(AgoraRtcEngineKit *)engine didJoinedOfUid:(NSUInteger)uid elapsed:(NSInteger)elapsed
{
      isCallConnected=YES;
     [myAudioPlayer stop];
    [timerCall invalidate];
    timerCall=nil;
    stopwatch = [[MZTimerLabel alloc] initWithLabel:_lblStatus];
    [stopwatch start];
}
- (void)rtcEngine:(AgoraRtcEngineKit *)engine didVideoMuted:(BOOL)muted byUid:(NSUInteger)uid {
    [self setVideoMuted:muted forUid:uid];
}

- (void)roomVCNeedClose
{
    if (!isCallConnected)
    {
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setValue:strZoeChatID forKey:@"to"];
        [dictParam setValue:appDelegate.strID forKey:@"from"];
        [dictParam setValue:@"out" forKey:@"pushType"];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_END_CALL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             
             if (response)
             {
                 //  NSLog(@"Response: %@",response);
                 if ([[response valueForKey:@"error"] boolValue]==false)
                 {
                     
                 }
                 else
                 {
                     
                     
                 }
             }
             
         }];
    }
    

    
    CXEndCallAction *endCallAction = [[CXEndCallAction alloc] initWithCallUUID:appDelegate.strUUID];
    CXTransaction *transaction = [[CXTransaction alloc] initWithAction:endCallAction];
    CXCallController *controller = [[CXCallController alloc] init];
    NSLog(@"%@ endCallRequest uuid %@",NSStringFromClass([self class]),appDelegate.strUUID.UUIDString);
    [controller requestTransaction:transaction completion:^(NSError * _Nullable error) {
        if (error) {
            NSLog(@"%@ endCallRequest error %@",NSStringFromClass([self class]),[error description]);
        }
    }];
   [self.navigationController popViewControllerAnimated:YES];
     isCallConnected=NO;
    [myAudioPlayer stop];
    [stopwatch pause];
    NSString *strCallStatus;
    
    if ([strInComing isEqualToString:@"no"])
    {
        strCallStatus=@"1";
    }
    else
    {
         strCallStatus=@"0";
    }
        
    BOOL success=[db_class insertCalls:@"" zoeChatId:strZoeChatID callTime:appDelegate.strCallTime callType:@"voice" callLog:strCallStatus callDuration:@"00:00"];
}



- (IBAction)onPickCall:(id)sender
{
    _viewForPickCall.hidden=YES;
}

- (IBAction)onEndCall:(id)sender
{
    [stopwatch pause];
    [myAudioPlayer stop];
    [self.navigationController popViewControllerAnimated:YES];
     isCallConnected=NO;
}

- (void)playSound :(NSString *)fName :(NSString *) ext{
    SystemSoundID audioEffect;
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fName ofType: @"mp3"];
    
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath ];
    
    myAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
    
    myAudioPlayer.numberOfLoops = -1;
    
    NSError *sessionError = nil;
    
    // Change the default output audio route
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    
    // get your audio session somehow
    
    [audioSession setCategory:AVAudioSessionCategoryMultiRoute error:&sessionError];
    
    
    
    BOOL success= [audioSession overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&sessionError];
    
    [audioSession setActive:YES error:&sessionError];
    if(!success)
    {
        NSLog(@"error doing outputaudioportoverride - %@", [sessionError localizedDescription]);
    }
    [myAudioPlayer setVolume:1.0f];
    [myAudioPlayer play];
    
}

-(long long)convertDateToMilliseconds:(NSDate *)date
{
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    long long milliseconds=timeInSeconds*1000;
    return milliseconds;
}
-(NSDate *)convertMillisecondsToDate:(long long)milliSeconds
{
    NSDate *dat = [NSDate dateWithTimeIntervalSince1970:(milliSeconds / 1000.0)];
    return dat;
}


@end
