//
//  GettingPhoneNumberVC.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 29/12/16.
//  Copyright © 2016 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GettingPhoneNumberVC : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnCountry;
- (IBAction)onCountry:(id)sender forEvent:(UIEvent *)event;

@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblCountryCode;

@property (weak, nonatomic) IBOutlet UIView *viewBG;
- (IBAction)onNext:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@end
