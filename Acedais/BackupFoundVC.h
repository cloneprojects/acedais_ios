//
//  BackupFoundVC.h
//  ZoeChat
//
//  Created by veena on 3/2/18.
//  Copyright © 2018 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SSZipArchive/SSZipArchive.h>

@interface BackupFoundVC : UIViewController<UIDocumentMenuDelegate,UIDocumentPickerDelegate>
- (IBAction)onRestoreBtnTapped:(id)sender;
- (IBAction)onSkipBtnTapped:(id)sender;


@property (strong, nonatomic) IBOutlet UILabel *minutesLabel;

@end
