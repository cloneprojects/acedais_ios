//
//  ChatVC.m
//  ZoeChat
//
//  Created by Pyramidions Solution on 31/12/16.
//  Copyright © 2016 Pyramidions Solution. All rights reserved.
//

#import "ChatVC.h"
#import "ChatCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SingleChatVC.h"
#import "Constants.h"
#import "DBClass.h"
#import "AppDelegate.h"
#import "LockViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "ForgotPasswordVC.h"
#import "MainTabBarVC.h"
#import "AFNHelper.h"

@interface ChatVC ()<UITextFieldDelegate>
{
    NSMutableArray *arrChats, *arrForTableView;
    UIView *ViewSearch;
    NSDictionary *dictToSendSingleChat;
    DBClass *db_class;
    AppDelegate *appdel;
    NSString *ChatRoomId;
    NSInteger getIndexvalue;
    NSString *isLockStr;
    
    NSString *getgroupId;
    NSString *getchatRoomType;

    NSMutableArray *pinArrayList;
    NSMutableArray *ArrayList;
    
    
    NSMutableArray *searchArray;
    NSString *searchTextString;
    BOOL isFilter;
    
    NSIndexPath *deleteChatIndexPath;
    UIAlertController *actionSheet;
}
@end

@implementation ChatVC

- (void)viewDidLoad {
    [super viewDidLoad];
    appdel = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    self.PasswordView.hidden=YES;
    _submitBtn.layer.cornerRadius = 5;
    _submitBtn.clipsToBounds = YES;
    [self.PasswordView.layer setCornerRadius:5.0f];
    [self.PasswordView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.PasswordView.layer setBorderWidth:0.2f];
    self.UnlockPasswordView.hidden=YES;
    _UnlockSubmitBtn.layer.cornerRadius = 5;
    _UnlockSubmitBtn.clipsToBounds = YES;
    [self.UnlockPasswordView.layer setCornerRadius:5.0f];
    [self.UnlockPasswordView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.UnlockPasswordView.layer setBorderWidth:0.2f];
    
        [self.navigationController.navigationBar setTranslucent:NO];
     [self setFontFamily:FONT_NORMAL forView:self.view andSubViews:YES];
    arrChats=[[NSMutableArray alloc]init];
    arrForTableView=[[NSMutableArray alloc]init];
    db_class=[[DBClass alloc]init];
    self.title = @"Chats";
    _tableChat.allowsMultipleSelectionDuringEditing = NO;
    _tableChat.delegate=self;
    _tableChat.dataSource=self;
    self.tabBarItem.image = [[UIImage imageNamed:@"Chat50.png"]
                             imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchView) name:@"Show_Search_View" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshChats) name:@"Refresh_Chats" object:nil];
    isFilter=NO;
    
    
    
    
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        [[UIApplication sharedApplication] keyWindow].tintColor = color;
        self.navigationController.navigationBar.translucent = NO;
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
            [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : color }
                                                     forState:UIControlStateSelected];

        [[UITabBar appearance] setSelectedImageTintColor:color];
        self.submitBtn.backgroundColor=color;
        self.UnlockSubmitBtn.backgroundColor=color;

        
        
        
    }
    else
    {
        [[UIApplication sharedApplication] keyWindow].tintColor = [UIColor whiteColor];
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
            [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0] }
                                                     forState:UIControlStateSelected];
        [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0]];
        self.submitBtn.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.UnlockSubmitBtn.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        
       


    }

   
    
}
-(void)searchView
{
    // [self.tableView reloadData];
    
    ViewSearch=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.navigationController.navigationBar.frame.size.height)];
    ViewSearch.backgroundColor=[UIColor whiteColor];
    ViewSearch.tag=111;
    [self.navigationController.navigationBar addSubview:ViewSearch];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 40, ViewSearch.frame.size.height)];
    [btnClose addTarget:self action:@selector(onClose) forControlEvents:UIControlEventTouchDown];
    [ViewSearch addSubview:btnClose];
    
    UIImageView *arrowImg=[[UIImageView alloc]initWithFrame:CGRectMake(10, (self.navigationController.navigationBar.frame.size.height/2)-10, 20, 20)];
    arrowImg.image=[UIImage imageNamed:@"BackToClose.png"];
    [ViewSearch addSubview:arrowImg];
    
    
    UITextField  *txtSearch=[[UITextField alloc]initWithFrame:CGRectMake(45, 0, self.view.frame.size.width-40, self.navigationController.navigationBar.frame.size.height)];
    txtSearch.clearButtonMode=UITextFieldViewModeWhileEditing;
    txtSearch.placeholder=@"Search...";
    txtSearch.delegate=self;
    [txtSearch becomeFirstResponder];
    [ViewSearch addSubview:txtSearch];
}

-(void)onClose
{
    //[ViewSearch removeFromSuperview];
    //[ViewSearch setHidden:YES];
//    [[self.navigationController.view viewWithTag:111] removeFromSuperview];
    for (UIView *view in self.navigationController.navigationBar.subviews) {
        if (view.tag != 0) {
            [view removeFromSuperview];
        }
    }
    [arrForTableView removeAllObjects];
    arrForTableView=[NSMutableArray arrayWithArray:arrChats];
    [_tableChat reloadData];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    isFilter=NO;

    [self refreshChats];
 //   [self getChatMessages];
    
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
-(void)viewWillDisappear:(BOOL)animated
{
    //[ViewSearch removeFromSuperview];
    //[ViewSearch setHidden:YES];
//     [[self.navigationController.view viewWithTag:111] removeFromSuperview];
    for (UIView *view in self.navigationController.navigationBar.subviews) {
        if (view.tag != 0) {
            [view removeFromSuperview];
        }
    }
}

-(void)refreshChats
{
    
    pinArrayList = [[NSMutableArray alloc]init];
    ArrayList = [[NSMutableArray alloc]init];
    
    arrChats=[[NSMutableArray alloc]init];
    arrForTableView=[[NSMutableArray alloc]init];

    
    arrChats=[db_class getChats];
    
    if (arrChats.count!=0)
    {
        for (int i=0; i<arrChats.count; i++)
        {
            NSMutableDictionary *dictSing=[arrChats objectAtIndex:i];
            
            NSDictionary *dictInfo =[db_class getUserInfo:[dictSing valueForKey:@"chatRoomId"]];
            
            NSLog(@"RefreshChats = %@",dictInfo);
            
            NSString *strName=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"name"]];
            NSString *strImage=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"image"]];
            [dictSing setObject:strName forKey:@"name"];
            [dictSing setObject:strImage forKey:@"image"];
            NSLog(@"%@",dictSing);
            [arrChats replaceObjectAtIndex:i withObject:dictSing];
        }
        NSLog(@"RefreshChats = %@",arrChats);
    }
    
    if (arrChats.count!=0)
    {
        for (int i=0; i<arrChats.count; i++)
        {
            NSDictionary *dictSing=[arrChats objectAtIndex:i];
            
            NSString *isPin=[NSString stringWithFormat:@"%@",[dictSing valueForKey:@"isPin"]];
            
            
            
            
            
            if ([isPin isEqualToString:@"1"])
            {
                [pinArrayList addObject:dictSing];
            }
            else
            {
                [ArrayList addObject:dictSing];
            }
            
            /*
            
            
            NSDictionary *dictInfo =[db_class getUserInfo:[dictSing valueForKey:@"chatRoomId"]];
            
            NSLog(@"RefreshChats = %@",dictInfo);
            
            NSString *strName=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"name"]];
            NSString *strImage=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"image"]];
            [dictSing setObject:strName forKey:@"name"];
            [dictSing setObject:strImage forKey:@"image"];
            NSLog(@"%@",dictSing);
            [arrChats replaceObjectAtIndex:i withObject:dictSing];*/
        }
         NSLog(@"RefreshChats = %@",arrChats);
    }
    
    
    NSLog(@"pinArrayList = %@",pinArrayList);
    NSLog(@"RefreshChats = %@",arrChats);
    NSLog(@"ArrayList = %@",ArrayList);
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"dateTime" ascending:NO];
    
    pinArrayList = [pinArrayList sortedArrayUsingDescriptors:@[sort]];
    
    ArrayList = [ArrayList sortedArrayUsingDescriptors:@[sort]];
    
    
//    arrChats=[arrChats sortedArrayUsingDescriptors:@[sort]];
    
/*    arrForTableView=[NSMutableArray arrayWithArray:pinArrayList];
    
    arrForTableView=[NSMutableArray arrayWithArray:ArrayList];
*/
    
    [arrForTableView addObjectsFromArray:pinArrayList];
    [arrForTableView addObjectsFromArray:ArrayList];

    
    
    NSLog(@"arrForTableView = %@",arrForTableView);

    
    
    [_tableChat reloadData];
}
-(long long)convertDateToMilliseconds:(NSDate *)date
{
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    long long milliseconds=timeInSeconds*1000;
    return milliseconds;
}
-(NSDate *)convertMillisecondsToDate:(long long)milliSeconds
{
    NSDate *dat = [NSDate dateWithTimeIntervalSince1970:(milliSeconds / 1000.0)];
    return dat;
}

#pragma mark -
#pragma mark - TableView Delagate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  60;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if(isFilter)
    {
        return [searchArray count];
    }
    else
       return arrForTableView.count;
        
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ChatCell *cell=(ChatCell *)[tableView dequeueReusableCellWithIdentifier:@"ChatCell"];
    if (cell==nil) {
        cell=[[ChatCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ChatCell"];
    }
    NSString *InitialStr;
    UIColor *bgColor;
    
    if(isFilter)
    {
        
        
        
        NSDictionary *dictArt=[arrForTableView objectAtIndex:indexPath.row];
        NSLog(@"%@",dictArt);
        [cell.lblImg setHidden:YES];

        getchatRoomType=[dictArt valueForKey:@"chatRoomType"];
        if ([[dictArt valueForKey:@"chatRoomType"] isEqualToString:@"0"])

        {
            if (![[dictArt valueForKey:@"name"] isEqualToString:@"(null)"])
            {
                cell.ChatName.text=[[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"name"]]capitalizedString];
                InitialStr=[cell.ChatName.text substringToIndex:1];
                if ([InitialStr isEqualToString:@"A"])
                {
                    bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"B"])
                {
                    bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"C"])
                {
                    bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"D"])
                {
                    bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"E"])
                {
                    bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"F"])
                {
                    bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"G"])
                {
                    bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"H"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"I"])
                {
                    bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"J"])
                {
                    bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"K"])
                {
                    bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"L"])
                {
                    bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"M"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"N"])
                {
                    bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"O"])
                {
                    bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"P"])
                {
                    bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"Q"])
                {
                    bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"R"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"S"])
                {
                    bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"T"])
                {
                    bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"U"])
                {
                    bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"V"])
                {
                    bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"W"])
                {
                    bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"X"])
                {
                    bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"Y"])
                {
                    bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"Z"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
                }
                
            }
            else
            {
                cell.ChatName.text=[NSString stringWithFormat:@"+%@",[dictArt valueForKey:@"chatRoomId"]];
                InitialStr=[cell.ChatName.text substringToIndex:1];
                if ([InitialStr isEqualToString:@"0"])
                {
                    bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"1"])
                {
                    bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"2"])
                {
                    bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"3"])
                {
                    bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"4"])
                {
                    bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"5"])
                {
                    bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"6"])
                {
                    bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"7"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"8"])
                {
                    bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"9"])
                {
                    bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
                }
                
            }
            NSString *strImageURL1=[dictArt valueForKey:@"image"];
            if (![strImageURL1 isEqualToString:@"(null)"])
            {
                if (![strImageURL1 isEqualToString:@""])
                {
                    
                    [cell.ChatImage sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
                    
                    
                }
            }
            else
            {
                [cell.lblImg setHidden:NO];
                
                [cell.ChatImage sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
                cell.lblImg.text=InitialStr;
                cell.lblImg.backgroundColor=bgColor;
            }
        }
        
        cell.ChatImage.layer.cornerRadius=cell.ChatImage.frame.size.height/2;
        cell.ChatImage.clipsToBounds=YES;
        cell.lblImg.layer.cornerRadius=cell.ChatImage.frame.size.height/2;
        cell.lblImg.clipsToBounds=YES;
       
        
        if ([[dictArt valueForKey:@"chatRoomType"] isEqualToString:@"1"])
        {
            cell.ChatName.text=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"groupName"]];
            [cell.ChatImage sd_setImageWithURL:[NSURL URLWithString:[dictArt valueForKey:@"groupImage"]] placeholderImage:[UIImage imageNamed:@"grp.png"]options:SDWebImageRefreshCached];
        }
        else if ([[dictArt valueForKey:@"chatRoomType"] isEqualToString:@"2"])
        {
            cell.ChatName.text=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"groupName"]];
            [cell.ChatImage sd_setImageWithURL:[NSURL URLWithString:[dictArt valueForKey:@"groupImage"]] placeholderImage:[UIImage imageNamed:@"megaphone.png"]options:SDWebImageRefreshCached];
        }
        else
        {
//            cell.ChatImage.image=[UIImage imageNamed:@"userImage.png"];
            
        }
        NSDate *dat=[self convertMillisecondsToDate:[[dictArt valueForKey:@"lastMessageTime"]longLongValue]];
        
        NSCalendar* calendar = [NSCalendar currentCalendar];
        BOOL isToday = [calendar isDateInToday:dat];
        BOOL isYesterday = [calendar isDateInYesterday:dat];
        
        
        NSDateFormatter *dFormatterTime = [[NSDateFormatter alloc] init];
        [dFormatterTime setDateFormat:@"hh:mm a"];
        NSString *strTime = [dFormatterTime stringFromDate:dat];
        if (isToday)
        {
            cell.ChatTime.text = strTime;
        }
        else if (isYesterday)
        {
            cell.ChatTime.text = @"Yesterday";
        }
        else
        {
            NSDate *dat=[self convertMillisecondsToDate:[[dictArt valueForKey:@"lastMessageTime"]longLongValue]];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd/MM/yyyy"];
            NSString *strDate=[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:dat]];
            cell.ChatTime.text=strDate;
        }
        
        
        cell.ChatUnreadCount.text=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"unreadCount"]];
        cell.ChatUnreadCount.layer.cornerRadius=cell.ChatUnreadCount.frame.size.height/2;
        cell.ChatUnreadCount.clipsToBounds=YES;
        int nUnReadCount=[[dictArt valueForKey:@"unreadCount"] intValue];
        if (nUnReadCount==0)
        {
            cell.ChatUnreadCount.hidden=YES;
            cell.ChatTime.textColor=[UIColor lightGrayColor];
        }
        else
        {
            cell.ChatUnreadCount.hidden=NO;
            
            NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
            UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
            if (color != nil) {
                cell.ChatTime.textColor=color;
                
                
            }
            else
            {
                cell.ChatTime.textColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
            }

            
        }
        
        NSString *strcheckLock=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"isLock"]];
        if ([strcheckLock isEqualToString:@"1"])
        {
            [cell.ChatlockImg setImage:[UIImage imageNamed:@"lock.png"]];
        }
        else
        {
            [cell.ChatlockImg setImage:[UIImage imageNamed:@""]];
        }
        
        
        NSString *strPinLock=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"isPin"]];
        if ([strPinLock isEqualToString:@"1"])
        {
            
            cell.pinChat.hidden = false;
            
        }
        else
        {
            cell.pinChat.hidden = true;
        }
        
        
        
        
        NSString *strMsgStatus=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"lastMessageStatus"]];
        NSString *strcheckType=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"chatRoomType"]];
        
        if ([strcheckType  isEqual: @"0"])
        {
            if ([strMsgStatus isEqualToString:@"sending"])
                cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_sending"];
            else if ([strMsgStatus isEqualToString:@"sent"])
                cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_sent"];
            else if ([strMsgStatus isEqualToString:@"delivered"])
                cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_notified"];
            else if ([strMsgStatus isEqualToString:@"read"])
                cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_read"];
            if ([strMsgStatus isEqualToString:@"failed"])
                cell.ImgStatusIcon.image = nil;
            
        }
        else if ([strcheckType  isEqual: @"2"])
        {
            if ([strMsgStatus isEqualToString:@"sending"])
                cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_sending"];
            else if ([strMsgStatus isEqualToString:@"sent"])
                cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_sent"];
            else if ([strMsgStatus isEqualToString:@"delivered"])
                cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_notified"];
            else if ([strMsgStatus isEqualToString:@"read"])
                cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_read"];
            if ([strMsgStatus isEqualToString:@"failed"])
                cell.ImgStatusIcon.image = nil;
            
        }
        else if ([strcheckType  isEqual: @"1"])
        {
            if ([strMsgStatus isEqualToString:@"sending"])
                cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_sending"];
            else if ([strMsgStatus isEqualToString:@"sent"])
                cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_sent"];
            else if ([strMsgStatus isEqualToString:@"delivered"])
                cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_sent"];
            else if ([strMsgStatus isEqualToString:@"read"])
                cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_sent"];
            
        }
        
        NSString *strMsgSender=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"sender"]];
        
        
        
        if ([[dictArt valueForKey:@"lastMessageType"]isEqualToString:@"image"])
        {
            cell.imgVwForMedia.hidden=NO;
            cell.imgVwForMedia.image= [UIImage imageNamed:@"Camera"];
            if ([strMsgSender isEqualToString:@"1"])
            {
                cell.ImgStatusIcon.hidden = YES;
                
                cell.imgVwForMedia.frame=CGRectMake(cell.ChatName.frame.origin.x, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
                
                
            }
            else
            {
                cell.ImgStatusIcon.hidden = NO;
                
                if ([strMsgStatus isEqualToString:@"delivered"] || [strMsgStatus isEqualToString:@"read"])
                {
                    cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 14, 10);
                    
                }
                else
                {
                    cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 10, 10);
                }
                
                cell.imgVwForMedia.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x+cell.ImgStatusIcon.frame.size.width+5, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
            }
            
            
            cell.ChatLastMessage.text=@"Photo";
            
            
            NSInteger xPos=cell.imgVwForMedia.frame.origin.x+20;
            cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width, cell.ChatLastMessage.frame.size.height);
        }
        else if ([[dictArt valueForKey:@"lastMessageType"]isEqualToString:@"location"])
        {
            cell.imgVwForMedia.hidden=NO;
            cell.imgVwForMedia.image= [UIImage imageNamed:@"Location"];
            if ([strMsgSender isEqualToString:@"1"])
            {
                cell.ImgStatusIcon.hidden = YES;
                
                cell.imgVwForMedia.frame=CGRectMake(cell.ChatName.frame.origin.x, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
                
            }
            else
            {
                cell.ImgStatusIcon.hidden = NO;
                
                if ([strMsgStatus isEqualToString:@"delivered"] || [strMsgStatus isEqualToString:@"read"])
                {
                    cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 14, 10);
                    
                }
                else
                {
                    cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 10, 10);
                }
                
                cell.imgVwForMedia.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x+cell.ImgStatusIcon.frame.size.width+5, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
            }
            
            
            cell.ChatLastMessage.text=@"Location";
            
            
            NSInteger xPos=cell.imgVwForMedia.frame.origin.x+20;
            cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width, cell.ChatLastMessage.frame.size.height);
            
            
        }
        else if ([[dictArt valueForKey:@"lastMessageType"]isEqualToString:@"video"])
        {
            cell.imgVwForMedia.hidden=NO;
            cell.imgVwForMedia.image= [UIImage imageNamed:@"CameraVideo"];
            if ([strMsgSender isEqualToString:@"1"])
            {
                cell.ImgStatusIcon.hidden = YES;
                
                cell.imgVwForMedia.frame=CGRectMake(cell.ChatName.frame.origin.x, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
                
            }
            else
            {
                cell.ImgStatusIcon.hidden = NO;
                
                if ([strMsgStatus isEqualToString:@"delivered"] || [strMsgStatus isEqualToString:@"read"])
                {
                    cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 14, 10);
                    
                }
                else
                {
                    cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 10, 10);
                }
                
                cell.imgVwForMedia.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x+cell.ImgStatusIcon.frame.size.width+5, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
            }
            
            
            cell.ChatLastMessage.text=@"Video";
            
            
            NSInteger xPos=cell.imgVwForMedia.frame.origin.x+20;
            cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width, cell.ChatLastMessage.frame.size.height);
            
            
        }
        else if ([[dictArt valueForKey:@"lastMessageType"]isEqualToString:@"contact"])
        {
            cell.imgVwForMedia.hidden=NO;
            cell.imgVwForMedia.image= [UIImage imageNamed:@"Contact"];
            if ([strMsgSender isEqualToString:@"1"])
            {
                cell.ImgStatusIcon.hidden = YES;
                
                cell.imgVwForMedia.frame=CGRectMake(cell.ChatName.frame.origin.x, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
                
            }
            else
            {
                cell.ImgStatusIcon.hidden = NO;
                
                if ([strMsgStatus isEqualToString:@"delivered"] || [strMsgStatus isEqualToString:@"read"])
                {
                    cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 14, 10);
                    
                }
                else
                {
                    cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 10, 10);
                }
                
                cell.imgVwForMedia.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x+cell.ImgStatusIcon.frame.size.width+5, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
            }
            
            
            cell.ChatLastMessage.text=@"Contact";
            
            
            NSInteger xPos=cell.imgVwForMedia.frame.origin.x+20;
            cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width, cell.ChatLastMessage.frame.size.height);
            
            
        }
        else if ([[dictArt valueForKey:@"lastMessageType"]isEqualToString:@"audio"])
        {
            cell.imgVwForMedia.hidden=NO;
            cell.imgVwForMedia.image= [UIImage imageNamed:@"music.png"];
            if ([strMsgSender isEqualToString:@"1"])
            {
                cell.ImgStatusIcon.hidden = YES;
                
                cell.imgVwForMedia.frame=CGRectMake(cell.ChatName.frame.origin.x, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
                
            }
            else
            {
                cell.ImgStatusIcon.hidden = NO;
                
                if ([strMsgStatus isEqualToString:@"delivered"] || [strMsgStatus isEqualToString:@"read"])
                {
                    cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 14, 10);
                    
                }
                else
                {
                    cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 10, 10);
                }
                
                cell.imgVwForMedia.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x+cell.ImgStatusIcon.frame.size.width+5, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
            }
            
            
            cell.ChatLastMessage.text=@"Audio";
            
            
            NSInteger xPos=cell.imgVwForMedia.frame.origin.x+20;
            cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width, cell.ChatLastMessage.frame.size.height);
            
            
        }
        else if ([[dictArt valueForKey:@"lastMessageType"]isEqualToString:@"document"])
            
        {
            cell.imgVwForMedia.hidden=NO;
            cell.imgVwForMedia.image= [UIImage imageNamed:@"forms"];
            if ([strMsgSender isEqualToString:@"1"])
            {
                cell.ImgStatusIcon.hidden = YES;
                
                cell.imgVwForMedia.frame=CGRectMake(cell.ChatName.frame.origin.x, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
                
            }
            else
            {
                cell.ImgStatusIcon.hidden = NO;
                
                if ([strMsgStatus isEqualToString:@"delivered"] || [strMsgStatus isEqualToString:@"read"])
                {
                    cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 14, 10);
                    
                }
                else
                {
                    cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 10, 10);
                }
                
                cell.imgVwForMedia.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x+cell.ImgStatusIcon.frame.size.width+5, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
            }
            cell.ChatLastMessage.text=@"Document";
            NSInteger xPos=cell.imgVwForMedia.frame.origin.x+20;
            cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width, cell.ChatLastMessage.frame.size.height);
            
        }
        else if ([[dictArt valueForKey:@"lastMessageType"]isEqualToString:@"sticker"])

        {
            cell.imgVwForMedia.hidden=NO;
            //cell.imgVwForMedia.image= [UIImage imageNamed:@"forms"];
            if ([strMsgSender isEqualToString:@"1"])
            {
                cell.ImgStatusIcon.hidden = YES;
                
                cell.imgVwForMedia.frame=CGRectMake(cell.ChatName.frame.origin.x, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
                
            }
            else
            {
                cell.ImgStatusIcon.hidden = NO;
                
                if ([strMsgStatus isEqualToString:@"delivered"] || [strMsgStatus isEqualToString:@"read"])
                {
                    cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 14, 10);
                    
                }
                else
                {
                    cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 10, 10);
                }
                
                cell.imgVwForMedia.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x+cell.ImgStatusIcon.frame.size.width+5, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
            }
            cell.ChatLastMessage.text=@"Sticker";
            NSInteger xPos=cell.imgVwForMedia.frame.origin.x+20;
            cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width, cell.ChatLastMessage.frame.size.height);
            
        }
        else
        {
            cell.imgVwForMedia.hidden=YES;
            
            if ([strMsgSender isEqualToString:@"1"])
            {
                cell.ImgStatusIcon.hidden = YES;
                NSInteger xPos=cell.ChatName.frame.origin.x;
                
                cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width, cell.ChatLastMessage.frame.size.height);
                
            }
            else
            {
                cell.ImgStatusIcon.hidden = NO;
                
                if ([strMsgStatus isEqualToString:@"delivered"] || [strMsgStatus isEqualToString:@"read"])
                {
                    NSInteger xPos=cell.ImgStatusIcon.frame.origin.x+19;
                    cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width-3, cell.ChatLastMessage.frame.size.height);
                    cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 14, 10);
                    
                }
                else
                {
                    NSInteger xPos=cell.ImgStatusIcon.frame.origin.x+14;
                    cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width, cell.ChatLastMessage.frame.size.height);
                    cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 10, 10);
                }
            }
            cell.ChatLastMessage.text=[NSString stringWithFormat:@"%@", [dictArt valueForKey:@"lastMessage"]];
            
        }
        
        
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        ChatRoomId=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"chatRoomId"]];
        cell.ChatRoomId.text=ChatRoomId;
    }
    else
    {
    NSDictionary *dictArt=[arrForTableView objectAtIndex:indexPath.row];
    NSLog(@"%@",dictArt);
         [cell.lblImg setHidden:YES];
        getchatRoomType=[dictArt valueForKey:@"chatRoomType"];
        if ([[dictArt valueForKey:@"chatRoomType"] isEqualToString:@"0"])
        {
        if (![[dictArt valueForKey:@"name"] isEqualToString:@"(null)"])
        {
            cell.ChatName.text=[[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"name"]]capitalizedString];
            InitialStr=[cell.ChatName.text substringToIndex:1];
            if ([InitialStr isEqualToString:@"A"])
            {
                bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"B"])
            {
                bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"C"])
            {
                bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"D"])
            {
                bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"E"])
            {
                bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"F"])
            {
                bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"G"])
            {
                bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"H"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"I"])
            {
                bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"J"])
            {
                bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"K"])
            {
                bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"L"])
            {
                bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"M"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"N"])
            {
                bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"O"])
            {
                bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"P"])
            {
                bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"Q"])
            {
                bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"R"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"S"])
            {
                bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"T"])
            {
                bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"U"])
            {
                bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"V"])
            {
                bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"W"])
            {
                bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"X"])
            {
                bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"Y"])
            {
                bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"Z"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
            }

        }
        else
        {
            cell.ChatName.text=[NSString stringWithFormat:@"+%@",[dictArt valueForKey:@"chatRoomId"]];
            InitialStr=[cell.ChatName.text substringToIndex:1];
            if ([InitialStr isEqualToString:@"0"])
            {
                bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"1"])
            {
                bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"2"])
            {
                bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"3"])
            {
                bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"4"])
            {
                bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"5"])
            {
                bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"6"])
            {
                bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"7"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"8"])
            {
                bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"9"])
            {
                bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
            }

        }
            NSString *strImageURL1=[dictArt valueForKey:@"image"];
            if (![strImageURL1 isEqualToString:@"(null)"])
            {
                if (![strImageURL1 isEqualToString:@""])
                {
                    
                    [cell.ChatImage sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
                    
                    
                }
            }
            else
            {
                [cell.lblImg setHidden:NO];
                
                [cell.ChatImage sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
                cell.lblImg.text=InitialStr;
                cell.lblImg.backgroundColor=bgColor;
            }
        }
       
    
    cell.ChatImage.layer.cornerRadius=cell.ChatImage.frame.size.height/2;
    cell.ChatImage.clipsToBounds=YES;
    cell.lblImg.layer.cornerRadius=cell.ChatImage.frame.size.height/2;
    cell.lblImg.clipsToBounds=YES;

    
    
    
    if ([[dictArt valueForKey:@"chatRoomType"] isEqualToString:@"1"])
    {
         cell.ChatName.text=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"groupName"]];
        [cell.ChatImage sd_setImageWithURL:[NSURL URLWithString:[dictArt valueForKey:@"groupImage"]] placeholderImage:[UIImage imageNamed:@"grp.png"]options:SDWebImageRefreshCached];
    }
    else if ([[dictArt valueForKey:@"chatRoomType"] isEqualToString:@"2"])
    {
        cell.ChatName.text=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"groupName"]];
        [cell.ChatImage sd_setImageWithURL:[NSURL URLWithString:[dictArt valueForKey:@"groupImage"]] placeholderImage:[UIImage imageNamed:@"megaphone.png"]options:SDWebImageRefreshCached];
    }
    else
    {
//        cell.ChatImage.image=[UIImage imageNamed:@"userImage.png"];
        
    }
    NSDate *dat=[self convertMillisecondsToDate:[[dictArt valueForKey:@"lastMessageTime"]longLongValue]];
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    BOOL isToday = [calendar isDateInToday:dat];
    BOOL isYesterday = [calendar isDateInYesterday:dat];
    
    
    NSDateFormatter *dFormatterTime = [[NSDateFormatter alloc] init];
    [dFormatterTime setDateFormat:@"hh:mm a"];
    NSString *strTime = [dFormatterTime stringFromDate:dat];
    if (isToday)
    {
        cell.ChatTime.text = strTime;
    }
    else if (isYesterday)
    {
        cell.ChatTime.text = @"Yesterday";
    }
    else
    {
        NSDate *dat=[self convertMillisecondsToDate:[[dictArt valueForKey:@"lastMessageTime"]longLongValue]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        NSString *strDate=[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:dat]];
        cell.ChatTime.text=strDate;
    }

    
    cell.ChatUnreadCount.text=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"unreadCount"]];
    cell.ChatUnreadCount.layer.cornerRadius=cell.ChatUnreadCount.frame.size.height/2;
    cell.ChatUnreadCount.clipsToBounds=YES;
    int nUnReadCount=[[dictArt valueForKey:@"unreadCount"] intValue];
    if (nUnReadCount==0)
    {
        cell.ChatUnreadCount.hidden=YES;
        cell.ChatTime.textColor=[UIColor lightGrayColor];
    }
    else
    {
         cell.ChatUnreadCount.hidden=NO;
        NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
        UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
        if (color != nil) {
            cell.ChatTime.textColor=color;
            cell.ChatUnreadCount.backgroundColor=color;
            
            
        }
        else
        {
            cell.ChatTime.textColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
            cell.ChatUnreadCount.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];

        }
    }
   
    NSString *strcheckLock=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"isLock"]];
    if ([strcheckLock isEqualToString:@"1"])
    {
        [cell.ChatlockImg setImage:[UIImage imageNamed:@"lock.png"]];
    }
    else
    {
        [cell.ChatlockImg setImage:[UIImage imageNamed:@""]];
    }
        
   
        
        NSString *strPinLock=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"isPin"]];
        if ([strPinLock isEqualToString:@"1"])
        {
            
            cell.pinChat.hidden = false;
            
        }
        else
        {
            cell.pinChat.hidden = true;
        }
        
        
        
        
    
    NSString *strMsgStatus=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"lastMessageStatus"]];
    NSString *strcheckType=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"chatRoomType"]];
    
    if ([strcheckType  isEqual: @"0"])
    {
        if ([strMsgStatus isEqualToString:@"sending"])
            cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_sending"];
        else if ([strMsgStatus isEqualToString:@"sent"])
            cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_sent"];
        else if ([strMsgStatus isEqualToString:@"delivered"])
            cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_notified"];
        else if ([strMsgStatus isEqualToString:@"read"])
            cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_read"];
        if ([strMsgStatus isEqualToString:@"failed"])
            cell.ImgStatusIcon.image = nil;
    }
    else if ([strcheckType  isEqual: @"2"])
    {
//        if ([strMsgStatus isEqualToString:@"sending"])
//            cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_sending"];
//        else if ([strMsgStatus isEqualToString:@"sent"])
            cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_sent"];
//        else if ([strMsgStatus isEqualToString:@"delivered"])
//            cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_notified"];
//        else if ([strMsgStatus isEqualToString:@"read"])
//            cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_read"];
//        if ([strMsgStatus isEqualToString:@"failed"])
//            cell.ImgStatusIcon.image = nil;
        
    }
    else if ([strcheckType  isEqual: @"1"])
    {
        if ([strMsgStatus isEqualToString:@"sending"])
            cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_sending"];
        else if ([strMsgStatus isEqualToString:@"sent"])
            cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_sent"];
        else if ([strMsgStatus isEqualToString:@"delivered"])
            cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_sent"];
        else if ([strMsgStatus isEqualToString:@"read"])
            cell.ImgStatusIcon.image = [UIImage imageNamed:@"status_sent"];

    }
    
    NSString *strMsgSender=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"sender"]];
    
   
   
    if ([[dictArt valueForKey:@"lastMessageType"]isEqualToString:@"image"])
    {
        cell.imgVwForMedia.hidden=NO;
        cell.imgVwForMedia.image= [UIImage imageNamed:@"Camera"];
            if ([strMsgSender isEqualToString:@"1"])
            {
                cell.ImgStatusIcon.hidden = YES;
                
                cell.imgVwForMedia.frame=CGRectMake(cell.ChatName.frame.origin.x, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
               
                
            }
            else
            {
                cell.ImgStatusIcon.hidden = NO;
                
                if ([strMsgStatus isEqualToString:@"delivered"] || [strMsgStatus isEqualToString:@"read"])
                {
                    cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 14, 10);
                    
                }
                else
                {
                    cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 10, 10);
                }
                
                cell.imgVwForMedia.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x+cell.ImgStatusIcon.frame.size.width+5, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
            }
       
        
         cell.ChatLastMessage.text=@"Photo";
      
        
        NSInteger xPos=cell.imgVwForMedia.frame.origin.x+20;
        cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width, cell.ChatLastMessage.frame.size.height);
    }
    else if ([[dictArt valueForKey:@"lastMessageType"]isEqualToString:@"sticker"])
        
    {
        cell.imgVwForMedia.hidden=NO;
        //cell.imgVwForMedia.image= [UIImage imageNamed:@"forms"];
        if ([strMsgSender isEqualToString:@"1"])
        {
            cell.ImgStatusIcon.hidden = YES;
            
            cell.imgVwForMedia.frame=CGRectMake(cell.ChatName.frame.origin.x, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
            
        }
        else
        {
            cell.ImgStatusIcon.hidden = NO;
            
            if ([strMsgStatus isEqualToString:@"delivered"] || [strMsgStatus isEqualToString:@"read"])
            {
                cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 14, 10);
                
            }
            else
            {
                cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 10, 10);
            }
            
            cell.imgVwForMedia.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x+cell.ImgStatusIcon.frame.size.width+5, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
        }
        cell.ChatLastMessage.text=@"Sticker";
        NSInteger xPos=cell.imgVwForMedia.frame.origin.x+20;
        cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width, cell.ChatLastMessage.frame.size.height);
        
    }

    else if ([[dictArt valueForKey:@"lastMessageType"]isEqualToString:@"location"])
    {
        cell.imgVwForMedia.hidden=NO;
         cell.imgVwForMedia.image= [UIImage imageNamed:@"Location"];
        if ([strMsgSender isEqualToString:@"1"])
        {
            cell.ImgStatusIcon.hidden = YES;
            
            cell.imgVwForMedia.frame=CGRectMake(cell.ChatName.frame.origin.x, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
            
        }
        else
        {
            cell.ImgStatusIcon.hidden = NO;
            
            if ([strMsgStatus isEqualToString:@"delivered"] || [strMsgStatus isEqualToString:@"read"])
            {
                cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 14, 10);
                
            }
            else
            {
                cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 10, 10);
            }
            
            cell.imgVwForMedia.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x+cell.ImgStatusIcon.frame.size.width+5, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
        }
        
        
        cell.ChatLastMessage.text=@"Location";
        
        
        NSInteger xPos=cell.imgVwForMedia.frame.origin.x+20;
        cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width, cell.ChatLastMessage.frame.size.height);
        
        
    }
    else if ([[dictArt valueForKey:@"lastMessageType"]isEqualToString:@"video"])
    {
        cell.imgVwForMedia.hidden=NO;
        cell.imgVwForMedia.image= [UIImage imageNamed:@"CameraVideo"];
        if ([strMsgSender isEqualToString:@"1"])
        {
            cell.ImgStatusIcon.hidden = YES;
            
            cell.imgVwForMedia.frame=CGRectMake(cell.ChatName.frame.origin.x, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
            
        }
        else
        {
            cell.ImgStatusIcon.hidden = NO;
            
            if ([strMsgStatus isEqualToString:@"delivered"] || [strMsgStatus isEqualToString:@"read"])
            {
                cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 14, 10);
                
            }
            else
            {
                cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 10, 10);
            }
            
            cell.imgVwForMedia.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x+cell.ImgStatusIcon.frame.size.width+5, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
        }
        
        
        cell.ChatLastMessage.text=@"Video";
        
        
        NSInteger xPos=cell.imgVwForMedia.frame.origin.x+20;
        cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width, cell.ChatLastMessage.frame.size.height);
        
        
    }
    else if ([[dictArt valueForKey:@"lastMessageType"]isEqualToString:@"contact"])
    {
        cell.imgVwForMedia.hidden=NO;
        cell.imgVwForMedia.image= [UIImage imageNamed:@"Contact"];
        if ([strMsgSender isEqualToString:@"1"])
        {
            cell.ImgStatusIcon.hidden = YES;
            
            cell.imgVwForMedia.frame=CGRectMake(cell.ChatName.frame.origin.x, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
            
        }
        else
        {
            cell.ImgStatusIcon.hidden = NO;
            
            if ([strMsgStatus isEqualToString:@"delivered"] || [strMsgStatus isEqualToString:@"read"])
            {
                cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 14, 10);
                
            }
            else
            {
                cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 10, 10);
            }
            
            cell.imgVwForMedia.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x+cell.ImgStatusIcon.frame.size.width+5, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
        }
        
        
        cell.ChatLastMessage.text=@"Contact";
        
        
        NSInteger xPos=cell.imgVwForMedia.frame.origin.x+20;
        cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width, cell.ChatLastMessage.frame.size.height);
        
        
    }
    else if ([[dictArt valueForKey:@"lastMessageType"]isEqualToString:@"audio"])
    {
        cell.imgVwForMedia.hidden=NO;
        cell.imgVwForMedia.image= [UIImage imageNamed:@"music"];
        if ([strMsgSender isEqualToString:@"1"])
        {
            cell.ImgStatusIcon.hidden = YES;
            
            cell.imgVwForMedia.frame=CGRectMake(cell.ChatName.frame.origin.x, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
            
        }
        else
        {
            cell.ImgStatusIcon.hidden = NO;
            
            if ([strMsgStatus isEqualToString:@"delivered"] || [strMsgStatus isEqualToString:@"read"])
            {
                cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 14, 10);
                
            }
            else
            {
                cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 10, 10);
            }
            
            cell.imgVwForMedia.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x+cell.ImgStatusIcon.frame.size.width+5, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
        }
        
        
        cell.ChatLastMessage.text=@"Audio";
        
        
        NSInteger xPos=cell.imgVwForMedia.frame.origin.x+20;
        cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width, cell.ChatLastMessage.frame.size.height);
        
        
    }
    else if ([[dictArt valueForKey:@"lastMessageType"]isEqualToString:@"document"])

    {
        cell.imgVwForMedia.hidden=NO;
        cell.imgVwForMedia.image= [UIImage imageNamed:@"forms"];
        if ([strMsgSender isEqualToString:@"1"])
        {
            cell.ImgStatusIcon.hidden = YES;
            
            cell.imgVwForMedia.frame=CGRectMake(cell.ChatName.frame.origin.x, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
            
        }
        else
        {
            cell.ImgStatusIcon.hidden = NO;
            
            if ([strMsgStatus isEqualToString:@"delivered"] || [strMsgStatus isEqualToString:@"read"])
            {
                cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 14, 10);
                
            }
            else
            {
                cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 10, 10);
            }
            
            cell.imgVwForMedia.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x+cell.ImgStatusIcon.frame.size.width+5, cell.imgVwForMedia.frame.origin.y, cell.imgVwForMedia.frame.size.width, cell.imgVwForMedia.frame.size.height);
        }
        cell.ChatLastMessage.text=@"Document";
        NSInteger xPos=cell.imgVwForMedia.frame.origin.x+20;
        cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width, cell.ChatLastMessage.frame.size.height);

    }

    else
    {
         cell.imgVwForMedia.hidden=YES;
        
        if ([strMsgSender isEqualToString:@"1"])
        {
            cell.ImgStatusIcon.hidden = YES;
            NSInteger xPos=cell.ChatName.frame.origin.x;
            
            cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width, cell.ChatLastMessage.frame.size.height);
            
        }
        else
        {
            cell.ImgStatusIcon.hidden = NO;
            
            if ([strMsgStatus isEqualToString:@"delivered"] || [strMsgStatus isEqualToString:@"read"])
            {
                NSInteger xPos=cell.ImgStatusIcon.frame.origin.x+19;
                cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width-3, cell.ChatLastMessage.frame.size.height);
                cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 14, 10);
                
            }
            else
            {
                NSInteger xPos=cell.ImgStatusIcon.frame.origin.x+14;
                cell.ChatLastMessage.frame = CGRectMake(xPos, cell.ChatLastMessage.frame.origin.y, cell.ChatLastMessage.frame.size.width, cell.ChatLastMessage.frame.size.height);
                cell.ImgStatusIcon.frame=CGRectMake(cell.ImgStatusIcon.frame.origin.x, cell.ImgStatusIcon.frame.origin.y, 10, 10);
            }
        }
        cell.ChatLastMessage.text=[NSString stringWithFormat:@"%@", [dictArt valueForKey:@"lastMessage"]];

    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    ChatRoomId=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"chatRoomId"]];
    cell.ChatRoomId.text=ChatRoomId;
}
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // [ViewSearch removeFromSuperview];
//    [ViewSearch setHidden:YES];
//     [[self.navigationController.view viewWithTag:111] removeFromSuperview];
    for (UIView *view in self.navigationController.navigationBar.subviews) {
        if (view.tag != 0) {
            [view removeFromSuperview];
        }
    }
    if(isFilter)
    {
        
        dictToSendSingleChat=[searchArray objectAtIndex:indexPath.row];
        getIndexvalue=indexPath.row;
        deleteChatIndexPath=indexPath;
        NSString *strcheckLock=[NSString stringWithFormat:@"%@",[dictToSendSingleChat valueForKey:@"isLock"]];
        ChatRoomId=[NSString stringWithFormat:@"%@",[dictToSendSingleChat valueForKey:@"chatRoomId"]];
        if ([strcheckLock isEqualToString:@"1"])
        {
            [self ShowPasswordView];
            
        }
        else
        {
            [self performSegueWithIdentifier:@"Chat_SingleChat" sender:self];
            
        }
    }
    else
    {
       
        dictToSendSingleChat=[arrForTableView objectAtIndex:indexPath.row];
        getIndexvalue=indexPath.row;
        deleteChatIndexPath=indexPath;

        NSString *strcheckLock=[NSString stringWithFormat:@"%@",[dictToSendSingleChat valueForKey:@"isLock"]];
        ChatRoomId=[NSString stringWithFormat:@"%@",[dictToSendSingleChat valueForKey:@"chatRoomId"]];
        if ([strcheckLock isEqualToString:@"1"])
        {
            [self ShowPasswordView];
            
        }
        else
        {
            [self performSegueWithIdentifier:@"Chat_SingleChat" sender:self];
            
        }

        
    }
    
    
    
   
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}
-(void)ShowPasswordView
{
    UIView *viewForPassword=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+self.navigationController.navigationBar.frame.size.height+50)];
    viewForPassword.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7f];
    viewForPassword.tag=9087650;
    [self.navigationController.view addSubview:viewForPassword];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onRemoveViewAudioVideo:)]; // this is the current problem like a lot of people out there...
    [viewForPassword addGestureRecognizer:tap];
    self.PasswordView.hidden=NO;
    
    [viewForPassword addSubview:self.PasswordView];
}
- (void) onRemoveViewAudioVideo: (UIGestureRecognizer *) gesture
{
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    self.PasswordView.hidden=YES;
    [viewForAudioOrVideo removeFromSuperview];
}
-(void)ShowUnlockPasswordView
{
    UIView *viewForPassword=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+self.navigationController.navigationBar.frame.size.height+50)];
    viewForPassword.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7f];
    viewForPassword.tag=1087650;
    [self.navigationController.view addSubview:viewForPassword];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onRemoveView:)]; // this is the current problem like a lot of people out there...
    [viewForPassword addGestureRecognizer:tap];
    self.UnlockPasswordView.hidden=NO;
    
    [viewForPassword addSubview:self.UnlockPasswordView];
}
- (void) onRemoveView: (UIGestureRecognizer *) gesture
{
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:1087650];
    self.UnlockPasswordView.hidden=YES;
    [viewForAudioOrVideo removeFromSuperview];
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}
-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChatCell *cell = (ChatCell *)[tableView cellForRowAtIndexPath:indexPath];
    ChatRoomId=cell.ChatRoomId.text;
    dictToSendSingleChat=[arrChats objectAtIndex:indexPath.row];
    
    
    
    NSLog(@"dictToSendSingleChat = %@",dictToSendSingleChat);
    
    
    
    getIndexvalue=indexPath.row;
    deleteChatIndexPath=indexPath;

    NSString *isLock=[NSString stringWithFormat:@"%@",[dictToSendSingleChat valueForKey:@"isLock"]];
    UITableViewRowAction *lockAction;
    if ([isLock isEqualToString:@"1"])
    {
        lockAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"RemoveLock" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
            NSLog(@"RemoveLock");
            isLockStr=@"NO";
            [self ShowUnlockPasswordView];
            

         
        }];

    }
    else
    {
        lockAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Lock" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
            NSLog(@"lock");
            ChatCell *cell = (ChatCell *)[tableView cellForRowAtIndexPath:indexPath];
            ChatRoomId=cell.ChatRoomId.text;
            [self CheckRecoveryAPI];
            
        }];
        lockAction.backgroundColor = [UIColor redColor];

    }
    
    
    
    UITableViewRowAction *MoreAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"More"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        
        actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            // Cancel button tappped do nothing.
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Clear Chat" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self ClearChat];

            
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Delete Chat" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self DeleteChat];
            
            
            
        }]];
        
        
        
        NSDictionary *dct;
        
        
        if(isFilter)
        {
           dct = [searchArray objectAtIndex:indexPath.row];
        }
        else
        {
            dct = [arrForTableView objectAtIndex:indexPath.row];
        }
        
        
        NSString *isPin=[NSString stringWithFormat:@"%@",[dct valueForKey:@"isPin"]];

        
        NSLog(@"isPin = %@",isPin);

        if ([isPin isEqualToString:@"1"])
        {
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"UnPin Chat" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                ChatCell *cell = (ChatCell *)[tableView cellForRowAtIndexPath:indexPath];
                ChatRoomId=cell.ChatRoomId.text;
                
                [db_class updatePinChatTable:ChatRoomId isPin:@"0"];
                
                
                [self refreshChats];
                
            }]];
            
        }
        else
        {
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"Pin Chat" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                ChatCell *cell = (ChatCell *)[tableView cellForRowAtIndexPath:indexPath];
                ChatRoomId=cell.ChatRoomId.text;
                
                
                if (pinArrayList.count == 3)
                {
                    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                    
                    // Configure for text only and offset down
                    hud.mode = MBProgressHUDModeText;
                    hud.label.text = @"You can only pin up to 3 chats";
                    hud.margin = 10.f;
                    hud.yOffset = 150.f;
                    hud.removeFromSuperViewOnHide = YES;
                    [hud hideAnimated:YES afterDelay:3];
                }
                else
                {
                    [db_class updatePinChatTable:ChatRoomId isPin:@"1"];
                }
                
                [self refreshChats];

                
            }]];
            
        }
        
        
        
        
        
/*
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Pin Chat" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            ChatCell *cell = (ChatCell *)[tableView cellForRowAtIndexPath:indexPath];
            ChatRoomId=cell.ChatRoomId.text;
            
            [db_class updatePinChatTable:ChatRoomId isPin:@"1"];
            
            
        }]];
        
        */
        
        [self presentViewController:actionSheet animated:YES completion:nil];
        NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
        UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
        if (color != nil) {
            actionSheet.view.tintColor=color;
        }
        else
        {
            
            actionSheet.view.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];

        }

        
    }];
    MoreAction.backgroundColor = [UIColor grayColor];

    return @[lockAction,MoreAction];
    
}




-(void)ClearChat
{
    ChatCell *cell = (ChatCell *)[self.tableChat cellForRowAtIndexPath:deleteChatIndexPath] ;
    NSString *titleString=[NSString stringWithFormat:@"Clear all chat with %@",cell.ChatName.text];
    actionSheet = [UIAlertController alertControllerWithTitle:titleString message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Clear Chat" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        getgroupId=cell.ChatRoomId.text;
        NSArray *arrAllChats=[db_class getOneToOneChatMessages:getgroupId];
        NSString *strGrpID,*strChatRoomType;
        NSLog(@"%@",arrAllChats);
        for (int i=0; i<arrAllChats.count; i++)
        {
            NSDictionary *dict=[arrAllChats objectAtIndex:i];
            strGrpID=[dict valueForKey:@"groupId"];
            strChatRoomType=[dict valueForKey:@"chatRoomType"];
            
            NSString *strContentType=[dict valueForKey:@"contentType"];
            if ([strContentType isEqualToString:@"location"])
            {
                NSString *strImageName=[dict valueForKey:@"content"];
                [self removeImage:strImageName];
                
            }
            else if ([strContentType isEqualToString:@"image"] || [strContentType isEqualToString:@"video"]|| [strContentType isEqualToString:@"location"] || [strContentType isEqualToString:@"audio"])
            {
                NSString *strContent=[dict valueForKey:@"content"];
                NSArray *components = [strContent componentsSeparatedByString:@"/"];
                NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                [self removeImage:strImageName];
                
            }
        }
        
        NSDictionary *dictArt=[arrForTableView objectAtIndex:deleteChatIndexPath.row];
        NSLog(@"%@",dictArt);
        getchatRoomType=[dictArt valueForKey:@"chatRoomType"];
        NSString *lastMsgTime=[dictArt valueForKey:@"lastMessageTime"];
        BOOL Success1=[db_class deleteChatMessages:getgroupId];
        BOOL success = [db_class updateLastMessage: getgroupId sender:@"" lastMessage:@"" lastMessageStatus:@"" lastMessageTime:lastMsgTime lastMessagesType:@"" unReadCount:@"0"];
        [self refreshChats];
        cell.ImgStatusIcon.image=[UIImage imageNamed:@""];
       
        
    }]];
    
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        actionSheet.view.tintColor=color;
    }
    else
    {
        
        actionSheet.view.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        
    }
}
-(void)DeleteChat
{
   
        ChatCell *cell = (ChatCell *)[self.tableChat cellForRowAtIndexPath:deleteChatIndexPath] ;
        NSString *titleString=[NSString stringWithFormat:@"Delete chat with %@",cell.ChatName.text];
        actionSheet = [UIAlertController alertControllerWithTitle:titleString message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            // Cancel button tappped do nothing.
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Delete Chat" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            NSString *strChatRoomId=cell.ChatRoomId.text;
            getgroupId=cell.ChatRoomId.text;
            NSArray *arrAllChats=[db_class getOneToOneChatMessages:getgroupId];
            NSString *strGrpID,*strChatRoomType;
            BOOL success = [db_class updatedeleteChatTable:getgroupId isDelete:@"1"];
            NSLog(@"%@",arrAllChats);
            for (int i=0; i<arrAllChats.count; i++)
            {
                NSDictionary *dict=[arrAllChats objectAtIndex:i];
                strGrpID=[dict valueForKey:@"groupId"];
                strChatRoomType=[dict valueForKey:@"chatRoomType"];
                 NSString *strContentType=[dict valueForKey:@"contentType"];
                if ([strContentType isEqualToString:@"location"])
                {
                    NSString *strImageName=[dict valueForKey:@"content"];
                    [self removeImage:strImageName];
            
                }
                else if ([strContentType isEqualToString:@"image"] || [strContentType isEqualToString:@"video"]|| [strContentType isEqualToString:@"location"] || [strContentType isEqualToString:@"audio"])
                    
                {
                    NSString *strContent=[dict valueForKey:@"content"];
                    NSArray *components = [strContent componentsSeparatedByString:@"/"];
                    NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                    [self removeImage:strImageName];
            
                }
            }
            NSDictionary *dictArt=[arrForTableView objectAtIndex:deleteChatIndexPath.row];
            NSLog(@"%@",dictArt);
            getchatRoomType=[dictArt valueForKey:@"chatRoomType"];
            if ([getchatRoomType isEqualToString:@"1"])
    
            {
                [[appdel.socket emitWithAck:@"exitGroup" with:@[@{@"groupId": getgroupId,@"from":appdel.strID}]] timingOutAfter:2 callback:^(NSArray* data) {
                    NSLog(@"%@",data);
                    NSArray *arrdata=[db_class getParticipants:getgroupId];
                    for (int i=0; i<arrdata.count; i++)
                    {
                        NSDictionary *dictParti=[arrdata objectAtIndex:i];
                        NSString *strID=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"groupId"]];
                        NSString *userId=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"userId"]];
                        if ([getgroupId isEqualToString:strID])
                        {
                            if ([appdel.strID isEqualToString:userId])
                            {
                                BOOL success2 = [db_class deleteParticipant:strID];
                                BOOL success = [db_class ExitChatTblGroup:getgroupId];
                                BOOL success1 = [db_class ExitChatTblMsgGroup:getgroupId];
                            }
                        }
                    }
                    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                    hud.mode = MBProgressHUDModeText;
                    hud.label.text = @"You are left from group";
                    hud.margin = 10.f;
                    hud.yOffset = 150.f;
                    hud.removeFromSuperViewOnHide = YES;
                    [hud hideAnimated:YES afterDelay:3];
                    [self refreshChats];
                }];
    
            }
            else
            {
                BOOL Success=[db_class deleteChat:getgroupId];
                BOOL Success1=[db_class deleteChatMessages:getgroupId];
                [self refreshChats];

            }
        }]];

    [self presentViewController:actionSheet animated:YES completion:nil];
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        actionSheet.view.tintColor=color;
    }
    else
    {
        
        actionSheet.view.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        
    }
    
}

-(void)CheckRecoveryAPI
{
    
    if ([appdel.strInternetMode isEqualToString:@"online"])
    {
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:appdel.strID forKey:@"id"];
        
        MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDAnimationFade;
        hud.label.text = @"Loading";
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_CHECKRECOVERY withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
             if (response)
             {
                 NSLog(@"Response: %@",response);
                 if ([[response valueForKey:@"error"] boolValue]==false)
                 {
                     isLockStr=@"YES";
                     [self ShowUnlockPasswordView];
                     
                 }
                 else
                 {
                     [self performSegueWithIdentifier:@"ToLockVC" sender:self];
                 }
             }
             
         }];
        
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Please check your internet connection"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.label.adjustsFontSizeToFitWidth=YES;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:2];
        
    }
    
}

// Remove image from documents directory
- (void)removeImage:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
       // UIAlertView *removedSuccessFullyAlert = [[UIAlertView alloc] initWithTitle:@"Congratulations:" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
      //  [removedSuccessFullyAlert show];
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}


-(void)insertdata
{
    
    BOOL success = NO;
    NSString *alertString = @"Data Insertion failed";
    
    NSArray *arrCont=[db_class getUsers];
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    arrChats=[arrCont sortedArrayUsingDescriptors:@[sort]];
    
    for (int i=1; i<=arrChats.count; i++)
    {
        NSDictionary *dictCont=[arrChats objectAtIndex:i-1];
        NSString *strId=[NSString stringWithFormat:@"%d",i];
        NSString *strMobile=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"mobile"]];
        //  NSString *strName=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"Name"]];
        
        success=[db_class insertChats:strId chatRoomId:strMobile chatRoomType:@"3" sender:@"29" lastMessage:@"hello" lastMessageStatus:@"" lastMessagesTime:@"12:21 PM" lastMessagesType:@"text" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:@"" isDelete:@"0" isLock:@"0" password:@""];
        
        success=[db_class insertChatMessages:strId userId:strMobile groupId:@"" chatRoomType:@"0" content:@"Hi Hello" contentType:@"bbb" contentStatus:@"ccc" sender:@"1" sentTime:@"3:21 PM" deliveredTime:@"eee" seenTime:@"3:21 PM" caption:@"fff" isDownloaded:@"1" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
        
             /*  if (success == NO)
         {
         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:
         alertString message:nil
         delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
         } */
        
    }
    
    
    
}

-(void)updatedata
{
   
        BOOL success = NO;
        NSString *alertString = @"Data Updation failed";
        
        //  NSString *strUserId=[NSString stringWithFormat:@"%d",i];
        
     //   success=[[DBManager getSharedInstance]updateLastMessage:@"1" sender:@"990" lastMessage:@"hi hello" lastMessageStatus:@"delivered" lastMessageTime:@"12:49 AM" unReadCount:@"3"];
        
        //  success=[[DBManager getSharedInstance]updateSeenTime:@"1" seenTime:@"08:33 PM"];
       

        
        if (success == NO)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:
                                  alertString message:nil
                                                          delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
      
}

-(void)doesUserExist
{
   
        BOOL success = NO;
        NSString *alertString = @"Data not available";
        
        //  NSString *strUserId=[NSString stringWithFormat:@"%d",i];
        
     //   success=[[DBManager getSharedInstance]doesUserExist:@"9876543211"];
        
        //  success=[[DBManager getSharedInstance]updateSeenTime:@"1" seenTime:@"08:33 PM"];
    
        if (success == NO)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:
                                  alertString message:nil
                                                          delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
   
    
}

-(void)doesChatExist
{
    
    BOOL success = NO;
    NSString *alertString = @"Data not available";
    
    //  NSString *strUserId=[NSString stringWithFormat:@"%d",i];
    
  //  success=[[DBManager getSharedInstance]doesChatExist:@"9876543211"];
    
    //  success=[[DBManager getSharedInstance]updateSeenTime:@"1" seenTime:@"08:33 PM"];
    
    if (success == NO)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:
                              alertString message:nil
                                                      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    
    
}

-(void)getChats
{
    //   NSDictionary *data = [[DBManager getSharedInstance]getUserName:@"9876543210"];
    //  NSDictionary *data = [[DBManager getSharedInstance]getUserImage:@"9876543210"];
    //  NSDictionary *data = [[DBManager getSharedInstance]getUserInfo:@"9876543210"];
    NSMutableArray *arrGetChats=[[NSMutableArray alloc]init];
    arrGetChats=[db_class getChats];
    if (arrGetChats.count!=0)
    {
        for (int i=0; i<arrGetChats.count; i++)
        {
            NSMutableDictionary *dictSing=[arrGetChats objectAtIndex:i];
            NSDictionary *dictInfo = [db_class getUserInfo:[dictSing valueForKey:@"chatRoomId"]];
            [ dictSing setObject:[dictInfo valueForKey:@"name"] forKey:@"name"];
            [ dictSing setObject:[dictInfo valueForKey:@"image"] forKey:@"image"];
            NSLog(@"%@",dictSing);
            
            [arrGetChats replaceObjectAtIndex:i withObject:dictSing];
        }
    }
    else
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:
                              @"Data not found" message:nil delegate:nil cancelButtonTitle:
                              @"OK" otherButtonTitles:nil];
        [alert show];

        
    }
    NSLog(@"%@",arrGetChats);
}

-(void)getChatMessages
{
    
    NSArray *data=[db_class getChatMessages];
    
    if (data == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:
                              @"Data not found" message:nil delegate:nil cancelButtonTitle:
                              @"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        
        // NSString *strName =[data objectAtIndex:0];
        // NSLog(@"%@",strName);
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"Chat_SingleChat"])
    {
        SingleChatVC *Single=[segue destinationViewController];
        Single.dictDetails=dictToSendSingleChat;
        appdel.ZOEID = [dictToSendSingleChat valueForKey:@"chatRoomId"];
        appdel.RoomTYPE = [dictToSendSingleChat valueForKey:@"chatRoomType"];
        NSLog(@"%@",Single.dictDetails);
    }
    else if ([[segue identifier] isEqualToString:@"ToLockVC"])
    {
        
        LockViewController *Details=[segue destinationViewController];
        Details.getChatId=ChatRoomId;
        
    }
    else if ([[segue identifier] isEqualToString:@"ToOTPVC"])
    {
        
        ForgotPasswordVC *Details=[segue destinationViewController];
        Details.getChatId=ChatRoomId;
        
    }
    


}



#pragma mark -
#pragma mark - Search Delegate

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    
    [arrForTableView removeAllObjects];
    arrForTableView=[NSMutableArray arrayWithArray:arrChats];
    
    if (arrForTableView.count!=0)
    {
        [_tableChat reloadData];
        
    }
    else
    {
        //_tablePatientDetails.hidden=YES;
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [arrForTableView removeAllObjects];
    NSString *strValue = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    
    if(![strValue isEqualToString:@""])
    {
        [arrForTableView removeAllObjects];
        isFilter=YES;

        
        for(NSDictionary * dictValue in arrChats)
        {
            NSString * strGroupName;
            NSString *checkRoomType=[dictValue objectForKey:@"chatRoomType"];
            if ([checkRoomType isEqualToString:@"0"])
            {
                 strGroupName = [dictValue objectForKey:@"name"];

            }
            else
            {
                strGroupName = [dictValue objectForKey:@"groupName"];

            }
            
            NSRange GroupName=[[strGroupName lowercaseString] rangeOfString:[strValue lowercaseString]];
            
            if(GroupName.location != NSNotFound)
                [arrForTableView addObject:dictValue];
            searchArray=[NSMutableArray arrayWithArray:arrForTableView];
        }
        
    }
    else if(strValue != nil && [strValue isEqualToString:@""])
    {
        isFilter=NO;

        [arrForTableView removeAllObjects];
        arrForTableView=[NSMutableArray arrayWithArray:arrChats];
        searchArray=[NSMutableArray arrayWithArray:arrForTableView];

    }
    
    
    if (range.length==1 && range.location==0)
    {
        [arrForTableView removeAllObjects];
        arrForTableView=[NSMutableArray arrayWithArray:arrChats];
        searchArray=[NSMutableArray arrayWithArray:arrForTableView];

    }
    
    
    
     if (arrForTableView.count!=0)
     {
         [_tableChat reloadData];
    
     }
     
    
    
    
    return YES;
}

#pragma mark -
#pragma mark - Font

-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)view;
        [btn setFont:[UIFont fontWithName:fontFamily size:[[btn font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *txtFld = (UITextField *)view;
        [txtFld setFont:[UIFont fontWithName:fontFamily size:[[txtFld font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextView class]])
    {
        UITextView *txtVw = (UITextView *)view;
        [txtVw setFont:[UIFont fontWithName:fontFamily size:[[txtVw font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
    
}
// 

- (IBAction)clickOnSubmit:(UIButton*)sender {
    
    [_passwordTxtFld resignFirstResponder];

    dictToSendSingleChat=[arrChats objectAtIndex:getIndexvalue];
    NSString *strcheckPassword=[NSString stringWithFormat:@"%@",[dictToSendSingleChat valueForKey:@"password"]];
    if ([_passwordTxtFld.text isEqualToString:@""] )
    {
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Fill all Details"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.label.adjustsFontSizeToFitWidth=YES;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:2];
    }

    else if ([strcheckPassword isEqualToString:_passwordTxtFld.text])
    {
        UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
        _passwordTxtFld.text=@"";
        self.PasswordView.hidden=YES;
        [viewForAudioOrVideo removeFromSuperview];
        [self performSegueWithIdentifier:@"Chat_SingleChat" sender:self];
    }
    else{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Enter Correct Password"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.label.adjustsFontSizeToFitWidth=YES;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:2];
        
    }


}

- (IBAction)clickOnForgotPassword:(id)sender {
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    self.PasswordView.hidden=YES;
    [viewForAudioOrVideo removeFromSuperview];
    [self performSegueWithIdentifier:@"ToOTPVC" sender:self];

//    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    ForgotPasswordVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"ForgotPasswordVC"];
//    [self.navigationController pushViewController:hme animated:YES];
}
- (IBAction)ClickOnUnlockSubmit:(id)sender {
    [_unlockPasswordTxt resignFirstResponder];
    [_UnlockConPasswordTxt resignFirstResponder];


        dictToSendSingleChat=[arrChats objectAtIndex:getIndexvalue];
         NSString *strcheckPassword=[NSString stringWithFormat:@"%@",[dictToSendSingleChat valueForKey:@"password"]];
        NSString *getChatId=[NSString stringWithFormat:@"%@",[dictToSendSingleChat valueForKey:@"chatRoomId"]];
    if ([isLockStr isEqualToString:@"YES"])
    {
        if ([_unlockPasswordTxt.text isEqualToString:@""] || [_UnlockConPasswordTxt.text isEqualToString:@""])
        {
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            // Configure for text only and offset down
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [NSString stringWithFormat:@"Fill all Details"];
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.label.adjustsFontSizeToFitWidth=YES;
            hud.removeFromSuperViewOnHide = YES;
            [hud hideAnimated:YES afterDelay:2];
        }

        else if ([_unlockPasswordTxt.text isEqualToString:_UnlockConPasswordTxt.text])
        {
            NSLog(@"%@",getChatId);
            
                BOOL success1=[db_class updateLockChatTable:getChatId isLock:@"1"];
                BOOL success2=[db_class updatePasswordChatTable:getChatId password:_unlockPasswordTxt.text];
                       UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:1087650];
            self.UnlockPasswordView.hidden=YES;
            [viewForAudioOrVideo removeFromSuperview];
            UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            MainTabBarVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainTabBarVC"];
            [self.navigationController pushViewController:hme animated:YES];
            
        }
    }
    else if ([isLockStr isEqualToString:@"NO"])

    {
        if ([_unlockPasswordTxt.text isEqualToString:@""] || [_UnlockConPasswordTxt.text isEqualToString:@""])
        {
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            // Configure for text only and offset down
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [NSString stringWithFormat:@"Fill all Details"];
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.label.adjustsFontSizeToFitWidth=YES;
            hud.removeFromSuperViewOnHide = YES;
            [hud hideAnimated:YES afterDelay:2];
        }

        else if ([strcheckPassword isEqualToString:_unlockPasswordTxt.text] && [_unlockPasswordTxt.text isEqualToString:_UnlockConPasswordTxt.text])
        {
            if ([_unlockPasswordTxt.text isEqualToString:_UnlockConPasswordTxt.text])
            {
                NSLog(@"%@",getChatId);
                
                    BOOL success1=[db_class updateLockChatTable:getChatId isLock:@"0"];
                    BOOL success2=[db_class updatePasswordChatTable:getChatId password:@""];
                    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:1087650];
                self.UnlockPasswordView.hidden=YES;
                [viewForAudioOrVideo removeFromSuperview];
                UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                MainTabBarVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainTabBarVC"];
                [self.navigationController pushViewController:hme animated:YES];
                
            }
        }
        else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            // Configure for text only and offset down
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [NSString stringWithFormat:@"Enter Correct Password"];
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.label.adjustsFontSizeToFitWidth=YES;
            hud.removeFromSuperViewOnHide = YES;
            [hud hideAnimated:YES afterDelay:2];
            
        }

    }
    
        else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
            // Configure for text only and offset down
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [NSString stringWithFormat:@"Enter Correct Password"];
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.label.adjustsFontSizeToFitWidth=YES;
            hud.removeFromSuperViewOnHide = YES;
            [hud hideAnimated:YES afterDelay:2];
            
        }
    
    

}
@end
