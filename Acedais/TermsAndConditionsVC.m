//
//  TermsAndConditionsVC.m
//  ZoeChat
//
//  Created by Pyramidions Solution on 29/12/16.
//  Copyright © 2016 Pyramidions Solution. All rights reserved.
//

#import "TermsAndConditionsVC.h"
#import "Constants.h"
#import "AppDelegate.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DBClass.h"
#import "NBPhoneNumberUtil.h"
#import "AFNHelper.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import <Photos/Photos.h>
#import "CustomAlbum.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "Reachability.h"

#define DOCUMENTS_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]

@interface TermsAndConditionsVC ()
{
    NSMutableArray *arrContacts;
    AppDelegate *appDelegate;
    DBClass *db_class;
    
    NSMutableDictionary *recordSetting;
    NSString *recorderFilePath;
    AVAudioRecorder *recorder;
}
@end

@implementation TermsAndConditionsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
     db_class=[[DBClass alloc]init];
 /*  NSArray *fontFamilies = [UIFont familyNames];
    
    for (int i = 0; i < [fontFamilies count]; i++)
    {
        NSString *fontFamily = [fontFamilies objectAtIndex:i];
        NSArray *fontNames = [UIFont fontNamesForFamilyName:[fontFamilies objectAtIndex:i]];
        NSLog (@"%@: %@", fontFamily, fontNames);
    }
  
   
  */
  
     [self setFontFamily:FONT_NORMAL forView:self.view andSubViews:YES];
    
    [_btnTermsPrivacyPolicy setTitle:@"" forState:UIControlStateNormal];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Tap 'Agree and Continue' to accept the Acedais Terms of Service and Privacy Policy"]];
    [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0] range:NSMakeRange(39, 43)];
    [_btnTermsPrivacyPolicy setAttributedTitle:attrStr forState:UIControlStateNormal];
    _btnTermsPrivacyPolicy.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    [_lblWelcome setFont:[UIFont fontWithName:FONT_HEAVY size:[[_lblWelcome font] pointSize]]];
    
    _btnAgree.clipsToBounds=YES;
    _btnAgree.layer.cornerRadius=4;
     [_btnAgree setFont:[UIFont fontWithName:FONT_MEDIUM size:[[_btnAgree font] pointSize]]];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchContactsandAuthorization) name:@"Ask_Refresh_Contacts" object:nil];
    
    [self createAlbum];
}

-(void)viewWillAppear:(BOOL)animated
{
     [self.navigationController.navigationBar setHidden:YES];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    if (reachability.isReachable) {
        NSLog(@"We have internet!");
        appDelegate.strInternetMode=@"online";
    } else {
        NSLog(@"No internet!");
        appDelegate.strInternetMode=@"ofline";
    }
    
   [self fetchContactsandAuthorization];
    [self startRecording];

    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:NO];
}

- (void)createAlbum
{
    [CustomAlbum makeAlbumWithTitle:CSAlbum onSuccess:^(NSString *AlbumId)
     {
         NSLog(@"Created");
     }
                            onError:^(NSError *error) {
                                NSLog(@"probelm in creating album");
                            }];
}


- (void)networkChanged:(NSNotification *)notification
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    if (reachability.isReachable) {
        NSLog(@"We have internet!");
        appDelegate.strInternetMode=@"online";
    } else {
        NSLog(@"No internet!");
        appDelegate.strInternetMode=@"ofline";
    }
    
    
}
-(void)fetchContactsandAuthorization
{
    // Request authorization to Contacts
    CNContactStore *store = [[CNContactStore alloc] init];
    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted == YES)
        {
            arrContacts=[[NSMutableArray alloc]init];
            //keys with fetching properties
            NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey];
            NSString *containerId = store.defaultContainerIdentifier;
            NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
            NSError *error;
            NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
            if (error)
            {
                NSLog(@"error fetching contacts %@", error);
            }
            else
            {
                NSString *phone;
                NSString *fullName=@"";
                NSString *firstName;
                NSString *lastName;
                
                for (CNContact *contact in cnContacts) {
                    // copy data to my custom Contacts class.
                    firstName = contact.givenName;
                    lastName = contact.familyName;
                    if (lastName == nil) {
                        fullName=[NSString stringWithFormat:@"%@",firstName];
                    }else if (firstName == nil){
                        fullName=[NSString stringWithFormat:@"%@",lastName];
                    }
                    else{
                        fullName=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                    }
                    
                    fullName = [fullName stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceCharacterSet]];

                    
                    NSMutableArray *contactNumbersArray = [[NSMutableArray alloc]init];
                    for (CNLabeledValue *label in contact.phoneNumbers) {
                        phone = [label.value stringValue];
                        if ([phone length] > 0) {
                            [contactNumbersArray addObject:phone];
                        }
                    }
                    NSMutableDictionary *dictContact=[[NSMutableDictionary alloc]init];
                    [dictContact setObject:fullName forKey:@"Name"];
                    
                    if (contactNumbersArray.count!=0)
                    {
                        for (int l=1; l<=contactNumbersArray.count; l++)
                        {
                            NSString *strPhNo=[NSString stringWithFormat:@"PhoneNumber%d",l];
                            NSString* strPhone = [[contactNumbersArray objectAtIndex:l-1]stringByReplacingOccurrencesOfString:@"\u00a0" withString:@""];
                            strPhone = [strPhone stringByReplacingOccurrencesOfString:@"-" withString:@""];
                            strPhone = [strPhone stringByReplacingOccurrencesOfString:@" " withString:@""];
                            strPhone = [strPhone stringByReplacingOccurrencesOfString:@"(" withString:@""];
                            strPhone = [strPhone stringByReplacingOccurrencesOfString:@")" withString:@""];

                            NSString *NEWstrPhone;
                            if ([strPhone containsString:@"+"]) {
                                
                                strPhone = [strPhone stringByReplacingOccurrencesOfString:@"+" withString:@""];
                                NEWstrPhone=[NSString stringWithFormat:@"%@",strPhone];
                                
                                
                            }
                            else {
                                NSString *setCountryCode = [[NSUserDefaults standardUserDefaults]stringForKey:@"getCountryCode"];
                                NEWstrPhone=[NSString stringWithFormat:@"%@%@",setCountryCode,strPhone];
                            }
                            [dictContact setObject:NEWstrPhone forKey:strPhNo];
                        }
                    }
                    [arrContacts addObject:dictContact];
                    // NSLog(@"The contactsArray are - %@",arrContacts);
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    // [tableViewContactData reloadData];
                });
            }
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
            dispatch_async(queue, ^{
                [self insertUser];
                dispatch_sync(dispatch_get_main_queue(), ^{
                });
            });        }
    }];
    
}

-(void)sendContacts
{
    
        NSMutableArray *arrContactsToSend=[[NSMutableArray alloc]init];

    NSArray *arrContdata=[db_class getUsers];
    for (int i=0; i<arrContdata.count; i++)
    {
        NSMutableDictionary *dictContact=[[NSMutableDictionary alloc]init];
        NSDictionary *dictCont=[arrContdata objectAtIndex:i];
        NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"mobile"]];
        [dictContact setObject:strPhone forKey:@"mobileNumber"];
        NSString *strname=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"name"]];
        [dictContact setObject:strname forKey:@"contactName"];
        [arrContactsToSend addObject:dictContact];
    }
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrContactsToSend options:0 error:&error];
    NSString *jsonString;
    
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    NSString* encodedString = [jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:appDelegate.strID forKey:@"from"];
    [dictParam setValue:jsonString forKey:@"contacts"];
    
    
//
//    for (int i=0; i<arrContacts.count; i++)
//    {
//        NSMutableDictionary *dictContact=[[NSMutableDictionary alloc]init];
//        NSDictionary *dictCont=[arrContacts objectAtIndex:i];
//        NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"PhoneNumber1"]];
//        [dictContact setObject:strPhone forKey:@"mobileNumber"];
//        [arrContactsToSend addObject:dictContact];
//        
//        
//    }
//    NSError *error;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrContactsToSend options:0 error:&error];
//    NSString *jsonString;
//    
//    if (! jsonData)
//    {
//        NSLog(@"Got an error: %@", error);
//    }
//    else
//    {
//        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    }
//
//    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
//    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
//    NSString* encodedString = [jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    
//    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
//    [dictParam setValue:jsonString forKey:@"contacts"];
    
    
//    MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//    hud.mode = MBProgressHUDAnimationFade;
//    hud.label.text = @"Loading";
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_CONTACTS_SYNC withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
         if (response)
         {
           //  NSLog(@"Response: %@",response);
             if ([[response valueForKey:@"error"] boolValue]==false)
             {
                 NSLog(@"Success");
             }
             else
             {
                 
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 // Configure for text only and offset down
                 hud.mode = MBProgressHUDModeText;
                 hud.label.text = [NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                 hud.margin = 10.f;
                 hud.yOffset = 150.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hideAnimated:YES afterDelay:3];
                 
                 
             }
         }
         
     }];

    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onAgree:(id)sender
{
    [self performSegueWithIdentifier:@"TermsAnConds_GettingPhoneNO" sender:self];
}

- (IBAction)onViewTermsPrivacyPolicy:(id)sender {
}

#pragma mark -
#pragma mark - Font
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)view;
        [btn setFont:[UIFont fontWithName:fontFamily size:[[btn font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *txtFld = (UITextField *)view;
        [txtFld setFont:[UIFont fontWithName:fontFamily size:[[txtFld font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextView class]])
    {
        UITextView *txtVw = (UITextView *)view;
        [txtVw setFont:[UIFont fontWithName:fontFamily size:[[txtVw font] pointSize]]];
    }
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}

-(void)insertUser
{
    [self deleteUser];
    BOOL success = NO;
    //   NSString *alertString = @"Data Insertion failed";
    
    for (int i=0; i<arrContacts.count; i++)
    {
        NSDictionary *dictCont=[arrContacts objectAtIndex:i];
        
        NSString *strMobile=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"PhoneNumber1"]];
        NSString *strName=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"Name"]];
        
        NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
        if ([[strName stringByTrimmingCharactersInSet: set] length] != 0)
        {
            success =[db_class insertUser:strMobile name:strName registeredName:@"" image:@"" status:@"" showInContactsPage:@"0" zoeChatId:@""];
            
            if (success == NO)
            {
                success=[db_class updateUserName:strMobile name:strName];
            }
            
        }
        
    }
    
    [self getUsers];
}

-(void)deleteUser
{
    NSArray *arrUsers=[db_class getUsers];
    
    BOOL success = NO;
    for (int i=0; i<arrUsers.count; i++)
    {
        int nAvail=0;
        NSDictionary *dictUser=[arrUsers objectAtIndex:i];
        NSString *strMobile=[NSString stringWithFormat:@"%@",[dictUser valueForKey:@"mobile"]];
        for (int j=0; j<arrContacts.count; j++)
        {
            NSDictionary *dictCont=[arrContacts objectAtIndex:j];
            NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"PhoneNumber1"]];
            
            if ([strPhone isEqualToString:strMobile])
            {
                nAvail=1;
            }
        }
        if (nAvail==0)
        {
            success=[db_class deleteUser:strMobile];
        }
        
    }
    
}

-(void)getUsers
{
    //   NSDictionary *data = [[DBManager getSharedInstance]getUserName:@"9876543210"];
    //  NSDictionary *data = [[DBManager getSharedInstance]getUserImage:@"9876543210"];
    //  NSDictionary *data = [[DBManager getSharedInstance]getUserInfo:@"9876543210"];
    NSArray *arrContdata=[db_class getUsers];
    NSMutableArray *arrContactsToSend=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrContdata.count; i++)
    {
        NSMutableDictionary *dictContact=[[NSMutableDictionary alloc]init];
        NSDictionary *dictCont=[arrContdata objectAtIndex:i];
        NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"mobile"]];
        [dictContact setObject:strPhone forKey:@"mobileNumber"];
        NSString *strname=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"name"]];
        [dictContact setObject:strname forKey:@"contactName"];
        [arrContactsToSend addObject:dictContact];
    }
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrContactsToSend options:0 error:&error];
    NSString *jsonString;
    
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    NSString* encodedString = [jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:appDelegate.strID forKey:@"from"];
    [dictParam setValue:jsonString forKey:@"contacts"];
    
    
    //    MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    //    hud.mode = MBProgressHUDAnimationFade;
    //    hud.label.text = @"Loading";
 //   NSLog(@"%@",appDelegate.strInternetMode);
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_CONTACTS_SYNC withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
         if (response)
         {
           //  NSLog(@"Response: %@",response);
             if ([[response valueForKey:@"error"] boolValue]==false)
             {
              //   NSLog(@"Success");
                 NSArray *arrContactsToUpdate=[response valueForKey:@"contacts"];
                 for (int i=0; i<arrContactsToUpdate.count; i++)
                 {
                     NSDictionary *dictCont=[arrContactsToUpdate objectAtIndex:i];
                     if ([[dictCont valueForKey:@"showInContactsPage"] boolValue]==true)
                     {
                         NSString *strZoeChatId = [[dictCont valueForKey:@"zoechatid"] stringByReplacingOccurrencesOfString:@"+" withString:@""];
                         BOOL success = NO;
                         success=[db_class updateUser:[dictCont valueForKey:@"mobileNumber"] registeredName:[dictCont valueForKey:@"name"] image:[dictCont valueForKey:@"image"] status:[dictCont valueForKey:@"status"] showInContactsPage:@"1" zoeChatId:strZoeChatId];
                     }
                     
                 }
                  [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Contacts" object:self];
             }
             else
             {
                 
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 // Configure for text only and offset down
                 hud.mode = MBProgressHUDModeText;
                 hud.label.text = [NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                 hud.margin = 10.f;
                 hud.yOffset = 150.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hideAnimated:YES afterDelay:3];
                 
                 
             }
         }
         
     }];

    
    if (arrContdata == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:
                              @"Data not found" message:nil delegate:nil cancelButtonTitle:
                              @"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        
        // NSString *strName =[data objectAtIndex:0];
        // NSLog(@"%@",strName);
        
    }
}
- (void) startRecording{
    
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *err = nil;
    [audioSession setCategory :AVAudioSessionCategoryPlayAndRecord error:&err];
    if(err){
        NSLog(@"audioSession: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
        return;
    }
    [audioSession setActive:YES error:&err];
    err = nil;
    if(err){
        NSLog(@"audioSession: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
        return;
    }
    
    
    
    
    recordSetting = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                     [NSNumber numberWithInt: kAudioFormatMPEG4AAC], AVFormatIDKey,
                     [NSNumber numberWithFloat:16000.0], AVSampleRateKey,
                     [NSNumber numberWithInt: 1], AVNumberOfChannelsKey,
                     nil];
    
    
    BOOL audioHWAvailable = audioSession.inputIsAvailable;
    if (! audioHWAvailable) {
        UIAlertView *cantRecordAlert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: @"Audio input hardware not available"
                                  delegate: nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [cantRecordAlert show];
        
        return;
    }
    else
    {
        // Create a new dated file
        NSDate *now=[NSDate date];
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"yyyyMMddHHmmssSSS"];
        NSString *localDateString = [dateFormatter1 stringFromDate:now];
        
        recorderFilePath = [NSString stringWithFormat:@"%@/%@.m4a", DOCUMENTS_FOLDER, localDateString];
        
        NSURL *url = [NSURL fileURLWithPath:recorderFilePath];
        err = nil;
        recorder = [[ AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:&err];
        if(!recorder){
            NSLog(@"recorder: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
            
            UIAlertView *alert =
            [[UIAlertView alloc] initWithTitle: @"Warning"
                                       message: [err localizedDescription]
                                      delegate: nil
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
            [alert show];
            
            return;
        }
        
        //prepare to record
        [recorder setDelegate:self];
        [recorder prepareToRecord];
        recorder.meteringEnabled = YES;
        
    }
    
    // start recording
    [recorder recordForDuration:(NSTimeInterval) 0];
    [self stopRecording];
    
}

- (void) stopRecording{
    
    [recorder stop];
    
    NSURL *url = [NSURL fileURLWithPath: recorderFilePath];
    NSError *err = nil;
    NSData *audioData = [NSData dataWithContentsOfFile:[url path] options: 0 error:&err];
    if(!audioData)
        NSLog(@"audio data: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
    
    [self removeImage:recorderFilePath];
    
}
- (void)removeImage:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:recorderFilePath error:&error];
    if (success)
    {
        
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}


@end
