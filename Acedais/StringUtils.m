
#import "StringUtils.h"

#import <Foundation/NSString.h>

@implementation StringUtils

+ (BOOL)isStringPresent:(NSString *)string {
    
	if ([StringUtils isBlank:string] || (string == nil) || ([self isKindOfClass:[NSNull class]])) {
		return NO;
	}
	return YES;
}

+ (NSString *)stringByStrippingWhitespace:(NSString *)string {
    
	return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
}

//+ (UIStoryboard *)getCurrentStoryBoardObject
//{
//    // Now that we have validated and got necessary bits, we can push to the Category Picker
//    
//    BOOL iPad = NO;
//    
//    UIStoryboard *mainStoryboard = nil;
//    
//#ifdef UI_USER_INTERFACE_IDIOM
//    iPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
//#endif
//    if (iPad)
//    {
//        // iPad specific code here
//        mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: [NSBundle mainBundle]];
//    }
//    else
//    {
//        // iPhone/iPod specific code here
//        mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: [NSBundle mainBundle]];
//    }
//    return mainStoryboard;
//}

+ (BOOL) validate_email: (NSString *) candidate {
    
    BOOL isvalid = NO;
    
    if (candidate.length != 0)
    {
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        isvalid =  [emailTest evaluateWithObject:candidate];
    }
    else
    {
        isvalid = YES;
    }
    
    return isvalid;
}



+ (BOOL)isBlank:(NSString *)string {
    
    if (string != nil) {
        
        if([[StringUtils stringByStrippingWhitespace:string] length] == 0)
            return YES;
        
    }
	
	return NO;
    
}

+ (BOOL)string:(NSString *)string containsSubstring:(NSString *)subString {
    
	NSRange range = [string rangeOfString:subString];
    return (range.location != NSNotFound);
    
}

+ (NSArray *)string:(NSString *)string splitWithCharacter:(char) ch {
    
	NSMutableArray *results = [[NSMutableArray alloc] init];
    
	int start = 0;
    
	for(int i=0; i < [string length]; i++) {
		
		BOOL isAtSplitChar = [string characterAtIndex:i] == ch;
		BOOL isAtEnd = i == [string length] - 1;
        
		if(isAtSplitChar || isAtEnd) {
            
			//take the substring & add it to the array
			NSRange range;
			range.location = start;
			range.length = i - start + 1;
			
			if(isAtSplitChar)
				range.length -= 1;
			
			[results addObject:[string substringWithRange:range]];
			start = i + 1;
            
		}
		
		if(isAtEnd && isAtSplitChar)
			[results addObject:@""];
        
	}
	
	return results ;
}

+ (NSString *)string:(NSString *)string substringFrom:(NSInteger)from to:(NSInteger)to {
    
	NSString *rightPart = [string substringFromIndex:from];
	return [rightPart substringToIndex:to-from];
    
}

+ (int)string:(NSString *)string indexOfString: (NSString *) str {
    
	NSRange range = [string rangeOfString:str];
	return (int)range.location;
    
}

+ (int)string:(NSString *)string indexOfString:(NSString *)str fromIndex:(int)fromIndex {
    
    NSRange rangeToSearch = NSMakeRange(fromIndex, [string length] - fromIndex);
    NSRange range = [string rangeOfString:string options:NSLiteralSearch range:rangeToSearch];
    
    return (int)range.location;
}

+ (int)string:(NSString *)string lastIndexOfString:(NSString *) str {
    
	NSRange range = [string rangeOfString:str options:NSBackwardsSearch];
    
	return (int)range.location;
    
}

+ (BOOL)string:(NSString *)string equalsIgnoreCase:(NSString *) str {
    
	if( [string caseInsensitiveCompare:str] == NSOrderedSame ) {
        
		return YES;
        
	} else {
        
		return NO;
	}
    
}

+ (NSString *)isStringNil:(NSString *)inputString {
    
	if (inputString == nil) {
        
		return @"";
        
	}
	
	return inputString;
}


+ (NSString *)stringByTrimmingLeadingCharactersInSet:(NSCharacterSet *)characterSet inString:(NSString*)string {
    NSRange rangeOfFirstWantedCharacter = [string rangeOfCharacterFromSet:[characterSet invertedSet]];
    if (rangeOfFirstWantedCharacter.location == NSNotFound) {
        return @"";
    }
    return [string substringFromIndex:rangeOfFirstWantedCharacter.location];
}


+ (NSString *)stringByTrimmingTrailingCharactersInSet:(NSCharacterSet *)characterSet inString:(NSString*)string {
    NSRange rangeOfLastWantedCharacter = [string rangeOfCharacterFromSet:[characterSet invertedSet]
                                                               options:NSBackwardsSearch];
    if (rangeOfLastWantedCharacter.location == NSNotFound) {
        return @"";
    }
    return [string substringToIndex:rangeOfLastWantedCharacter.location+1]; // non-inclusive
}

+ (NSString *)stringByTrimmingTrailingAndLeadingWhitespaceAndNewlineCharactersInString:(NSString*)string {
    
    NSString *str = [self stringByTrimmingTrailingCharactersInSet:
                     [NSCharacterSet whitespaceAndNewlineCharacterSet]  inString:string];
    
    str = [self stringByTrimmingLeadingCharactersInSet:
           [NSCharacterSet whitespaceAndNewlineCharacterSet] inString:str];
    
    return str;
}

+ (NSData *) get_json_string:(NSArray *)json_array
{
    
    NSError *error;
    NSMutableData *json_string;
    NSData *json_data = [NSJSONSerialization dataWithJSONObject:json_array
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (!json_data)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,error);
    }
    else
    {
        json_string = [NSMutableData dataWithBytes:(const void *)"r=" length:2];
        [json_string appendData:json_data];
    }

    return  json_string;
    
}


@end