//
//  ToShareLocationVC.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 10/04/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>

@interface ToShareLocationVC : UIViewController<GMSMapViewDelegate, CLLocationManagerDelegate>
@property(nonatomic,strong)CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet GMSMapView *GoogleMapView;
@property (strong, nonatomic) IBOutlet UIButton *btnShareCurrLoc;
@property (strong, nonatomic) IBOutlet UIView *infoWindow;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UIButton *btnShareSelectedLoc;

@property(strong, nonatomic)NSString *strZoeChatId;
@property(strong, nonatomic)NSString *strChatRoomType;
@end
