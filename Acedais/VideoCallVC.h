//
//  VideoCallVC.h
//  Texel
//
//  Created by Pyramidions Solution on 30/03/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AgoraRtcEngineKit/AgoraRtcEngineKit.h>

@class VideoCallVC;
@protocol VideoCallVCDelegate <NSObject>
- (void)roomVCNeedClose:(VideoCallVC *)roomVC;
@end

@interface VideoCallVC : UIViewController
@property (copy, nonatomic) NSString *roomName;
@property (assign, nonatomic) AgoraRtcVideoProfile videoProfile;
@property (weak, nonatomic) id<VideoCallVCDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *viewForNameStatus;
@property (weak, nonatomic) IBOutlet UIView *viewForPickCall;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
- (IBAction)onPickCall:(id)sender;
- (IBAction)onEndCall:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *callNumberlbl;

@property (weak, nonatomic) NSString  *strName, *strTexelID, *strimage, *strInComing;
@property (strong, nonatomic) IBOutlet UIButton *AcceptBtn;
@property (strong, nonatomic) IBOutlet UIButton *DecllainBtn;
@end
