//
//  CreateGroupContactVC.m
//  ZoeChat
//
//  Created by Pyramidions Solution on 22/04/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "CreateGroupContactVC.h"
#import "AppDelegate.h"
#import "DBClass.h"
#import "ContactCell.h"
#import "Constants.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "GetNameImageVC.h"
#import "MBProgressHUD.h"
#import "AFNHelper.h"
#import "Constants.h"
#import "MainTabBarVC.h"

@interface CreateGroupContactVC ()<UITextFieldDelegate>
{
    AppDelegate *appDelegate;
    DBClass *db_class;
     NSMutableArray *arrContacts, *arrForTableView, *arrSelectedContacts;
    NSDictionary *dictToSendSingleChat;
    UIView *ViewSearch;
    //SelectedIndex
    NSIndexPath *selectedIndex;
    NSString *strBroadcastId;
    NSString *participateName;
   
}
@end

@implementation CreateGroupContactVC
@synthesize CheckPartiIDS,getGroupImg,getGroupName;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    strBroadcastId = [[NSUUID UUID] UUIDString];


    // Do any additional setup after loading the view.
    NSLog(@"%@",CheckPartiIDS);
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    db_class=[[DBClass alloc]init];
     arrForTableView=[[NSMutableArray alloc]init];
    arrContacts=[[NSMutableArray alloc]init];
     arrSelectedContacts=[[NSMutableArray alloc]init];
    UIBarButtonItem *barBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"BackButton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onBack)];
     [self.navigationItem setLeftBarButtonItem:barBtn ];
    
    UIBarButtonItem *barBtnSearch = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Search.png"] style:UIBarButtonItemStylePlain target:self action:@selector(searchView)];
     [self.navigationItem setRightBarButtonItem:barBtnSearch ];
    
    
    if (appDelegate.strGroupIdToAddParticipants.length!=0)
    {
         self.title=@"Add Paricipants";
    }
    else if([appDelegate.isBroadCast isEqualToString:@"YES"])
    {
        self.title=@"New BroadCast";
        appDelegate.isBroadCast=@"NO";

    }
    else
    {
         self.title=@"New Group";
    }
      [self setFontFamily:FONT_NORMAL forView:self.view andSubViews:YES];
    _tableContacts.delegate=self;
    _tableContacts.dataSource=self;
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self RefreshContacts];
}
-(void)viewWillDisappear:(BOOL)animated
{
     appDelegate.strGroupIdToAddParticipants=@"";
}
-(void)RefreshContacts
{
    
    arrContacts=[[NSMutableArray alloc]init];
    
    NSMutableArray *arrZoeCont=[[NSMutableArray alloc]init];
    NSArray *arrCont=[db_class getUsers];
    if (arrCont.count!=0)
    {
        for (int i=0; i<arrCont.count; i++)
        {
            NSDictionary *dictCont=[arrCont objectAtIndex:i];
            NSString *strShowInContacts=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"showincontactspage"]];
            NSString *strzoe=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"zoechatid"]];
            if ([strShowInContacts boolValue]==true)
            {
                if (![appDelegate.strID isEqualToString:strzoe])
                {
                    [arrZoeCont addObject:dictCont];
                }
                
            }
            
            
        }
    }
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    arrContacts=[arrZoeCont sortedArrayUsingDescriptors:@[sort]];
      arrForTableView=[NSMutableArray arrayWithArray:arrContacts];
    
    [_tableContacts reloadData];
    
}
-(void)searchView
{
    // [self.tableView reloadData];
    
    ViewSearch=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.navigationController.navigationBar.frame.size.height)];
    ViewSearch.backgroundColor=[UIColor whiteColor];
    [self.navigationController.navigationBar addSubview:ViewSearch];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 40, ViewSearch.frame.size.height)];
    [btnClose addTarget:self action:@selector(onClose) forControlEvents:UIControlEventTouchDown];
    [ViewSearch addSubview:btnClose];
    
    UIImageView *arrowImg=[[UIImageView alloc]initWithFrame:CGRectMake(10, (self.navigationController.navigationBar.frame.size.height/2)-10, 20, 20)];
    arrowImg.image=[UIImage imageNamed:@"BackToClose.png"];
    [ViewSearch addSubview:arrowImg];
    
    
    UITextField  *txtSearch=[[UITextField alloc]initWithFrame:CGRectMake(45, 0, self.view.frame.size.width-40, self.navigationController.navigationBar.frame.size.height)];
    txtSearch.clearButtonMode=UITextFieldViewModeWhileEditing;
    txtSearch.placeholder=@"Search...";
    txtSearch.delegate=self;
    [txtSearch becomeFirstResponder];
    [ViewSearch addSubview:txtSearch];
}
-(void)onClose
{
    [ViewSearch removeFromSuperview];
    [arrForTableView removeAllObjects];
    arrForTableView=[NSMutableArray arrayWithArray:arrContacts];
    [_tableContacts reloadData];
    
}

#pragma mark -
#pragma mark - TableView Delagate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  60;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return arrForTableView.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    ContactCell *cell=(ContactCell *)[tableView dequeueReusableCellWithIdentifier:@"ContactCell"];
    if (cell==nil) {
        cell=[[ContactCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ContactCell"];
    }
    NSString *InitialStr;
    UIColor *bgColor;
    [cell.lblImg setHidden:YES];

    NSDictionary *dictArt=[arrForTableView objectAtIndex:indexPath.row];
    NSString *strName=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"name"]];
    cell.ContactName.text=[strName capitalizedString];
    cell.ContactName.font=[UIFont fontWithName:FONT_NORMAL size:16];
    InitialStr=[cell.ContactName.text substringToIndex:1];
    if ([InitialStr isEqualToString:@"A"])
    {
        bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"B"])
    {
        bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"C"])
    {
        bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"D"])
    {
        bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"E"])
    {
        bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"F"])
    {
        bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"G"])
    {
        bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"H"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"I"])
    {
        bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"J"])
    {
        bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"K"])
    {
        bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"L"])
    {
        bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"M"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"N"])
    {
        bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"O"])
    {
        bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"P"])
    {
        bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"Q"])
    {
        bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"R"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"S"])
    {
        bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"T"])
    {
        bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"U"])
    {
        bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"V"])
    {
        bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"W"])
    {
        bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"X"])
    {
        bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"Y"])
    {
        bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"Z"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
    }
    

    NSString *strImageURL1=[dictArt valueForKey:@"image"];
    if (![strImageURL1 isEqualToString:@"(null)"])
    {
        if (![strImageURL1 isEqualToString:@""])
        {
            [cell.ContactImage sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
        }
    }
    else
        
    {
        [cell.lblImg setHidden:NO];
        
        [cell.ContactImage sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
        cell.lblImg.text=InitialStr;
        cell.lblImg.backgroundColor=bgColor;

    }
    NSString *strStatus=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"status"]];
    
    cell.ContactImage.layer.cornerRadius=cell.ContactImage.frame.size.height/2;
    cell.ContactImage.clipsToBounds=YES;
    cell.lblImg.layer.cornerRadius=cell.lblImg.frame.size.height/2;
    cell.lblImg.clipsToBounds=YES;
      if ([strStatus isEqualToString:@"(null)"])
    {
        cell.ContactStatus.text=@"Hey there! I am using Zoechat";
    }
    else
    {
        cell.ContactStatus.text=strStatus;
    }
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        
        cell.tintColor=color;
        
        
        
    }
    else
    {
        cell.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        
        
        
        
    }
    cell.ContactStatus.font=[UIFont fontWithName:FONT_NORMAL size:14];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactCell *cell = (ContactCell *)[tableView cellForRowAtIndexPath:indexPath];

    NSArray *getArray = [arrForTableView objectAtIndex:indexPath.row];
    NSDictionary *dict = [arrForTableView objectAtIndex:indexPath.row];
    participateName = [dict valueForKey:@"registeredname"];
    NSLog(@"%@",getArray);
    NSLog(@"%@",CheckPartiIDS);
    if ([self.title isEqualToString:@"Add Paricipants"])
    {
        
        NSString *cheZEOStr = [getArray valueForKey:@"zoechatid"];
        if ([CheckPartiIDS containsObject:cheZEOStr])
        {
            NSLog(@"Already Added");
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            // Configure for text only and offset down
            hud.mode = MBProgressHUDModeText;
            hud.label.text = @"Already Added";
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hideAnimated:YES afterDelay:3];
            
        }
        else{
            selectedIndex = indexPath;
            [self showPopView];
            
            
        }
    }
        else if ([self.title isEqualToString:@"New Group"])
        {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                [arrSelectedContacts addObject:[arrForTableView objectAtIndex:indexPath.row]];
        }
        else if ([self.title isEqualToString:@"New BroadCast"])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [arrSelectedContacts addObject:[arrForTableView objectAtIndex:indexPath.row]];
        }

    }


-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactCell *cell = (ContactCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryNone;
    [arrSelectedContacts removeObject:[arrForTableView objectAtIndex:indexPath.row]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)onBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)onNext:(id)sender
{
    if (arrSelectedContacts.count!=0)
    {
        if (appDelegate.strGroupIdToAddParticipants.length!=0)
        {
            
        }
        else if([self.title isEqualToString:@"New BroadCast"])
        {
            
            if (arrSelectedContacts.count<=1)
            {
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                
                // Configure for text only and offset down
                hud.mode = MBProgressHUDModeText;
                
                hud.label.text = [NSString stringWithFormat:@"Please select atlease two recipients"];
                hud.margin = 10.f;
                hud.yOffset = 150.f;
                hud.label.adjustsFontSizeToFitWidth=YES;
                hud.removeFromSuperViewOnHide = YES;
                [hud hideAnimated:YES afterDelay:3];
            }

            else
            {
                
                NSString *strContent=[NSString stringWithFormat:@"You Created a broadcast list with %lu recipients",(unsigned long)arrSelectedContacts.count];
                NSLog(@"%@",strContent);
                long long milliSeconds=[self convertDateToMilliseconds:[NSDate date]];
                NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
                
                NSUUID *uuid=[NSUUID UUID];
                NSString *strMgsId=[NSString stringWithFormat:@"%@",uuid];
                appDelegate.BCarrParticipants=arrSelectedContacts;
                NSLog(@"%@",appDelegate.BCarrParticipants);

                
                NSMutableArray *arrPartis=[[NSMutableArray alloc]init];
                NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
                [dictParti setObject:appDelegate.strID forKey:@"participantId"];
                [dictParti setObject:@"0" forKey:@"isAdmin"];
                
                [arrPartis addObject:dictParti];
                
                for (int i=0; i<arrSelectedContacts.count; i++)
                {
                    NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
                    NSDictionary *dictCont=[arrSelectedContacts objectAtIndex:i];
                    NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"zoechatid"]];
                    [dictParti setObject:strPhone forKey:@"participantId"];
                    [dictParti setObject:@"0" forKey:@"isAdmin"];
                    [arrPartis addObject:dictParti];
                     BOOL success = [db_class insertParticipants:@"" userId:strPhone groupId:strBroadcastId joinedAt:strMilliSeconds addedBy:appDelegate.strID isAdmin:@"0"];
                }
                
                
                NSError *error;
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrPartis options:0 error:&error];
                NSString *jsonString;
                
                if (! jsonData)
                {
                    NSLog(@"Got an error: %@", error);
                }
                else
                {
                    jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                }
                
                jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
                jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                NSString* encodedString = [jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                
                NSString *strGroupName;
                
                strGroupName=[NSString stringWithFormat:@"%ld recipients",(unsigned long)arrSelectedContacts.count];
                    [dictParam setValue:appDelegate.strID forKey:@"from"];
                    [dictParam setValue:strGroupName forKey:@"groupName"];
                    [dictParam setValue:strBroadcastId forKey:@"groupId"];
                    [dictParam setValue:@"" forKey:@"groupImage"];
                    [dictParam setValue:jsonString forKey:@"participants"];
               
            
                
                

                
                

            
            BOOL success=[db_class insertChats:@"" chatRoomId:strBroadcastId chatRoomType:@"2" sender:@"" lastMessage:strContent lastMessageStatus:@"delivered" lastMessagesTime:strMilliSeconds lastMessagesType:@"header" unReadCount:@"1" groupName:strGroupName groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
            
            BOOL success1=[db_class insertChatMessages:strMgsId userId:appDelegate.strID groupId:strBroadcastId chatRoomType:@"2" content:strContent contentType:@"header" contentStatus:@"delivered" sender:@"" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];

            [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
            
            
            UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            MainTabBarVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainTabBarVC"];
            [self.navigationController pushViewController:hme animated:YES];
            }
        }
        else
        {
            [self performSegueWithIdentifier:@"CreateGroup_GetNameImage" sender:self];
        }
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        
        hud.label.text = [NSString stringWithFormat:@"Please select atlease one participant"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.label.adjustsFontSizeToFitWidth=YES;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:3];
    }
}
-(long long)convertDateToMilliseconds:(NSDate *)date
{
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    long long milliseconds=timeInSeconds*1000;
    return milliseconds;
}
-(NSDate *)convertMillisecondsToDate:(long long)milliSeconds
{
    NSDate *dat = [NSDate dateWithTimeIntervalSince1970:(milliSeconds / 1000.0)];
    return dat;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"CreateGroup_GetNameImage"])
    {
        GetNameImageVC *parti=[segue destinationViewController];
        parti.arrParticipants=arrSelectedContacts;
    }
}

#pragma mark -
#pragma mark - Search Delegate

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    
    [arrForTableView removeAllObjects];
    arrForTableView=[NSMutableArray arrayWithArray:arrContacts];
    
    if (arrForTableView.count!=0)
    {
        [_tableContacts reloadData];
        
    }
    else
    {
        //_tablePatientDetails.hidden=YES;
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [arrForTableView removeAllObjects];
    
    NSString *strValue = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    
    if(![strValue isEqualToString:@""])
    {
        [arrForTableView removeAllObjects];
        
        
        for(NSDictionary * dictValue in arrContacts)
        {
            NSString * strGroupName = [dictValue objectForKey:@"name"];
            
            NSRange GroupName=[[strGroupName lowercaseString] rangeOfString:[strValue lowercaseString]];
            
            if(GroupName.location != NSNotFound)
                [arrForTableView addObject:dictValue];
        }
        
    }
    else if(strValue != nil && [strValue isEqualToString:@""])
    {
        [arrForTableView removeAllObjects];
        arrForTableView=[NSMutableArray arrayWithArray:arrContacts];
    }
    
    
    if (range.length==1 && range.location==0)
    {
        [arrForTableView removeAllObjects];
        arrForTableView=[NSMutableArray arrayWithArray:arrContacts];
    }
    
    
    
    if (arrForTableView.count!=0)
    {
        [_tableContacts reloadData];
        
    }
    
    
    
    
    return YES;
}


#pragma mark -
#pragma mark - Font

-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)view;
        [btn setFont:[UIFont fontWithName:fontFamily size:[[btn font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *txtFld = (UITextField *)view;
        [txtFld setFont:[UIFont fontWithName:fontFamily size:[[txtFld font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextView class]])
    {
        UITextView *txtVw = (UITextView *)view;
        [txtVw setFont:[UIFont fontWithName:fontFamily size:[[txtVw font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
    
}
-(void)showPopView
{
    UIView *viewForAudioOrVideo=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+self.navigationController.navigationBar.frame.size.height+50)];
    viewForAudioOrVideo.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7f];
    viewForAudioOrVideo.tag=9087650;
    [self.navigationController.view addSubview:viewForAudioOrVideo];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onRemoveViewAudioVideo:)]; // this is the current problem like a lot of people out there...
    [viewForAudioOrVideo addGestureRecognizer:tap];
    
    UIView *viewForWhiteBG=[[UIView alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)-140, (viewForAudioOrVideo.frame.size.height/2)-50, 280, 100)];
    viewForWhiteBG.backgroundColor=[UIColor whiteColor];
    viewForWhiteBG.layer.cornerRadius=3;
    viewForWhiteBG.clipsToBounds=YES;
    [viewForAudioOrVideo addSubview:viewForWhiteBG];
    
    UILabel *InfoLbl=[[UILabel alloc]initWithFrame:CGRectMake(20, 0, viewForWhiteBG.frame.size.width-40, 50)];
    ContactCell *cell = (ContactCell *)[_tableContacts cellForRowAtIndexPath:selectedIndex];
    NSString *labelNameStr=cell.ContactName.text;
    
    //    NSMutableAttributedString *yourAttributedString = [[NSMutableAttributedString alloc] initWithString:labelNameStr];
    //    NSString *boldString = labelNameStr;
    //    NSRange boldRange = [labelNameStr rangeOfString:boldString];
    //    [yourAttributedString addAttribute: NSFontAttributeName value:[UIFont boldSystemFontOfSize:12] range:boldRange];
    NSString *alertString=[NSString stringWithFormat:@"Are you sure you want to add %@ in Group",labelNameStr];
    [InfoLbl setText: alertString];
    InfoLbl.numberOfLines = 0;
    InfoLbl.textAlignment = NSTextAlignmentCenter;
    [InfoLbl setFont:[UIFont fontWithName:FONT_NORMAL size:14]];
    [viewForWhiteBG addSubview:InfoLbl];
    
    UIButton *cancel=[[UIButton alloc]initWithFrame:CGRectMake(35, 51, 50, 30)];
    [cancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancel setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    cancel.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:14];
    [cancel addTarget:self action:@selector(ClickOnCancel:) forControlEvents:UIControlEventTouchUpInside];
    [viewForWhiteBG addSubview:cancel];
    
    UIButton *ok=[[UIButton alloc]initWithFrame:CGRectMake(215, 51, 50, 30)];
    [ok setTitle:@"OK" forState:UIControlStateNormal];
    [ok setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    ok.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:14];
    [ok addTarget:self action:@selector(clickOnOk:) forControlEvents:UIControlEventTouchUpInside];
    [viewForWhiteBG addSubview:ok];
}
- (IBAction)ClickOnCancel:(id)sender {
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    [viewForAudioOrVideo removeFromSuperview];
}

- (IBAction)clickOnOk:(id)sender {
    ContactCell *cell = (ContactCell *)[self.tableContacts cellForRowAtIndexPath:selectedIndex];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    [arrSelectedContacts addObject:[arrForTableView objectAtIndex:selectedIndex.row]];
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    [viewForAudioOrVideo removeFromSuperview];
    [self getParticipants];

}
-(void)getParticipants
{
    NSMutableArray *arrPartis=[[NSMutableArray alloc]init];
    NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
    
    for (int i=0; i<arrSelectedContacts.count; i++)
    {
        NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
        NSDictionary *dictCont=[arrSelectedContacts objectAtIndex:i];
        NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"zoechatid"]];
        [dictParti setObject:strPhone forKey:@"participantId"];
        [arrPartis addObject:dictParti];
        
        
    }
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrPartis options:0 error:&error];
    NSString *jsonString;
    
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    NSString* encodedString = [jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *trimmString=[NSString stringWithFormat:@"%@",jsonString];
    // [{\"participantId\":\"919786543211\"}]
    NSCharacterSet *unwantedChars = [NSCharacterSet characterSetWithCharactersInString:@"participantId""\"[]""{}"":"];
    NSString *requiredString = [[trimmString componentsSeparatedByCharactersInSet:unwantedChars] componentsJoinedByString: @""];
    NSLog(@"%@",requiredString);
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:appDelegate.strID forKey:@"from"];
    [dictParam setValue:appDelegate.strGroupIdToAddParticipants forKey:@"groupId"];
    [dictParam setValue:requiredString forKey:@"participantId"];
    
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
     NSString *fromname = [defaults valueForKey:@"name"];
  
    
    NSLog(@"Zeocatid:%@ ",appDelegate.strID);
     NSLog(@"fromName:%@ ",fromname);
     NSLog(@"groupId:%@ ",appDelegate.strGroupIdToAddParticipants);
     NSLog(@"groupName:%@ ",getGroupName);
     NSLog(@"groupImage:%@ ",getGroupImg);
     NSLog(@"participantId:%@ ",requiredString);
    NSLog(@"participantName:%@",participateName);
    [appDelegate.socket emit:@"requestParticipants" with:@[@{@"fromId": appDelegate.strID, @"fromName":fromname,@"groupId":appDelegate.strGroupIdToAddParticipants,@"groupName":getGroupName,@"groupImage":getGroupImg,@"participantId":requiredString,@"participantName":participateName}]];
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MainTabBarVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainTabBarVC"];
    [self.navigationController pushViewController:hme animated:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
   /*
    [[appDelegate.socket emitWithAck:@"addParticipants" with:@[@{@"from": appDelegate.strID,@"groupId": appDelegate.strGroupIdToAddParticipants,@"participantId":requiredString,@"groupName": getGroupName,@"groupImage": getGroupImg }]] timingOutAfter:2 callback:^(NSArray* data) {
        NSLog(@"%@",data);
        appDelegate.isPart=requiredString;
        NSString *checkAck=[data objectAtIndex:0];
        NSInteger getack=[checkAck integerValue];
        NSLog(@"%ld",(long)getack);
        if (getack == 1)
        {
            NSDictionary *dictName=[db_class getUserNameForNotification:[dictParam valueForKey:@"participantId"]];
            NSString *strNameToShow;
            
            if ([[dictName allKeys] containsObject:@"name"])
            {
                strNameToShow=[NSString stringWithFormat:@"%@",[dictName valueForKey:@"name"]];
            }
            else
            {
                strNameToShow=[NSString stringWithFormat:@"%@",[dictParam valueForKey:@"participantId"]];
            }
            
            
            
            NSString *strContent=[NSString stringWithFormat:@"You added %@",strNameToShow];
            long long milliSeconds=[self convertDateToMilliseconds:[NSDate date]];
            NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
            
            NSUUID *uuid=[NSUUID UUID];
            NSString *strMgsId=[NSString stringWithFormat:@"%@",uuid];
            
            BOOL isAvailinChatsTable=NO;
            isAvailinChatsTable=[db_class doesChatExist:appDelegate.strGroupIdToAddParticipants];
            
            BOOL success=NO;
            if (isAvailinChatsTable==NO)
            {
                success=[db_class insertChats:@"" chatRoomId:appDelegate.strGroupIdToAddParticipants chatRoomType:@"1" sender:@"" lastMessage:strContent lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"header" unReadCount:@"1" groupName:getGroupName  groupImage:getGroupImg sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
                if (success==YES)
                {
                    isAvailinChatsTable=YES;
                }
            }
            else
            {
                BOOL success1 = [db_class updateLastMessage: appDelegate.strGroupIdToAddParticipants sender:@"" lastMessage:strContent lastMessageStatus:@"delivered" lastMessageTime:strMilliSeconds lastMessagesType:@"header" unReadCount:@"1"];
                NSLog(@"success");
            }
            
            
            BOOL succe=[db_class insertChatMessages:strMgsId userId:appDelegate.strID groupId:appDelegate.strGroupIdToAddParticipants chatRoomType:@"1" content:strContent contentType:@"header" contentStatus:@"delivered" sender:@"" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0"showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
            
            //[self.navigationController popViewControllerAnimated:YES];
            
            UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            MainTabBarVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainTabBarVC"];
            [self.navigationController pushViewController:hme animated:YES];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
            
        }
        
        
        
        else
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            // Configure for text only and offset down
            hud.mode = MBProgressHUDModeText;
            hud.label.text = @"No Ack";
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hideAnimated:YES afterDelay:3];
        }
    }];*/
    
    
    
    
    //    NSDictionary *dictName=[db_class getUserNameForNotification:[dictParam valueForKey:@"participantId"]];
    //    NSString *strNameToShow;
    //
    //    if ([[dictName allKeys] containsObject:@"name"])
    //    {
    //        strNameToShow=[NSString stringWithFormat:@"%@",[dictName valueForKey:@"name"]];
    //    }
    //    else
    //    {
    //        strNameToShow=[NSString stringWithFormat:@"%@",[dictParam valueForKey:@"participantId"]];
    //    }
    //
    //
    //
    //    NSString *strContent=[NSString stringWithFormat:@"You added %@",strNameToShow];
    //    long long milliSeconds=[self convertDateToMilliseconds:[NSDate date]];
    //    NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
    //
    //    NSUUID *uuid=[NSUUID UUID];
    //    NSString *strMgsId=[NSString stringWithFormat:@"%@",uuid];
    //
    //    BOOL isAvailinChatsTable=NO;
    //    isAvailinChatsTable=[db_class doesChatExist:appDelegate.strGroupIdToAddParticipants];
    //
    //    BOOL success=NO;
    //    if (isAvailinChatsTable==NO)
    //    {
    //        success=[db_class insertChats:@"" chatRoomId:appDelegate.strGroupIdToAddParticipants chatRoomType:@"1" sender:@"" lastMessage:strContent lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"header" unReadCount:@"0" groupName:getGroupName  groupImage:getGroupImg sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
    //        if (success==YES)
    //        {
    //            isAvailinChatsTable=YES;
    //        }
    //    }
    //    else
    //    {
    //        BOOL success1 = [db_class updateLastMessage: appDelegate.strGroupIdToAddParticipants sender:@"" lastMessage:strContent lastMessageStatus:@"delivered" lastMessageTime:strMilliSeconds lastMessagesType:@"header" unReadCount:@"0"];
    //        NSLog(@"success");
    //    }
    //
    //
    //    BOOL succe=[db_class insertChatMessages:strMgsId userId:appDelegate.strID groupId:appDelegate.strGroupIdToAddParticipants chatRoomType:@"1" content:strContent contentType:@"header" contentStatus:@"delivered" sender:@"" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0"];
    //
    //    //[self.navigationController popViewControllerAnimated:YES];
    //
    //    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    MainTabBarVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainTabBarVC"];
    //    [self.navigationController pushViewController:hme animated:YES];
    
}
- (void) onRemoveViewAudioVideo: (UIGestureRecognizer *) gesture
{
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    [viewForAudioOrVideo removeFromSuperview];
}

@end
