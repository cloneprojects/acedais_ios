//
//  AppDelegate.m
//  ZoeChat
//
//  Created by Pyramidions Solution on 29/12/16.
//  Copyright © 2016 Pyramidions Solution. All rights reserved.

#import "AppDelegate.h"
#import "MainTabBarVC.h"
#import <AWSS3/AWSS3.h>
#import "Constants.h"
#import "AFNHelper.h"
#import "DatabaseMigrator.h"
#import "AbstractRepository.h"
#import "DBClass.h"
#import "VoiceCallVC.h"
#import <PushKit/PushKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import<CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCall.h>
#import <Stickerpipe/Stickerpipe.h>
#import <SSKeychain/SSKeychain.h>
#import "NSString+MD5.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
@import CallKit;

@import Firebase;
@import FirebaseMessaging;
@import UserNotifications;

// 9566192135


@interface AppDelegate ()<UNUserNotificationCenterDelegate, FIRMessagingDelegate, PKPushRegistryDelegate, CXProviderDelegate>
{
    NSMutableArray *arrLocalNoti;
    DBClass *db_class;
    NSTimer *CallTimer;
    CXProvider *callkitProvider;
}
@property (nonatomic, strong) CTCallCenter* callCenter;

@end

@implementation AppDelegate
@synthesize socket, strAlertTitle, strCountryCode, strMobileNumber, strOTP, strLoginStatus, strName, strID, strDeviceToken,strTokenVOIP, strInternetMode, strProfilePic, strStatus, isFrmLocationPage,strUUID, dictInComomgCall, strCallTime, strGroupIdToAddParticipants, strGroupName,isBroadCast,BCarrParticipants,uniqueID,isDriveLogin;

@synthesize heightStr;
@synthesize dynamicRowHeight;
@synthesize ZOEID,RoomTYPE,isContact,notiSoundPath,isStarred,isfromView,ViewDict,isPart,isDoodle;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    
   

//    NSURL *ubiq = [[NSFileManager defaultManager]
//                   URLForUbiquityContainerIdentifier:];
//    if (ubiq) {
//        NSLog(@"iCloud access at %@", ubiq);
//        // TODO: Load document...
//    } else {
//        NSLog(@"No iCloud access");
//    }

    [self setupDb];
    [self CreateTable];
    db_class=[[DBClass alloc]init];
    dictInComomgCall=[[NSMutableDictionary alloc]init];
    // Use Firebase library to configure APIs
    [FIRApp configure];
    //Fabric


    [Fabric with:@[[Crashlytics class]]];


    [GMSServices provideAPIKey:Google_Maps_API]; //Google Maps

    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc]initWithRegionType:AWSRegionAPNortheast2 identityPoolId:FILE_AWS_POOL_ID];
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionAPNortheast2 credentialsProvider:credentialsProvider];
    AWSServiceManager.defaultServiceManager.defaultServiceConfiguration = configuration;
    isFrmLocationPage=@"";

    strAlertTitle=@"Acedais";
    strDeviceToken=@"1234ABCD5678EFGH90IJ";
    strGroupIdToAddParticipants=@"";
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    strCountryCode=[defaults valueForKey:@"countryCode"];
    strMobileNumber=[defaults valueForKey:@"mobileNumber"];
    strID=[defaults valueForKey:@"_id"];
    strName=[defaults valueForKey:@"name"];
    strProfilePic=[defaults valueForKey:@"image"];
    strStatus=[defaults valueForKey:@"status"];
    strLoginStatus=[defaults valueForKey:@"LoginStatus"];
    isfromView=@"No";
    isBroadCast=@"NO";
    isDoodle=@"NO";


    CXProviderConfiguration *cxconfiguration = [[CXProviderConfiguration alloc] initWithLocalizedName:@"ZoeChat"];


    callkitProvider = [[CXProvider alloc] initWithConfiguration: cxconfiguration];
    [callkitProvider setDelegate:self queue:nil];

    //Notification


    NSString *getPage = [defaults objectForKey:@"isNotificationPage"];
    if ([getPage isEqualToString:@"YES"])
    {

    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"msgON"];
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"grpON"];
        [[NSUserDefaults standardUserDefaults] setObject:@"Default" forKey:@"msgSOUND"];
        [[NSUserDefaults standardUserDefaults] setObject:@"Default" forKey:@"grpSOUND"];
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"selectedCell1"];
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"selectedCell2"];

        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    NSUserDefaults *defaults12 = [NSUserDefaults standardUserDefaults];
    [defaults12 setObject:@"YES" forKey:@"isDataPage"];
    [defaults12 synchronize];


    /*

    NSURL* url = [[NSURL alloc] initWithString:SOCKET_URL];

    socket = [[SocketIOClient alloc] initWithSocketURL:url config:@{@"log": @YES, @"forcePolling": @YES,@"reconnects":@YES,@"reconnectAttempts":@-1,@"reconnectWait":@5}];
    
    */
    
    
    if ([strLoginStatus isEqualToString:@"LoggedIn"])
    {

  /*      [socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack)
         {
             [socket emit:@"online" with:@[@{@"id":strID}]];
             [self sendingMessages];

             //            [socket emit:@"ack" with:@[@{@"userid":strID,@"uniqueid":uniqueID}]];
         }];
        [socket connect];*/

        UINavigationController *navController = (UINavigationController *)self.window.rootViewController;
        UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MainTabBarVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainTabBarVC"];
        [navController pushViewController:hme animated:NO];
        [self.window makeKeyAndVisible];
    }


    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];

        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        // For iOS 10 data message (sent via FCM)
        [FIRMessaging messaging].remoteMessageDelegate = self;
#endif
    }

    [[UIApplication sharedApplication] registerForRemoteNotifications];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAppBadgeCount) name:@"Refresh_App_Badge_Count" object:nil];
    [self handleAppBadgeCount];

    [self voipRegistration];
    [self handleCall];
    //Theme

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    [[UITextField appearance] setTintColor:[UIColor blackColor]];
    [[UITextField appearance] setTextColor:[UIColor blackColor]];
    //    [[UIButton appearance] setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];


    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        [[UIApplication sharedApplication] keyWindow].tintColor = color;
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]]; // this will change the back button tint
        [[UINavigationBar appearance] setBarTintColor:color];
        [[UINavigationBar appearance]  setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        [[UITabBar appearance] setTintColor:color];
        [[UITabBarItem appearance]setTitleTextAttributes:@{NSForegroundColorAttributeName:color} forState:UIControlStateSelected];
        [[[[UIApplication sharedApplication] delegate] window] setTintColor:color];



    }
    else
    {
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]]; // this will change the back button tint
        [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0]];
        [[UINavigationBar appearance]  setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        [[UITabBar appearance] setTintColor:[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0]];

        [[UITabBarItem appearance]setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0]} forState:UIControlStateSelected];

        [[[[UIApplication sharedApplication] delegate] window] setTintColor:[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0]];






    }
    //Sticker
    [STKStickersManager initWithApiKey:@"c67495c61079060965c5c6b47064e452"];

    [STKStickersManager setStartTimeInterval];
    [STKStickersManager setUserKey: [self userId]];

    [STKStickersManager setPriceBWithLabel: @"0.99 USD" andValue: 0.99f];
    [STKStickersManager setPriceCwithLabel: @"1.99 USD" andValue: 1.99f];

    return YES;
}



- (NSString*)getUniqueDeviceIdentifierAsString {
    NSString* appName = [[[NSBundle mainBundle] infoDictionary] objectForKey: (NSString*) kCFBundleNameKey];
    
    NSString* strApplicationUUID = [SSKeychain passwordForService: appName account: @"incoding"];
    if (strApplicationUUID == nil) {
        strApplicationUUID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [SSKeychain setPassword: strApplicationUUID forService: appName account: @"incoding"];
    }
    
    return strApplicationUUID;
}

- (NSString*)userId {
    NSString* currentDeviceId = [self getUniqueDeviceIdentifierAsString];
    NSString* appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    return [[currentDeviceId stringByAppendingString: appVersionString] MD5Digest];
}
- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    
    // TODO: If necessary send token to application server.
}

- (void)connectToFcm {
    // Won't connect since there is no token
    if (![[FIRInstanceID instanceID] token]) {
        return;
    }
    // Disconnect previous FCM connection if it exists.
    [[FIRMessaging messaging] disconnect];
    
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
    }];
}

// With "FirebaseAppDelegateProxyEnabled": NO
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    //Format token as you need:
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    strDeviceToken=token;
    NSLog(@"==========%@",strDeviceToken);
    
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken
                                        type:FIRInstanceIDAPNSTokenTypeSandbox];
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken
                                        type:FIRInstanceIDAPNSTokenTypeProd];
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"%@",userInfo);
    if ([[userInfo valueForKey:@"pushType"]isEqualToString:@"inc"])
    {
        NSLog(@"%@ endCallRequest push %@",NSStringFromClass([self class]),userInfo);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DisConnect_Call" object:self];
    }
    else if ([[userInfo valueForKey:@"pushType"]isEqualToString:@"out"])
    {
        //         [[NSNotificationCenter defaultCenter] postNotificationName:@"DisConnect_Call" object:self];
        NSLog(@"%@ endCallRequest push %@",NSStringFromClass([self class]),userInfo);
        //         [self EndCall];
    }
    else if ([[userInfo valueForKey:@"pushType"]isEqualToString:@"GroupAdd"])
    {
        NSString *strMess=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"content"]];
        NSDictionary *dictName=[db_class getUserNameForNotification:[userInfo valueForKey:@"createdBy"]];
        NSString *strNameToShow;

        
        if ([[userInfo valueForKey:@"pushType"]isEqualToString:@"GroupAdd"])
        {
            NSString *strGroupName=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"groupName"]];
            NSString *strContent=[NSString stringWithFormat:@"added you"];
            NSDictionary *dictName=[db_class getUserNameForNotification:[userInfo valueForKey:@"createdBy"]];
            NSString *strNameToShow;
        }
        if ([[dictName allKeys] containsObject:@"name"])
        {
            strNameToShow=[NSString stringWithFormat:@"%@",[dictName valueForKey:@"name"]];
        }
        else
        {
            strNameToShow=[NSString stringWithFormat:@"+%@",[userInfo valueForKey:@"createdBy"]];
        }
        
        NSDictionary *dictNam=[db_class getSingleChat:[userInfo valueForKey:@"groupId"]];
        strMess=[NSString stringWithFormat:@"%@: %@",strNameToShow,strMess];
        strNameToShow=[NSString stringWithFormat:@"%@",[dictNam valueForKey:@"groupName"]];
        NSLog(@"%@",ZOEID);
        NSString *checkID = [userInfo valueForKey:@"groupId"];
        NSLog(@"%@",checkID);
        if ([ZOEID isEqualToString: checkID ])
        {
            
        }
        else
        {
            NSString *ismsgON = [[NSUserDefaults standardUserDefaults]stringForKey:@"grpON"];
            
            if ([ismsgON isEqualToString:@"1"])
            {
                UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
                content.userInfo = userInfo;
                content.title=strNameToShow;
                content.body=strMess;
                NSString *notificationSound = [[NSUserDefaults standardUserDefaults]stringForKey:@"grpSOUND"];
                if ([notificationSound isEqualToString:@"Default"])
                {
                    content.sound = [UNNotificationSound defaultSound];
                }
                else
                {
                    [self playSound:notificationSound :@"mp3"];
                }
                
                [content setValue:@(YES) forKeyPath:@"shouldAlwaysAlertWhileAppIsForeground"];
                NSLog(@"%@",content);
                UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"Notif" content:content trigger:nil];
                [[UNUserNotificationCenter currentNotificationCenter] addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error)
                 {
                    NSLog(@"Error:%@", error);
                }];
            }
        }
    }
    else
    {
        NSString *strPayload=[userInfo valueForKey:@"payload"];
        NSData *data = [strPayload dataUsingEncoding:NSUTF8StringEncoding];
        id dictPayload = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSString *strChatRmTyp=[NSString stringWithFormat:@"%@",[dictPayload valueForKey:@"chatRoomType"]];
        NSString *strMess=[NSString stringWithFormat:@"%@",[dictPayload valueForKey:@"content"]];
        NSDictionary *dictName=[db_class getUserNameForNotification:[userInfo valueForKey:@"fromId"]];
        NSString *strNameToShow;
        //         }
        
        if ([strChatRmTyp isEqualToString:@"0"])
        {
            NSDictionary *dictNam=[db_class getSingleChat:[dictPayload valueForKey:@"from"]];
            strMess=[NSString stringWithFormat:@"%@",strMess];
            if ([[dictName allKeys] containsObject:@"name"])
            {
                strNameToShow=[NSString stringWithFormat:@"%@",[dictName valueForKey:@"name"]];
            }
            // strNameToShow=[NSString stringWithFormat:@"%@",[dictNam valueForKey:@"name"]];
            NSLog(@"%@",ZOEID);
            NSString *checkID = [userInfo valueForKey:@"fromId"];
            NSLog(@"%@",checkID);
            if ([ZOEID isEqualToString: checkID ])
            {
                
            }
            else if (![ZOEID isEqualToString: checkID])
            {
                NSString *ismsgON = [[NSUserDefaults standardUserDefaults]stringForKey:@"msgON"];
                
                if ([ismsgON isEqualToString:@"1"])
                {
                    UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
                    content.userInfo = userInfo;
                    content.title=strNameToShow;
                    content.body=strMess;
                    NSString *notificationSound = [[NSUserDefaults standardUserDefaults]stringForKey:@"msgSOUND"];
                    if ([notificationSound isEqualToString:@"Default"])
                    {
                        content.sound = [UNNotificationSound defaultSound];
                        
                    }
                    else
                    {
                        [self playSound:notificationSound :@"mp3"];
                        
                    }
                    
                    [content setValue:@(YES) forKeyPath:@"shouldAlwaysAlertWhileAppIsForeground"];
                    NSLog(@"%@",content);
                    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"Notif" content:content trigger:nil];
                    [[UNUserNotificationCenter currentNotificationCenter] addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                        NSLog(@"Error:%@", error);
                    }];
                }
            }
        }
        else
        {
            if ([[userInfo valueForKey:@"pushType"]isEqualToString:@"GroupAdd"])
            {
                NSString *strGroupName=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"groupName"]];
                NSString *strContent=[NSString stringWithFormat:@"added you"];
                NSDictionary *dictName=[db_class getUserNameForNotification:[userInfo valueForKey:@"createdBy"]];
                NSString *strNameToShow;
                if ([[dictName allKeys] containsObject:@"name"])
                {
                    strNameToShow=[NSString stringWithFormat:@"%@",[dictName valueForKey:@"name"]];
                }
                else
                {
                    strNameToShow=[NSString stringWithFormat:@"+%@",[userInfo valueForKey:@"createdBy"]];
                }
                NSString *strMess=[NSString stringWithFormat:@"%@ %@",strNameToShow,strContent];
                UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
                content.userInfo = userInfo;
                content.title=strGroupName;
                content.body=strMess;
                NSString *notificationSound = [[NSUserDefaults standardUserDefaults]stringForKey:@"grpSOUND"];
                if ([notificationSound isEqualToString:@"Default"])
                {
                    content.sound = [UNNotificationSound defaultSound];
                    
                }
                else
                {
                    [self playSound:notificationSound :@"mp3"];
                    
                }
                
                
                
                
                
                [content setValue:@(YES) forKeyPath:@"shouldAlwaysAlertWhileAppIsForeground"];
                NSLog(@"%@",content);
                UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"Notif" content:content trigger:nil];
                [[UNUserNotificationCenter currentNotificationCenter] addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                    NSLog(@"Error:%@", error);
                }];
                
            }
            if ([[dictName allKeys] containsObject:@"name"])
            {
                strNameToShow=[NSString stringWithFormat:@"%@",[dictName valueForKey:@"name"]];
            }
            else
            {
                strNameToShow=[NSString stringWithFormat:@"+%@",[userInfo valueForKey:@"fromId"]];
            }
            
            if ([strChatRmTyp isEqualToString:@"1"])
            {
                NSDictionary *dictNam=[db_class getSingleChat:[dictPayload valueForKey:@"groupId"]];
                strMess=[NSString stringWithFormat:@"%@: %@",strNameToShow,strMess];
                strNameToShow=[NSString stringWithFormat:@"%@",[dictNam valueForKey:@"groupName"]];
                NSLog(@"%@",ZOEID);
                NSString *checkID = [dictPayload valueForKey:@"groupId"];
                NSLog(@"%@",checkID);
                if ([ZOEID isEqualToString: checkID ])
                {
                    
                }
                else
                {
                    NSString *ismsgON = [[NSUserDefaults standardUserDefaults]stringForKey:@"grpON"];
                    
                    if ([ismsgON isEqualToString:@"1"])
                    {
                        UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
                        content.userInfo = userInfo;
                        content.title=strNameToShow;
                        content.body=strMess;
                        NSString *notificationSound = [[NSUserDefaults standardUserDefaults]stringForKey:@"grpSOUND"];
                        if ([notificationSound isEqualToString:@"Default"])
                        {
                            content.sound = [UNNotificationSound defaultSound];
                            
                        }
                        else
                        {
                            [self playSound:notificationSound :@"mp3"];
                            
                        }
                        
                        
                        
                        [content setValue:@(YES) forKeyPath:@"shouldAlwaysAlertWhileAppIsForeground"];
                        NSLog(@"%@",content);
                        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"Notif" content:content trigger:nil];
                        [[UNUserNotificationCenter currentNotificationCenter] addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                            NSLog(@"Error:%@", error);
                        }];
                    }
                }
            }
            
            
        }
        // else
        // {
        //     NSString *strPayload=[userInfo valueForKey:@"payload"];
        //     NSData *data = [strPayload dataUsingEncoding:NSUTF8StringEncoding];
        //     id dictPayload = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        //
        //     NSString *strChatRmTyp=[NSString stringWithFormat:@"%@",[dictPayload valueForKey:@"chatRoomType"]];
        //     NSString *strMess=[NSString stringWithFormat:@"%@",[dictPayload valueForKey:@"content"]];
        //     NSDictionary *dictName=[db_class getUserNameForNotification:[userInfo valueForKey:@"fromId"]];
        //     NSString *strNameToShow;
        
        //      }
        
    }
    completionHandler(UIBackgroundFetchResultNewData);
}


- (void)playSound :(NSString *)fName :(NSString *) ext{
    SystemSoundID audioEffect;
    NSString *path = [[NSBundle mainBundle] pathForResource : fName ofType :ext];
    if ([[NSFileManager defaultManager] fileExistsAtPath : path]) {
        NSURL *pathURL = [NSURL fileURLWithPath: path];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef) pathURL, &audioEffect);
        AudioServicesPlaySystemSound(audioEffect);
    }
    else {
        NSLog(@"error, file not found: %@", path);
    }
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler
{
    NSLog(@"%@",notification.request.content);
    // Notification arrived while the app was in foreground
    completionHandler(UNNotificationPresentationOptionAlert);
    // This argument will make the notification appear in foreground
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSLog(@"Disconnected from FCM");
    if ([strLoginStatus isEqualToString:@"LoggedIn"])
    {
        
        [socket emit:@"offline" with:@[@{@"id":strID}]];
/*        NSString *strReceiveMessage=[NSString stringWithFormat:@"%@:receiveMessage",strID];
        [socket off:strReceiveMessage];
        NSString *strACK=[NSString stringWithFormat:@"%@:ack",strID];
        [socket off:strACK];
        [socket off:@"connect"];*/
        
        [socket disconnect];
        
    }
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    /*
    if ([strLoginStatus isEqualToString:@"LoggedIn"])
    {
        NSURL* url = [[NSURL alloc] initWithString:SOCKET_URL];
        socket = [[SocketIOClient alloc] initWithSocketURL:url config:@{@"log": @YES, @"forcePolling": @YES,@"reconnects":@YES,@"reconnectAttempts":@-1,@"reconnectWait":@5}];
        [socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
            NSLog(@"socket connected");
            [socket emit:@"online" with:@[@{@"id":strID}]];
            [self sendingMessages];
        }];
        [socket connect];
        //
        //
    }
    */
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Unable to register for remote notifications: %@", error);
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
    if ([strLoginStatus isEqualToString:@"LoggedIn"])
    {
        NSURL* url = [[NSURL alloc] initWithString:SOCKET_URL];
        
        socket = [[SocketIOClient alloc] initWithSocketURL:url config:@{@"log": @NO, @"forcePolling": @YES}];
        
        [socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack)
         {
             [socket emit:@"online" with:@[@{@"id":strID}]];
             [self sendingMessages];
             
             //            [socket emit:@"ack" with:@[@{@"userid":strID,@"uniqueid":uniqueID}]];
         }];
        [socket connect];
    }

    
    if ([strLoginStatus isEqualToString:@"LoggedIn"])
    {
        //  [socket emit:@"online" with:@[@{@"id":strID}]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Ask_Refresh_Contacts" object:self];
        
        NSString *strReceiveMessage=[NSString stringWithFormat:@"%@:receiveMessage",strID];
        [socket on:strReceiveMessage callback:^(NSArray *dataReceiveMsg, SocketAckEmitter *ack)
         {
             BOOL success=NO;
             NSLog(@"%@",dataReceiveMsg);
             NSLog(@"%@",ack);
             
             
             for (int j=0; j<dataReceiveMsg.count; j++)
             {
                 NSDictionary *dictReceiveMsg=[dataReceiveMsg objectAtIndex:j];
                 NSString *strChatRoomType=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"chatRoomType"]];
                 NSString *strMsg=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"content"]];
                 NSString *strMsgFrom=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"from"]];
                 NSString *strMsgType=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"contentType"]];
                 NSString *strMsgTime=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"time"]];
                 NSString *strMsgId=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"messageId"]];
                 NSString *strLatitude=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"latitude"]];
                 NSString *strLogitude=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"longitude"]];
                 NSString *strGroupId=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"groupId"]];
                 NSString *strMsgFromUserId=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"from"]];
                 NSString *strContactName=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"contactName"]];
                 NSString *strContactNo=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"contactNumber"]];
                 NSString *strToMsg=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"to"]];
                 NSString *strShowPreview=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"showPreview"]];
                 NSString *strPreviewTitle=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"metaTitle"]];
                 NSString *strPreviewDescription=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"metaDescription"]];
                 NSString *strPreviewLogo=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"metaLogo"]];
                 
                 if ([strLatitude isEqualToString:@"(null)"])
                 {
                     strLatitude=@"";
                 }
                 if ([strLogitude isEqualToString:@"(null)"])
                 {
                     strLogitude=@"";
                 }
                 
                 if ([strChatRoomType isEqualToString:@"1"])
                 {
                     BOOL success2 = [db_class updatedeleteChatTable:strGroupId isDelete:@"0"];
                 }
                 else if([strChatRoomType isEqualToString:@"0"])
                     
                 {
                     strMsgFrom=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"from"]];
                     BOOL success2 = [db_class updatedeleteChatTable:strMsgFrom isDelete:@"0"];
                 }
                 BOOL isAvailinChatsTable=NO;
                 if ([strID isEqualToString:strMsgFrom])
                 {
                     isAvailinChatsTable=[db_class doesChatExist:strToMsg];
                     
                     if (isAvailinChatsTable==NO)
                     {
                         success=[db_class insertChats:@"" chatRoomId:strToMsg chatRoomType:strChatRoomType sender:@"1" lastMessage:strMsg lastMessageStatus:@"delivered" lastMessagesTime:strMsgTime lastMessagesType:strMsgType unReadCount:@"1" groupName:@"" groupImage:@"" sentBy:strMsgFrom isDelete:@"0" isLock:@"0" password:@""];
                         if (success==YES)
                         {
                             isAvailinChatsTable=YES;
                         }
                     }
                     else
                     {
                         if ([strChatRoomType isEqualToString:@"1"])
                         {
                             NSDictionary *dictSing=[db_class getSingleChat:strGroupId];
                             NSString *strgetCount=[NSString stringWithFormat:@"%@",[dictSing valueForKey:@"unreadCount"]];
                             NSInteger nUnreadCount=[strgetCount integerValue];
                             nUnreadCount=nUnreadCount+1;
                             NSString *strUnreadCount=[NSString stringWithFormat:@"%ld",(long)nUnreadCount];
                             
                             success=[db_class updateLastMessage:strGroupId sender:@"1" lastMessage:strMsg lastMessageStatus:@"delivered" lastMessageTime:strMsgTime lastMessagesType:strMsgType unReadCount:strUnreadCount];
                             //   NSLog(@"success");
                         }
                         else if ([strChatRoomType isEqualToString:@"0"])
                         {
                             
                             if ([strMsgFrom isEqualToString:strID])
                             {
                                 
                                 NSDictionary *dictSing=[db_class getSingleChat:strToMsg];
                                 NSString *strgetCount=[NSString stringWithFormat:@"%@",[dictSing valueForKey:@"unreadCount"]];
                                 NSInteger nUnreadCount=[strgetCount integerValue];
                                 nUnreadCount=nUnreadCount+1;
                                 NSString *strUnreadCount=[NSString stringWithFormat:@"%ld",(long)nUnreadCount];
                                 success=[db_class updateLastMessage:strToMsg sender:@"0" lastMessage:strMsg lastMessageStatus:@"delivered" lastMessageTime:strMsgTime lastMessagesType:strMsgType unReadCount:strUnreadCount];
                             }
                             else
                             {
                                 NSDictionary *dictSing=[db_class getSingleChat:strMsgFrom];
                                 NSString *strgetCount=[NSString stringWithFormat:@"%@",[dictSing valueForKey:@"unreadCount"]];
                                 NSInteger nUnreadCount=[strgetCount integerValue];
                                 nUnreadCount=nUnreadCount+1;
                                 NSString *strUnreadCount=[NSString stringWithFormat:@"%ld",(long)nUnreadCount];
                                 success=[db_class updateLastMessage:strMsgFrom sender:@"1" lastMessage:strMsg lastMessageStatus:@"delivered" lastMessageTime:strMsgTime lastMessagesType:strMsgType unReadCount:strUnreadCount];
                             }
                         }
                     }
                 }
                 else
                 {
                     isAvailinChatsTable=[db_class doesChatExist:strMsgFrom];
                     
                     if (isAvailinChatsTable==NO)
                     {
                         success=[db_class insertChats:@"" chatRoomId:strMsgFrom chatRoomType:strChatRoomType sender:@"1" lastMessage:strMsg lastMessageStatus:@"delivered" lastMessagesTime:strMsgTime lastMessagesType:strMsgType unReadCount:@"1" groupName:@"" groupImage:@"" sentBy:strMsgFrom isDelete:@"0" isLock:@"0" password:@""];
                         if (success==YES)
                         {
                             isAvailinChatsTable=YES;
                         }
                     }
                     else
                     {
                         if ([strChatRoomType isEqualToString:@"1"])
                         {
                             NSDictionary *dictSing=[db_class getSingleChat:strGroupId];
                             NSString *strgetCount=[NSString stringWithFormat:@"%@",[dictSing valueForKey:@"unreadCount"]];
                             NSInteger nUnreadCount=[strgetCount integerValue];
                             nUnreadCount=nUnreadCount+1;
                             NSString *strUnreadCount=[NSString stringWithFormat:@"%ld",(long)nUnreadCount];
                             
                             success=[db_class updateLastMessage:strGroupId sender:@"1" lastMessage:strMsg lastMessageStatus:@"delivered" lastMessageTime:strMsgTime lastMessagesType:strMsgType unReadCount:strUnreadCount];
                             //   NSLog(@"success");
                         }
                         else if ([strChatRoomType isEqualToString:@"0"])
                         {
                             NSDictionary *dictSing=[db_class getSingleChat:strMsgFrom];
                             NSString *strgetCount=[NSString stringWithFormat:@"%@",[dictSing valueForKey:@"unreadCount"]];
                             NSInteger nUnreadCount=[strgetCount integerValue];
                             nUnreadCount=nUnreadCount+1;
                             NSString *strUnreadCount=[NSString stringWithFormat:@"%ld",(long)nUnreadCount];
                             if ([strMsgFrom isEqualToString:strID])
                             {
                                 
                                 
                                 
                                 success=[db_class updateLastMessage:strToMsg sender:@"0" lastMessage:strMsg lastMessageStatus:@"delivered" lastMessageTime:strMsgTime lastMessagesType:strMsgType unReadCount:strUnreadCount];
                                 
                             }
                             else
                             {
                                 
                                 success=[db_class updateLastMessage:strMsgFrom sender:@"1" lastMessage:strMsg lastMessageStatus:@"delivered" lastMessageTime:strMsgTime lastMessagesType:strMsgType unReadCount:strUnreadCount];
                                 
                             }
                         }
                     }
                     
                 }
                 
                 long long milliSecond=[self convertDateToMilliseconds:[NSDate date]];
                 NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSecond];
                 
                 
                 if ([strMsgType isEqualToString:@"location"])
                 {
                     NSDate *now=[NSDate date];
                     NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                     [dateFormatter1 setDateFormat:@"yyyyMMddHHmmssSSS"];
                     NSString *localDateString = [dateFormatter1 stringFromDate:now];
                     //  NSLog(@"%@",localDateString);
                     
                     
                     NSString* strExt = @"PNG";
                     
                     NSString *UploadFileName=[NSString stringWithFormat:@"%@.%@",localDateString,strExt];
                     
                     
                     
                     NSURL *imageURL = [NSURL URLWithString:[strMsg stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                     
                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                         NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                         
                         dispatch_async(dispatch_get_main_queue(), ^{
                             // Update the UI
                             //image you want to upload
                             // UIImage* imageToUpload = [UIImage imageWithData:imageData];
                             UIImage *imageToUpload=[self compressImage:[UIImage imageWithData:imageData]];
                             //convert uiimage to
                             NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                             NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",UploadFileName]];
                             [UIImagePNGRepresentation(imageToUpload) writeToFile:filePath atomically:YES];
                             
                             
                             BOOL success1=[db_class insertChatMessages:strMsgId userId:strMsgFromUserId groupId:strGroupId chatRoomType:strChatRoomType content:UploadFileName contentType:strMsgType contentStatus:@"delivered" sender:@"1" sentTime:strMsgTime deliveredTime:strMilliSeconds seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:strLatitude longitude:strLogitude contactName:strContactName contactNumber:strContactNo checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                             
                             NSDictionary *theInfo =[NSDictionary dictionaryWithObjectsAndKeys:dataReceiveMsg,@"msgsArray", nil];
                             [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_SingleChat_ReceivedMsgs"
                                                                                 object:self
                                                                               userInfo:theInfo];
                             
                         });
                     });
                     
                     
                 }
                 else
                 {
                     
                     
                     NSLog(@"%@",strID);
                     if ([strID isEqualToString:strMsgFrom])
                     {
                         NSLog(@"%@",strMsgFrom);
                         success=[db_class insertChatMessages:strMsgId userId:strToMsg groupId:strGroupId chatRoomType:strChatRoomType content:strMsg contentType:strMsgType contentStatus:@"delivered" sender:@"0" sentTime:strMsgTime deliveredTime:strMilliSeconds seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:strLatitude longitude:strLogitude contactName:strContactName contactNumber:strContactNo checkStar:@"0" showPreview:strShowPreview linkTitle:strPreviewTitle linkLogo:strPreviewLogo linkDescription:strPreviewDescription];
                         
                     }
                     else
                     {
                         success=[db_class insertChatMessages:strMsgId userId:strMsgFromUserId groupId:strGroupId chatRoomType:strChatRoomType content:strMsg contentType:strMsgType contentStatus:@"delivered" sender:@"1" sentTime:strMsgTime deliveredTime:strMilliSeconds seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:strLatitude longitude:strLogitude contactName:strContactName contactNumber:strContactNo checkStar:@"0" showPreview:strShowPreview linkTitle:strPreviewTitle linkLogo:strPreviewLogo linkDescription:strPreviewDescription];
                     }
                 }
                 
                 
                 NSLog(@"%@",strID);
                 if ([strID isEqualToString:strMsgFrom])
                 {
                     NSLog(@"%@",strMsgFrom);
                     
                 }
                 
                 [socket emit:@"sendDelivered" with:@[@{@"messageId":strMsgId,@"from":strMsgFrom,@"to": strID,@"time":strMilliSeconds,@"chatRoomType":strChatRoomType,@"userid":strID}]];
                 
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_App_Badge_Count" object:self];
                 
                 if (![strMsgType isEqualToString:@"location"])
                 {
                     NSDictionary *theInfo =[NSDictionary dictionaryWithObjectsAndKeys:dataReceiveMsg,@"msgsArray", nil];
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_SingleChat_ReceivedMsgs"
                                                                         object:self
                                                                       userInfo:theInfo];
                     
                 }
                 
             }
             
         }];
        
        NSString *strACK=[NSString stringWithFormat:@"%@:ack",strID];
        [socket on:strACK callback:^(NSArray *dataACK, SocketAckEmitter *ack)
         {
             NSLog(@"%@",dataACK);
             for (int j=0; j<dataACK.count; j++)
             {
                 NSDictionary *dictACK=[dataACK objectAtIndex:j];
                 NSString *strMsgId=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"messageid"]];
                 NSString *strToWhomMsgd=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"to"]];
                 NSString *strMsgStatus=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"status"]];
                 NSString *strMsgTime=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"time"]];
                 NSString *strChatRoomTypeACK=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"chatRoomType"]];
                 NSString *strRoomId=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"chatRoomId"]];
                 NSString *strMsgType=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"contentType"]];
                 NSString *strMsg=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"content"]];
                 uniqueID=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"uniqueid"]];
                 if ([strChatRoomTypeACK isEqualToString:@"1"])
                 {
                     strToWhomMsgd=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"to"]];
                 }
                 BOOL success=NO;
                 BOOL success1=NO;
                 success=[db_class updateContentStatus:strMsgId contentStatus:strMsgStatus];
                 
                 if ([strMsgStatus isEqualToString:@"delivered"])
                 {
                     success=[db_class updateDeliveredTime:strMsgId deliveredTime:strMsgTime];
                     [socket emit:@"ack" with:@[@{@"userid":strID,@"uniqueid":uniqueID}]];
                     
                 }
                 else if ([strMsgStatus isEqualToString:@"read"])
                 {
                     success=[db_class updateSeenTime:strMsgId seenTime:strMsgTime];
                     [socket emit:@"ack" with:@[@{@"userid":strID,@"uniqueid":uniqueID}]];
                     
                 }
                 
                 NSDictionary *dict=[db_class getSingleChat:strToWhomMsgd];
                 
                 //  if ([[dict valueForKey:@"lastMessageTime"]isEqualToString:strMsgTime])
                 // {
                 success1=[db_class updateLastMsgStatus:strToWhomMsgd lastMsgStatus:strMsgStatus];
                 // }
                 
             }
             
             [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
             NSDictionary *theInfo =[NSDictionary dictionaryWithObjectsAndKeys:dataACK,@"ackArray", nil];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_SingleChat_acks"
                                                                 object:self
                                                               userInfo:theInfo];
         }];
        
        
        NSString *strGroup=[NSString stringWithFormat:@"%@:group",strID];
        [socket on:strGroup callback:^(NSArray *dataACK, SocketAckEmitter *ack)
         {
             NSLog(@"%@",dataACK);
             for (int j=0; j<dataACK.count; j++)
             {
                 NSDictionary *userInfo=[dataACK objectAtIndex:j];
                 NSString *strGroupId=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"groupId"]];
                 NSString *strTime=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"time"]];
                 NSString *strFrom=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"createdBy"]];
                 NSString *strGroupName=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"groupName"]];
                 NSString *strGroupImage=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"groupImage"]];
                 NSString *strContentType=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"contentType"]];
                 NSString *strFromMsg=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"from"]];
                 NSString *StrMsgg=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"content"]];
                 NSString *PartcipantIDstr=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"participantId"]];
                 NSString *StrOldName=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"oldName"]];
                 uniqueID=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"uniqueid"]];
                 
                 if ([strContentType isEqualToString:@"GroupAdd"])
                 {
                     NSUUID *uuid=[NSUUID UUID];
                     NSString *strMgsId=[NSString stringWithFormat:@"%@",uuid];
                     
                     NSString *strMess=[NSString stringWithFormat:@"added you"];
                     NSDictionary *dictName=[db_class getUserNameForNotification:[userInfo valueForKey:@"createdBy"]];
                     NSString *strNameToShow;
                     
                     if ([[dictName allKeys] containsObject:@"name"])
                     {
                         strNameToShow=[NSString stringWithFormat:@"%@",[dictName valueForKey:@"name"]];
                     }
                     else
                     {
                         strNameToShow=[NSString stringWithFormat:@"+%@",[userInfo valueForKey:@"createdBy"]];
                     }
                     
                     NSString *strContent=[NSString stringWithFormat:@"%@ %@",strNameToShow,strMess];
                     
                     
                     if (![strFrom isEqualToString:strID])
                     {
                         BOOL success=[db_class insertChats:@"" chatRoomId:strGroupId chatRoomType:@"1" sender:@"" lastMessage:strContent lastMessageStatus:@"delivered" lastMessagesTime:strTime lastMessagesType:@"header" unReadCount:@"1" groupName:strGroupName groupImage:strGroupImage sentBy:strFrom isDelete:@"0" isLock:@"0" password:@""];
                         
                         BOOL success1=[db_class insertChatMessages:strMgsId userId:strFrom groupId:strGroupId chatRoomType:@"1" content:strContent contentType:@"header" contentStatus:@"delivered" sender:@"" sentTime:strTime deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                         
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
                     }
                     
                 }
                 else if ([strContentType isEqualToString:@"GroupNameEdit"])
                 {
                     NSUUID *uuid=[NSUUID UUID];
                     NSString *strMgsId=[NSString stringWithFormat:@"%@",uuid];
                     
                     NSDictionary *dictName=[db_class getUserNameForNotification:[userInfo valueForKey:@"from"]];
                     NSString *strNameToShow;
                     if ([strFromMsg isEqualToString:strID])
                     {
                         strNameToShow = @"You";
                     }
                     else{
                         if ([[dictName allKeys] containsObject:@"name"])
                         {
                             strNameToShow=[NSString stringWithFormat:@"%@",[dictName valueForKey:@"name"]];
                         }
                         else
                         {
                             strNameToShow=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"from"]];
                         }
                     }
                     NSString *strContent=[NSString stringWithFormat:@" %@ changed the subject from %@ to %@",strNameToShow,StrOldName,strGroupName];
                     
                     if (![strFrom isEqualToString:strID])
                     {
                         
                         
                         BOOL success2=[db_class updateGroupName:strGroupId groupName:strGroupName];
                         BOOL isAvailinChatsTable=NO;
                         isAvailinChatsTable=[db_class doesChatExist:strGroupId];
                         
                         BOOL success=NO;
                         if (isAvailinChatsTable==NO)
                         {
                             success=[db_class insertChats:@"" chatRoomId:strGroupId chatRoomType:@"1" sender:@"" lastMessage:strContent lastMessageStatus:@"sending" lastMessagesTime:strTime lastMessagesType:@"header" unReadCount:@"1" groupName:strGroupName  groupImage:strGroupImage sentBy:strID isDelete:@"0" isLock:@"0" password:@""];
                             if (success==YES)
                             {
                                 isAvailinChatsTable=YES;
                             }
                         }
                         else
                         {
                             BOOL success1 = [db_class updateLastMessage: strGroupId sender:@"" lastMessage:strContent lastMessageStatus:@"delivered" lastMessageTime:strTime lastMessagesType:@"header" unReadCount:@"1"];
                             NSLog(@"success");
                         }
                         
                         //
                         //
                         BOOL succe=[db_class insertChatMessages:strMgsId userId:strFromMsg groupId:strGroupId chatRoomType:@"1" content:strContent contentType:@"header" contentStatus:@"delivered" sender:@"" sentTime:strTime deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                         
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
                     }
                     
                 }
                 
                 else if ([strContentType isEqualToString:@"GroupImageEdit"])
                 {
                     
                     
                     NSUUID *uuid=[NSUUID UUID];
                     NSString *strMgsId=[NSString stringWithFormat:@"%@",uuid];
                     
                     NSDictionary *dictName=[db_class getUserNameForNotification:[userInfo valueForKey:@"from"]];
                     NSString *strNameToShow;
                     if ([strFromMsg isEqualToString:strID])
                     {
                         strNameToShow = @"You";
                     }
                     else{
                         if ([[dictName allKeys] containsObject:@"name"])
                         {
                             strNameToShow=[NSString stringWithFormat:@"%@",[dictName valueForKey:@"name"]];
                         }
                         else
                         {
                             strNameToShow=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"from"]];
                         }
                     }
                     NSString *strContent=[NSString stringWithFormat:@" %@ changed the group Icon",strNameToShow];
                     
                     
                     
                     
                     
                     if (![strFrom isEqualToString:strID])
                     {
                         
                         BOOL succ = [db_class updateGroupImage:strGroupId  groupImage:strGroupImage];
                         
                         BOOL isAvailinChatsTable=NO;
                         isAvailinChatsTable=[db_class doesChatExist:strGroupId];
                         BOOL success=NO;
                         if (isAvailinChatsTable==NO)
                         {
                             success=[db_class insertChats:@"" chatRoomId:strGroupId chatRoomType:@"1" sender:@"" lastMessage:strContent lastMessageStatus:@"sending" lastMessagesTime:strTime lastMessagesType:@"header" unReadCount:@"1" groupName:strGroupName  groupImage:strGroupImage sentBy:strID isDelete:@"0" isLock:@"0" password:@""];
                             if (success==YES)
                             {
                                 isAvailinChatsTable=YES;
                             }
                         }
                         else
                         {
                             BOOL success1 = [db_class updateLastMessage: strGroupId sender:@"" lastMessage:strContent lastMessageStatus:@"delivered" lastMessageTime:strTime lastMessagesType:@"header" unReadCount:@"1"];
                             NSLog(@"success");
                         }
                         
                         
                         
                         
                         BOOL success2=[db_class insertChatMessages:strMgsId userId:strFromMsg groupId:strGroupId chatRoomType:@"1" content:strContent contentType:@"header" contentStatus:@"delivered" sender:@"" sentTime:strTime deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                         
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
                     }
                     
                 }
                 //
                 else if ([strContentType isEqualToString:@"GroupExit"])
                 {
                     NSUUID *uuid=[NSUUID UUID];
                     NSString *strMgsId=[NSString stringWithFormat:@"%@",uuid];
                     //NSString *strLeftNum=[NSString stringWithFormat:@"%@",strFromMsg];
                     NSString *strMess=[NSString stringWithFormat:@"left from Group"];
                     
                     NSString *strNameToShow;
                     
                     
                     NSDictionary *dictName=[db_class getUserNameForNotification:[userInfo valueForKey:@"from"]];
                     if ([[dictName allKeys] containsObject:@"name"])
                     {
                         strNameToShow=[NSString stringWithFormat:@"%@",[dictName valueForKey:@"name"]];
                     }
                     else
                     {
                         strNameToShow=[NSString stringWithFormat:@"+%@",[userInfo valueForKey:@"from"]];
                     }
                     
                     
                     NSString *strcontent=[NSString stringWithFormat:@"%@ %@",strNameToShow,strMess];
                     
                     
                     if (![strFrom isEqualToString:strID])
                     {
                         
                         
                         
                         BOOL isAvailinChatsTable=NO;
                         isAvailinChatsTable=[db_class doesChatExist:strGroupId];
                         BOOL success=NO;
                         if (isAvailinChatsTable==NO)
                         {
                             success=[db_class insertChats:@"" chatRoomId:strGroupId chatRoomType:@"1" sender:@"" lastMessage:strcontent lastMessageStatus:@"sending" lastMessagesTime:strTime lastMessagesType:@"header" unReadCount:@"1" groupName:strGroupName  groupImage:strGroupImage sentBy:strID isDelete:@"0" isLock:@"0" password:@""];
                             if (success==YES)
                             {
                                 isAvailinChatsTable=YES;
                             }
                         }
                         else
                         {
                             BOOL success1 = [db_class updateLastMessage: strGroupId sender:@"" lastMessage:strcontent lastMessageStatus:@"delivered" lastMessageTime:strTime lastMessagesType:@"header" unReadCount:@"1"];
                             NSLog(@"success");
                         }
                         
                         
                         
                         
                         BOOL success2=[db_class insertChatMessages:strMgsId userId:strFromMsg groupId:strGroupId chatRoomType:@"1" content:strcontent contentType:@"header" contentStatus:@"delivered" sender:@"" sentTime:strTime deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                         
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
                     }
                     
                 }
                 else if ([strContentType isEqualToString:@"PartAdd"])
                 {
                     NSUUID *uuid=[NSUUID UUID];
                     NSString *strMgsId=[NSString stringWithFormat:@"%@",uuid];
                     
                     
                     //                     NSString *strNameToShow1;
                     //                     NSString *strNameToShow2;
                     NSString *strNameToShow;
                     NSString *strContent;
                     NSString *strMess;
                     NSDictionary *dictName1;
                     NSDictionary *dictName2;
                     
                     
                     dictName1=[db_class getUserNameForNotification:[userInfo valueForKey:@"addedBy"]];
                     dictName2=[db_class getUserNameForNotification:[userInfo valueForKey:@"participantId"]];
                     if ([[dictName1 allKeys] containsObject:@"name"])
                     {
                         strNameToShow=[NSString stringWithFormat:@"%@",[dictName1 valueForKey:@"name"]];
                         strMess=[NSString stringWithFormat:@"added %@",[dictName2 valueForKey:@"name"]];
                         if ([strMess isEqualToString:@"added (null)"])
                         {
                             strNameToShow=[NSString stringWithFormat:@"%@",[dictName1 valueForKey:@"name"]];
                             strMess=[NSString stringWithFormat:@"added %@",PartcipantIDstr];
                         }
                         strContent=[NSString stringWithFormat:@"%@ %@",strNameToShow,strMess];
                     }
                     
                     else
                     {
                         strNameToShow=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"addedBy"]];
                         strMess=[NSString stringWithFormat:@"added %@",PartcipantIDstr];
                         strContent=[NSString stringWithFormat:@"%@ %@",strNameToShow,strMess];
                     }
                     
                     
                     if (![strFrom isEqualToString:strID])
                     {
                         BOOL isAvailinChatsTable=NO;
                         isAvailinChatsTable=[db_class doesChatExist:strGroupId];
                         BOOL success=NO;
                         if (isAvailinChatsTable==NO)
                         {
                             success=[db_class insertChats:@"" chatRoomId:strGroupId chatRoomType:@"1" sender:@"" lastMessage:strContent lastMessageStatus:@"sending" lastMessagesTime:strTime lastMessagesType:@"header" unReadCount:@"1" groupName:strGroupName  groupImage:strGroupImage sentBy:strID isDelete:@"0" isLock:@"0" password:@""];
                             if (success==YES)
                             {
                                 isAvailinChatsTable=YES;
                             }
                         }
                         else
                         {
                             BOOL success1 = [db_class updateLastMessage: strGroupId sender:@"" lastMessage:strContent lastMessageStatus:@"delivered" lastMessageTime:strTime lastMessagesType:@"header" unReadCount:@"1"];
                             NSLog(@"success");
                         }
                         BOOL success2=[db_class insertChatMessages:strMgsId userId:strFromMsg groupId:strGroupId chatRoomType:@"1" content:strContent contentType:@"header" contentStatus:@"delivered" sender:@"" sentTime:strTime deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                         
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
                     }
                     
                 }
                 
                 
                 
             }
             [socket emit:@"ack" with:@[@{@"userid":strID,@"uniqueid":uniqueID}]];
             
         }];
        
    }
}


- (void)applicationWillTerminate:(UIApplication *)application
{

    
    if ([strLoginStatus isEqualToString:@"LoggedIn"])
    {
        [socket emit:@"offline" with:@[@{@"id":strID}]];
        NSString *strReceiveMessage=[NSString stringWithFormat:@"%@:receiveMessage",strID];
        [socket off:strReceiveMessage];
        NSString *strACK=[NSString stringWithFormat:@"%@:ack",strID];
        [socket off:strACK];
        [socket off:@"connect"];
        
        
        
    }
    
    
    
    NSLog(@"terminated");
}


- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier
  completionHandler:(void (^)())completionHandler {
    /* Store the completion handler.*/
    [AWSS3TransferUtility interceptApplication:application handleEventsForBackgroundURLSession:identifier completionHandler:completionHandler];
}

#pragma mark - SetUp DB

- (void)setupDb
{
    DatabaseMigrator *migrator = [[DatabaseMigrator alloc] initWithDatabaseFile:[AbstractRepository databaseFilename]];
    [migrator moveDatabaseToUserDirectoryIfNeeded];
}
-(void)CreateTable
{
    DBClass *db_access= [[DBClass alloc]init];
    
    [db_access createUserTable];
    
    [db_access createChatsTable];
    
    [db_access createChatMessagesTable];
    
    [db_access createCallsTable];
    
    [db_access createGroupParticipantsTable];
    
    [db_access createLinkPreviewTable];
    
    
    
}

-(long long)convertDateToMilliseconds:(NSDate *)date
{
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    long long milliseconds=timeInSeconds*1000;
    return milliseconds;
}
-(NSDate *)convertMillisecondsToDate:(long long)milliSeconds
{
    NSDate *dat = [NSDate dateWithTimeIntervalSince1970:(milliSeconds / 1000.0)];
    return dat;
}


-(void)handleAppBadgeCount
{
    int nUnreadCount=[db_class getTotalUnreadCount];
    NSLog(@"%d",nUnreadCount);
    [UIApplication sharedApplication].applicationIconBadgeNumber = nUnreadCount;
}

-(void)sendingMessages
{
    //  [socket emit:@"online" with:@[@{@"id":strID}]];
    
    NSArray *arrSendingMsgs=[db_class getSendingChatMessages];
    //  NSLog(@"%@",arrSendingMsgs);
    
    if (arrSendingMsgs.count!=0)
    {
        for (int i=0; i<arrSendingMsgs.count; i++)
        {
            NSDictionary *dictSending=[arrSendingMsgs objectAtIndex:i];
            NSString *strMsgType=[NSString stringWithFormat:@"%@",[dictSending valueForKey:@"contentType"]];
            NSString *strChatRoomType=[NSString stringWithFormat:@"%@",[dictSending valueForKey:@"chatRoomType"]];
            if ([strMsgType isEqualToString:@"text"])
            {
                NSString *strMsg=[NSString stringWithFormat:@"%@",[dictSending valueForKey:@"content"]];
                NSString *strMsgTo=[NSString stringWithFormat:@"%@",[dictSending valueForKey:@"userId"]];
                NSString *strMsgTime=[NSString stringWithFormat:@"%@",[dictSending valueForKey:@"sentTime"]];
                NSString *strMsgId=[NSString stringWithFormat:@"%@",[dictSending valueForKey:@"id"]];
                NSString *strGroupId=[NSString stringWithFormat:@"%@",[dictSending valueForKey:@"groupId"]];
                
                [socket emit:@"sendMessage" with:@[@{@"from":strID,@"to": strMsgTo,@"content": strMsg,@"contentType":strMsgType,@"messageId": strMsgId,@"time":strMsgTime,@"chatRoomType":strChatRoomType,@"groupId":strGroupId}]];
            }
            else if ([strMsgType isEqualToString:@"image"])
            {
                NSString *strContentStatus=[NSString stringWithFormat:@"%@",[dictSending valueForKey:@"contentStatus"]];
                if ([strContentStatus isEqualToString:@"uploaded"])
                {
                    NSString *strMsg=[NSString stringWithFormat:@"%@",[dictSending valueForKey:@"content"]];
                    NSString *strMsgTo=[NSString stringWithFormat:@"%@",[dictSending valueForKey:@"userId"]];
                    NSString *strMsgTime=[NSString stringWithFormat:@"%@",[dictSending valueForKey:@"sentTime"]];
                    NSString *strMsgId=[NSString stringWithFormat:@"%@",[dictSending valueForKey:@"id"]];
                    NSString *strGroupId=[NSString stringWithFormat:@"%@",[dictSending valueForKey:@"groupId"]];
                    
                    [socket emit:@"sendMessage" with:@[@{@"from":strID,@"to": strMsgTo,@"content": strMsg,@"contentType":strMsgType,@"messageId": strMsgId,@"time":strMsgTime,@"chatRoomType":strChatRoomType,@"groupId":strGroupId}]];
                }
                
            }
            
            
        }
    }
    
}

// Register for VoIP notifications
- (void) voipRegistration {
    dispatch_queue_t mainQueue = dispatch_get_main_queue();
    // Create a push registry object
    PKPushRegistry * voipRegistry = [[PKPushRegistry alloc] initWithQueue: mainQueue];
    // Set the registry's delegate to self
    voipRegistry.delegate = self;
    // Set the push type to VoIP
    voipRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
}

// Handle updated push credentials
- (void)pushRegistry:(PKPushRegistry *)registry didUpdatePushCredentials: (PKPushCredentials *)credentials forType:(NSString *)type {
    // Register VoIP push token (a property of PKPushCredentials) with server
    
    // 087ff1e40f6296485b4ee72eefe99541d5abcbb42e7944bedee638088317121f
    
    NSString * token = [NSString stringWithFormat:@"%@", credentials.token];
    //Format token as you need:
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    NSLog(@"voip token: %@",token);
    strTokenVOIP=token;
}

// Handle incoming pushes
- (void)pushRegistry:(PKPushRegistry *)registry didReceiveIncomingPushWithPayload:(PKPushPayload *)payload forType:(NSString *)type {
    // Process the received push
    
    CTCallCenter *ctCallCenter = [[CTCallCenter alloc] init];
    if (ctCallCenter.currentCalls == nil)
    {
        NSLog(@"Incoming call endCall %@",payload.dictionaryPayload);
        
        NSMutableDictionary *DictUserDetails=[db_class getUserInfo:[payload.dictionaryPayload valueForKey:@"fromId"]];
        [DictUserDetails setObject:[payload.dictionaryPayload valueForKey:@"channel_id"] forKey:@"channelId"];
        [DictUserDetails setObject:[payload.dictionaryPayload valueForKey:@"fromId"] forKey:@"fromId"];
        //  [DictUserDetails setObject:[payload.dictionaryPayload valueForKey:@"gcm.message_id"] forKey:@"messageId"];
        [DictUserDetails setObject:[payload.dictionaryPayload valueForKey:@"pushType"] forKey:@"callType"];
        [DictUserDetails setObject:@"yes" forKey:@"inComing"];
        NSLog(@"%@",DictUserDetails);
        
        long long milliSeconds=[self convertDateToMilliseconds:[NSDate date]];
        strCallTime=[NSString stringWithFormat:@"%lld",milliSeconds];
        
        NSString *strCallerName=[DictUserDetails valueForKey:@"name"];
        
        if (strCallerName.length==0)
        {
            strCallerName=[NSString stringWithFormat:@"+%@",[DictUserDetails valueForKey:@"fromId"]];
            [DictUserDetails setObject:strCallerName forKey:@"name"];
        }
        NSString *strCallType=[NSString stringWithFormat:@"%@",[DictUserDetails valueForKey:@"callType"]];
        
        dictInComomgCall=DictUserDetails;
        CXCallUpdate *update = [[CXCallUpdate alloc] init];
        
        update.localizedCallerName = strCallerName;
        [update setRemoteHandle:[[CXHandle alloc] initWithType:CXHandleTypeGeneric value:[DictUserDetails valueForKey:@"fromId"]]];
        [update setLocalizedCallerName:[DictUserDetails valueForKey:@"name"]];
        if ([strCallType isEqualToString:@"voicecall"])
        {
            update.hasVideo=NO;
        }
        else
        {
            update.hasVideo=YES;
        }
        // strUUID=nil;
        NSUUID *uuid= [[NSUUID alloc] initWithUUIDString:@"A5D59C2F-FE68-4BE7-B318-95029619C759"];//[NSUUID UUID];
        strUUID=uuid;
        NSLog(@"endCall initiating uuid%@",strUUID);
        
        //        [callkitProvider reportCallWithUUID:strUUID updated:update];
        [callkitProvider reportNewIncomingCallWithUUID:strUUID  update:update completion:^(NSError * _Nullable error) {
            
            if (!error)
            {
                //                update set
                //                [callkitProvider reportCallWithUUID:strUUID updated:update];
                
                [CallTimer invalidate];
                CallTimer=nil;
                CallTimer=[NSTimer scheduledTimerWithTimeInterval:45 target:self selector:@selector(EndCall) userInfo:nil repeats:NO];
            }
            else
            {
                NSLog(@"Error: %@", error);
            }
        }];
        
        
    }
    
    
    
}



- (void) provider: (CXProvider *)provider performAnswerCallAction: (CXAnswerCallAction *)action
{
    [CallTimer invalidate];
    CallTimer=nil;
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:[dictInComomgCall valueForKey:@"fromId"] forKey:@"to"];
    [dictParam setValue:strID forKey:@"from"];
    [dictParam setValue:[dictInComomgCall valueForKey:@"channelId"] forKey:@"channelId"];
    strUUID = action.callUUID;
    NSLog(@"Answer UUID %@",action.callUUID);
    NSLog(@"Answer endCall %@ ",dictParam);
    //    NSMutableDictionary *dictParam1=[[NSMutableDictionary alloc]init];
    //    dictParam1 = [dictInComomgCall valueForKey:@"alert"];
    //    [dictParam setValue:[dictParam1 valueForKey:@"body"] forKey:@"body"];
    //    [dictParam setValue:[dictParam1 valueForKey:@"title"] forKey:@"title"];
    
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_ACCEPT_CALL withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         
         if (response)
         {
             //  NSLog(@"Response: %@",response);
             if ([[response valueForKey:@"error"] boolValue]==false)
             {
                 
             }
             else
             {
                 
                 
             }
         }
         
     }];
    
    CXCallUpdate *callUpdate = [[CXCallUpdate alloc] init];
    [callUpdate setRemoteHandle:[[CXHandle alloc] initWithType:CXHandleTypePhoneNumber value:[dictInComomgCall valueForKey:@"fromId"]]];
    callUpdate.localizedCallerName = @"NAME";
    [provider reportCallWithUUID:action.callUUID updated:callUpdate];
    CXHandle *handle = [[CXHandle alloc]initWithType:CXHandleTypePhoneNumber value:[dictInComomgCall valueForKey:@"fromId"]];
    CXStartCallAction *startAction = [[CXStartCallAction alloc] initWithCallUUID:strUUID handle:handle];
    
    //    CXEndCallAction *endCallAction = [[CXEndCallAction alloc] initWithCallUUID:strUUID];
    //
    CXTransaction *transaction = [[CXTransaction alloc] initWithAction:startAction];
    CXCallController *controller = [[CXCallController alloc] init];
    //    NSLog(@"%@ endCallRequest uuid %@",NSStringFromClass([self class]),strUUID.UUIDString);
    //    NSLog(@"%@",strUUID);
    [controller requestTransaction:transaction completion:^(NSError * _Nullable error) {
        if (error) {
            NSLog(@"%@",error);
            NSLog(@"%@",error.localizedDescription);
            NSLog(@"%@ endCallRequest error %@",NSStringFromClass([self class]),[error description]);
            NSLog(@"%@",strUUID);
        }
        else{
            NSLog(@"%@ endCallRequest successful ",NSStringFromClass([self class]));
        }
    }];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Incoming_Call" object:self];
    NSDate *now = [[NSDate alloc] init];
    [action fulfillWithDateConnected: now];
}
-(void)provider:(CXProvider *)provider performStartCallAction:(CXStartCallAction *)action{
    NSLog(@"endCall StartCallAction Called");
    [action fulfill];
}
- (void) provider: (CXProvider *)provider performEndCallAction: (CXEndCallAction *)action {
    //    NSDate *now = [[NSDate alloc] init];
    //    [action fulfillWithDateEnded: now];
    NSLog(@"End UUID %@",action.UUID);
    NSString *strZoeChatID=[dictInComomgCall valueForKey:@"fromId"];
    NSString *callType;
    NSString *strCallType=[NSString stringWithFormat:@"%@",[dictInComomgCall valueForKey:@"callType"]];
    
    if ([strCallType isEqualToString:@"voicecall"])
    {
        callType=@"voice";
    }
    else
    {
        callType=@"video";
    }
    
    BOOL success=[db_class insertCalls:@"" zoeChatId:strZoeChatID callTime:strCallTime callType:callType callLog:@"0" callDuration:@"00:00"];
    dictInComomgCall=[[NSMutableDictionary alloc]init];
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:strZoeChatID forKey:@"to"];
    [dictParam setValue:strID forKey:@"from"];
    [dictParam setValue:@"inc" forKey:@"pushType"];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_END_CALL withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         
         if (response)
         {
             //  NSLog(@"Response: %@",response);
             if ([[response valueForKey:@"error"] boolValue]==false)
             {
                 
             }
             else
             {
                 
                 
             }
         }
         
     }];
    [action fulfill];
    
}
-(void)EndCall
{
    if (strUUID)
    {
        CXEndCallAction *endCallAction = [[CXEndCallAction alloc] initWithCallUUID:strUUID];
        
        CXTransaction *transaction = [[CXTransaction alloc] initWithAction:endCallAction];
        CXCallController *controller = [[CXCallController alloc] init];
        NSLog(@"%@ endCallRequest uuid %@",NSStringFromClass([self class]),strUUID.UUIDString);
        
        NSLog(@"%@",strUUID);
        [controller requestTransaction:transaction completion:^(NSError * _Nullable error) {
            if (error) {
                NSLog(@"%@",error);
                NSLog(@"%@",error.localizedDescription);
                NSLog(@"%@ endCallRequest error %@",NSStringFromClass([self class]),[error description]);
                NSLog(@"%@",strUUID);
            }
            else{
                NSLog(@"%@ endCallRequest successful ",NSStringFromClass([self class]));
            }
        }];
        
    }
    
    
    
}


- (UIImage *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth) {
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
}

-(void)handleCall
{
    self.callCenter.callEventHandler = ^(CTCall *call){
        
        if ([call.callState isEqualToString: CTCallStateConnected])
        {
            NSLog(@"endcall CTCallStateConnected");
        }
        else if ([call.callState isEqualToString: CTCallStateDialing])
        {
            NSLog(@"endcall CTCallStateDialing");
        }
        else if ([call.callState isEqualToString: CTCallStateDisconnected])
        {
            //resume back your app background method
            NSLog(@"endcall CTCallStateDisconnected");
        }
        else if ([call.callState isEqualToString: CTCallStateIncoming])
        {
            NSLog(@"endcall CTCallStateIncoming");
        }
        else  {
            NSLog(@"endcall NO");
        }
    };
}

@end

