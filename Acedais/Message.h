//
//  Message.h
//  Whatsapp
//
//  Created by Rafael Castro on 6/16/15.
//  Copyright (c) 2015 HummingBird. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


typedef NS_ENUM(NSInteger, MessageStatus)
{
    MessageStatusSending,
    MessageStatusSent,
    MessageStatusReceived,
    MessageStatusRead,
    MessageStatusFailed,
    MessageStatusCancelled,
    MessageStatusUploaded,
    MessageStatusDownloaded,
    MessageStatusDownloading
};

typedef NS_ENUM(NSInteger, MessageSender)
{
    MessageSenderMyself,
    MessageSenderSomeone
};

//
// This class is the message object itself
//
@interface Message : NSObject

@property (assign, nonatomic) MessageSender sender;
@property (assign, nonatomic) MessageStatus status;
@property (strong, nonatomic) NSString *identifier;
@property (strong, nonatomic) NSString *chat_id;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *Latitude;
@property (strong, nonatomic) NSString *Longitude;
@property (strong, nonatomic) NSString *contactName;
@property (strong, nonatomic) NSString *contactNo;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *caption;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) NSDate *date;
@property (assign, nonatomic) CGFloat heigh;
@property (strong, nonatomic) NSString *checkStar;
@property (strong, nonatomic) NSString *contentType;
@property (strong, nonatomic) NSString *HeaderText;
@property (strong, nonatomic) NSString *isDownloaded;
@property (strong, nonatomic) NSString *seenTime;
@property (strong, nonatomic) NSString *showPreview;

@property (strong, nonatomic) NSString *linkTitle;
@property (strong, nonatomic) NSString *linkLogo;
@property (strong, nonatomic) NSString *linkDescription;


@property (strong, nonatomic) NSString *deliveredTime;




//@property (assign, nonatomic)AppDelegate *app;

//
@property (strong, nonatomic) NSString *SeenT;


+(Message *)messageFromDictionary:(NSDictionary *)dictionary;

@end


