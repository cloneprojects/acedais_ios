//
//  OtherContactsCell.h
//  ZoeChat
//
//  Created by macmini on 09/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtherContactsCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *contactNameLbl;
@property (strong, nonatomic) IBOutlet UIImageView *ContactImgView;
@property (strong, nonatomic) IBOutlet UILabel *otherInitialLbl;

@end
