//
//  ContactCell.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 05/01/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ContactImage;
@property (weak, nonatomic) IBOutlet UILabel *ContactName;
@property (weak, nonatomic) IBOutlet UILabel *ContactStatus;
@property (strong, nonatomic) IBOutlet UIImageView *contactlockImg;
@property (strong, nonatomic) IBOutlet UILabel *lblImg;
@end
