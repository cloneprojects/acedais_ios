//
//  SettingVC.m
//  ZoeChat
//
//  Created by macmini on 11/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "SettingVC.h"
#import "AppDelegate.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <QuartzCore/QuartzCore.h>
#import "SingleChatVC.h"


@interface SettingVC ()
{
    AppDelegate *appDelegate;
    BOOL clickTheme;
    NSMutableDictionary *supportDict;
}

@end

@implementation SettingVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    clickTheme=YES;
    supportDict=[[NSMutableDictionary alloc]init];
    appDelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.profileName.text=[NSString stringWithFormat:@"%@",appDelegate.strName];
    self.profileStatus.text=[NSString stringWithFormat:@"%@",appDelegate.strStatus];
    NSString *imgURL=[NSString stringWithFormat:@"%@",appDelegate.strProfilePic];
    //Lbl Initial
    NSString *InitialStr;
    UIColor *bgColor;
    [self.btnTheme setHidden:YES];
    [_InitialLbl setHidden:YES];
    if (![appDelegate.strName isEqualToString:@"(null)"])
    {
        NSString *capitalizedString = [appDelegate.strName capitalizedString];
    InitialStr=[capitalizedString substringToIndex:1];
    if ([InitialStr isEqualToString:@"A"])
    {
        bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"B"])
    {
        bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"C"])
    {
        bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"D"])
    {
        bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"E"])
    {
        bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"F"])
    {
        bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"G"])
    {
        bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"H"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"I"])
    {
        bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"J"])
    {
        bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"K"])
    {
        bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"L"])
    {
        bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"M"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"N"])
    {
        bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"O"])
    {
        bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"P"])
    {
        bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"Q"])
    {
        bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"R"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"S"])
    {
        bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"T"])
    {
        bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"U"])
    {
        bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"V"])
    {
        bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"W"])
    {
        bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"X"])
    {
        bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"Y"])
    {
        bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"Z"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
    }
    
}
else
{
    InitialStr=[appDelegate.strID substringToIndex:1];
    if ([InitialStr isEqualToString:@"0"])
    {
        bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"1"])
    {
        bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"2"])
    {
        bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"3"])
    {
        bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"4"])
    {
        bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"5"])
    {
        bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"6"])
    {
        bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"7"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"8"])
    {
        bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"9"])
    {
        bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
    }
    
}
    

//    if (![imgURL isEqual:[NSNull null]])
//    {
//        if (![imgURL isEqualToString:@""])
//        {
//            [self.profileImgView sd_setImageWithURL:[NSURL URLWithString:imgURL] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
//        }
//    }
    
    if (![imgURL isEqualToString:@"(null)"])
    {
        if (![imgURL isEqualToString:@""])
        {
            [self.profileImgView sd_setImageWithURL:[NSURL URLWithString:imgURL] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
        }
    }

    else
    {
        [_InitialLbl setHidden:NO];
        [self.profileImgView sd_setImageWithURL:[NSURL URLWithString:imgURL] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
        _InitialLbl.text=InitialStr;
        _InitialLbl.backgroundColor=bgColor;
    }
    self.profileImgView.layer.cornerRadius=self.profileImgView.frame.size.width/2;
    self.profileImgView.clipsToBounds=YES;
    self.profileImgView.layer.borderWidth = 0;
    self.profileImgView.layer.masksToBounds=YES;
    self.profileBtn.layer.cornerRadius=self.profileBtn.frame.size.width/2;
    self.profileBtn.clipsToBounds=YES;
    self.profileBtn.layer.borderWidth = 0;
    self.profileBtn.layer.masksToBounds=YES;
    self.InitialLbl.layer.cornerRadius=self.InitialLbl.frame.size.width/2;
    self.InitialLbl.clipsToBounds=YES;
    self.InitialLbl.layer.borderWidth = 0;
    //Theme
    [self.themeView setHidden:YES];
    self.b1.layer.cornerRadius=self.b1.frame.size.width/2;
    self.b1.clipsToBounds=YES;
    self.b2.layer.cornerRadius=self.b2.frame.size.width/2;
    self.b2.clipsToBounds=YES;
    self.b3.layer.cornerRadius=self.b3.frame.size.width/2;
    self.b3.clipsToBounds=YES;
    self.b4.layer.cornerRadius=self.b4.frame.size.width/2;
    self.b4.clipsToBounds=YES;
    self.b5.layer.cornerRadius=self.b5.frame.size.width/2;
    self.b5.clipsToBounds=YES;
    self.b6.layer.cornerRadius=self.b6.frame.size.width/2;
    self.b6.clipsToBounds=YES;
    self.b7.layer.cornerRadius=self.b7.frame.size.width/2;
    self.b7.clipsToBounds=YES;
    self.b8.layer.cornerRadius=self.b8.frame.size.width/2;
    self.b8.clipsToBounds=YES;
    self.b9.layer.cornerRadius=self.b9.frame.size.width/2;
    self.b9.clipsToBounds=YES;
    self.b10.layer.cornerRadius=self.b10.frame.size.width/2;
    self.b10.clipsToBounds=YES;
    self.b11.layer.cornerRadius=self.b11.frame.size.width/2;
    self.b11.clipsToBounds=YES;
    self.b12.layer.cornerRadius=self.b12.frame.size.width/2;
    self.b12.clipsToBounds=YES;
    
    
    
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        

        [self.fbBtn setImage:[[UIImage imageNamed:@"fb.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        self.fbBtn.tintColor=color;
        [self.instaBtn setImage:[[UIImage imageNamed:@"intsa.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        self.instaBtn.tintColor=color;
        [self.twitterBtn setImage:[[UIImage imageNamed:@"twitter.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        self.twitterBtn.tintColor=color;
        [self.supportBtn setImage:[[UIImage imageNamed:@"support.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        self.supportBtn.tintColor=color;
        
        
        
        
        
    }
    else
    {
        [self.fbBtn setImage:[[UIImage imageNamed:@"fb.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        self.fbBtn.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
         [self.instaBtn setImage:[[UIImage imageNamed:@"intsa.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        self.instaBtn.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
       [self.twitterBtn setImage:[[UIImage imageNamed:@"twitter.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        self.twitterBtn.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        [self.supportBtn setImage:[[UIImage imageNamed:@"support.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        self.supportBtn.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        
        
        
        
    }
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    [UINavigationBar appearance].tintColor = [UIColor colorWithRed:(247.0f/255.0f) green:(247.0f/255.0f) blue:(247.0f/255.0f) alpha:1];
    
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)clickOnBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickOnImgBtn:(id)sender {
    [self performSegueWithIdentifier:@"showProfile" sender:self];
}
- (IBAction)dataUsage:(id)sender {
    [self performSegueWithIdentifier:@"ToDataVC" sender:self];
}
- (IBAction)clickOnTheme:(id)sender
{
    if (clickTheme)
    {
        [self.themeView setHidden:NO];
        clickTheme=NO;

    }
    else if(clickTheme==NO)
    {
        [self.themeView setHidden:YES];
        clickTheme=YES;

    }
    
}

- (IBAction)Privacy:(id)sender {
    [self performSegueWithIdentifier:@"ToPrivacyVC" sender:self];

}

- (IBAction)notification:(id)sender {
    [self performSegueWithIdentifier:@"ToNotificationVC" sender:self];

}
- (IBAction)ClickOnB1:(id)sender {
    [[UIApplication sharedApplication] keyWindow].tintColor = [UIColor colorWithRed:26/255.0 green:186/255.0 blue:156/255.0 alpha:1.0];
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:[UIColor colorWithRed:26/255.0 green:186/255.0 blue:156/255.0 alpha:1.0]];
    [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:@"myColor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:26/255.0 green:186/255.0 blue:156/255.0 alpha:1.0];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:26/255.0 green:186/255.0 blue:156/255.0 alpha:1.0]}
                                             forState:UIControlStateSelected];
    
    
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:26/255.0 green:186/255.0 blue:156/255.0 alpha:1.0]];
[self.fbBtn setImage:[[UIImage imageNamed:@"fb.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.fbBtn.tintColor=[UIColor colorWithRed:26/255.0 green:186/255.0 blue:156/255.0 alpha:1.0];
[self.instaBtn setImage:[[UIImage imageNamed:@"intsa.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.instaBtn.tintColor=[UIColor colorWithRed:26/255.0 green:186/255.0 blue:156/255.0 alpha:1.0];
    [self.twitterBtn setImage:[[UIImage imageNamed:@"twitter.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.supportBtn setImage:[[UIImage imageNamed:@"support.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.twitterBtn.tintColor=[UIColor colorWithRed:26/255.0 green:186/255.0 blue:156/255.0 alpha:1.0];
    self.supportBtn.tintColor=[UIColor colorWithRed:26/255.0 green:186/255.0 blue:156/255.0 alpha:1.0];
    [self.themeView setHidden:YES];
    clickTheme=YES;

    

}

- (IBAction)ClickOnB2:(id)sender {
    [[UIApplication sharedApplication] keyWindow].tintColor = [UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0]];
    [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:@"myColor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0] }
                                             forState:UIControlStateSelected];
    
    
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0]];
[self.fbBtn setImage:[[UIImage imageNamed:@"fb.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.fbBtn.tintColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
[self.instaBtn setImage:[[UIImage imageNamed:@"intsa.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.instaBtn.tintColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
    [self.twitterBtn setImage:[[UIImage imageNamed:@"twitter.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.supportBtn setImage:[[UIImage imageNamed:@"support.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.twitterBtn.tintColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
    self.supportBtn.tintColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
    [self.themeView setHidden:YES];
    clickTheme=YES;

}

- (IBAction)ClickOnB3:(id)sender {
    [[UIApplication sharedApplication] keyWindow].tintColor = [UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0]];
    [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:@"myColor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0] }
                                             forState:UIControlStateSelected];
    
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0]];
[self.fbBtn setImage:[[UIImage imageNamed:@"fb.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.fbBtn.tintColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
[self.instaBtn setImage:[[UIImage imageNamed:@"intsa.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.instaBtn.tintColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
    [self.twitterBtn setImage:[[UIImage imageNamed:@"twitter.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.supportBtn setImage:[[UIImage imageNamed:@"support.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.twitterBtn.tintColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
    self.supportBtn.tintColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
    [self.themeView setHidden:YES];
    clickTheme=YES;

}

- (IBAction)ClickOnB4:(id)sender {
    [[UIApplication sharedApplication] keyWindow].tintColor = [UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0]];
    [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:@"myColor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0] }
                                             forState:UIControlStateSelected];
    
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0]];
[self.fbBtn setImage:[[UIImage imageNamed:@"fb.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.fbBtn.tintColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
[self.instaBtn setImage:[[UIImage imageNamed:@"intsa.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.instaBtn.tintColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
    [self.twitterBtn setImage:[[UIImage imageNamed:@"twitter.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.supportBtn setImage:[[UIImage imageNamed:@"support.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.twitterBtn.tintColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
    self.supportBtn.tintColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
    [self.themeView setHidden:YES];
    clickTheme=YES;

}

- (IBAction)ClickOnB5:(id)sender {
    [[UIApplication sharedApplication] keyWindow].tintColor = [UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0]];
    [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:@"myColor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0] }
                                             forState:UIControlStateSelected];
    
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0]];
[self.fbBtn setImage:[[UIImage imageNamed:@"fb.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.fbBtn.tintColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
[self.instaBtn setImage:[[UIImage imageNamed:@"intsa.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.instaBtn.tintColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
    [self.twitterBtn setImage:[[UIImage imageNamed:@"twitter.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.supportBtn setImage:[[UIImage imageNamed:@"support.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.twitterBtn.tintColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
    self.supportBtn.tintColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];

    [self.themeView setHidden:YES];
    clickTheme=YES;

}

- (IBAction)ClickOnB6:(id)sender {
    [[UIApplication sharedApplication] keyWindow].tintColor = [UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0]];
    [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:@"myColor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0]}
                                             forState:UIControlStateSelected];
    
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0]];
[self.fbBtn setImage:[[UIImage imageNamed:@"fb.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.fbBtn.tintColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
[self.instaBtn setImage:[[UIImage imageNamed:@"intsa.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.instaBtn.tintColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
    [self.twitterBtn setImage:[[UIImage imageNamed:@"twitter.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.supportBtn setImage:[[UIImage imageNamed:@"support.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.twitterBtn.tintColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
    self.supportBtn.tintColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
    [self.themeView setHidden:YES];
    clickTheme=YES;

}

- (IBAction)ClickOnB7:(id)sender {
    [[UIApplication sharedApplication] keyWindow].tintColor = [UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0]];
    [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:@"myColor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0] }
                                             forState:UIControlStateSelected];
    
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0]];
[self.fbBtn setImage:[[UIImage imageNamed:@"fb.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.fbBtn.tintColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
[self.instaBtn setImage:[[UIImage imageNamed:@"intsa.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.instaBtn.tintColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
    [self.twitterBtn setImage:[[UIImage imageNamed:@"twitter.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.supportBtn setImage:[[UIImage imageNamed:@"support.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.twitterBtn.tintColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
    self.supportBtn.tintColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
    [self.themeView setHidden:YES];
    clickTheme=YES;

}

- (IBAction)ClickOnB8:(id)sender {
    [[UIApplication sharedApplication] keyWindow].tintColor = [UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0]];
    [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:@"myColor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0] }
                                             forState:UIControlStateSelected];
    
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0]];
[self.fbBtn setImage:[[UIImage imageNamed:@"fb.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.fbBtn.tintColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
[self.instaBtn setImage:[[UIImage imageNamed:@"intsa.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.instaBtn.tintColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
    [self.twitterBtn setImage:[[UIImage imageNamed:@"twitter.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.supportBtn setImage:[[UIImage imageNamed:@"support.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.twitterBtn.tintColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
    self.supportBtn.tintColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
    [self.themeView setHidden:YES];
    
    clickTheme=YES;

}

- (IBAction)ClickOnB9:(id)sender {
    [[UIApplication sharedApplication] keyWindow].tintColor = [UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0]];
    [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:@"myColor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0] }
                                             forState:UIControlStateSelected];

[[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0]];
[self.fbBtn setImage:[[UIImage imageNamed:@"fb.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.fbBtn.tintColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
[self.instaBtn setImage:[[UIImage imageNamed:@"intsa.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.instaBtn.tintColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
    [self.twitterBtn setImage:[[UIImage imageNamed:@"twitter.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.supportBtn setImage:[[UIImage imageNamed:@"support.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.twitterBtn.tintColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
    self.supportBtn.tintColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
    [self.themeView setHidden:YES];

}

- (IBAction)ClickOnB10:(id)sender {
    [[UIApplication sharedApplication] keyWindow].tintColor = [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0]];
    [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:@"myColor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];

    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0] }
                                          forState:UIControlStateSelected];
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0]];
[self.fbBtn setImage:[[UIImage imageNamed:@"fb.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.fbBtn.tintColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
[self.instaBtn setImage:[[UIImage imageNamed:@"intsa.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.instaBtn.tintColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
    [self.twitterBtn setImage:[[UIImage imageNamed:@"twitter.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.supportBtn setImage:[[UIImage imageNamed:@"support.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.twitterBtn.tintColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
    self.supportBtn.tintColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
    [self.themeView setHidden:YES];

}

- (IBAction)ClickOnB11:(id)sender {
    [[UIApplication sharedApplication] keyWindow].tintColor = [UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0]];
    [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:@"myColor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0]];

    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0]}
                                             forState:UIControlStateSelected];
[self.fbBtn setImage:[[UIImage imageNamed:@"fb.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.fbBtn.tintColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
[self.instaBtn setImage:[[UIImage imageNamed:@"intsa.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.instaBtn.tintColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
    [self.twitterBtn setImage:[[UIImage imageNamed:@"twitter.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.supportBtn setImage:[[UIImage imageNamed:@"support.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.twitterBtn.tintColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
    self.supportBtn.tintColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];

    [self.themeView setHidden:YES];

}

- (IBAction)ClickOnB12:(id)sender {
    [[UIApplication sharedApplication] keyWindow].tintColor = [UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0]];
    [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:@"myColor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0]];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0] }
                                             forState:UIControlStateSelected];

[self.fbBtn setImage:[[UIImage imageNamed:@"fb.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.fbBtn.tintColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
[self.instaBtn setImage:[[UIImage imageNamed:@"intsa.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];    self.instaBtn.tintColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
    [self.twitterBtn setImage:[[UIImage imageNamed:@"twitter.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.supportBtn setImage:[[UIImage imageNamed:@"support.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.twitterBtn.tintColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
    self.supportBtn.tintColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];

    [self.themeView setHidden:YES];

}
- (IBAction)clickOnFB:(id)sender {
    NSURL *url = [NSURL URLWithString:@"https://www.facebook.com/whatsappcloneapp"];
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:NULL];
    }else{
        // Fallback on earlier versions
        [[UIApplication sharedApplication] openURL:url];
    }

}

- (IBAction)clickOnInstagram:(id)sender {
    NSURL *url = [NSURL URLWithString:@"https://www.instagram.com/whatsappclone//"];
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:NULL];
    }else{
        // Fallback on earlier versions
        [[UIApplication sharedApplication] openURL:url];
    }

}

- (IBAction)clickOnTwitter:(id)sender {
    NSURL *url = [NSURL URLWithString:@"https://twitter.com/whatsappclonez"];
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:NULL];
    }else{
        // Fallback on earlier versions
        [[UIApplication sharedApplication] openURL:url];
    }

}

- (IBAction)clickOnSupport:(id)sender {
    
    
    
    long long milliSeconds=[self convertDateToMilliseconds:[NSDate date]];
    NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
    NSLog(@"%@",strMilliSeconds);
    long long milli=[strMilliSeconds longLongValue];
    NSDate *dateTime=[self convertMillisecondsToDate:milli];
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyyMMddHHmmssSSS"];
    NSString *DateString = [dateFormatter1 stringFromDate:dateTime];
    
    [supportDict setValue:@"919566025629" forKey:@"chatRoomId"];
    [supportDict setValue:@"0" forKey:@"chatRoomType"];
    [supportDict setValue:DateString forKey:@"dateTime"];
    [supportDict setValue:@"" forKey:@"groupImage"];
    [supportDict setValue:@"" forKey:@"groupName"];
    [supportDict setValue:@"" forKey:@"image"];
    [supportDict setValue:@"0" forKey:@"isDelete"];
    [supportDict setValue:@"0" forKey:@"isLock"];
    [supportDict setValue:@"" forKey:@"lastMessage"];
    [supportDict setValue:@"sent" forKey:@"lastMessageStatus"];
    [supportDict setValue:strMilliSeconds forKey:@"lastMessageTime"];
    [supportDict setValue:@"text" forKey:@"lastMessageType"];
    [supportDict setValue:@"Support ZoeChat" forKey:@"name"];
    [supportDict setValue:@"" forKey:@"password"];
    [supportDict setValue:@"0" forKey:@"sender"];
    [supportDict setValue:appDelegate.strID forKey:@"sentBy"];
    [supportDict setValue:@"0" forKey:@"unreadCount"];
    NSLog(@"%@",supportDict);
    [self performSegueWithIdentifier:@"Support_SingleChat" sender:self];

    
    

}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"Support_SingleChat"])
    {
        SingleChatVC *Single=[segue destinationViewController];
        Single.dictDetails=supportDict;
//        appdel.ZOEID = [dictToSendSingleChat valueForKey:@"chatRoomId"];
//        appdel.RoomTYPE = [dictToSendSingleChat valueForKey:@"chatRoomType"];
        NSLog(@"%@",Single.dictDetails);
    }
}
-(long long)convertDateToMilliseconds:(NSDate *)date
{
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    long long milliseconds=timeInSeconds*1000;
    return milliseconds;
}
-(NSDate *)convertMillisecondsToDate:(long long)milliSeconds
{
    NSDate *dat = [NSDate dateWithTimeIntervalSince1970:(milliSeconds / 1000.0)];
    return dat;
}
@end
