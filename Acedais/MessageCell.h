//
//  MessageCell.h
//  Whatsapp
//
//  Created by Rafael Castro on 7/23/15.
//  Copyright (c) 2015 HummingBird. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Message.h"

//
// This class build bubble message cells
// for Income or Outgoing messages
//
@interface MessageCell : UITableViewCell

@property (strong, nonatomic) Message *message;
@property (strong, nonatomic) UIButton *resendButton;

@property (strong, nonatomic) UILabel *timeLabel;
@property (strong, nonatomic) UILabel *nameLabel;
@property (strong, nonatomic) UITextView *textView;
@property (strong, nonatomic) UIImageView *imgView;
@property (strong, nonatomic) UIImageView *bubbleImage;
@property (strong, nonatomic) UIImageView *statusIcon;
@property (strong, nonatomic) UIImageView *ImageBG;

@property(strong, nonatomic)UIImageView *imgContact;
@property(strong, nonatomic)UILabel *lblContName;
@property(strong, nonatomic)UILabel *lblContNumber;

@property(strong, nonatomic)UIButton *btnInviteOrMsg;
@property(strong, nonatomic)UIButton *btnAddContact;
@property(strong, nonatomic)UIButton *btnAudioAction;
@property(strong, nonatomic)UIView *viewContact;
@property(strong,nonatomic)UISlider *slider;

@property (strong, nonatomic) UIImageView *StarimgView;
//Document

@property(strong, nonatomic)UIImageView *documentImg;
@property(strong, nonatomic)UILabel *documentName;
@property(strong, nonatomic)UIButton *docBtn;


//HeaderView
@property (strong, nonatomic) UITextView *HeadertextView;

//LinkPreview

@property(strong, nonatomic)UIImageView *LogoImg;
@property(strong, nonatomic)UILabel *TitleLbl;
@property(strong, nonatomic)UILabel *DescriptionLbl;
@property(strong, nonatomic)UILabel *LinkView;
@property(strong, nonatomic)UIButton *LinkBtn;

-(void)updateMessageStatus;

//Estimate BubbleCell Height
-(CGFloat)height;
//sticker
@property (strong, nonatomic) UIImageView *StickerimgView;

- (void)fillWithStickerMessage: (NSString*)message downloaded: (BOOL)downloaded;
@end
