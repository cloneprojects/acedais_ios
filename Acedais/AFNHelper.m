//
//  AFNHelper.m
//  gHealth Doctor
//
//  Created by Ramesh P on 16/11/16.
//  Copyright © 2016 provenlogic. All rights reserved.
//

#import "AFNHelper.h"
#import "AFNetworking.h"
#import "Constants.h"

@implementation AFNHelper

@synthesize strReqMethod;

#pragma mark -
#pragma mark - Init

-(id)initWithRequestMethod:(NSString *)method
{
    if ((self = [super init])) {
        self.strReqMethod=method;
    }
    return self;
}

#pragma mark -
#pragma mark - Methods
-(void)getDataFromPath:(NSString *)path withParamData:(NSMutableDictionary *)dictParam withBlock:(RequestCompletionBlock)block
{
    if (block) {
        dataBlock=[block copy];
    }
    //[raw urlEncodeUsingEncoding:NSUTF8Encoding]
    
    NSRange match;
    match = [path rangeOfString: @"application"];
    NSRange match1;
    match1 = [path rangeOfString: @"request/path"];
    
    AFHTTPSessionManager *manager=[AFHTTPSessionManager manager];
    
  //  manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@[@"text/html",@"application/json"]];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.requestSerializer.timeoutInterval=600;
    
    NSString *strURL=[NSString stringWithFormat:@"%@%@",SERVICE_URL,path];
    
    if ([self.strReqMethod isEqualToString:POST_METHOD])
    {
        [manager POST:strURL parameters:dictParam progress:^(NSProgress * _Nonnull uploadProgress) {
            
        }
              success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
             if(dataBlock){
                 
                 if(responseObject==nil)
                     dataBlock(task.response,nil);
                 else
                     dataBlock(responseObject,nil);
             }
         }
              failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                  NSLog(@"Error %@",error);
                   dataBlock(nil,error);
              }];
        
    }
    else
    {
        [manager GET:strURL parameters:dictParam progress:^(NSProgress * _Nonnull uploadProgress) {
            
        }
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
             if(dataBlock){
                 if(responseObject==nil)
                     dataBlock(task.response,nil);
                 else
                     dataBlock(responseObject,nil);
             }
             
         }
             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 NSLog(@"Error %@",error);
                  dataBlock(nil,error);
             }];
        
        
    }
    
    
}
-(void)getDataFromPath:(NSString *)path withParamDataImage:(NSMutableDictionary *)dictParam andImage:(UIImage *)image withBlock:(RequestCompletionBlock)block{
    
    if (block) {
        dataBlock=[block copy];
    }
    NSData *imageToUpload = UIImageJPEGRepresentation(image, 1.0);//(uploadedImgView.image);
    if (imageToUpload)
    {
        NSString *url=[NSString stringWithFormat:@"%@%@",SERVICE_URL,path] ;//stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
        AFHTTPSessionManager *manager=[AFHTTPSessionManager manager];
        
      //  manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@[@"text/html",@"application/json"]];
        
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.requestSerializer.timeoutInterval=600;
        
        [manager POST:url parameters:dictParam constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            [formData appendPartWithFileData:imageToUpload name:PICTURE fileName:@"temp.jpg" mimeType:@"image/jpg"];
            
        } progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if(dataBlock){
                if(responseObject==nil)
                    dataBlock(task.response,nil);
                else
                    dataBlock(responseObject,nil);
            }
            
        }
              failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                  NSLog(@"Error %@",error);
                   dataBlock(nil,error);
              }];
    }
    
}
-(void)getAddressFromGooglewithParamData:(NSMutableDictionary *)dictParam withBlock:(RequestCompletionBlock)block
{
    if (block) {
        dataBlock=[block copy];
    }
    //[raw urlEncodeUsingEncoding:NSUTF8Encoding]
    NSString *url=[NSString stringWithFormat:@"%@",Address_URL];
         //stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    AFHTTPSessionManager *manager=[AFHTTPSessionManager manager];
    
   // manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@[@"text/html",@"application/json"]];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.requestSerializer.timeoutInterval=600;
 
    if ([self.strReqMethod isEqualToString:POST_METHOD])
    {
        [manager POST:url parameters:dictParam progress:^(NSProgress * _Nonnull uploadProgress) {
            
        }
              success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
             if(dataBlock){
                 
                 if(responseObject==nil)
                     dataBlock(task.response,nil);
                 else
                     dataBlock(responseObject,nil);
             }
         }
              failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                  NSLog(@"Error %@",error);
                   dataBlock(nil,error);
              }];
        
    }
    else
    {
        [manager GET:url parameters:dictParam progress:^(NSProgress * _Nonnull uploadProgress) {
            
        }
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
             if(dataBlock){
                 if(responseObject==nil)
                     dataBlock(task.response,nil);
                 else
                     dataBlock(responseObject,nil);
             }
             
         }
             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 NSLog(@"Error %@",error);
                  dataBlock(nil,error);
             }];
        
    }

}

-(void)getAddressFromGooglewAutoCompletewithParamData:(NSMutableDictionary *)dictParam withBlock:(RequestCompletionBlock)block{
    
    if (block) {
        dataBlock=[block copy];
    }
    //[raw urlEncodeUsingEncoding:NSUTF8Encoding]
    NSString *url=[NSString stringWithFormat:@"%@",AutoComplete_URL];
                   //stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    AFHTTPSessionManager *manager=[AFHTTPSessionManager manager];
    
    //manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@[@"text/html",@"application/json"]];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.requestSerializer.timeoutInterval=600;
    if ([self.strReqMethod isEqualToString:POST_METHOD])
    {
        [manager POST:url parameters:dictParam progress:^(NSProgress * _Nonnull uploadProgress) {
            
        }
              success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
             if(dataBlock){
                 
                 if(responseObject==nil)
                     dataBlock(task.response,nil);
                 else
                     dataBlock(responseObject,nil);
             }
         }
              failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                  NSLog(@"Error %@",error);
                   dataBlock(nil,error);
              }];
        
    }
    else
    {
        [manager GET:url parameters:dictParam progress:^(NSProgress * _Nonnull uploadProgress) {
            
        }
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
             if(dataBlock){
                 if(responseObject==nil)
                     dataBlock(task.response,nil);
                 else
                     dataBlock(responseObject,nil);
             }
             
         }
             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 NSLog(@"Error %@",error);
                  dataBlock(nil,error);
             }];
        
    }

    
}
@end
