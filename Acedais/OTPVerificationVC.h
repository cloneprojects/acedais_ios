//
//  OTPVerificationVC.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 29/12/16.
//  Copyright © 2016 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OTPVerificationVC : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblVerify;
@property (weak, nonatomic) IBOutlet UIButton *btnWrongNumber;
- (IBAction)onWrongNumber:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtOTP;
@property (weak, nonatomic) IBOutlet UIButton *btnResend;
- (IBAction)onResend:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCallMe;
- (IBAction)onCallMe:(id)sender;
- (IBAction)onNext:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property(strong,nonatomic)NSString *strPhoneNumber;
@end
