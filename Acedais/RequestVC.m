//
//  RequestVC.m
//  ZoeChat
//
//  Created by admin on 16/05/18.
//  Copyright © 2018 Pyramidions Solution. All rights reserved.
//

#import "RequestVC.h"
#import "RequestTVC.h"
#import "AFNHelper.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "AppDelegate.h"
#import "Constants.h"
#import <AFNetworking/AFNetworking.h>
#import <SDWebImage/UIImageView+WebCache.h>
@interface RequestVC ()<UITableViewDelegate,UITableViewDataSource>
{
 AppDelegate *appdelegate;
    NSArray*RequestList;
}
@end

@implementation RequestVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.datarequest = [[NSMutableArray alloc]initWithObjects:@"sample1",@"sample2",@"sample3",@"sample4",@"sample5",@"sample6",@"sample6", nil];
      appdelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.vwTop.hidden = YES;
   // self.location = [[NSMutableArray alloc]initWithObjects:@"Location1",@"Location2",@"Location3",@"Location4",@"Location5",@"Location6",@"Location7", nil];
    // Do any additional setup after loading the view.
    [self Request_Api];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    RequestTVC *cell = [tableView dequeueReusableCellWithIdentifier:@"RequestTVC"];
    
    NSDictionary *dict = [RequestList objectAtIndex:indexPath.row];
    
    cell.lblgrpname.text = [dict valueForKey:@"groupName"];
    NSString *strImageURL1=[dict valueForKey:@"image"];
    if (![strImageURL1 isEqualToString:@" "])
    {
        
        cell.imggrp.image = [UIImage imageNamed:@"grp.png"];
        
            
    }
    else {
        [cell.imggrp sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"grp.png"]options:SDWebImageRefreshCached];
    }
  
    // cell.lblgrptime.text = [self.location objectAtIndex:indexPath.row];
//    cell.imggrp.image = ""
    cell.imggrp.layer.cornerRadius = cell.imggrp.frame.size.width / 2;
    cell.imggrp.layer.masksToBounds = true;
    cell.btnYes.tag = indexPath.row;
    cell.btnNo.tag = indexPath.row;
    [cell.btnYes addTarget:self action:@selector(Btnyes:) forControlEvents:UIControlEventTouchUpInside ];
    [cell.btnNo addTarget:self action:@selector(BtnNo:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(void)Btnyes:(UIButton*)sender
{
    NSDictionary *dict = [RequestList objectAtIndex: sender.tag];
    
    NSString *fromId = [dict valueForKey:@"fromId"];
    NSString *fromName = [dict valueForKey:@"fromName"];
    NSString *groupId = [dict valueForKey:@"groupId"];
    NSString *groupName = [dict valueForKey:@"groupName"];
    NSString *groupImage = [dict valueForKey:@"groupImage"];
    NSString *ParticipateId = [dict valueForKey:@"participantId"];
    NSString *participateName = [dict valueForKey:@"participantName"];
    NSString *Id = [dict valueForKey:@"_id"];
    
//    [appdelegate.socket emit:@"groupAccept" with:@[@{@"from": appdelegate.strID, @"fromId":fromId,@"fromName":fromName,@"groupId":groupId,@"groupName":groupName,@"groupImage":groupImage,@"participantId":ParticipateId,@"participantName":participateName,@"_id":Id}]];
    
    
//    NSString *strOnlineStatus=[NSString stringWithFormat:@"%@:onlineStatus",appdelegate.strID];
 
    
    
    
    
    [[appdelegate.socket emitWithAck:@"groupAccept" with:@[@{@"from": appdelegate.strID, @"from":fromId,@"fromName":fromName,@"groupId":groupId,@"groupName":groupName,@"groupImage":groupImage,@"participantId":ParticipateId,@"participantName":participateName,@"id":Id}]] timingOutAfter:1 callback:^(NSArray* data)
     {
         NSLog(@"%@",data);
         [self Request_Api];

     }];
    
    
    [self.requestTable reloadData];
    
  
    /*
    
    
    [appdelegate.socket on:strOnlineStatus callback:^(NSArray *data, SocketAckEmitter *ack)
     {
         NSLog(@"%@",data);
        
                              
    }];
   
    */
    
}

-(void)BtnNo:(UIButton*)sender
{
    
    
    NSDictionary *dict = [RequestList objectAtIndex: sender.tag];
    NSString *groupId = [dict valueForKey:@"groupId"];
    NSString *ParticipateId = [dict valueForKey:@"participantId"];
    NSLog(@"ParticipateId: %@",ParticipateId);
    NSLog(@"groupId: %@",groupId);
     NSString *Id = [dict valueForKey:@"_id"];
    [appdelegate.socket emit:@"groupReject" with:@[@{@"participantId":Id,@"groupId":groupId}]];
    [self.requestTable reloadData];
    [self Request_Api];
}

-(void)Request_Api
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:appdelegate.strID forKey:@"userId"];
    NSLog(@"Acedias Id:%@",appdelegate.strID);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:@"http://18.207.40.111:3000/user/groupRequest" parameters:dictParam progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         
         
         if (responseObject)
         {
             NSString *status = [responseObject objectForKey:@"error"];
             if ([status isEqualToString:@"false"])
             {
                 RequestList = [responseObject valueForKey:@"requestList"];
                 if (RequestList.count == 0){
                     self.vwTop.hidden = NO;
                 }
                 [self.requestTable reloadData];
             }
             else
             {
                 NSString *status = [responseObject objectForKey:@"message"];
                 
             }
         }
         else
         {
         }
     }
          failure:^(NSURLSessionTask *operation, NSError *error)
     {
         
         NSLog(@"Error: %@", error);
     }];
    
    
    /*
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:@"http://18.207.40.111:3000/user/groupRequest" parameters:dictParam progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        NSLog(@"JSON: %@", responseObject);
        NSDictionary *dataDict = responseObject;
        if([[dataDict valueForKey:@"error"]boolValue] == true)
        {
                 
            
             }
             else
             {
                 
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 // Configure for text only and offset down
                 hud.mode = MBProgressHUDModeText;
                 hud.label.text = [NSString stringWithFormat:@"%@",[dataDict valueForKey:@"error"]];
                 hud.margin = 10.f;
                 hud.yOffset = 150.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hideAnimated:YES afterDelay:2];
             }
    }];
     */
         
   
}
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return RequestList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}
@end
