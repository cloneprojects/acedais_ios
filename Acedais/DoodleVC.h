//
//  DoodleVC.h
//  Wohoo
//
//  Created by Pyramidions on 19/07/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACEDrawingView.h"
#import <AVFoundation/AVUtilities.h>
#import <QuartzCore/QuartzCore.h>
@interface DoodleVC : UIViewController
- (IBAction)ClickOnBack:(id)sender;
@property (weak, nonatomic) IBOutlet ACEDrawingView *doodleView;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (weak, nonatomic) IBOutlet UIButton *colorBtn;
@property (weak, nonatomic) IBOutlet UIButton *clearBtn;
- (IBAction)clickOnClrBtn:(id)sender;
- (IBAction)ClickOnColorBtn:(id)sender;
- (IBAction)ClickOnsendBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *colorView;
@property (weak, nonatomic) IBOutlet UIButton *cl;
@property (weak, nonatomic) IBOutlet UIButton *c2;
@property (weak, nonatomic) IBOutlet UIButton *c3;
@property (weak, nonatomic) IBOutlet UIButton *c4;
@property (weak, nonatomic) IBOutlet UIButton *c5;
@property (weak, nonatomic) IBOutlet UIButton *c6;
@property (weak, nonatomic) IBOutlet UIButton *c7;
@property (weak, nonatomic) IBOutlet UIButton *c8;
@property (weak, nonatomic) IBOutlet UIButton *c9;
@property (weak, nonatomic) IBOutlet UIButton *c10;
@property (weak, nonatomic) IBOutlet UIButton *c11;
@property (weak, nonatomic) IBOutlet UIButton *c12;


- (IBAction)clickOnC1:(id)sender;
- (IBAction)clickOnC2:(id)sender;
- (IBAction)clickOnC3:(id)sender;
- (IBAction)clickOnC4:(id)sender;
- (IBAction)clickOnC5:(id)sender;
- (IBAction)clickOnC6:(id)sender;
- (IBAction)clickOnC7:(id)sender;
- (IBAction)clickOnC8:(id)sender;
- (IBAction)clickOnC9:(id)sender;
- (IBAction)clickOnC10:(id)sender;
- (IBAction)clickOnC11:(id)sender;
- (IBAction)clickOnC12:(id)sender;




@property(strong, nonatomic)NSString *strZoeChatId;
@property(strong, nonatomic)NSString *strChatRoomType;
@end
