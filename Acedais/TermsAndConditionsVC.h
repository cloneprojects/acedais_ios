//
//  TermsAndConditionsVC.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 29/12/16.
//  Copyright © 2016 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Contacts/Contacts.h>


@interface TermsAndConditionsVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnAgree;
@property (weak, nonatomic) IBOutlet UIButton *btnTermsPrivacyPolicy;
@property (weak, nonatomic) IBOutlet UILabel *lblWelcome;
- (IBAction)onAgree:(id)sender;
- (IBAction)onViewTermsPrivacyPolicy:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *img;
@end
