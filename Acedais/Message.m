//
//  Message.m
//  Whatsapp
//
//  Created by Rafael Castro on 6/16/15.
//  Copyright (c) 2015 HummingBird. All rights reserved.
//

#import "Message.h"
@implementation Message

-(id)init
{
    
    self = [super init];
    if (self)
    {
        self.sender = MessageSenderMyself;
        self.status = MessageStatusSending;
        self.text = @"";
//        self.heigh = 44;
        self.date = [NSDate date];
        self.identifier = @"";
        self.type=@"";
        self.image=nil;
        self.caption=@"";
        self.Latitude=@"";
        self.Longitude=@"";
        self.name=@"";
        self.contactNo=@"";
        self.contactName=@"";
        self.checkStar = @"";
        self.contentType =@"";
        self.HeaderText= @"";
        self.isDownloaded= @"";
        self.seenTime=@"";
        self.deliveredTime=@"";
        self.showPreview=@"0";
        self.linkDescription=@"";
        self.linkLogo=@"";
        self.linkTitle=@"";
        AppDelegate *app = [[UIApplication sharedApplication] delegate];
        NSLog(@"%@",app.heightStr);
        NSLog(@"%f",app.dynamicRowHeight);

        if ([app.heightStr  isEqual: @"TRUE"])
        {
            if (app.dynamicRowHeight >= 124)
            {
                self.heigh = app.dynamicRowHeight + 150;
            }
            else
            {
            self.heigh = app.dynamicRowHeight;
            }
            app.heightStr = @"";
            app.dynamicRowHeight = 0.0;
        }
        else
        {
            self.heigh = 44;

        }
    }
    return self;
}

+(Message *)messageFromDictionary:(NSDictionary *)dictionary
{
    Message *message = [[Message alloc] init];
    message.text = dictionary[@"text"];
    message.identifier = dictionary[@"message_id"];
    message.status = [dictionary[@"status"] integerValue] + 1;
    message.type=dictionary[@"type"];
    message.image=dictionary[@"image"];
    NSString *dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSSSS";
    message.checkStar=dictionary[@"checkStar"];
    message.HeaderText=dictionary[@"HeaderText"];
    message.isDownloaded=dictionary[@"isDownloaded"];
    message.seenTime=dictionary[@"seenTime"];
    message.deliveredTime=dictionary[@"deliveredTime"];
    message.showPreview=dictionary[@"showPreview"];
    message.linkTitle=dictionary[@"linkTitle"];
    message.linkLogo=dictionary[@"linkLogo"];
    message.linkDescription=dictionary[@"linkDescription"];



    
    //Date in UTC
    NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
    [inputDateFormatter setTimeZone:inputTimeZone];
    [inputDateFormatter setDateFormat:dateFormat];
    NSDate *date = [inputDateFormatter dateFromString:dictionary[@"sent"]];
    
    //Convert time in UTC to Local TimeZone
    NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
    NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
    [outputDateFormatter setTimeZone:outputTimeZone];
    [outputDateFormatter setDateFormat:dateFormat];
    NSString *outputString = [outputDateFormatter stringFromDate:date];
    
    message.date = [outputDateFormatter dateFromString:outputString];
    
    return message;
}
@end
