//
//  GettingPhoneNumberVC.m
//  ZoeChat
//
//  Created by Pyramidions Solution on 29/12/16.
//  Copyright © 2016 Pyramidions Solution. All rights reserved.
//

#import "GettingPhoneNumberVC.h"
#import "AppDelegate.h"
#import "OTPVerificationVC.h"
#import "Constants.h"
#import "AFNHelper.h"
#import "MBProgressHUD.h"
#import <AFNetworking/AFNetworking.h>

@interface GettingPhoneNumberVC ()
{
    NSMutableArray *arrForCountry;
    UIView *viewContentTable;
    UITableView *tableViewPicker;
  //  NSMutableArray *arrForTableView;
    AppDelegate *appDelegate;
    
    UITextField *currentTextView;
    UITextView *currentTextView1;
    bool keyboardIsShown;
    UITapGestureRecognizer *tapGesture;
}
@end

@implementation GettingPhoneNumberVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     // [self.navigationController.navigationBar setHidden:NO];
    
    [self.navigationController.navigationBar setTranslucent:NO];
    
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
     [self setFontFamily:FONT_NORMAL forView:self.view andSubViews:YES];
    
    CALayer *midVerticleBorder = [CALayer layer];
    midVerticleBorder.frame = CGRectMake(80.0f, 0.0f, 1.0f, _viewBG.frame.size.height);
    midVerticleBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                     alpha:1.0f].CGColor;
    [_viewBG.layer addSublayer:midVerticleBorder];
    self.title=@"Your PhoneNumber";
//    NSDictionary *size = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:FONT_NORMAL size:12.0],NSFontAttributeName, nil];
//    self.navigationController.navigationBar.titleTextAttributes = size;
//    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],UITextAttributeTextColor, nil];
//    
//    self.navigationController.navigationBar.titleTextAttributes = attributes;
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:
    @{NSForegroundColorAttributeName:[UIColor whiteColor],
      NSFontAttributeName:[UIFont fontWithName:FONT_HEAVY size:15]}];

    CALayer *middleBorder = [CALayer layer];
    middleBorder.frame = CGRectMake(0.0f, 42.0f, _viewBG.frame.size.width, 1.0f);
    middleBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                    alpha:1.0f].CGColor;
    [_viewBG.layer addSublayer:middleBorder];

    CALayer *upperBorder = [CALayer layer];
    upperBorder.frame = CGRectMake(0.0f, 0.0f, _viewBG.frame.size.width, 1.5f);
    upperBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                     alpha:1.0f].CGColor;
    [_viewBG.layer addSublayer:upperBorder];
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, 84.0f, _viewBG.frame.size.width, 1.5f);
    bottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                     alpha:1.0f].CGColor;
    [_viewBG.layer addSublayer:bottomBorder];
    
    UIView *viewLeft=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.txtPhoneNumber.leftView=viewLeft;
    self.txtPhoneNumber.leftViewMode=UITextFieldViewModeAlways;
    _txtPhoneNumber.delegate=self;
  //  _txtPhoneNumber.text=@"9786543210";
    _btnNext.clipsToBounds=YES;
    _btnNext.layer.cornerRadius=4;
    [_btnNext setFont:[UIFont fontWithName:FONT_MEDIUM size:[[_btnNext font] pointSize]]];
    
    self.btnCountry.titleEdgeInsets=UIEdgeInsetsMake(0, 5, 0, 0);
    
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"countrycodes" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    arrForCountry = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    NSString *strAlpha=[[arrForCountry objectAtIndex:0] valueForKey:@"alpha-2"];
    NSString *strSTD=[[arrForCountry objectAtIndex:0] valueForKey:@"phone-code"];
  //  NSString *strValue1=[NSString stringWithFormat:@"%@ (%@)",strAlpha, strSTD];
    
    //ScrollView moving
    
    //  _scrollView.contentSize=CGSizeMake(self.view.frame.size.width, self.btnRegister.frame.origin.y+70);
    
    
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.scrollView.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    self.scrollView.contentSize = contentRect.size;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyBoard)];
    
    [self.view addGestureRecognizer:tap];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"GettingPhNo_OTPVerification"])
    {
        OTPVerificationVC *OTP=[segue destinationViewController];
        NSString *strPhNo=[NSString stringWithFormat:@"%@ %@",_lblCountryCode.text,_txtPhoneNumber.text];
        OTP.strPhoneNumber=strPhNo;
    }
    
}





- (IBAction)onNext:(id)sender
{
    if ([appDelegate.strInternetMode isEqualToString:@"online"])
    {
        if ([self.txtPhoneNumber.text isEqualToString:@""])
        {
            [self.txtPhoneNumber resignFirstResponder];
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            // Configure for text only and offset down
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [NSString stringWithFormat:@"Please enter phone number"];
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hideAnimated:YES afterDelay:2];
            
            return;
        }
//        else if (self.txtPhoneNumber.text.length!=10)
//        {
//            [self.txtPhoneNumber resignFirstResponder];
//            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//            
//            // Configure for text only and offset down
//            hud.mode = MBProgressHUDModeText;
//            hud.label.text = [NSString stringWithFormat:@"Please enter phone valid number"];
//            hud.margin = 10.f;
//            hud.yOffset = 150.f;
//            hud.removeFromSuperViewOnHide = YES;
//            [hud hideAnimated:YES afterDelay:2];
//            return;
//        }
        NSString *strCounCode=[NSString stringWithFormat:@"%@",_lblCountryCode.text];
        strCounCode = [strCounCode stringByReplacingOccurrencesOfString:@"+" withString:@""];
        [[NSUserDefaults standardUserDefaults] setObject:strCounCode forKey:@"getCountryCode"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        appDelegate.strCountryCode=[NSString stringWithFormat:@"%@",strCounCode];
        
        
      //Removing 0[if it is placed after countrycode] from Phonenumber
        NSString *FirstVal=[self.txtPhoneNumber.text substringToIndex:1];
        if ([FirstVal isEqualToString:@"0"])
        {
            NSString *str = self.txtPhoneNumber.text;
            NSString *newStr = [str substringFromIndex:1];
            appDelegate.strMobileNumber=[NSString stringWithFormat:@"%@",newStr];
        }
        else
        {
            appDelegate.strMobileNumber=[NSString stringWithFormat:@"%@",self.txtPhoneNumber.text];
        }
        
        MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDAnimationFade;
        hud.label.text = @"Loading";
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setValue:appDelegate.strCountryCode forKey:@"countryCode"];
        [dictParam setValue:appDelegate.strMobileNumber forKey:@"mobileNumber"];
        
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager POST:@"http://18.207.40.111:3000/user/getOTP" parameters:dictParam progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
            NSLog(@"JSON: %@", responseObject);
            NSDictionary *dataDict = responseObject;
            if([[dataDict valueForKey:@"error"]boolValue] == true)
            {
                NSString*Message = [dataDict valueForKey:@"message"];
                
                //Alert
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Acedais"
                                                                              message:Message
                                                                       preferredStyle:UIAlertControllerStyleAlert];
                
                alert.view.tintColor=[UIColor blackColor];

                UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Ok"
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action)
                                            {
                                                NSLog(@"you pressed No, thanks button");
                                            }];
                
                
                
                
                [alert addAction:yesButton];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
            else{
                appDelegate.strOTP=[NSString stringWithFormat:@"%@",[dataDict valueForKey:@"otp"]];
                
                appDelegate.strID=[NSString stringWithFormat:@"%@%@",appDelegate.strCountryCode,appDelegate.strMobileNumber];
                
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setValue:appDelegate.strCountryCode forKey:@"countryCode"];
                [defaults setValue:appDelegate.strMobileNumber forKey:@"mobileNumber"];
                
                
                
                [self performSegueWithIdentifier:@"GettingPhNo_OTPVerification" sender:self];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
            NSLog(@"Error: %@", error);
        }];
        
        
        
        /*
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_GET_OTP withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             NSLog(@"Params:%@",dictParam);
             NSLog(@"Url:%@",FILE_GET_OTP);
             [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
               NSLog(@"Response: %@",response);
             if (response)
             {
                   NSLog(@"Response: %@",response);
                 if ([[response valueForKey:@"error"] boolValue]==false)
                 {
                     appDelegate.strCountryCode=[NSString stringWithFormat:@"%@",[response valueForKey:@"countryCode"]];
                     appDelegate.strMobileNumber=[NSString stringWithFormat:@"%@",[response valueForKey:@"mobileNumber"]];
                     appDelegate.strOTP=[NSString stringWithFormat:@"%@",[response valueForKey:@"otp"]];
                     
                     appDelegate.strID=[NSString stringWithFormat:@"%@%@",appDelegate.strCountryCode,appDelegate.strMobileNumber];
                     
                     
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setValue:appDelegate.strCountryCode forKey:@"countryCode"];
                     [defaults setValue:appDelegate.strMobileNumber forKey:@"mobileNumber"];
                     
                     
                     
                     [self performSegueWithIdentifier:@"GettingPhNo_OTPVerification" sender:self];
                 }
                 else
                 {
                     
                     //                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     //
                     //                     // Configure for text only and offset down
                     //                     hud.mode = MBProgressHUDModeText;
                     //                     hud.label.text = [NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                     //                     hud.margin = 10.f;
                     //                     hud.yOffset = 150.f;
                     //                     hud.removeFromSuperViewOnHide = YES;
                     //                     [hud hideAnimated:YES afterDelay:2];
                     
                     
                     
                     
                     
                     //                     appDelegate.strCountryCode=[NSString stringWithFormat:@"%@",[response valueForKey:@"countryCode"]];
                     //                     appDelegate.strMobileNumber=[NSString stringWithFormat:@"%@",[response valueForKey:@"mobileNumber"]];
                     appDelegate.strOTP=[NSString stringWithFormat:@"%@",[response valueForKey:@"otp"]];
                     
                     appDelegate.strID=[NSString stringWithFormat:@"%@%@",appDelegate.strCountryCode,appDelegate.strMobileNumber];
                     
                     
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setValue:appDelegate.strCountryCode forKey:@"countryCode"];
                     [defaults setValue:appDelegate.strMobileNumber forKey:@"mobileNumber"];
                     
                     
                     
                     [self performSegueWithIdentifier:@"GettingPhNo_OTPVerification" sender:self];
                     
                     
                 }
             }
             
         }];
         
         */
        
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Please check your internet connection"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.label.adjustsFontSizeToFitWidth=YES;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:2];
        
    }
    
    
    
}
- (IBAction)onCountry:(id)sender forEvent:(UIEvent *)event
{
    
    UIView *View = (UIView *)sender;
    UITouch *touch = [[event touchesForView:View] anyObject];
    CGPoint location = [touch locationInView:self.view];
    NSLog(@"Location in button: %f, %f", location.x, location.y);
    
    
    
    [viewContentTable removeFromSuperview];
   // arrForCountry=[[NSMutableArray alloc]init];
   // arrForCountry=[arrForCountry copy];
    
    viewContentTable = [[UIView alloc] initWithFrame:CGRectMake(location.x-50, location.y-75, 200, 250)];
    
    viewContentTable.backgroundColor = [UIColor whiteColor];
    viewContentTable.layer.shadowOpacity = 0.6f;
    viewContentTable.layer.shadowOffset = CGSizeMake(0.4f, 0.4f);
    viewContentTable.layer.shadowRadius = 2.0f;
    viewContentTable.layer.cornerRadius = 2.5f;
    viewContentTable.tag=123;
    [self.view addSubview:viewContentTable];
    
    tableViewPicker=[[UITableView alloc]initWithFrame:CGRectMake(5, 10, viewContentTable.frame.size.width-10, viewContentTable.frame.size.height-20)];
    tableViewPicker.delegate=self;
    tableViewPicker.dataSource=self;
    tableViewPicker.separatorStyle=UITableViewCellSeparatorStyleNone;
    [viewContentTable addSubview:tableViewPicker];
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(removeTableView)];
    
    [self.view addGestureRecognizer:tap1];
}

-(void)removeTableView
{
    [viewContentTable removeFromSuperview];
}
#pragma mark -
#pragma mark - TableView Delagate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrForCountry.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        
        
    }
    
    NSString *strValue;
    NSDictionary *dictOptName;
    
    //  "name":"Afghanistan",
    //"alpha-2":"AF",
    //"country-code":"004",
    //"phone-code":"+93"
    
    dictOptName=[arrForCountry objectAtIndex:indexPath.row];
    
    
      /*  NSString *strAlpha=[dictOptName valueForKey:@"alpha-2"];
        NSString *strSTD=[dictOptName valueForKey:@"phone-code"];
        strValue=[NSString stringWithFormat:@"%@ (%@)",strAlpha, strSTD]; */
   
   strValue=[dictOptName valueForKey:@"name"];
    
    cell.textLabel.text=strValue;
    cell.textLabel.textAlignment=NSTextAlignmentLeft;
    cell.textLabel.font=[UIFont systemFontOfSize:12];
    cell.textLabel.textColor=[UIColor blackColor];
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureAction:)];
    [recognizer setNumberOfTapsRequired:1];
    //  scrollView.userInteractionEnabled = YES;
    [cell addGestureRecognizer:recognizer];
    
    
    return cell;
    
}

-(void)gestureAction:(UITapGestureRecognizer *) sender
{
    
    CGPoint touchLocation = [sender locationOfTouch:0 inView:tableViewPicker];
    NSIndexPath *indexPath = [tableViewPicker indexPathForRowAtPoint:touchLocation];
    
    NSString *strCountryName=[tableViewPicker cellForRowAtIndexPath:indexPath].textLabel.text;
    [self.btnCountry setTitle:strCountryName forState:UIControlStateNormal];
    
    NSDictionary *dictSelected=[arrForCountry objectAtIndex:indexPath.row];
    self.lblCountryCode.text=[dictSelected valueForKey:@"phone-code"];
    
    
    [viewContentTable removeFromSuperview];
    [tableViewPicker removeFromSuperview];
    
    // NSDictionary *dictLocal=[arrListAllPub objectAtIndex:indexPath.row];
}



#pragma mark -
#pragma mark - Keyboard

-(void)dismissKeyBoard
{
    // [tableTexture removeFromSuperview];
    
    for(UIView *subView in self.scrollView.subviews)
    {
        if([subView isKindOfClass:[UITextField class]])
        {
            if([subView isFirstResponder])
            {
                [subView resignFirstResponder];
                break;
            }
        }
        else if([subView isKindOfClass:[UITextView class]])
        {
            if([subView isFirstResponder])
            {
                [subView resignFirstResponder];
                break;
            }
        }
        
        for (UIView *subView1 in subView.subviews)
        {
            if([subView1 isKindOfClass:[UITextField class]])
            {
                if([subView1 isFirstResponder])
                {
                    [subView1 resignFirstResponder];
                    break;
                }
            }
        }
        
    }
    
    
    
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
       return YES;
}
-(void) textFieldDidBeginEditing:(UITextField *)textField
{
    
    currentTextView=textField;
    [self moveScrollView:textField.frame];
    
    /* if (textField!= _txtPassword)
     {
     
     [btnShowPwd removeFromSuperview];
     _txtPassword.secureTextEntry=YES;
     } */
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
    currentTextView1=textView;
    [self moveScrollView:textView.frame];
}
- (void)keyboardDidHide:(NSNotification *)n
{
    keyboardIsShown = NO;
    [self.view removeGestureRecognizer:tapGesture];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardDidShow:(NSNotification *)n
{
    if (keyboardIsShown) {
        return;
    }
    
    keyboardIsShown = YES;
    [self.scrollView addGestureRecognizer:tapGesture];
}

- (void) moveScrollView:(CGRect) rect
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    
    CGSize kbSize = CGSizeMake(32, 260); // keyboard height
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.scrollView.frame;
    aRect.size.height -= (kbSize.height+55);
    if (!CGRectContainsPoint(aRect, rect.origin) )
    {
        CGPoint scrollPoint = CGPointMake(0.0, rect.origin.y-kbSize.height+100);
        
        if(screenHeight==480)
            scrollPoint = CGPointMake(0.0, rect.origin.y-kbSize.height+100+90);
        
        if(scrollPoint.y>0)
            [self.self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

- (void) hideKeyBoard
{
    
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    //  NSLog(@"Keyboard is hidden");
    UIEdgeInsets content=UIEdgeInsetsMake(0, 0, 0, 0);
    self.scrollView.contentInset = content;
    self.scrollView.scrollIndicatorInsets = content;}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    if (textField == _txtPhoneNumber) {
       NSString *phoneString = [textField.text stringByReplacingCharactersInRange:range
                                                                       withString:string];
        self.navigationItem.title=[NSString stringWithFormat:@"%@ %@",_lblCountryCode.text,phoneString];
    }
    if ([_txtPhoneNumber.text isEqualToString:@""])
    {
        self.navigationItem.title=@"Your PhoneNumber";
        
        
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 15;
    
    }

#pragma mark -
#pragma mark - Font

-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)view;
        [btn setFont:[UIFont fontWithName:fontFamily size:[[btn font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *txtFld = (UITextField *)view;
        [txtFld setFont:[UIFont fontWithName:fontFamily size:[[txtFld font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextView class]])
    {
        UITextView *txtVw = (UITextView *)view;
        [txtVw setFont:[UIFont fontWithName:fontFamily size:[[txtVw font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}



@end
