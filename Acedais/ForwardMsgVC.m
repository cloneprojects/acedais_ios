//
//  ForwardMsgVC.m
//  ZoeChat
//
//  Created by macmini on 07/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "ForwardMsgVC.h"
#import "DBClass.h"
#import "SingleChatVC.h"
#import "RecentChatsCell.h"
#import "Constants.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AppDelegate.h"
#import "OtherContactsCell.h"
#import "Message.h"


@interface ForwardMsgVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *dataArray;
    DBClass *db_class;
    NSMutableArray *arrChats, *arrForTableView, *arrZoechatIds, *CheckarrZoechatIds;
    NSMutableArray *arrContacts, *arrForTableView1, *arrSelectedContacts;
    AppDelegate *delegate;
    UILabel *lbl;
    NSDictionary *dictToSendSingleChat;
    BOOL isAvailinChatsTable;
    NSIndexPath *forwardIndexpath;
    NSString *getFrwdString;


}


@end

@implementation ForwardMsgVC
@synthesize typeString,textString,ContactNameString,ContactNumberString,latitudeString,longitudeString,strZoeChatId,strChatRoomType,captionString,getPreviewVal,getLinkTitle,getLinkLogo,getLinkDes;



- (void)viewDidLoad {
    [super viewDidLoad];
//    [self.navigationController setNavigationBarHidden:YES];
    delegate =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    db_class=[[DBClass alloc]init];
    arrChats=[[NSMutableArray alloc]init];
    arrForTableView=[[NSMutableArray alloc]init];
    arrZoechatIds=[[NSMutableArray alloc]init];
    arrContacts=[[NSMutableArray alloc]init];
    CheckarrZoechatIds=[[NSMutableArray alloc]init];
    arrForTableView1=[[NSMutableArray alloc]init];
    arrSelectedContacts=[[NSMutableArray alloc]init];
    [self.forwardBtn setTintColor:[UIColor clearColor]];
    self.forwardBtn.enabled = NO;
    self.forwardBtn.accessibilityElementsHidden = YES;
    [self getRecentChatsContacts];
    [self RefreshContacts];
    NSLog(@"%@",typeString);
    NSLog(@"%@",textString);
    isAvailinChatsTable=NO;

}
-(void)getRecentChatsContacts
{
    arrChats=[db_class getChats];
    if (arrChats.count!=0)
    {
        for (int i=0; i<arrChats.count; i++)
        {
            NSMutableDictionary *dictSing=[arrChats objectAtIndex:i];
            NSDictionary *dictInfo = [db_class getUserInfo:[dictSing valueForKey:@"chatRoomId"]];
            if ([[dictSing valueForKey:@"chatRoomType"] isEqualToString:@"0"])
            {
                [ dictSing setObject:[dictInfo valueForKey:@"name"] forKey:@"name"];
                [ dictSing setObject:[dictInfo valueForKey:@"image"] forKey:@"image"];
                NSString *getZoeID=[dictInfo valueForKey:@"zoechatid"];
                [arrZoechatIds addObject:getZoeID];
            }
            else
            {
                NSString *getGroupname = [dictSing valueForKey:@"groupName"];
                NSString *getGroupimg = [dictSing valueForKey:@"groupImage"];
                [ dictSing setObject:getGroupname forKey:@"name"];
                [ dictSing setObject:getGroupimg forKey:@"image"];
            }
            
            NSLog(@"%@",dictSing);
            NSLog(@"%@",arrZoechatIds);
            
            [arrChats replaceObjectAtIndex:i withObject:dictSing];
        }
    }
    else
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:
                              @"Data not found" message:nil delegate:nil cancelButtonTitle:
                              @"OK" otherButtonTitles:nil];
        [alert show];
        
        
    }
    NSLog(@"%@",arrChats);
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"dateTime" ascending:NO];
    arrChats=[arrChats sortedArrayUsingDescriptors:@[sort]];
    arrForTableView=[NSMutableArray arrayWithArray:arrChats];
//    [_RecentChatTblView setFrame:CGRectMake(_RecentChatTblView.frame.origin.x, _RecentChatTblView.frame.origin.y, _RecentChatTblView.frame.size.width,(55.0f*([arrForTableView count])))];
    [_RecentChatTblView reloadData];
//    lbl=[[UILabel alloc]initWithFrame:CGRectMake(_RecentChatTblView.frame.size.height, _RecentChatTblView.frame.size.height, 200, 30)];
//    lbl.text = @"OTHER CONTACTS";
//     lbl.font=[UIFont fontWithName:FONT_NORMAL size:20];
//    [self.view addSubview:lbl];


}
//otherContacts
-(void)RefreshContacts
{
    
    arrContacts=[[NSMutableArray alloc]init];
    
    NSMutableArray *arrZoeCont=[[NSMutableArray alloc]init];
    NSArray *arrCont=[db_class getUsers];
    if (arrCont.count!=0)
    {
        for (int i=0; i<arrCont.count; i++)
        {
            NSDictionary *dictCont=[arrCont objectAtIndex:i];
            NSString *strShowInContacts=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"showincontactspage"]];
            NSString *strzoe=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"zoechatid"]];
            if ([strShowInContacts boolValue]==true)
            {
                
                if (![delegate.strID isEqualToString:strzoe])
                {
                    
                    [arrZoeCont addObject:dictCont];
//                    for (int i=0; i<arrZoeCont.count; i++)
//                    {
//                        NSString *checkzoeID=[arrZoechatIds objectAtIndex:i];
//                        if (![arrZoeCont containsObject:checkzoeID])
//                        {
//                            [CheckarrZoechatIds addObject:arrZoeCont];
//                        }
//                        
//                            
//                            
//                    }
//
                    //[CheckarrZoechatIds addObject:strzoe];

                }
                
            }
            
        }
    }
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    arrContacts=[arrZoeCont sortedArrayUsingDescriptors:@[sort]];
    arrForTableView1=[NSMutableArray arrayWithArray:arrContacts];
//    arrContacts=[CheckarrZoechatIds sortedArrayUsingDescriptors:@[sort]];
//    arrForTableView1=[NSMutableArray arrayWithArray:arrContacts];
    NSLog(@"%@",CheckarrZoechatIds);
    [_contactsTblVie setFrame:CGRectMake(_contactsTblVie.frame.origin.x, _contactsTblVie.frame.origin.y, _contactsTblVie.frame.size.width,(55.0f*([arrForTableView1 count])))];
    [_contactsTblVie reloadData];
    
}
//-(void)getOtherContacts
//{
//    for (int i=0; i<arrSelectedContacts.count; i++)
//    {
//        NSString *finalContact=[arrZoechatIds objectAtIndex:i];
//        if ([arrSelectedContacts containsObject:finalContact])
//        {
//            
//        }
//        else{
//            
//        }
//        
//    }
//
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  55;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowCount;
    
    if (tableView.tag == 11)
    {
        rowCount=arrForTableView.count;
    }
    else if(tableView.tag == 111)
    {
        rowCount=arrForTableView1.count;

    }
    return rowCount;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    NSString *InitialStr;
    UIColor *bgColor;
    if (tableView.tag == 11)
    {

    RecentChatsCell *cell=(RecentChatsCell *)[tableView dequeueReusableCellWithIdentifier:@"RecentChatsCell"];
        [cell.RecentInitialLbl setHidden:YES];

        NSDictionary *dictArt=[arrForTableView objectAtIndex:indexPath.row];
        NSString *getChatroomType=[dictArt valueForKey:@"chatRoomType"];
        cell.nameLbl.text=[dictArt valueForKey:@"name"];
        cell.nameLbl.font=[UIFont fontWithName:FONT_NORMAL size:16];
        InitialStr=[cell.nameLbl.text substringToIndex:1];
        if ([InitialStr isEqualToString:@"A"])
        {
            bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"B"])
        {
            bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"C"])
        {
            bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"D"])
        {
            bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"E"])
        {
            bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"F"])
        {
            bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"G"])
        {
            bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"H"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"I"])
        {
            bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"J"])
        {
            bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"K"])
        {
            bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"L"])
        {
            bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"M"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"N"])
        {
            bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"O"])
        {
            bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"P"])
        {
            bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"Q"])
        {
            bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"R"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"S"])
        {
            bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"T"])
        {
            bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"U"])
        {
            bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"V"])
        {
            bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"W"])
        {
            bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"X"])
        {
            bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"Y"])
        {
            bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"Z"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
        }

    NSString *strImage=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"image"]];
        if ([getChatroomType isEqualToString:@"0"])
        {
            cell.profileImgView.image=[UIImage imageNamed:@"clear.png"];
            if (![strImage isEqualToString:@"(null)"])
                
            {
                if (![strImage isEqualToString:@""])
                {
                    [cell.profileImgView sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
                }
            }
            else
            {
                [cell.RecentInitialLbl setHidden:NO];
                
                
                cell.RecentInitialLbl.text=InitialStr;
                cell.RecentInitialLbl.backgroundColor=bgColor;
            }

        }
        else if ([getChatroomType isEqualToString:@"1"])
        {
            [cell.profileImgView sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"grp.png"]options:SDWebImageRefreshCached];
        }
        else if ([getChatroomType isEqualToString:@"2"])
        {
            [cell.profileImgView sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"megaphone.png"]options:SDWebImageRefreshCached];
        }
    
    cell.profileImgView.layer.cornerRadius=cell.profileImgView.frame.size.height/2;
    cell.profileImgView.clipsToBounds=YES;
        cell.RecentInitialLbl.layer.cornerRadius=cell.RecentInitialLbl.frame.size.height/2;
        cell.RecentInitialLbl.clipsToBounds=YES;
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
    }
    else if(tableView.tag == 111)
    {
        OtherContactsCell *cell=(OtherContactsCell *)[tableView dequeueReusableCellWithIdentifier:@"OtherContactsCell"];
        NSDictionary *dictArt=[arrForTableView1 objectAtIndex:indexPath.row];
        [cell.otherInitialLbl setHidden:YES];

        cell.contactNameLbl.text=[dictArt valueForKey:@"name"] ;
        cell.contactNameLbl.font=[UIFont fontWithName:FONT_NORMAL size:16];
        InitialStr=[cell.contactNameLbl.text substringToIndex:1];
        if ([InitialStr isEqualToString:@"A"])
        {
            bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"B"])
        {
            bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"C"])
        {
            bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"D"])
        {
            bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"E"])
        {
            bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"F"])
        {
            bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"G"])
        {
            bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"H"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"I"])
        {
            bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"J"])
        {
            bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"K"])
        {
            bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"L"])
        {
            bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"M"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"N"])
        {
            bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"O"])
        {
            bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"P"])
        {
            bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"Q"])
        {
            bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"R"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"S"])
        {
            bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"T"])
        {
            bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"U"])
        {
            bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"V"])
        {
            bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"W"])
        {
            bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"X"])
        {
            bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"Y"])
        {
            bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"Z"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
        }

        NSString *strImage=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"image"]];
        
            cell.ContactImgView.image=[UIImage imageNamed:@"clear.png"];
            if (![strImage isEqualToString:@"(null)"])
                
                
            {
                if (![strImage isEqualToString:@""])
                {
                    [cell.ContactImgView sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
                }
            }
            else
            {
                [cell.otherInitialLbl setHidden:NO];
                
                
                cell.otherInitialLbl.text=InitialStr;
                cell.otherInitialLbl.backgroundColor=bgColor;
            }

        
        cell.ContactImgView.layer.cornerRadius=cell.ContactImgView.frame.size.height/2;
        cell.ContactImgView.clipsToBounds=YES;
        cell.otherInitialLbl.layer.cornerRadius=cell.otherInitialLbl.frame.size.height/2;
        cell.otherInitialLbl.clipsToBounds=YES;
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;

    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView.tag == 11)
    {

            RecentChatsCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//            cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        [arrSelectedContacts addObject:[arrForTableView objectAtIndex:indexPath.row]];
        dictToSendSingleChat=[arrForTableView objectAtIndex:indexPath.row];
        forwardIndexpath=indexPath;
        getFrwdString=cell.nameLbl.text;
    }
    else if (tableView.tag == 111)
    {
        long long milliSeconds=[self convertDateToMilliseconds:[NSDate date]];
                NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
                NSLog(@"%@",strMilliSeconds);
        long long milli=[strMilliSeconds longLongValue];

        NSDate *dateTime=[self convertMillisecondsToDate:milli];
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"yyyyMMddHHmmssSSS"];
        NSString *DateString = [dateFormatter1 stringFromDate:dateTime];
        NSMutableDictionary *contactDict = [[NSMutableDictionary alloc]init];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        OtherContactsCell *cell = [tableView cellForRowAtIndexPath:indexPath];
       // cell.accessoryType = UITableViewCellAccessoryCheckmark;
        forwardIndexpath=indexPath;
        getFrwdString=cell.contactNameLbl.text;

        [contactDict setObject:[[arrForTableView1 valueForKey:@"mobile"]objectAtIndex:indexPath.row] forKey:@"chatRoomId"];
        [contactDict setObject:@"0" forKey:@"chatRoomType"];

        [contactDict setObject:DateString forKey:@"dateTime"];

        [contactDict setObject:@"" forKey:@"groupImage"];

        [contactDict setObject:@"" forKey:@"groupName"];

        [contactDict setObject:[[arrForTableView1 valueForKey:@"image"]objectAtIndex:indexPath.row]  forKey:@"image"];
        [contactDict setObject:@"0" forKey:@"isDelete"];

        [contactDict setObject:textString forKey:@"lastMessage"];
        [contactDict setObject:@"sent" forKey:@"lastMessageStatus"];

        [contactDict setObject:strMilliSeconds forKey:@"lastMessageTime"];
         [contactDict setObject:typeString forKey:@"lastMessageType"];
         [contactDict setObject:[[arrForTableView1 valueForKey:@"name"]objectAtIndex:indexPath.row]  forKey:@"name"];
         [contactDict setObject:@"0" forKey:@"sender"];
        [contactDict setObject:delegate.strID forKey:@"sentBy"];

        [contactDict setObject:@"0" forKey:@"unreadCount"];

       // [resultArray addObject:contactDict];
        NSLog(@"%@",dictToSendSingleChat);

        NSLog(@"%lu",(unsigned long)dictToSendSingleChat.count);

        dictToSendSingleChat=contactDict;
        NSLog(@"%@",dictToSendSingleChat);


    }
//    self.forwardBtn.enabled = YES;
//    self.forwardBtn.tintColor=[[UIColor alloc] initWithRed:38/255.f green:186/255.f blue:154/255.f alpha:1];
    [self showPopView];

    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"ForwardMsg"])
    {
        SingleChatVC *Single=[segue destinationViewController];
        Single.dictDetails=dictToSendSingleChat;
        
        NSLog(@"%@",Single.dictDetails);
    }
    
}

/*
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 11)
    {
    RecentChatsCell *cell = (RecentChatsCell*)[tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryNone;
    [arrSelectedContacts removeObject:[arrForTableView objectAtIndex:indexPath.row]];
    }
    else if (tableView.tag == 111)
    {
        OtherContactsCell *cell = (OtherContactsCell*)[tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryNone;
        [arrSelectedContacts removeObject:[arrForTableView1 objectAtIndex:indexPath.row]];
    }
}
*/
- (IBAction)clickOnCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
/*
- (IBAction)ClickOnForwardBtn:(id)sender {
   // [self.navigationController popViewControllerAnimated:YES];
    BOOL success=NO;

    NSString *chatId=[dictToSendSingleChat valueForKey:@"chatRoomId"];
    NSString *chatroomType=[dictToSendSingleChat valueForKey:@"chatRoomType"];
    NSString *strMsgId = [[NSUUID UUID] UUIDString]; //id
    long long milliSeconds=[self convertDateToMilliseconds:[NSDate date]];
    NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
    NSLog(@"%@",strMilliSeconds);
    NSString *trimmedString = [textString stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    trimmedString = [trimmedString stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    isAvailinChatsTable=[db_class doesChatExist:chatId];
    
    if (![trimmedString isEqualToString:@""])
    {
        if (isAvailinChatsTable==NO)
        {
            success=[db_class insertChats:@"" chatRoomId:chatId chatRoomType:chatroomType sender:@"0" lastMessage:trimmedString lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:typeString unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:delegate.strID isDelete:@"0" isLock:@"0" password:@""];
            if (success==YES)
            {
                isAvailinChatsTable=YES;
            }
        }
        else
        {
            success=[db_class updateLastMessage:chatId sender:@"0" lastMessage:trimmedString lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:typeString unReadCount:@"0"];
            NSLog(@"success");
        }
        
        success=[db_class insertChatMessages:strMsgId userId:chatId groupId:chatId chatRoomType:chatroomType content:trimmedString contentType:typeString contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0"];
        if ([typeString isEqualToString:@"location"])
        {
            NSString *strURL=[NSString stringWithFormat:@"http://maps.google.com/maps/api/staticmap?center=%@,%@&markers=size:large|color:red|%@,%@&zoom=12&size=400x400&sensor=false",latitudeString,longitudeString,latitudeString,longitudeString];
            [delegate.socket emit:@"sendMessage" with:@[@{@"from": delegate.strID,@"to": chatId,@"content": strURL,@"contentType":typeString,@"messageId": strMsgId,@"time":strMilliSeconds, @"caption":@"", @"latitude":latitudeString, @"longitude":longitudeString ,@"chatRoomType":chatroomType,@"groupId":@""}]];
        }
        else if([typeString isEqualToString:@"contact"])
        {
            [delegate.socket emit:@"sendMessage" with:@[@{@"from": delegate.strID,@"to": chatId,@"content": @"Contact",@"contentType":typeString,@"messageId": strMsgId,@"time":strMilliSeconds, @"caption":@"", @"contactName":ContactNameString, @"contactNumber":ContactNumberString ,@"chatRoomType":chatroomType,@"groupId":@""}]];

        }
        
        else{
        [delegate.socket emit:@"sendMessage" with:@[@{@"from": delegate.strID,@"to": chatId,@"content": trimmedString,@"contentType":typeString,@"messageId": strMsgId,@"time":strMilliSeconds,@"chatRoomType":chatroomType,@"groupId":@""}]];
        }
        
        
        [self performSegueWithIdentifier:@"ForwardMsg" sender:self];
    }
}
 */
-(long long)convertDateToMilliseconds:(NSDate *)date
{
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    long long milliseconds=timeInSeconds*1000;
    return milliseconds;
}
-(NSDate *)convertMillisecondsToDate:(long long)milliSeconds
{
    NSDate *dat = [NSDate dateWithTimeIntervalSince1970:(milliSeconds / 1000.0)];
    return dat;
}

-(void)showPopView
{
    UIView *viewForAudioOrVideo=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+self.navigationController.navigationBar.frame.size.height+50)];
    viewForAudioOrVideo.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7f];
    viewForAudioOrVideo.tag=9087650;
    [self.navigationController.view addSubview:viewForAudioOrVideo];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onRemoveViewAudioVideo:)]; // this is the current problem like a lot of people out there...
    [viewForAudioOrVideo addGestureRecognizer:tap];
    
    UIView *viewForWhiteBG=[[UIView alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)-140, (viewForAudioOrVideo.frame.size.height/2)-50, 280, 100)];
    viewForWhiteBG.backgroundColor=[UIColor whiteColor];
    viewForWhiteBG.layer.cornerRadius=3;
    viewForWhiteBG.clipsToBounds=YES;
    [viewForAudioOrVideo addSubview:viewForWhiteBG];
    
    UILabel *InfoLbl=[[UILabel alloc]initWithFrame:CGRectMake(20, 0, viewForWhiteBG.frame.size.width-40, 50)];
    NSString *infoString=[NSString stringWithFormat:@"Do you want to forward this message to %@",getFrwdString];
    [InfoLbl setText:infoString];
    InfoLbl.numberOfLines = 0;
    InfoLbl.textAlignment = NSTextAlignmentCenter;
    [InfoLbl setFont:[UIFont fontWithName:FONT_NORMAL size:14]];
    [viewForWhiteBG addSubview:InfoLbl];
    
    UIButton *cancel=[[UIButton alloc]initWithFrame:CGRectMake(35, 51, 50, 30)];
    [cancel setTitle:@"No" forState:UIControlStateNormal];
    [cancel setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    cancel.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:14];
    [cancel addTarget:self action:@selector(ClickOnCancel:) forControlEvents:UIControlEventTouchUpInside];
    [viewForWhiteBG addSubview:cancel];
    
    UIButton *ok=[[UIButton alloc]initWithFrame:CGRectMake(215, 51, 50, 30)];
    [ok setTitle:@"Yes" forState:UIControlStateNormal];
    [ok setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    ok.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:14];
    [ok addTarget:self action:@selector(clickOnOk:) forControlEvents:UIControlEventTouchUpInside];
    [viewForWhiteBG addSubview:ok];
}
- (IBAction)ClickOnCancel:(id)sender {
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    [viewForAudioOrVideo removeFromSuperview];
}

- (IBAction)clickOnOk:(id)sender {
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    [viewForAudioOrVideo removeFromSuperview];
    BOOL success=NO;
    
    NSString *chatId=[dictToSendSingleChat valueForKey:@"chatRoomId"];
    NSString *chatroomType=[dictToSendSingleChat valueForKey:@"chatRoomType"];
    NSString *strMsgId = [[NSUUID UUID] UUIDString]; //id
    long long milliSeconds=[self convertDateToMilliseconds:[NSDate date]];
    NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
    NSLog(@"%@",strMilliSeconds);
    NSString *trimmedString = [textString stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    trimmedString = [trimmedString stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    isAvailinChatsTable=[db_class doesChatExist:chatId];
    if (isAvailinChatsTable==NO)
    {
        success=[db_class insertChats:@"" chatRoomId:chatId chatRoomType:chatroomType sender:@"0" lastMessage:trimmedString lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:typeString unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:delegate.strID isDelete:@"0" isLock:@"0" password:@""];
        if (success==YES)
        {
            isAvailinChatsTable=YES;
        }
    }
    else
    {
        success=[db_class updateLastMessage:chatId sender:@"0" lastMessage:trimmedString lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:typeString unReadCount:@"0"];
        NSLog(@"success");
    }

    
    if (![trimmedString isEqualToString:@""])
    {
        
        if ([typeString isEqualToString:@"location"])
        {
            NSString *strURL=[NSString stringWithFormat:@"http://maps.google.com/maps/api/staticmap?center=%@,%@&markers=size:large|color:red|%@,%@&zoom=12&size=400x400&sensor=false",latitudeString,longitudeString,latitudeString,longitudeString];
            
                                       success=[db_class insertChatMessages:strMsgId userId:chatId groupId:chatId chatRoomType:chatroomType content:trimmedString contentType:@"location" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:captionString isDownloaded:@"0" latitude:latitudeString longitude:longitudeString contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                    
                    
                    [delegate.socket emit:@"sendMessage" with:@[@{@"from": delegate.strID,@"to": chatId,@"content": strURL,@"contentType":typeString,@"messageId": strMsgId,@"time":strMilliSeconds, @"caption":captionString, @"latitude":latitudeString, @"longitude":longitudeString ,@"chatRoomType":chatroomType,@"groupId":@""}]];
                    delegate.isFrmLocationPage=@"yes";
                    [self performSegueWithIdentifier:@"ForwardMsg" sender:self];

            
            
        }
        else if([typeString isEqualToString:@"contact"])
        {
            
            success=[db_class insertChatMessages:strMsgId userId:chatId groupId:chatId chatRoomType:chatroomType content:textString contentType:@"contact" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:ContactNameString contactNumber:ContactNumberString checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
            
            
            [delegate.socket emit:@"sendMessage" with:@[@{@"from": delegate.strID,@"to": chatId,@"content": textString,@"contentType":typeString,@"messageId": strMsgId,@"time":strMilliSeconds, @"caption":@"", @"contactName":ContactNameString, @"contactNumber":ContactNumberString ,@"chatRoomType":chatroomType,@"groupId":@""}]];
            delegate.isFrmLocationPage=@"yes";
            [self performSegueWithIdentifier:@"ForwardMsg" sender:self];

            
        }
        else if([typeString isEqualToString:@"sticker"])
        {
                        success=[db_class insertChatMessages:strMsgId userId:chatId groupId:chatId chatRoomType:chatroomType content:trimmedString contentType:@"sticker" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:ContactNameString contactNumber:ContactNumberString checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
            
            
            [delegate.socket emit:@"sendMessage" with:@[@{@"from": delegate.strID,@"to": chatId,@"content": trimmedString,@"contentType":typeString,@"messageId": strMsgId,@"time":strMilliSeconds, @"caption":@"", @"contactName":@"", @"contactNumber":@"" ,@"chatRoomType":chatroomType,@"groupId":@""}]];
            delegate.isFrmLocationPage=@"yes";
            [self performSegueWithIdentifier:@"ForwardMsg" sender:self];
 
            
        }
        
        else{
            
            success=[db_class insertChatMessages:strMsgId userId:chatId groupId:chatId chatRoomType:chatroomType content:trimmedString contentType:typeString contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:getPreviewVal linkTitle:getLinkTitle linkLogo:getLinkLogo linkDescription:getLinkDes];
            
            
            
            [delegate.socket emit:@"sendMessage" with:@[@{@"from": delegate.strID,@"to": chatId,@"content": trimmedString,@"contentType":typeString,@"messageId": strMsgId,@"time":strMilliSeconds,@"chatRoomType":chatroomType,@"groupId":@""}]];
            [self performSegueWithIdentifier:@"ForwardMsg" sender:self];

        }
        
        
    }
    
    
}
- (UIImage *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth) {
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    return [UIImage imageWithData:imageData];
}
- (void) onRemoveViewAudioVideo: (UIGestureRecognizer *) gesture
{
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    [viewForAudioOrVideo removeFromSuperview];
}

@end
