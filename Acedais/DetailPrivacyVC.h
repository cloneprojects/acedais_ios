//
//  DetailPrivacyVC.h
//  ZoeChat
//
//  Created by macmini on 10/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailPrivacyVC : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *detailPrivacyTblView;
@property (strong, nonatomic)NSString *titleStr;
@property (strong, nonatomic)NSIndexPath *LastseenIndexPath;
@property (strong, nonatomic)NSIndexPath *ProfilePhotoIndexPath;
@property (strong, nonatomic)NSIndexPath *AboutIndexPath;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLbl;

@end
