//
//  ToShowLocation.m
//  ZoeChat
//
//  Created by Pyramidions Solution on 11/04/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "ToShowLocation.h"

@interface ToShowLocation ()
{
    NSString *strCurrLat, *strCurrLong;
    GMSMarker *marker;
}
@end

@implementation ToShowLocation
@synthesize locationManager, strLat, strLong, strName, isShowDistance;

- (void)viewDidLoad {
    [super viewDidLoad];
   
    // Do any additional setup after loading the view.
    UIBarButtonItem *barBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"BackButton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onBack)];
    [self.navigationItem setLeftBarButtonItem:barBtn ];
    _GoogleMapView.delegate=self;
    _GoogleMapView.myLocationEnabled = YES;
   // _GoogleMapView.settings.myLocationButton=YES;
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[strLat doubleValue]
                                                            longitude:[strLong doubleValue]
                                                                 zoom:13];
    [_GoogleMapView animateToCameraPosition:camera];
    
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake([strLat doubleValue], [strLong doubleValue]);
    marker = [GMSMarker markerWithPosition:position];
    marker.title = strName;
    marker.map = _GoogleMapView;
    
    [_GoogleMapView setSelectedMarker:marker];

    if ([isShowDistance isEqualToString:@"yes"])
    {
         [self start_to_detect_location:nil];
    }
    else
    {
        marker.title=[NSString stringWithFormat:@"%@ (you)",strName];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)onBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark -
#pragma mark - Current Location

-(void)start_to_detect_location:(id)sender
{
    
    
    locationManager                   = [[CLLocationManager alloc] init];
    locationManager.delegate          =   self;
    locationManager.desiredAccuracy   =   kCLLocationAccuracyBest;
    
    if([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
    {
        [locationManager requestAlwaysAuthorization];
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if(status == kCLAuthorizationStatusRestricted || status == kCLAuthorizationStatusDenied)
    {
        NSLog(@"authentication failure");
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    
    
    [locationManager stopUpdatingLocation];
    self.locationManager = nil;
    NSLog(@"didFailWithError: %@", error);
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    strCurrLat=[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
    strCurrLong=[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];;
    [locationManager stopUpdatingLocation];
    self.locationManager = nil;
    
    CLLocation *userloc = [[CLLocation alloc]initWithLatitude:[strCurrLat doubleValue] longitude:[strCurrLong doubleValue]];
    CLLocation *dest = [[CLLocation alloc]initWithLatitude:[strLat doubleValue] longitude:[strLong doubleValue]];
    
    CLLocationDistance dist = [userloc distanceFromLocation:dest]/1000;
    
    //NSLog(@"%f",dist);
    NSString *distance = [NSString stringWithFormat:@"%.01f KM away",dist];
    
    marker.snippet=distance;

}


@end
