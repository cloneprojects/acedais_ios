//
//  ToShowLocation.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 11/04/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>

@interface ToShowLocation : UIViewController<GMSMapViewDelegate, CLLocationManagerDelegate>
@property(nonatomic,strong)CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet GMSMapView *GoogleMapView;

@property(strong, nonatomic)NSString *strLat;
@property(strong, nonatomic)NSString *strLong;
@property(strong, nonatomic)NSString *strName, *isShowDistance;
@end
