//
//  CallVC.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 20/04/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CallVC : UIViewController<UITextFieldDelegate, UITabBarControllerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableCalls;
@end
