//
//  PrivacySettingVC.h
//  ZoeChat
//
//  Created by macmini on 10/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatVC : UIViewController<UITextFieldDelegate, UITabBarControllerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableChat;
@property (strong, nonatomic) IBOutlet UIView *PasswordView;
@property (strong, nonatomic) IBOutlet UITextField *passwordTxtFld;
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
- (IBAction)clickOnSubmit:(id)sender;
- (IBAction)clickOnForgotPassword:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *UnlockPasswordView;
@property (strong, nonatomic) IBOutlet UITextField *unlockPasswordTxt;
@property (strong, nonatomic) IBOutlet UITextField *UnlockConPasswordTxt;
@property (strong, nonatomic) IBOutlet UIButton *UnlockSubmitBtn;
- (IBAction)ClickOnUnlockSubmit:(id)sender;

@end
