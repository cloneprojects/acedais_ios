//
//  InviteFriendsVC.h
//  ZoeChat
//
//  Created by macmini on 03/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteFriendsVC : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *InviteFriendsTblView;
- (IBAction)clickonBackBtn:(id)sender;

@end
