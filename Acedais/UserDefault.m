

#import "UserDefault.h"

@implementation UserDefault

+ (void)saveBOOLValue:(BOOL)value forKey:(NSString*)key {
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:value] forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

+ (BOOL)retriveBOOLValueFroKey:(NSString*)key {
    
    return [[[NSUserDefaults standardUserDefaults] objectForKey:key] boolValue];
    
}

+ (void)saveIntValue:(int)value forKey:(NSString*)key {
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:value] forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

+ (int)retriveIntValueFroKey:(NSString*)key
{    
    return [[[NSUserDefaults standardUserDefaults] objectForKey:key] intValue];
}


+ (void)saveDoubleValue:(double)value forKey:(NSString*)key {
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithDouble:value] forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

+ (int)retriveDoubleValueFroKey:(NSString*)key {
    
    return [[[NSUserDefaults standardUserDefaults] objectForKey:key] doubleValue];
    
}



+ (void)saveObject:(id)obj forKey:(NSString*)key {
    
    [[NSUserDefaults standardUserDefaults] setObject:obj forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

+ (void)removeObjectforKey:(NSString*)key {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

+ (id)retriveObjectForKey:(NSString*)key {
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
    
}

@end

