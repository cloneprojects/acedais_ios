//
//  ChatCell.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 10/01/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ChatImage;
@property (weak, nonatomic) IBOutlet UILabel *ChatName;
@property (weak, nonatomic) IBOutlet UILabel *ChatLastMessage;
@property (weak, nonatomic) IBOutlet UILabel *ChatTime;
@property (weak, nonatomic) IBOutlet UILabel *ChatUnreadCount;
@property (weak, nonatomic) IBOutlet UIImageView *ImgStatusIcon;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwForMedia;
@property (strong, nonatomic) IBOutlet UIImageView *ChatlockImg;
@property (weak, nonatomic) IBOutlet UILabel *ChatRoomId;
@property (strong, nonatomic) IBOutlet UILabel *lblImg;
@property (weak, nonatomic) IBOutlet UIImageView *pinChat;


@end
