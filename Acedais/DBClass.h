//
//  DBClass.h
//  DownTown
//
//  Created by VinothV on 6/30/15.
//  Copyright (c) 2015 WePOP AR Research Lab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractRepository.h"


@interface DBClass : AbstractRepository

#pragma mark -
#pragma mark - Create Table
-(BOOL)createUserTable;
-(BOOL)createChatsTable;
-(BOOL)createChatMessagesTable;
-(BOOL)createCallsTable;
-(BOOL)createGroupParticipantsTable;
-(BOOL)createLinkPreviewTable;
-(NSString*)getRowCount;


#pragma mark -
#pragma mark - User Table (Insert Update Delete Get)

-(BOOL)insertUser:(NSString*)mobile name:(NSString*)name registeredName:(NSString*)registeredName image:(NSString*)image status:(NSString*)status showInContactsPage:(NSString*)showInContactsPage zoeChatId:(NSString*)zoeChatId;

-(BOOL)updateUser:(NSString*)mobile registeredName:(NSString*)registeredName image:(NSString*)image status:(NSString*)status showInContactsPage:(NSString*)showInContactsPage zoeChatId:(NSString*)zoeChatId;
-(BOOL)updateUserImage:(NSString*)zoeChatId image:(NSString*)image;
-(BOOL)updateUserStatus:(NSString*)zoeChatId status:(NSString*)status;
-(BOOL)updateUserName:(NSString*)mobile name:(NSString*)name;
-(BOOL)doesUserExist:(NSString*)mobile;

-(BOOL)deleteUser:(NSString*)mobile;

-(NSDictionary *)getUserName:(NSString*)mobile;
-(NSDictionary *)getUserNameForNotification:(NSString*)mobile;
-(NSDictionary *)getUserImage:(NSString*)mobile;
-(NSDictionary *)getUserInfo:(NSString*)mobile;
-(NSArray *)getUsers;


#pragma mark -
#pragma mark - Chats Table Insert (Update Delete Get)

-(BOOL)insertChats:(NSString*)id chatRoomId:(NSString*)chatRoomId chatRoomType:(NSString*)chatRoomType sender:(NSString*)sender lastMessage:(NSString*)lastMessage lastMessageStatus:(NSString*)lastMessageStatus lastMessagesTime:(NSString*)lastMessagesTime lastMessagesType:(NSString*)lastMessagesType unReadCount:(NSString*)unReadCount groupName:(NSString*)groupName groupImage:(NSString*)groupImage sentBy:(NSString*)sentBy isDelete:(NSString*)isDelete isLock:(NSString*)isLock password:(NSString*)password;

-(BOOL)updateLastMessage:(NSString*)id sender:(NSString*)sender lastMessage:(NSString*)lastMessage lastMessageStatus:(NSString*)lastMessageStatus lastMessageTime:(NSString*)lastMessageTime lastMessagesType:(NSString*)lastMessagesType unReadCount:(NSString*)unReadCount;
-(NSMutableArray *) getChats;
-(NSDictionary *) getOneChat:(NSString*)chatRoomId;

-(BOOL)doesChatExist:(NSString*)chatRoomId;
-(BOOL)updateLastMsgStatus:(NSString*)id lastMsgStatus:(NSString*)lastMsgStatus;
-(NSDictionary *)getSingleChat:(NSString*)id;
-(BOOL)updateUnreadCount:(NSString*)id unreadCount:(NSString*)unreadCount;
-(BOOL)updateGroupName:(NSString*)id groupName:(NSString*)groupName;
-(int) getTotalUnreadCount;
-(BOOL)deleteChat:(NSString*)chatRoomId;
//UpdateGrpImg
-(BOOL)updateGroupImage:(NSString*)id groupImage:(NSString*)groupImage;
// update delete chatTable
-(BOOL)updatedeleteChatTable:(NSString*)chatRoomId isDelete:(NSString*)isDelete;
//delete groupmsg
-(BOOL)deleteGroupMessages:(NSString*)grpId;
//update lock
-(BOOL)updatePasswordChatTable:(NSString*)chatRoomId  password:(NSString*)password;
-(BOOL)updateLockChatTable:(NSString *)chatRoomId isLock:(NSString *)isLock;



#pragma mark -
#pragma mark - Chat Messages Table (Insert Update Delete Get)
-(BOOL)insertChatMessages:(NSString*)id userId:(NSString*)userId groupId:(NSString*)groupId chatRoomType:(NSString*)chatRoomType content:(NSString*)content contentType:(NSString*)contentType contentStatus:(NSString*)contentStatus sender:(NSString*)sender sentTime:(NSString*)sentTime deliveredTime:(NSString*)deliveredTime seenTime:(NSString*)seenTime caption:(NSString*)caption isDownloaded:(NSString*)isDownloaded latitude:(NSString*)latitude longitude:(NSString*)longitude contactName:(NSString*)contactName contactNumber:(NSString*)contactNumber checkStar:(NSString*)checkStar showPreview:(NSString*)showPreview linkTitle:(NSString*)linkTitle linkLogo:(NSString*)linkLogo linkDescription:(NSString*)linkDescription;


-(BOOL)updateDeliveredTime:(NSString*)id deliveredTime:(NSString*)deliveredTime;
-(BOOL)updateSeenTime:(NSString*)id seenTime:(NSString*)seenTime;
-(BOOL)updateContentStatus:(NSString*)id contentStatus:(NSString*)contentStatus;
-(BOOL)updateIsDownloaded:(NSString*)id isDownloaded:(NSString*)isDownloaded;
-(BOOL)updateStarredMessage:(NSString*)userId checkStar:(NSString*)checkStar;
-(BOOL)updateGroupStarredMessage:(NSString*)groupId checkStar:(NSString*)checkStar;



-(NSMutableArray *) getChatMessages;
-(NSMutableArray *) getSendingChatMessages;
-(NSDictionary *)getChatMessage:(NSString*)id;
-(NSArray *)getOneToOneChatMessages:(NSString*)userId;
-(NSArray *)getGroupChatMessagesForMedia:(NSString*)userId;
-(NSArray *)getOneToOneChatMessagesForMedia:(NSString*)userId;
-(BOOL)deleteChatMessages:(NSString*)userId;
-(BOOL)deleteMessages:(NSString*)msgId;
-(NSArray *)getGroupMessages:(NSString*)groupId;

//ExitGroup

-(BOOL)ExitChatTblGroup:(NSString*)grpId;
-(BOOL)ExitChatTblMsgGroup:(NSString*)grpId;
//Pagination
-(NSMutableArray *)getChatMessages:(NSString*)userId limitVal:(NSString*)limitVal;
-(NSMutableArray *)getGroupMessages:(NSString*)groupId limitVal:(NSString*)limitVal;




#pragma mark -
#pragma mark - Calls Table Insert (Update Delete Get)

-(BOOL)insertCalls:(NSString*)id zoeChatId:(NSString*)zoeChatId callTime:(NSString*)callTime callType:(NSString*)callType callLog:(NSString*)callLog callDuration:(NSString*)callDuration;

-(NSMutableArray *) getCalls;
-(BOOL)deleteAllCalls;
-(BOOL)deleteCall:(NSString*)zoeChatId;
-(NSMutableArray *)getcall:(NSString*)zoeChatId;


#pragma mark -
#pragma mark - Group Participants Table (Insert Update Delete Get)

-(BOOL)insertParticipants:(NSString*)id userId:(NSString*)userId groupId:(NSString*)groupId joinedAt:(NSString*)joinedAt addedBy:(NSString*)addedBy isAdmin:(NSString*)isAdmin ;
-(NSArray *)getParticipants:(NSString*)groupId;
-(BOOL)doesGroupExist:(NSString*)groupId;
-(NSDictionary *)getSingleGroup:(NSString*)groupId;
-(BOOL)deleteParticipant:(NSString*)userId;
-(BOOL)deleteParticipant:(NSString*)userId groupId:(NSString*)groupId;
//Update GroupAdmin
-(BOOL)updateAdminParticipant:(NSString*)isAdmin userId:(NSString*)userId;
-(BOOL)deleteGroupParticipant:(NSString*)groupId;
-(NSString *)participant:(NSString*)groupId;


-(BOOL)updatePinChatTable:(NSString *)chatRoomId isPin:(NSString *)isPin;



#pragma mark -
#pragma mark - Link Preview Table (Insert Update Delete Get)

-(BOOL)insertLinks:(NSString*)title description:(NSString*)description logo:(NSString*)logo url:(NSString*)url ;
-(NSMutableArray *) getLinkurls;


@end
