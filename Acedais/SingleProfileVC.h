//
//  SingleProfileVC.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 27/03/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleProfileVC : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (strong, nonatomic) IBOutlet UIButton *editImgBtn;
@property (weak, nonatomic) IBOutlet UILabel *mediaLbl;
@property (strong, nonatomic) IBOutlet UILabel *Initiallbl;

@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblStatusAndPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblMobileNo;
@property (weak, nonatomic) IBOutlet UIView *ViewForMedia;
@property(strong, nonatomic)NSDictionary *dictDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblNoMediaFiles;
@property (weak, nonatomic) IBOutlet UILabel *lblSmallMobile;

@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UITextField *txtNewSubject;
@property (weak, nonatomic) IBOutlet UIView *viewEnterNewSubject;
@property (weak, nonatomic) IBOutlet UIButton *btncancel;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;
- (IBAction)onCancel:(id)sender;
- (IBAction)onOk:(id)sender;
- (IBAction)ClickOnEditImg:(id)sender;


@end
