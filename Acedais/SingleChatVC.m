
//
//  SingleChatVC.m
//  ZoeChat
//
//  Created by Pyramidions Solution on 10/01/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "SingleChatVC.h"
#import "ChatCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIButton+WebCache.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "MessageCell.h"
#import "TableArray.h"
#import "DBClass.h"
#import "Inputbar.h"
#import "DAKeyboardControl.h"
#import <QBImagePickerController/QBImagePickerController.h>
#import "MBProgressHUD.h"
#import "AWSS3TransferUtility.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "MWPhotoBrowser.h"
#import "DGActivityIndicatorView.h"
#import <Photos/Photos.h>
#import "CustomAlbum.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "SingleProfileVC.h"
#import "CBAutoScrollLabel.h"
#import "VoiceCallVC.h"
#import "VideoCallVC.h"
#import "ToShareLocationVC.h"
#import "ToShowLocation.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import "ShowAllContactsVC.h"
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import <MessageUI/MessageUI.h>
#import "LoadDocumentVC.h"
#import "ForwardMsgVC.h"
#import "DoodleVC.h"
#import "MTDURLPreview.h"
#import "TFHpple.h"
#import "HPGrowingTextView.h"
#import "NSData+Blocks.h"
#import "MainTabBarVC.h"
#import "AFNHelper.h"


@interface SingleChatVC ()<InputbarDelegate, QBImagePickerControllerDelegate, UIToolbarDelegate, UIGestureRecognizerDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, MWPhotoBrowserDelegate, MFMessageComposeViewControllerDelegate, CNContactViewControllerDelegate, AVAudioPlayerDelegate,STKStickerControllerDelegate,UITextViewDelegate,HPGrowingTextViewDelegate,UITextFieldDelegate>
{
    UIView *ViewImgName;
    float keyBoardHeight;
    UITextField *currentTextView;
    UITextView *currentTextView1;
    bool keyboardIsShown;
    UITapGestureRecognizer *tapGesture;
    UIView *viewAttachments;
    UITapGestureRecognizer *tapviewAttachments;
    AppDelegate *appDelegate;
    NSString *strZoeChatId;
    BOOL isAvailinChatsTable;
    DBClass *db_class;
    NSDateFormatter *dateFormatter;
    NSMutableArray *arrWholetableArray;
    AWSS3TransferUtilityUploadTask *uploadTask;
    AWSS3TransferUtilityDownloadTask *downloadTask;
    NSString *strCurrentlyUploading, *strCurrentlyDownloading;
    NSMutableArray *arrPhotos;
    NSTimer *timerGetOnlineStatus;
    NSString *strLat, *strLong;
    NSString *strName, *strNameToSend, *isShowDistance, *strChatRoomType, *strPlayingTag;
    AVAudioPlayer *myAudioPlayer;
    NSIndexPath *loadIndexathss;
    int rowNO;
    NSIndexPath *lastIndexPth;
    NSTimer *sliderTime;
    NSTimer *Timer;
    BOOL isPlaying;
    NSMutableArray *getSingleSentTime;
    NSURL *fileUrl1;
    NSString *thumbImg;
    //delete
    NSIndexPath *deleteIndexPth;
    //pagination
    NSMutableArray *dataArray;
    NSMutableArray *nextArray;
    NSString *rowString;
    BOOL isPageRefreshing;
    int page;
    int getNext;
    NSString *setLimit;
    NSString *isFirst;
    BOOL isScrolling;
    NSMutableArray *paginationArray;
    BOOL isStartTyping;
    NSString *getchatRoomType;
    //Preview
    BOOL isPreview;
    CGFloat keyboardHeight;


    
 }
@property (weak, nonatomic) IBOutlet Inputbar *inputbar;
@property (strong, nonatomic) TableArray *tableArray;
@property (atomic, assign) BOOL canceled;




//Sticker
@property (strong, nonatomic) STKStickerController *stickerController;
@property (nonatomic, weak) IBOutlet UITextView* inputTextView;



@end

@implementation SingleChatVC
@synthesize dictDetails;


- (void)viewDidLoad {

    
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
    isStartTyping=YES;
    isPreview=YES;
    appDelegate.isFrmLocationPage=@"no";
    db_class=[[DBClass alloc]init];
    strCurrentlyUploading=@"";
    strCurrentlyDownloading=@"";
    self.linkPreviewView.hidden=YES;
    self.linkPreviewView.layer.borderWidth = 1;
    
    self.linkPreviewView.layer.borderColor = [UIColor grayColor].CGColor;
    strPlayingTag=@"";
    setLimit=@"0";
    page=10;
    self.addContactView.hidden=YES;

    arrWholetableArray=[[NSMutableArray alloc]init];
     dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy hh:mm a"];
    paginationArray = [[NSMutableArray alloc]init];
    UIImageView *bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgimg.png"]];
    [_tableSingleChat setBackgroundView:bg];
    
    ViewImgName=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 150, self.navigationController.navigationBar.frame.size.height)];
    ViewImgName.backgroundColor=[UIColor clearColor];
    self.navigationItem.titleView=ViewImgName;
   
    
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
     [self setFontFamily:FONT_NORMAL forView:self.view andSubViews:YES];
    
    UIButton *btnImg=[[UIButton alloc]initWithFrame:CGRectMake(0, (self.navigationController.navigationBar.frame.size.height/2)-17.5, 35, 35)];
   btnImg.layer.cornerRadius=btnImg.frame.size.height/2;
   btnImg.clipsToBounds=YES;
    [ViewImgName addSubview:btnImg];
    
    NSLog(@"Details**** :%@",dictDetails);
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:[dictDetails valueForKey:@"chatRoomId"] forKey:@"groupId"];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_GET_PARTICIPANTS withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if (response)
         {
             NSLog(@"Response: %@",response);
             if ([[response valueForKey:@"error"] boolValue]==false)
             {
                 NSDictionary *dictGroupDetails=[response valueForKey:@"groupDetails"];
                 NSString *strCreatedBy=[NSString stringWithFormat:@"%@",[dictGroupDetails valueForKey:@"createdBy"]];
                 NSDictionary *dictName=[db_class getUserNameForNotification:strCreatedBy];
                 
                 
                 
                 
                 
                 
             }
             
             
         }
         else {
             NSLog(@"Main Issue");
         }
         
     }];

    strZoeChatId=[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"zoechatid"]];
    strChatRoomType=[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"chatRoomType"]];
    
    
    //callbtn hide for groupchat
    self.inputTextView.tintColor=[UIColor clearColor];

    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    
    if (color != nil)
    {
        self.inputbar.tintColor=color;
        if ([strChatRoomType isEqualToString:@"1"])
        {
            self.callBtn.enabled = NO;
            self.callBtn.tintColor = color;
            self.callBtn.accessibilityElementsHidden = YES;
        }
        else if ([strChatRoomType isEqualToString:@"2"])
        {
            self.callBtn.enabled = NO;
            self.callBtn.tintColor = color;
            self.callBtn.accessibilityElementsHidden = YES;
        }

        else{
            self.callBtn.tintColor = [UIColor whiteColor];

        }
        
    }
    else
    {
        self.inputbar.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];

        if ([strChatRoomType isEqualToString:@"1"])
        {
            self.callBtn.enabled = NO;
            self.callBtn.tintColor = [UIColor clearColor];
            
        }
        else if ([strChatRoomType isEqualToString:@"2"])
        {
            self.callBtn.enabled = NO;
            self.callBtn.tintColor = [UIColor clearColor];
        }

        else{
            self.callBtn.tintColor = [UIColor whiteColor];
            
        }
        
        
    }
    
    
    
    
    
    
    

    if ([strZoeChatId isEqualToString:@"(null)"])
    {
         strZoeChatId=[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"chatRoomId"]];
        NSLog(@"%@",strZoeChatId);

    }
    isAvailinChatsTable=NO;
    isAvailinChatsTable=[db_class doesChatExist:strZoeChatId];
    
        strName= [[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"name"]]capitalizedString];
    UILabel  *lblName=[[UILabel alloc]initWithFrame:CGRectMake(45, (self.navigationController.navigationBar.frame.size.height/2)-10, ViewImgName.frame.size.width-50, 20)];
    lblName.tag=110010;
    if (![strName isEqualToString:@"(Null)"])
    {
        lblName.text=strName;
    }
    else
    {
        lblName.text=[NSString stringWithFormat:@"+%@",[dictDetails valueForKey:@"chatRoomId"]];
        strName=[NSString stringWithFormat:@"+%@",[dictDetails valueForKey:@"chatRoomId"]];
        self.addContactView.hidden=NO;
        [self.tableSingleChat bringSubviewToFront:self.addContactView];


    }
    
    if ([[dictDetails valueForKey:@"chatRoomType"] isEqualToString:@"1"])
    {
       lblName.text=[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"groupName"]];
        self.addContactView.hidden=YES;
        NSString *strImage=[dictDetails valueForKey:@"groupImage"];
        NSURL *imageURL = [NSURL URLWithString:strImage];
        [btnImg setImage:[UIImage imageNamed:@"grp.png"] forState:UIControlStateNormal];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:imageURL
                              options:SDWebImageRefreshCached
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    // do something with image
                                    [btnImg setImage:image forState:UIControlStateNormal];
                                    
                                }
                            }];
        
    }
    else if ([[dictDetails valueForKey:@"chatRoomType"] isEqualToString:@"2"])
    {
        lblName.text=[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"groupName"]];
        self.addContactView.hidden=YES;

        NSString *strImage=[dictDetails valueForKey:@"groupImage"];
        NSURL *imageURL = [NSURL URLWithString:strImage];
        [btnImg setImage:[UIImage imageNamed:@"megaphone.png"] forState:UIControlStateNormal];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:imageURL
                              options:SDWebImageRefreshCached
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    // do something with image
                                    [btnImg setImage:image forState:UIControlStateNormal];
                                    
                                }
                            }];
        
    }

    else
    {
        NSString *strImage=[dictDetails valueForKey:@"image"];
        NSURL *imageURL = [NSURL URLWithString:strImage];
        //lblImg
        NSString *InitialStr;
        UIColor *bgColor;
        if (![strName isEqualToString:@"(Null)"])
        {
            strName=[strName capitalizedString];
        InitialStr=[strName substringToIndex:1];
        if ([InitialStr isEqualToString:@"A"])
        {
            bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"B"])
        {
            bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"C"])
        {
            bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"D"])
        {
            bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"E"])
        {
            bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"F"])
        {
            bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"G"])
        {
            bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"H"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"I"])
        {
            bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"J"])
        {
            bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"K"])
        {
            bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"L"])
        {
            bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"M"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"N"])
        {
            bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"O"])
        {
            bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"P"])
        {
            bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"Q"])
        {
            bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"R"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"S"])
        {
            bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"T"])
        {
            bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"U"])
        {
            bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"V"])
        {
            bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"W"])
        {
            bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
        }else if ([InitialStr isEqualToString:@"X"])
        {
            bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"Y"])
        {
            bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"Z"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
        }
        
    }
    else
    {
        strName=[NSString stringWithFormat:@"+%@",[dictDetails valueForKey:@"chatRoomId"]];
       
        int index = 2;
        NSString *theCharacter = [NSString stringWithFormat:@"%c", [strName characterAtIndex:index-1]];
        InitialStr=theCharacter;
        if ([InitialStr isEqualToString:@"0"])
        {
            bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"1"])
        {
            bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"2"])
        {
            bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"3"])
        {
            bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"4"])
        {
            bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"5"])
        {
            bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"6"])
        {
            bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"7"])
        {
            bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"8"])
        {
            bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
        }
        else if ([InitialStr isEqualToString:@"9"])
        {
            bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
        }
        
    }

     
        [btnImg setImage:[UIImage imageNamed:@"clear.png"] forState:UIControlStateNormal];
//        [btnImg setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [btnImg setTitle:InitialStr forState:UIControlStateNormal];
//        btnImg.backgroundColor=bgColor;
        
        
        UILabel *InitialLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, (self.navigationController.navigationBar.frame.size.height/2)-17.5, 35, 35)];
        InitialLbl.layer.cornerRadius=btnImg.frame.size.height/2;
        InitialLbl.clipsToBounds=YES;
        InitialLbl.text=InitialStr;
        InitialLbl.textColor=[UIColor whiteColor];
        InitialLbl.textAlignment=NSTextAlignmentCenter;
        InitialLbl.backgroundColor=bgColor;
        [ViewImgName addSubview:InitialLbl];
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:imageURL
                              options:SDWebImageRefreshCached
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    // do something with image
                                    [btnImg setImage:image forState:UIControlStateNormal];
                                    [InitialLbl setHidden:YES];
                                    
                                }
                            }];
    
    }

    lblName.font=[UIFont fontWithName:FONT_NORMAL size:16];
    lblName.textColor=[UIColor whiteColor];
    [ViewImgName addSubview:lblName];

    CBAutoScrollLabel  *lblStatus=[[CBAutoScrollLabel alloc]initWithFrame:CGRectMake(45, (self.navigationController.navigationBar.frame.size.height/2)+2, ViewImgName.frame.size.width-50, 20)];
    lblStatus.tag=110011;
    lblStatus.font=[UIFont fontWithName:FONT_NORMAL size:12];
    lblStatus.hidden=YES;
    lblStatus.pauseInterval = 3.f;
    lblStatus.textColor = [UIColor whiteColor];
    [lblStatus observeApplicationNotifications];
    [ViewImgName addSubview:lblStatus];
    
    UIButton *btnSingleProfile=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, ViewImgName.frame.size.width, ViewImgName.frame.size.height)];
    btnSingleProfile.backgroundColor=[UIColor clearColor];
    if ([[dictDetails valueForKey:@"chatRoomId"] isEqualToString:@"919566025629"])
    {
        
    }
    else{
        [btnSingleProfile  addTarget:self action:@selector(onSingleProfile:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [ViewImgName addSubview:btnSingleProfile];
    
    [self setInputbar];
    [self getChatMessages];
//[self getpaginationData:@"5"];
    [self setTableView];
    self.tableSingleChat.delegate = self;
    self.tableSingleChat.dataSource = self;
   
    //[self.tableSingleChat reloadData];
    dispatch_async(dispatch_get_main_queue(),^{
        NSLog(@" IndexPaths:%ld",(long)loadIndexathss);
        NSLog(@" IndexPaths:%ld",(long)loadIndexathss.row);
        NSLog(@" IndexPaths:%ld",(long)loadIndexathss.section);
        
        // NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        //Basically maintain your logic to get the indexpath
        [self.tableSingleChat scrollToRowAtIndexPath:loadIndexathss
                                    atScrollPosition:UITableViewScrollPositionMiddle
                                            animated:NO];
        
    });

    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.tableSingleChat addGestureRecognizer:singleTap];
    
     [self startUploadingImagesOrVideoOrAudio];
    [self startDownloadImageOrVideo];
    
    if ([[dictDetails valueForKey:@"chatRoomType"] isEqualToString:@"0"])
    {

        [self GetOnlineDetails];
        
        NSString *strListenTyping=[NSString stringWithFormat:@"%@:typing",appDelegate.strID];
        [appDelegate.socket on:strListenTyping callback:^(NSArray *data, SocketAckEmitter *ack)
         {
             NSLog(@"%@",data);
             UILabel *lblStatus=(UILabel *)[self.navigationItem.titleView viewWithTag:110011];
             NSDictionary *DictData=[data objectAtIndex:0];
             NSString *strStatus=[NSString stringWithFormat:@"%@",[DictData valueForKey:@"status"]];
             lblStatus.hidden=NO;
             if ([strStatus isEqualToString:@"start"])
             {
                 lblStatus.text=@"typing...";
             }
             else
             {
                 [appDelegate.socket emit:@"getOnline" with:@[@{@"id": strZoeChatId,@"from":appDelegate.strID}]];
             }
             
             NSLog(@"%@",strStatus);
             
         }];
        
        
        NSString *strOnlineStatus=[NSString stringWithFormat:@"%@:onlineStatus",appDelegate.strID];
        [appDelegate.socket on:strOnlineStatus callback:^(NSArray *data, SocketAckEmitter *ack)
         {
             NSLog(@"%@",data);
//             [appDelegate.socket emit:@"ack" with:@[@{@"userid":appDelegate.strID,@"uniqueid":appDelegate.uniqueID}]];
             UILabel *lblName=(UILabel *)[self.navigationItem.titleView viewWithTag:110010];
             UILabel *lblStatus=(UILabel *)[self.navigationItem.titleView viewWithTag:110011];
             
             [UIView animateWithDuration:0.5
                                   delay:0.1
                                 options: UIViewAnimationCurveEaseIn
                              animations:^{
                                  lblName.frame = CGRectMake(45, 5, ViewImgName.frame.size.width-38, 20);
                              }
                              completion:^(BOOL finished){
                                  NSDictionary *DictData=[data objectAtIndex:0];
                                  NSString *strStatus=[NSString stringWithFormat:@"%@",[DictData valueForKey:@"status"]];
                                  NSString *strLastSeen=[NSString stringWithFormat:@"%@",[DictData valueForKey:@"lastSeen"]];
                                  lblStatus.hidden=NO;
                                  if ([strLastSeen isEqualToString:@""]){
                                  }
                                  else
                                  {
                                  if ([strStatus isEqualToString:@"0"])
                                  {
                                  
                                      NSDate *LastSeendate=[self convertMillisecondsToDate:[[DictData valueForKey:@"lastSeen"]longLongValue]];
                                      
                                      NSCalendar* calendar = [NSCalendar currentCalendar];
                                      BOOL isToday = [calendar isDateInToday:LastSeendate];
                                      BOOL isYesterday = [calendar isDateInYesterday:LastSeendate];
                                      
                                      NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
                                      [formatterDate setDateFormat:@"dd/MM/yyyy"];
                                      NSString *strDate = [formatterDate stringFromDate:LastSeendate];
                                      
                                      NSDateFormatter *dFormatterTime = [[NSDateFormatter alloc] init];
                                      [dFormatterTime setDateFormat:@"hh:mm a"];
                                      NSString *strTime = [dFormatterTime stringFromDate:LastSeendate];
                                      if (isToday)
                                      {
                                          lblStatus.text=[NSString stringWithFormat:@"last seen today at %@",strTime];
                                      }
                                      else if (isYesterday)
                                      {
                                          lblStatus.text=[NSString stringWithFormat:@"last seen yesterday at %@",strTime];
                                      }
                                      else
                                      {
                                          lblStatus.text=[NSString stringWithFormat:@"last seen %@ at %@", strDate,strTime];
                                      }
                                      
                                      
                                  }
                                  else
                                  {
                                      lblStatus.text=@"online";
                                  }
                              }
              
                                  
                                  
                                  
                              }];
         }];

    }
    //DataUsage
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *getPage = [defaults objectForKey:@"isDataPage"];
    if ([getPage isEqualToString:@"YES"])
    {
        
    }
    else
    {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"Wi-fi" forKey:@"photoStatus"];
    [defaults setObject:@"Wi-fi" forKey:@"videoStatus"];
    [defaults setObject:@"Wi-fi" forKey:@"audioStatus"];
    [defaults setObject:@"Wi-fi" forKey:@"documentStatus"];
    [defaults synchronize];
    }
    //Sticker
//    self.inputTextView.layer.cornerRadius = 7.0;
//    self.inputTextView.layer.borderWidth = 1.0;
//    self.inputTextView.layer.borderColor = [UIColor colorWithRed: 0.84 green: 0.84 blue: 0.85 alpha: 1].CGColor;
    singleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(textViewDidTap:)];
//    [self.inputTextView addGestureRecognizer: tapGesture1];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(willHideKeyboard:)
                                                 name: UIKeyboardWillHideNotification
                                               object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(didShowKeyboard:)
                                                 name: UIKeyboardWillShowNotification
                                               object: nil];
    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.tableSingleChat addGestureRecognizer:singleTap];

    self.stickerController = [STKStickerController new];
    self.stickerController.delegate = self;
    self.stickerController.textInputView = self.inputTextView;
    
    self.tableSingleChat.rowHeight = UITableViewAutomaticDimension;
    
    


}
- (void)keyboardWillChangeFrame:(NSNotification *)notification
{
    NSLog(@"%f", [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height);
    keyboardHeight=[notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    NSLog(@"%f", keyBoardHeight);
}

- (void)textViewDidTap: (UITapGestureRecognizer*)gestureRecognizer {
    [self.inputTextView becomeFirstResponder];
}
- (void)didShowKeyboard: (NSNotification*)notification {
    CGRect keyboardBounds = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    UIViewAnimationCurve curve = (UIViewAnimationCurve) [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    CGFloat keyboardHeight = keyboardBounds.size.height;
    
    CGFloat animationDuration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    
    [UIView animateWithDuration: animationDuration animations: ^ {
        [UIView setAnimationCurve: curve];
        [self.view layoutIfNeeded];
    }];
    
}

- (void)willHideKeyboard: (NSNotification*)notification {
    
    CGFloat animationDuration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    
    [UIView animateWithDuration: animationDuration animations: ^ {
        [self.view layoutIfNeeded];
    }];
}

-(void)GetOnlineDetails
{
    [appDelegate.socket emit:@"getOnline" with:@[@{@"id": strZoeChatId,@"from":appDelegate.strID}]];

    [timerGetOnlineStatus invalidate];
    timerGetOnlineStatus=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(GetOnlineDetails) userInfo:nil repeats:NO];
}
-(IBAction)onSingleProfile:(id)sender
{
    [self performSegueWithIdentifier:@"SingChat_SingProfile" sender:self];
}
- (void) incomingNotification:(NSNotification *)notification{
    NSString *strLength = [notification object];
    
    
    if ([[dictDetails valueForKey:@"chatRoomType"] isEqualToString:@"0"])
    {
    if ([strLength intValue]>0)
    {
    if (isStartTyping)
    {
            [appDelegate.socket emit:@"typing" with:@[@{@"from":appDelegate.strID,@"to":strZoeChatId,@"status":@"start"}]];
        isStartTyping=NO;
    }
    else if(isStartTyping == NO)
    {
        
    }
    }
    else
        {
            [appDelegate.socket emit:@"typing" with:@[@{@"from":appDelegate.strID,@"to":strZoeChatId,@"status":@"stop"}]];
            isStartTyping=YES;
        }
    }
    else{
        
    }
    
    
   }
//linkPreview

- (void) LinkincomingNotification:(NSNotification *)notification{
    NSString *strMatch = [notification object];
    
    NSError *error = nil;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                               error:&error];
    
    [detector enumerateMatchesInString:strMatch
                               options:0
                                 range:NSMakeRange(0, strMatch.length)
                            usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
     {
         if (result.resultType == NSTextCheckingTypeLink)
         {
             NSString *str = [NSString stringWithFormat:@"%@",result.URL];
             NSLog(@"%@",str);
                 if([self validateUrl:str])
                 {
                     NSLog(@"URLSSSS");
                     
                     
                     
                     NSURL* url = [NSURL URLWithString:str];
                     
                     
                     
                     NSMutableArray *arrLinks=[db_class getLinkurls];
                     
                     if (arrLinks.count!=0)
                     {
                         for (int i=0; i<arrLinks.count; i++)
                         {
                             NSMutableDictionary *dictSing=[arrLinks objectAtIndex:i];
                             NSLog(@"%@",dictSing);
                             NSString *getDescription=[dictSing valueForKey:@"description"];
                             UIImageView *LogoImg;
                             UILabel *DescriptionLbl;
                             UILabel *TitleLbl;
                             NSString *geturl=[dictSing valueForKey:@"url"];
                             if ([geturl isEqualToString:str])
                             {
                                 
                                 self.linkPreviewView.hidden=NO;
//                                 [self.view bringSubviewToFront:self.linkPreviewView];


                                
                                 if ([[dictSing valueForKey:@"logo"] isEqualToString:@""])
                                 {
                                     
                                     self.LinkTitlelbl.text=[dictSing valueForKey:@"title"];
                                     self.linkDescriptionLbl.text=getDescription;
 
                                 }
                                 else
                                 {
                                     [self.linkLogo sd_setImageWithURL:[NSURL URLWithString:[dictSing valueForKey:@"logo"]]
                                                                                      placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
                                     self.LinkTitlelbl.text=[dictSing valueForKey:@"title"];
                                     self.linkDescriptionLbl.text=getDescription;

                                 }
                             }
                             else
                             {
                                 [NSData dataWithContentsOfURL:url completionBlock:^(NSData *data, NSError *error) {
                                     if(!error) {
                                         dispatch_sync(dispatch_get_main_queue(), ^(void) {
                                             TFHpple *doc = [TFHpple hppleWithHTMLData:data];
                                             NSString* getData = @"//head/meta";
                                             NSArray *Nodes = [doc searchWithXPathQuery:getData];
                                             for (TFHppleElement *element in Nodes)
                                             {
                                                 [self Element:element];
                                             }
                                         });
                                     } else {
                                         NSLog(@"error %@", error);
                                     }
                                 }];

                             }
                         }
                     }

                   }
                 else
                 {
                     NSLog(@"TEXT");
             
                 }
             
//             NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:@"(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))" options:NSRegularExpressionCaseInsensitive error:NULL];
//             NSString *someString = str;
//             NSString *match = [someString substringWithRange:[expression rangeOfFirstMatchInString:someString options:NSMatchingCompleted range:NSMakeRange(0, [someString length])]];
//             NSLog(@"%@", match);
             
             
             
             
//             NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:@"(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))" options:NSRegularExpressionCaseInsensitive error:NULL];
//             NSString *someString = str;
//             
//             NSArray *matches = [expression matchesInString:someString options:NSMatchingCompleted range:NSMakeRange(0, someString.length)];
//             for (NSTextCheckingResult *result in matches) {
//                 NSString *url = [someString substringWithRange:result.range];
//                 NSLog(@"found url:%@", url);
             //}
         }
         else
         {
             self.linkPreviewView.hidden=YES;
         }
     }];
 
}

- (void) ClearView:(NSNotification *)notification{
    
    self.linkPreviewView.hidden=YES;
    
    
}
//Link

- (void)Element:(TFHppleElement *)element
{
    
    NSString *trimmedString = [_inputbar.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    
    [MTDURLPreview loadPreviewWithURL:[NSURL URLWithString:trimmedString] completion:^(MTDURLPreview *preview, NSError *error) {
        
        NSLog(@"%@",preview.title);
        NSLog(@"%@",preview.domain);
        NSLog(@"%@",preview.content);
        NSLog(@"%@",preview.description);
        NSLog(@"%@",preview.imageURL);
               if ([element.attributes[@"name"] isEqualToString:@"description"])
        {
            NSString *getDescription=element.attributes[@"content"];
            NSLog(@"%@",getDescription);
            NSString *imgStr=[preview.imageURL absoluteString];
            
            
            self.linkPreviewView.hidden=NO;

            [self.linkLogo sd_setImageWithURL:[NSURL URLWithString:imgStr]
                             placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
            self.LinkTitlelbl.text=preview.title;
            self.linkDescriptionLbl.text=getDescription;
        }
        
        
        
    }];
    
}




-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    if ([appDelegate.isFrmLocationPage isEqualToString:@"yes"])
    {
        self.tableArray = [[TableArray alloc] init];
        arrWholetableArray=[[NSMutableArray alloc]init];
        [self getChatMessages];
        if ([appDelegate.isDoodle isEqualToString:@"YES"])
        {
            [self startUploadingImagesOrVideoOrAudio];
            appDelegate.isDoodle=@"NO";
        }
        else
        {
            
        }
        appDelegate.isFrmLocationPage=@"no";
    }
    
    //[_tableSingleChat reloadData];
    
    if([self.tableSingleChat numberOfSections]!=0)
    {
        NSInteger numberOfSections = [self.tableArray numberOfSections];
        NSInteger numberOfRows = [self.tableArray numberOfMessagesInSection:numberOfSections-1];
        if (numberOfRows)
        {
            [_tableSingleChat reloadData];
            [_tableSingleChat scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                                    atScrollPosition:UITableViewScrollPositionBottom animated:animated];
        }
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateReceiveMessage:)
                                                 name:@"Refresh_SingleChat_ReceivedMsgs"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateAck:)
                                                 name:@"Refresh_SingleChat_acks"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(audioClip:)
                                                 name:@"Audio_File_Created"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(incomingNotification:) name:@"InputBar_Length" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LinkincomingNotification:) name:@"LinkURL" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ClearView:) name:@"ClearView" object:nil];

    isAvailinChatsTable=NO;
    isAvailinChatsTable=[db_class doesChatExist:strZoeChatId];
     __weak SingleChatVC *controller = self;
    if (arrWholetableArray.count!=0)
    {
        [controller tableViewScrollToBottomAnimated:NO];
    }
   
}


-(void)audioClip:(NSNotification *)notification
{
    NSDictionary  *dictAudio = [[notification userInfo] objectForKey:@"AudioDict"];
    NSLog(@"%@",dictAudio);
    
    NSDate *now=[NSDate date];
    long long milliSeconds=[self convertDateToMilliseconds:now];
    NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
    
    NSString * strAudioURL=[NSString stringWithFormat:@"%@%@",FILE_AWS_IMAGE_BASE_URL,[dictAudio valueForKey:@"audioFileName"]];
    NSString *strMsgId = [[NSUUID UUID] UUIDString]; //id
    
    
    BOOL success=NO;
    if (isAvailinChatsTable==NO)
    {
        success=[db_class insertChats:@"" chatRoomId:strZoeChatId chatRoomType:strChatRoomType sender:@"0" lastMessage:strAudioURL lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"audio" unReadCount:@"0" groupName:@""  groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
        if (success==YES)
        {
            isAvailinChatsTable=YES;
        }
    }
    else
    {
        success=[db_class updateLastMessage:strZoeChatId sender:@"0" lastMessage:strAudioURL lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"audio" unReadCount:@"0"];
        NSLog(@"success");
    }
    
    success=[db_class insertChatMessages:strMsgId userId:strZoeChatId groupId:strZoeChatId chatRoomType:strChatRoomType content:strAudioURL contentType:@"audio" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
    
    Message *message = [[Message alloc] init];
  //  message.image = imageToUpload;
    message.type=@"audio";
    message.date = now;
    message.chat_id = strMsgId;
    message.text=strAudioURL;
    //   message.caption=@"hi hsdbfh sjhdbfsd hsdbv";
    
    [self.tableArray addObject:message];
    [arrWholetableArray addObject:message];
    
    //Insert Message in UI
    NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
    [self.tableSingleChat beginUpdates];
    if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
        [self.tableSingleChat insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                            withRowAnimation:UITableViewRowAnimationNone];
    [self.tableSingleChat insertRowsAtIndexPaths:@[indexPath]
                                withRowAnimation:UITableViewRowAnimationBottom];
    [self.tableSingleChat endUpdates];
    
    [self.tableSingleChat scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                                atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    NSArray *arr=[[NSArray alloc]initWithObjects:[self.tableArray indexPathForLastMessage], nil];
    [self.tableSingleChat reloadRowsAtIndexPaths:arr withRowAnimation:NO];
    
    
    if ([strCurrentlyUploading length]==0)
    {
        [self startUploadingImagesOrVideoOrAudio];
    }

}

-(void)updateReceiveMessage:(NSNotification *)notification
{
    
    NSArray  *dataReceiveMsg = [[notification userInfo] objectForKey:@"msgsArray"];
    
    NSLog(@"%@",dataReceiveMsg);
    NSString *strMsgFrom;
    NSString *strGroupId;
    NSString *strChtRmTyp;
    for (int j=0; j<dataReceiveMsg.count; j++)
    {
        NSDictionary *dictReceiveMsg=[dataReceiveMsg objectAtIndex:j];
        strMsgFrom=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"from"]];
        strGroupId=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"groupId"]];
        
        Message *message = [[Message alloc] init];
        message.text = [dictReceiveMsg valueForKey:@"content"];
        message.type=[dictReceiveMsg valueForKey:@"contentType"];
        message.date=[self convertMillisecondsToDate:[[dictReceiveMsg valueForKey:@"time"]longLongValue]];
        message.chat_id = [dictReceiveMsg valueForKey:@"messageId"];
        message.caption=[dictReceiveMsg valueForKey:@"caption"];
        message.contactName=[dictReceiveMsg valueForKey:@"contactName"];
        message.contactNo=[dictReceiveMsg valueForKey:@"contactNumber"];
        
        strChtRmTyp=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"chatRoomType"]];
        if ([strChtRmTyp isEqualToString:@"1"])
        {
            NSDictionary *dictInfo = [db_class getUserInfo:[dictReceiveMsg valueForKey:@"from"]];
            if (dictInfo.count!=0)
            {
                message.name=[dictInfo valueForKey:@"name"];
            }
            else
            {
                NSString *strNme=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"from"]];
                message.name=strNme;
            }
        }
        
        
        if ([message.type isEqualToString:@"location"])
        {
            NSDictionary *dictGetMsg=[db_class getChatMessage:message.chat_id];
            
            NSString *strImageName=[NSString stringWithFormat:@"%@", [dictGetMsg valueForKey:@"content"]];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
            message.image = [UIImage imageWithContentsOfFile:getImagePath];
            message.Latitude=[dictReceiveMsg valueForKey:@"latitude"];
            message.Longitude=[dictReceiveMsg valueForKey:@"longitude"];
        }
        else if ([message.type isEqualToString:@"image"])
        {
            NSString *strContent=[NSString stringWithFormat:@"%@", [dictReceiveMsg valueForKey:@"content"]];
            NSArray *components = [strContent componentsSeparatedByString:@"/"];
            NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
            
            strImageName=[NSString stringWithFormat:@"%@thumb_%@",FILE_AWS_IMAGE_BASE_URL, strImageName];
            
            NSURL *imageURL = [NSURL URLWithString:strImageName];
            //                 NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            //                 message.image = [UIImage imageWithData:imageData];
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadImageWithURL:imageURL
                                  options:0
                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                     // progression tracking code, optional.
                                     // You can set the progress block to nil
                                 }
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (image) {
                                        // Resize your image here, then set it to UIImageView
                                        NSIndexPath *index = [NSIndexPath indexPathForRow:j inSection:0];
                                        
                                        
                                        message.image =image;
                                        [_tableSingleChat reloadData];
                                        
                                        //                                             [_tableSingleChat reloadRowsAtIndexPaths:[NSArray arrayWithObject:index] withRowAnimation:UITableViewRowAnimationNone];
                                        // resize to 0.5x, function available since iOS 6
                                    }
                                    else{
                                        
                                        NSLog(@"Image not found %@",error);
                                    }
                                }];
            
        }
        
        else if ([message.type isEqualToString:@"video"])
            
        {
            
            
            NSString *strContent=[NSString stringWithFormat:@"%@", [dictReceiveMsg valueForKey:@"content"]];
            NSArray *components = [strContent componentsSeparatedByString:@"/"];
            NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
            NSString *name = strImageName;
            NSString *S1 = [name componentsSeparatedByString:@"."][0];
            NSString *S2 = [NSString stringWithFormat:@"%@.jpg",S1];
            NSLog(@"%@",S1);
            NSLog(@"%@",S2);
            
            strImageName=[NSString stringWithFormat:@"%@thumb_%@",FILE_AWS_IMAGE_BASE_URL, S2];
            //                            UIImageView *getImg = [[UIImageView alloc]init];
            //
            //
            //                            [getImg sd_setImageWithURL:[NSURL URLWithString:strImageName]placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
            //                            message.image = getImg.image;
            NSURL *imageURL = [NSURL URLWithString:strImageName];
            //                            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            //                            message.image = [UIImage imageWithData:imageData];
            message.text = [dictReceiveMsg valueForKey:@"content"];
            
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadImageWithURL:imageURL
                                  options:0
                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                     // progression tracking code, optional.
                                     // You can set the progress block to nil
                                 }
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (image) {
                                        // Resize your image here, then set it to UIImageView
                                        message.image =image;
                                        [_tableSingleChat reloadData];
                                        
                                        // resize to 0.5x, function available since iOS 6
                                    }
                                }];
            
            
        }
        
        
        long long milli=[self convertDateToMilliseconds:[NSDate date]];
        NSString *strMsgTime=[NSString stringWithFormat:@"%lld",milli];
        NSString *strMsgId=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"messageId"]];
        NSString *strMsgSender=[NSString stringWithFormat:@"%@",[dictReceiveMsg valueForKey:@"from"]];
        BOOL success=NO;
        NSLog(@"%@",strMsgFrom);
        NSLog(@"%@",strZoeChatId);
        
        if ([strMsgFrom isEqualToString:strZoeChatId] || [strGroupId isEqualToString:strZoeChatId])
        {
            message.sender=MessageSenderSomeone;
            message.status=MessageStatusRead;
            
            [self.tableArray addObject:message];
            [arrWholetableArray addObject:message];
            
            success=[db_class updateSeenTime:strMsgId seenTime:strMsgTime];
            
            if ([strChatRoomType isEqualToString:@"0"])
            {
                [appDelegate.socket emit:@"sendSeen" with:@[@{@"messageId":strMsgId,@"from":strMsgSender,@"to": appDelegate.strID,@"time":strMsgTime,@"chatRoomType":strChatRoomType}]];
            }
            else
            {
                [appDelegate.socket emit:@"sendSeen" with:@[@{@"messageId":strMsgId,@"from":strMsgSender,@"to": strZoeChatId,@"time":strMsgTime,@"chatRoomType":strChatRoomType}]];
            }
            
            NSDictionary *dict=[db_class getSingleChat:strMsgSender];
            
            if ([[dict valueForKey:@"lastMessageTime"]isEqualToString:[dictReceiveMsg valueForKey:@"time"]])
            {
                success=[db_class updateLastMsgStatus:strMsgSender lastMsgStatus:@"read"];
            }
            [self playSound:@"MessageReceived" :@"mp3"];
            
        }
        else{
            
            NSLog(@"%@",strMsgFrom);
            NSLog(@"%@",strZoeChatId);
            NSLog(@"%@",strMsgSender);
            
            message.sender=MessageSenderMyself;
            message.status=MessageStatusSent;
            
            if ([message.type isEqualToString:@"image"])
            {
                NSString *strContent=[NSString stringWithFormat:@"%@", [dictReceiveMsg valueForKey:@"content"]];
                NSArray *components = [strContent componentsSeparatedByString:@"/"];
                NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                
                strImageName=[NSString stringWithFormat:@"%@thumb_%@",FILE_AWS_IMAGE_BASE_URL, strImageName];
                
                NSURL *imageURL = [NSURL URLWithString:strImageName];
                //                 NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                //                 message.image = [UIImage imageWithData:imageData];
                SDWebImageManager *manager = [SDWebImageManager sharedManager];
                [manager downloadImageWithURL:imageURL
                                      options:0
                                     progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                         // progression tracking code, optional.
                                         // You can set the progress block to nil
                                     }
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                        if (image) {
                                            // Resize your image here, then set it to UIImageView
                                            NSIndexPath *index = [NSIndexPath indexPathForRow:j inSection:0];
                                            
                                            
                                            message.image =image;
                                            [_tableSingleChat reloadData];
                                            
                                            //                                             [_tableSingleChat reloadRowsAtIndexPaths:[NSArray arrayWithObject:index] withRowAnimation:UITableViewRowAnimationNone];
                                            // resize to 0.5x, function available since iOS 6
                                        }
                                        else{
                                            
                                            NSLog(@"Image not found %@",error);
                                        }
                                    }];
                
                success=[db_class insertChatMessages:strMsgId userId:strMsgSender groupId:strMsgSender chatRoomType:strChtRmTyp content:strContent contentType:@"image" contentStatus:@"sending" sender:@"0" sentTime:strMsgTime deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                
            }
            
            else if ([message.type isEqualToString:@"location"])
            {
                NSDictionary *dictGetMsg=[db_class getChatMessage:message.chat_id];
                
                NSString *strImageName=[NSString stringWithFormat:@"%@", [dictGetMsg valueForKey:@"content"]];
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
                message.image = [UIImage imageWithContentsOfFile:getImagePath];
                message.Latitude=[dictReceiveMsg valueForKey:@"latitude"];
                message.Longitude=[dictReceiveMsg valueForKey:@"longitude"];
                
                success=[db_class insertChatMessages:strMsgId userId:strMsgSender groupId:strMsgSender chatRoomType:strChtRmTyp content:strImageName contentType:@"location" contentStatus:@"sending" sender:@"0" sentTime:strMsgTime deliveredTime:@"0" seenTime:@"0" caption:message.caption isDownloaded:@"0" latitude:message.Latitude longitude:message.Longitude contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
            }
            else if ([message.type isEqualToString:@"video"])
                
            {
                
                
                NSString *strContent=[NSString stringWithFormat:@"%@", [dictReceiveMsg valueForKey:@"content"]];
                NSArray *components = [strContent componentsSeparatedByString:@"/"];
                NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                NSString *name = strImageName;
                NSString *S1 = [name componentsSeparatedByString:@"."][0];
                NSString *S2 = [NSString stringWithFormat:@"%@.jpg",S1];
                NSLog(@"%@",S1);
                NSLog(@"%@",S2);
                
                strImageName=[NSString stringWithFormat:@"%@thumb_%@",FILE_AWS_IMAGE_BASE_URL, S2];
                //                            UIImageView *getImg = [[UIImageView alloc]init];
                //
                //
                //                            [getImg sd_setImageWithURL:[NSURL URLWithString:strImageName]placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
                //                            message.image = getImg.image;
                NSURL *imageURL = [NSURL URLWithString:strImageName];
                //                            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                //                            message.image = [UIImage imageWithData:imageData];
                message.text = [dictReceiveMsg valueForKey:@"content"];
                
                SDWebImageManager *manager = [SDWebImageManager sharedManager];
                [manager downloadImageWithURL:imageURL
                                      options:0
                                     progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                         // progression tracking code, optional.
                                         // You can set the progress block to nil
                                     }
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                        if (image) {
                                            // Resize your image here, then set it to UIImageView
                                            message.image =image;
                                            [_tableSingleChat reloadData];
                                            
                                            // resize to 0.5x, function available since iOS 6
                                        }
                                    }];
                success=[db_class insertChatMessages:strMsgId userId:strMsgSender groupId:strMsgSender chatRoomType:strChtRmTyp content:strContent contentType:@"image" contentStatus:@"sending" sender:@"0" sentTime:strMsgTime deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                
                
            }
            else
            {
                
                success=[db_class insertChatMessages:strMsgId userId:strMsgSender groupId:strMsgSender chatRoomType:strChtRmTyp content:message.text contentType:message.type contentStatus:@"delivered" sender:@"0" sentTime:strMsgTime deliveredTime:strMsgTime seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
            }
            
            [self.tableArray addObject:message];
            [arrWholetableArray addObject:message];
            if ([strChtRmTyp isEqualToString:@"0"])
            {
                if ([strMsgFrom isEqualToString:strZoeChatId] )
                    
                {
                    [self.tableSingleChat reloadData];
                }
            }
            else if ([strChtRmTyp isEqualToString:@"1"])
            {
                if ( [strGroupId isEqualToString:strZoeChatId] )
                {
                    [self.tableSingleChat reloadData];
                }
            }

        }
        
        
    }
    if ([strChtRmTyp isEqualToString:@"0"])
    {
        if ([strMsgFrom isEqualToString:strZoeChatId] )
            
        {
            [self.tableSingleChat reloadData];
        }
    }
    else if ([strChtRmTyp isEqualToString:@"1"])
    {
        if ( [strGroupId isEqualToString:strZoeChatId] )
        {
            [self.tableSingleChat reloadData];
        }
    }

    
    BOOL Success=NO;
    Success=[db_class updateUnreadCount:strZoeChatId unreadCount:@"0"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_App_Badge_Count" object:self];
    
}

-(void)updateAck:(NSNotification *)notification
{
    NSArray  *dataACK = [[notification userInfo] objectForKey:@"ackArray"];
         for (int j=0; j<dataACK.count; j++)
         {
             
             NSDictionary *dictACK=[dataACK objectAtIndex:j];
             for (int i=0; i<arrWholetableArray.count; i++)
             {
                 Message *message = [arrWholetableArray objectAtIndex:i];
                 NSString *strMsgId=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"messageid"]];
                 if ([message.chat_id isEqualToString:strMsgId])
                 {
                     NSString *strMsgStatus=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"status"]];
                     if ([strMsgStatus isEqualToString:@"sent"])
                     {
                          message.status = MessageStatusSent;
                          NSString *strMsgTo=[NSString stringWithFormat:@"%@",[dictACK valueForKey:@"to"]];
                         if ([strMsgTo isEqualToString:strZoeChatId])
                         {
                             
                             [self playSound:@"MessageSentTick" :@"mp3"];
                         }
                     }
                     else if ([strMsgStatus isEqualToString:@"delivered"])
                     {
                         message.status = MessageStatusReceived;
                     }
                     else if ([strMsgStatus isEqualToString:@"read"])
                     {
                         message.status = MessageStatusRead;
                     }
                     
                     NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
                     MessageCell *cell = (MessageCell *)[self.tableSingleChat cellForRowAtIndexPath:indexPath];
                     [cell updateMessageStatus];
                     [self.tableSingleChat reloadData];
                     
                 }
             }
             
         }
  
}
- (void)playSound :(NSString *)fName :(NSString *) ext{
    SystemSoundID audioEffect;
    NSString *path = [[NSBundle mainBundle] pathForResource : fName ofType :ext];
    if ([[NSFileManager defaultManager] fileExistsAtPath : path]) {
        NSURL *pathURL = [NSURL fileURLWithPath: path];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef) pathURL, &audioEffect);
        AudioServicesPlaySystemSound(audioEffect);
    }
    else {
        NSLog(@"error, file not found: %@", path);
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
//    appDelegate.ZOEID = @"";
    __weak Inputbar *inputbar = _inputbar;
    __weak UITableView *tableView = _tableSingleChat;
    __weak SingleChatVC *controller = self;
    
    [controller tableViewScrollToBottomAnimated:NO];
    self.view.keyboardTriggerOffset = inputbar.frame.size.height;
    [self.view addKeyboardPanningWithActionHandler:^(CGRect keyboardFrameInView, BOOL opening, BOOL closing) {
        /*
         Try not to call "self" inside this block (retain cycle).
         But if you do, make sure to remove DAKeyboardControl
         when you are done with the view controller by calling:
         [self.view removeKeyboardControl];
         */
        
        CGRect toolBarFrame = inputbar.frame;
        toolBarFrame.origin.y = keyboardFrameInView.origin.y - toolBarFrame.size.height;
        inputbar.frame = toolBarFrame;
        
        CGRect tableViewFrame = tableView.frame;
        tableViewFrame.size.height = toolBarFrame.origin.y;
        tableView.frame = tableViewFrame;
        
        [controller tableViewScrollToBottomAnimated:NO];
    }];
    
  
    
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.view endEditing:YES];
    [self.view removeKeyboardControl];
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    appDelegate.ZOEID = @"";
   // [self.gateway dismiss];
}
-(void)viewWillDisappear:(BOOL)animated
{
    
    NSString *strListenTyping=[NSString stringWithFormat:@"%@:typing",strZoeChatId];
    [appDelegate.socket off:strListenTyping];
    
    NSString *strOnlineStatus=[NSString stringWithFormat:@"%@:onlineStatus",strZoeChatId];
    [appDelegate.socket off:strOnlineStatus];
    
     [timerGetOnlineStatus invalidate];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
     [_inputbar resignFirstResponder];
}


#pragma mark -

-(void)setInputbar
{
    self.inputbar.placeholder = @"Type a message";
    self.inputbar.delegate = self;
}

-(void)setTableView
{
    self.tableArray = [[TableArray alloc] init];
    _tableSingleChat.delegate=self;
    _tableSingleChat.dataSource=self;
    self.tableSingleChat.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f,self.view.frame.size.width, 10.0f)];
    self.tableSingleChat.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableSingleChat.backgroundColor = [UIColor clearColor];
 //   [self.tableSingleChat registerClass:[MessageCell class] forCellReuseIdentifier:nil];
    
    self.tableSingleChat.showsVerticalScrollIndicator=NO;
    self.tableSingleChat.showsHorizontalScrollIndicator=NO;
    
    __weak Inputbar *inputbar = _inputbar;
    CGRect toolBarFrame = inputbar.frame;
    CGRect tableViewFrame = self.tableSingleChat.frame;
    tableViewFrame.size.height = toolBarFrame.origin.y;
    self.tableSingleChat.frame = tableViewFrame;

}

#pragma mark - Actions

- (IBAction)userDidTapScreen:(id)sender
{
    [_inputbar resignFirstResponder];
    [_inputTextView resignFirstResponder];
    [self.view endEditing:YES];
}
-(void)inputbarDidBecomeFirstResponder:(Inputbar *)inputbar
{
    [self RemoveViewAttachments];
}

-(void)insertdata
{
    for (int i=1; i<=1; i++)
    {
        BOOL success = NO;
        NSString *alertString = @"Data Insertion failed";
        
        //  NSString *strUserId=[NSString stringWithFormat:@"%d",i];
        
        success=[db_class insertChatMessages:@"1" userId:@"99" groupId:@"" chatRoomType:strChatRoomType content:@"aaa" contentType:@"bbb" contentStatus:@"ccc" sender:@"1" sentTime:@"ddd" deliveredTime:@"eee" seenTime:@"3:21 PM" caption:@"fff" isDownloaded:@"1" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
        
        if (success == NO)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:
                                  alertString message:nil
                                                          delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   //  // Dispose of any resources that can be recreated.
}

-(IBAction)onBack:(id)sender
{
//    [self.navigationController popViewControllerAnimated:YES];
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MainTabBarVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainTabBarVC"];
    [self.navigationController pushViewController:hme animated:YES];
}


#pragma mark - TableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.tableArray numberOfSections];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%@",self.tableArray);
    return [self.tableArray numberOfMessagesInSection:section];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:[indexPath description]];
    DGActivityIndicatorView *activityIndicatorView;
    Message *msg = [arrWholetableArray objectAtIndex:indexPath.row];
    
   
  
    
        NSInteger rowNumber = 0;
        
        for (NSInteger i = 0; i < indexPath.section; i++) {
            rowNumber += [self tableView:tableView numberOfRowsInSection:i];
        }
        
        rowNumber += indexPath.row;
        loadIndexathss = indexPath;
        NSLog(@"%ld",(long)loadIndexathss);
        NSLog(@"ROW:%ld",(long)indexPath.row);
        NSLog(@"SECTIONS:%ld",(long)indexPath.section);
         if (!cell)
        {
            cell = [[MessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        }
        
        cell.message = [self.tableArray objectAtIndexPath:indexPath];
        if ([cell.message.type isEqualToString:@"video"])
        {
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *getStatus = [defaults objectForKey:@"videoStatus"];
            if([getStatus isEqualToString:@"Wi-fi"] || [getStatus isEqualToString:@"Wi-fi and Cellular"])
            {
                NSString *strContent=[NSString stringWithFormat:@"%@", cell.message.text];
                NSArray *components = [strContent componentsSeparatedByString:@"/"];
                NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
                NSURL *assetURL=[NSURL fileURLWithPath:getImagePath];
                cell.message.image = [self thumbnailImageFromURL:assetURL];
                cell.message.text = strContent;
            }
            else{
            NSString *strContent=[NSString stringWithFormat:@"%@", cell.message.text];
            NSArray *components = [strContent componentsSeparatedByString:@"/"];
            NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
            NSString *name = strImageName;
            NSString *S1 = [name componentsSeparatedByString:@"."][0];
            NSString *S2 = [NSString stringWithFormat:@"%@.jpg",S1];
            NSLog(@"%@",S1);
            NSLog(@"%@",S2);
            
            strImageName=[NSString stringWithFormat:@"%@thumb_%@",FILE_AWS_IMAGE_BASE_URL, S2];
            UIImageView *getImg = [[UIImageView alloc]init];
            NSURL *imageURL = [NSURL URLWithString:strImageName];
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadImageWithURL:imageURL
                                  options:0
                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                     // progression tracking code, optional.
                                     // You can set the progress block to nil
                                 }
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (image) {
                                        // Resize your image here, then set it to UIImageView
                                        cell.message.image =image;
                                        // resize to 0.5x, function available since iOS 6
                                    }
                                }];
            cell.message.text = strContent;
            }
            
        }
        NSArray *arrChatMsgs=[[NSArray alloc]init];
        NSDictionary *getlastInfo=[[NSDictionary alloc]init];
    getlastInfo = [arrWholetableArray objectAtIndex:rowNumber];
    /*
        if ([strChatRoomType isEqualToString:@"1"])
        {
            arrChatMsgs = [db_class getGroupMessages:strZoeChatId];
            NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
            if (arrChatMsgs.count > 10)
            {
                NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
                getlastInfo = [reversed objectAtIndex:rowNumber];
                
            }
            else{
                getlastInfo = [arrWholetableArray objectAtIndex:rowNumber];
                
            }
        }
        else
        {
            arrChatMsgs = [db_class getOneToOneChatMessages:strZoeChatId];
            NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
            if (arrChatMsgs.count > 10)
            {
                NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
                getlastInfo = [reversed objectAtIndex:rowNumber];
                
            }
            else{
                getlastInfo = [arrWholetableArray objectAtIndex:rowNumber];
                
            }
        }
    */

        NSString *checkStar = [getlastInfo valueForKey:@"checkStar"];
        if([checkStar  isEqual:@"1"])
        {
            cell.StarimgView.image = [UIImage imageNamed:@"FilledStar"];
            
        }
        else
        {
            cell.StarimgView.image = [UIImage imageNamed:@""];
            
        }
        
        
        
        if ([cell.message.type isEqualToString:@"image"] || [cell.message.type isEqualToString:@"video"])
        {
            long status=cell.message.status;
            NSString *strStat=[NSString stringWithFormat:@"%ld",status];
            
            if (cell.message.sender==MessageSenderMyself)
            {
                if ([strStat isEqualToString:@"0"])
                {
                    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
                    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
                    if (color != nil) {
                        
                        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:color size:50.0f];
                        
                        
                        
                        
                        
                    }
                    else
                    {
                        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor whiteColor] size:50.0f];
                        
                        
                        
                        
                        
                        
                    }

                    
                    activityIndicatorView.frame = CGRectMake((cell.imgView.frame.size.width/2)-25, (cell.imgView.frame.size.height/2)-25, 50, 50);
                    activityIndicatorView.backgroundColor=[UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:0.5];
                    activityIndicatorView.layer.cornerRadius=25;
                    activityIndicatorView.clipsToBounds=YES;
                    [cell.imgView addSubview:activityIndicatorView];
                    activityIndicatorView.tag=rowNumber+22000;
                    [activityIndicatorView startAnimating];
                    
                    UIButton *btnCancelUpload=[[UIButton alloc]initWithFrame:CGRectMake(cell.imgView.center.x-12.5, cell.imgView.center.y-12.5, 25, 25)];
                    btnCancelUpload.tag=rowNumber+25000;
                    [btnCancelUpload setBackgroundImage:[UIImage imageNamed:@"CloseIcon.png"] forState:UIControlStateNormal];
                    [btnCancelUpload addTarget:self action:@selector(cancelUpload:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.contentView addSubview:btnCancelUpload];
                }
                else if ([strStat isEqualToString:@"5"])
                {
                    UIButton *btnRetry=[[UIButton alloc]initWithFrame:CGRectMake(cell.imgView.center.x-25, cell.imgView.center.y-12.5, 50, 25)];
                    btnRetry.tag=rowNumber+28000;
                    [btnRetry setBackgroundImage:[UIImage imageNamed:@"retry.png"] forState:UIControlStateNormal];
                    [btnRetry addTarget:self action:@selector(RetryUpload:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.contentView addSubview:btnRetry];
                }
                else if ([strStat isEqualToString:@"6"])
                {
                    UIButton *btnCancelUpload=(UIButton *)[cell.contentView viewWithTag:rowNumber+25000];
                    [btnCancelUpload removeFromSuperview];
                    
                    UIButton *btnRetry=(UIButton *)[cell.contentView viewWithTag:rowNumber+28000];
                    [btnRetry removeFromSuperview];
                    DGActivityIndicatorView *activityIndicatorView = (DGActivityIndicatorView *)[cell.imgView viewWithTag:rowNumber+22000];
                    [activityIndicatorView stopAnimating];
                    
                }
                
                if (![strStat isEqualToString:@"0"] && ![strStat isEqualToString:@"4"] && ![strStat isEqualToString:@"5"])
                {
                    if ([cell.message.type isEqualToString:@"video"])
                    {
                        UIButton *btnPlay=[[UIButton alloc]initWithFrame:CGRectMake(cell.imgView.center.x-25, cell.imgView.center.y-25, 50 , 50)];
                        btnPlay.tag=rowNumber+35000;
                        [btnPlay setBackgroundImage:[UIImage imageNamed:@"Play.png"] forState:UIControlStateNormal];
                        [btnPlay addTarget:self action:@selector(onPlayVideo:) forControlEvents:UIControlEventTouchUpInside];
                        [cell.contentView addSubview:btnPlay];
                    }
                }
            }
            else
                {
                    
                    NSString *strIsDownloaded=[getlastInfo valueForKey:@"isDownloaded"];
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    NSString *getStatus = [defaults objectForKey:@"photoStatus"];
                    if([getStatus isEqualToString:@"Wi-fi"] || [getStatus isEqualToString:@"Wi-fi and Cellular"])
                            {
                            
                                
                                if ([strStat isEqualToString:@"8"])
                                {
                                    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
                                    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
                                    if (color != nil) {
                                        
                                        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:color size:50.0f];
                                        
                                        
                                        
                                        
                                        
                                    }
                                    else
                                    {
                                        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor whiteColor] size:50.0f];
                                        
                                        
                                        
                                        
                                        
                                        
                                    }

                                    activityIndicatorView.frame = CGRectMake((cell.imgView.frame.size.width/2)-25, (cell.imgView.frame.size.height/2)-25, 50, 50);
                                    activityIndicatorView.backgroundColor=[UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:0.5];
                                    activityIndicatorView.layer.cornerRadius=25;
                                    activityIndicatorView.clipsToBounds=YES;
                                    [cell.imgView addSubview:activityIndicatorView];
                                    activityIndicatorView.tag=rowNumber+22000;
                                    [activityIndicatorView startAnimating];
                                    
                                    
                                    UIButton *btnCancelDownload=[[UIButton alloc]initWithFrame:CGRectMake(cell.imgView.center.x-12.5, cell.imgView.center.y-12.5, 25, 25)];
                                    btnCancelDownload.tag=rowNumber+33000;
                                    [btnCancelDownload setBackgroundImage:[UIImage imageNamed:@"CloseIcon.png"] forState:UIControlStateNormal];
                                    [btnCancelDownload addTarget:self action:@selector(cancelDownload:) forControlEvents:UIControlEventTouchUpInside];
                                    [cell.contentView addSubview:btnCancelDownload];
                                    
                                }
                                else if ([strStat isEqualToString:@"7"])
                                {
                                    if ([cell.message.type isEqualToString:@"video"])
                                    {
                                        UIButton *btnPlay=[[UIButton alloc]initWithFrame:CGRectMake(cell.imgView.center.x-25, cell.imgView.center.y-25, 50, 50)];
                                        btnPlay.tag=rowNumber+35000;
                                        [btnPlay setBackgroundImage:[UIImage imageNamed:@"Play.png"] forState:UIControlStateNormal];
                                        [btnPlay addTarget:self action:@selector(onPlayVideo:) forControlEvents:UIControlEventTouchUpInside];
                                        [cell.contentView addSubview:btnPlay];
                                    }
                                }
                                else
                                {
                                    UIButton *btnDownload=[[UIButton alloc]initWithFrame:cell.imgView.frame];
                                    btnDownload.tag=rowNumber+30000;
                                    [self autoDownloaded:indexPath.row];

                                    [btnDownload setImage:[UIImage imageNamed:@"download.png"] forState:UIControlStateNormal];
                                    [btnDownload addTarget:self action:@selector(downloadImageOrVideo:) forControlEvents:UIControlEventTouchUpInside];
                                    [cell.contentView addSubview:btnDownload];
                                }
                            }
                                 
        
                    else
                    {
                if ([strStat isEqualToString:@"8"])
                {
                    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
                    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
                    if (color != nil) {
                        
                        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:color size:50.0f];
                        
                        
                        
                        
                        
                    }
                    else
                    {
                        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor whiteColor] size:50.0f];
                        
                        
                        
                        
                        
                        
                    }

                    activityIndicatorView.frame = CGRectMake((cell.imgView.frame.size.width/2)-25, (cell.imgView.frame.size.height/2)-25, 50, 50);
                    activityIndicatorView.backgroundColor=[UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:0.5];
                    activityIndicatorView.layer.cornerRadius=25;
                    activityIndicatorView.clipsToBounds=YES;
                    [cell.imgView addSubview:activityIndicatorView];
                    activityIndicatorView.tag=rowNumber+22000;
                    [activityIndicatorView startAnimating];
                    
                    
                    UIButton *btnCancelDownload=[[UIButton alloc]initWithFrame:CGRectMake(cell.imgView.center.x-12.5, cell.imgView.center.y-12.5, 25, 25)];
                    btnCancelDownload.tag=rowNumber+33000;
                    [btnCancelDownload setBackgroundImage:[UIImage imageNamed:@"CloseIcon.png"] forState:UIControlStateNormal];
                    [btnCancelDownload addTarget:self action:@selector(cancelDownload:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.contentView addSubview:btnCancelDownload];
                    
                }
                else if ([strStat isEqualToString:@"7"])
                {
                    if ([cell.message.type isEqualToString:@"video"])
                    {
                        UIButton *btnPlay=[[UIButton alloc]initWithFrame:CGRectMake(cell.imgView.center.x-25, cell.imgView.center.y-25, 50, 50)];
                        btnPlay.tag=rowNumber+35000;
                        [btnPlay setBackgroundImage:[UIImage imageNamed:@"Play.png"] forState:UIControlStateNormal];
                        [btnPlay addTarget:self action:@selector(onPlayVideo:) forControlEvents:UIControlEventTouchUpInside];
                        [cell.contentView addSubview:btnPlay];
                    }
                }
                else
                {
                    UIButton *btnDownload=[[UIButton alloc]initWithFrame:cell.imgView.frame];
                    btnDownload.tag=rowNumber+30000;
                    [btnDownload setImage:[UIImage imageNamed:@"download.png"] forState:UIControlStateNormal];
                    [btnDownload addTarget:self action:@selector(downloadImageOrVideo:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.contentView addSubview:btnDownload];
                }
                    }
                }
            
            
            
        }
    else if ([cell.message.type isEqualToString:@"location"])
    {
        
        
//        if (cell.message.sender==MessageSenderMyself)
//                    {
        NSString *strImageName=[NSString stringWithFormat:@"%@", cell.message.text];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
        cell.message.image = [UIImage imageWithContentsOfFile:getImagePath];
        cell.message.text = cell.message.text;
        cell.message.Latitude=cell.message.Latitude;
        cell.message.Longitude=cell.message.Longitude;
//                    }
//        else
//        {
//            NSString *strImageName=[NSString stringWithFormat:@"%@", cell.message.text];
//            
//            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
//            NSString *documentsDirectory = [paths objectAtIndex:0];
//            NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
//            cell.message.image = [UIImage imageWithContentsOfFile:getImagePath];
//            cell.message.text = cell.message.text;
//            cell.message.Latitude=cell.message.Latitude;
//            cell.message.Longitude=cell.message.Longitude;
//        }
        
    }
        else if ([cell.message.type isEqualToString:@"contact"])
        {
            if (cell.message.sender==MessageSenderMyself)
            {
                [cell.btnInviteOrMsg addTarget:self action:@selector(onInvite:) forControlEvents:UIControlEventTouchDown];
                cell.btnInviteOrMsg.tag=rowNumber+45000;
            }
            else
            {
                [cell.btnAddContact addTarget:self action:@selector(onAddNewContact:) forControlEvents:UIControlEventTouchDown];
                cell.btnAddContact.tag=rowNumber+48000;
                
                
                [cell.btnInviteOrMsg addTarget:self action:@selector(onInvite:) forControlEvents:UIControlEventTouchDown];
                cell.btnInviteOrMsg.tag=rowNumber+45000;
                
            }
        }
        else if ([cell.message.type isEqualToString:@"audio"])
        {
            long status=cell.message.status;
            NSString *strStat=[NSString stringWithFormat:@"%ld",status];
            
            if (cell.message.sender==MessageSenderMyself)
            {
                [cell.btnAudioAction setUserInteractionEnabled:YES];
                if ([strStat isEqualToString:@"0"])
                {
                    float xPos=cell.viewContact.frame.origin.x + cell.imgContact.frame.size.width;
                    
                    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
                    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
                    if (color != nil) {
                        
                        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:color size:30.0f];
                        
                        
                        
                        
                        
                    }
                    else
                    {
                        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor blackColor] size:30.0f];
                        
                        
                        
                        
                        
                        
                    }

                    activityIndicatorView.frame = CGRectMake(xPos + 15, 12, 30, 30);
                    // activityIndicatorView.frame = CGRectMake(0, 12, 30, 30);
                    activityIndicatorView.backgroundColor=[UIColor clearColor];
                    activityIndicatorView.layer.cornerRadius=activityIndicatorView.frame.size.height/2;
                    activityIndicatorView.clipsToBounds=YES;
                    [cell.contentView addSubview:activityIndicatorView];
                    activityIndicatorView.tag=rowNumber+22000;
                    [activityIndicatorView startAnimating];
                    
                    [cell.btnAudioAction addTarget:self action:@selector(cancelUpload:) forControlEvents:UIControlEventTouchDown];
                    cell.btnAudioAction.tag=rowNumber+25000;
                    [cell.btnAudioAction setBackgroundImage:[UIImage imageNamed:@"CloseIcon.png"] forState:UIControlStateNormal];
                    
                    [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                    cell.slider.tag=rowNumber+25000;
                    NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                    
                }
                else if ([strStat isEqualToString:@"5"])
                {
                    [cell.btnAudioAction addTarget:self action:@selector(RetryUpload:) forControlEvents:UIControlEventTouchDown];
                    cell.btnAudioAction.tag=rowNumber+28000;
                    [cell.btnAudioAction setBackgroundImage:[UIImage imageNamed:@"UploadAudio.png"] forState:UIControlStateNormal];
                    
                    [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                    cell.slider.tag=rowNumber+54000;
                    NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                }
                 else if ([strStat isEqualToString:@"1"])
                 {
                     [cell.btnAudioAction addTarget:self action:@selector(onAudioPlay:) forControlEvents:UIControlEventTouchUpInside];
                     cell.btnAudioAction.tag=rowNumber+51000;
                     [cell.btnAudioAction setBackgroundImage:[UIImage imageNamed:@"PlayButton.png"] forState:UIControlStateNormal];
                     
                     [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                     cell.slider.tag=rowNumber+54000;
                     NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                 }
                else
                {
                    [cell.btnAudioAction addTarget:self action:@selector(onAudioPlay:) forControlEvents:UIControlEventTouchUpInside];
                    cell.btnAudioAction.tag=rowNumber+51000;
                    [cell.btnAudioAction setBackgroundImage:[UIImage imageNamed:@"PlayButton.png"] forState:UIControlStateNormal];
                    
                    [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                    cell.slider.tag=rowNumber+54000;
                    NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                }
            }
            
            else
            {
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *getStatus = [defaults objectForKey:@"audioStatus"];
                if([getStatus isEqualToString:@"Wi-fi"] || [getStatus isEqualToString:@"Wi-fi and Cellular"])
                {
                    if ([strStat isEqualToString:@"8"])
                    {
                        // float xPos=cell.viewContact.frame.origin.x - cell.imgContact.frame.size.width;
                        NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
                        UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
                        if (color != nil) {
                            
                            activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:color size:50.0f];
                            
                            
                            
                            
                            
                        }
                        else
                        {
                            activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor blackColor] size:50.0f];
                            
                            
                            
                            
                            
                            
                        }

                        //                _viewContact.frame.origin.x + 10, 18, 20, 20
                        
                        activityIndicatorView.frame = CGRectMake(25 , 12, 30, 30);
                        //activityIndicatorView.frame = CGRectMake(xPos + 15, 12, 30, 30);
                        
                        activityIndicatorView.backgroundColor=[UIColor clearColor];
                        activityIndicatorView.layer.cornerRadius=activityIndicatorView.frame.size.height/2;
                        activityIndicatorView.clipsToBounds=YES;
                        [cell.contentView addSubview:activityIndicatorView];
                        activityIndicatorView.tag=rowNumber+22000;
                        [activityIndicatorView startAnimating];
                        
                        [cell.btnAudioAction addTarget:self action:@selector(cancelUpload:) forControlEvents:UIControlEventTouchDown];
                        cell.btnAudioAction.tag=rowNumber+25000;
                        [cell.btnAudioAction setBackgroundImage:[UIImage imageNamed:@"CloseIcon.png"] forState:UIControlStateNormal];
                        
                        [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                        cell.slider.tag=rowNumber+25000;
                        NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                        
                    }
                    else if ([strStat isEqualToString:@"7"])
                    {
                        [cell.btnAudioAction addTarget:self action:@selector(onAudioPlay:) forControlEvents:UIControlEventTouchUpInside];
                        cell.btnAudioAction.tag=rowNumber+51000;
                        [cell.btnAudioAction setBackgroundImage:[UIImage imageNamed:@"PlayButton.png"] forState:UIControlStateNormal];
                        
                        [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                        cell.slider.tag=rowNumber+54000;
                        NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                        
                        
                    }
                    else
                    {
                        
                        cell.btnAudioAction.tag=rowNumber+30000;
                        [cell.btnAudioAction setBackgroundImage:[UIImage imageNamed:@"DownloadAudio.png"] forState:UIControlStateNormal];
//                        [cell.btnAudioAction addTarget:self action:@selector(downloadImageOrVideo:) forControlEvents:UIControlEventTouchDown];
//                        [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
//                        cell.slider.tag=rowNumber+54000;
                        [self autoDownloaded:indexPath.row];
                        NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                        
                    }
                    
                }
                
                
                else
                {
                    if ([strStat isEqualToString:@"8"])
                    {
                        // float xPos=cell.viewContact.frame.origin.x - cell.imgContact.frame.size.width;
                        NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
                        UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
                        if (color != nil) {
                            
                            activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:color size:50.0f];
                            
                            
                            
                            
                            
                        }
                        else
                        {
                            activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor blackColor] size:50.0f];
                            
                            
                            
                            
                            
                            
                        }

                        //                _viewContact.frame.origin.x + 10, 18, 20, 20
                        
                        activityIndicatorView.frame = CGRectMake(25 , 12, 30, 30);
                        //activityIndicatorView.frame = CGRectMake(xPos + 15, 12, 30, 30);
                        
                        activityIndicatorView.backgroundColor=[UIColor clearColor];
                        activityIndicatorView.layer.cornerRadius=activityIndicatorView.frame.size.height/2;
                        activityIndicatorView.clipsToBounds=YES;
                        [cell.contentView addSubview:activityIndicatorView];
                        activityIndicatorView.tag=rowNumber+22000;
                        [activityIndicatorView startAnimating];
                        
                        [cell.btnAudioAction addTarget:self action:@selector(cancelUpload:) forControlEvents:UIControlEventTouchDown];
                        cell.btnAudioAction.tag=rowNumber+25000;
                        [cell.btnAudioAction setBackgroundImage:[UIImage imageNamed:@"CloseIcon.png"] forState:UIControlStateNormal];
                        
                        [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                        cell.slider.tag=rowNumber+25000;
                        NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                        
                    }
                    else if ([strStat isEqualToString:@"7"])
                    {
                        [cell.btnAudioAction addTarget:self action:@selector(onAudioPlay:) forControlEvents:UIControlEventTouchUpInside];
                        cell.btnAudioAction.tag=rowNumber+51000;
                        [cell.btnAudioAction setBackgroundImage:[UIImage imageNamed:@"PlayButton.png"] forState:UIControlStateNormal];
                        
                        [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                        cell.slider.tag=rowNumber+54000;
                        NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                        
                        
                    }
                    else
                    {
                        
                        cell.btnAudioAction.tag=rowNumber+30000;
                        [cell.btnAudioAction setBackgroundImage:[UIImage imageNamed:@"DownloadAudio.png"] forState:UIControlStateNormal];
                        [cell.btnAudioAction addTarget:self action:@selector(downloadImageOrVideo:) forControlEvents:UIControlEventTouchDown];
                        [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                       
                        cell.slider.tag=rowNumber+54000;
                        NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                        
                    }
                    
                }
            }
            
        }
        else if ([cell.message.type isEqualToString:@"document"])
            
            
        {
            long status=cell.message.status;
            NSString *strStat=[NSString stringWithFormat:@"%ld",status];
            NSString *getdocument = [NSString stringWithFormat:@"%@",cell.message.text];
            NSLog(@"%@",getdocument);
            NSArray *components = [getdocument componentsSeparatedByString:@"/"];
            NSString *FileName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
            
            cell.documentName.textColor = [UIColor blackColor];
            [cell.documentName setFont: [cell.documentName.font fontWithSize: 10.0]];
            cell.documentName.text = FileName;

            if (cell.message.sender==MessageSenderMyself)
            {
                if ([strStat isEqualToString:@"0"])
                {
                    float xPos=cell.viewContact.frame.origin.x + cell.imgContact.frame.size.width;
                    
                    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
                    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
                    if (color != nil) {
                        
                        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:color size:50.0f];
                        
                        
                        
                        
                        
                    }
                    else
                    {
                        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor whiteColor] size:50.0f];
                        
                        
                        
                        
                        
                        
                    }

                    activityIndicatorView.frame = CGRectMake(xPos + 15, 12, 30, 30);
                    // activityIndicatorView.frame = CGRectMake(0, 12, 30, 30);
                    activityIndicatorView.backgroundColor=[UIColor clearColor];
                    activityIndicatorView.layer.cornerRadius=activityIndicatorView.frame.size.height/2;
                    activityIndicatorView.clipsToBounds=YES;
                    [cell.contentView addSubview:activityIndicatorView];
                    activityIndicatorView.tag=rowNumber+22000;
                    [activityIndicatorView startAnimating];
                    
                    //                    [cell.docBtn addTarget:self action:@selector(cancelUpload:) forControlEvents:UIControlEventTouchDown];
                    cell.docBtn.tag=rowNumber+25000;
                    //                    [cell.docBtn setBackgroundImage:[UIImage imageNamed:@"CloseIcon.png"] forState:UIControlStateNormal];
                    
                    //                    [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                    //                    cell.slider.tag=rowNumber+25000;
                    //                    NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                    
                }
                else if ([strStat isEqualToString:@"5"])
                {
                  
                    cell.docBtn.tag=rowNumber+28000;
                    //                    [cell.docBtn setBackgroundImage:[UIImage imageNamed:@"UploadAudio.png"] forState:UIControlStateNormal];
                    
                    //                    [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                    //                    cell.slider.tag=rowNumber+54000;
                    //                    NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                }
                
                else
                {
                    [cell.docBtn addTarget:self action:@selector(downloadDoc:) forControlEvents:UIControlEventTouchUpInside];
                    cell.docBtn.tag=rowNumber+51000;
                    //                    [cell.docBtn setBackgroundImage:[UIImage imageNamed:@"PlayButton.png"] forState:UIControlStateNormal];
                    
                    //                    [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                    //                    cell.slider.tag=rowNumber+54000;
                    //                    NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                }
            }
                            else
                {
                if ([strStat isEqualToString:@"8"])
                {
                    float xPos=cell.viewContact.frame.origin.x + cell.imgContact.frame.size.width;
                    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
                    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
                    if (color != nil) {
                        
                        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:color size:50.0f];
                        
                        
                        
                        
                        
                    }
                    else
                    {
                        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor whiteColor] size:50.0f];
                        
                        
                        
                        
                        
                        
                    }

                    //                _viewContact.frame.origin.x + 10, 18, 20, 20
                    //                _btnAudioAction=[[UIButton alloc]initWithFrame:CGRectMake(_viewContact.frame.origin.x + 10, 18, 20, 20)];
                    activityIndicatorView.frame = CGRectMake(xPos-62, 5, 50, 50);
                    //activityIndicatorView.frame = CGRectMake(18, 12, 30, 30);
                    
                    activityIndicatorView.backgroundColor=[UIColor clearColor];
                    activityIndicatorView.layer.cornerRadius=activityIndicatorView.frame.size.height/2;
                    activityIndicatorView.clipsToBounds=YES;
                   // [cell.contentView addSubview:activityIndicatorView];
                    activityIndicatorView.tag=rowNumber+22000;
                    [activityIndicatorView startAnimating];
                    
//                    [cell.docBtn addTarget:self action:@selector(cancelUpload:) forControlEvents:UIControlEventTouchDown];
//                    cell.docBtn.tag=rowNumber+25000;
//                    [cell.docBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                    
                }
                else if ([strStat isEqualToString:@"7"])
                {
                    [cell.docBtn addTarget:self action:@selector(downloadDoc:) forControlEvents:UIControlEventTouchUpInside];
                    cell.docBtn.tag=rowNumber+51000;
                }
                else
                {
                    cell.docBtn.tag=rowNumber+30000;
                    //                [cell.docBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                    [cell.docBtn addTarget:self action:@selector(downloadImageOrVideo:) forControlEvents:UIControlEventTouchDown];
                    
                    NSLog(@"%ld",(long)cell.docBtn.tag);
                    
                }
            }
        }
        else if ([cell.message.type isEqualToString:@"sticker"])
        {
            [cell.bubbleImage setHidden:YES];

            NSLog(@"%@",cell.message.text);
            [cell fillWithStickerMessage: cell.message.text downloaded: [self.stickerController isStickerPackDownloaded: cell.message.text]];
        }
    else
    {
        
//        NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:@"(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))" options:NSRegularExpressionCaseInsensitive error:NULL];
//        NSString *someString = cell.message.text;
//        NSString *match = [someString substringWithRange:[expression rangeOfFirstMatchInString:someString options:NSMatchingCompleted range:NSMakeRange(0, [someString length])]];
//        NSLog(@"%@", match);
        
        
        if ([self validateUrl:cell.message.text]){
        
            if (cell.message.sender==MessageSenderMyself)
            {
            [cell.LinkBtn addTarget:self action:@selector(ShowPage:) forControlEvents:UIControlEventTouchUpInside];
            cell.LinkBtn.tag=rowNumber+91000;
            }
            else
            {
                [cell.LinkBtn addTarget:self action:@selector(ShowPage:) forControlEvents:UIControlEventTouchUpInside];
                cell.LinkBtn.tag=rowNumber+91000;
            }
        }
        else
        {
        }
    }
        return cell;
        
   // }

   

    // return cell;
}

- (void)ShowPage:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int nPos=btn.tag;
    nPos=nPos-91000;
    
    Message *msg = [arrWholetableArray objectAtIndex:nPos];
    NSLog(@"%@",msg);
    NSString *getLink = msg.text;
    NSURL *url = [NSURL URLWithString:getLink];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
    
    
}
//LongPress
- (BOOL) canBecomeFirstResponder {
    return YES;
}

- (BOOL) canPerformAction:(SEL)action withSender:(id)sender
{
            if (action == @selector(copy:))
            {
        
                return NO;
        
            }
    
    else if (action == @selector(cut:))
    {
        
        return NO;
        
    }
    else if (action == @selector(paste:))
    {
        
        return NO;
        
    }
    else if (action == @selector(select:))
    {
        
        return NO;
        
    }
     else if (action == @selector(selectAll:))
    {
        
        return NO;
        
    }
    
    else if (action == @selector(Delete:))
    {
        
        return YES;

    }
    else if (action == @selector(star:))
    {
        return YES;
    }
    else if (action == @selector(info:))
    {
        return YES;
    }
    else if (action == @selector(Copied:))
    {
        return YES;
    }
       // }
    return [super canPerformAction:action withSender:sender];
    
    
}
-(void)Copied:(id)sender
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    NSInteger rowNumber = 0;
    for (NSInteger i = 0; i < deleteIndexPth.section; i++) {
        rowNumber += [self tableView:self.tableSingleChat numberOfRowsInSection:i];
    }

    rowNumber +=deleteIndexPth.row;
    NSDictionary *getlastInfo=[[NSDictionary alloc]init];
    NSArray *arrChatMsgs=[[NSArray alloc]init];
    getlastInfo = [arrWholetableArray objectAtIndex:rowNumber];
    /*
     //Pagination
    if ([strChatRoomType isEqualToString:@"1"])
    {
        arrChatMsgs = [db_class getGroupMessages:strZoeChatId];
        NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
        if (arrChatMsgs.count > 10)
        {
            NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
            getlastInfo = [reversed objectAtIndex:rowNumber];
            
        }
        else{
            getlastInfo = [arrWholetableArray objectAtIndex:rowNumber];
            
        }
    }
    else
    {
        arrChatMsgs = [db_class getOneToOneChatMessages:strZoeChatId];
        NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
        if (arrChatMsgs.count > 10)
        {
            NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
            getlastInfo = [reversed objectAtIndex:rowNumber];
            
        }
        else{
            getlastInfo = [arrWholetableArray objectAtIndex:rowNumber];
            
        }
    }
*/
    
    NSLog(@"%@",getlastInfo);
    NSString *getlastMsg = [getlastInfo valueForKey:@"text"];
    pasteboard.string = getlastMsg;
}
- (void)Delete:(id)sender {
    NSLog(@"Cell was flagged");
    
            NSInteger rowNumber = 0;
    
            for (NSInteger i = 0; i < deleteIndexPth.section; i++) {
                rowNumber += [self tableView:self.tableSingleChat numberOfRowsInSection:i];
            }
            rowNumber += deleteIndexPth.row;
    Message *msg;
    NSArray *arrChatMsgs=[[NSArray alloc]init];
    msg = [arrWholetableArray objectAtIndex:rowNumber];

/*
 //Pagination
    if ([strChatRoomType isEqualToString:@"1"])
    {
        arrChatMsgs = [db_class getGroupMessages:strZoeChatId];
        NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
        if (arrChatMsgs.count > 10)
        {
            NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
            msg = [reversed objectAtIndex:rowNumber];
            
        }
        else{
            msg = [arrWholetableArray objectAtIndex:rowNumber];
            
        }
    }
    else
    {
        arrChatMsgs = [db_class getOneToOneChatMessages:strZoeChatId];
        NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
        if (arrChatMsgs.count > 10)
        {
            NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
            msg = [reversed objectAtIndex:rowNumber];
            
        }
        else{
            msg = [arrWholetableArray objectAtIndex:rowNumber];
            
        }
    }
    */

    
            NSLog(@"%@",msg.chat_id);
            BOOL Success1=[db_class deleteMessages:msg.chat_id];
        NSLog(@"%hhd",Success1);
    [self.tableArray removeObject:msg];
    [arrWholetableArray removeObject:msg];
    [_tableSingleChat reloadData];
    NSArray *arrChatMsgs1 = [db_class getOneToOneChatMessages:strZoeChatId];
    NSDictionary *getlastInfo = [arrChatMsgs1 lastObject];
    NSLog(@"%@",getlastInfo);
    NSString *getsender = [getlastInfo valueForKey:@"sender"];
    NSString *getlastMsg = [getlastInfo valueForKey:@"content"];
    NSString *getlastMsgStatus = [getlastInfo valueForKey:@"contentStatus"];
    NSString *getlastMsgTime = [getlastInfo valueForKey:@"sentTime"];
    NSString *getlastMsgType = [getlastInfo valueForKey:@"contentType"];
   BOOL success=[db_class updateLastMessage:strZoeChatId sender:getsender lastMessage:getlastMsg lastMessageStatus:getlastMsgStatus lastMessageTime:getlastMsgTime lastMessagesType:getlastMsgType unReadCount:@"0"];
    NSLog(@"%hhd",success);

    
}
- (UIImage *)thumbnailFromImage:(UIImage *)source
{
    CGSize originalSize = source.size;
    CGSize destSize = CGSizeMake(originalSize.width / 3, originalSize.height / 3);
    
    UIGraphicsBeginImageContext(destSize);
    [ source drawInRect:(CGRect){CGPointZero, destSize}];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
- (void)star:(id)sender {
    NSLog(@"Cell was approved");
    NSLog(@"%@",strZoeChatId);
    
    NSInteger rowNumber = 0;
    
    for (NSInteger i = 0; i < deleteIndexPth.section; i++) {
        rowNumber += [self tableView:self.tableSingleChat numberOfRowsInSection:i];
    }
    rowNumber += deleteIndexPth.row;
    Message *msg;
    
    NSArray *arrChatMsgs=[[NSArray alloc]init];
    msg = [arrWholetableArray objectAtIndex:rowNumber];
    msg.checkStar = @"1";
    
    [[arrWholetableArray objectAtIndex:rowNumber] setValue:@"1" forKey:@"checkStar"];
    BOOL success=[db_class updateStarredMessage:msg.chat_id checkStar:@"1"];

    /*
     //Pagination
    if ([strChatRoomType isEqualToString:@"1"])
    {
        arrChatMsgs = [db_class getGroupMessages:strZoeChatId];
        NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
        if (arrChatMsgs.count > 10)
        {
            NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
            msg = [reversed objectAtIndex:rowNumber];
            msg.checkStar = @"1";
            
            [[reversed objectAtIndex:rowNumber] setValue:@"1" forKey:@"checkStar"];
            
        }
        else{
            msg = [arrWholetableArray objectAtIndex:rowNumber];
            msg.checkStar = @"1";
            
            [[arrWholetableArray objectAtIndex:rowNumber] setValue:@"1" forKey:@"checkStar"];
            
        }
        BOOL success=[db_class updateGroupStarredMessage:msg.chat_id checkStar:@"1"];
    }
    else
    {
        arrChatMsgs = [db_class getOneToOneChatMessages:strZoeChatId];
        NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
        if (arrChatMsgs.count > 10)
        {
            NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
            msg = [reversed objectAtIndex:rowNumber];
            msg.checkStar = @"1";
            
            [[reversed objectAtIndex:rowNumber] setValue:@"1" forKey:@"checkStar"];
        }
        else{
            msg = [arrWholetableArray objectAtIndex:rowNumber];
            msg.checkStar = @"1";
            
            [[arrWholetableArray objectAtIndex:rowNumber] setValue:@"1" forKey:@"checkStar"];
            
        }
     
        BOOL success=[db_class updateStarredMessage:msg.chat_id checkStar:@"1"];

    }
     */

    NSLog(@"%@",msg.chat_id);
//    msg.checkStar = @"1";
//
//    [[arrWholetableArray objectAtIndex:rowNumber] setValue:@"1" forKey:@"checkStar"];
    
    MessageCell *cell = [self.tableSingleChat cellForRowAtIndexPath:deleteIndexPth];
    cell.StarimgView.image = [UIImage imageNamed:@"FilledStar"];

    
}
- (void)starred:(id)sender {
    NSLog(@"Cell was approved");
    
    NSInteger rowNumber = 0;
    
    for (NSInteger i = 0; i < deleteIndexPth.section; i++) {
        rowNumber += [self tableView:self.tableSingleChat numberOfRowsInSection:i];
    }
    rowNumber += deleteIndexPth.row;
    Message *msg;
    msg = [arrWholetableArray objectAtIndex:rowNumber];
    [[arrWholetableArray objectAtIndex:rowNumber] setValue:@"0" forKey:@"checkStar"];
    msg.checkStar = @"0";
    BOOL success=[db_class updateStarredMessage:msg.chat_id checkStar:@"0"];
    /*
     //Pagination
    NSArray *arrChatMsgs=[[NSArray alloc]init];
    if ([strChatRoomType isEqualToString:@"1"])
    {
        arrChatMsgs = [db_class getGroupMessages:strZoeChatId];
        NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
        if (arrChatMsgs.count > 10)
        {
            NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
            msg = [reversed objectAtIndex:rowNumber];
            [[reversed objectAtIndex:rowNumber] setValue:@"0" forKey:@"checkStar"];
            msg.checkStar = @"0";
            
        }
        else{
            msg = [arrWholetableArray objectAtIndex:rowNumber];
            [[arrWholetableArray objectAtIndex:rowNumber] setValue:@"0" forKey:@"checkStar"];
            msg.checkStar = @"0";
            
        }
        BOOL success=[db_class updateGroupStarredMessage:msg.chat_id checkStar:@"0"];

    }
    else
    {
        arrChatMsgs = [db_class getOneToOneChatMessages:strZoeChatId];
        NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
        if (arrChatMsgs.count > 10)
        {
            NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
            msg = [reversed objectAtIndex:rowNumber];
            [[reversed objectAtIndex:rowNumber] setValue:@"0" forKey:@"checkStar"];
            msg.checkStar = @"0";
            
        }
        else{
            msg = [arrWholetableArray objectAtIndex:rowNumber];
            [[arrWholetableArray objectAtIndex:rowNumber] setValue:@"0" forKey:@"checkStar"];
            msg.checkStar = @"0";
            
        }
        BOOL success=[db_class updateStarredMessage:msg.chat_id checkStar:@"0"];

    }
     */

    NSLog(@"%@",msg.chat_id);

    MessageCell *cell = [self.tableSingleChat cellForRowAtIndexPath:deleteIndexPth];
    cell.StarimgView.image = [UIImage imageNamed:@""];
    
}
- (void)forwardMsg:(id)sender
{
    NSInteger rowNumber = 0;
    
    for (NSInteger i = 0; i < deleteIndexPth.section; i++) {
        rowNumber += [self tableView:self.tableSingleChat numberOfRowsInSection:i];
    }
    rowNumber += deleteIndexPth.row;
    Message *msg;
     msg = [arrWholetableArray objectAtIndex:rowNumber];
/*
 //Pagination
    NSArray *arrChatMsgs=[[NSArray alloc]init];
    if ([strChatRoomType isEqualToString:@"1"])
    {
        arrChatMsgs = [db_class getGroupMessages:strZoeChatId];
        NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
        if (arrChatMsgs.count > 10)
        {
            NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
            msg = [reversed objectAtIndex:rowNumber];
            
        }
        else{
            msg = [arrWholetableArray objectAtIndex:rowNumber];
            
        }
    }
    else
    {
        arrChatMsgs = [db_class getOneToOneChatMessages:strZoeChatId];
        NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
        if (arrChatMsgs.count > 10)
        {
            NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
            msg = [reversed objectAtIndex:rowNumber];
            
        }
        else{
            msg = [arrWholetableArray objectAtIndex:rowNumber];
            
        }
    }
*/
    NSLog(@"%@",msg.chat_id);
    NSLog(@"%@",msg.type);
    if ([msg.type isEqualToString:@"contact"]) {
        
        msg.text = @"Contact";

        
    }
    
    else{
    NSLog(@"%@",msg.text);
    }
    [self performSegueWithIdentifier:@"ToForwardMsg" sender:self];
    
}
- (void)info:(id)sender {
    NSLog(@"Cell was denied");
    NSInteger rowNumber = 0;
    
    for (NSInteger i = 0; i < deleteIndexPth.section; i++) {
        rowNumber += [self tableView:self.tableSingleChat numberOfRowsInSection:i];
    }
    rowNumber += deleteIndexPth.row;
    Message *msg;
    msg = [arrWholetableArray objectAtIndex:rowNumber];

    /*
     //Pagination
    NSArray *arrChatMsgs=[[NSArray alloc]init];
    if ([strChatRoomType isEqualToString:@"1"])
    {
        arrChatMsgs = [db_class getGroupMessages:strZoeChatId];
        NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
        if (arrChatMsgs.count > 10)
        {
            NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
            msg = [reversed objectAtIndex:rowNumber];
            
        }
        else{
            msg = [arrWholetableArray objectAtIndex:rowNumber];
            
        }
    }
    else
    {
        arrChatMsgs = [db_class getOneToOneChatMessages:strZoeChatId];
        NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
        if (arrChatMsgs.count > 10)
        {
            NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
            msg = [reversed objectAtIndex:rowNumber];
            
        }
        else{
            msg = [arrWholetableArray objectAtIndex:rowNumber];

        }
    }
    */
    NSLog(@"%@",msg.seenTime);
    NSLog(@"%@",msg.deliveredTime);
    NSLog(@"%ld",(long)msg.status);
    NSString *getMsgStatus;

    if (msg.status == 0)
    {
        getMsgStatus=@"sending";
    }
    else if (msg.status == 1)
    {
        getMsgStatus=@"sent";

    }
    else if (msg.status == 2)
    {
        getMsgStatus=@"delivered";

    }
    else if (msg.status == 3)
    {
        getMsgStatus=@"read";

    }
    else if (msg.status == 7)
    {
        getMsgStatus=@"downloaded";
        
    }
     [self MessageInfoView];
    [self.statusLabel setText:getMsgStatus];
    if ([msg.seenTime isEqualToString:@"0"])
    {
        [self.statusTimeLabel setText:@"-"];

    }
    else
    {
        //Seen Time
        double seenTime = [msg.seenTime doubleValue];
        NSDate *Seendate = [NSDate dateWithTimeIntervalSince1970:(seenTime / 1000.0)];
        NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
        [dateFormat1 setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
        [dateFormat1 setTimeZone:[NSTimeZone systemTimeZone]];
        NSString *finalDate1 = [dateFormat1 stringFromDate:Seendate];
        NSLog(@"finalDate==%@",finalDate1);

        [self.statusTimeLabel setText:finalDate1];

    }
    [self.DeliveredLabel setText:@"Delivered"];
    if ([msg.deliveredTime isEqualToString:@"0"])
    {
        [self.DeliveredTimeLabel setText:@"-"];
        
    }
    else
    {
        //Delivered Time
        double deliTime = [msg.deliveredTime doubleValue];
        NSDate *Delivereddate = [NSDate dateWithTimeIntervalSince1970:(deliTime / 1000.0)];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
        [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
        NSString *finalDate = [dateFormat stringFromDate:Delivereddate];
        NSLog(@"finalDate==%@",finalDate);

        [self.DeliveredTimeLabel setText:finalDate];
        
    }

    
    if ([getMsgStatus  isEqual: @"sending"])
    {
        self.StatusImgview.image = [UIImage imageNamed:@"status_sending"];
    }
    else  if ([getMsgStatus  isEqual: @"sent"])
    {
        self.StatusImgview.image = [UIImage imageNamed:@"status_sent"];
    }
    else  if ([getMsgStatus  isEqual: @"read"])
    {
        self.StatusImgview.image = [UIImage imageNamed:@"status_read"];
    }
    else  if ([getMsgStatus  isEqual: @"delivered"])
    {
        self.StatusImgview.image = [UIImage imageNamed:@"delivered"];
    }
    else  if ([getMsgStatus  isEqual: @"downloaded"])
    {
        self.StatusImgview.image = [UIImage imageNamed:@"delivered"];
    }
    
    _deliveredImgview.image = [UIImage imageNamed:@"delivered"];
    
}
-(void)autoDownloaded:(int)getIndex
{
    
    Message *message = [arrWholetableArray objectAtIndex:getIndex];
    message.status=MessageStatusDownloading;
    
    DGActivityIndicatorView *activityIndicatorView;
    NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
    MessageCell *cell = (MessageCell *)[self.tableSingleChat cellForRowAtIndexPath:indexPath];
    
    UIButton *btnDownload=(UIButton *)[cell viewWithTag:getIndex];
    [btnDownload removeFromSuperview];
    
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        
        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:color size:50.0f];
        
        
        
        
        
    }
    else
    {
        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor whiteColor] size:50.0f];
        
        
        
        
        
        
    }

    activityIndicatorView.frame = CGRectMake((cell.imgView.frame.size.width/2)-25, (cell.imgView.frame.size.height/2)-25, 50, 50);
    activityIndicatorView.backgroundColor=[UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:0.5];
    activityIndicatorView.layer.cornerRadius=25;
    activityIndicatorView.clipsToBounds=YES;
    [cell.imgView addSubview:activityIndicatorView];
    activityIndicatorView.tag=getIndex;
    [activityIndicatorView startAnimating];
    
    
    UIButton *btnCancelDownload=[[UIButton alloc]initWithFrame:CGRectMake(cell.imgView.center.x-12.5, cell.imgView.center.y-12.5, 25, 25)];
    btnCancelDownload.tag=getIndex;
    [btnCancelDownload setBackgroundImage:[UIImage imageNamed:@"CloseIcon.png"] forState:UIControlStateNormal];
    [btnCancelDownload addTarget:self action:@selector(cancelDownload:) forControlEvents:UIControlEventTouchUpInside];
    [cell.contentView addSubview:btnCancelDownload];
    
    
    BOOL success=[db_class updateContentStatus:message.chat_id contentStatus:@"downloading"];
    
    if ([strCurrentlyDownloading isEqualToString:@""])
    {
        [self startDownloadImageOrVideo];
    }

}

- (void)longPress:(UILongPressGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        MessageCell *cell = (MessageCell *)recognizer.view;
        [cell becomeFirstResponder];
        
        UIMenuItem *flag = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(Delete:)];
        UIMenuItem *approve = [[UIMenuItem alloc] initWithTitle:@"Info" action:@selector(info:)];
        UIMenuItem *deny = [[UIMenuItem alloc] initWithTitle:@"Star" action:@selector(star:)];
        
        UIMenuController *menu = [UIMenuController sharedMenuController];
        [menu setMenuItems:[NSArray arrayWithObjects:flag, approve, deny, nil]];
        [menu setTargetRect:cell.frame inView:cell.superview];
        [menu setMenuVisible:YES animated:YES];
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Message *message = [self.tableArray objectAtIndexPath:indexPath];
    NSLog(@"%f",message.heigh);
    return message.heigh;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    return [self.tableArray titleForSection:section];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGRect frame = CGRectMake(0, 0, tableView.frame.size.width, 40);
    
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.backgroundColor = [UIColor clearColor];
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UILabel *label = [[UILabel alloc] init];
    label.text = [self tableView:tableView titleForHeaderInSection:section];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:FONT_NORMAL size:20.0];
    [label sizeToFit];
    label.center = view.center;
    label.font = [UIFont fontWithName:FONT_NORMAL size:13.0];
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        label.backgroundColor = color;
        label.textColor=[UIColor whiteColor];
        
        
        
    }
    else
    {
        label.backgroundColor = [UIColor colorWithRed:207/255.0 green:220/255.0 blue:252.0/255.0 alpha:1];

    }
    label.layer.cornerRadius = 10;
    label.layer.masksToBounds = YES;
    label.autoresizingMask = UIViewAutoresizingNone;
    [view addSubview:label];
    return view;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%d",indexPath.row);
    MessageCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSInteger rowNumber = 0;
    for (NSInteger i = 0; i < indexPath.section; i++) {
        rowNumber += [self tableView:tableView numberOfRowsInSection:i];
    }
    rowNumber += indexPath.row;
    deleteIndexPth = indexPath;
     NSLog(@" IndexPaths:%ld",(long)deleteIndexPth);
    NSLog(@" IndexPaths:%ld",(long)deleteIndexPth.row);
    NSLog(@" IndexPaths:%ld",(long)deleteIndexPth.section);
    
    
    NSArray *arrChatMsgs=[[NSArray alloc]init];
    NSDictionary *getlastInfo=[[NSDictionary alloc]init];
    getlastInfo = [arrWholetableArray objectAtIndex:rowNumber];

    /*
    if ([strChatRoomType isEqualToString:@"1"])
    {
        arrChatMsgs = [db_class getGroupMessages:strZoeChatId];
        NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
        if (arrChatMsgs.count > 10)
        {
            NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
            getlastInfo = [reversed objectAtIndex:rowNumber];
            
        }
        else{
            getlastInfo = [arrWholetableArray objectAtIndex:rowNumber];
            
        }
    }
    else
    {
        arrChatMsgs = [db_class getOneToOneChatMessages:strZoeChatId];
        NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
        if (arrChatMsgs.count > 10)
        {
            NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
            getlastInfo = [reversed objectAtIndex:rowNumber];
            
        }
        else{
            getlastInfo = [arrWholetableArray objectAtIndex:rowNumber];
            
        }
    }
     */

//    NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
//    NSDictionary *getlastInfo = [reversed objectAtIndex:rowNumber];
//    NSLog(@"%@",getlastInfo);
    getchatRoomType = [dictDetails valueForKey:@"chatRoomType"];
    if([getchatRoomType isEqualToString:@"1"])
    {
    NSString *checkStar = [getlastInfo valueForKey:@"checkStar"];
    NSString *getType = [getlastInfo valueForKey:@"type"];
    NSString *getsender = [[getlastInfo valueForKey:@"sender"] stringValue];

    if ([getsender isEqualToString:@"0"])
    {
        if ([getType  isEqual: @"text"])
    {
    if([checkStar  isEqual:@"1"])
    {
        [cell becomeFirstResponder];
        
        UIMenuItem *delete = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(Delete:)];
        //UIMenuItem *info = [[UIMenuItem alloc] initWithTitle:@"Info" action:@selector(info:)];
        UIMenuItem *star = [[UIMenuItem alloc] initWithTitle:@"Unstar" action:@selector(starred:)];
        UIMenuItem *copied = [[UIMenuItem alloc] initWithTitle:@"Copy" action:@selector(Copied:)];
        UIMenuItem *forward = [[UIMenuItem alloc] initWithTitle:@"Forward" action:@selector(forwardMsg:)];
        UIMenuController *menu = [UIMenuController sharedMenuController];
        [menu setMenuItems:[NSArray arrayWithObjects:delete, star,copied,forward, nil]];
        [menu setTargetRect:cell.frame inView:cell.superview];
        [menu setMenuVisible:YES animated:YES];

    }
   
    else{
        [cell becomeFirstResponder];
        
        UIMenuItem *delete = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(Delete:)];
       // UIMenuItem *info = [[UIMenuItem alloc] initWithTitle:@"Info" action:@selector(info:)];
        UIMenuItem *star = [[UIMenuItem alloc] initWithTitle:@"Star" action:@selector(star:)];
        UIMenuItem *copied = [[UIMenuItem alloc] initWithTitle:@"Copy" action:@selector(Copied:)];
        UIMenuItem *forward = [[UIMenuItem alloc] initWithTitle:@"Forward" action:@selector(forwardMsg:)];
        UIMenuController *menu = [UIMenuController sharedMenuController];
        [menu setMenuItems:[NSArray arrayWithObjects:delete,  star,copied,forward, nil]];
        [menu setTargetRect:cell.frame inView:cell.superview];
        [menu setMenuVisible:YES animated:YES];

    }
    }
    else
    {
        if([checkStar  isEqual:@"1"])
        {
            [cell becomeFirstResponder];
            
            UIMenuItem *flag = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(Delete:)];
            //UIMenuItem *approve = [[UIMenuItem alloc] initWithTitle:@"Info" action:@selector(info:)];
            UIMenuItem *deny = [[UIMenuItem alloc] initWithTitle:@"Unstar" action:@selector(starred:)];
           UIMenuItem *forward = [[UIMenuItem alloc] initWithTitle:@"Forward" action:@selector(forwardMsg:)];
            UIMenuController *menu = [UIMenuController sharedMenuController];
            [menu setMenuItems:[NSArray arrayWithObjects:flag,  deny,forward, nil]];
            [menu setTargetRect:cell.frame inView:cell.superview];
            [menu setMenuVisible:YES animated:YES];
            
        }
        
        else{
            [cell becomeFirstResponder];
            
            UIMenuItem *flag = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(Delete:)];
            //UIMenuItem *approve = [[UIMenuItem alloc] initWithTitle:@"Info" action:@selector(info:)];
            UIMenuItem *deny = [[UIMenuItem alloc] initWithTitle:@"Star" action:@selector(star:)];
            UIMenuItem *forward = [[UIMenuItem alloc] initWithTitle:@"Forward" action:@selector(forwardMsg:)];
                       UIMenuController *menu = [UIMenuController sharedMenuController];
            [menu setMenuItems:[NSArray arrayWithObjects:flag, deny,forward, nil]];
            [menu setTargetRect:cell.frame inView:cell.superview];
            [menu setMenuVisible:YES animated:YES];
            
        }
    }
    }
    else
    {
        if ([getType  isEqual: @"text"])
        {
            if([checkStar  isEqual:@"1"])
            {
                [cell becomeFirstResponder];
                
                UIMenuItem *flag = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(Delete:)];
//                UIMenuItem *approve = [[UIMenuItem alloc] initWithTitle:@"Info" action:@selector(info:)];
                UIMenuItem *deny = [[UIMenuItem alloc] initWithTitle:@"Unstar" action:@selector(starred:)];
                UIMenuItem *copied = [[UIMenuItem alloc] initWithTitle:@"Copy" action:@selector(Copied:)];
                UIMenuItem *forward = [[UIMenuItem alloc] initWithTitle:@"Forward" action:@selector(forwardMsg:)];
                UIMenuController *menu = [UIMenuController sharedMenuController];
                [menu setMenuItems:[NSArray arrayWithObjects:flag, deny,copied,forward, nil]];
                [menu setTargetRect:cell.frame inView:cell.superview];
                [menu setMenuVisible:YES animated:YES];
                
            }
            
            else{
                [cell becomeFirstResponder];
                
                UIMenuItem *flag = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(Delete:)];
//                UIMenuItem *approve = [[UIMenuItem alloc] initWithTitle:@"Info" action:@selector(info:)];
                UIMenuItem *deny = [[UIMenuItem alloc] initWithTitle:@"Star" action:@selector(star:)];
                UIMenuItem *copied = [[UIMenuItem alloc] initWithTitle:@"Copy" action:@selector(Copied:)];
                UIMenuItem *forward = [[UIMenuItem alloc] initWithTitle:@"Forward" action:@selector(forwardMsg:)];
                UIMenuController *menu = [UIMenuController sharedMenuController];
                [menu setMenuItems:[NSArray arrayWithObjects:flag, deny,copied,forward, nil]];        [menu setTargetRect:cell.frame inView:cell.superview];
                [menu setMenuVisible:YES animated:YES];
                
            }
        }
        else
        {
            if([checkStar  isEqual:@"1"])
            {
                [cell becomeFirstResponder];
                
                UIMenuItem *flag = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(Delete:)];
//                UIMenuItem *approve = [[UIMenuItem alloc] initWithTitle:@"Info" action:@selector(info:)];
                UIMenuItem *deny = [[UIMenuItem alloc] initWithTitle:@"Unstar" action:@selector(starred:)];
                UIMenuItem *forward = [[UIMenuItem alloc] initWithTitle:@"Forward" action:@selector(forwardMsg:)];
                UIMenuController *menu = [UIMenuController sharedMenuController];
                [menu setMenuItems:[NSArray arrayWithObjects:flag, deny,forward, nil]];
                [menu setTargetRect:cell.frame inView:cell.superview];
                [menu setMenuVisible:YES animated:YES];
                
            }
            
            else{
                [cell becomeFirstResponder];
                
                UIMenuItem *flag = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(Delete:)];
//                UIMenuItem *approve = [[UIMenuItem alloc] initWithTitle:@"Info" action:@selector(info:)];
                UIMenuItem *deny = [[UIMenuItem alloc] initWithTitle:@"Star" action:@selector(star:)];
                UIMenuItem *forward = [[UIMenuItem alloc] initWithTitle:@"Forward" action:@selector(forwardMsg:)];
                UIMenuController *menu = [UIMenuController sharedMenuController];
                [menu setMenuItems:[NSArray arrayWithObjects:flag, deny,forward, nil]];
                [menu setTargetRect:cell.frame inView:cell.superview];
                [menu setMenuVisible:YES animated:YES];
                
            }
        }
    }
    }
    else
    {
        NSString *checkStar = [getlastInfo valueForKey:@"checkStar"];
        NSString *getType = [getlastInfo valueForKey:@"type"];
        NSString *getsender = [[getlastInfo valueForKey:@"sender"] stringValue];
        
        if ([getsender isEqualToString:@"0"])
        {
            if ([getType  isEqual: @"text"])
            {
                if([checkStar  isEqual:@"1"])
                {
                    [cell becomeFirstResponder];
                    
                    UIMenuItem *delete = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(Delete:)];
                    UIMenuItem *info = [[UIMenuItem alloc] initWithTitle:@"Info" action:@selector(info:)];
                    UIMenuItem *star = [[UIMenuItem alloc] initWithTitle:@"Unstar" action:@selector(starred:)];
                    UIMenuItem *copied = [[UIMenuItem alloc] initWithTitle:@"Copy" action:@selector(Copied:)];
                    UIMenuItem *forward = [[UIMenuItem alloc] initWithTitle:@"Forward" action:@selector(forwardMsg:)];
                    UIMenuController *menu = [UIMenuController sharedMenuController];
                    [menu setMenuItems:[NSArray arrayWithObjects:delete, info, star,copied,forward, nil]];
                    [menu setTargetRect:cell.frame inView:cell.superview];
                    [menu setMenuVisible:YES animated:YES];
                    
                }
                
                else{
                    [cell becomeFirstResponder];
                    
                    UIMenuItem *delete = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(Delete:)];
                    UIMenuItem *info = [[UIMenuItem alloc] initWithTitle:@"Info" action:@selector(info:)];
                    UIMenuItem *star = [[UIMenuItem alloc] initWithTitle:@"Star" action:@selector(star:)];
                    UIMenuItem *copied = [[UIMenuItem alloc] initWithTitle:@"Copy" action:@selector(Copied:)];
                    UIMenuItem *forward = [[UIMenuItem alloc] initWithTitle:@"Forward" action:@selector(forwardMsg:)];
                    UIMenuController *menu = [UIMenuController sharedMenuController];
                    [menu setMenuItems:[NSArray arrayWithObjects:delete, info, star,copied,forward, nil]];        [menu setTargetRect:cell.frame inView:cell.superview];
                    [menu setMenuVisible:YES animated:YES];
                    
                }
            }
            else
            {
                if([checkStar  isEqual:@"1"])
                {
                    [cell becomeFirstResponder];
                    
                    UIMenuItem *flag = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(Delete:)];
                    UIMenuItem *approve = [[UIMenuItem alloc] initWithTitle:@"Info" action:@selector(info:)];
                    UIMenuItem *deny = [[UIMenuItem alloc] initWithTitle:@"Unstar" action:@selector(starred:)];
                    UIMenuItem *forward = [[UIMenuItem alloc] initWithTitle:@"Forward" action:@selector(forwardMsg:)];
                    UIMenuController *menu = [UIMenuController sharedMenuController];
                    [menu setMenuItems:[NSArray arrayWithObjects:flag, approve, deny,forward, nil]];
                    [menu setTargetRect:cell.frame inView:cell.superview];
                    [menu setMenuVisible:YES animated:YES];
                    
                }
                
                else{
                    [cell becomeFirstResponder];
                    
                    UIMenuItem *flag = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(Delete:)];
                    UIMenuItem *approve = [[UIMenuItem alloc] initWithTitle:@"Info" action:@selector(info:)];
                    UIMenuItem *deny = [[UIMenuItem alloc] initWithTitle:@"Star" action:@selector(star:)];
                    UIMenuItem *forward = [[UIMenuItem alloc] initWithTitle:@"Forward" action:@selector(forwardMsg:)];
                    UIMenuController *menu = [UIMenuController sharedMenuController];
                    [menu setMenuItems:[NSArray arrayWithObjects:flag, approve, deny,forward, nil]];        [menu setTargetRect:cell.frame inView:cell.superview];
                    [menu setMenuVisible:YES animated:YES];
                    
                }
            }
        }
        else
        {
            if ([getType  isEqual: @"text"])
            {
                if([checkStar  isEqual:@"1"])
                {
                    [cell becomeFirstResponder];
                    
                    UIMenuItem *flag = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(Delete:)];
                    //                UIMenuItem *approve = [[UIMenuItem alloc] initWithTitle:@"Info" action:@selector(info:)];
                    UIMenuItem *deny = [[UIMenuItem alloc] initWithTitle:@"Unstar" action:@selector(starred:)];
                    UIMenuItem *copied = [[UIMenuItem alloc] initWithTitle:@"Copy" action:@selector(Copied:)];
                    UIMenuItem *forward = [[UIMenuItem alloc] initWithTitle:@"Forward" action:@selector(forwardMsg:)];
                    UIMenuController *menu = [UIMenuController sharedMenuController];
                    [menu setMenuItems:[NSArray arrayWithObjects:flag, deny,copied,forward, nil]];
                    [menu setTargetRect:cell.frame inView:cell.superview];
                    [menu setMenuVisible:YES animated:YES];
                    
                }
                
                else{
                    [cell becomeFirstResponder];
                    
                    UIMenuItem *flag = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(Delete:)];
                    //                UIMenuItem *approve = [[UIMenuItem alloc] initWithTitle:@"Info" action:@selector(info:)];
                    UIMenuItem *deny = [[UIMenuItem alloc] initWithTitle:@"Star" action:@selector(star:)];
                    UIMenuItem *copied = [[UIMenuItem alloc] initWithTitle:@"Copy" action:@selector(Copied:)];
                    UIMenuItem *forward = [[UIMenuItem alloc] initWithTitle:@"Forward" action:@selector(forwardMsg:)];
                    UIMenuController *menu = [UIMenuController sharedMenuController];
                    [menu setMenuItems:[NSArray arrayWithObjects:flag, deny,copied,forward, nil]];        [menu setTargetRect:cell.frame inView:cell.superview];
                    [menu setMenuVisible:YES animated:YES];
                    
                }
            }
            else
            {
                if([checkStar  isEqual:@"1"])
                {
                    [cell becomeFirstResponder];
                    
                    UIMenuItem *flag = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(Delete:)];
                    //                UIMenuItem *approve = [[UIMenuItem alloc] initWithTitle:@"Info" action:@selector(info:)];
                    UIMenuItem *deny = [[UIMenuItem alloc] initWithTitle:@"Unstar" action:@selector(starred:)];
                    UIMenuItem *forward = [[UIMenuItem alloc] initWithTitle:@"Forward" action:@selector(forwardMsg:)];
                    UIMenuController *menu = [UIMenuController sharedMenuController];
                    [menu setMenuItems:[NSArray arrayWithObjects:flag, deny,forward, nil]];
                    [menu setTargetRect:cell.frame inView:cell.superview];
                    [menu setMenuVisible:YES animated:YES];
                    
                }
                
                else{
                    [cell becomeFirstResponder];
                    
                    UIMenuItem *flag = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(Delete:)];
                    //                UIMenuItem *approve = [[UIMenuItem alloc] initWithTitle:@"Info" action:@selector(info:)];
                    UIMenuItem *deny = [[UIMenuItem alloc] initWithTitle:@"Star" action:@selector(star:)];
                    UIMenuItem *forward = [[UIMenuItem alloc] initWithTitle:@"Forward" action:@selector(forwardMsg:)];
                    UIMenuController *menu = [UIMenuController sharedMenuController];
                    [menu setMenuItems:[NSArray arrayWithObjects:flag, deny,forward, nil]];
                    [menu setTargetRect:cell.frame inView:cell.superview];
                    [menu setMenuVisible:YES animated:YES];
                    
                }
            }
        }
    }

 }




- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    // do something here
    MessageCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //[cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    //textView
//    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
}
- (void)tableViewScrollToBottomAnimated:(BOOL)animated
{
    NSInteger numberOfSections = [self.tableArray numberOfSections];
    NSInteger numberOfRows = [self.tableArray numberOfMessagesInSection:numberOfSections-1];
    if (numberOfRows)
    {
        [_tableSingleChat scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                          atScrollPosition:UITableViewScrollPositionBottom animated:animated];
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    UITableView *tableView = (UITableView *)gestureRecognizer.view;
    CGPoint p = [gestureRecognizer locationInView:gestureRecognizer.view];
    if ([tableView indexPathForRowAtPoint:p]) {
        return YES;
    }
    return NO;
}
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)sender successfully:(BOOL)success
{
    if (success)
    {
        [myAudioPlayer stop];

        [_PlayPauseButton setBackgroundImage:[UIImage imageNamed:@"PlayButton.png"] forState:UIControlStateNormal];

    }
}
-(void)sliderAction:(id)sender
{
    UISlider *slider = (UISlider*)sender;
    NSInteger value = lroundf(slider.value);

}
- (void)playAudioFile :(NSString *)fName :(int)tag
{
//    [myAudioPlayer stop];
    tag=tag+51000;
    strPlayingTag=[NSString stringWithFormat:@"%d",tag]; // 51000
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *getPath = [documentsDirectory stringByAppendingPathComponent:@""];
    NSURL *audioURL = [NSURL fileURLWithPath:getPath];
    
    NSData *audioData = [NSData dataWithContentsOfURL:audioURL];
    NSString *docDirPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", docDirPath , fName];
    [audioData writeToFile:filePath atomically:YES];
    
    NSURL *fileURL = [NSURL fileURLWithPath:filePath];

    AVPlayer *player = [AVPlayer playerWithURL:fileURL];
    
    // create a player view controller
    AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
    [self presentViewController:controller animated:YES completion:nil];
    controller.player = player;
    [player play];
    
    
//       NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *getPath = [documentsDirectory stringByAppendingPathComponent:fName];
//    
//    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:getPath ];
//    
//    myAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
//     //myAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:NULL];
//
//    self.movingslider.maximumValue = myAudioPlayer.duration;
//    NSLog(@"%f",myAudioPlayer.duration);
//    
//    
//    sliderTime = [NSTimer scheduledTimerWithTimeInterval:1.0f
//                                                  target:self
//                                                selector:@selector(updateSlider:)
//                                                userInfo:nil
//                                                 repeats:YES];
//    Timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
//                                             target:self
//                                           selector:@selector(updateTimer)
//                                           userInfo:nil
//                                            repeats:YES];
//    
//    NSTimeInterval currentTime = myAudioPlayer.currentTime;
//    NSInteger minutes = floor(currentTime/60);
//    NSInteger seconds = trunc(currentTime - minutes * 60);
//    //self.TimeLabel.text = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];
//    NSLog(@"TimeLabel:%@",[NSString stringWithFormat:@"%d:%02d", minutes, seconds]);
//
//  
//    myAudioPlayer.delegate=self;
//    NSError *sessionError = nil;
////    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
////    [audioSession setCategory:AVAudioSessionCategoryMultiRoute error:&sessionError];
////    BOOL success= [audioSession overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&sessionError];
////    [audioSession setActive:YES error:&sessionError];
////    if(!success)
////    {
////        NSLog(@"error doing outputaudioportoverride - %@", [sessionError localizedDescription]);
////    }
//    AVAudioSession *session = [AVAudioSession sharedInstance];
//    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
    
//   // myAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
////[myAudioPlayer setDelegate:self];
//    [myAudioPlayer prepareToPlay];
//    [myAudioPlayer play];
////    [myAudioPlayer setVolume:3.0f];
////    [myAudioPlayer play];
    
    
    
}

- (void)onAudioPlay:(id)sender
{
    [self.tableSingleChat reloadData];
    UIButton *btn=(UIButton *)sender;
    int nPos=btn.tag;
    nPos=nPos-51000;
//    NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
//    Message *message = [reversed objectAtIndex:nPos];

    Message *message = [arrWholetableArray objectAtIndex:nPos];
    NSString *strContent= message.text;
    
    NSArray *components = [strContent componentsSeparatedByString:@"/"];
    NSString *strAudioFileName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
    
    
    //[self CreateAudioPreview:strAudioFileName];

    [self playAudioFile:strAudioFileName: nPos];
    
   
}
- (void)downloadDoc:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int nPos=btn.tag;
    nPos=nPos-51000;
    
    Message *msg;
    msg = [arrWholetableArray objectAtIndex:nPos];

    /*
    NSArray *arrChatMsgs=[[NSArray alloc]init];
    if ([strChatRoomType isEqualToString:@"1"])
    {
        arrChatMsgs = [db_class getGroupMessages:strZoeChatId];
        NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
        if (arrChatMsgs.count > 10)
        {
            NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
            msg = [reversed objectAtIndex:nPos];
            
        }
        else{
            msg = [arrWholetableArray objectAtIndex:nPos];
            
        }
    }
    else
    {
        arrChatMsgs = [db_class getOneToOneChatMessages:strZoeChatId];
        NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
        if (arrChatMsgs.count > 10)
        {
            NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
            msg = [reversed objectAtIndex:nPos];
            
        }
        else{
            msg = [arrWholetableArray objectAtIndex:nPos];
            
        }
    }
*/
    
    NSLog(@"%@",msg);
    NSString *getdocument = msg.text;
    NSLog(@"%@",getdocument);
    UIStoryboard *storyboard =
    [UIStoryboard storyboardWithName:@"Main"
                              bundle:[NSBundle mainBundle]];
    LoadDocumentVC *yourViewController =
    [storyboard instantiateViewControllerWithIdentifier:@"LoadDocumentVC"];
    yourViewController.getWebStr = getdocument;
    [self.navigationController pushViewController:yourViewController animated:NO];
    
}
//MessageInfoView
-(void)MessageInfoView
{
    
    _FullView=[[UIView alloc]initWithFrame:CGRectMake(self.tableSingleChat.frame.origin.x, self.tableSingleChat.frame.origin.y, self.tableSingleChat.frame.size.width, self.tableSingleChat.frame.size.height)];
    _FullView.backgroundColor=[UIColor colorWithRed:179.0/255.0f green:179.0/255.0f blue:179.0/255.0f alpha:0.7];
    _FullView.tag=9087650;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onRemoveViewAudioVideo:)];
    [_FullView addGestureRecognizer:tap];
    
    
    _infoView=[[UIView alloc]initWithFrame:CGRectMake(15, 258, 255, 200)];
    [_infoView setBackgroundColor:[UIColor whiteColor]];
    _infoView.layer.cornerRadius = 5;
    _infoView.layer.masksToBounds = YES;
    _infoView.center = _FullView.center;
    //Img
    _StatusImgview = [[UIImageView alloc] initWithFrame:CGRectMake(30, 62, 15, 15)];
    [_infoView addSubview:_StatusImgview];
    _deliveredImgview = [[UIImageView alloc] initWithFrame:CGRectMake(30, 135, 15, 15)];
    [_infoView addSubview:_deliveredImgview];
    //StatusLbl
    _statusLabel=[[UILabel alloc]initWithFrame:CGRectMake(60, 53, 269, 32)];
    [_statusLabel setBackgroundColor:[UIColor clearColor]];
    [_statusLabel setFont: [_statusLabel.font fontWithSize: 15.0]];
    [_infoView addSubview:_statusLabel];
    
    _statusTimeLabel=[[UILabel alloc]initWithFrame:CGRectMake(60, 80, 269, 32)];
    [_statusTimeLabel setBackgroundColor:[UIColor clearColor]];
    [_statusTimeLabel setFont: [_statusTimeLabel.font fontWithSize: 15.0]];
    [_infoView addSubview:_statusTimeLabel];
    
    //BackBtn
    _backButton=[[UIButton alloc]initWithFrame:CGRectMake(15, 8, 20, 20)];
    [_backButton setBackgroundColor:[UIColor clearColor]];
    [_backButton setBackgroundImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];

    [_backButton addTarget:self action:@selector(Back) forControlEvents:UIControlEventTouchDown];
    [_infoView addSubview:_backButton];
    //Lbl
    _msgInfoLb=[[UILabel alloc]initWithFrame:CGRectMake(83, 8, 253, 25)];
    [_msgInfoLb setBackgroundColor:[UIColor clearColor]];
    [_msgInfoLb setText:@"Message Info"];
    [_msgInfoLb setFont: [_msgInfoLb.font fontWithSize: 15.0]];
    [_infoView addSubview:_msgInfoLb];

    
    //DeliveredLbl
    _DeliveredLabel=[[UILabel alloc]initWithFrame:CGRectMake(60, 126, 230, 32)];
    [_DeliveredLabel setBackgroundColor:[UIColor clearColor]];
    [_DeliveredLabel setFont: [_DeliveredLabel.font fontWithSize: 15.0]];
    [_infoView addSubview:_DeliveredLabel];
    
    _DeliveredTimeLabel=[[UILabel alloc]initWithFrame:CGRectMake(60, 159, 269, 32)];
    [_DeliveredTimeLabel setBackgroundColor:[UIColor clearColor]];
    [_DeliveredTimeLabel setFont: [_DeliveredTimeLabel.font fontWithSize: 15.0]];
    [_infoView addSubview:_DeliveredTimeLabel];
    
    [_FullView addSubview:_infoView];
    [self.tableSingleChat.superview addSubview:_FullView];

}

-(void)cancel
{
    [self.WholeView setHidden:YES];
    //[myAudioPlayer pause];
}
-(void)Back
{
    [self.FullView setHidden:YES];
    //[myAudioPlayer pause];
}

-(void)getplay:(UIButton*)sender
{

//    if (myAudioPlayer.isPlaying)
//    {
//        PlayButton.png
        [_PlayPauseButton setBackgroundImage:[UIImage imageNamed:@"PlayButton.png"] forState:UIControlStateNormal];
//    }
//    else{
//        [_PlayPauseButton setBackgroundImage:[UIImage imageNamed:@"PauseButton.png"] forState:UIControlStateNormal];
//    }
    
    NSString *name = sender.accessibilityHint;

    
    NSString *S1 = [name componentsSeparatedByString:@"."][0];
    NSString *S2 = [name componentsSeparatedByString:@"."][1];
    NSLog(@"%@",S1);
    NSLog(@"%@",S2);
   
//    if ([S2 isEqualToString:@"aac"])
//    {
//    NSString *appendString = @"m4a";
//        name = [NSString stringWithFormat:@"%@.%@",S1,appendString];
//    }
//    NSLog(@"%@",name);
    NSError *error;
    
   
    
    
    
    
    
    
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *getPath = [documentsDirectory stringByAppendingPathComponent:@""];
    NSURL *audioURL = [NSURL fileURLWithPath:getPath];
    
    NSData *audioData = [NSData dataWithContentsOfURL:audioURL];
    NSString *docDirPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", docDirPath , name];
    [audioData writeToFile:filePath atomically:YES];
    
    NSURL *fileURL = [NSURL fileURLWithPath:filePath];
//    myAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
//    if (myAudioPlayer == nil) {
//        NSLog(@"AudioPlayer did not load properly: %@", [error description]);
//    } else {
//        [myAudioPlayer play];
//    }
    
    
    
    
    AVPlayer *player = [AVPlayer playerWithURL:fileURL];
    
    // create a player view controller
    AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
    [self presentViewController:controller animated:YES completion:nil];
    controller.player = player;
    [player play];

    
    
//    myAudioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:audioURL error:&error];
//    myAudioPlayer.delegate = self;
//    
//    AVAudioSession* audioSession = [AVAudioSession sharedInstance];
//    NSError *errorSession = nil;
//    [audioSession setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionDuckOthers error:nil];
//    [audioSession setActive:NO error:&errorSession];
//    
//    [myAudioPlayer prepareToPlay];
//    [myAudioPlayer setVolume:1.0];
//    [myAudioPlayer play];
//    if (error) {
//        NSLog(@"error %@",[error localizedDescription]);
//    }

    // Construct URL to sound file
//    NSString *path = [NSString stringWithFormat:@"%@/20170522231600775.m4a", [[NSBundle mainBundle] resourcePath]];
//    NSURL *soundUrl = [NSURL fileURLWithPath:path];
//    
//    // Create audio player object and initialize with URL to sound
//    myAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    
//    NSURL *soundURL = [[NSBundle mainBundle] URLForResource:S1
//                                              withExtension:S2];
//    myAudioPlayer = [[AVAudioPlayer alloc]
//               initWithContentsOfURL:soundURL error:nil];
//    
//    [myAudioPlayer play];
    
    
    

//    NSError *error = nil;
//    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *getPath = [documentsDirectory stringByAppendingPathComponent:name];
//
//        //NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:getPath ];
//    NSURL *fileURL = [[NSURL alloc]initWithString:getPath];
//
//    
//    myAudioPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
//    if (!myAudioPlayer) {
//        NSLog(@"failed playing SeriouslyFunnySound1, error: %@", error.description);
//    }
//    else{
//        [[AVAudioSession sharedInstance] setDelegate: self];
//        NSError *setCategoryError = nil;
//        [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error: &setCategoryError];
//        if (setCategoryError)
//            NSLog(@"Error setting category! %@", setCategoryError);
//    myAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
//    [myAudioPlayer setVolume:5.0];
//        [myAudioPlayer prepareToPlay];
//
//    [myAudioPlayer play];
//    }
//    self.movingslider.maximumValue = myAudioPlayer.duration;
//    NSLog(@"%f",myAudioPlayer.duration);


    sliderTime = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                              target:self
                                            selector:@selector(updateSlider:)
                                            userInfo:nil
                                             repeats:YES];
    Timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                  target:self
                                                selector:@selector(updateTimer)
                                                userInfo:nil
                                                 repeats:YES];
        
    NSTimeInterval currentTime = myAudioPlayer.currentTime;
    NSInteger minutes = floor(currentTime/60);
    NSInteger seconds = trunc(currentTime - minutes * 60);
    NSLog(@"TimeLabel:%@",[NSString stringWithFormat:@"%d:%02d", minutes, seconds]);
   

}


-(void)SliderChanged:(UISlider*)sender
{
    [myAudioPlayer stop];
    myAudioPlayer.currentTime = sender.value;
    [myAudioPlayer prepareToPlay];
    [myAudioPlayer play];
}

-(void)updateSlider:(UISlider*)sender
{
    self.movingslider.value = myAudioPlayer.currentTime;
}
-(void)updateTimer
{
    
    NSTimeInterval currentTime = myAudioPlayer.currentTime;
    NSInteger minutes = floor(currentTime/60);
    NSInteger seconds = trunc(currentTime - minutes * 60);
    self.TimeLabel.text = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];
    NSLog(@"TimeLabel:%@",self.TimeLabel.text);
    double latdouble = [self.TimeLabel.text doubleValue];
    NSLog(@"latdouble: %f", latdouble);
    if (myAudioPlayer.duration == latdouble)
    {
        [_PlayPauseButton setBackgroundImage:[UIImage imageNamed:@"PlayButton.png"] forState:UIControlStateNormal];
    }
}

-(IBAction)onPlayVideo:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int nPos=btn.tag;
    nPos=nPos-35000;
//    NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
//    Message *message = [reversed objectAtIndex:nPos];
    Message *message = [arrWholetableArray objectAtIndex:nPos];
    NSString *strContent= message.text;
    NSArray *components = [strContent componentsSeparatedByString:@"/"];
    NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
    NSURL *movieURL=[NSURL fileURLWithPath:getImagePath];
    
    AVPlayer *player = [AVPlayer playerWithURL:movieURL];
    AVPlayerViewController *playerViewController = [AVPlayerViewController new];
    playerViewController.player = player;
    [playerViewController.player play];//Used to Play On start
    [self presentViewController:playerViewController animated:YES completion:nil];
}
- (void)handleTap:(UITapGestureRecognizer *)tap
{
    if (UIGestureRecognizerStateEnded == tap.state)
    {
        UITableView *tableView = (UITableView *)tap.view;
        CGPoint p = [tap locationInView:tap.view];
        NSIndexPath* indexPath = [tableView indexPathForRowAtPoint:p];
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        MessageCell *cell = (MessageCell *)[self.tableSingleChat cellForRowAtIndexPath:indexPath];
        CGPoint pointInCell = [tap locationInView:cell];
        if (CGRectContainsPoint(cell.imgView.frame, pointInCell))
        {
            // user tapped image
            NSInteger rowNumber = 0;
            
            for (NSInteger i = 0; i < indexPath.section; i++) {
                rowNumber += [self tableView:tableView numberOfRowsInSection:i];
            }
            
            rowNumber += indexPath.row;
//            NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
//            Message *msg = [reversed objectAtIndex:rowNumber];
            Message *msg = [arrWholetableArray objectAtIndex:rowNumber];
            if ([msg.type isEqualToString:@"image"])
            {
                int nSelectedIndex=0;
                arrPhotos=[[NSMutableArray alloc]init];
                for (int i=0; i<arrWholetableArray.count; i++)
                {
                    Message *message = [arrWholetableArray objectAtIndex:i];
                    if ([message.type isEqualToString:@"image"])
                    {
                        if (message.sender==MessageSenderMyself)
                        {
                            MWPhoto *photo = [MWPhoto photoWithImage:message.image];
                            photo.caption = message.caption;
                            [arrPhotos addObject:photo];
                        }
                        else
                        {
                            if (message.status==MessageStatusDownloaded)
                            {
                                MWPhoto *photo = [MWPhoto photoWithImage:message.image];
                                photo.caption = message.caption;
                                [arrPhotos addObject:photo];
                            }
                        }
                       
                        
                        if (rowNumber==i)
                        {
                            nSelectedIndex=arrPhotos.count-1;
                        }
                        
                    }
                }
                MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
                browser.displayActionButton = YES;
                browser.displaySelectionButtons = YES;
                browser.zoomPhotosToFill = NO;
                browser.alwaysShowControls = NO;
                browser.enableGrid = YES;
                browser.startOnGrid = NO;
                browser.hidesBottomBarWhenPushed=YES;
                [browser setCurrentPhotoIndex:nSelectedIndex];
                [self.navigationController pushViewController:browser animated:YES];

                
            }
            else if ([msg.type isEqualToString:@"location"])
            {
                strLat=msg.Latitude;
                strLong=msg.Longitude;
                
                if (msg.sender==MessageSenderMyself)
                {
                    strNameToSend=appDelegate.strName;
                    isShowDistance=@"no";
                }
                else
                {
                    strNameToSend=strName;
                     isShowDistance=@"yes";
                }

                BOOL canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"http://maps.google.com/maps?z=8"]];
                if (canHandle)
                {
                    NSLog(@"GoogleMaps");
                    NSString *urlString = [NSString stringWithFormat:@"http://maps.google.com/maps?daddr=%@,%@", strLat,strLong];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
                }
                else {
                    [self performSegueWithIdentifier:@"SingleChat_ToShowLocation" sender:self];

                }
            }
            else if ([msg.type isEqualToString:@"video"])
            {
                NSString *strContent= msg.text;
                NSArray *components = [strContent componentsSeparatedByString:@"/"];
                NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
                NSURL *movieURL=[NSURL fileURLWithPath:getImagePath];
                
                AVPlayer *player = [AVPlayer playerWithURL:movieURL];
                AVPlayerViewController *playerViewController = [AVPlayerViewController new];
                playerViewController.player = player;
                [playerViewController.player play];//Used to Play On start
                [self presentViewController:playerViewController animated:YES completion:nil];
                
            }
        }
        [_inputbar resignFirstResponder];
       

        
    }
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if(![touch.view isMemberOfClass:[UITextField class]]) {
        [touch.view endEditing:YES];
    }
    if(![touch.view isMemberOfClass:[UITextView class]]) {
        [touch.view endEditing:YES];
    }
}
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < arrPhotos.count) {
        return [arrPhotos objectAtIndex:index];
    }
    return nil;
}



-(void)startUploadingImagesOrVideoOrAudio
{
    for (int i=0; i<arrWholetableArray.count; i++)
    {
        Message *messageNew = [arrWholetableArray objectAtIndex:i];
        if ([messageNew.type isEqualToString:@"image"] || [messageNew.type isEqualToString:@"video"] || [messageNew.type isEqualToString:@"audio"])
        {
            long status=messageNew.status;
            NSString *strStat=[NSString stringWithFormat:@"%ld",status];
            
            if ([strStat isEqualToString:@"0"])
            {
                strCurrentlyUploading=[NSString stringWithFormat:@"%d",i];
                NSDictionary *dictContent=[db_class getChatMessage:messageNew.chat_id];
                NSString *strToMsg=[NSString stringWithFormat:@"%@", [dictContent valueForKey:@"userId"]];
                NSString *strImageURL=[NSString stringWithFormat:@"%@", [dictContent valueForKey:@"content"]];
                NSArray *components = [strImageURL componentsSeparatedByString:@"/"];
                NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                //Compress Img
                
//                UIImage *original = [UIImage imageNamed:strImageName];
//                UIImage *compressedImage = UIImageJPEGRepresentation(original, 0.5f);
//                NSLog(@"Original --- Size of Image(bytes):%d",[compressedImage length]);

                
              //  NSLog(@"%@",strImageName);
                NSString *strMsgId=[NSString stringWithFormat:@"%@",messageNew.chat_id];
                long long milliSeconds=[self convertDateToMilliseconds:messageNew.date];
                NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
                
                //convert uiimage to
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
                NSURL* fileUrl = [NSURL fileURLWithPath:filePath];
                
                
                AWSS3TransferUtilityUploadExpression *expression = [AWSS3TransferUtilityUploadExpression new];
                
                expression.progressBlock = ^(AWSS3TransferUtilityTask *task, NSProgress *progress) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                     });
                };
                
                AWSS3TransferUtilityUploadCompletionHandlerBlock completionHandler = ^(AWSS3TransferUtilityUploadTask *task, NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // Do something e.g. Alert a user for transfer completion.
                        // On failed uploads, `error` contains the error object.
                        NSLog(@"%@",error);
                        NSIndexPath *indexPath = [self.tableArray indexPathForMessage:messageNew];
                        MessageCell *cell = (MessageCell *)[self.tableSingleChat cellForRowAtIndexPath:indexPath];
                        strCurrentlyUploading=@"";
                      // [MBProgressHUD hideHUDForView:cell.imgView animated:YES];
                        DGActivityIndicatorView *activityIndicatorView = (DGActivityIndicatorView *)[cell.imgView viewWithTag:i+22000];
                        [activityIndicatorView stopAnimating];
                        if ([messageNew.type isEqualToString:@"audio"] )
                        {
                            DGActivityIndicatorView *activityIndicatorView = (DGActivityIndicatorView *)[cell.contentView viewWithTag:i+22000];
                            [activityIndicatorView stopAnimating];
                            [activityIndicatorView removeFromSuperview];
                        }

                        if (error==nil)
                        {
                            if ([messageNew.type isEqualToString:@"image"] )
                            {
                                AWSS3TransferUtility *transferUtility1 = [AWSS3TransferUtility defaultS3TransferUtility];
                                [[transferUtility1 uploadFile:fileUrl1 bucket:FILE_BUCKET_NAME key:strImageURL contentType:@"image" expression:expression completionHandler:^(AWSS3TransferUtilityUploadTask *task, NSError *error) {
                                                 dispatch_async(dispatch_get_main_queue(), ^{

                                                     if ([strChatRoomType isEqualToString:@"2"])
                                                     {
                                                         NSMutableArray *arrPartis=[[NSMutableArray alloc]init];
                                                         NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
                                                         NSArray *arrdata=[db_class getParticipants:strZoeChatId];
                                                         for (int i=0; i<arrdata.count; i++)
                                                         {
                                                             NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
                                                             NSDictionary *dictCont=[arrdata objectAtIndex:i];
                                                             NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"userId"]];
                                                             [dictParti setObject:strPhone forKey:@"participantId"];
                                                             [arrPartis addObject:dictParti];
//                                                             NSString *BCstrMsgId = [[NSUUID UUID] UUIDString]; //id
//                                                             
//                                                             BOOL isBroadcastChat;
//                                                             isBroadcastChat=NO;
//                                                             isBroadcastChat=[db_class doesChatExist:strPhone];
//                                                             
//                                                             if (isBroadcastChat==NO)
//                                                             {
//                                                                 
//                                                                 
//                                                                 BOOL success=[db_class insertChats:@"" chatRoomId:strPhone chatRoomType:@"0" sender:@"0" lastMessage:strImageURL lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"image" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
//                                                                 
//                                                                 if (success==YES)
//                                                                 {
//                                                                     isBroadcastChat=YES;
//                                                                 }
//                                                             }
//                                                             else
//                                                             {
//                                                                 
//                                                                 
//                                                                 BOOL success=[db_class updateLastMessage:strPhone sender:@"0" lastMessage:strImageURL lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"image" unReadCount:@"0"];
//                                                                 NSLog(@"success");
//                                                                 
//                                                                 
//                                                             }
//                                                             BOOL success=[db_class insertChatMessages:BCstrMsgId userId:strPhone groupId:strPhone chatRoomType:@"0" content:strImageURL contentType:@"image" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                                                         }
                                                         NSError *error;
                                                         NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrPartis options:0 error:&error];
                                                         NSString *jsonString;
                                                         
                                                         if (! jsonData)
                                                         {
                                                             NSLog(@"Got an error: %@", error);
                                                         }
                                                         else
                                                         {
                                                             jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                                                         }
                                                         jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
                                                         jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\" withString:@""];

                                                         
                                                         NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                                                         [dictParam setValue:appDelegate.strID forKey:@"from"];
                                                         [dictParam setValue:jsonString forKey:@"participantId"];
                                                         
                                                       
                                                         [appDelegate.socket emit:@"sendBroadcast" with:@[@{@"from": appDelegate.strID,@"participants": jsonString,@"content":strImageURL,@"contentType": @"image",@"messageId": strMsgId,@"time":strMilliSeconds,@"chatRoomType":@"0",@"groupId":@""}]] ;
                                                         
                                                         
                                                         
                                                         for (int i=0; i<arrdata.count; i++)
                                                         {
                                                             NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
                                                             NSDictionary *dictCont=[arrdata objectAtIndex:i];
                                                             NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"userId"]];
                                                             [dictParti setObject:strPhone forKey:@"participantId"];
                                                             [arrPartis addObject:dictParti];
                                                             NSString *BCstrMsgId = [[NSUUID UUID] UUIDString]; //id
                                                             
                                                             BOOL isBroadcastChat;
                                                             isBroadcastChat=NO;
                                                             isBroadcastChat=[db_class doesChatExist:strPhone];
                                                             
                                                             if (isBroadcastChat==NO)
                                                             {
                                                                 
                                                                 
                                                                 BOOL success=[db_class insertChats:@"" chatRoomId:strPhone chatRoomType:@"0" sender:@"0" lastMessage:strImageURL lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"image" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
                                                                 
                                                                 if (success==YES)
                                                                 {
                                                                     isBroadcastChat=YES;
                                                                 }
                                                             }
                                                             else
                                                             {
                                                                 
                                                                 
                                                                 BOOL success=[db_class updateLastMessage:strPhone sender:@"0" lastMessage:strImageURL lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"image" unReadCount:@"0"];
                                                                 NSLog(@"success");
                                                                 
                                                                 
                                                             }
                                                             BOOL success=[db_class insertChatMessages:BCstrMsgId userId:strPhone groupId:strPhone chatRoomType:@"0" content:strImageURL contentType:@"image" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                                                         }

                                                     }
                                                     
                                                     else
                                                         
                                                     {
                                                     
                                                          [appDelegate.socket emit:@"sendMessage" with:@[@{@"from": appDelegate.strID,@"to": strToMsg,@"content": strImageURL,@"contentType":@"image",@"messageId": strMsgId,@"time":strMilliSeconds,@"chatRoomType":strChatRoomType,@"groupId":@""}]];
                                                     }
//                                                     });
                                                 
                                                 }
                                                                );}
                                  ] continueWithBlock:^id(AWSTask *task) {
                                    if (task.error) {
                                        NSLog(@"Error: %@", task.error);
                                    }
                                   
                                    if (task.result) {
                                        uploadTask = task.result;
                                        // Do something with uploadTask.
                                        NSLog(@"%@",uploadTask);
                                        
                                    }
                                    
                                    return nil;
                                }];

                                
                            }
                            else if([messageNew.type isEqualToString:@"video"])
                            {
                                
                                

                                AWSS3TransferUtility *transferUtility1 = [AWSS3TransferUtility defaultS3TransferUtility];
                                [[transferUtility1 uploadFile:fileUrl1 bucket:FILE_BUCKET_NAME key:thumbImg contentType:@"image" expression:expression completionHandler:completionHandler]
                              continueWithBlock:^id(AWSTask *task) {
                                    if (task.error) {
                                        NSLog(@"Error: %@", task.error);
                                    }
                                    
                                    if (task.result) {
                                        uploadTask = task.result;
                                        // Do something with uploadTask.
                                        NSLog(@"%@",uploadTask);
                                        
                                        if ([strChatRoomType isEqualToString:@"2"]) {
                                            NSMutableArray *arrPartis=[[NSMutableArray alloc]init];
                                            NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
                                            NSArray *arrdata=[db_class getParticipants:strZoeChatId];
                                            for (int i=0; i<arrdata.count; i++)
                                            {
                                                NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
                                                NSDictionary *dictCont=[arrdata objectAtIndex:i];
                                                NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"userId"]];
                                                [dictParti setObject:strPhone forKey:@"participantId"];
                                                [arrPartis addObject:dictParti];
//                                                NSString *BCstrMsgId = [[NSUUID UUID] UUIDString]; //id
//                                                
//                                                BOOL isBroadcastChat;
//                                                isBroadcastChat=NO;
//                                                isBroadcastChat=[db_class doesChatExist:strPhone];
//                                                
//                                                if (isBroadcastChat==NO)
//                                                {
//                                                    
//                                                    
//                                                    BOOL success=[db_class insertChats:@"" chatRoomId:strPhone chatRoomType:@"0" sender:@"0" lastMessage:strImageURL lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"video" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
//                                                    
//                                                    if (success==YES)
//                                                    {
//                                                        isBroadcastChat=YES;
//                                                    }
//                                                }
//                                                else
//                                                {
//                                                    
//                                                    
//                                                    BOOL success=[db_class updateLastMessage:strPhone sender:@"0" lastMessage:strImageURL lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"video" unReadCount:@"0"];
//                                                    NSLog(@"success");
//                                                    
//                                                    
//                                                }
//                                                BOOL success=[db_class insertChatMessages:BCstrMsgId userId:strPhone groupId:strPhone chatRoomType:@"0" content:strImageURL contentType:@"video" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                                            }
                                            NSError *error;
                                            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrPartis options:0 error:&error];
                                            NSString *jsonString;
                                            
                                            if (! jsonData)
                                            {
                                                NSLog(@"Got an error: %@", error);
                                            }
                                            else
                                            {
                                                jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                                            }
                                            jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
                                            jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\" withString:@""];

                                            
                                            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                                            [dictParam setValue:appDelegate.strID forKey:@"from"];
                                            [dictParam setValue:jsonString forKey:@"participantId"];
                                            
                                            [appDelegate.socket emit:@"sendBroadcast" with:@[@{@"from": appDelegate.strID,@"participants": jsonString,@"content":strImageURL,@"contentType": @"video",@"messageId": strMsgId,@"time":strMilliSeconds,@"chatRoomType":@"0",@"groupId":@""}]];
                                            
                                            
                                            
                                            for (int i=0; i<arrdata.count; i++)
                                            {
                                                NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
                                                NSDictionary *dictCont=[arrdata objectAtIndex:i];
                                                NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"userId"]];
                                                [dictParti setObject:strPhone forKey:@"participantId"];
                                                [arrPartis addObject:dictParti];
                                                NSString *BCstrMsgId = [[NSUUID UUID] UUIDString]; //id
                                                
                                                BOOL isBroadcastChat;
                                                isBroadcastChat=NO;
                                                isBroadcastChat=[db_class doesChatExist:strPhone];
                                                
                                                if (isBroadcastChat==NO)
                                                {
                                                    
                                                    
                                                    BOOL success=[db_class insertChats:@"" chatRoomId:strPhone chatRoomType:@"0" sender:@"0" lastMessage:strImageURL lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"video" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
                                                    
                                                    if (success==YES)
                                                    {
                                                        isBroadcastChat=YES;
                                                    }
                                                }
                                                else
                                                {
                                                    
                                                    
                                                    BOOL success=[db_class updateLastMessage:strPhone sender:@"0" lastMessage:strImageURL lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"video" unReadCount:@"0"];
                                                    NSLog(@"success");
                                                    
                                                    
                                                }
                                                BOOL success=[db_class insertChatMessages:BCstrMsgId userId:strPhone groupId:strPhone chatRoomType:@"0" content:strImageURL contentType:@"video" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                                            }

                                        }
                                        
                                        else
                                            
                                        {
                                            
                                            [appDelegate.socket emit:@"sendMessage" with:@[@{@"from": appDelegate.strID,@"to": strZoeChatId,@"content": strImageURL,@"contentType":@"video",@"messageId": strMsgId,@"time":strMilliSeconds,@"chatRoomType":strChatRoomType,@"groupId":@""}]];
                                        }
                                    }
                                    
                                    return nil;
                                }];

                             
                            }
                            else if([messageNew.type isEqualToString:@"audio"])
                            {
                                if ([strChatRoomType isEqualToString:@"2"]) {
                                    NSMutableArray *arrPartis=[[NSMutableArray alloc]init];
                                    NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
                                    NSArray *arrdata=[db_class getParticipants:strZoeChatId];
                                    for (int i=0; i<arrdata.count; i++)
                                    {
                                        NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
                                        NSDictionary *dictCont=[arrdata objectAtIndex:i];
                                        NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"userId"]];
                                        [dictParti setObject:strPhone forKey:@"participantId"];
                                        [arrPartis addObject:dictParti];
//                                        NSString *BCstrMsgId = [[NSUUID UUID] UUIDString]; //id
//                                        
//                                        BOOL isBroadcastChat;
//                                        isBroadcastChat=NO;
//                                        isBroadcastChat=[db_class doesChatExist:strPhone];
//                                        
//                                        if (isBroadcastChat==NO)
//                                        {
//                                            
//                                            
//                                           BOOL success=[db_class insertChats:@"" chatRoomId:strPhone chatRoomType:@"0" sender:@"0" lastMessage:strImageURL lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"audio" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
//                                            
//                                            if (success==YES)
//                                            {
//                                                isBroadcastChat=YES;
//                                            }
//                                        }
//                                        else
//                                        {
//                                            
//                                            
//                                           BOOL success=[db_class updateLastMessage:strPhone sender:@"0" lastMessage:strImageURL lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"audio" unReadCount:@"0"];
//                                            NSLog(@"success");
//                                            
//                                            
//                                        }
//                                       BOOL success=[db_class insertChatMessages:BCstrMsgId userId:strPhone groupId:strPhone chatRoomType:@"0" content:strImageURL contentType:@"audio" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                                    }
                                    NSError *error;
                                    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrPartis options:0 error:&error];
                                    NSString *jsonString;
                                    
                                    if (! jsonData)
                                    {
                                        NSLog(@"Got an error: %@", error);
                                    }
                                    else
                                    {
                                        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                                    }
                                    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
                                    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\" withString:@""];

                                    
                                    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                                    [dictParam setValue:appDelegate.strID forKey:@"from"];
                                    [dictParam setValue:jsonString forKey:@"participantId"];
                                    
                                    [appDelegate.socket emit:@"sendBroadcast" with:@[@{@"from": appDelegate.strID,@"participants": jsonString,@"content":strImageURL,@"contentType": @"audio",@"messageId": strMsgId,@"time":strMilliSeconds,@"chatRoomType":@"0",@"groupId":@""}]];
                                    
                                    for (int i=0; i<arrdata.count; i++)
                                    {
                                        NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
                                        NSDictionary *dictCont=[arrdata objectAtIndex:i];
                                        NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"userId"]];
                                        [dictParti setObject:strPhone forKey:@"participantId"];
                                        [arrPartis addObject:dictParti];
                                        NSString *BCstrMsgId = [[NSUUID UUID] UUIDString]; //id
                                        
                                        BOOL isBroadcastChat;
                                        isBroadcastChat=NO;
                                        isBroadcastChat=[db_class doesChatExist:strPhone];
                                        
                                        if (isBroadcastChat==NO)
                                        {
                                            
                                            
                                            BOOL success=[db_class insertChats:@"" chatRoomId:strPhone chatRoomType:@"0" sender:@"0" lastMessage:strImageURL lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"audio" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
                                            
                                            if (success==YES)
                                            {
                                                isBroadcastChat=YES;
                                            }
                                        }
                                        else
                                        {
                                            
                                            
                                            BOOL success=[db_class updateLastMessage:strPhone sender:@"0" lastMessage:strImageURL lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"audio" unReadCount:@"0"];
                                            NSLog(@"success");
                                            
                                            
                                        }
                                        BOOL success=[db_class insertChatMessages:BCstrMsgId userId:strPhone groupId:strPhone chatRoomType:@"0" content:strImageURL contentType:@"audio" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                                    }
                                    
                                }
                                
                                else

                                {
                                    
                                [appDelegate.socket emit:@"sendMessage" with:@[@{@"from": appDelegate.strID,@"to": strZoeChatId,@"content": strImageURL,@"contentType":@"audio",@"messageId": strMsgId,@"time":strMilliSeconds,@"chatRoomType":strChatRoomType,@"groupId":@""}]];
                                }
                            }

                           
                            BOOL success=[db_class updateContentStatus:strMsgId contentStatus:@"uploaded"];
                            UIButton *btnCancelUpload=(UIButton *)[cell viewWithTag:i+25000];
                            [btnCancelUpload removeFromSuperview];
                            messageNew.status=MessageStatusUploaded;
                            
                        }
                        else // if ([[error.userInfo valueForKey:@"NSLocalizedDescription"]isEqualToString:@"cancelled"])
                        {
                            BOOL success=[db_class updateContentStatus:strMsgId contentStatus:@"cancelled"];
                            
                            UIButton *btnCancelUpload=(UIButton *)[cell viewWithTag:i+25000];
                            [btnCancelUpload removeFromSuperview];
                            
                            UIButton *btnRetry=[[UIButton alloc]initWithFrame:CGRectMake(cell.imgView.center.x-25, cell.imgView.center.y-12.5, 50, 25)];
                            btnRetry.tag=indexPath.row+28000;
                             [btnRetry setBackgroundImage:[UIImage imageNamed:@"retry.png"] forState:UIControlStateNormal];
                            [btnRetry addTarget:self action:@selector(RetryUpload:) forControlEvents:UIControlEventTouchUpInside];
                            [cell.contentView addSubview:btnRetry];
                            messageNew.status=MessageStatusCancelled;
                        }
                        CGPoint offset = self.tableSingleChat.contentOffset;
                         self.tableArray = [[TableArray alloc] init];
                        arrWholetableArray=[[NSMutableArray alloc]init];
                         [self getChatMessages];
                        [self.tableSingleChat layoutIfNeeded]; // Force layout so things are updated before resetting the contentOffset.
                        [self.tableSingleChat setContentOffset:offset];
                        [self startUploadingImagesOrVideoOrAudio];
                    });
                };
                
                if ([messageNew.type isEqualToString:@"image"])
                {
                   UIImage *testImage = [UIImage imageWithContentsOfFile:filePath];
                    UIImage *compressedImage = [self compressImageForThumbnail:testImage];
                    NSData *imgData2 = UIImageJPEGRepresentation(compressedImage, 0.1);
                    NSLog(@"After --- Size of Image(bytes):%lu",(unsigned long)[imgData2 length]);
                    testImage = [UIImage imageWithData:imgData2];
                    
                    NSDate *now=[NSDate date];
                    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                    [dateFormatter1 setDateFormat:@"yyyyMMddHHmmssSSS"];
                    NSString *localDateString = [dateFormatter1 stringFromDate:now];
                    //  NSLog(@"%@",localDateString);
                    NSString *strExt=@"jpg";
                    NSString *imgStr = [NSString stringWithFormat:@"%@.%@",localDateString,strExt];
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                         NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                                      imgStr];

                    [imgData2 writeToFile:path atomically:YES];
                    
                    NSLog(@"%@",path);
                     fileUrl1 = [NSURL fileURLWithPath:path];
                    
                    thumbImg = [NSString stringWithFormat:@"thumb_%@",strImageName];
                    AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility defaultS3TransferUtility];
                     [[transferUtility uploadFile:fileUrl bucket:FILE_BUCKET_NAME key:strImageURL contentType:@"image" expression:expression completionHandler:completionHandler] continueWithBlock:^id(AWSTask *task) {
                        if (task.error) {
                            NSLog(@"Error: %@", task.error);
                        }
                        
                        if (task.result) {
                            uploadTask = task.result;
                            // Do something with uploadTask.
                            NSLog(@"%@",uploadTask);
                            
                            
                            
                            
                        }
                        
                        return nil;
                    }];
                    
                   
                    
                    
                    
                    

                }
                else if([messageNew.type isEqualToString:@"video"])
                {
                    AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility defaultS3TransferUtility];
                    
                     [[transferUtility uploadFile:fileUrl bucket:FILE_BUCKET_NAME key:strImageName contentType:@"video" expression:expression completionHandler:completionHandler]
                    continueWithBlock:^id(AWSTask *task) {
                        if (task.error) {
                            NSLog(@"Error: %@", task.error);
                        }
                       
                        if (task.result) {
                            uploadTask = task.result;
                            // Do something with uploadTask.
                            NSLog(@"%@",uploadTask);
                            //thumbnail
                            NSURL *url = [NSURL fileURLWithPath:filePath];
                            UIImage* thumbnail;
                            AVAsset *asset = [AVAsset assetWithURL:url];
                            AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
                            imageGenerator.appliesPreferredTrackTransform = YES;
                            CMTime time = [asset duration];
                            time.value = 0;
                            CGFloat duration = CMTimeGetSeconds([asset duration]);
                            for(CGFloat i = 0.0; i<duration; i=i+0.1)
                            {
                                CGImageRef imgRef = [imageGenerator copyCGImageAtTime:CMTimeMake(i, duration) actualTime:NULL error:nil];
                                thumbnail = [[UIImage alloc] initWithCGImage:imgRef scale:UIViewContentModeScaleAspectFit orientation:UIImageOrientationUp];
                            }
//                            UIImage *compressedImage = [self compressImageForThumbnail:thumbnail];
                            
                            NSData *data = UIImagePNGRepresentation(thumbnail);
                            
                            //Path for the documentDirectory
                            NSDate *now=[NSDate date];
                            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                            [dateFormatter1 setDateFormat:@"yyyyMMddHHmmssSSS"];
                            NSString *localDateString = [dateFormatter1 stringFromDate:now];
                            NSString *strExt=@"jpg";
                            NSString *VideoimgStr = [NSString stringWithFormat:@"%@.%@",localDateString,strExt];

                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
                            NSString *documentsDirectory = [paths objectAtIndex:0];
                            NSString* path = [documentsDirectory stringByAppendingPathComponent:
                                              VideoimgStr];
                            [data writeToFile:path atomically:YES];
                            NSLog(@"%@",path);
                             fileUrl1 = [NSURL fileURLWithPath:path];
                            NSString *name = strImageName;
                            NSString *S1 = [name componentsSeparatedByString:@"."][0];
                            NSString *S2 = [NSString stringWithFormat:@"%@.jpg",S1];
                            NSLog(@"%@",S1);
                            NSLog(@"%@",S2);
                            thumbImg = [NSString stringWithFormat:@"thumb_%@",S2];
                            
                            
                            
                         }
                        
                        return nil;
                    }];
                }
                else if([messageNew.type isEqualToString:@"audio"])
                {
                    AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility defaultS3TransferUtility];
                    [[transferUtility uploadFile:fileUrl bucket:FILE_BUCKET_NAME key:strImageName contentType:@"audio" expression:expression completionHandler:completionHandler]
                    continueWithBlock:^id(AWSTask *task) {
                        if (task.error) {
                            NSLog(@"Error: %@", task.error);
                        }
                        
                        if (task.result) {
                            uploadTask = task.result;
                            // Do something with uploadTask.
                            NSLog(@"%@",uploadTask);
                            
                        }
                        
                        return nil;
                    }];
                }

                
                                break;
            }
        }
    }
}

- (void)cancelUpload:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int nPos=btn.tag;
    nPos=nPos-25000;
    
//    NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
//    Message *message = [reversed objectAtIndex:nPos];
        Message *message = [arrWholetableArray objectAtIndex:nPos];
        NSLog(@"%@",message.text);
        if ([message.type isEqualToString:@"image"] || [message.type isEqualToString:@"video"] || [message.type isEqualToString:@"audio"])
        {
            long status=message.status;
            NSString *strStat=[NSString stringWithFormat:@"%ld",status];
            
            if ([strStat isEqualToString:@"0"])
            {
               
                
                NSString *strSenderTag=[NSString stringWithFormat:@"%d",nPos];
                if ([strCurrentlyUploading isEqualToString:strSenderTag])
                {
                    [uploadTask cancel];
                }
                else
                {
                    NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
                    NSLog(@"%ld",(long)indexPath.row);
                    MessageCell *cell = (MessageCell *)[self.tableSingleChat cellForRowAtIndexPath:indexPath];
                  //  [MBProgressHUD hideHUDForView:cell.imgView animated:YES];
                    DGActivityIndicatorView *activityIndicatorView = (DGActivityIndicatorView *)[cell.imgView viewWithTag:nPos+22000];
                    [activityIndicatorView stopAnimating];

                    BOOL success=[db_class updateContentStatus:message.chat_id contentStatus:@"cancelled"];
                    
                    UIButton *btnCancelUpload=(UIButton *)[cell viewWithTag:nPos+25000];
                    [btnCancelUpload removeFromSuperview];
                    
                    UIButton *btnRetry=[[UIButton alloc]initWithFrame:CGRectMake(cell.imgView.center.x-25, cell.imgView.center.y-12.5, 50, 25)];
                    btnRetry.tag=indexPath.row+28000;
                     [btnRetry setBackgroundImage:[UIImage imageNamed:@"retry.png"] forState:UIControlStateNormal];
                    [btnRetry addTarget:self action:@selector(RetryUpload:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.contentView addSubview:btnRetry];
                    message.status=MessageStatusCancelled;
                }
            }
        }
}
- (void)RetryUpload:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int nPos=btn.tag;
    nPos=nPos-28000;
//    NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
//    Message *message = [reversed objectAtIndex:nPos];
    Message *message = [arrWholetableArray objectAtIndex:nPos];
    BOOL success=[db_class updateContentStatus:message.chat_id contentStatus:@"sending"];
    
    CGPoint offset = self.tableSingleChat.contentOffset;
    self.tableArray = [[TableArray alloc] init];
    arrWholetableArray=[[NSMutableArray alloc]init];
    [self getChatMessages];
    [self.tableSingleChat layoutIfNeeded]; // Force layout so things are updated before resetting the contentOffset.
    [self.tableSingleChat setContentOffset:offset];
    
    if ([strCurrentlyUploading length]==0)
    {
        [self startUploadingImagesOrVideoOrAudio];
    }
}
- (void)downloadImageOrVideo:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int nPos=btn.tag;
    nPos=nPos-30000;
    
    Message *message = [arrWholetableArray objectAtIndex:nPos];
   // NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
    //Message *message = [reversed objectAtIndex:nPos];

    message.status=MessageStatusDownloading;
    
    DGActivityIndicatorView *activityIndicatorView;
    NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
    MessageCell *cell = (MessageCell *)[self.tableSingleChat cellForRowAtIndexPath:indexPath];
    
    UIButton *btnDownload=(UIButton *)[cell viewWithTag:nPos+30000];
    [btnDownload removeFromSuperview];
    
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        
        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:color size:50.0f];
        
        
        
        
        
    }
    else
    {
        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor blackColor] size:50.0f];
        
        
        
        
        
        
    }

    activityIndicatorView.frame = CGRectMake((cell.imgView.frame.size.width/2)-25, (cell.imgView.frame.size.height/2)-25, 50, 50);
    activityIndicatorView.backgroundColor=[UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:0.5];
    activityIndicatorView.layer.cornerRadius=25;
    activityIndicatorView.clipsToBounds=YES;
    [cell.imgView addSubview:activityIndicatorView];
    activityIndicatorView.tag=nPos+22000;
    [activityIndicatorView startAnimating];
    
    
    UIButton *btnCancelDownload=[[UIButton alloc]initWithFrame:CGRectMake(cell.imgView.center.x-12.5, cell.imgView.center.y-12.5, 25, 25)];
    btnCancelDownload.tag=nPos+33000;
    [btnCancelDownload setBackgroundImage:[UIImage imageNamed:@"CloseIcon.png"] forState:UIControlStateNormal];
    [btnCancelDownload addTarget:self action:@selector(cancelDownload:) forControlEvents:UIControlEventTouchUpInside];
    [cell.contentView addSubview:btnCancelDownload];

    
    BOOL success=[db_class updateContentStatus:message.chat_id contentStatus:@"downloading"];
    
    if ([strCurrentlyDownloading isEqualToString:@""])
    {
        [self startDownloadImageOrVideo];
    }
}
- (void)startDownloadImageOrVideo
{
    
    self.tableArray = [[TableArray alloc] init];
    arrWholetableArray=[[NSMutableArray alloc]init];
    [self getChatMessages];
   // NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
   // Message *message = [reversed objectAtIndex:nPos];
   
    
     for (int i=0; i<arrWholetableArray.count; i++)
    {
       // int index = [arrWholetableArray count] - 1;
        
        Message *message = [arrWholetableArray objectAtIndex:i];
        if ([message.type isEqualToString:@"image"] || [message.type isEqualToString:@"video"] || [message.type isEqualToString:@"audio"] || [message.type isEqualToString:@"document"])
        {
            long status=message.status;
            NSString *strStat=[NSString stringWithFormat:@"%ld",status];
            
            if ([strStat isEqualToString:@"8"])
            {
                strCurrentlyDownloading=[NSString stringWithFormat:@"%d",i];
             //   Message *message = [arrWholetableArray objectAtIndex:nPos];
                NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
                MessageCell *cell = (MessageCell *)[self.tableSingleChat cellForRowAtIndexPath:indexPath];
                
                
                NSString *strImgUrl=message.text;
                NSArray *components = [strImgUrl componentsSeparatedByString:@"/"];
                NSString *strIMGVIDName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                
//                if ([message.type isEqualToString:@"video"])
//                {
//                    NSArray *components = [strIMGVIDName componentsSeparatedByString:@"."];
//                     strIMGVIDName = ((![components count])? nil :[components objectAtIndex:0]);
//                    strIMGVIDName=[NSString stringWithFormat:@"%@.mp4",strIMGVIDName];
//                }
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strIMGVIDName]];
                
                
                NSString *strImgURLAsString = strImgUrl;
                [strImgURLAsString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                NSURL *fileURL = [NSURL fileURLWithPath:filePath]; // The file URL of the download destination.
                
                AWSS3TransferUtilityDownloadExpression *expression = [AWSS3TransferUtilityDownloadExpression new];
                expression.progressBlock = ^(AWSS3TransferUtilityTask *task, NSProgress *progress) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // Do something e.g. Update a progress bar.
                    });
                };
                AWSS3TransferUtilityDownloadCompletionHandlerBlock completionHandler = ^(AWSS3TransferUtilityDownloadTask *task, NSURL *location, NSData *data, NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        NSString *fileExtension = [fileURL pathExtension];
                        NSString *UTI = (__bridge_transfer NSString *)UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)fileExtension, NULL);
                        NSString *contentType = (__bridge_transfer NSString *)UTTypeCopyPreferredTagWithClass((__bridge CFStringRef)UTI, kUTTagClassMIMEType);
                        
                        NSLog(@"%@",contentType);
                        
                        NSArray *compos = [contentType componentsSeparatedByString:@"/"];
                        contentType = ((![compos count])? nil :[compos objectAtIndex:0] );
                        
                        // [MBProgressHUD hideHUDForView:cell.imgView animated:YES];
                        DGActivityIndicatorView *activityIndicatorView = (DGActivityIndicatorView *)[cell.imgView viewWithTag:i+22000];
                        [activityIndicatorView stopAnimating];
                        
                        UIButton *btnCancelDownload=(UIButton *)[cell viewWithTag:i+33000];
                        [btnCancelDownload removeFromSuperview];
                        strCurrentlyDownloading=@"";
                        if (!error) {
                            UIImage *img ;
                            if ([contentType isEqualToString:@"image"])
                            {
                                img = [[UIImage alloc] initWithData:data];
                                [UIImagePNGRepresentation(img) writeToFile:filePath atomically:YES];
                                
                                 UIImage *imageFrmDirc = [UIImage imageWithContentsOfFile:filePath];
                                
                                [CustomAlbum addNewAssetWithImage:imageFrmDirc toAlbum:[CustomAlbum getMyAlbumWithName:CSAlbum] onSuccess:^(NSString *ImageId) {
                                    NSLog(@"image saved");
                                    
                                }
                                                          onError:^(NSError *error)
                                 {
                                     NSLog(@"probelm in saving image");
                                 }];
                                
                                
                            }
                            else if ([contentType isEqualToString:@"video"])
                            {
                                BOOL success1 = [data writeToFile:filePath atomically:NO];
                                img=[self thumbnailImageFromURL:fileURL];
                                
                                ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                                NSURL *capturedVideoURL = [NSURL URLWithString:filePath];
                                
                                if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:capturedVideoURL]) {
                                    // request to save video in photo roll.
                                    [library writeVideoAtPathToSavedPhotosAlbum:capturedVideoURL completionBlock:^(NSURL *assetURL, NSError *error) {
                                        if (error) {
                                            NSLog(@"error while saving video");
                                        } else{
                                            [self addAssetURL:assetURL toAlbum:CSAlbum];
                                        }
                                    }];
                                }
                            }
                            else if ([contentType isEqualToString:@"audio"])
                            {
                                img = [[UIImage alloc] initWithData:data];
                                
                            }
                            
                            
                            cell.imgView.image=img;
                            BOOL success=[db_class updateContentStatus:message.chat_id contentStatus:@"downloaded"];
                            BOOL succes=[db_class updateIsDownloaded:message.chat_id isDownloaded:@"1"];
                            message.status=MessageStatusDownloaded;
                            
                            
                            
                        }
                        else
                        {
                            NSLog(@"%@",error);
                            BOOL success=[db_class updateContentStatus:message.chat_id contentStatus:@"cancelled"];
                            message.status=MessageStatusCancelled;
                            UIButton *btnDownload=[[UIButton alloc]initWithFrame:cell.imgView.frame];
                            btnDownload.tag=i+30000;
                            [btnDownload setImage:[UIImage imageNamed:@"download.png"] forState:UIControlStateNormal];
                            [btnDownload addTarget:self action:@selector(downloadImageOrVideo:) forControlEvents:UIControlEventTouchUpInside];
                            [cell.contentView addSubview:btnDownload];
                            
                        }
                        CGPoint offset = self.tableSingleChat.contentOffset;
                        self.tableArray = [[TableArray alloc] init];
                        arrWholetableArray=[[NSMutableArray alloc]init];
                        [self getChatMessages];
                        [self.tableSingleChat layoutIfNeeded]; // Force layout so things are updated before resetting the contentOffset.
                        [self.tableSingleChat setContentOffset:offset];
                        strCurrentlyDownloading=@"";
                        [self startDownloadImageOrVideo];
                    });
                };
                
                AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility defaultS3TransferUtility];
                [[transferUtility downloadToURL:fileURL bucket:FILE_BUCKET_NAME key:strIMGVIDName expression:expression completionHandler:completionHandler]
                
                 continueWithBlock:^id(AWSTask *task) {
                    if (task.error) {
                        NSLog(@"Error: %@", task.error);
                    }
                    if (task.result) {
                         downloadTask = task.result;
                        NSLog(@"%@",downloadTask);
                        // Do something with downloadTask.
                    }
                    
                    return nil;
                }];
                
                break;

            }
        }
    }
    
}

-(void)cancelDownload:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int nPos=btn.tag;
    nPos=nPos-33000;
    
//    NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
//    Message *message = [reversed objectAtIndex:nPos];

    Message *message = [arrWholetableArray objectAtIndex:nPos];
    NSLog(@"%@",message.text);
    if ([message.type isEqualToString:@"image"] || [message.type isEqualToString:@"video"])
    {
        long status=message.status;
        NSString *strStat=[NSString stringWithFormat:@"%ld",status];
        
        if ([strStat isEqualToString:@"8"])
        {
            
            
            NSString *strSenderTag=[NSString stringWithFormat:@"%d",nPos];
            if ([strCurrentlyDownloading isEqualToString:strSenderTag])
            {
                [downloadTask cancel];
            }
            else
            {
                NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
                NSLog(@"%ld",(long)indexPath.row);
                MessageCell *cell = (MessageCell *)[self.tableSingleChat cellForRowAtIndexPath:indexPath];
                //  [MBProgressHUD hideHUDForView:cell.imgView animated:YES];
                DGActivityIndicatorView *activityIndicatorView = (DGActivityIndicatorView *)[cell.imgView viewWithTag:nPos+22000];
                [activityIndicatorView stopAnimating];
                
                BOOL success=[db_class updateContentStatus:message.chat_id contentStatus:@"cancelled"];
                
                UIButton *btnCancelDownload=(UIButton *)[cell viewWithTag:nPos+33000];
                [btnCancelDownload removeFromSuperview];
                
                UIButton *btnDownload=[[UIButton alloc]initWithFrame:cell.imgView.frame];
                btnDownload.tag=nPos+30000;
                [btnDownload setImage:[UIImage imageNamed:@"download.png"] forState:UIControlStateNormal];
                [btnDownload addTarget:self action:@selector(downloadImageOrVideo:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:btnDownload];
            }
        }
    }
}

- (void)addAssetURL:(NSURL*)assetURL toAlbum:(NSString*)albumName
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    [library enumerateGroupsWithTypes:ALAssetsGroupAlbum usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if ([albumName compare: [group valueForProperty:ALAssetsGroupPropertyName]]==NSOrderedSame) {
            //If album found
            [library assetForURL:assetURL resultBlock:^(ALAsset *asset) {
                //add asset to album
                [group addAsset:asset];
            } failureBlock:nil];
        }
        else {
            //if album not found create an album
            [library addAssetsGroupAlbumWithName:albumName resultBlock:^(ALAssetsGroup *group)     {
                [self addAssetURL:assetURL toAlbum:albumName];
            } failureBlock:nil];
        }
    } failureBlock: nil];
}

-(IBAction)onAddNewContact:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int nPos=btn.tag;
    nPos=nPos-48000;
    
    Message *message = [arrWholetableArray objectAtIndex:nPos];
//    NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
//    Message *message = [reversed objectAtIndex:nPos];

    
    
    CNContactStore *store = [[CNContactStore alloc] init];
    
    // create contact
    
    CNMutableContact *contact = [[CNMutableContact alloc] init];
  //  contact.familyName = @"Gopalsamy";
    contact.givenName = message.contactName;
    
    CNLabeledValue *homePhone = [CNLabeledValue labeledValueWithLabel:CNLabelHome value:[CNPhoneNumber phoneNumberWithStringValue:message.contactNo]];
    contact.phoneNumbers = @[homePhone];
    
    CNContactViewController *controller = [CNContactViewController viewControllerForUnknownContact:contact];
    controller.contactStore = store;
    controller.title=@"Add Contact";
    UIBarButtonItem *barBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"BackButton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onBack:)];
    [controller.navigationItem setLeftBarButtonItem:barBtn ];

    controller.delegate = self;
    
    [self.navigationController pushViewController:controller animated:TRUE];
    
    
   /* CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    if (status == CNAuthorizationStatusDenied || status == CNAuthorizationStatusRestricted) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Access to contacts." message:@"This app requires access to contacts because ..." preferredStyle:UIAlertControllerStyleActionSheet];
        [alert addAction:[UIAlertAction actionWithTitle:@"Go to Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alert animated:TRUE completion:nil];
        return;
    }
    
    CNContactStore *store = [[CNContactStore alloc] init];
    
    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (!granted) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // user didn't grant access;
                // so, again, tell user here why app needs permissions in order  to do it's job;
                // this is dispatched to the main queue because this request could be running on background thread
            });
            return;
        }
        
        // create contact
        
        CNMutableContact *contact = [[CNMutableContact alloc] init];
        contact.familyName = message.contactName;
      //  contact.givenName = @"John";
        
        CNLabeledValue *homePhone = [CNLabeledValue labeledValueWithLabel:CNLabelHome value:[CNPhoneNumber phoneNumberWithStringValue:message.contactNo]];
        contact.phoneNumbers = @[homePhone];
        
        CNSaveRequest *request = [[CNSaveRequest alloc] init];
        [request addContact:contact toContainerWithIdentifier:nil];
        
        // save it
        
        NSError *saveError;
        if (![store executeSaveRequest:request error:&saveError]) {
            NSLog(@"error = %@", saveError);
        }
    }];
    */
}
-(IBAction)onInvite:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int nPos=btn.tag;
    nPos=nPos-45000;
//    NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
//    Message *message = [reversed objectAtIndex:nPos];

    Message *message = [arrWholetableArray objectAtIndex:nPos];
    NSLog(@"%@",message.contactName);
     NSLog(@"%@",message.contactNo);
    
    MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
    picker.messageComposeDelegate = self;
    picker.recipients = [NSArray arrayWithObjects:message.contactNo, nil];
    picker.body = @"Check out Acedais Messenger for your smartphone. Download it today from ";
    //https://www.pyramidions.com/
   [self presentViewController:picker animated:NO completion:NULL];
    
    
    
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            NSLog(@"Cancelled");
            break;
        case MessageComposeResultFailed:
           
            break;
        case MessageComposeResultSent:
            
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:NO completion:NULL];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"SingChat_SingProfile"])
    {
        SingleProfileVC *Single=[segue destinationViewController];
        Single.dictDetails=dictDetails;
    }
    else if ([[segue identifier] isEqualToString:@"SingleChat_ShareLoc"])
    {
        ToShareLocationVC *Share=[segue destinationViewController];
        Share.strZoeChatId=strZoeChatId;
        Share.strChatRoomType=strChatRoomType;
    }
    else if ([[segue identifier] isEqualToString:@"SingleChat_Doodle"])
    {
        DoodleVC *Share=[segue destinationViewController];
        Share.strZoeChatId=strZoeChatId;
        Share.strChatRoomType=strChatRoomType;
    }
    else if ([[segue identifier] isEqualToString:@"SingleChat_ToShowLocation"])
    {
        ToShowLocation *Show=[segue destinationViewController];
        Show.strLat=strLat;
        Show.strLong=strLong;
        Show.strName=strNameToSend;
        Show.isShowDistance=isShowDistance;
    }
    else if ([[segue identifier] isEqualToString:@"SingleChat_VoiceCallVC"])
    {
         NSString *strImage=[dictDetails valueForKey:@"image"];
        
        VoiceCallVC *Details=[segue destinationViewController];
        Details.strZoeChatID=strZoeChatId;
        Details.strName=strName;
        Details.strimage=strImage;
        Details.strInComing=@"no";
    }
    else if ([[segue identifier] isEqualToString:@"SingleChat_VideoCallVC"])
    {
        NSString *strImage=[dictDetails valueForKey:@"image"];
        
        VoiceCallVC *Details=[segue destinationViewController];
        Details.strZoeChatID=strZoeChatId;
        Details.strName=strName;
        Details.strimage=strImage;
        Details.strInComing=@"no";
    }
    else if ([[segue identifier] isEqualToString:@"SingleChat_ShowAllContacts"])
    {
        ShowAllContactsVC *Share=[segue destinationViewController];
        Share.strZoeChatId=strZoeChatId;
        Share.strChatRoomType=strChatRoomType;
    }
    else if ([[segue identifier] isEqualToString:@"ToForwardMsg"])
    {
        ForwardMsgVC *Share=[segue destinationViewController];
        Share.strZoeChatId=strZoeChatId;
        Share.strChatRoomType=strChatRoomType;
        NSInteger rowNumber = 0;
        
        for (NSInteger i = 0; i < deleteIndexPth.section; i++) {
            rowNumber += [self tableView:self.tableSingleChat numberOfRowsInSection:i];
        }
        rowNumber += deleteIndexPth.row;
        
        Message *msg;
        msg = [arrWholetableArray objectAtIndex:rowNumber];

        /*
        NSArray *arrChatMsgs=[[NSArray alloc]init];
        if ([strChatRoomType isEqualToString:@"1"])
        {
            arrChatMsgs = [db_class getGroupMessages:strZoeChatId];
            NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
            if (arrChatMsgs.count > 10)
            {
                NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
                msg = [reversed objectAtIndex:rowNumber];
                
            }
            else{
                msg = [arrWholetableArray objectAtIndex:rowNumber];
                
            }
        }
        else
        {
            arrChatMsgs = [db_class getOneToOneChatMessages:strZoeChatId];
            NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
            if (arrChatMsgs.count > 10)
            {
                NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
                msg = [reversed objectAtIndex:rowNumber];
                
            }
            else{
                msg = [arrWholetableArray objectAtIndex:rowNumber];
                
            }
        }
         */
        
//        NSArray *reversed = [[arrWholetableArray reverseObjectEnumerator] allObjects];
//        Message *msg = [reversed objectAtIndex:rowNumber];
        NSLog(@"%@",msg.chat_id);
        NSLog(@"%@",msg.type);
        NSLog(@"%@",msg.text);
        if ([msg.type isEqualToString:@"contact"]) {
            
            Share.textString = @"Contact";
            Share.typeString=msg.type;
            Share.ContactNumberString=msg.contactNo;
            Share.ContactNameString=msg.contactName;
            
        }
        else if ([msg.type isEqualToString:@"location"]) {
            
            Share.latitudeString=msg.Latitude;
            Share.longitudeString=msg.Longitude;
            Share.typeString=msg.type;
            Share.textString=msg.text;
            Share.captionString=msg.caption;

        }
        else
        {
        Share.typeString=msg.type;
        Share.textString=msg.text;
        Share.getLinkDes=msg.linkDescription;
        Share.getPreviewVal=msg.showPreview;
        Share.getLinkLogo=msg.linkLogo;
        Share.getLinkTitle=msg.linkTitle;
        }
        
    }
    
}


// 1485327798.161989

-(long long)convertDateToMilliseconds:(NSDate *)date
{
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    long long milliseconds=timeInSeconds*1000;
    return milliseconds;
}
-(NSDate *)convertMillisecondsToDate:(long long)milliSeconds
{
    NSDate *dat = [NSDate dateWithTimeIntervalSince1970:(milliSeconds / 1000.0)];
    return dat;
}

#pragma mark - InputbarDelegate

-(void)inputbarDidPressRightButton:(Inputbar *)inputbar
{
    BOOL success=NO;
    NSString *strMsgId = [[NSUUID UUID] UUIDString]; //id
//    appDelegate.heightStr = @"TRUE";
    
    // NSString *strTime=[dateFormatter stringFromDate:[NSDate date]];
    
    long long milliSeconds=[self convertDateToMilliseconds:[NSDate date]];
    NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
    NSLog(@"%@",strMilliSeconds);
    
    NSString *trimmedString = [inputbar.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    trimmedString = [trimmedString stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    if (![trimmedString isEqualToString:@""])
    {
        if (isAvailinChatsTable==NO)
        {
            success=[db_class insertChats:@"" chatRoomId:strZoeChatId chatRoomType:strChatRoomType sender:@"0" lastMessage:trimmedString lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"text" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
            if (success==YES)
            {
                isAvailinChatsTable=YES;
            }
        }
        else
        {
            success=[db_class updateLastMessage:strZoeChatId sender:@"0" lastMessage:trimmedString lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"text" unReadCount:@"0"];
            NSLog(@"success");
        }
        Message *message = [[Message alloc] init];
        
        
        
        NSString *str = trimmedString;
        NSString *getUrl;
        NSArray *arrString = [str componentsSeparatedByString:@" "];
        
        for(int i=0; i<arrString.count;i++){
            if([[arrString objectAtIndex:i] rangeOfString:@"http://"].location != NSNotFound)
                NSLog(@"%@", [arrString objectAtIndex:i]);
            getUrl=[arrString objectAtIndex:i];
        }
        
        
        
        
        
        
        
        if ([self validateUrl:trimmedString]){
        }
        else
        {
        success=[db_class insertChatMessages:strMsgId userId:strZoeChatId groupId:strZoeChatId chatRoomType:strChatRoomType content:trimmedString contentType:@"text" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
            
            
           
            message.text = trimmedString;
            message.date = [self convertMillisecondsToDate:[strMilliSeconds longLongValue]];
            message.chat_id = strMsgId;
            message.type=@"text"; //text
            //Store Message in memory
            [self.tableArray addObject:message];
            [arrWholetableArray addObject:message];
            
            
            //Insert Message in UI
            NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
            [self.tableSingleChat beginUpdates];
            if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
                [self.tableSingleChat insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                                    withRowAnimation:UITableViewRowAnimationNone];
            [self.tableSingleChat insertRowsAtIndexPaths:@[indexPath]
                                        withRowAnimation:UITableViewRowAnimationBottom];
            [self.tableSingleChat endUpdates];
            
            [self.tableSingleChat scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                                        atScrollPosition:UITableViewScrollPositionBottom animated:YES];

        }
        
        
        
        if ([strChatRoomType isEqualToString:@"1"])
        {
            [appDelegate.socket emit:@"sendMessage" with:@[@{@"from": appDelegate.strID,@"to": strZoeChatId,@"content": trimmedString,@"contentType":@"text",@"messageId": strMsgId,@"time":strMilliSeconds,@"chatRoomType":strChatRoomType,@"groupId":strZoeChatId}]];
        }
        else if ([strChatRoomType isEqualToString:@"2"])
        {
            
            NSMutableArray *arrPartis=[[NSMutableArray alloc]init];
            NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
            NSArray *arrdata=[db_class getParticipants:strZoeChatId];
            for (int i=0; i<arrdata.count; i++)
            {
                NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
                NSDictionary *dictCont=[arrdata objectAtIndex:i];
                NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"userId"]];
                [dictParti setObject:strPhone forKey:@"participantId"];
                [arrPartis addObject:dictParti];
                NSString *BCstrMsgId = [[NSUUID UUID] UUIDString]; //id

                BOOL isBroadcastChat;
                isBroadcastChat=NO;
                isBroadcastChat=[db_class doesChatExist:strPhone];
                
                if (isBroadcastChat==NO)
                {
                    success=[db_class insertChats:@"" chatRoomId:strPhone chatRoomType:@"0" sender:@"0" lastMessage:trimmedString lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"text" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
                    if (success==YES)
                    {
                        isBroadcastChat=YES;
                    }
                }
                else
                {
                    success=[db_class updateLastMessage:strPhone sender:@"0" lastMessage:trimmedString lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"text" unReadCount:@"0"];
                    NSLog(@"success");
                }
                success=[db_class insertChatMessages:BCstrMsgId userId:strPhone groupId:strPhone chatRoomType:@"0" content:trimmedString contentType:@"text" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                
                
                
                
            }
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrPartis options:0 error:&error];
            NSString *jsonString;
            
            if (! jsonData)
            {
                NSLog(@"Got an error: %@", error);
            }
            else
            {
                jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            }
//            jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
//            jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
            jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
            
            
            
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setValue:appDelegate.strID forKey:@"from"];
            [dictParam setValue:jsonString forKey:@"participantId"];
            
            [appDelegate.socket emit:@"sendBroadcast" with:@[@{@"from": appDelegate.strID,@"participants": jsonString,@"content":trimmedString,@"contentType": @"text",@"messageId": strMsgId,@"time":strMilliSeconds,@"chatRoomType":@"0",@"groupId":@""}]];
            
            
           
            
            
        }
               else
        {
            
//            NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:@"(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))" options:NSRegularExpressionCaseInsensitive error:NULL];
//            NSString *someString = trimmedString;
//            NSString *match = [someString substringWithRange:[expression rangeOfFirstMatchInString:someString options:NSMatchingCompleted range:NSMakeRange(0, [someString length])]];
//            NSLog(@"%@", match);
            if (isPreview) {
              
            if ([self validateUrl:trimmedString]) {
                NSURL *myURL = [NSURL URLWithString:trimmedString];
                NSData *HtmlData = [NSData dataWithContentsOfURL:myURL];

                               TFHpple *doc = [TFHpple hppleWithHTMLData:HtmlData];
                NSString* getData = @"//head/meta";
                NSArray *Nodes = [doc searchWithXPathQuery:getData];
                for (TFHppleElement *element in Nodes)
                {
                    [self handleElement:element];
                }
                }
            
            else
            {
            [appDelegate.socket emit:@"sendMessage" with:@[@{@"from": appDelegate.strID,@"to": strZoeChatId,@"content": trimmedString,@"contentType":@"text",@"messageId": strMsgId,@"time":strMilliSeconds,@"chatRoomType":strChatRoomType,@"groupId":@""}]];
            }
            }
            
                else
                {
                    BOOL success=[db_class insertChatMessages:strMsgId userId:strZoeChatId groupId:strZoeChatId chatRoomType:strChatRoomType content:trimmedString contentType:@"text" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                    message.text = trimmedString;
                    message.date = [self convertMillisecondsToDate:[strMilliSeconds longLongValue]];
                    message.chat_id = strMsgId;
                    message.type=@"text"; //text
                    //Store Message in memory
                    [self.tableArray addObject:message];
                    [arrWholetableArray addObject:message];
                    
                    
                    //Insert Message in UI
                    NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
                    [self.tableSingleChat beginUpdates];
                    if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
                        [self.tableSingleChat insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                                            withRowAnimation:UITableViewRowAnimationNone];
                    [self.tableSingleChat insertRowsAtIndexPaths:@[indexPath]
                                                withRowAnimation:UITableViewRowAnimationBottom];
                    [self.tableSingleChat endUpdates];
                    
                    [self.tableSingleChat scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                                                atScrollPosition:UITableViewScrollPositionBottom animated:YES];
                    [appDelegate.socket emit:@"sendMessage" with:@[@{@"from": appDelegate.strID,@"to": strZoeChatId,@"content": trimmedString,@"contentType":@"text",@"messageId": strMsgId,@"time":strMilliSeconds,@"chatRoomType":strChatRoomType,@"groupId":@""}]];
                    isPreview=YES;
                    self.linkPreviewView.hidden=YES;


                }
                
            
        }
        
        
        
    }
    
}

- (BOOL) validateUrl: (NSString *) candidate {
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
}
- (void)handleElement:(TFHppleElement *)element
{
    
    NSString *trimmedString = [_inputbar.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    NSString *strMsgId = [[NSUUID UUID] UUIDString]; //id
    long long milliSeconds=[self convertDateToMilliseconds:[NSDate date]];
    NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
    NSLog(@"%@",strMilliSeconds);
    
        [MTDURLPreview loadPreviewWithURL:[NSURL URLWithString:trimmedString] completion:^(MTDURLPreview *preview, NSError *error) {
            
            NSLog(@"%@",preview.title);
            NSLog(@"%@",preview.domain);
            NSLog(@"%@",preview.content);
            NSLog(@"%@",preview.imageURL);
            NSString *getDescription;
            if ([element.attributes[@"name"] isEqualToString:@"description"])
            {
                getDescription=element.attributes[@"content"];
                NSLog(@"%@",getDescription);
                NSString *imgStr=[preview.imageURL absoluteString];
                
                
                
                
                BOOL success=[db_class insertChatMessages:strMsgId userId:strZoeChatId groupId:strZoeChatId chatRoomType:strChatRoomType content:trimmedString contentType:@"text" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"1" linkTitle:preview.title linkLogo:imgStr linkDescription:getDescription];
                
//                BOOL succ=[db_class insertLinks:@"" title:preview.title description:getDescription logo:imgStr url:trimmedString];
                BOOL succ=[db_class insertLinks:preview.title description:getDescription logo:imgStr url:trimmedString];
                Message *message = [[Message alloc] init];
                message.text = trimmedString;
                message.date = [self convertMillisecondsToDate:[strMilliSeconds longLongValue]];
                message.chat_id = strMsgId;
                message.type=@"text"; //text
                message.showPreview=@"1";
                message.linkLogo=imgStr;
                message.linkDescription=getDescription;
                message.linkTitle=preview.title;
                //Store Message in memory
                [self.tableArray addObject:message];
                [arrWholetableArray addObject:message];
                
                
                //Insert Message in UI
                NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
                [self.tableSingleChat beginUpdates];
                if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
                    [self.tableSingleChat insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                                        withRowAnimation:UITableViewRowAnimationNone];
                [self.tableSingleChat insertRowsAtIndexPaths:@[indexPath]
                                            withRowAnimation:UITableViewRowAnimationBottom];
                [self.tableSingleChat endUpdates];
                
                [self.tableSingleChat scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                                            atScrollPosition:UITableViewScrollPositionBottom animated:YES];
                
                [appDelegate.socket emit:@"sendMessage" with:@[@{@"from": appDelegate.strID,@"to": strZoeChatId,@"content":trimmedString,@"contentType":@"text",@"messageId": strMsgId,@"time":strMilliSeconds,@"chatRoomType":strChatRoomType,@"groupId":@"",@"showPreview":@"1",@"metaTitle":message.linkTitle,@"metaDescription":message.linkDescription,@"metaLogo":imgStr}]];
                
                self.linkPreviewView.hidden=YES;

                
            }
//            else
//            {
//               
//                
//                
//                
//                
//                BOOL success=[db_class insertChatMessages:strMsgId userId:strZoeChatId groupId:strZoeChatId chatRoomType:strChatRoomType content:trimmedString contentType:@"text" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"1" linkTitle:preview.title linkLogo:@"" linkDescription:getDescription];
//                
//                              BOOL succ=[db_class insertLinks:preview.title description:getDescription logo:@"" url:trimmedString];
//                Message *message = [[Message alloc] init];
//                message.text = trimmedString;
//                message.date = [self convertMillisecondsToDate:[strMilliSeconds longLongValue]];
//                message.chat_id = strMsgId;
//                message.type=@"text"; //text
//                message.showPreview=@"1";
//                message.linkLogo=@"vbfggbd";
//                message.linkDescription=getDescription;
//                message.linkTitle=preview.title;
//                //Store Message in memory
//                [self.tableArray addObject:message];
//                [arrWholetableArray addObject:message];
//                
//                
//                //Insert Message in UI
//                NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
//                [self.tableSingleChat beginUpdates];
//                if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
//                    [self.tableSingleChat insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
//                                        withRowAnimation:UITableViewRowAnimationNone];
//                [self.tableSingleChat insertRowsAtIndexPaths:@[indexPath]
//                                            withRowAnimation:UITableViewRowAnimationBottom];
//                [self.tableSingleChat endUpdates];
//                
//                [self.tableSingleChat scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
//                                            atScrollPosition:UITableViewScrollPositionBottom animated:YES];
//                
//                [appDelegate.socket emit:@"sendMessage" with:@[@{@"from": appDelegate.strID,@"to": strZoeChatId,@"content":trimmedString,@"contentType":@"text",@"messageId": strMsgId,@"time":strMilliSeconds,@"chatRoomType":strChatRoomType,@"groupId":@"",@"showPreview":@"1",@"metaTitle":message.linkTitle,@"metaDescription":@"",@"metaLogo":@""}]];
//                
//                self.linkPreviewView.hidden=YES;
            
                
           // }
            
            
            
            
        }];
    
    
    
}

-(void)inputbarDidPressLeftButton:(Inputbar *)inputbar
{
    [_inputbar resignFirstResponder];
    [self RemoveViewAttachments];
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    //Use camera if device has one otherwise use photo library
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    else
    {
        [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
    
    [imagePicker setDelegate:self];
    
    //Show image picker
    [self presentViewController:imagePicker animated:YES completion:nil];

    
    
}
-(void)inputbarDidChangeHeight:(CGFloat)new_height
{
    //Update DAKeyboardControl
    self.view.keyboardTriggerOffset = new_height;
}


-(void)getChatMessages
{
    NSMutableArray *data=[[NSMutableArray alloc]init];
    NSArray *arrChatMsgs=[[NSArray alloc]init];
    data=[db_class getChatMessages];
    
    if (data.count!=0)
    {
        for (int i=0; i<data.count; i++)
        {
            getSingleSentTime = [[NSMutableArray alloc]init];
            NSMutableDictionary *dictSing=[data objectAtIndex:i];
            NSDictionary *dictInfo = [db_class getUserInfo:[dictSing valueForKey:@"userId"]];
            if (dictInfo.count!=0)
            {
                [ dictSing setObject:[dictInfo valueForKey:@"name"] forKey:@"name"];
            }
            else
            {
                NSString *strNme=[NSString stringWithFormat:@"%@",[dictSing valueForKey:@"userId"]];
                [ dictSing setObject:strNme forKey:@"name"];
            }
            NSLog(@"%@",dictSing);
            [data replaceObjectAtIndex:i withObject:dictSing];
            
        }
    }
    
    if (data.count!=0)
    {
        for (int i=0; i<data.count; i++)
        {
            NSDictionary *dict=[data objectAtIndex:i];
            NSString *strIsDownloaded=[NSString stringWithFormat:@"%@",[dict valueForKey:@"isDownloaded"]];
            if ([strChatRoomType isEqualToString:@"0"])
            {
                NSString *strsndr=[NSString stringWithFormat:@"%@",[dict valueForKey:@"sender"]];
                if ([[dict valueForKey:@"userId"]isEqualToString:strZoeChatId] && [[dict valueForKey:@"chatRoomType"]isEqualToString:@"0"])
                {
                    
                    Message *message = [[Message alloc] init];
                    
                    message.type=[dict valueForKey:@"contentType"];
                    message.contentType=[dict valueForKey:@"contentType"];
                    
                    if ([message.type isEqualToString:@"image"])
                    {
                        message.text = [dict valueForKey:@"content"];
                        
                        if ([strsndr isEqualToString:@"1"])
                        {
                            if ([strIsDownloaded isEqualToString:@"1"])
                            {
                                NSString *strContent=[NSString stringWithFormat:@"%@", [dict valueForKey:@"content"]];
                                NSArray *components = [strContent componentsSeparatedByString:@"/"];
                                NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                                
                                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
                                NSString *documentsDirectory = [paths objectAtIndex:0];
                                NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
                                message.image = [UIImage imageWithContentsOfFile:getImagePath];
                            }
                            
                                else{

                                NSString *strContent=[NSString stringWithFormat:@"%@", [dict valueForKey:@"content"]];
                                NSArray *components = [strContent componentsSeparatedByString:@"/"];
                                NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                                
                                strImageName=[NSString stringWithFormat:@"%@thumb_%@",FILE_AWS_IMAGE_BASE_URL, strImageName];
                                
                                NSURL *imageURL = [NSURL URLWithString:strImageName];
                                //                                NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                                //                                message.image = [UIImage imageWithData:imageData];
                                SDWebImageManager *manager = [SDWebImageManager sharedManager];
                                [manager downloadImageWithURL:imageURL
                                                      options:0
                                                     progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                         // progression tracking code, optional.
                                                         // You can set the progress block to nil
                                                     }
                                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                                        
                                                        if (image) {
                                                            // Resize your image here, then set it to UIImageView
                                                            NSIndexPath *index = [NSIndexPath indexPathForRow:i inSection:0];
                                                            
                                                            
                                                            message.image =image;
                                                            [_tableSingleChat reloadData];
                                                            //                                                            [_tableSingleChat reloadRowsAtIndexPaths:[NSArray arrayWithObject:index] withRowAnimation:UITableViewRowAnimationNone];
                                                            // resize to 0.5x, function available since iOS 6
                                                        }
                                                    }];
                            }
                            
                        }
                        else{
                            NSString *strContent=[NSString stringWithFormat:@"%@", [dict valueForKey:@"content"]];
                            NSArray *components = [strContent componentsSeparatedByString:@"/"];
                            NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                            
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
                            NSString *documentsDirectory = [paths objectAtIndex:0];
                            NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
                            message.image = [UIImage imageWithContentsOfFile:getImagePath];
                            
                        }
                        
                    }
                    else if ([message.type isEqualToString:@"location"])
                    {
//                        if ([strsndr isEqualToString:@"1"])
//                        {

                        NSString *strImageName=[NSString stringWithFormat:@"%@", [dict valueForKey:@"content"]];
                        
                        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
                        NSString *documentsDirectory = [paths objectAtIndex:0];
                        NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
                        message.image = [UIImage imageWithContentsOfFile:getImagePath];
                        message.text = [dict valueForKey:@"content"];
                        message.Latitude=[dict valueForKey:@"latitude"];
                        message.Longitude=[dict valueForKey:@"longitude"];
//                        }
//                        else
//                        {
//                            
//                            NSString *strImageName=[NSString stringWithFormat:@"%@", [dict valueForKey:@"content"]];
//                            
//                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
//                            NSString *documentsDirectory = [paths objectAtIndex:0];
//                            NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
//                            message.image = [UIImage imageWithContentsOfFile:getImagePath];
//                            message.text = [dict valueForKey:@"content"];
//                            message.Latitude=[dict valueForKey:@"latitude"];
//                            message.Longitude=[dict valueForKey:@"longitude"];
//                        }
                        
                    }
                    else if ([message.type isEqualToString:@"video"])
                    {
                        if ([strsndr isEqualToString:@"0"])
                        {
                            NSString *strContent=[NSString stringWithFormat:@"%@", [dict valueForKey:@"content"]];
                            NSArray *components = [strContent componentsSeparatedByString:@"/"];
                            NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
                            NSString *documentsDirectory = [paths objectAtIndex:0];
                            NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
                            NSURL *assetURL=[NSURL fileURLWithPath:getImagePath];
                            message.image = [self thumbnailImageFromURL:assetURL];
                            message.text = [dict valueForKey:@"content"];
                        }
                        else if ([strsndr isEqualToString:@"1"])
                        {
                            
                            NSString *strContent=[NSString stringWithFormat:@"%@", [dict valueForKey:@"content"]];
                            NSArray *components = [strContent componentsSeparatedByString:@"/"];
                            NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                            NSString *name = strImageName;
                            NSString *S1 = [name componentsSeparatedByString:@"."][0];
                            NSString *S2 = [NSString stringWithFormat:@"%@.jpg",S1];
                            NSLog(@"%@",S1);
                            NSLog(@"%@",S2);
                            
                            strImageName=[NSString stringWithFormat:@"%@thumb_%@",FILE_AWS_IMAGE_BASE_URL, S2];
                            //                            UIImageView *getImg = [[UIImageView alloc]init];
                            //
                            //
                            //                            [getImg sd_setImageWithURL:[NSURL URLWithString:strImageName]placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
                            //                            message.image = getImg.image;
                            NSURL *imageURL = [NSURL URLWithString:strImageName];
                            //                            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                            //                            message.image = [UIImage imageWithData:imageData];
                            //                            message.text = [dict valueForKey:@"content"];
                            // [_tableSingleChat reloadData];
                            
                            SDWebImageManager *manager = [SDWebImageManager sharedManager];
                            [manager downloadImageWithURL:imageURL
                                                  options:0
                                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                     // progression tracking code, optional.
                                                     // You can set the progress block to nil
                                                 }
                                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                                    if (image) {
                                                        message.text = [dict valueForKey:@"content"];
                                                        // Resize your image here, then set it to UIImageView
                                                        message.image =image;
                                                        [_tableSingleChat reloadData];
                                                        
                                                        // resize to 0.5x, function available since iOS 6
                                                    }
                                                }];
                            
                            
                        }
                    }
                    else if ([message.type isEqualToString:@"contact"])
                    {
                        message.contactName = [dict valueForKey:@"contactName"];
                        message.contactNo = [dict valueForKey:@"contactNumber"];
                    }
                    else if ([message.type isEqualToString:@"sticker"])
                    {
                         message.text = [dict valueForKey:@"content"];
                    }
                    else if ([message.type isEqualToString:@"document"])
                    {
                        
                        message.text = [dict valueForKey:@"content"];
                    }

                    else
                    {
//                        NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:@"(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))" options:NSRegularExpressionCaseInsensitive error:NULL];
//                        NSString *someString = [dict valueForKey:@"content"];
//                        NSString *match = [someString substringWithRange:[expression rangeOfFirstMatchInString:someString options:NSMatchingCompleted range:NSMakeRange(0, [someString length])]];
//                        NSLog(@"%@", match);

                        
                        if ([self validateUrl:[dict valueForKey:@"content"]]){
                            message.text = [dict valueForKey:@"content"];
                            message.linkLogo = [dict valueForKey:@"linkLogo"];
                            message.linkDescription = [dict valueForKey:@"linkDescription"];
                            message.linkTitle = [dict valueForKey:@"linkTitle"];
                            message.showPreview = [dict valueForKey:@"showPreview"];



                        }
                        else
                        {
                        message.text = [dict valueForKey:@"content"];
                        }
                    }
                    message.date=[self convertMillisecondsToDate:[[dict valueForKey:@"sentTime"]longLongValue]];
                    message.chat_id = [dict valueForKey:@"id"];
                    message.caption=[dict valueForKey:@"caption"];
                    message.checkStar=[dict valueForKey:@"checkStar"];
                    message.seenTime=[dict valueForKey:@"seenTime"];
                    message.deliveredTime=[dict valueForKey:@"deliveredTime"];

                    
                    if ([strsndr isEqualToString:@"0"])
                    {
                        message.sender=MessageSenderMyself;
                        if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"sending"])
                        {
                            message.status = MessageStatusSending;
                        }
                        else if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"sent"])
                        {
                            message.status = MessageStatusSent;
                        }
                        else if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"delivered"])
                        {
                            message.status = MessageStatusReceived;
                        }                        else if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"read"])
                        {
                            message.status = MessageStatusRead;
                        }
                        else if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"cancelled"])
                        {
                            message.status = MessageStatusCancelled;
                        }
                        else if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"uploaded"])
                        {
                            message.status = MessageStatusUploaded;
                        }
                        
                        
                    }
                    else
                    {
                        message.sender=MessageSenderSomeone;
                        message.status = MessageStatusRead;
                        NSString *strSeenTime=[NSString stringWithFormat:@"%@",[dict valueForKey:@"seenTime"]];
                        //  BOOL  success=[db_class updateContentStatus:message.chat_id contentStatus:@"read"];
                        if ([strSeenTime isEqualToString:@"0"])
                        {
                            long long milli=[self convertDateToMilliseconds:[NSDate date]];
                            NSString *strMsgTime=[NSString stringWithFormat:@"%lld",milli];
                            NSString *strMsgId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"id"]];
                            NSString *strMsgSender=[NSString stringWithFormat:@"%@",[dict valueForKey:@"userId"]];
                            BOOL success=NO;
                            success=[db_class updateSeenTime:strMsgId seenTime:strMsgTime];
                            
                            if ([strChatRoomType isEqualToString:@"0"])
                            {
                                [appDelegate.socket emit:@"sendSeen" with:@[@{@"messageId":strMsgId,@"from":strMsgSender,@"to": appDelegate.strID,@"time":strMsgTime,@"chatRoomType":strChatRoomType}]];
                            }
                            else
                            {
                                [appDelegate.socket emit:@"sendSeen" with:@[@{@"messageId":strMsgId,@"from":strMsgSender,@"to": strZoeChatId,@"time":strMsgTime,@"chatRoomType":strChatRoomType}]];
                            }
                            
                            
                            NSDictionary *dictSing=[db_class getSingleChat:strMsgSender];
                            
                            if ([[dictSing valueForKey:@"lastMessageTime"]isEqualToString:[dict valueForKey:@"sentTime"]])
                            {
                                success=[db_class updateLastMsgStatus:strMsgSender lastMsgStatus:@"read"];
                            }
                            
                        }
                        if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"downloaded"])
                        {
                            message.status = MessageStatusDownloaded;
                        }
                        else if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"downloading"])
                        {
                            message.status = MessageStatusDownloading;
                        }
                    }
                    [self.tableArray addObject:message];
                    [arrWholetableArray addObject:message];
                }
                
            }
            else
            {
                NSLog(@"%@",dict);
                NSString *strsndr=[NSString stringWithFormat:@"%@",[dict valueForKey:@"sender"]];
                if ([[dict valueForKey:@"groupId"]isEqualToString:strZoeChatId])
                {
                    
                    Message *message = [[Message alloc] init];
                    
                    message.type=[dict valueForKey:@"contentType"];
                    if ([message.type isEqualToString:@"image"])
                    {
                        message.text = [dict valueForKey:@"content"];
                        
                        if ([strsndr isEqualToString:@"1"])
                        {
                            if ([strIsDownloaded isEqualToString:@"1"])
                            {
                                NSString *strContent=[NSString stringWithFormat:@"%@", [dict valueForKey:@"content"]];
                                NSArray *components = [strContent componentsSeparatedByString:@"/"];
                                NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                                
                                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
                                NSString *documentsDirectory = [paths objectAtIndex:0];
                                NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
                                message.image = [UIImage imageWithContentsOfFile:getImagePath];
                            }
                            else
                            {
                                NSString *strContent=[NSString stringWithFormat:@"%@", [dict valueForKey:@"content"]];
                                NSArray *components = [strContent componentsSeparatedByString:@"/"];
                                NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                                
                                strImageName=[NSString stringWithFormat:@"%@thumb_%@",FILE_AWS_IMAGE_BASE_URL, strImageName];
                                
                                NSURL *imageURL = [NSURL URLWithString:strImageName];
                                //                                NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                                SDWebImageManager *manager = [SDWebImageManager sharedManager];
                                [manager downloadImageWithURL:imageURL
                                                      options:0
                                                     progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                         // progression tracking code, optional.
                                                         // You can set the progress block to nil
                                                     }
                                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                                        if (image) {
                                                            // Resize your image here, then set it to UIImageView
                                                            message.image =image;
                                                            // resize to 0.5x, function available since iOS 6
                                                        }
                                                    }];
                                //                                message.image = [UIImage imageWithData:imageData];
                            }
                        }
                        else{
                            NSString *strContent=[NSString stringWithFormat:@"%@", [dict valueForKey:@"content"]];
                            NSArray *components = [strContent componentsSeparatedByString:@"/"];
                            NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                            
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
                            NSString *documentsDirectory = [paths objectAtIndex:0];
                            NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
                            message.image = [UIImage imageWithContentsOfFile:getImagePath];
                            
                        }
                        
                    }
                    else if ([message.type isEqualToString:@"location"])
                    {
//                        if ([strsndr isEqualToString:@"1"])
//                        {
                        
                            NSString *strImageName=[NSString stringWithFormat:@"%@", [dict valueForKey:@"content"]];
                            
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
                            NSString *documentsDirectory = [paths objectAtIndex:0];
                            NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
                            message.image = [UIImage imageWithContentsOfFile:getImagePath];
                            message.text = [dict valueForKey:@"content"];
                            message.Latitude=[dict valueForKey:@"latitude"];
                            message.Longitude=[dict valueForKey:@"longitude"];
//                        }
//                        else
//                        {
//                            
//                            NSString *strImageName=[NSString stringWithFormat:@"%@", [dict valueForKey:@"content"]];
//                            
//                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
//                            NSString *documentsDirectory = [paths objectAtIndex:0];
//                            NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
//                            message.image = [UIImage imageWithContentsOfFile:getImagePath];
//                            message.text = [dict valueForKey:@"content"];
//                            message.Latitude=[dict valueForKey:@"latitude"];
//                            message.Longitude=[dict valueForKey:@"longitude"];
//                        }
                        
                    }
                    else if ([message.type isEqualToString:@"video"])
                    {
                        if ([strsndr isEqualToString:@"0"])
                        {
                            NSString *strContent=[NSString stringWithFormat:@"%@", [dict valueForKey:@"content"]];
                            NSArray *components = [strContent componentsSeparatedByString:@"/"];
                            NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
                            NSString *documentsDirectory = [paths objectAtIndex:0];
                            NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
                            NSURL *assetURL=[NSURL fileURLWithPath:getImagePath];
                            message.image = [self thumbnailImageFromURL:assetURL];
                            message.text = [dict valueForKey:@"content"];
                        }
                        else if ([strsndr isEqualToString:@"1"])
                        {
                            
                            NSString *strContent=[NSString stringWithFormat:@"%@", [dict valueForKey:@"content"]];
                            NSArray *components = [strContent componentsSeparatedByString:@"/"];
                            NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                            NSString *name = strImageName;
                            NSString *S1 = [name componentsSeparatedByString:@"."][0];
                            NSString *S2 = [NSString stringWithFormat:@"%@.jpg",S1];
                            NSLog(@"%@",S1);
                            NSLog(@"%@",S2);
                            
                            strImageName=[NSString stringWithFormat:@"%@thumb_%@",FILE_AWS_IMAGE_BASE_URL, S2];
                            //                            UIImageView *getImg = [[UIImageView alloc]init];
                            //
                            //
                            //                            [getImg sd_setImageWithURL:[NSURL URLWithString:strImageName]placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
                            //                            message.image = getImg.image;
                            NSURL *imageURL = [NSURL URLWithString:strImageName];
                            //                            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                            //                            message.image = [UIImage imageWithData:imageData];
                            //                            message.text = [dict valueForKey:@"content"];
                            // [_tableSingleChat reloadData];
                            
                            SDWebImageManager *manager = [SDWebImageManager sharedManager];
                            [manager downloadImageWithURL:imageURL
                                                  options:0
                                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                     // progression tracking code, optional.
                                                     // You can set the progress block to nil
                                                 }
                                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                                    if (image) {
                                                        message.text = [dict valueForKey:@"content"];
                                                        // Resize your image here, then set it to UIImageView
                                                        message.image =image;
                                                        [_tableSingleChat reloadData];
                                                        
                                                        // resize to 0.5x, function available since iOS 6
                                                    }
                                                }];
                            
                            
                        }
                    }
                    else if ([message.type isEqualToString:@"contact"])
                    {
                        message.contactName = [dict valueForKey:@"contactName"];
                        message.contactNo = [dict valueForKey:@"contactNumber"];
                    }
                    else if ([message.type isEqualToString:@"document"])
                    {
                        
                        message.text = [dict valueForKey:@"content"];
                    }
                    else if ([message.type isEqualToString:@"sticker"])
                    {
                        message.text = [dict valueForKey:@"content"];
                    }
                    else
                    {
//                        NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:@"(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))" options:NSRegularExpressionCaseInsensitive error:NULL];
//                        NSString *someString = [dict valueForKey:@"content"];
//                        NSString *match = [someString substringWithRange:[expression rangeOfFirstMatchInString:someString options:NSMatchingCompleted range:NSMakeRange(0, [someString length])]];
//                        NSLog(@"%@", match);
                        
                        
                        if ([self validateUrl:[dict valueForKey:@"content"]]){
                            message.text = [dict valueForKey:@"content"];
                            message.linkLogo = [dict valueForKey:@"linkLogo"];
                            message.linkDescription = [dict valueForKey:@"linkDescription"];
                            message.linkTitle = [dict valueForKey:@"linkTitle"];
                            message.showPreview = [dict valueForKey:@"showPreview"];

                            
                            
                        }
                        else
                        {
                            message.text = [dict valueForKey:@"content"];
                        }
                    }
                    
                    message.date=[self convertMillisecondsToDate:[[dict valueForKey:@"sentTime"]longLongValue]];
                    message.chat_id = [dict valueForKey:@"id"];
                    message.caption=[dict valueForKey:@"caption"];
                    message.checkStar=[dict valueForKey:@"checkStar"];
                    message.seenTime=[dict valueForKey:@"seenTime"];
                    message.deliveredTime=[dict valueForKey:@"deliveredTime"];
                    if ([strsndr isEqualToString:@"0"])
                    {
                        message.sender=MessageSenderMyself;
                        if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"sending"])
                        {
                            message.status = MessageStatusSending;
                        }
                        else if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"sent"])
                        {
                            message.status = MessageStatusSent;
                        }
                        else if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"delivered"])
                        {
                            message.status = MessageStatusReceived;
                        }
                        else if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"read"])
                        {
                            message.status = MessageStatusRead;
                        }
                        else if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"cancelled"])
                        {
                            message.status = MessageStatusCancelled;
                        }
                        else if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"uploaded"])
                        {
                            message.status = MessageStatusUploaded;
                        }
                        
                        
                    }
                    else
                    {
                        message.sender=MessageSenderSomeone;
                        message.status = MessageStatusRead;
                        NSString *strMine=[NSString stringWithFormat:@"%@",appDelegate.strID];
                        if ([[dict valueForKey:@"name"] isEqualToString:strMine])
                        {
                            message.name=@"";
                        }
                        else
                        {
                            message.name=[dict valueForKey:@"name"];
                        }
                        
                        
                        NSString *strSeenTime=[NSString stringWithFormat:@"%@",[dict valueForKey:@"seenTime"]];
                        //  BOOL  success=[db_class updateContentStatus:message.chat_id contentStatus:@"read"];
                        if ([strSeenTime isEqualToString:@"0"])
                        {
                            long long milli=[self convertDateToMilliseconds:[NSDate date]];
                            NSString *strMsgTime=[NSString stringWithFormat:@"%lld",milli];
                            NSString *strMsgId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"id"]];
                            NSString *strMsgSender=[NSString stringWithFormat:@"%@",[dict valueForKey:@"userId"]];
                            BOOL success=NO;
                            success=[db_class updateSeenTime:strMsgId seenTime:strMsgTime];
                            
                            if ([strChatRoomType isEqualToString:@"0"])
                            {
                                [appDelegate.socket emit:@"sendSeen" with:@[@{@"messageId":strMsgId,@"from":strMsgSender,@"to": appDelegate.strID,@"time":strMsgTime,@"chatRoomType":strChatRoomType}]];
                            }
                            else
                            {
                                [appDelegate.socket emit:@"sendSeen" with:@[@{@"messageId":strMsgId,@"from":strMsgSender,@"to": strZoeChatId,@"time":strMsgTime}]];
                            }
                            
                            NSDictionary *dictSing=[db_class getSingleChat:strMsgSender];
                            
                            if ([[dictSing valueForKey:@"lastMessageTime"]isEqualToString:[dict valueForKey:@"sentTime"]])
                            {
                                success=[db_class updateLastMsgStatus:strMsgSender lastMsgStatus:@"read"];
                            }
                            
                        }
                        if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"downloaded"])
                        {
                            message.status = MessageStatusDownloaded;
                        }
                        else if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"downloading"])
                        {
                            message.status = MessageStatusDownloading;
                        }
                    }
                    [self.tableArray addObject:message];
                    [arrWholetableArray addObject:message];
                }
                
                
            }
            
            [getSingleSentTime addObject:[dict valueForKey:@"sentTime"]];
            
        }
        
        
    }
 
    [self.tableSingleChat reloadData];
  
    
    BOOL Success=NO;
    Success=[db_class updateUnreadCount:strZoeChatId unreadCount:@"0"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_App_Badge_Count" object:self];
}
/////Pagination

//-(void)callbackMtd:(int)set
//{
//    isPageRefreshing=NO;
//    setLimit = [@(set) stringValue];
//    [self getChatMessages];
//    if ([isFirst isEqualToString:@"YES"])
//    {
//        getNext = getNext+10;
//    }
//    else{
//        
//    }
//    
//    NSLog(@"%lu",(unsigned long)arrWholetableArray.count);
//    NSLog(@"%@",arrWholetableArray);
//
//    [self.tableSingleChat reloadData];
//}
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    
//    if (isScrolling)
//    {
//        isScrolling=NO;
//        [self scrollingStopped];
//    }
//}
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    isScrolling=YES;
//}
//- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
//{
//    isScrolling=YES;
//}
//-(void)scrollingStopped
//{
//
//    NSLog(@"Scrolling stopped");
//    NSLog(@"%f",self.tableSingleChat.contentOffset.y);
//    NSLog(@"%f",self.tableSingleChat.contentSize.height);
//    
//    NSLog(@"%f",self.tableSingleChat.bounds.size.height);
//    
//        NSArray *arrChatMsgs = [db_class getOneToOneChatMessages:strZoeChatId];
//        NSLog(@"%lu",(unsigned long)arrChatMsgs.count);
//        if (arrChatMsgs.count > 10)
//        {
//         
//    if(self.tableSingleChat.contentOffset.y >= (self.tableSingleChat.contentSize.height - self.tableSingleChat.bounds.size.height))
//    {
//        page=page+10;
//        if(isPageRefreshing==NO){
//            isPageRefreshing=YES;
//            isFirst=@"YES";
//            [self callbackMtd:page];
//            
//        }
//    }
//    }
//   
//}


- (IBAction)onAttachments:(id)sender
{
   // [viewAttachments removeFromSuperview]; 9884711142
    
    if (viewAttachments==nil)
    {
        [self.tableSingleChat removeGestureRecognizer:tapviewAttachments];
        
        viewAttachments=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, 130)];
        viewAttachments.backgroundColor=[UIColor whiteColor];
        viewAttachments.layer.borderWidth = 0.75;
        viewAttachments.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        viewAttachments.layer.cornerRadius = 2.0f;
        viewAttachments.clipsToBounds=YES;
        
        
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:viewAttachments.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(15.0, 15.0)];
        
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.view.bounds;
        maskLayer.path  = maskPath.CGPath;
        viewAttachments.layer.mask = maskLayer;
        
        viewAttachments.backgroundColor=[UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1.0];
        [viewAttachments.layer setCornerRadius:5.0f];
        [viewAttachments.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [viewAttachments.layer setBorderWidth:0.2f];
        [viewAttachments.layer setShadowColor:[UIColor colorWithRed:225.0/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1.0].CGColor];
        [viewAttachments.layer setShadowOpacity:1.0];
        [viewAttachments.layer setShadowRadius:5.0];
        [viewAttachments.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
        
        [self.view addSubview:viewAttachments];
        
        int xPosForButtons=self.view.frame.size.width/3;
        
        UIButton *btnPhotos=[UIButton buttonWithType:UIButtonTypeCustom];
        btnPhotos.frame=CGRectMake((xPosForButtons/2)-22.5, 10, 45, 45);
        UIImage *imgPto=[UIImage imageNamed:@"cam.png"];
        [btnPhotos setBackgroundImage:imgPto forState:UIControlStateNormal];
        [btnPhotos addTarget:self action:@selector(onPhotos:) forControlEvents:UIControlEventTouchDown];
        btnPhotos.layer.cornerRadius=22.5;
        btnPhotos.clipsToBounds=YES;
        [viewAttachments addSubview:btnPhotos];
        
        UILabel *lblPhotos=[[UILabel alloc]initWithFrame:CGRectMake((xPosForButtons/2)-22.5, 55, 45, 15)];
        lblPhotos.text=@"Photos";
        lblPhotos.textAlignment=NSTextAlignmentCenter;
        lblPhotos.font=[UIFont systemFontOfSize:10];
        [viewAttachments addSubview:lblPhotos];
        
        
        UIButton *btnAudio=[UIButton buttonWithType:UIButtonTypeCustom];
        btnAudio.frame=CGRectMake(((xPosForButtons/2)-22.5)+xPosForButtons, 10, 45, 45);
        UIImage *imgAud=[UIImage imageNamed:@"Doodle.png"];
        [btnAudio setBackgroundImage:imgAud forState:UIControlStateNormal];
        [btnAudio addTarget:self action:@selector(onDoodle:) forControlEvents:UIControlEventTouchDown];
        btnAudio.layer.cornerRadius=22.5;
        btnAudio.clipsToBounds=YES;
        [viewAttachments addSubview:btnAudio];
        
        UILabel *lblAudio=[[UILabel alloc]initWithFrame:CGRectMake(((xPosForButtons/2)-22.5)+xPosForButtons, 55, 45, 15)];
        lblAudio.text=@"Doodle";
        lblAudio.textAlignment=NSTextAlignmentCenter;
        lblAudio.font=[UIFont systemFontOfSize:10];
        [viewAttachments addSubview:lblAudio];
        
        UIButton *btnVideo=[UIButton buttonWithType:UIButtonTypeCustom];
        btnVideo.frame=CGRectMake(((xPosForButtons/2)-22.5)+(xPosForButtons*2), 10, 45, 45);
        UIImage *imgVid=[UIImage imageNamed:@"gal.png"];
        [btnVideo setBackgroundImage:imgVid forState:UIControlStateNormal];
        [btnVideo addTarget:self action:@selector(onVideo:) forControlEvents:UIControlEventTouchDown];
        btnVideo.layer.cornerRadius=22.5;
        btnVideo.clipsToBounds=YES;
        [viewAttachments addSubview:btnVideo];
        
        UILabel *lblVideo=[[UILabel alloc]initWithFrame:CGRectMake(((xPosForButtons/2)-22.5)+(xPosForButtons*2), 55, 45, 15)];
        lblVideo.text=@"Videos";
        lblVideo.textAlignment=NSTextAlignmentCenter;
        lblVideo.font=[UIFont systemFontOfSize:10];
        [viewAttachments addSubview:lblVideo];
        
        UIButton *btnLocation=[UIButton buttonWithType:UIButtonTypeCustom];
        btnLocation.frame=CGRectMake(xPosForButtons-22.5, 65, 45, 45);
        UIImage *imgLoc=[UIImage imageNamed:@"loc.png"];
        [btnLocation setBackgroundImage:imgLoc forState:UIControlStateNormal];
        [btnLocation addTarget:self action:@selector(onLocation:) forControlEvents:UIControlEventTouchDown];
        btnLocation.layer.cornerRadius=22.5;
        btnLocation.clipsToBounds=YES;
        [viewAttachments addSubview:btnLocation];
        
        UILabel *lblLocation=[[UILabel alloc]initWithFrame:CGRectMake(xPosForButtons-22.5, 110, 45, 15)];
        lblLocation.text=@"Location";
        lblLocation.textAlignment=NSTextAlignmentCenter;
        lblLocation.font=[UIFont systemFontOfSize:10];
        [viewAttachments addSubview:lblLocation];
        
        UIButton *btnContacts=[UIButton buttonWithType:UIButtonTypeCustom];
        btnContacts.frame=CGRectMake((xPosForButtons*2)-22.5, 65, 45, 45);
        UIImage *imgCont=[UIImage imageNamed:@"con.png"];
        [btnContacts setBackgroundImage:imgCont forState:UIControlStateNormal];
        [btnContacts addTarget:self action:@selector(onContacts:) forControlEvents:UIControlEventTouchDown];
        btnContacts.layer.cornerRadius=22.5;
        btnContacts.clipsToBounds=YES;
        [viewAttachments addSubview:btnContacts];
        
        UILabel *lblContact=[[UILabel alloc]initWithFrame:CGRectMake((xPosForButtons*2)-22.5, 110, 45, 15)];
        lblContact.text=@"Contacts";
        lblContact.textAlignment=NSTextAlignmentCenter;
        lblContact.font=[UIFont systemFontOfSize:10];
        [viewAttachments addSubview:lblContact];
        tapviewAttachments=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(RemoveViewAttachments)];
        [self.tableSingleChat addGestureRecognizer:tapviewAttachments];
        
        [UIView animateWithDuration:0.60 delay:0.0 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
            viewAttachments.frame = CGRectMake(0, 0, self.view.frame.size.width, 130);
        } completion:^(BOOL finished) {
            // your animation finished
        }];

    }
    else
    {
        [self RemoveViewAttachments];
    }
   
}

-(void)RemoveViewAttachments
{
    [UIView animateWithDuration:0.60 delay:0.0 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
        viewAttachments.frame = CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, 130);
    } completion:^(BOOL finished) {
        // your animation finished
        [viewAttachments removeFromSuperview];
        viewAttachments=nil;
    }];
    [self.tableSingleChat removeGestureRecognizer:tapviewAttachments];
}

-(IBAction)onPhotos:(id)sender
{
    [self RemoveViewAttachments];
    QBImagePickerController *imagePickerController = [QBImagePickerController new];
    imagePickerController.delegate = self;
    imagePickerController.allowsMultipleSelection = YES;
    imagePickerController.maximumNumberOfSelection = 5;
    imagePickerController.showsNumberOfSelectedAssets = YES;
    imagePickerController.mediaType = QBImagePickerMediaTypeImage;
   // imagePickerController.prompt = @"Select the photos you want to upload!";
    imagePickerController.numberOfColumnsInPortrait = 4;
    imagePickerController.numberOfColumnsInLandscape = 7;
    imagePickerController.assetCollectionSubtypes = @[
                                                      @(PHAssetCollectionSubtypeSmartAlbumUserLibrary), // Camera Roll
                                                      @(PHAssetCollectionSubtypeAlbumMyPhotoStream), // My Photo Stream
                                                      @(PHAssetCollectionSubtypeSmartAlbumPanoramas), // Panoramas
                                                      @(PHAssetCollectionSubtypeSmartAlbumBursts) // Bursts
                                                      ];
    [self presentViewController:imagePickerController animated:YES completion:NULL];
    
}
-(IBAction)onDoodle:(id)sender
{
    [self RemoveViewAttachments];
    [self performSegueWithIdentifier:@"SingleChat_Doodle" sender:self];
}

-(IBAction)onVideo:(id)sender
{
     [self RemoveViewAttachments];
    UIImagePickerController *videoPicker = [[UIImagePickerController alloc] init];
    videoPicker.delegate = self;
    videoPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
    videoPicker.mediaTypes =[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    videoPicker.mediaTypes = @[(NSString*)kUTTypeMovie, (NSString*)kUTTypeAVIMovie, (NSString*)kUTTypeVideo, (NSString*)kUTTypeMPEG4];
    videoPicker.videoQuality = UIImagePickerControllerQualityTypeHigh;
    [self presentViewController:videoPicker animated:YES completion:nil];
    
}

-(IBAction)onLocation:(id)sender
{
     [self RemoveViewAttachments];
    [self performSegueWithIdentifier:@"SingleChat_ShareLoc" sender:self];
}

-(IBAction)onContacts:(id)sender
{
      [self RemoveViewAttachments];
    [self performSegueWithIdentifier:@"SingleChat_ShowAllContacts" sender:self];
}


#pragma mark
#pragma mark - ImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *strMediaType=[NSString stringWithFormat:@"%@",[info valueForKey:@"UIImagePickerControllerMediaType"]];
    //NSLog(@"%@",strMediaType);
    
    if ([strMediaType isEqualToString:@"public.image"])
    {
        UIImage *imageToUpload=[self compressImage:info[UIImagePickerControllerOriginalImage]];
        NSURL *assetURL = [info objectForKey:UIImagePickerControllerReferenceURL];
        
        NSString *strURL=[NSString stringWithFormat:@"%@",assetURL];
        __block NSString *fileName = nil;
        
        NSArray *components = [strURL componentsSeparatedByString:@"/"];
        fileName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
        NSDate *now=[NSDate date];
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"yyyyMMddHHmmssSSS"];
        NSString *localDateString = [dateFormatter1 stringFromDate:now];
        //  NSLog(@"%@",localDateString);
        
        NSArray* arrExt = [fileName componentsSeparatedByString: @"."];
        NSString* strExt = [arrExt objectAtIndex: arrExt.count-1];
        strExt=@"jpg";
        NSString *UploadFileName=[NSString stringWithFormat:@"%@.%@",localDateString,strExt];
        
        
        //convert uiimage to
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",UploadFileName]];
        [UIImagePNGRepresentation(imageToUpload) writeToFile:filePath atomically:YES];
        
        NSString * strImageURL=[NSString stringWithFormat:@"%@%@",FILE_AWS_IMAGE_BASE_URL,UploadFileName];
        NSString *strMsgId = [[NSUUID UUID] UUIDString]; //id
        
        UIImage *imageFrmDirc = [UIImage imageWithContentsOfFile:filePath];
        
        [CustomAlbum addNewAssetWithImage:imageFrmDirc toAlbum:[CustomAlbum getMyAlbumWithName:CSAlbum] onSuccess:^(NSString *ImageId) {
            NSLog(@"image saved");
            
        }
                                  onError:^(NSError *error)
         {
             NSLog(@"probelm in saving image");
         }];
        
        long long milliSeconds=[self convertDateToMilliseconds:now];
        NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
        
        BOOL success=NO;
        if (isAvailinChatsTable==NO)
        {
            success=[db_class insertChats:@"" chatRoomId:strZoeChatId chatRoomType:strChatRoomType sender:@"0" lastMessage:strImageURL lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"image" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
            if (success==YES)
            {
                isAvailinChatsTable=YES;
            }
        }
        else
        {
            success=[db_class updateLastMessage:strZoeChatId sender:@"0" lastMessage:strImageURL lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"image" unReadCount:@"0"];
            NSLog(@"success");
        }
        
        success=[db_class insertChatMessages:strMsgId userId:strZoeChatId groupId:strZoeChatId chatRoomType:strChatRoomType content:strImageURL contentType:@"image" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
        
        Message *message = [[Message alloc] init];
        message.image = imageToUpload;
        message.type=@"image";
        message.date = now;
        message.chat_id = strMsgId;
        //   message.caption=@"hi hsdbfh sjhdbfsd hsdbv";
        
        [self.tableArray addObject:message];
        [arrWholetableArray addObject:message];
        
        //Insert Message in UI
        NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
        [self.tableSingleChat beginUpdates];
        if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
            [self.tableSingleChat insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                                withRowAnimation:UITableViewRowAnimationNone];
        [self.tableSingleChat insertRowsAtIndexPaths:@[indexPath]
                                    withRowAnimation:UITableViewRowAnimationBottom];
        [self.tableSingleChat endUpdates];
        
        [self.tableSingleChat scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                                    atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        
        if ([strCurrentlyUploading length]==0)
        {
            [self startUploadingImagesOrVideoOrAudio];
        }

    }
    else
    {
         NSURL *assetURL = [info objectForKey:UIImagePickerControllerMediaURL];
        NSString *strURL=[NSString stringWithFormat:@"%@",assetURL];
        __block NSString *fileName = nil;
        
        NSArray *components = [strURL componentsSeparatedByString:@"/"];
        fileName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
        NSDate *now=[NSDate date];
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"yyyyMMddHHmmssSSS"];
        NSString *localDateString = [dateFormatter1 stringFromDate:now];
        //  NSLog(@"%@",localDateString);
        
        NSArray* arrExt = [fileName componentsSeparatedByString: @"."];
        NSString* strExt = [arrExt objectAtIndex: arrExt.count-1];
        
        NSString *UploadFileName=[NSString stringWithFormat:@"%@.%@",localDateString,strExt];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",UploadFileName]];
        
        NSURL* uploadURL = [NSURL fileURLWithPath:filePath];
        NSData *videoData = [NSData dataWithContentsOfURL:assetURL];
        NSLog(@"Size of new Video after compression is (bytes):%lu",(unsigned long)[videoData length]);
        
        BOOL success1 = [videoData writeToFile:filePath atomically:NO];
        
        //    UIImage *videoToUpload=[self compressImage:[UIImage imageWithData:videoData]];
        //convert uiimage to
        
        //   [UIImagePNGRepresentation(videoToUpload) writeToFile:filePath atomically:YES];
        
        NSString * strVideoURL=[NSString stringWithFormat:@"%@%@",FILE_AWS_IMAGE_BASE_URL,UploadFileName];
        NSString *strMsgId = [[NSUUID UUID] UUIDString]; //id
        
        
        long long milliSeconds=[self convertDateToMilliseconds:now];
        NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
        
        BOOL success=NO;
        if (isAvailinChatsTable==NO)
        {
            success=[db_class insertChats:@"" chatRoomId:strZoeChatId chatRoomType:strChatRoomType sender:@"0" lastMessage:strVideoURL lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"video" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
            if (success==YES)
            {
                isAvailinChatsTable=YES;
            }
        }
        else
        {
            success=[db_class updateLastMessage:strZoeChatId sender:@"0" lastMessage:strVideoURL lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"video" unReadCount:@"0"];
            NSLog(@"success");
        }
        
        success=[db_class insertChatMessages:strMsgId userId:strZoeChatId groupId:strZoeChatId chatRoomType:strChatRoomType content:strVideoURL contentType:@"video" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
        
        
        
        Message *message = [[Message alloc] init];
        message.image = [self thumbnailImageFromURL:assetURL];
        message.type=@"video";
        message.date = now;
        message.chat_id = strMsgId;
        message.text=strVideoURL;
        //   message.caption=@"hi hsdbfh sjhdbfsd hsdbv";
        
        [self.tableArray addObject:message];
        [arrWholetableArray addObject:message];
        
        //Insert Message in UI
        NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
        [self.tableSingleChat beginUpdates];
        if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
            [self.tableSingleChat insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                                withRowAnimation:UITableViewRowAnimationNone];
        [self.tableSingleChat insertRowsAtIndexPaths:@[indexPath]
                                    withRowAnimation:UITableViewRowAnimationBottom];
        [self.tableSingleChat endUpdates];
        
        [self.tableSingleChat scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                                    atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        
       
        if ([strCurrentlyUploading length]==0)
        {
            [self startUploadingImagesOrVideoOrAudio];
        }

        
    }
    

   
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark - QBImagePickerControllerDelegate

- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets
{
    PHImageManager *imageManager = [PHImageManager new];
    __block int nCount=0;
    for (PHAsset *asset in assets) {
        [imageManager requestImageDataForAsset:asset
                                       options:0
                                 resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
                                     
                                     NSURL *assetURL = [info objectForKey:@"PHImageFileURLKey"];
                                     NSString *strURL=[NSString stringWithFormat:@"%@",assetURL];
                                     __block NSString *fileName = nil;
                                     
                                     NSArray *components = [strURL componentsSeparatedByString:@"/"];
                                     fileName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                                     NSDate *now=[NSDate date];
                                     NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                                     [dateFormatter1 setDateFormat:@"yyyyMMddHHmmssSSS"];
                                     NSString *localDateString = [dateFormatter1 stringFromDate:now];
                                     //  NSLog(@"%@",localDateString);
                                     
                                     NSArray* arrExt = [fileName componentsSeparatedByString: @"."];
                                     NSString* strExt = [arrExt objectAtIndex: arrExt.count-1];
                                     
                                     NSString *UploadFileName=[NSString stringWithFormat:@"%@.%@",localDateString,strExt];
                                     
                                     //image you want to upload
                                     // UIImage* imageToUpload = [UIImage imageWithData:imageData];
                                     UIImage *imageToUpload=[self compressImage:[UIImage imageWithData:imageData]];
                                     //convert uiimage to
                                     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                     NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",UploadFileName]];
                                     [UIImagePNGRepresentation(imageToUpload) writeToFile:filePath atomically:YES];
                                     
                                     NSString * strImageURL=[NSString stringWithFormat:@"%@%@",FILE_AWS_IMAGE_BASE_URL,UploadFileName];
                                     NSString *strMsgId = [[NSUUID UUID] UUIDString]; //id
                                     
                                     
                                     long long milliSeconds=[self convertDateToMilliseconds:now];
                                     NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
                                     
                                     BOOL success=NO;
                                     if (isAvailinChatsTable==NO)
                                     {
                                         success=[db_class insertChats:@"" chatRoomId:strZoeChatId chatRoomType:strChatRoomType sender:@"0" lastMessage:strImageURL lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"image" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
                                         if (success==YES)
                                         {
                                             isAvailinChatsTable=YES;
                                         }
                                     }
                                     else
                                     {
                                         success=[db_class updateLastMessage:strZoeChatId sender:@"0" lastMessage:strImageURL lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"image" unReadCount:@"0"];
                                         NSLog(@"success");
                                     }
                                     
                                     success=[db_class insertChatMessages:strMsgId userId:strZoeChatId groupId:strZoeChatId chatRoomType:strChatRoomType content:strImageURL contentType:@"image" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                                     
                                     Message *message = [[Message alloc] init];
                                     message.image = [UIImage imageWithData:imageData];
                                     message.type=@"image";
                                     message.date = now;
                                     message.chat_id = strMsgId;
                                     //   message.caption=@"hi hsdbfh sjhdbfsd hsdbv";
                                     
                                     [self.tableArray addObject:message];
                                     [arrWholetableArray addObject:message];
                                     
                                     //Insert Message in UI
                                     NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
                                     [self.tableSingleChat beginUpdates];
                                     if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
                                         [self.tableSingleChat insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                                                             withRowAnimation:UITableViewRowAnimationNone];
                                     [self.tableSingleChat insertRowsAtIndexPaths:@[indexPath]
                                                                 withRowAnimation:UITableViewRowAnimationBottom];
                                     [self.tableSingleChat endUpdates];
                                     
                                     [self.tableSingleChat scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                                                                 atScrollPosition:UITableViewScrollPositionBottom animated:YES];
                                     nCount=nCount+1;
                                     if (nCount==assets.count)
                                     {
                                         if ([strCurrentlyUploading length]==0)
                                         {
                                             [self startUploadingImagesOrVideoOrAudio];
                                         }
                                     }
                                 }];
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    NSLog(@"Canceled.");
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (UIImage *)thumbnailImageFromURL:(NSURL *)videoURL
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL: videoURL options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    NSError *err = NULL;
    CMTime requestedTime = CMTimeMake(1, 60);     // To create thumbnail image
    CGImageRef imgRef = [generator copyCGImageAtTime:requestedTime actualTime:NULL error:&err];
    NSLog(@"err = %@, imageRef = %@", err, imgRef);
    UIImage *thumbnailImage = [[UIImage alloc] initWithCGImage:imgRef];
    CGImageRelease(imgRef);    // MUST release explicitly to avoid memory leak
    return thumbnailImage;
}
- (UIImage *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.2;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth) {
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    return [UIImage imageWithData:imageData];
}

- (UIImage *)compressImageForThumbnail:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 100.0;
    float maxWidth = 120.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.1;//10 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth) {
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    return [UIImage imageWithData:imageData];
}

- (void)compressVideo:(NSURL*)inputURL
            outputURL:(NSURL*)outputURL
              handler:(void (^)(AVAssetExportSession*))completion  {
    AVURLAsset *urlAsset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:urlAsset presetName:AVAssetExportPresetMediumQuality];
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    exportSession.shouldOptimizeForNetworkUse = YES;
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        completion(exportSession);
    }];
}

#pragma mark -
#pragma mark - Font

-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)view;
        [btn setFont:[UIFont fontWithName:fontFamily size:[[btn font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *txtFld = (UITextField *)view;
        [txtFld setFont:[UIFont fontWithName:fontFamily size:[[txtFld font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextView class]])
    {
        UITextView *txtVw = (UITextView *)view;
        [txtVw setFont:[UIFont fontWithName:fontFamily size:[[txtVw font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}


-(IBAction)onCall:(id)sender
{
    [self.view endEditing:YES];
    UIView *viewForAudioOrVideo=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+self.navigationController.navigationBar.frame.size.height+50)];
    viewForAudioOrVideo.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7f];
    viewForAudioOrVideo.tag=9087650;
    [self.navigationController.view addSubview:viewForAudioOrVideo];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onRemoveViewAudioVideo:)]; // this is the current problem like a lot of people out there...
    [viewForAudioOrVideo addGestureRecognizer:tap];
    
    UIView *viewForWhiteBG=[[UIView alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)-140, (viewForAudioOrVideo.frame.size.height/2)-50, 280, 100)];
    viewForWhiteBG.backgroundColor=[UIColor whiteColor];
    viewForWhiteBG.layer.cornerRadius=3;
    viewForWhiteBG.clipsToBounds=YES;
    [viewForAudioOrVideo addSubview:viewForWhiteBG];
    
    UIButton *btnVoiceCall=[[UIButton alloc]initWithFrame:CGRectMake(20, 0, viewForWhiteBG.frame.size.width-40, 50)];
    [btnVoiceCall setTitle:@"Voice Call" forState:UIControlStateNormal];
    [btnVoiceCall setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnVoiceCall.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:14];
    btnVoiceCall.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [btnVoiceCall addTarget:self action:@selector(onVoiceCall:) forControlEvents:UIControlEventTouchUpInside];
    [viewForWhiteBG addSubview:btnVoiceCall];
    
    UIButton *btnVideoCall=[[UIButton alloc]initWithFrame:CGRectMake(20, 51, viewForWhiteBG.frame.size.width-40, 50)];
    [btnVideoCall setTitle:@"Video Call" forState:UIControlStateNormal];
    [btnVideoCall setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnVideoCall.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:14];
    btnVideoCall.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [btnVideoCall addTarget:self action:@selector(onVideoCall:) forControlEvents:UIControlEventTouchUpInside];
    [viewForWhiteBG addSubview:btnVideoCall];
}
-(IBAction)onVoiceCall:(id)sender
{
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    [viewForAudioOrVideo removeFromSuperview];
    [self performSegueWithIdentifier:@"SingleChat_VoiceCallVC" sender:self];
}
-(IBAction)onVideoCall:(id)sender
{
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    [viewForAudioOrVideo removeFromSuperview];
    [self performSegueWithIdentifier:@"SingleChat_VideoCallVC" sender:self];
}
- (void) onRemoveViewAudioVideo: (UIGestureRecognizer *) gesture
{
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    [viewForAudioOrVideo removeFromSuperview];
}
//Sticker delegate
- (void)stickerController: (STKStickerController*)stickerController didSelectStickerWithMessage: (NSString*)message {
    
    MessageCell *cell = [self.tableSingleChat dequeueReusableCellWithIdentifier:[deleteIndexPth description]];

    [cell fillWithStickerMessage: message downloaded: [self.stickerController isStickerPackDownloaded: message]];
    BOOL success=NO;
    NSString *strMsgId = [[NSUUID UUID] UUIDString]; //id
    
    
    long long milliSeconds=[self convertDateToMilliseconds:[NSDate date]];
    NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
    NSLog(@"%@",strMilliSeconds);
    
    
    
    
    
    if (isAvailinChatsTable==NO)
    {
        success=[db_class insertChats:@"" chatRoomId:strZoeChatId chatRoomType:strChatRoomType sender:@"0" lastMessage:message lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"sticker" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
        if (success==YES)
        {
            isAvailinChatsTable=YES;
        }
    }
    else
    {
        success=[db_class updateLastMessage:strZoeChatId sender:@"0" lastMessage:message lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"sticker" unReadCount:@"0"];
        NSLog(@"success");
    }
    
    success=[db_class insertChatMessages:strMsgId userId:strZoeChatId groupId:strZoeChatId chatRoomType:strChatRoomType content:message contentType:@"sticker" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
    
    Message *msg = [[Message alloc] init];
    msg.text = message;
    msg.date = [self convertMillisecondsToDate:[strMilliSeconds longLongValue]];
    msg.chat_id = strMsgId;
    msg.type=@"sticker";
    //Store Message in memory
    [self.tableArray addObject:msg];
    [arrWholetableArray addObject:msg];
    //Insert Message in UI
    NSIndexPath *indexPath = [self.tableArray indexPathForMessage:msg];
    [self.tableSingleChat beginUpdates];
    if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
        [self.tableSingleChat insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                            withRowAnimation:UITableViewRowAnimationNone];
    [self.tableSingleChat insertRowsAtIndexPaths:@[indexPath]
                                withRowAnimation:UITableViewRowAnimationBottom];
    [self.tableSingleChat endUpdates];
    
    [self.tableSingleChat scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                                atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    

    if ([strChatRoomType isEqualToString:@"2"])
    {
        
        NSMutableArray *arrPartis=[[NSMutableArray alloc]init];
        NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
        NSArray *arrdata=[db_class getParticipants:strZoeChatId];
        for (int i=0; i<arrdata.count; i++)
        {
            NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
            NSDictionary *dictCont=[arrdata objectAtIndex:i];
            NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"userId"]];
            [dictParti setObject:strPhone forKey:@"participantId"];
            [arrPartis addObject:dictParti];
//            NSString *BCstrMsgId = [[NSUUID UUID] UUIDString]; //id
//            
//            BOOL isBroadcastChat;
//            isBroadcastChat=NO;
//            isBroadcastChat=[db_class doesChatExist:strPhone];
//            
//            if (isBroadcastChat==NO)
//            {
//                
//                
//                success=[db_class insertChats:@"" chatRoomId:strPhone chatRoomType:@"0" sender:@"0" lastMessage:message lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"sticker" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
//                
//                if (success==YES)
//                {
//                    isBroadcastChat=YES;
//                }
//            }
//            else
//            {
//                
//                
//                success=[db_class updateLastMessage:strPhone sender:@"0" lastMessage:message lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"sticker" unReadCount:@"0"];
//                NSLog(@"success");
//
//                
//            }
//            success=[db_class insertChatMessages:BCstrMsgId userId:strPhone groupId:strPhone chatRoomType:@"0" content:message contentType:@"sticker" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
         }
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrPartis options:0 error:&error];
        NSString *jsonString;
        
        if (! jsonData)
        {
            NSLog(@"Got an error: %@", error);
        }
        else
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\" withString:@""];

        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setValue:appDelegate.strID forKey:@"from"];
        [dictParam setValue:jsonString forKey:@"participantId"];
        
        [appDelegate.socket emit:@"sendBroadcast" with:@[@{@"from": appDelegate.strID,@"participants": jsonString,@"content":message,@"contentType": @"sticker",@"messageId": strMsgId,@"time":strMilliSeconds,@"chatRoomType":@"0",@"groupId":@""}]];
        
        
        
        for (int i=0; i<arrdata.count; i++)
        {
            NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
            NSDictionary *dictCont=[arrdata objectAtIndex:i];
            NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"userId"]];
            [dictParti setObject:strPhone forKey:@"participantId"];
            [arrPartis addObject:dictParti];
            NSString *BCstrMsgId = [[NSUUID UUID] UUIDString]; //id
            
            BOOL isBroadcastChat;
            isBroadcastChat=NO;
            isBroadcastChat=[db_class doesChatExist:strPhone];
            
            if (isBroadcastChat==NO)
            {
                
                
                success=[db_class insertChats:@"" chatRoomId:strPhone chatRoomType:@"0" sender:@"0" lastMessage:message lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"sticker" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
                
                if (success==YES)
                {
                    isBroadcastChat=YES;
                }
            }
            else
            {
                
                
                success=[db_class updateLastMessage:strPhone sender:@"0" lastMessage:message lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"sticker" unReadCount:@"0"];
                NSLog(@"success");
                
                
            }
            success=[db_class insertChatMessages:BCstrMsgId userId:strPhone groupId:strPhone chatRoomType:@"0" content:message contentType:@"sticker" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
        }

    }
    else
    {
            [appDelegate.socket emit:@"sendMessage" with:@[@{@"from": appDelegate.strID,@"to": strZoeChatId,@"content": message,@"contentType":@"sticker",@"messageId": strMsgId,@"time":strMilliSeconds,@"chatRoomType":strChatRoomType,@"groupId":strZoeChatId}]];
    }

    
    [self.stickerController stickerMessageSendStatistic];
}

- (UIViewController*)stickerControllerViewControllerForPresentingModalView {
    return self;
}

- (IBAction)CancelBtn:(id)sender {
    isPreview=NO;
    self.linkPreviewView.hidden=YES;
}
- (IBAction)ClickOnAddContact:(id)sender {
    CNContactStore *store = [[CNContactStore alloc] init];
    
    // create contact
    
    CNMutableContact *contact = [[CNMutableContact alloc] init];
    
    //    contact.givenName = message.contactName;
    NSString *getPhoneNum=[NSString stringWithFormat:@"+ %@",strName];
    CNLabeledValue *homePhone = [CNLabeledValue labeledValueWithLabel:CNLabelHome value:[CNPhoneNumber phoneNumberWithStringValue:getPhoneNum]];
    contact.phoneNumbers = @[homePhone];
    
    CNContactViewController *controller = [CNContactViewController viewControllerForUnknownContact:contact];
    controller.contactStore = store;
    controller.title=@"Add Contact";
    UIBarButtonItem *barBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"BackButton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onBack:)];
    [controller.navigationItem setLeftBarButtonItem:barBtn ];
    
    controller.delegate = self;
    controller.navigationController.navigationBar.tintColor=[UIColor blackColor];
    [self.navigationController pushViewController:controller animated:TRUE];
}
@end
