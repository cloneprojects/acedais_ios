//
//  RestoreVC.h
//  ZoeChat
//
//  Created by Pyramidions on 07/03/18.
//  Copyright © 2018 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestoreVC : UIViewController


@property (strong, nonatomic) IBOutlet UILabel *minutesLabel;
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;

- (IBAction)nextBtn:(id)sender;


@end
