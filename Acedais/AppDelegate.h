//
//  AppDelegate.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 29/12/16.
//  Copyright © 2016 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
@import SocketIO;

@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(strong, nonatomic)NSString *strAlertTitle, *strID, *strName, *strStatus, *strCountryCode, *strMobileNumber, *strOTP, *strLoginStatus, *strDeviceToken, *strTokenVOIP, *strInternetMode, *strProfilePic, *isFrmLocationPage, *strCallTime, *strGroupIdToAddParticipants, *strGroupName, *isDriveLogin, *time, *driveId, *messageCount;
@property(strong, nonatomic)SocketIOClient* socket;
@property(strong, nonatomic)NSUUID *strUUID;
@property(strong,nonatomic)NSMutableDictionary *dictInComomgCall;
@property(strong, nonatomic)NSString *heightStr;
@property(nonatomic)CGFloat dynamicRowHeight;
@property(strong, nonatomic)NSString *uniqueID;


//noti
@property(strong, nonatomic)NSString *ZOEID;
//Disabletick
@property(strong, nonatomic)NSString *RoomTYPE;

@property(strong, nonatomic)NSString *isContact;
@property(strong, nonatomic)NSString *notiSoundPath;
//starredMsgpage
@property(strong, nonatomic)NSString *isStarred;

@property(strong, nonatomic)NSString *isfromView;
@property(strong, nonatomic)NSMutableDictionary *ViewDict;
//BroadCast
@property(strong, nonatomic)NSString *isBroadCast;
@property (weak, nonatomic)  NSArray *BCarrParticipants;

//addPartcipant
@property(strong, nonatomic)NSString *isPart;
//Doodle
@property(strong, nonatomic)NSString *isDoodle;



@end

