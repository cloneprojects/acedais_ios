//
//  CreateGroupContactVC.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 22/04/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateGroupContactVC : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableContacts;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
//
@property (weak, nonatomic) NSMutableArray *CheckPartiIDS;
@property (weak, nonatomic) NSString *getGroupName;
@property (weak, nonatomic) NSString *getGroupImg;
//PopView


@end
