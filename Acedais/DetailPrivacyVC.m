//
//  DetailPrivacyVC.m
//  ZoeChat
//
//  Created by macmini on 10/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "DetailPrivacyVC.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "Constants.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface DetailPrivacyVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *dataArray;
    AppDelegate *appdelegate;
    NSString *setStatusStr;
}


@end

@implementation DetailPrivacyVC
@synthesize titleStr;

- (void)viewDidLoad {
    [super viewDidLoad];
    dataArray = [[NSArray alloc]initWithObjects:@"Everyone",@"My Contacts",@"Nobody", nil];
    self.title = titleStr;
    appdelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    if ([titleStr isEqualToString:@"LastSeen"])
            {
                self.descriptionLbl.hidden=NO;
            }
            else if ([titleStr isEqualToString:@"ProfilePhoto"])
            {
                self.descriptionLbl.hidden=YES;

            }
            else if ([titleStr isEqualToString:@"About"])
            {
                self.descriptionLbl.hidden=YES;

                
            }

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
     if ([titleStr isEqualToString:@"LastSeen"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *lastRow = [defaults objectForKey:@"privacyCell1"];
        if (lastRow) {
            self.LastseenIndexPath = [NSIndexPath indexPathForRow:lastRow.integerValue inSection:0];
            self.descriptionLbl.hidden=NO;
        }
        else
        {
            self.LastseenIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            self.descriptionLbl.hidden=NO;
            
        }
    }
    else if ([titleStr isEqualToString:@"ProfilePhoto"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *lastRow = [defaults objectForKey:@"privacyCell2"];
        if (lastRow) {
            self.ProfilePhotoIndexPath = [NSIndexPath indexPathForRow:lastRow.integerValue inSection:0];
            self.descriptionLbl.hidden=YES;

        }
        else
        {
            self.ProfilePhotoIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            self.descriptionLbl.hidden=YES;
            
        }
    }
    else if ([titleStr isEqualToString:@"About"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *lastRow = [defaults objectForKey:@"privacyCell3"];
        if (lastRow) {
            self.AboutIndexPath = [NSIndexPath indexPathForRow:lastRow.integerValue inSection:0];
            self.descriptionLbl.hidden=YES;

        }
        else
        {
            self.AboutIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            self.descriptionLbl.hidden=YES;
            
        }
        
    }
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  44;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    UITableViewCell *cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.textLabel.text=[dataArray objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        
        cell.tintColor=color;
        
        
        
    }
    else
    {
        cell.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
      }
    if ([titleStr isEqualToString:@"LastSeen"])
    {
        if([self.LastseenIndexPath isEqual:indexPath])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    else if ([titleStr isEqualToString:@"ProfilePhoto"])
    {
        if([self.ProfilePhotoIndexPath isEqual:indexPath])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    else if ([titleStr isEqualToString:@"About"])
    {
        if([self.AboutIndexPath isEqual:indexPath])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([titleStr isEqualToString:@"LastSeen"])
    {
        if(self.LastseenIndexPath)
        {
            UITableViewCell* uncheckCell = [tableView
                                            cellForRowAtIndexPath:self.LastseenIndexPath];
            uncheckCell.accessoryType = UITableViewCellAccessoryNone;
        }
        if([self.LastseenIndexPath isEqual:indexPath])
        {
            self.LastseenIndexPath = nil;
        }
        else
        {
            UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            self.LastseenIndexPath = indexPath;
        }
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[NSNumber numberWithInt:self.LastseenIndexPath.row] forKey:@"privacyCell1"];
        [defaults setObject:[dataArray objectAtIndex:indexPath.row] forKey:@"seenStatus"];
        if (indexPath.row == 0)
        {
            setStatusStr=@"e";
        }
        else if (indexPath.row == 1)
        {
            setStatusStr=@"c";
        }
        else if (indexPath.row == 2)
        {
            setStatusStr=@"n";
        }
        [defaults synchronize];
    }
    else if ([titleStr isEqualToString:@"ProfilePhoto"])
    {
        if(self.ProfilePhotoIndexPath)
        {
            UITableViewCell* uncheckCell = [tableView
                                            cellForRowAtIndexPath:self.ProfilePhotoIndexPath];
            uncheckCell.accessoryType = UITableViewCellAccessoryNone;
        }
        if([self.ProfilePhotoIndexPath isEqual:indexPath])
        {
            self.ProfilePhotoIndexPath = nil;
        }
        else
        {
            UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            self.ProfilePhotoIndexPath = indexPath;
        }
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[NSNumber numberWithInt:self.ProfilePhotoIndexPath.row] forKey:@"privacyCell2"];
        [defaults setObject:[dataArray objectAtIndex:indexPath.row] forKey:@"profileStatus"];
        if (indexPath.row == 0)
        {
            setStatusStr=@"e";
        }
        else if (indexPath.row == 1)
        {
            setStatusStr=@"c";
        }
        else if (indexPath.row == 2)
        {
            setStatusStr=@"n";
        }
        [defaults synchronize];
    }
    else if ([titleStr isEqualToString:@"About"])
    {
        if(self.AboutIndexPath)
        {
            UITableViewCell* uncheckCell = [tableView
                                            cellForRowAtIndexPath:self.AboutIndexPath];
            uncheckCell.accessoryType = UITableViewCellAccessoryNone;
        }
        if([self.AboutIndexPath isEqual:indexPath])
        {
            self.AboutIndexPath = nil;
        }
        else
        {
            UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            self.AboutIndexPath = indexPath;
        }
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[NSNumber numberWithInt:self.AboutIndexPath.row] forKey:@"privacyCell3"];
        [defaults setObject:[dataArray objectAtIndex:indexPath.row] forKey:@"aboutStatus"];
        if (indexPath.row == 0)
        {
            setStatusStr=@"e";
        }
        else if (indexPath.row == 1)
        {
            setStatusStr=@"c";
        }
        else if (indexPath.row == 2)
        {
            setStatusStr=@"n";
        }
        [defaults synchronize];
    }
    [self callAPI];
    
}



-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryNone;
}
-(void)callAPI

{
    if ([appdelegate.strInternetMode isEqualToString:@"online"])
    {
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];

        if ([titleStr isEqualToString:@"LastSeen"])
        {
            
            [dictParam setObject:appdelegate.strID forKey:@"id"];
            [dictParam setObject:setStatusStr forKey:@"value"];
            [dictParam setObject:@"lastSeenPrivacy" forKey:@"key"];

           
        }
        
    
        else if ([titleStr isEqualToString:@"ProfilePhoto"])
        {
            [dictParam setObject:appdelegate.strID forKey:@"id"];
            [dictParam setObject:setStatusStr forKey:@"value"];
            [dictParam setObject:@"imagePrivacy" forKey:@"key"];

        }
        else if ([titleStr isEqualToString:@"About"]){
            [dictParam setObject:appdelegate.strID forKey:@"id"];
            [dictParam setObject:setStatusStr forKey:@"value"];
            [dictParam setObject:@"statusPrivacy" forKey:@"key"];
        }
        
        
        MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDAnimationFade;
        hud.label.text = @"Loading";
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_PRIVACY withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
             if (response)
             {
                    NSLog(@"Response: %@",response);
                 if ([[response valueForKey:@"error"] boolValue]==false)
                 {
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     // Configure for text only and offset down
                     hud.mode = MBProgressHUDModeText;
                     hud.label.text = @"Privacy Settings Changed";
                     hud.margin = 10.f;
                     hud.yOffset = 150.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hideAnimated:YES afterDelay:2];

                     
                 }
                 else
                 {
                     
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     // Configure for text only and offset down
                     hud.mode = MBProgressHUDModeText;
                     hud.label.text = [NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                     hud.margin = 10.f;
                     hud.yOffset = 150.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hideAnimated:YES afterDelay:2];
                     
                     
                 }
             }
             
         }];
        
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Please check your internet connection"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.label.adjustsFontSizeToFitWidth=YES;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:2];
        
    }
    
}
@end
