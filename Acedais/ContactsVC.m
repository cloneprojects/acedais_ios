//
//  ContactsVC.m
//  ZoeChat
//
//  Created by Pyramidions Solution on 31/12/16.
//  Copyright © 2016 Pyramidions Solution. All rights reserved.
//

#import "ContactsVC.h"
#import "ContactCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SingleChatVC.h"
#import "Constants.h"
#import "DBClass.h"
#import "AppDelegate.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "ForgotPasswordVC.h"
#import "InviteTblCell.h"
#import <MessageUI/MessageUI.h>
#import "VoiceCallVC.h"
#import "VideoCallVC.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "ContactInviteCell.h"

@interface ContactsVC ()<UITextFieldDelegate,ABNewPersonViewControllerDelegate,MFMessageComposeViewControllerDelegate>
{
    NSMutableArray *arrContacts,*TableArray;
    NSMutableDictionary *dictToSendSingleChat;
    DBClass *db_class;
    AppDelegate *appDelegate;
    NSMutableArray *arrChats, *arrForTableView, *InviteFriendsArray,*InviteTblArray;
    NSInteger getindexval;
    NSString *ChatRoomId;
    UIView *ViewSearch;
    NSIndexPath *getindepath;
    NSIndexPath *Callindepath;
    
    NSMutableArray *searchArray, *WholeTableArray;
    BOOL isFilter;
    UIScrollView *scroll;
    
    
    
    
}
@end

@implementation ContactsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    scroll=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.passwordView.hidden=YES;
    _submitBtn.layer.cornerRadius = 5;
    _submitBtn.clipsToBounds = YES;
    [self.passwordView.layer setCornerRadius:5.0f];
    [self.passwordView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.passwordView.layer setBorderWidth:0.2f];
    
    db_class=[[DBClass alloc]init];
    [self setFontFamily:FONT_NORMAL forView:self.view andSubViews:YES];
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    arrContacts=[[NSMutableArray alloc]init];
    arrForTableView=[[NSMutableArray alloc]init];
    TableArray=[[NSMutableArray alloc]init];
    WholeTableArray=[[NSMutableArray alloc]init];
    InviteTblArray=[[NSMutableArray alloc]init];
    
    
    
    arrChats=[[NSMutableArray alloc]init];
    _tableContacts.delegate=self;
    _tableContacts.dataSource=self;
    appDelegate.isContact=@"yes";
    self.tabBarItem.image = [[UIImage imageNamed:@"Contacts50.png"]
                             imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        [[UIApplication sharedApplication] keyWindow].tintColor = color;
        self.navigationController.navigationBar.translucent = NO;
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }
                                                 forState:UIControlStateSelected];
        [[UITabBar appearance] setSelectedImageTintColor:[UIColor whiteColor]];
        self.submitBtn.backgroundColor=color;
        self.passwordTxt.tintColor=color;
        
        [[UITabBar appearance] setBarTintColor:color];
        
        
    }
    else
    {
        [[UIApplication sharedApplication] keyWindow].tintColor = [UIColor whiteColor];
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName :[UIColor whiteColor]}forState:UIControlStateSelected];
        [[UITabBar appearance] setSelectedImageTintColor:[UIColor whiteColor]];
        self.submitBtn.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.passwordTxt.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        
        [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0]];
        
        
        
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SearchView) name:@"Show_Search" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(RefreshContacts) name:@"Refresh_Contacts" object:nil];
    isFilter=NO;
    // isInviteFilter=NO;
    
    [scroll addSubview:self.passwordView];
   
    
    UINib *nib = [UINib nibWithNibName:@"ContactInviteCell" bundle:nil];
    [self.tableContacts registerNib:nib forCellReuseIdentifier:@"ContactInviteCell"];
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        [[UIApplication sharedApplication] keyWindow].tintColor = color;
        self.navigationController.navigationBar.translucent = NO;
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }
                                                 forState:UIControlStateSelected];
        [[UITabBar appearance] setSelectedImageTintColor:[UIColor whiteColor]];
        self.submitBtn.backgroundColor=color;
        self.passwordTxt.tintColor=color;
        
        [[UITabBar appearance] setBarTintColor:color];
    }
    else
    {
        [[UIApplication sharedApplication] keyWindow].tintColor = [UIColor whiteColor];
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }
                                                 forState:UIControlStateSelected];
        [[UITabBar appearance] setSelectedImageTintColor:[UIColor whiteColor]];
        self.submitBtn.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.passwordTxt.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0]];
    }
    [[UITabBar appearance] setUnselectedItemTintColor:[UIColor lightGrayColor]];
    isFilter=NO;
    [self RefreshContacts];
    [self InviteFriends];
    [self getLockContact];
}
-(void)InviteFriends
{
    InviteFriendsArray=[[NSMutableArray alloc]init];
    
    NSMutableArray *arrZoeCont=[[NSMutableArray alloc]init];
    NSArray *arrCont=[db_class getUsers];
    if (arrCont.count!=0)
    {
        for (int i=0; i<arrCont.count; i++)
        {
            NSDictionary *dictCont=[arrCont objectAtIndex:i];
            NSString *strShowInContacts=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"showincontactspage"]];
            NSString *strzoe=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"zoechatid"]];
            if ([strShowInContacts boolValue]==false)
            {
                if (![appDelegate.strID isEqualToString:strzoe])
                {
                    [arrZoeCont addObject:dictCont];
                }
                
            }
            
            
        }
    }
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    InviteFriendsArray=[arrZoeCont sortedArrayUsingDescriptors:@[sort]];
    InviteTblArray=[NSMutableArray arrayWithArray:InviteFriendsArray];
    [WholeTableArray addObjectsFromArray:InviteFriendsArray];
    TableArray=[NSMutableArray arrayWithArray:WholeTableArray];
    NSLog(@"%@",WholeTableArray);
    
    [self.tableContacts setFrame:CGRectMake(self.tableContacts.frame.origin.x, self.tableContacts.frame.origin.y  , self.tableContacts.frame.size.width,(60.0f*([TableArray count])))];
    scroll.contentSize=CGSizeMake(self.view.frame.size.width, self.tableContacts.frame.origin.y + self.tableContacts.frame.size.height + 120);
    [self.tableContacts reloadData];
    
    [scroll addSubview:self.tableContacts];
    
    [self.view addSubview:scroll];
}

-(void)SearchView
{
    // [self.tableView reloadData];
    
    ViewSearch=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.navigationController.navigationBar.frame.size.height)];
    ViewSearch.backgroundColor=[UIColor whiteColor];
    [self.navigationController.navigationBar addSubview:ViewSearch];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 40, ViewSearch.frame.size.height)];
    [btnClose addTarget:self action:@selector(onClose) forControlEvents:UIControlEventTouchDown];
    [ViewSearch addSubview:btnClose];
    
    UIImageView *arrowImg=[[UIImageView alloc]initWithFrame:CGRectMake(10, (self.navigationController.navigationBar.frame.size.height/2)-10, 20, 20)];
    arrowImg.image=[UIImage imageNamed:@"BackToClose.png"];
    [ViewSearch addSubview:arrowImg];
    
    
    UITextField  *txtSearch=[[UITextField alloc]initWithFrame:CGRectMake(45, 0, self.view.frame.size.width-40, self.navigationController.navigationBar.frame.size.height)];
    txtSearch.clearButtonMode=UITextFieldViewModeWhileEditing;
    txtSearch.placeholder=@"Search...";
    txtSearch.delegate=self;
    [txtSearch becomeFirstResponder];
    [ViewSearch addSubview:txtSearch];
}
-(void)onClose
{
    [ViewSearch removeFromSuperview];
    [TableArray removeAllObjects];
    TableArray=[NSMutableArray arrayWithArray:WholeTableArray];
    [_tableContacts reloadData];
    
}
-(void)RefreshContacts
{
    
    arrContacts=[[NSMutableArray alloc]init];
    
    NSMutableArray *arrZoeCont=[[NSMutableArray alloc]init];
    NSArray *arrCont=[db_class getUsers];
    if (arrCont.count!=0)
    {
        for (int i=0; i<arrCont.count; i++)
        {
            NSDictionary *dictCont=[arrCont objectAtIndex:i];
            NSString *strShowInContacts=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"showincontactspage"]];
            NSString *strzoe=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"zoechatid"]];
            if ([strShowInContacts boolValue]==true)
            {
                if (![appDelegate.strID isEqualToString:strzoe])
                {
                    [arrZoeCont addObject:dictCont];
                }
                
            }
            
            
        }
    }
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    arrContacts=[arrZoeCont sortedArrayUsingDescriptors:@[sort]];
    
    WholeTableArray=[NSMutableArray arrayWithArray:arrContacts];
    // [self.tableContacts setFrame:CGRectMake(self.tableContacts.frame.origin.x, self.tableContacts.frame.origin.y, self.tableContacts.frame.size.width,(60.0f*([arrContacts count])))];
    
    
    
}
-(void)getLockContact
{
    arrChats=[db_class getChats];
    
    if (arrChats.count!=0)
    {
        for (int i=0; i<arrChats.count; i++)
        {
            NSMutableDictionary *dictSing=[arrChats objectAtIndex:i];
            NSDictionary *dictInfo =[db_class getUserInfo:[dictSing valueForKey:@"chatRoomId"]];
            NSString *strName=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"name"]];
            NSString *strImage=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"image"]];
            [dictSing setObject:strName forKey:@"name"];
            [dictSing setObject:strImage forKey:@"image"];
            NSLog(@"%@",dictSing);
            
            [arrChats replaceObjectAtIndex:i withObject:dictSing];
        }
    }
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"dateTime" ascending:NO];
    arrChats=[arrChats sortedArrayUsingDescriptors:@[sort]];
    
    arrForTableView=[NSMutableArray arrayWithArray:arrChats];
    //[_tableContacts reloadData];
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(void)insertdata
{
    
    BOOL success = NO;
    NSString *alertString = @"Data Insertion failed";
    
    //  NSString *strUserId=[NSString stringWithFormat:@"%d",i];
    
    success =[db_class insertUser:@"9876543210" name:@"gopal" registeredName:@"" image:@"" status:@"" showInContactsPage:@"1" zoeChatId:@""];
    
    if (success == NO)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:
                              alertString message:nil
                                                      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    
}

-(void)updatedata
{
    
    BOOL success = NO;
    NSString *alertString = @"Data Updation failed";
    
    //  NSString *strUserId=[NSString stringWithFormat:@"%d",i];
    
    success=[db_class updateUser:@"8885555512" registeredName:@"" image:@"" status:@"Hi there! Im using Acedais" showInContactsPage:@"1" zoeChatId:@""];
    
    //  success=[[DBManager getSharedInstance]updateUserImage:@"9876543210" image:@"http://krishna"];
    
    //success=[[DBManager getSharedInstance]updateUserStatus:@"9876543210" status:@"hi krishna"];
    //   success=[[DBManager getSharedInstance]updateUserName:@"9876543210" name:@"gopalsmy"];
    
    if (success == NO)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:
                              alertString message:nil
                                                      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    
    
}

-(void)deletedata
{
    
    BOOL success = NO;
    NSString *alertString = @"Data Deletion failed";
    
    //  NSString *strUserId=[NSString stringWithFormat:@"%d",i];
    
    success=[db_class deleteUser:@"9876543211"];
    
    if (success == NO)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:
                              alertString message:nil
                                                      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    
    
}

-(void)getUserName
{
    //   NSDictionary *data = [[DBManager getSharedInstance]getUserName:@"9876543210"];
    //  NSDictionary *data = [[DBManager getSharedInstance]getUserImage:@"9876543210"];
    //  NSDictionary *data = [[DBManager getSharedInstance]getUserInfo:@"9876543210"];
    NSArray *data=[db_class getUsers];
    
    if (data == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:
                              @"Data not found" message:nil delegate:nil cancelButtonTitle:
                              @"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        
        // NSString *strName =[data objectAtIndex:0];
        // NSLog(@"%@",strName);
        
    }
}

#pragma mark -
#pragma mark - TableView Delagate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  60;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSInteger rowCount = 0;
    
    
    if(isFilter)
    {
        
        rowCount=searchArray.count;
    }
    else
    {
        rowCount=TableArray.count;
        
    }
    
    return rowCount;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell;
    NSString *InitialStr;
    UIColor *bgColor;
    if(isFilter)
    {
        NSDictionary *dictArt=[searchArray objectAtIndex:indexPath.row];
        NSString *showincontactspage=[dictArt valueForKey:@"showincontactspage"];
        if ([showincontactspage isEqualToString:@"1"])
        {
            ContactCell *cell=(ContactCell *)[tableView dequeueReusableCellWithIdentifier:@"ContactCell"];
            [cell.lblImg setHidden:YES];
            NSString *strName=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"name"]];
            cell.ContactName.text=[strName capitalizedString];
            cell.ContactName.font=[UIFont fontWithName:FONT_NORMAL size:16];
            InitialStr=[cell.ContactName.text substringToIndex:1];
            if ([InitialStr isEqualToString:@"A"])
            {
                bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"B"])
            {
                bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"C"])
            {
                bgColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"D"])
            {
                bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"E"])
            {
                bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"F"])
            {
                bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"G"])
            {
                bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"H"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"I"])
            {
                bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"J"])
            {
                bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"K"])
            {
                bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"L"])
            {
                bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"M"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"N"])
            {
                bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"O"])
            {
                bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"P"])
            {
                bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"Q"])
            {
                bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"R"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"S"])
            {
                bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"T"])
            {
                bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"U"])
            {
                bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"V"])
            {
                bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"W"])
            {
                bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"X"])
            {
                bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"Y"])
            {
                bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"Z"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
            }
            
            
            
            
            NSString *strImageURL1=[dictArt valueForKey:@"image"];
            cell.ContactImage.image=[UIImage imageNamed:@"userImage.png"];
            if (![strImageURL1 isEqualToString:@"(null)"])
            {
                if (![strImageURL1 isEqualToString:@""])
                {
                    [cell.ContactImage sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
                }
            }
            else
            {
                [cell.lblImg setHidden:NO];
                
                [cell.ContactImage sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
                cell.lblImg.text=InitialStr;
                cell.lblImg.backgroundColor=bgColor;
            }
            NSString *strStatus=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"status"]];
            
            cell.ContactImage.layer.cornerRadius=cell.ContactImage.frame.size.height/2;
            cell.ContactImage.clipsToBounds=YES;
            cell.lblImg.layer.cornerRadius=cell.ContactImage.frame.size.height/2;
            cell.lblImg.clipsToBounds=YES;
            if ([strStatus isEqualToString:@"(null)"])
            {
                cell.ContactStatus.text=@"Hey there! I am using Zoechat";
            }
            else
            {
                cell.ContactStatus.text=strStatus;
            }
            cell.ContactStatus.font=[UIFont fontWithName:FONT_NORMAL size:14];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            NSDictionary *checkZoeidDict=[TableArray objectAtIndex:indexPath.row];
            NSString *strid=[checkZoeidDict valueForKey:@"zoechatid"];
            ChatRoomId=[NSString stringWithFormat:@"%@",strid];
            NSDictionary *dict=[db_class getOneChat:strid];
            NSLog(@"%@",dict);
            NSString *strcheckLock=[NSString stringWithFormat:@"%@",[dict valueForKey:@"isLock"]];
            if ([strcheckLock isEqualToString:@"1"])
            {
                [cell.contactlockImg setImage:[UIImage imageNamed:@"lock.png"]];
            }
            else
            {
                [cell.contactlockImg setImage:[UIImage imageNamed:@""]];
                
            }
           
            return cell;
        }
        else if([showincontactspage isEqualToString:@"0"])
        {
            ContactInviteCell *cell = (ContactInviteCell *)[tableView dequeueReusableCellWithIdentifier:@"ContactInviteCell"];
            if (cell == nil) {
                // Load the top-level objects from the custom cell XIB.
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ContactInviteCell" owner:self options:nil];
                // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
                cell = [topLevelObjects objectAtIndex:0];
            }
            NSString *InitialStr;
            UIColor *bgColor;
            [cell.LbImg setHidden:YES];
            NSDictionary *dictArt=[TableArray objectAtIndex:indexPath.row];
            NSString *strName=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"name"]];
            cell.NamLbl.text=[strName capitalizedString];
            cell.NamLbl.font=[UIFont fontWithName:FONT_NORMAL size:16];
            InitialStr=[cell.NamLbl.text substringToIndex:1];
            if ([InitialStr isEqualToString:@"A"])
            {
                bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"B"])
            {
                bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"C"])
            {
                bgColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"D"])
            {
                bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"E"])
            {
                bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"F"])
            {
                bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"G"])
            {
                bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"H"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"I"])
            {
                bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"J"])
            {
                bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"K"])
            {
                bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"L"])
            {
                bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"M"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"N"])
            {
                bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"O"])
            {
                bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"P"])
            {
                bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"Q"])
            {
                bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"R"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"S"])
            {
                bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"T"])
            {
                bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"U"])
            {
                bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"V"])
            {
                bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"W"])
            {
                bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"X"])
            {
                bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"Y"])
            {
                bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"Z"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
            }
            
            NSString *strImageURL1=[dictArt valueForKey:@"image"];
            
            if ([strImageURL1 isEqualToString:@""])
            {
                
                
                [cell.LbImg setHidden:NO];
                
                [cell.imgiview sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
                
                cell.LbImg.text=InitialStr;
                cell.LbImg.backgroundColor=bgColor;
            }
            else
            {
                [cell.imgiview sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
            }
            
            
            cell.imgiview.layer.cornerRadius=cell.imgiview.frame.size.height/2;
            cell.imgiview.clipsToBounds=YES;
            cell.LbImg.layer.cornerRadius=cell.imgiview.frame.size.height/2;
            cell.LbImg.clipsToBounds=YES;
            cell.invitBtn.layer.cornerRadius = 5; // this value vary as per your desire
            cell.invitBtn.clipsToBounds = YES;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell.invitBtn addTarget:self action:@selector(ClickOnInvite:) forControlEvents:UIControlEventTouchUpInside];
            NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
            UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
            if (color != nil) {
                
                cell.invitBtn.backgroundColor=color;
                
                
                
            }
            else
            {
                cell.invitBtn.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
                
                
                
                
            }
            return cell;
        }
        
    }
    else{
        NSLog(@"%@",WholeTableArray);
        
        NSDictionary *dictArt=[TableArray objectAtIndex:indexPath.row];
        NSString *showincontactspage=[dictArt valueForKey:@"showincontactspage"];
        if ([showincontactspage isEqualToString:@"1"])
        {
            ContactCell *cell=(ContactCell *)[tableView dequeueReusableCellWithIdentifier:@"ContactCell"];
            [cell.lblImg setHidden:YES];
            NSString *strName=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"name"]];
            cell.ContactName.text=[strName capitalizedString];
            cell.ContactName.font=[UIFont fontWithName:FONT_NORMAL size:16];
            InitialStr=[cell.ContactName.text substringToIndex:1];
            if ([InitialStr isEqualToString:@"A"])
            {
                bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"B"])
            {
                bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"C"])
            {
                bgColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"D"])
            {
                bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"E"])
            {
                bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"F"])
            {
                bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"G"])
            {
                bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"H"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"I"])
            {
                bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"J"])
            {
                bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"K"])
            {
                bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"L"])
            {
                bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"M"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"N"])
            {
                bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"O"])
            {
                bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"P"])
            {
                bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"Q"])
            {
                bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"R"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"S"])
            {
                bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"T"])
            {
                bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"U"])
            {
                bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"V"])
            {
                bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"W"])
            {
                bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"X"])
            {
                bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"Y"])
            {
                bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"Z"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
            }
            
            
           
            
            NSString *strImageURL1=[dictArt valueForKey:@"image"];
            cell.ContactImage.image=[UIImage imageNamed:@"userImage.png"];
            if (![strImageURL1 isEqualToString:@"(null)"])
            {
                if (![strImageURL1 isEqualToString:@""])
                {
                    [cell.ContactImage sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
                }
            }
            else
            {
                [cell.lblImg setHidden:NO];
                
                [cell.ContactImage sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
                cell.lblImg.text=InitialStr;
                cell.lblImg.backgroundColor=bgColor;
            }
            NSString *strStatus=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"status"]];
            
            cell.ContactImage.layer.cornerRadius=cell.ContactImage.frame.size.height/2;
            cell.ContactImage.clipsToBounds=YES;
            cell.lblImg.layer.cornerRadius=cell.ContactImage.frame.size.height/2;
            cell.lblImg.clipsToBounds=YES;
            if ([strStatus isEqualToString:@"(null)"])
            {
                cell.ContactStatus.text=@"Hey there! I am using Zoechat";
            }
            else
            {
                cell.ContactStatus.text=strStatus;
            }
            cell.ContactStatus.font=[UIFont fontWithName:FONT_NORMAL size:14];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            NSDictionary *checkZoeidDict=[TableArray objectAtIndex:indexPath.row];
            NSString *strid=[checkZoeidDict valueForKey:@"zoechatid"];
            ChatRoomId=[NSString stringWithFormat:@"%@",strid];
            NSDictionary *dict=[db_class getOneChat:strid];
            NSLog(@"%@",dict);
            NSString *strcheckLock=[NSString stringWithFormat:@"%@",[dict valueForKey:@"isLock"]];
            if ([strcheckLock isEqualToString:@"1"])
            {
                [cell.contactlockImg setImage:[UIImage imageNamed:@"lock.png"]];
            }
            else
            {
                [cell.contactlockImg setImage:[UIImage imageNamed:@""]];
                
            }
            NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
            UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
            
            return cell;
        }
        else if([showincontactspage isEqualToString:@"0"])
        {
            ContactInviteCell *cell = (ContactInviteCell *)[tableView dequeueReusableCellWithIdentifier:@"ContactInviteCell"];
            if (cell == nil) {
                // Load the top-level objects from the custom cell XIB.
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ContactInviteCell" owner:self options:nil];
                // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
                cell = [topLevelObjects objectAtIndex:0];
            }
            NSString *InitialStr;
            UIColor *bgColor;
            [cell.LbImg setHidden:YES];
            NSDictionary *dictArt=[TableArray objectAtIndex:indexPath.row];
            NSString *strName=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"name"]];
            cell.NamLbl.text=[strName capitalizedString];
            cell.NamLbl.font=[UIFont fontWithName:FONT_NORMAL size:16];
            InitialStr=[cell.NamLbl.text substringToIndex:1];
            if ([InitialStr isEqualToString:@"A"])
            {
                bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"B"])
            {
                bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"C"])
            {
                bgColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"D"])
            {
                bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"E"])
            {
                bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"F"])
            {
                bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"G"])
            {
                bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"H"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"I"])
            {
                bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"J"])
            {
                bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"K"])
            {
                bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"L"])
            {
                bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"M"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"N"])
            {
                bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"O"])
            {
                bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"P"])
            {
                bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"Q"])
            {
                bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"R"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"S"])
            {
                bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"T"])
            {
                bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"U"])
            {
                bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"V"])
            {
                bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"W"])
            {
                bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"X"])
            {
                bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"Y"])
            {
                bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"Z"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
            }
            
            NSString *strImageURL1=[dictArt valueForKey:@"image"];
            
            if ([strImageURL1 isEqualToString:@""])
            {
                
                
                [cell.LbImg setHidden:NO];
                
                [cell.imgiview sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
                
                cell.LbImg.text=InitialStr;
                cell.LbImg.backgroundColor=bgColor;
            }
            else
            {
                [cell.imgiview sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
            }
            
            
            cell.imgiview.layer.cornerRadius=cell.imgiview.frame.size.height/2;
            cell.imgiview.clipsToBounds=YES;
            cell.LbImg.layer.cornerRadius=cell.imgiview.frame.size.height/2;
            cell.LbImg.clipsToBounds=YES;
            cell.invitBtn.layer.cornerRadius = 5; // this value vary as per your desire
            cell.invitBtn.clipsToBounds = YES;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell.invitBtn addTarget:self action:@selector(ClickOnInvite:) forControlEvents:UIControlEventTouchUpInside];
            NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
            UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
            if (color != nil) {
                
                cell.invitBtn.backgroundColor=color;
                
                
                
            }
            else
            {
                cell.invitBtn.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
                
                
                
                
            }
            return cell;
        }
        
        
    }
    
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    if(isFilter)
    {
        [ViewSearch removeFromSuperview];
        getindexval=indexPath.row;
        NSDictionary *checkZoeidDict=[searchArray objectAtIndex:getindexval];
        NSString *showninContact=[checkZoeidDict valueForKey:@"showincontactspage"];
        NSString *strid=[checkZoeidDict valueForKey:@"zoechatid"];
        if ([showninContact isEqualToString:@"1"])
        {
            NSDictionary *dict=[db_class getOneChat:strid];
            NSLog(@"%@",dict);
            NSString *strcheckLock=[NSString stringWithFormat:@"%@",[dict valueForKey:@"isLock"]];
            if ([strcheckLock isEqualToString:@"1"])
            {
                [self ShowPasswordView];
            }
            else
            {
                dictToSendSingleChat=[[NSMutableDictionary alloc]init];
                dictToSendSingleChat=[searchArray objectAtIndex:indexPath.row];
                [ dictToSendSingleChat setObject:@"0" forKey:@"chatRoomType"];
                [self performSegueWithIdentifier:@"Contacts_SingleChat" sender:self];
            }
        }
        else if([showninContact isEqualToString:@"0"])
        {
        }
        
    }
    
    else
    {
        getindexval=indexPath.row;
        NSDictionary *checkZoeidDict=[TableArray objectAtIndex:getindexval];
        NSString *strid=[checkZoeidDict valueForKey:@"zoechatid"];
        NSString *showninContact=[checkZoeidDict valueForKey:@"showincontactspage"];
        if ([showninContact isEqualToString:@"1"])
        {
            
            NSDictionary *dict=[db_class getOneChat:strid];
            NSLog(@"%@",dict);
            NSString *strcheckLock=[NSString stringWithFormat:@"%@",[dict valueForKey:@"isLock"]];
            if ([strcheckLock isEqualToString:@"1"])
            {
                [self ShowPasswordView];
            }
            else
            {
                dictToSendSingleChat=[[NSMutableDictionary alloc]init];
                dictToSendSingleChat=[WholeTableArray objectAtIndex:indexPath.row];
                [ dictToSendSingleChat setObject:@"0" forKey:@"chatRoomType"];
                [self performSegueWithIdentifier:@"Contacts_SingleChat" sender:self];
            }
        }
        else if([showninContact isEqualToString:@"0"])
        {
        }
    }
    
    
    
    
    
    
}
-(IBAction)ClickOnVideoCall:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableContacts];
    NSIndexPath *indexPath = [self.tableContacts indexPathForRowAtPoint:buttonPosition];
    Callindepath = indexPath;
    [self performSegueWithIdentifier:@"Contact_VidoCall" sender:self];
    
}
-(IBAction)ClickOnVoiceCall:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableContacts];
    NSIndexPath *indexPath = [self.tableContacts indexPathForRowAtPoint:buttonPosition];
    Callindepath = indexPath;
    [self performSegueWithIdentifier:@"Contact_VoiceCall" sender:self];
    
}
-(IBAction)ClickOnInvite:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableContacts];
    NSIndexPath *indexPath = [self.tableContacts indexPathForRowAtPoint:buttonPosition];
    getindepath = indexPath;
    [self showPopView];
    
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            NSLog(@"Cancelled");
            break;
        case MessageComposeResultFailed:
            
            break;
        case MessageComposeResultSent:
            
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:NO completion:NULL];
}
-(void)showPopView
{
    UIView *viewForAudioOrVideo=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+self.navigationController.navigationBar.frame.size.height+50)];
    viewForAudioOrVideo.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7f];
    viewForAudioOrVideo.tag=9087650;
    [self.navigationController.view addSubview:viewForAudioOrVideo];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onRemoveViewAudioVideo:)]; // this is the current problem like a lot of people out there...
    [viewForAudioOrVideo addGestureRecognizer:tap];
    
    UIView *viewForWhiteBG=[[UIView alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)-140, (viewForAudioOrVideo.frame.size.height/2)-50, 280, 100)];
    viewForWhiteBG.backgroundColor=[UIColor whiteColor];
    viewForWhiteBG.layer.cornerRadius=3;
    viewForWhiteBG.clipsToBounds=YES;
    [viewForAudioOrVideo addSubview:viewForWhiteBG];
    
    UILabel *InfoLbl=[[UILabel alloc]initWithFrame:CGRectMake(20, 0, viewForWhiteBG.frame.size.width-40, 50)];
    [InfoLbl setText:@"Are you sure you want to invite this contact?"];
    InfoLbl.numberOfLines = 0;
    InfoLbl.textAlignment = NSTextAlignmentCenter;
    [InfoLbl setFont:[UIFont fontWithName:FONT_NORMAL size:14]];
    [viewForWhiteBG addSubview:InfoLbl];
    
    UIButton *cancel=[[UIButton alloc]initWithFrame:CGRectMake(35, 51, 50, 30)];
    [cancel setTitle:@"No" forState:UIControlStateNormal];
    [cancel setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    cancel.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:14];
    [cancel addTarget:self action:@selector(ClickOnCancel:) forControlEvents:UIControlEventTouchUpInside];
    [viewForWhiteBG addSubview:cancel];
    
    UIButton *ok=[[UIButton alloc]initWithFrame:CGRectMake(215, 51, 50, 30)];
    [ok setTitle:@"Yes" forState:UIControlStateNormal];
    [ok setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    ok.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:14];
    [ok addTarget:self action:@selector(clickOnOk:) forControlEvents:UIControlEventTouchUpInside];
    [viewForWhiteBG addSubview:ok];
}
- (IBAction)ClickOnCancel:(id)sender {
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    [viewForAudioOrVideo removeFromSuperview];
}

- (IBAction)clickOnOk:(id)sender {
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    [viewForAudioOrVideo removeFromSuperview];
    NSDictionary *dictArt=[TableArray objectAtIndex:getindepath.row];
    NSString *strnumber=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"mobile"]];
    MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
    picker.messageComposeDelegate = self;
    picker.view.tintColor=[UIColor blackColor];
    picker.recipients = [NSArray arrayWithObjects:strnumber, nil];
    picker.body = @"Hey, Check out Acedais to connect with your friends,family and Colleagues . Download it today from:";
    //https://itunes.apple.com/us/app/bitchat-messenger/id1303410382?ls=1&mt=8;
    
    [self presentViewController:picker animated:NO completion:NULL];
    
    
}
- (void) onRemoveViewAudioVideo: (UIGestureRecognizer *) gesture
{
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    [viewForAudioOrVideo removeFromSuperview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)ShowPasswordView
{
    UIView *viewForPassword=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+self.navigationController.navigationBar.frame.size.height+50)];
    viewForPassword.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7f];
    viewForPassword.tag=9087651;
    [self.navigationController.view addSubview:viewForPassword];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onRemovePasswordView:)]; // this is the current problem like a lot of people out there...
    [viewForPassword addGestureRecognizer:tap];
    self.passwordView.hidden=NO;
    
    [viewForPassword addSubview:self.passwordView];
}
- (void) onRemovePasswordView: (UIGestureRecognizer *) gesture
{
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087651];
    self.passwordView.hidden=YES;
    [viewForAudioOrVideo removeFromSuperview];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"Contacts_SingleChat"])
    {
        SingleChatVC *Single=[segue destinationViewController];
        Single.dictDetails=dictToSendSingleChat;
        appDelegate.RoomTYPE=[dictToSendSingleChat valueForKey:@"chatRoomType"];
        NSLog(@"%@",dictToSendSingleChat);
    }
    else if ([[segue identifier] isEqualToString:@"tootp"])
    {
        
        ForgotPasswordVC *Details=[segue destinationViewController];
        Details.getChatId=ChatRoomId;
        
    }
    else if ([[segue identifier] isEqualToString:@"Contact_VoiceCall"])
    {
        NSDictionary *dictDetails = [arrContacts objectAtIndex:Callindepath.row];
        NSString *strImage=[dictDetails valueForKey:@"image"];
        
        VoiceCallVC *Details=[segue destinationViewController];
        Details.strZoeChatID=[dictDetails valueForKey:@"zoechatid"];
        Details.strName=[dictDetails valueForKey:@"name"];
        Details.strimage=strImage;
        Details.strInComing=@"no";
    }
    else if ([[segue identifier] isEqualToString:@"Contact_VidoCall"])
    {
        NSDictionary *dictDetails = [arrContacts objectAtIndex:Callindepath.row];
        NSString *strImage=[dictDetails valueForKey:@"image"];
        
        VoiceCallVC *Details=[segue destinationViewController];
        Details.strZoeChatID=[dictDetails valueForKey:@"zoechatid"];
        Details.strName=[dictDetails valueForKey:@"name"];
        Details.strimage=strImage;
        Details.strInComing=@"no";
    }
}


#pragma mark -
#pragma mark - Font

-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)view;
        [btn setFont:[UIFont fontWithName:fontFamily size:[[btn font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *txtFld = (UITextField *)view;
        [txtFld setFont:[UIFont fontWithName:fontFamily size:[[txtFld font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextView class]])
    {
        UITextView *txtVw = (UITextView *)view;
        [txtVw setFont:[UIFont fontWithName:fontFamily size:[[txtVw font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
    
}



- (IBAction)ClickOnSubmitBtn:(id)sender {
    
    
    NSDictionary *checkZoeidDict=[arrContacts objectAtIndex:getindexval];
    NSString *strid=[checkZoeidDict valueForKey:@"zoechatid"];
    ChatRoomId=[NSString stringWithFormat:@"%@",strid];
    NSDictionary *dict=[db_class getOneChat:strid];
    NSLog(@"%@",dict);
    NSString *strcheckPassword=[NSString stringWithFormat:@"%@",[dict valueForKey:@"password"]];
    if ([strcheckPassword isEqualToString:_passwordTxt.text])
    {
        
        UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
        _passwordTxt.text=@"";
        self.passwordView.hidden=YES;
        [viewForAudioOrVideo removeFromSuperview];
        dictToSendSingleChat=[[NSMutableDictionary alloc]init];
        dictToSendSingleChat=[arrContacts objectAtIndex:getindexval];
        [ dictToSendSingleChat setObject:@"0" forKey:@"chatRoomType"];
        [self performSegueWithIdentifier:@"Contacts_SingleChat" sender:self];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Enter Correct Password"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.label.adjustsFontSizeToFitWidth=YES;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:2];
    }
    
    
    
    
    
    //
    //
    //
    //
    //
    //    if (arrChats.count!=0)
    //    {
    //        for (int i=0; i<arrChats.count; i++)
    //        {
    //            NSMutableDictionary *dictSing=[arrChats objectAtIndex:i];
    //            NSDictionary *dictInfo =[db_class getUserInfo:[dictSing valueForKey:@"chatRoomId"]];
    //            NSString *Name=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"name"]];
    //            NSString *strImage=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"image"]];
    //            [dictSing setObject:Name forKey:@"name"];
    //            [dictSing setObject:strImage forKey:@"image"];
    //            NSLog(@"%@",dictSing);
    //
    //            ChatRoomId=[NSString stringWithFormat:@"%@",[dictSing valueForKey:@"chatRoomId"]];
    //            NSString *strcheckPassword=[NSString stringWithFormat:@"%@",[dictSing valueForKey:@"password"]];
    //
    //            if ([_passwordTxt.text isEqualToString:@""] )
    //            {
    //
    //                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    //
    //                // Configure for text only and offset down
    //                hud.mode = MBProgressHUDModeText;
    //                hud.label.text = [NSString stringWithFormat:@"Fill all Details"];
    //                hud.margin = 10.f;
    //                hud.yOffset = 150.f;
    //                hud.label.adjustsFontSizeToFitWidth=YES;
    //                hud.removeFromSuperViewOnHide = YES;
    //                [hud hideAnimated:YES afterDelay:2];
    //            }
    //
    //            else if ([strcheckPassword isEqualToString:_passwordTxt.text])
    //            {
    //                UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    //                _passwordTxt.text=@"";
    //                self.passwordView.hidden=YES;
    //                [viewForAudioOrVideo removeFromSuperview];
    //                dictToSendSingleChat=[[NSMutableDictionary alloc]init];
    //                dictToSendSingleChat=[arrContacts objectAtIndex:getindexval];
    //                [ dictToSendSingleChat setObject:@"0" forKey:@"chatRoomType"];
    //                [self performSegueWithIdentifier:@"Contacts_SingleChat" sender:self];
    //            }
    ////            else{
    ////                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    ////
    ////                // Configure for text only and offset down
    ////                hud.mode = MBProgressHUDModeText;
    ////                hud.label.text = [NSString stringWithFormat:@"Enter Correct Password"];
    ////                hud.margin = 10.f;
    ////                hud.yOffset = 150.f;
    ////                hud.label.adjustsFontSizeToFitWidth=YES;
    ////                hud.removeFromSuperViewOnHide = YES;
    ////                [hud hideAnimated:YES afterDelay:2];
    ////
    ////            }
    //
    //
    //        }
    //    }
    //
    
    
    
}

- (IBAction)ClickOnForgotPassword:(id)sender {
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    self.passwordView.hidden=YES;
    [viewForAudioOrVideo removeFromSuperview];
    [self performSegueWithIdentifier:@"tootp" sender:self];
}



#pragma mark -
#pragma mark - Search Delegate

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    
    [arrForTableView removeAllObjects];
    arrForTableView=[NSMutableArray arrayWithArray:arrChats];
    
    if (arrForTableView.count!=0)
    {
        [_tableContacts reloadData];
        
    }
    else
    {
        //_tablePatientDetails.hidden=YES;
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [TableArray removeAllObjects];
    NSString *strValue = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    
    if(![strValue isEqualToString:@""])
    {
        [TableArray removeAllObjects];
        isFilter=YES;
        
        
        for(NSDictionary * dictValue in WholeTableArray)
        {
            NSString * strGroupName;
            strGroupName = [dictValue objectForKey:@"name"];
            
            
            
            NSRange GroupName=[[strGroupName lowercaseString] rangeOfString:[strValue lowercaseString]];
            
            if(GroupName.location != NSNotFound)
                [TableArray addObject:dictValue];
            searchArray=[NSMutableArray arrayWithArray:TableArray];
        }
        
    }
    else if(strValue != nil && [strValue isEqualToString:@""])
    {
        isFilter=NO;
        
        [TableArray removeAllObjects];
        TableArray=[NSMutableArray arrayWithArray:WholeTableArray];
        searchArray=[NSMutableArray arrayWithArray:TableArray];
        
    }
    
    
    if (range.length==1 && range.location==0)
    {
        [TableArray removeAllObjects];
        TableArray=[NSMutableArray arrayWithArray:WholeTableArray];
        searchArray=[NSMutableArray arrayWithArray:TableArray];
        
    }
    
    
    
    if (TableArray.count!=0)
    {
        [_tableContacts reloadData];
        
    }
    
    
    
    
    return YES;
}
- (IBAction)ClickonNewContactBtn:(id)sender {
    ABNewPersonViewController *controller = [[ABNewPersonViewController alloc] init];
    controller.newPersonViewDelegate = self;
    controller.view.tintColor=[UIColor blackColor];
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark -
#pragma mark - ABNewPersonViewControllerDelegate
- (void)newPersonViewController:(ABNewPersonViewController *)newPersonView didCompleteWithNewPerson:(nullable ABRecordRef)person {
    // Trick to go back to your view by popping it from the navigation stack when done or cancel button is pressed
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)ClickOnInviteBtn:(id)sender {
    [self performSegueWithIdentifier:@"Contact_Invite" sender:self];
}
- (IBAction)ClickonNewGroupBtn:(id)sender {
    [self performSegueWithIdentifier:@"Contact_GreateGroup" sender:self];
}
@end
