//
//  MainTabBarVC.m
//  ZoeChat
//
//  Created by Pyramidions Solution on 31/12/16.
//  Copyright © 2016 Pyramidions Solution. All rights reserved.
//

#import "MainTabBarVC.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "Constants.h"
#import "Reachability.h"
#import "TermsAndConditionsVC.h"
#import "DBClass.h"
#import "MBProgressHUD.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIButton+WebCache.h"
#import "VoiceCallVC.h"
#import "VideoCallVC.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "RequestVC.h"
@import SocketIO;
#import "AFNetworking.h"


@interface MainTabBarVC ()<ABNewPersonViewControllerDelegate>
{
    AppDelegate *appDelegate;
    UIView *viewMenu;
    UIView *ViewSearch;
    UITapGestureRecognizer *tapViewMenu;
    NSString *strActiveTabBar;
   // UISearchBar *searchBar;
     NSMutableArray *arrContacts;
    DBClass *db_class;
    BOOL isRefreshBtnPressed;
    BOOL isFirstClick;

}
@end

@implementation MainTabBarVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    isFirstClick=YES;
    // Do any additional setup after loading the view.
    appDelegate =(AppDelegate *)[[UIApplication sharedApplication]delegate];
    db_class=[[DBClass alloc]init];
     [self setFontFamily:FONT_NORMAL forView:self.view andSubViews:YES];
    self.delegate=self;
    strActiveTabBar=@"chat";
    isRefreshBtnPressed=NO;
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    if (reachability.isReachable) {
        NSLog(@"We have internet!");
        appDelegate.strInternetMode=@"online";
    } else {
        NSLog(@"No internet!");
        appDelegate.strInternetMode=@"ofline";
    }
    
    
    NSLog(@"%@",appDelegate.strInternetMode);
    
  //  cell.ContactStatus.font=[UIFont fontWithName:FONT_NORMAL size:16];
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{ NSFontAttributeName: [UIFont fontWithName:FONT_NORMAL size:18.0f]}];
    
    self.navigationItem.hidesBackButton = YES;
    


           NSString *strImage=appDelegate.strProfilePic;
        NSURL *imageURL = [NSURL URLWithString:strImage];
        // [self.btnProfile setImage:[UIImage imageNamed:@"user.png"] forState:UIControlStateNormal];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:imageURL
                              options:SDWebImageRefreshCached
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    // do something with image
                                    
                                }
                            }];
        
    
   
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inCommingCall) name:@"Incoming_Call" object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchContactsandAuthorization) name:@"Ask_Refresh_Contacts" object:nil];
    
     [self fetchContactsandAuthorization];

    
    [self updateToken];
}


-(void)updateToken
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:appDelegate.strID forKey:@"id"];
    [dictParam setValue:appDelegate.strDeviceToken forKey:@"pushNotificationId"];
    [dictParam setValue:@"IOS" forKey:@"os_type"];
    [dictParam setValue:appDelegate.strTokenVOIP forKey:@"voipToken"];
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:@"http://18.207.40.111:3000/user/updateDeviceToken" parameters:dictParam progress:nil success:^(NSURLSessionTask *task, id responseObject)
    {
        NSLog(@"ResponseObject: %@", responseObject);
        
        if ([[responseObject valueForKey:@"error"] boolValue]==false)
        {
            
        }
        else
        {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                           message:[responseObject valueForKey:@"message"]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            alert.view.tintColor=[UIColor blackColor];

            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action)
            {
                [self removeDB:@"ZoeChat.db"];
            }];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];

        }
        
        
    }
    failure:^(NSURLSessionTask *operation, NSError *error)
    {
        NSLog(@"updateDeviceToken Error: %@", error);
    }];


    /*
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    
    NSLog(@"Params = %@",dictParam);
    
    [afn getDataFromPath:UPDATEDEVICETOKEN withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         
         if (response)
         {
             NSLog(@"Update Token = : %@",response);
             
             
             if ([[response valueForKey:@"error"] boolValue]==false)
             {
                 
             }
             else
             {
                 
             }
         }
         
     }];
     */
}


- (void)removeDB:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success)
    {
        [self resetDefaults];
        UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MainTabBarVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainNavView"];
        [self presentViewController:hme animated:YES completion:nil];
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

- (void)resetDefaults
{
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.leftBarButtonItem=nil;
    [self.navigationItem setHidesBackButton:YES animated:YES];
   // self.title=@"Message";
     NSLog(@"%@",appDelegate.strInternetMode);
    
//    [UINavigationBar appearance].tintColor = [UIColor colorWithRed:(247.0f/255.0f) green:(247.0f/255.0f) blue:(247.0f/255.0f) alpha:1];
   
    self.navigationItem.hidesBackButton = YES;

    [UINavigationBar appearance].tintColor = [UIColor whiteColor];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    if (reachability.isReachable)
    {
        NSLog(@"We have internet!");
        appDelegate.strInternetMode=@"online";
    }
    else
    {
        NSLog(@"No internet!");
        appDelegate.strInternetMode=@"ofline";
    }
    
    
//    [appDelegate.socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
//        NSLog(@"socket connected");
//        [appDelegate.socket emit:@"online" with:@[@{@"id": appDelegate.strID}]];
//        
//    }];
 
}
- (void)networkChanged:(NSNotification *)notification
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    if (reachability.isReachable)
    {
        NSLog(@"We have internet!");
        appDelegate.strInternetMode=@"online";
    }
    else
    {
        NSLog(@"No internet!");
        appDelegate.strInternetMode=@"ofline";
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    NSLog(@"selected %lu",(unsigned long)tabBarController.selectedIndex);
    
    if (tabBarController.selectedIndex==0)
    {
        self.title=@"Messages";
        [_btnSearch setImage:[UIImage imageNamed:@"Search.png"]];
        [_ContactSearch setImage:[UIImage imageNamed:@""]];
        strActiveTabBar=@"chat";
    }
    else if (tabBarController.selectedIndex==1)
    {
        self.title=@"Contacts";
        [_btnSearch setImage:[UIImage imageNamed:@"AddContact.png"]];
        [_ContactSearch setImage:[UIImage imageNamed:@"Search.png"]];

        strActiveTabBar=@"contact";
    }
    else if (tabBarController.selectedIndex==2)
    {
        self.title=@"Calls";
        [_btnSearch setImage:[UIImage imageNamed:@"AddContact.png"]];
        [_ContactSearch setImage:[UIImage imageNamed:@""]];

        strActiveTabBar=@"call";
    }
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
     if ([[segue identifier] isEqualToString:@"SingleChat_VoiceCallVC"])
    {
        NSString *strImage=[appDelegate.dictInComomgCall valueForKey:@"image"];
        
        VoiceCallVC *Details=[segue destinationViewController];
        Details.strZoeChatID=[appDelegate.dictInComomgCall valueForKey:@"zoechatid"];;
        Details.strName=[appDelegate.dictInComomgCall valueForKey:@"name"];
        Details.strimage=strImage;
        Details.strInComing=@"yes";
    }
    else if ([[segue identifier] isEqualToString:@"SingleChat_VideoCallVC"])
    {
        NSString *strImage=[appDelegate.dictInComomgCall valueForKey:@"image"];
        
        VoiceCallVC *Details=[segue destinationViewController];
        Details.strZoeChatID=[appDelegate.dictInComomgCall valueForKey:@"zoechatid"];;
        Details.strName=[appDelegate.dictInComomgCall valueForKey:@"name"];
        Details.strimage=strImage;
        Details.strInComing=@"yes";
    }

    
}

-(void)inCommingCall
{
     NSString *strCallType=[NSString stringWithFormat:@"%@",[appDelegate.dictInComomgCall valueForKey:@"callType"]];
    if ([strCallType isEqualToString:@"voicecall"])
    {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        VoiceCallVC *voiceCall = (VoiceCallVC *)[sb instantiateViewControllerWithIdentifier:@"VoiceCallVC"];
        [self.navigationController pushViewController:voiceCall animated:YES];
    }
    else
    {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        VideoCallVC *VideoCall = (VideoCallVC *)[sb instantiateViewControllerWithIdentifier:@"VideoCallVC"];
        [self.navigationController pushViewController:VideoCall animated:YES]; 
    }

    
   
}
- (IBAction)onMenu:(id)sender
{
    NSLog(@"%@",appDelegate.strInternetMode);
    if(isFirstClick )
    {
    [viewMenu removeFromSuperview];
    [self.view removeGestureRecognizer:tapViewMenu];
    
    if ([strActiveTabBar isEqualToString:@"chat"])
    {
        
        viewMenu=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-140, 10, 130, 200)];
        viewMenu.backgroundColor=[UIColor whiteColor];
        viewMenu.layer.borderWidth = 0.75;
        viewMenu.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        viewMenu.layer.cornerRadius = 2.0f;
        viewMenu.clipsToBounds=YES;
        [self.view addSubview:viewMenu];
        
        UIButton *NewGroup=[[UIButton alloc]initWithFrame:CGRectMake(5, 100, viewMenu.frame.size.width-10, 50)];
        [NewGroup setTitle:@"Group Request" forState:UIControlStateNormal];
        NewGroup.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        NewGroup.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:15];
        [NewGroup setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [NewGroup addTarget:self action:@selector(GroupRequest:) forControlEvents:UIControlEventTouchUpInside];
        [viewMenu addSubview:NewGroup];
        
        UIButton *btnNewGroup=[[UIButton alloc]initWithFrame:CGRectMake(5, 0, viewMenu.frame.size.width-10, 50)];
        [btnNewGroup setTitle:@"New Group" forState:UIControlStateNormal];
        [btnNewGroup setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btnNewGroup addTarget:self action:@selector(onNewGroup:) forControlEvents:UIControlEventTouchUpInside];
        btnNewGroup.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:15];
        btnNewGroup.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        [viewMenu addSubview:btnNewGroup];
        
        
        
        UIButton *btnSettings=[[UIButton alloc]initWithFrame:CGRectMake(5, 150, viewMenu.frame.size.width-10, 50)];
        [btnSettings setTitle:@"Settings" forState:UIControlStateNormal];
        btnSettings.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        btnSettings.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:15];
        [btnSettings setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btnSettings addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
        [viewMenu addSubview:btnSettings];
                //QR code
             /*   UIButton *btnZoeWeb=[[UIButton alloc]initWithFrame:CGRectMake(5, 100, viewMenu.frame.size.width-10, 50)];
                [btnZoeWeb setTitle:@"Acedais Web" forState:UIControlStateNormal];
                btnZoeWeb.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
                btnZoeWeb.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:15];
                [btnZoeWeb setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [btnZoeWeb addTarget:self action:@selector(onZoeWeb:) forControlEvents:UIControlEventTouchUpInside];
                [viewMenu addSubview:btnZoeWeb];
              */
        
        //StarredMsg
        UIButton *btnStarredMsg=[[UIButton alloc]initWithFrame:CGRectMake(5, 50, viewMenu.frame.size.width-10, 50)];
        [btnStarredMsg setTitle:@"Starred Message" forState:UIControlStateNormal];
        btnStarredMsg.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        btnStarredMsg.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:15];
        [btnStarredMsg setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btnStarredMsg addTarget:self action:@selector(onStarredMsg:) forControlEvents:UIControlEventTouchUpInside];
        [viewMenu addSubview:btnStarredMsg];
        //BroadCast
      /*  UIButton *btnBroadcast=[[UIButton alloc]initWithFrame:CGRectMake(5, 50, viewMenu.frame.size.width-10, 50)];
        [btnBroadcast setTitle:@"New Broadcast" forState:UIControlStateNormal];
        btnBroadcast.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        btnBroadcast.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:15];
        [btnBroadcast setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btnBroadcast addTarget:self action:@selector(onBroadcast:) forControlEvents:UIControlEventTouchUpInside];
        [viewMenu addSubview:btnBroadcast];
        */
        
        
        
    }
    else if ([strActiveTabBar isEqualToString:@"contact"])
    {
        viewMenu=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-140, 10, 130, 150)];
        viewMenu.backgroundColor=[UIColor whiteColor];
        viewMenu.layer.borderWidth = 0.75;
        viewMenu.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        viewMenu.layer.cornerRadius = 2.0f;
        viewMenu.clipsToBounds=YES;
        [self.view addSubview:viewMenu];
        
        UIButton *btnRefresh=[[UIButton alloc]initWithFrame:CGRectMake(5, 0, viewMenu.frame.size.width-10, 50)];
        [btnRefresh setTitle:@"Refresh" forState:UIControlStateNormal];
        btnRefresh.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        [btnRefresh setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btnRefresh addTarget:self action:@selector(onRefresh:) forControlEvents:UIControlEventTouchUpInside];
        btnRefresh.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:15];
        [viewMenu addSubview:btnRefresh];
        
//        UIButton *btnUpdateStatus=[[UIButton alloc]initWithFrame:CGRectMake(5, 50, viewMenu.frame.size.width-10, 50)];
//        [btnUpdateStatus setTitle:@"Update Status" forState:UIControlStateNormal];
//        [btnUpdateStatus setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        [btnUpdateStatus addTarget:self action:@selector(onUpdateStatus:) forControlEvents:UIControlEventTouchUpInside];
//        btnUpdateStatus.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:15];
//        btnUpdateStatus.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
//        [viewMenu addSubview:btnUpdateStatus];
        
        UIButton *btnSettings=[[UIButton alloc]initWithFrame:CGRectMake(5, 50, viewMenu.frame.size.width-10, 50)];
        [btnSettings setTitle:@"Settings" forState:UIControlStateNormal];
        btnSettings.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        btnSettings.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:15];
        [btnSettings setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btnSettings addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
        [viewMenu addSubview:btnSettings];
        //Invite
        UIButton *btnInvite=[[UIButton alloc]initWithFrame:CGRectMake(5, 100, viewMenu.frame.size.width-10, 50)];
        [btnInvite setTitle:@"Invite Friends" forState:UIControlStateNormal];
        btnInvite.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        btnInvite.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:15];
        [btnInvite setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btnInvite addTarget:self action:@selector(onInvite:) forControlEvents:UIControlEventTouchUpInside];
        [viewMenu addSubview:btnInvite];
        
    }
    else
    {
        
        viewMenu=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-140, 10, 130, 100)];
        viewMenu.backgroundColor=[UIColor whiteColor];
        viewMenu.layer.borderWidth = 0.75;
        viewMenu.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        viewMenu.layer.cornerRadius = 2.0f;
        viewMenu.clipsToBounds=YES;
        [self.view addSubview:viewMenu];
        
        
        UIButton *btnClearLog=[[UIButton alloc]initWithFrame:CGRectMake(5, 0, viewMenu.frame.size.width-10, 50)];
        [btnClearLog setTitle:@"Clear Log" forState:UIControlStateNormal];
        [btnClearLog setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btnClearLog addTarget:self action:@selector(onClearLog:) forControlEvents:UIControlEventTouchUpInside];
        btnClearLog.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:15];
        btnClearLog.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        [viewMenu addSubview:btnClearLog];
        
        UIButton *btnSettings=[[UIButton alloc]initWithFrame:CGRectMake(5, 50, viewMenu.frame.size.width-10, 50)];
        [btnSettings setTitle:@"Settings" forState:UIControlStateNormal];
        btnSettings.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        btnSettings.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:15];
        [btnSettings setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btnSettings addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
        [viewMenu addSubview:btnSettings];
    }
    tapViewMenu=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(RemoveSettings)];
    
    [self.view addGestureRecognizer:tapViewMenu];
        isFirstClick=NO;
}
    else if(isFirstClick == NO)
    {
        [viewMenu removeFromSuperview];
        [self.view removeGestureRecognizer:tapViewMenu];
        isFirstClick=YES;
        
    }
}
- (IBAction)onSearch:(id)sender
{
     [viewMenu removeFromSuperview];
    [self.view removeGestureRecognizer:tapViewMenu];
    isFirstClick=YES;
    if ([strActiveTabBar isEqualToString:@"chat"])
    {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Show_Search_View" object:self];
    }
    else
    {
        ABNewPersonViewController *controller = [[ABNewPersonViewController alloc] init];
        controller.newPersonViewDelegate = self;
        controller.view.tintColor=[UIColor blackColor];
               [self.navigationController pushViewController:controller animated:YES];
    }
}

- (IBAction)onRefresh:(id)sender
{
    [self RemoveSettings];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
     isRefreshBtnPressed=YES;
    [self fetchContactsandAuthorization];

}
-(IBAction)onNewGroup:(id)sender
{
     [viewMenu removeFromSuperview];
    isFirstClick=YES;
    [self performSegueWithIdentifier:@"MainTab_CreateGroup" sender:self];
}
- (IBAction)onUpdateStatus:(id)sender {
}
- (IBAction)onSettings:(id)sender
{
    [viewMenu removeFromSuperview];
    [self.view removeGestureRecognizer:tapViewMenu];
    isFirstClick=YES;

    [self performSegueWithIdentifier:@"ToSettingVc" sender:self];
}
- (IBAction)GroupRequest:(id)sender
{
    [viewMenu removeFromSuperview];
    [self.view removeGestureRecognizer:tapViewMenu];
    isFirstClick = YES;
   
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    RequestVC * vc = [storyboard instantiateViewControllerWithIdentifier:@"RequestVC"];
//    vc.checkBack = @"1";
//    vc.dictDetails=chatDict;
    
    [self.navigationController pushViewController:vc animated:YES];
}
//Invite
- (IBAction)onInvite:(id)sender
{
    [viewMenu removeFromSuperview];
    [self.view removeGestureRecognizer:tapViewMenu];
    isFirstClick=YES;
    
    [self performSegueWithIdentifier:@"ToInviteVC" sender:self];
}
//QR code
- (IBAction)onZoeWeb:(id)sender
{
    [viewMenu removeFromSuperview];
    [self.view removeGestureRecognizer:tapViewMenu];
    isFirstClick=YES;
    [self performSegueWithIdentifier:@"ToQRCodeVC" sender:self];
}
//StarredMsg
- (IBAction)onStarredMsg:(id)sender
{
    [viewMenu removeFromSuperview];
    [self.view removeGestureRecognizer:tapViewMenu];
    isFirstClick=YES;
    [self performSegueWithIdentifier:@"ToStarredMsg" sender:self];
}

- (IBAction)onBroadcast:(id)sender
{
    [viewMenu removeFromSuperview];
    appDelegate.isBroadCast=@"YES";
    isFirstClick=YES;
    [self performSegueWithIdentifier:@"MainTab_CreateGroup" sender:self];
}

-(IBAction)onClearLog:(id)sender
{
     [self RemoveSettings];
    
    BOOL success=[db_class deleteAllCalls];
    if (success)
    {
          [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Calls" object:self];
    }
    
}
-(void)RemoveSettings
{
    [viewMenu removeFromSuperview];
    [self.view removeGestureRecognizer:tapViewMenu];
}

-(IBAction)onBack:(id)sender
{
  //  [self.navigationController popViewControllerAnimated:YES];
}

-(void)fetchContactsandAuthorization
{
    // Request authorization to Contacts
    CNContactStore *store = [[CNContactStore alloc] init];
    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted == YES)
        {
            arrContacts=[[NSMutableArray alloc]init];
            //keys with fetching properties
            NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey];
            NSString *containerId = store.defaultContainerIdentifier;
            NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
            NSError *error;
            NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
            if (error)
            {
                NSLog(@"error fetching contacts %@", error);
            }
            else
            {
                NSString *phone;
                NSString *fullName=@"";
                NSString *firstName;
                NSString *lastName;
                
                for (CNContact *contact in cnContacts) {
                    // copy data to my custom Contacts class.
                    firstName = contact.givenName;
                    lastName = contact.familyName;
                    if (lastName == nil) {
                        fullName=[NSString stringWithFormat:@"%@",firstName];
                    }else if (firstName == nil){
                        fullName=[NSString stringWithFormat:@"%@",lastName];
                    }
                    else{
                        fullName=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                    }
                   fullName = [fullName stringByTrimmingCharactersInSet:
                                               [NSCharacterSet whitespaceCharacterSet]];
                    
                    NSMutableArray *contactNumbersArray = [[NSMutableArray alloc]init];
                    for (CNLabeledValue *label in contact.phoneNumbers) {
                        phone = [label.value stringValue];
                        if ([phone length] > 0) {
                            [contactNumbersArray addObject:phone];
                        }
                    }
                    NSMutableDictionary *dictContact=[[NSMutableDictionary alloc]init];
                    [dictContact setObject:fullName forKey:@"Name"];
                    
                    if (contactNumbersArray.count!=0)
                    {
                        for (int l=1; l<=contactNumbersArray.count; l++)
                        {
                            NSString *strPhNo=[NSString stringWithFormat:@"PhoneNumber%d",l];
                            NSString* strPhone = [[contactNumbersArray objectAtIndex:l-1]stringByReplacingOccurrencesOfString:@"\u00a0" withString:@""];
                            strPhone = [strPhone stringByReplacingOccurrencesOfString:@"-" withString:@""];
                            strPhone = [strPhone stringByReplacingOccurrencesOfString:@" " withString:@""];
                            strPhone = [strPhone stringByReplacingOccurrencesOfString:@"(" withString:@""];
                            strPhone = [strPhone stringByReplacingOccurrencesOfString:@")" withString:@""];


                            NSString *NEWstrPhone;
                            if ([strPhone containsString:@"+"]) {
                                
                                strPhone = [strPhone stringByReplacingOccurrencesOfString:@"+" withString:@""];
                                NEWstrPhone=[NSString stringWithFormat:@"%@",strPhone];
                                
                                
                            }
                            else {
                                NSString *setCountryCode = [[NSUserDefaults standardUserDefaults]stringForKey:@"getCountryCode"];
                                NEWstrPhone=[NSString stringWithFormat:@"%@%@",setCountryCode,strPhone];
                            }
                            [dictContact setObject:NEWstrPhone forKey:strPhNo];
                        }
                    }
                    [arrContacts addObject:dictContact];
                    // NSLog(@"The contactsArray are - %@",arrContacts);
                }
                
            }
            
           
           //  [self insertUser];
            
            dispatch_queue_t queue =dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
            dispatch_async(queue, ^{
                [self insertUser];
               
            });
            
        }
    }];
    
}
-(void)insertUser
{
     [self deleteUser];
    BOOL success = NO;
    //   NSString *alertString = @"Data Insertion failed";
    
    for (int i=0; i<arrContacts.count; i++)
    {
        NSDictionary *dictCont=[arrContacts objectAtIndex:i];
        
        NSString *strMobile=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"PhoneNumber1"]];
        NSString *strName=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"Name"]];
        
        NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
        if ([[strName stringByTrimmingCharactersInSet: set] length] != 0)
        {
            success =[db_class insertUser:strMobile name:strName registeredName:@"" image:@"" status:@"" showInContactsPage:@"0" zoeChatId:@""];
            
            if (success == NO)
            {
                success=[db_class updateUserName:strMobile name:strName];
            }

        }
        
    }
    
    [self getUsers];
}

-(void)deleteUser
{
    NSArray *arrUsers=[db_class getUsers];
    
    BOOL success = NO;
    for (int i=0; i<arrUsers.count; i++)
    {
        int nAvail=0;
        NSDictionary *dictUser=[arrUsers objectAtIndex:i];
        NSString *strMobile=[NSString stringWithFormat:@"%@",[dictUser valueForKey:@"mobile"]];
        for (int j=0; j<arrContacts.count; j++)
        {
            NSDictionary *dictCont=[arrContacts objectAtIndex:j];
            NSString *strPhone;
            if ([[dictCont allKeys] containsObject:@"PhoneNumber1"])
            {
                    strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"PhoneNumber1"]];
            }
            else
            {
            }
        
            
            if ([strPhone isEqualToString:strMobile])
            {
                nAvail=1;
            }
        }
        if (nAvail==0)
        {
            success=[db_class deleteUser:strMobile];
        }
        
    }
    
}

-(void)getUsers
{
    //   NSDictionary *data = [[DBManager getSharedInstance]getUserName:@"9876543210"];
    //  NSDictionary *data = [[DBManager getSharedInstance]getUserImage:@"9876543210"];
    //  NSDictionary *data = [[DBManager getSharedInstance]getUserInfo:@"9876543210"];
    NSArray *arrContdata=[db_class getUsers];
    
    
    NSMutableArray *arrContactsToSend=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrContdata.count; i++)
    {
        NSMutableDictionary *dictContact=[[NSMutableDictionary alloc]init];
        NSDictionary *dictCont=[arrContdata objectAtIndex:i];
        NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"mobile"]];
        [dictContact setObject:strPhone forKey:@"mobileNumber"];
        NSString *strname=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"name"]];
        [dictContact setObject:strname forKey:@"contactName"];
        
        [arrContactsToSend addObject:dictContact];
        
        
    }
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrContactsToSend options:0 error:&error];
    NSString *jsonString;
    
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    NSString* encodedString = [jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:appDelegate.strID forKey:@"from"];

    [dictParam setValue:jsonString forKey:@"contacts"];
    //    MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    //    hud.mode = MBProgressHUDAnimationFade;
    //    hud.label.text = @"Loading";
    NSLog(@"%@",appDelegate.strInternetMode);
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_CONTACTS_SYNC withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
         [UIApplication sharedApplication].networkActivityIndicatorVisible = FALSE;
         if (response)
         {
             //  NSLog(@"Response: %@",response);
             if ([[response valueForKey:@"error"] boolValue]==false)
             {
                 NSLog(@"Success");
                 NSArray *arrContactsToUpdate=[response valueForKey:@"contacts"];
                 for (int i=0; i<arrContactsToUpdate.count; i++)
                 {
                     NSDictionary *dictCont=[arrContactsToUpdate objectAtIndex:i];
                     if ([[dictCont valueForKey:@"showInContactsPage"] boolValue]==true)
                     {
                         NSString *strZoeChatId = [[dictCont valueForKey:@"zoechatid"] stringByReplacingOccurrencesOfString:@"+" withString:@""];
                         BOOL success = NO;
                         success=[db_class updateUser:[dictCont valueForKey:@"mobileNumber"] registeredName:[dictCont valueForKey:@"name"] image:[dictCont valueForKey:@"image"] status:[dictCont valueForKey:@"status"] showInContactsPage:@"1" zoeChatId:strZoeChatId];
                     }
                     
                 }
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Contacts" object:self];
                 
                 if (isRefreshBtnPressed)
                 {
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     // Configure for text only and offset down
                     hud.mode = MBProgressHUDModeText;
                     hud.label.text = @"Contacts refreshed";
                     hud.margin = 10.f;
                     hud.yOffset = 150.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hideAnimated:YES afterDelay:3];
                      isRefreshBtnPressed=NO;
                 }
                

             }
             else
             {
                 
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 // Configure for text only and offset down
                 hud.mode = MBProgressHUDModeText;
                 hud.label.text = [NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                 hud.margin = 10.f;
                 hud.yOffset = 150.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hideAnimated:YES afterDelay:3];
                 
                 
             }
         }
          isRefreshBtnPressed=NO;
     }];
    
    
    if (arrContdata == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:
                              @"Data not found" message:nil delegate:nil cancelButtonTitle:
                              @"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        
        // NSString *strName =[data objectAtIndex:0];
        // NSLog(@"%@",strName);
        
    }
}





#pragma mark -
#pragma mark - Font

-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)view;
        [btn setFont:[UIFont fontWithName:fontFamily size:[[btn font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *txtFld = (UITextField *)view;
        [txtFld setFont:[UIFont fontWithName:fontFamily size:[[txtFld font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextView class]])
    {
        UITextView *txtVw = (UITextView *)view;
        [txtVw setFont:[UIFont fontWithName:fontFamily size:[[txtVw font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}

#pragma mark -
#pragma mark - ABNewPersonViewControllerDelegate
- (void)newPersonViewController:(ABNewPersonViewController *)newPersonView didCompleteWithNewPerson:(nullable ABRecordRef)person {
    // Trick to go back to your view by popping it from the navigation stack when done or cancel button is pressed
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)ClickOnContactSearch:(id)sender
{
    [viewMenu removeFromSuperview];
    [self.view removeGestureRecognizer:tapViewMenu];
    
    if ([strActiveTabBar isEqualToString:@"contact"])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Show_Search" object:self];
    }
    else
    {
        ABNewPersonViewController *controller = [[ABNewPersonViewController alloc] init];
        controller.newPersonViewDelegate = self;
        [self.navigationController pushViewController:controller animated:YES];
    }

}
@end
