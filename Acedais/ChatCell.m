//
//  ChatCell.m
//  ZoeChat
//
//  Created by Pyramidions Solution on 10/01/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "ChatCell.h"

@implementation ChatCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    
    UIColor *color = [UIColor grayColor];
    
    self.pinChat.tintColor = color;
    
    UIImage *newImage = [self.pinChat.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIGraphicsBeginImageContextWithOptions(self.pinChat.image.size, NO, self.pinChat.image.scale);
    [color set];
    [newImage drawInRect:CGRectMake(0, 0, self.pinChat.image.size.width, self.pinChat.image.size.height)];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.pinChat.image = newImage;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
