//
//  BackupVC.m
//  ZoeChat
//
//  Created by veena on 2/28/18.
//  Copyright © 2018 Pyramidions Solution. All rights reserved.
//

#import "BackupVC.h"
#import "SingleChatVC.h"
#import "DBClass.h"

@interface BackupVC ()
{
    DBClass *db_class;
    NSString *msgCount;
    NSString *localDateString;
}
@end

@implementation BackupVC{
    
    AppDelegate *appDelegate;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];

    
    db_class=[[DBClass alloc]init];
    msgCount = [db_class getRowCount];
}


-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//    [UINavigationBar appearance].tintColor = [UIColor redColor];
    
 /*
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        
        [UINavigationBar appearance].tintColor = color;
    }
    else
    {
        
        [UINavigationBar appearance].tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        
    }*/
    
}


-(void)viewDidAppear:(BOOL)animated
{
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        
        [UINavigationBar appearance].tintColor = color;
    }
    else
    {
        
        [UINavigationBar appearance].tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        
    }
}




/*
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [UINavigationBar appearance].tintColor = [UIColor whiteColor];
}

*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)upload:(id)sender
{
    NSDate *now=[NSDate date];
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyyMMddHHmmssSSS"];
    localDateString = [dateFormatter1 stringFromDate:now];
    


    
    

    
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docDirectory = [paths objectAtIndex:0];
    
        NSLog(@"docDirectory = %@",docDirectory);
    
    NSString *filePath = NSTemporaryDirectory();
    
    NSString *str = [NSString stringWithFormat:@"%@_ZoeChat.zip",localDateString];
    
    filePath = [filePath stringByAppendingString:str];
    
    BOOL status = [SSZipArchive createZipFileAtPath:filePath withContentsOfDirectory:docDirectory];
    
        NSURL *filePathToUpload = [NSURL fileURLWithPath:filePath];
    
        UIDocumentPickerViewController *docPicker = [[UIDocumentPickerViewController alloc] initWithURL:filePathToUpload inMode:UIDocumentPickerModeExportToService];
        docPicker.delegate = self;
        docPicker.modalPresentationStyle = UIModalPresentationFormSheet;

    
        [self presentViewController:docPicker animated:YES completion:nil];

}



- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url
{
    if (controller.documentPickerMode == UIDocumentPickerModeImport)
    {
        
        // Condition called when user download the file
        NSData *fileData = [NSData dataWithContentsOfURL:url];
        
        NSLog(@"%@",fileData);
        
        // NSData of the content that was downloaded - Use this to upload on the server or save locally in directory
        
        //Showing alert for success
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *alertMessage = [NSString stringWithFormat:@"Successfully downloaded file %@", [url lastPathComponent]];
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Zoechat"
                                                  message:alertMessage
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
            
            
            NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
            UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
            if (color != nil) {
                alertController.view.tintColor=color;
            }
            else
            {
                
                alertController.view.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
                
            }

            
            
            [self presentViewController:alertController animated:YES completion:nil];
            
        });
    }
    else  if (controller.documentPickerMode == UIDocumentPickerModeExportToService)
    {
        // Called when user uploaded the file - Display success alert
        
        
            NSString *alertMessage = [NSString stringWithFormat:@"Successfully uploaded file %@", [url lastPathComponent]];
            
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Zoechat"
                                         message:alertMessage
                                         preferredStyle:UIAlertControllerStyleAlert];
        
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Ok"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                                            
                                            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                                            [dictParam setValue:appDelegate.strID
                                                         forKey:@"zoechatid"];
                                            [dictParam setValue:appDelegate.strName forKey:@"accountName"];
                                            [dictParam setValue:@"true" forKey:@"isDriveLogin"];
                                            [dictParam setValue:localDateString forKey:@"time"];
                                            [dictParam setValue:@"abcdefg" forKey:@"driveId"];
                                            [dictParam setValue:msgCount forKey:@"messageCount"];
                                            
                                            
                                            
                                            
                                            NSLog(@"Dict Param = %@",dictParam);
                                            
                                            
                                            MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                                            hud.mode = MBProgressHUDAnimationFade;
                                            hud.label.text = @"Loading";
                                            
                                            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                                            [afn getDataFromPath:FILE_BACKUPUPDATE withParamData:dictParam withBlock:^(id response, NSError *error)
                                             {
                                                 [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                                                 if (response)
                                                 {
                                                     NSLog(@"Response: %@",response);
                                                     if ([[response valueForKey:@"error"] boolValue]==false)
                                                     {
                                                         
                                                     }
                                                     
                                                 }
                                                 
                                             }];
                                            
                                            
                                            
                                        }];
           
            [alert addAction:yesButton];
        
        
        NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
        UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
        if (color != nil) {
            alert.view.tintColor=color;
        }
        else
        {
            
            alert.view.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
            
        }

        
        
        
            [self presentViewController:alert animated:YES completion:nil];
        }
}


@end
