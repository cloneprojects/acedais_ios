//
//  QRCodeVC.m
//  ZoeChat
//
//  Created by macmini on 30/05/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "QRCodeVC.h"
#import <AVFoundation/AVFoundation.h>
#import "AppDelegate.h"
#import "MainTabBarVC.h"

@interface QRCodeVC () <AVCaptureMetadataOutputObjectsDelegate>
{
    AVCaptureSession *_session;
    AVCaptureDevice *_device;
    AVCaptureDeviceInput *_input;
    AVCaptureMetadataOutput *_output;
    AVCaptureVideoPreviewLayer *_prevLayer;
    
    UIView *_highlightView;
    UILabel *_label;
    UILabel *InfoLbl;
    AppDelegate *appDelegate;

    
}
@end

@implementation QRCodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    _highlightView = [[UIView alloc] init];
    _highlightView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
    _highlightView.layer.borderColor = [UIColor greenColor].CGColor;
    _highlightView.layer.borderWidth = 3;
    [self.view addSubview:_highlightView];
    
    _label = [[UILabel alloc] init];
    _label.frame = CGRectMake(0, self.view.bounds.size.height - 40, self.view.bounds.size.width, 40);
    _label.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    _label.backgroundColor = [UIColor colorWithWhite:0.15 alpha:0.65];
    _label.textColor = [UIColor whiteColor];
    _label.textAlignment = NSTextAlignmentCenter;
    //_label.text = @"(none)";
    [self.view addSubview:_label];
    
    InfoLbl = [[UILabel alloc] init];
    InfoLbl.frame = CGRectMake(0, 60, self.view.bounds.size.width, 60);
    InfoLbl.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    InfoLbl.backgroundColor = [UIColor whiteColor];
    InfoLbl.textColor = [UIColor blackColor];
    InfoLbl.textAlignment = NSTextAlignmentCenter;
    InfoLbl.numberOfLines = 0;
    InfoLbl.text = @"Go to web.zoechat.com on your computer and scan the QR code";
    [self.view addSubview:InfoLbl];
    
    
    _session = [[AVCaptureSession alloc] init];
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    
    _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
    if (_input) {
        [_session addInput:_input];
    } else {
        NSLog(@"Error: %@", error);
    }
    
    _output = [[AVCaptureMetadataOutput alloc] init];
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    [_session addOutput:_output];
    
    _output.metadataObjectTypes = [_output availableMetadataObjectTypes];
    
    _prevLayer = [AVCaptureVideoPreviewLayer layerWithSession:_session];
    _prevLayer.frame = self.view.bounds;
    _prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:_prevLayer];
    
    [_session startRunning];
    [self.view bringSubviewToFront:_highlightView];
    [self.view bringSubviewToFront:_label];
    [self.view bringSubviewToFront:InfoLbl];
}
/*
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    CGRect highlightViewRect = CGRectZero;
    AVMetadataMachineReadableCodeObject *barCodeObject;
    NSString *detectionString = nil;
    
    NSArray *barCodeTypes = @[AVMetadataObjectTypeQRCode];
    for (AVMetadataObject *metadata in metadataObjects) {
        for (NSString *type in barCodeTypes)
        {
            if ([metadata.type isEqualToString:type])
            {
                barCodeObject = (AVMetadataMachineReadableCodeObject *)[_prevLayer transformedMetadataObjectForMetadataObject:(AVMetadataMachineReadableCodeObject *)metadata];
                highlightViewRect = barCodeObject.bounds;
                detectionString = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
                break;
            }
        }
    }
    
    
        if (detectionString != nil)
        {
            _label.text = detectionString;
            [[appDelegate.socket emitWithAck:@"qridemit" with:@[@{@"id": _label.text,@"from":appDelegate.strID}]] timingOutAfter:2 callback:^(NSArray* data) {
                        NSLog(@"%@",data);
                NSLog(@"detectionString:::::%@",_label.text);
                [_session stopRunning];
                UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                MainTabBarVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainTabBarVC"];
                [self.navigationController pushViewController:hme animated:YES];
                    }];
        }
        else
            _label.text = @"(none)";
    //}
//    [[appDelegate.socket emitWithAck:@"qridemit" with:@[@{@"id": _label.text,@"from":appDelegate.strID}]] timingOutAfter:2 callback:^(NSArray* data) {
//        NSLog(@"%@",data);
//        
//    }];
//    [appDelegate.socket emitWithAck:@"qridemit" with:@[@{@"id": _label.text,@"from":appDelegate.strID}]];
    
    _highlightView.frame = highlightViewRect;
}
*/
 - (IBAction)ClickonBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}
-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    NSString *detectionString = nil;
    CGRect highlightViewRect = CGRectZero;
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            highlightViewRect = metadataObj.bounds;
            detectionString = [metadataObj stringValue];
            if (detectionString != nil)
            {
                _label.text = detectionString;
                [[appDelegate.socket emitWithAck:@"qridemit" with:@[@{@"id": _label.text,@"from":appDelegate.strID}]] timingOutAfter:2 callback:^(NSArray* data)
                {
                    NSLog(@"%@",data);
                    
                    
                    
                    
                    
                    
                    
                    
                    
                }];
                NSLog(@"detectionString:::::%@",_label.text);

                [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
            }
//            else
//                _label.text = @"(none)";
            
            
        }
    }
    _highlightView.frame = highlightViewRect;

}
-(void)stopReading{
    [_session stopRunning];
    _session = nil;
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MainTabBarVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainTabBarVC"];
    [self.navigationController pushViewController:hme animated:YES];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
