//
//  SettingsVC.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 07/01/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsVC : UIViewController<UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>


@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnProfile;
- (IBAction)onProfile:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtStatus;
- (IBAction)onEdit:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIImageView *camImgView;
@property (weak, nonatomic) IBOutlet UIImageView *nameEditImg;
@property (weak, nonatomic) IBOutlet UIView *camBgView;
@property (weak, nonatomic) IBOutlet UIImageView *statusEditImg;

@end
