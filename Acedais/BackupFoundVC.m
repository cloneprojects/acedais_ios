//
//  BackupFoundVC.m
//  ZoeChat
//
//  Created by veena on 3/2/18.
//  Copyright © 2018 Pyramidions Solution. All rights reserved.
//

#import "BackupFoundVC.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "Constants.h"
#import "MainTabBarVC.h"
#import "RestoreVC.h"

@interface BackupFoundVC ()
@end

@implementation BackupFoundVC
{
    
    AppDelegate *appDelegate;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.minutesLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"messageTime"];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated
{
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        
        [UINavigationBar appearance].tintColor = color;
    }
    else
    {
        
        [UINavigationBar appearance].tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        
    }
}

- (IBAction)onRestoreBtnTapped:(id)sender
{
/*    NSArray *types = @[(NSString*)kUTTypeImage,(NSString*)kUTTypeSpreadsheet,(NSString*)kUTTypePresentation,(NSString*)kUTTypeDatabase,(NSString*)kUTTypeFolder,(NSString*)kUTTypeZipArchive,(NSString*)kUTTypeVideo];
    UIDocumentPickerViewController *docPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:types inMode:UIDocumentPickerModeImport];
    docPicker.delegate = self;
    [self presentViewController:docPicker animated:YES completion:nil];*/
    
        UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:@[@"public.item"]
                                                                                                                inMode:UIDocumentPickerModeImport];
    
    
    
        documentPicker.delegate = self;
        documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentViewController:documentPicker animated:YES completion:nil];
    
    
    
}

- (IBAction)onSkipBtnTapped:(id)sender
{
    [self performSegueWithIdentifier:@"SegueHome" sender:self];
}
- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url
{
    if (controller.documentPickerMode == UIDocumentPickerModeImport)
    {
        
        // Condition called when user download the file
        NSData *fileData = [NSData dataWithContentsOfURL:url];
        
        NSString *filePath = NSTemporaryDirectory();
        
        NSString *str = [NSString stringWithFormat:@"Documents.zip"];
        
        filePath = [filePath stringByAppendingString:str];

        
        [fileData writeToFile:filePath atomically:YES];


        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docDirectory = [paths objectAtIndex:0];
        
        
        BOOL status = [SSZipArchive unzipFileAtPath:filePath toDestination:docDirectory];

        NSLog(@"%@",fileData);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *alertMessage = [NSString stringWithFormat:@"Successfully downloaded file %@", [url lastPathComponent]];
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Zoechat"
                                                  message:alertMessage
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                 {
                                        [self performSegueWithIdentifier:@"BackupFound" sender:self];
                                 }];
            
            
            [alertController addAction:ok];
            
            
            NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
            UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
            if (color != nil) {
                alertController.view.tintColor=color;
            }
            else
            {
                
                alertController.view.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
                
            }

            
            [self presentViewController:alertController animated:YES completion:nil];
            
            
            
            
            
        });
    }else  if (controller.documentPickerMode == UIDocumentPickerModeExportToService)
    {
        // Called when user uploaded the file - Display success alert
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *alertMessage = [NSString stringWithFormat:@"Successfully uploaded file %@", [url lastPathComponent]];
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Zoechat"
                                                  message:alertMessage
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                 {
//                                     [self performSegueWithIdentifier:@"SegueHome" sender:self];
                                 }];

            
            [alertController addAction:ok];
            
            NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
            UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
            if (color != nil)
            {
                alertController.view.tintColor=color;
            }
            else
            {
                alertController.view.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
            }


            [self presentViewController:alertController animated:YES completion:nil];
            
        });
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"BackupFound"]) {
        RestoreVC *lvc = segue.destinationViewController;
        [lvc setHidesBottomBarWhenPushed:YES];
        lvc.navigationItem.hidesBackButton = YES;
    }
}

@end
