//
//  DataUsageVC.h
//  ZoeChat
//
//  Created by macmini on 06/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataUsageVC : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *dataTblView;

- (IBAction)clickonBackBtn:(id)sender;

@end
