//
//  ToShareLocationVC.m
//  ZoeChat
//
//  Created by Pyramidions Solution on 10/04/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "ToShareLocationVC.h"
#import "DBClass.h"
#import "AppDelegate.h"

@interface ToShareLocationVC ()
{
    NSString *strCurrLat, *strCurrLong, *strSelectedLat, *strSelectedLong;
    BOOL isAvailinChatsTable;
    DBClass *db_class;
    AppDelegate *appDelegate;
}
@end

@implementation ToShareLocationVC
@synthesize locationManager, strZoeChatId, strChatRoomType;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  
     appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
     db_class=[[DBClass alloc]init];
    
    UIBarButtonItem *barBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"BackButton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onBack)];
    [self.navigationItem setLeftBarButtonItem:barBtn ];
    _GoogleMapView.delegate=self;
    _GoogleMapView.myLocationEnabled = YES;
    _GoogleMapView.settings.myLocationButton=YES;
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:12.9081
                                                            longitude:77.6476
                                                                 zoom:8];
    [_GoogleMapView animateToCameraPosition:camera];
      [self start_to_detect_location:nil];
    
    _infoWindow.layer.cornerRadius=3;
    _infoWindow.clipsToBounds=YES;
    _lblTitle.text=@"Send this location";
    
    isAvailinChatsTable=NO;
    isAvailinChatsTable=[db_class doesChatExist:strZoeChatId];
    
    appDelegate.isFrmLocationPage=@"no";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)onBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)onSelectedLoc:(id)sender
{
    NSDate *now=[NSDate date];
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyyMMddHHmmssSSS"];
    NSString *localDateString = [dateFormatter1 stringFromDate:now];
    //  NSLog(@"%@",localDateString);
    
   
    NSString* strExt = @"PNG";
    
    NSString *UploadFileName=[NSString stringWithFormat:@"%@.%@",localDateString,strExt];
    
    NSString *strURL=[NSString stringWithFormat:@"http://maps.google.com/maps/api/staticmap?center=%@,%@&markers=size:large|color:red|%@,%@&zoom=12&size=400x400&sensor=false",strSelectedLat,strSelectedLong,strSelectedLat,strSelectedLong];
    
    NSURL *imageURL = [NSURL URLWithString:[strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            //image you want to upload
            // UIImage* imageToUpload = [UIImage imageWithData:imageData];
            UIImage *imageToUpload=[self compressImage:[UIImage imageWithData:imageData]];
            //convert uiimage to
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",UploadFileName]];
            [UIImagePNGRepresentation(imageToUpload) writeToFile:filePath atomically:YES];
            
            NSString *strMsgId = [[NSUUID UUID] UUIDString]; //id
            
            
            long long milliSeconds=[self convertDateToMilliseconds:now];
            NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
            
            BOOL success=NO;
            if (isAvailinChatsTable==NO)
            {
                success=[db_class insertChats:@"" chatRoomId:strZoeChatId chatRoomType:strChatRoomType sender:@"0" lastMessage:UploadFileName lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"location" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
                if (success==YES)
                {
                    isAvailinChatsTable=YES;
                }
            }
            else
            {
                success=[db_class updateLastMessage:strZoeChatId sender:@"0" lastMessage:UploadFileName lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"location" unReadCount:@"0"];
                NSLog(@"success");
            }
            
            success=[db_class insertChatMessages:strMsgId userId:strZoeChatId groupId:strZoeChatId chatRoomType:strChatRoomType content:UploadFileName contentType:@"location" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:_lblAddress.text isDownloaded:@"0" latitude:strSelectedLat longitude:strSelectedLong contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
            
            
            
            
            
            if ([strChatRoomType isEqualToString:@"2"])
            {
                NSMutableArray *arrPartis=[[NSMutableArray alloc]init];
                NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
                NSArray *arrdata=[db_class getParticipants:strZoeChatId];
                for (int i=0; i<arrdata.count; i++)
                {
                    NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
                    NSDictionary *dictCont=[arrdata objectAtIndex:i];
                    NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"userId"]];
                    [dictParti setObject:strPhone forKey:@"participantId"];
                    [arrPartis addObject:dictParti];
//                    NSString *BCstrMsgId = [[NSUUID UUID] UUIDString]; //id
//                    
//                    BOOL isBroadcastChat;
//                    isBroadcastChat=NO;
//                    isBroadcastChat=[db_class doesChatExist:strPhone];
//                    
//                    if (isBroadcastChat==NO)
//                    {
//                        
//                        success=[db_class insertChats:@"" chatRoomId:strPhone chatRoomType:@"0" sender:@"0" lastMessage:UploadFileName lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"location" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
//                       
//                       
//                        
//                        if (success==YES)
//                        {
//                            isBroadcastChat=YES;
//                        }
//                    }
//                    else
//                    {
//                        
//                         success=[db_class updateLastMessage:strPhone sender:@"0" lastMessage:UploadFileName lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"location" unReadCount:@"0"];
//                        
//                        
//                        NSLog(@"success");
//                        
//                        
//                    }
//                    
//                    success=[db_class insertChatMessages:strMsgId userId:strPhone groupId:strPhone chatRoomType:@"0" content:UploadFileName contentType:@"location" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:_lblAddress.text isDownloaded:@"0" latitude:strSelectedLat longitude:strSelectedLong contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                    
                }
                NSError *error;
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrPartis options:0 error:&error];
                NSString *jsonString;
                
                if (! jsonData)
                {
                    NSLog(@"Got an error: %@", error);
                }
                else
                {
                    jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                }
                jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
                jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\" withString:@""];

                
                NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                [dictParam setValue:appDelegate.strID forKey:@"from"];
                [dictParam setValue:jsonString forKey:@"participantId"];
                
                
                [appDelegate.socket emit:@"sendBroadcast" with:@[@{@"from": appDelegate.strID,@"participants": jsonString,@"content":strURL,@"contentType": @"location",@"messageId": strMsgId,@"time":strMilliSeconds, @"caption":_lblAddress.text, @"latitude":strSelectedLat, @"longitude":strSelectedLong ,@"chatRoomType":@"0",@"groupId":@""}]] ;
                
                
                for (int i=0; i<arrdata.count; i++)
                {
                    NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
                    NSDictionary *dictCont=[arrdata objectAtIndex:i];
                    NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"userId"]];
                    [dictParti setObject:strPhone forKey:@"participantId"];
                    [arrPartis addObject:dictParti];
                    NSString *BCstrMsgId = [[NSUUID UUID] UUIDString]; //id
                    
                    BOOL isBroadcastChat;
                    isBroadcastChat=NO;
                    isBroadcastChat=[db_class doesChatExist:strPhone];
                    
                    if (isBroadcastChat==NO)
                    {
                        
                        success=[db_class insertChats:@"" chatRoomId:strPhone chatRoomType:@"0" sender:@"0" lastMessage:UploadFileName lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"location" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
                        
                        
                        
                        if (success==YES)
                        {
                            isBroadcastChat=YES;
                        }
                    }
                    else
                    {
                        
                        success=[db_class updateLastMessage:strPhone sender:@"0" lastMessage:UploadFileName lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"location" unReadCount:@"0"];
                        
                        
                        NSLog(@"success");
                        
                        
                    }
                    
                    success=[db_class insertChatMessages:BCstrMsgId userId:strPhone groupId:strPhone chatRoomType:@"0" content:UploadFileName contentType:@"location" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:_lblAddress.text isDownloaded:@"0" latitude:strSelectedLat longitude:strSelectedLong contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                    
                }
            }
            
            else
                
            {
            
            [appDelegate.socket emit:@"sendMessage" with:@[@{@"from": appDelegate.strID,@"to": strZoeChatId,@"content": strURL,@"contentType":@"location",@"messageId": strMsgId,@"time":strMilliSeconds, @"caption":_lblAddress.text, @"latitude":strSelectedLat, @"longitude":strSelectedLong ,@"chatRoomType":strChatRoomType,@"groupId":@""}]];
            }
            appDelegate.isFrmLocationPage=@"yes";
            
             [self.navigationController popViewControllerAnimated:YES];
        });
    });
    
    
   
  
    
    
}
-(IBAction)onCurrLoc:(id)sender
{
    NSDate *now=[NSDate date];
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyyMMddHHmmssSSS"];
    NSString *localDateString = [dateFormatter1 stringFromDate:now];
    //  NSLog(@"%@",localDateString);
    
    
    NSString* strExt = @"PNG";
    
    NSString *UploadFileName=[NSString stringWithFormat:@"%@.%@",localDateString,strExt];
    
    NSString *strURL=[NSString stringWithFormat:@"http://maps.google.com/maps/api/staticmap?center=%@,%@&markers=size:large|color:red|%@,%@&zoom=12&size=400x400&sensor=false",strCurrLat,strCurrLong,strCurrLat,strCurrLong];
    
    NSURL *imageURL = [NSURL URLWithString:[strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
   
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            //image you want to upload
            // UIImage* imageToUpload = [UIImage imageWithData:imageData];
            //image you want to upload
            // UIImage* imageToUpload = [UIImage imageWithData:imageData];
            UIImage *imageToUpload=[self compressImage:[UIImage imageWithData:imageData]];
            //convert uiimage to
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",UploadFileName]];
            [UIImagePNGRepresentation(imageToUpload) writeToFile:filePath atomically:YES];
            
            NSString *strMsgId = [[NSUUID UUID] UUIDString]; //id
            
            
            long long milliSeconds=[self convertDateToMilliseconds:now];
            NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
            
            BOOL success=NO;
            if (isAvailinChatsTable==NO)
            {
                success=[db_class insertChats:@"" chatRoomId:strZoeChatId chatRoomType:strChatRoomType sender:@"0" lastMessage:UploadFileName lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"location" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
                if (success==YES)
                {
                    isAvailinChatsTable=YES;
                }
            }
            else
            {
                success=[db_class updateLastMessage:strZoeChatId sender:@"0" lastMessage:UploadFileName lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"location" unReadCount:@"0"];
                NSLog(@"success");
            }
            
            success=[db_class insertChatMessages:strMsgId userId:strZoeChatId groupId:strZoeChatId chatRoomType:strChatRoomType content:UploadFileName contentType:@"location" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:strCurrLat longitude:strCurrLong contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
            
            
            if ([strChatRoomType isEqualToString:@"2"])
            {
                NSMutableArray *arrPartis=[[NSMutableArray alloc]init];
                NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
                NSArray *arrdata=[db_class getParticipants:strZoeChatId];
                for (int i=0; i<arrdata.count; i++)
                {
                    NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
                    NSDictionary *dictCont=[arrdata objectAtIndex:i];
                    NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"userId"]];
                    [dictParti setObject:strPhone forKey:@"participantId"];
                    [arrPartis addObject:dictParti];
//                    NSString *BCstrMsgId = [[NSUUID UUID] UUIDString]; //id
//                    
//                    BOOL isBroadcastChat;
//                    isBroadcastChat=NO;
//                    isBroadcastChat=[db_class doesChatExist:strPhone];
//                    
//                    if (isBroadcastChat==NO)
//                    {
//                        
//                        success=[db_class insertChats:@"" chatRoomId:strPhone chatRoomType:@"0" sender:@"0" lastMessage:UploadFileName lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"location" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
//                        
//                        
//                        
//                        if (success==YES)
//                        {
//                            isBroadcastChat=YES;
//                        }
//                    }
//                    else
//                    {
//                        
//                        success=[db_class updateLastMessage:strPhone sender:@"0" lastMessage:UploadFileName lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"location" unReadCount:@"0"];
//                        
//                        
//                        NSLog(@"success");
//                        
//                        
//                    }
//                    
//                    success=[db_class insertChatMessages:BCstrMsgId userId:strPhone groupId:strPhone chatRoomType:@"0" content:UploadFileName contentType:@"location" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:_lblAddress.text isDownloaded:@"0" latitude:strSelectedLat longitude:strSelectedLong contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                    
                }
                NSError *error;
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrPartis options:0 error:&error];
                NSString *jsonString;
                
                if (! jsonData)
                {
                    NSLog(@"Got an error: %@", error);
                }
                else
                {
                    jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                }
                jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
                jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                
                NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                [dictParam setValue:appDelegate.strID forKey:@"from"];
                [dictParam setValue:jsonString forKey:@"participantId"];
                
                
                [appDelegate.socket emit:@"sendBroadcast" with:@[@{@"from": appDelegate.strID,@"participants": jsonString,@"content":strURL,@"contentType": @"location",@"messageId": strMsgId,@"time":strMilliSeconds, @"caption":_lblAddress.text, @"latitude":strSelectedLat, @"longitude":strSelectedLong ,@"chatRoomType":@"0",@"groupId":@""}]] ;
                
                
                for (int i=0; i<arrdata.count; i++)
                {
                    NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
                    NSDictionary *dictCont=[arrdata objectAtIndex:i];
                    NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"userId"]];
                    [dictParti setObject:strPhone forKey:@"participantId"];
                    [arrPartis addObject:dictParti];
                                        NSString *BCstrMsgId = [[NSUUID UUID] UUIDString]; //id
                    
                                        BOOL isBroadcastChat;
                                        isBroadcastChat=NO;
                                        isBroadcastChat=[db_class doesChatExist:strPhone];
                    
                                        if (isBroadcastChat==NO)
                                        {
                    
                                            success=[db_class insertChats:@"" chatRoomId:strPhone chatRoomType:@"0" sender:@"0" lastMessage:UploadFileName lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"location" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
                    
                    
                    
                                            if (success==YES)
                                            {
                                                isBroadcastChat=YES;
                                            }
                                        }
                                        else
                                        {
                    
                                            success=[db_class updateLastMessage:strPhone sender:@"0" lastMessage:UploadFileName lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"location" unReadCount:@"0"];
                    
                    
                                            NSLog(@"success");
                    
                    
                                        }
                                        
                                        success=[db_class insertChatMessages:BCstrMsgId userId:strPhone groupId:strPhone chatRoomType:@"0" content:UploadFileName contentType:@"location" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:_lblAddress.text isDownloaded:@"0" latitude:strSelectedLat longitude:strSelectedLong contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
                }
            }
            
            else
                
            {
            [appDelegate.socket emit:@"sendMessage" with:@[@{@"from": appDelegate.strID,@"to": strZoeChatId,@"content": strURL,@"contentType":@"location",@"messageId": strMsgId,@"time":strMilliSeconds, @"caption":@"", @"latitude":strCurrLat, @"longitude":strCurrLong ,@"chatRoomType":strChatRoomType,@"groupId":@""}]];
            }
            appDelegate.isFrmLocationPage=@"yes";
            
             [self.navigationController popViewControllerAnimated:YES];

        });
    });

    
    
    
}

- (UIImage *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth) {
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    return [UIImage imageWithData:imageData];
}

#pragma mark -
#pragma mark - GoogleMaps Delegate
- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{
    _infoWindow.hidden=YES;
}

-(void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
    _infoWindow.hidden=NO;
    _lblAddress.text=@"getting Address";
    double latitude = mapView.camera.target.latitude;
    double longitude = mapView.camera.target.longitude;
 //   NSLog(@"Lat: %f", latitude);
 //   NSLog(@"Long: %f", longitude);
    
    strSelectedLat=[NSString stringWithFormat:@"%f",latitude];
    strSelectedLong=[NSString stringWithFormat:@"%f",longitude];;
    
    CLLocationCoordinate2D addressCoordinates = CLLocationCoordinate2DMake(latitude,longitude);
    
    GMSGeocoder* coder = [[GMSGeocoder alloc] init];
    [coder reverseGeocodeCoordinate:addressCoordinates completionHandler:^(GMSReverseGeocodeResponse *results, NSError *error) {
        if (error) {
            // NSLog(@"Error %@", error.description);
        } else {
            GMSAddress* address = [results firstResult];
           
            NSArray *arr = [address valueForKey:@"lines"];
            NSString *strThorough=[address valueForKey:@"thoroughfare"];
            NSString *strCount = [NSString stringWithFormat:@"%lu",(unsigned long)[arr count]];
            NSString *strAddress=@"";
            if ([strCount isEqualToString:@"0"]) {
               // self.txtPlaceSearch.text = @"";
                strAddress=@"";
            }
            else if ([strCount isEqualToString:@"1"]) {
                NSString *str2 = [arr objectAtIndex:0];
               strAddress = str2;
            }
            else if ([strCount isEqualToString:@"2"]) {
                NSString *str2 = [arr objectAtIndex:0];
                NSString *str3 = [arr objectAtIndex:1];
               strAddress = [NSString stringWithFormat:@"%@,%@",str2,str3];
            }
            _lblAddress.text=strAddress;
            
            
        }
      
    }];

}

#pragma mark -
#pragma mark - Current Location

-(void)start_to_detect_location:(id)sender
{
    
    
    locationManager                   = [[CLLocationManager alloc] init];
    locationManager.delegate          =   self;
    locationManager.desiredAccuracy   =   kCLLocationAccuracyBest;
    
    if([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
    {
        [locationManager requestAlwaysAuthorization];
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if(status == kCLAuthorizationStatusRestricted || status == kCLAuthorizationStatusDenied)
    {
        NSLog(@"authentication failure");
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    
    
    [locationManager stopUpdatingLocation];
    self.locationManager = nil;
    NSLog(@"didFailWithError: %@", error);
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    strCurrLat=[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
    strCurrLong=[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];;
    [locationManager stopUpdatingLocation];
    self.locationManager = nil;
    
   
    
  //  NSLog(@"Lat: %@", strLat);
    // NSLog(@"Long: %@", strLong);
    
   
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[strCurrLat doubleValue]
                                                            longitude:[strCurrLong doubleValue]
                                                                 zoom:13];
    [_GoogleMapView animateToCameraPosition:camera];
    
    
}


-(long long)convertDateToMilliseconds:(NSDate *)date
{
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    long long milliseconds=timeInSeconds*1000;
    return milliseconds;
}
-(NSDate *)convertMillisecondsToDate:(long long)milliSeconds
{
    NSDate *dat = [NSDate dateWithTimeIntervalSince1970:(milliSeconds / 1000.0)];
    return dat;
}



@end
