//
//  LoadDocumentVC.m
//  ZoeChat
//
//  Created by macmini on 20/05/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "LoadDocumentVC.h"

@interface LoadDocumentVC ()
@property (strong, nonatomic) IBOutlet UIWebView *webview;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *actView;

@end

@implementation LoadDocumentVC
@synthesize getWebStr;
- (void)viewDidLoad {
    [super viewDidLoad];
    _actView.center = self.view.center;
       NSURL *targetURL = [NSURL URLWithString:getWebStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [_webview loadRequest:request];
}
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [_actView startAnimating];
    
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_actView stopAnimating];
    [_actView setHidden:YES];
    NSString *text = [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.innerText"];
    NSLog(@"%@",text);
    //[_actView hidesWhenStopped];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
