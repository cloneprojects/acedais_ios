//
//  DBClass.m
//  DownTown
//
//  Created by VinothV on 6/30/15.
//  Copyright (c) 2015 WePOP AR Research Lab. All rights reserved.
//
#import "DBClass.h"

@implementation DBClass
{
    
}


#pragma mark - Create tables

-(BOOL)createUserTable
{
    BOOL result     = NO;
    // NSString *query=@"DROP TABLE IF EXISTS CHEQUETEMPLATE";
    

    NSString *query=@"create table if not exists UserTable (mobile integer primary key, name text, registeredname text, image text, status text, showInContactsPage integer, zoeChatId text)";
    
    result = [ db executeUpdate:query ];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
}



-(BOOL)createChatsTable
{
    BOOL result     = NO;
    // NSString *query=@"DROP TABLE IF EXISTS CHEQUETEMPLATE";
    
    NSString *query=@"create table if not exists chatsTable (id integer primary key autoincrement, chatRoomId text, chatRoomType integer, sender integer, lastMessage text, lastMessageStatus text, lastMessageTime text, lastMessageType text, unreadCount integer,groupName text,groupImage text, sentBy text, isDelete text, isLock text, password text, isPin text)";
    
    result = [ db executeUpdate:query ];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
}



-(NSString*)getRowCount
{
    NSString *query=@"select * from chatMessagesTable";
    
    FMResultSet *rs = [db executeQuery:query];
    
    int i = 0;
    
    while([rs next])
    {
        i++;
    }
    [rs close];
    
    NSString *cnt = [NSString stringWithFormat:@"%d",i];
    
    return cnt;
}


-(BOOL)createChatMessagesTable
{
    BOOL result     = NO;
    // NSString *query=@"DROP TABLE IF EXISTS CHEQUETEMPLATE";
    
    NSString *query=@"create table if not exists chatMessagesTable (id text primary key, userId integer,groupId text, chatRoomType text, content text, contentType text, contentStatus text, sender integer, sentTime text, deliveredTime text, seenTime text, caption text, isDownloaded integer, latitude text, longitude text, contactName text, contactNumber text, checkStar text, showPreview text, linkTitle text, linkLogo text, linkDescription text)";
    
    result = [ db executeUpdate:query ];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
}

-(BOOL)createCallsTable
{
    BOOL result     = NO;
    
    NSString *query=@"create table if not exists callsTable (id integer primary key, zoeChatId text, callTime text, callType text, callLog text, callDuration text)";
    
    result = [ db executeUpdate:query ];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
}
-(BOOL)createGroupParticipantsTable
{
    
    BOOL result     = NO;
    
    NSString *query=@"create table if not exists GroupParticipants (id integer primary key, userId text, groupId text, joinedAt text, addedBy text, isAdmin text)";
    
    result = [ db executeUpdate:query ];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;

    
}

-(BOOL)createLinkPreviewTable
{
    
    BOOL result     = NO;
    
    NSString *query=@"create table if not exists PreviewLinks (title text, description text, logo text, url text primary key)";
    
    
    result = [ db executeUpdate:query ];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
    
    
}

#pragma mark -
#pragma mark - User Table (Insert Update Delete Get)

-(BOOL)insertUser:(NSString*)mobile name:(NSString*)name registeredName:(NSString*)registeredName image:(NSString*)image status:(NSString*)status showInContactsPage:(NSString*)showInContactsPage zoeChatId:(NSString*)zoeChatId;
{
    
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"insert into UserTable (mobile, name, registeredname, image, status, showInContactsPage, zoeChatId) values(\"%@\",\"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\")",mobile,name, registeredName, image, status, showInContactsPage,zoeChatId];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
    
}

-(BOOL)updateUser:(NSString*)mobile registeredName:(NSString*)registeredName image:(NSString*)image status:(NSString*)status showInContactsPage:(NSString*)showInContactsPage zoeChatId:(NSString*)zoeChatId;
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"UPDATE UserTable set registeredName = '%@', image = '%@', showInContactsPage = '%@', zoeChatId = '%@' , status = '%@'  WHERE mobile = %@", registeredName, image, showInContactsPage , zoeChatId, status, mobile];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
    
}

-(BOOL)updateUserName:(NSString*)mobile name:(NSString*)name;
{
    
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"UPDATE UserTable set name = '%@' WHERE mobile = %@",name, mobile];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
    
    
}


-(BOOL)updateUserImage:(NSString*)zoeChatId image:(NSString*)image;
{
 BOOL result = NO;
 NSString *query = [NSString stringWithFormat:@"UPDATE UserTable set image = '%@' WHERE mobile = %@",image, zoeChatId];
 result = [db executeUpdate:query];
 
 if ([db hadError])
 {
 NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
 return NO;
 }
 return result;
 
 
}

-(BOOL)updateUserStatus:(NSString*)zoeChatId status:(NSString*)status;
{
 BOOL result = NO;
 NSString *query = [NSString stringWithFormat:@"UPDATE UserTable set status = '%@' WHERE mobile = %@",status, zoeChatId];
 result = [db executeUpdate:query];
 
 if ([db hadError])
 {
 NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
 return NO;
 }
 return result;
 
}

-(BOOL)deleteUser:(NSString*)mobile;
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"DELETE FROM UserTable WHERE mobile = %@", mobile];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
    
    
}

-(NSDictionary *)getUserName:(NSString*)mobile;
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"select name from UserTable where mobile=\"%@\"",mobile];
        
        
        NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            //NSMutableArray *arrLocalList= [[NSMutableArray alloc] init];
             [resultDict setObject:[rs stringForColumn:@"name"] forKey:@"name"];
            //[arrAccNo addObject:arrLocalList];
        }
        [rs close];
        
     //   NSLog(@"%@",resultDict);
        
        return resultDict;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
    

}
-(NSDictionary *)getUserNameForNotification:(NSString*)mobile;
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"select name from UserTable where zoeChatId=\"%@\"",mobile];
        
        
        NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            //NSMutableArray *arrLocalList= [[NSMutableArray alloc] init];
            [resultDict setObject:[rs stringForColumn:@"name"] forKey:@"name"];
            //[arrAccNo addObject:arrLocalList];
        }
        [rs close];
        
        //   NSLog(@"%@",resultDict);
        
        return resultDict;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
    
    
}


-(NSDictionary *)getUserImage:(NSString*)mobile;
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"select image from UserTable where mobile=\"%@\"",mobile];
        
        
        NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            //NSMutableArray *arrLocalList= [[NSMutableArray alloc] init];
            [resultDict setObject:[rs stringForColumn:@"image"] forKey:@"image"];
            //[arrAccNo addObject:arrLocalList];
        }
        [rs close];
        
       // NSLog(@"%@",resultDict);
        
        return resultDict;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;

    
}

-(NSDictionary *)getUserInfo:(NSString*)mobile;
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@" select * from UserTable where zoechatid = %@",mobile];
        
        NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            [resultDict setObject:[rs stringForColumn:@"name"] forKey:@"name"];
            [resultDict setObject:[rs stringForColumn:@"registeredname"] forKey:@"registeredname"];
            [resultDict setObject:[rs stringForColumn:@"image"] forKey:@"image"];
            [resultDict setObject:[rs stringForColumn:@"status"] forKey:@"status"];
            [resultDict setObject:[rs stringForColumn:@"showincontactspage"] forKey:@"showincontactspage"];
            [resultDict setObject:[rs stringForColumn:@"zoechatid"] forKey:@"zoechatid"];            
            
        }
        [rs close];
        
      //  NSLog(@"%@",resultDict);
        
        return resultDict;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;

    
}

-(NSArray *)getUsers;
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"select * from UserTable"];
         NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
             NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
            [resultDict setObject:[rs stringForColumn:@"mobile"] forKey:@"mobile"];
            [resultDict setObject:[rs stringForColumn:@"name"] forKey:@"name"];
            [resultDict setObject:[rs stringForColumn:@"registeredname"] forKey:@"registeredname"];
            [resultDict setObject:[rs stringForColumn:@"image"] forKey:@"image"];
            [resultDict setObject:[rs stringForColumn:@"status"] forKey:@"status"];
            [resultDict setObject:[rs stringForColumn:@"showincontactspage"] forKey:@"showincontactspage"];
            [resultDict setObject:[rs stringForColumn:@"zoechatid"] forKey:@"zoechatid"];
             [resultArray addObject:resultDict];
        }
        [rs close];
        
     //   NSLog(@"%@",resultArray);
        
        return resultArray;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
    
}


-(BOOL)doesUserExist:(NSString*)mobile;
{
 BOOL result = NO;
 @try
 {
 NSString *query = [NSString stringWithFormat:@"select mobile from UserTable where mobile=\"%@\"",mobile];
 FMResultSet         *rs              = [db executeQuery:query];
 
 while([rs next])
 {
     result =YES;
 }
 [rs close];
 
 return result;
 }
 @catch (NSException *exception)
 {
 NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
 }
 return 0;

 
}


#pragma mark -
#pragma mark - Chats Table Insert (Update Delete Get)

-(BOOL)insertChats:(NSString*)id chatRoomId:(NSString*)chatRoomId chatRoomType:(NSString*)chatRoomType sender:(NSString*)sender lastMessage:(NSString*)lastMessage lastMessageStatus:(NSString*)lastMessageStatus lastMessagesTime:(NSString*)lastMessagesTime lastMessagesType:(NSString*)lastMessagesType unReadCount:(NSString*)unReadCount groupName:(NSString*)groupName groupImage:(NSString*)groupImage sentBy:(NSString*)sentBy isDelete:(NSString*)isDelete isLock:(NSString*)isLock password:(NSString*)password;


{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"insert into chatsTable (chatRoomId, chatRoomType, sender, lastMessage, lastMessageStatus, lastMessageTime, lastMessageType, unreadCount, groupName,groupImage, sentBy,isDelete, isLock, password, isPin) values(\"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")", chatRoomId , chatRoomType , sender , lastMessage, lastMessageStatus,lastMessagesTime, lastMessagesType, unReadCount, groupName, groupImage, sentBy, isDelete, isLock, password,@"0"];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;

    
}

-(BOOL)updateLastMessage:(NSString*)id sender:(NSString*)sender lastMessage:(NSString*)lastMessage lastMessageStatus:(NSString*)lastMessageStatus lastMessageTime:(NSString*)lastMessageTime lastMessagesType:(NSString*)lastMessagesType unReadCount:(NSString*)unReadCount;
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"UPDATE chatsTable set sender = '%@', lastMessage = '%@', lastMessageStatus = '%@', lastMessageTime = '%@' ,  lastMessageType = '%@' ,  unreadCount = '%@'  WHERE chatRoomId = '%@'", sender , lastMessage, lastMessageStatus, lastMessageTime, lastMessagesType, unReadCount, id];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;

}
//update deletechat
-(BOOL)updatedeleteChatTable:(NSString*)chatRoomId isDelete:(NSString*)isDelete
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"UPDATE chatsTable set isDelete = '%@' WHERE chatRoomID = '%@'",isDelete, chatRoomId];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;

}


//update lock
-(BOOL)updateLockChatTable:(NSString *)chatRoomId isLock:(NSString *)isLock
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"UPDATE chatsTable set isLock = '%@' WHERE chatRoomID = '%@'",isLock, chatRoomId];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
}


-(BOOL)updatePinChatTable:(NSString *)chatRoomId isPin:(NSString *)isPin
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"UPDATE chatsTable set isPin = '%@' WHERE chatRoomID = '%@'",isPin, chatRoomId];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
}



-(BOOL)updatePasswordChatTable:(NSString*)chatRoomId  password:(NSString*)password
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"UPDATE chatsTable set password = '%@' WHERE chatRoomID = '%@'",password, chatRoomId];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;

}


-(BOOL)updateLastMsgStatus:(NSString*)id lastMsgStatus:(NSString*)lastMsgStatus;
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"UPDATE chatsTable set lastMessageStatus = '%@' WHERE chatRoomId = '%@'",lastMsgStatus, id];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
    
}
-(BOOL)updateGroupName:(NSString*)id groupName:(NSString*)groupName
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"UPDATE chatsTable set groupName = '%@' WHERE chatRoomId = '%@'",groupName, id];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
    
}
-(BOOL)updateGroupImage:(NSString*)id groupImage:(NSString*)groupImage
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"UPDATE chatsTable set groupImage = '%@' WHERE chatRoomId = '%@'",groupImage, id];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
}

-(NSMutableArray *)getChats;
{
    @try
    {
        //select * from chatsTable where chatRoomId=\"%@\"",id
        NSString *query = [NSString stringWithFormat:@"select * from chatsTable WHERE isDelete = \"%@\"", @"0"];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
            
            NSString *strMilli=[NSString stringWithFormat:@"%@",[rs stringForColumn:@"lastMessageTime"]];
            long long milli=[strMilli longLongValue];
            NSDate *dateTime=[self convertMillisecondsToDate:milli];
            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
            [dateFormatter1 setDateFormat:@"yyyyMMddHHmmssSSS"];
            NSString *DateString = [dateFormatter1 stringFromDate:dateTime];

            
            [resultDict setObject:[rs stringForColumn:@"chatRoomId"] forKey:@"chatRoomId"];
            [resultDict setObject:[rs stringForColumn:@"chatRoomType"] forKey:@"chatRoomType"];
            [resultDict setObject:[rs stringForColumn:@"sender"] forKey:@"sender"];
            [resultDict setObject:[rs stringForColumn:@"lastMessage"] forKey:@"lastMessage"];
            [resultDict setObject:[rs stringForColumn:@"lastMessageStatus"] forKey:@"lastMessageStatus"];
            [resultDict setObject:[rs stringForColumn:@"lastMessageTime"] forKey:@"lastMessageTime"];
            [resultDict setObject:[rs stringForColumn:@"lastMessageType"] forKey:@"lastMessageType"];
            [resultDict setObject:[rs stringForColumn:@"unreadCount"] forKey:@"unreadCount"];
            [resultDict setObject:[rs stringForColumn:@"groupName"] forKey:@"groupName"];
            [resultDict setObject:[rs stringForColumn:@"groupImage"] forKey:@"groupImage"];
            [resultDict setObject:[rs stringForColumn:@"sentBy"] forKey:@"sentBy"];
            [resultDict setObject:[rs stringForColumn:@"isDelete"] forKey:@"isDelete"];
            [resultDict setObject:[rs stringForColumn:@"isLock"] forKey:@"isLock"];

            [resultDict setObject:[rs stringForColumn:@"password"] forKey:@"password"];
            [resultDict setObject:[rs stringForColumn:@"isPin"] forKey:@"isPin"];

            
            [resultDict setObject:DateString forKey:@"dateTime"];
            [resultArray addObject:resultDict];
        }
        [rs close];
        
        NSLog(@"%@",resultArray);
        
        return resultArray;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
    
    
    }
-(NSDictionary *) getOneChat:(NSString*)chatRoomId

{
    @try
    {
        //select * from chatsTable where chatRoomId=\"%@\"",id
        NSString *query = [NSString stringWithFormat:@"select * from chatsTable WHERE chatRoomId = '%@'", chatRoomId];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            
            NSString *strMilli=[NSString stringWithFormat:@"%@",[rs stringForColumn:@"lastMessageTime"]];
            long long milli=[strMilli longLongValue];
            NSDate *dateTime=[self convertMillisecondsToDate:milli];
            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
            [dateFormatter1 setDateFormat:@"yyyyMMddHHmmssSSS"];
            NSString *DateString = [dateFormatter1 stringFromDate:dateTime];
            
            
            [resultDict setObject:[rs stringForColumn:@"chatRoomId"] forKey:@"chatRoomId"];
            [resultDict setObject:[rs stringForColumn:@"chatRoomType"] forKey:@"chatRoomType"];
            [resultDict setObject:[rs stringForColumn:@"sender"] forKey:@"sender"];
            [resultDict setObject:[rs stringForColumn:@"lastMessage"] forKey:@"lastMessage"];
            [resultDict setObject:[rs stringForColumn:@"lastMessageStatus"] forKey:@"lastMessageStatus"];
            [resultDict setObject:[rs stringForColumn:@"lastMessageTime"] forKey:@"lastMessageTime"];
            [resultDict setObject:[rs stringForColumn:@"lastMessageType"] forKey:@"lastMessageType"];
            [resultDict setObject:[rs stringForColumn:@"unreadCount"] forKey:@"unreadCount"];
            [resultDict setObject:[rs stringForColumn:@"groupName"] forKey:@"groupName"];
            [resultDict setObject:[rs stringForColumn:@"groupImage"] forKey:@"groupImage"];
            [resultDict setObject:[rs stringForColumn:@"sentBy"] forKey:@"sentBy"];
            [resultDict setObject:[rs stringForColumn:@"isDelete"] forKey:@"isDelete"];
            [resultDict setObject:[rs stringForColumn:@"isLock"] forKey:@"isLock"];
            
            [resultDict setObject:[rs stringForColumn:@"password"] forKey:@"password"];
            
            [resultDict setObject:DateString forKey:@"dateTime"];
            //[resultArray addObject:resultDict];
        }
        [rs close];
        
        //    NSLog(@"%@",resultArray);
        
        return resultDict;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
    
    
}

-(BOOL)doesChatExist:(NSString*)chatRoomId;
{
    BOOL result = NO;
    @try
    {
        NSString *query = [NSString stringWithFormat:@"select chatRoomId from chatsTable where chatRoomId=\"%@\"",chatRoomId];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            result =YES;
        }
        [rs close];
        
        return result;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;

}

-(NSDictionary *)getSingleChat:(NSString*)id;
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"select * from chatsTable where chatRoomId=\"%@\"",id];
        
        
        NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            [resultDict setObject:[rs stringForColumn:@"lastMessage"] forKey:@"lastMessage"];
            [resultDict setObject:[rs stringForColumn:@"lastMessageStatus"] forKey:@"lastMessageStatus"];
            [resultDict setObject:[rs stringForColumn:@"lastMessageTime"] forKey:@"lastMessageTime"];
            [resultDict setObject:[rs stringForColumn:@"unreadCount"] forKey:@"unreadCount"];
            [resultDict setObject:[rs stringForColumn:@"groupName"] forKey:@"groupName"];
             [resultDict setObject:[rs stringForColumn:@"chatRoomType"] forKey:@"chatRoomType"];
            [resultDict setObject:[rs stringForColumn:@"sentBy"] forKey:@"sentBy"];
        }
//        {
//            NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
//            
//            NSString *strMilli=[NSString stringWithFormat:@"%@",[rs stringForColumn:@"lastMessageTime"]];
//            long long milli=[strMilli longLongValue];
//            NSDate *dateTime=[self convertMillisecondsToDate:milli];
//            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
//            [dateFormatter1 setDateFormat:@"yyyyMMddHHmmssSSS"];
//            NSString *DateString = [dateFormatter1 stringFromDate:dateTime];
//            
//            
//            [resultDict setObject:[rs stringForColumn:@"chatRoomId"] forKey:@"chatRoomId"];
//            [resultDict setObject:[rs stringForColumn:@"chatRoomType"] forKey:@"chatRoomType"];
//            [resultDict setObject:[rs stringForColumn:@"sender"] forKey:@"sender"];
//            [resultDict setObject:[rs stringForColumn:@"lastMessage"] forKey:@"lastMessage"];
//            [resultDict setObject:[rs stringForColumn:@"lastMessageStatus"] forKey:@"lastMessageStatus"];
//            [resultDict setObject:[rs stringForColumn:@"lastMessageTime"] forKey:@"lastMessageTime"];
//            [resultDict setObject:[rs stringForColumn:@"lastMessageType"] forKey:@"lastMessageType"];
//            [resultDict setObject:[rs stringForColumn:@"unreadCount"] forKey:@"unreadCount"];
//            [resultDict setObject:[rs stringForColumn:@"groupName"] forKey:@"groupName"];
//            [resultDict setObject:[rs stringForColumn:@"groupImage"] forKey:@"groupImage"];
//            [resultDict setObject:[rs stringForColumn:@"sentBy"] forKey:@"sentBy"];
//            [resultDict setObject:[rs stringForColumn:@"isDelete"] forKey:@"isDelete"];
//            [resultDict setObject:[rs stringForColumn:@"isLock"] forKey:@"isLock"];
//            
//            [resultDict setObject:[rs stringForColumn:@"password"] forKey:@"password"];
//            
//            [resultDict setObject:DateString forKey:@"dateTime"];
//           // [resultArray addObject:resultDict];
//        }
        [rs close];
        
      //  NSLog(@"%@",resultDict);
        
        return resultDict;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
    
    
}
-(BOOL)updateUnreadCount:(NSString*)id unreadCount:(NSString*)unreadCount;
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"UPDATE chatsTable set unreadCount = '%@' WHERE chatRoomId = '%@'",unreadCount, id];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
}
-(int) getTotalUnreadCount
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"select unreadCount from chatsTable"];
        int nTotalUnread=0;
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            int curr=[[rs stringForColumn:@"unreadCount"]intValue];
            nTotalUnread=nTotalUnread+curr;
        }
        [rs close];
        
        return nTotalUnread;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
}

-(BOOL)deleteChat:(NSString*)chatRoomId
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"DELETE FROM chatsTable WHERE chatRoomId = '%@'", chatRoomId];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
    
    
}
//delete Call
-(BOOL)deleteCall:(NSString*)id
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"DELETE FROM callsTable WHERE id = '%@'", id];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
    
    
}
-(NSMutableArray *)getcall:(NSString*)zoeChatId;
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"select * from callsTable where zoeChatId=\"%@\"",zoeChatId];
        
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            NSMutableDictionary *resultDict= [[NSMutableDictionary alloc] init];
            [resultDict setObject:[rs stringForColumn:@"zoeChatId"] forKey:@"zoeChatId"];
            [resultDict setObject:[rs stringForColumn:@"callLog"] forKey:@"callLog"];
            [resultDict setObject:[rs stringForColumn:@"callDuration"] forKey:@"callDuration"];
            [resultDict setObject:[rs stringForColumn:@"callTime"] forKey:@"callTime"];
            [resultDict setObject:[rs stringForColumn:@"callType"] forKey:@"callType"];
            
            
            
            
            
            
            [resultArray addObject:resultDict];
        }
        [rs close];
        
        
        return resultArray;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
    
    
}


#pragma mark -
#pragma mark - Chat Messages Table Insert (Update Delete Get)
-(BOOL)insertChatMessages:(NSString*)id userId:(NSString*)userId groupId:(NSString*)groupId chatRoomType:(NSString*)chatRoomType content:(NSString*)content contentType:(NSString*)contentType contentStatus:(NSString*)contentStatus sender:(NSString*)sender sentTime:(NSString*)sentTime deliveredTime:(NSString*)deliveredTime seenTime:(NSString*)seenTime caption:(NSString*)caption isDownloaded:(NSString*)isDownloaded latitude:(NSString*)latitude longitude:(NSString*)longitude contactName:(NSString*)contactName contactNumber:(NSString*)contactNumber checkStar:(NSString*)checkStar showPreview:(NSString *)showPreview linkTitle:(NSString *)linkTitle linkLogo:(NSString *)linkLogo linkDescription:(NSString *)linkDescription
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"insert into chatMessagesTable (id,userId, groupId, chatRoomType, content, contentType, contentStatus, sender, sentTime, deliveredTime, seenTime, caption, isDownloaded, latitude, longitude, contactName, contactNumber, checkStar,showPreview,linkTitle,linkLogo,linkDescription) values(\"%@\",\"%@\",\"%@\",\"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",id ,userId, groupId ,chatRoomType, content, contentType, contentStatus, sender ,sentTime, deliveredTime, seenTime, caption, isDownloaded,latitude, longitude, contactName, contactNumber,checkStar,showPreview,linkTitle,linkLogo,linkDescription];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
}




-(BOOL)updateDeliveredTime:(NSString*)id deliveredTime:(NSString*)deliveredTime;
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"UPDATE chatMessagesTable set deliveredTime = '%@' WHERE id = '%@'",deliveredTime, id];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;

    
}

-(BOOL)updateSeenTime:(NSString*)id seenTime:(NSString*)seenTime;
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"UPDATE chatMessagesTable set seenTime = '%@' WHERE id = '%@'",seenTime, id];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;

    
}

-(BOOL)updateContentStatus:(NSString*)id contentStatus:(NSString*)contentStatus;
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"UPDATE chatMessagesTable set contentStatus = '%@' WHERE id = '%@'",contentStatus, id];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;

}

-(BOOL)updateIsDownloaded:(NSString*)id isDownloaded:(NSString*)isDownloaded;
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"UPDATE chatMessagesTable set isDownloaded = '%@' WHERE id = '%@'",isDownloaded, id];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;

    
   
}

-(BOOL)updateStarredMessage:(NSString*)userId checkStar:(NSString*)checkStar
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"UPDATE chatMessagesTable set checkStar = '%@' WHERE id = '%@'",checkStar, userId];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
    
    
    
}
-(BOOL)updateGroupStarredMessage:(NSString*)groupId checkStar:(NSString*)checkStar
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"UPDATE chatMessagesTable set checkStar = '%@' WHERE id = '%@'",checkStar, groupId];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
    
    
    
}


-(NSMutableArray *)getChatMessages
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"select * from chatMessagesTable"];

        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
            [resultDict setObject:[rs stringForColumn:@"id"] forKey:@"id"];
            [resultDict setObject:[rs stringForColumn:@"userId"] forKey:@"userId"];
            [resultDict setObject:[rs stringForColumn:@"content"] forKey:@"content"];
            [resultDict setObject:[rs stringForColumn:@"contentType"] forKey:@"contentType"];
            [resultDict setObject:[rs stringForColumn:@"contentStatus"] forKey:@"contentStatus"];
            [resultDict setObject:[rs stringForColumn:@"sender"] forKey:@"sender"];
            [resultDict setObject:[rs stringForColumn:@"sentTime"] forKey:@"sentTime"];
            [resultDict setObject:[rs stringForColumn:@"deliveredTime"] forKey:@"deliveredTime"];
            [resultDict setObject:[rs stringForColumn:@"seenTime"] forKey:@"seenTime"];
            [resultDict setObject:[rs stringForColumn:@"caption"] forKey:@"caption"];
            [resultDict setObject:[rs stringForColumn:@"isDownloaded"] forKey:@"isDownloaded"];
            [resultDict setObject:[rs stringForColumn:@"latitude"] forKey:@"latitude"];
            [resultDict setObject:[rs stringForColumn:@"longitude"] forKey:@"longitude"];
            [resultDict setObject:[rs stringForColumn:@"contactName"] forKey:@"contactName"];
            [resultDict setObject:[rs stringForColumn:@"contactNumber"] forKey:@"contactNumber"];
            [resultDict setObject:[rs stringForColumn:@"chatRoomType"] forKey:@"chatRoomType"];
             [resultDict setObject:[rs stringForColumn:@"groupId"] forKey:@"groupId"];
            [resultDict setObject:[rs stringForColumn:@"checkStar"] forKey:@"checkStar"];
            [resultDict setObject:[rs stringForColumn:@"showPreview"] forKey:@"showPreview"];
            [resultDict setObject:[rs stringForColumn:@"linkLogo"] forKey:@"linkLogo"];
            [resultDict setObject:[rs stringForColumn:@"linkTitle"] forKey:@"linkTitle"];
            [resultDict setObject:[rs stringForColumn:@"linkDescription"] forKey:@"linkDescription"];


            [resultArray addObject:resultDict];
        }
        [rs close];
        
      //  NSLog(@"%@",resultArray);
        
        return resultArray;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
}
//pagination
-(NSMutableArray *)getChatMessages:(NSString*)userId limitVal:(NSString*)limitVal
{
    @try
    {
        
        NSString *query = [NSString stringWithFormat:@"select * from chatMessagesTable WHERE userId='%@' ORDER BY sentTime DESC LIMIT '%@', 10",userId,limitVal];
       
//         NSString *query = [NSString stringWithFormat:@"select * from chatMessagesTable WHERE userId='%@' ORDER BY sentTime DESC LIMIT '%@'",userId,limitVal];
        
        
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
            [resultDict setObject:[rs stringForColumn:@"id"] forKey:@"id"];
            [resultDict setObject:[rs stringForColumn:@"userId"] forKey:@"userId"];
            [resultDict setObject:[rs stringForColumn:@"content"] forKey:@"content"];
            [resultDict setObject:[rs stringForColumn:@"contentType"] forKey:@"contentType"];
            [resultDict setObject:[rs stringForColumn:@"contentStatus"] forKey:@"contentStatus"];
            [resultDict setObject:[rs stringForColumn:@"sender"] forKey:@"sender"];
            [resultDict setObject:[rs stringForColumn:@"sentTime"] forKey:@"sentTime"];
            [resultDict setObject:[rs stringForColumn:@"deliveredTime"] forKey:@"deliveredTime"];
            [resultDict setObject:[rs stringForColumn:@"seenTime"] forKey:@"seenTime"];
            [resultDict setObject:[rs stringForColumn:@"caption"] forKey:@"caption"];
            [resultDict setObject:[rs stringForColumn:@"isDownloaded"] forKey:@"isDownloaded"];
            [resultDict setObject:[rs stringForColumn:@"latitude"] forKey:@"latitude"];
            [resultDict setObject:[rs stringForColumn:@"longitude"] forKey:@"longitude"];
            [resultDict setObject:[rs stringForColumn:@"contactName"] forKey:@"contactName"];
            [resultDict setObject:[rs stringForColumn:@"contactNumber"] forKey:@"contactNumber"];
            [resultDict setObject:[rs stringForColumn:@"chatRoomType"] forKey:@"chatRoomType"];
            [resultDict setObject:[rs stringForColumn:@"groupId"] forKey:@"groupId"];
            
            [resultDict setObject:[rs stringForColumn:@"checkStar"] forKey:@"checkStar"];
            
            [resultArray addObject:resultDict];
        }
        [rs close];
        
        //  NSLog(@"%@",resultArray);
        
        return resultArray;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
}
//pagination Group
-(NSMutableArray *)getGroupMessages:(NSString*)groupId limitVal:(NSString*)limitVal
{
    @try
    {
        
        NSString *query = [NSString stringWithFormat:@"select * from chatMessagesTable WHERE groupId='%@' ORDER BY sentTime DESC LIMIT '%@', 10",groupId,limitVal];
        
        //         NSString *query = [NSString stringWithFormat:@"select * from chatMessagesTable WHERE userId='%@' ORDER BY sentTime DESC LIMIT '%@'",userId,limitVal];
        
        
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
            [resultDict setObject:[rs stringForColumn:@"id"] forKey:@"id"];
            [resultDict setObject:[rs stringForColumn:@"userId"] forKey:@"userId"];
            [resultDict setObject:[rs stringForColumn:@"content"] forKey:@"content"];
            [resultDict setObject:[rs stringForColumn:@"contentType"] forKey:@"contentType"];
            [resultDict setObject:[rs stringForColumn:@"contentStatus"] forKey:@"contentStatus"];
            [resultDict setObject:[rs stringForColumn:@"sender"] forKey:@"sender"];
            [resultDict setObject:[rs stringForColumn:@"sentTime"] forKey:@"sentTime"];
            [resultDict setObject:[rs stringForColumn:@"deliveredTime"] forKey:@"deliveredTime"];
            [resultDict setObject:[rs stringForColumn:@"seenTime"] forKey:@"seenTime"];
            [resultDict setObject:[rs stringForColumn:@"caption"] forKey:@"caption"];
            [resultDict setObject:[rs stringForColumn:@"isDownloaded"] forKey:@"isDownloaded"];
            [resultDict setObject:[rs stringForColumn:@"latitude"] forKey:@"latitude"];
            [resultDict setObject:[rs stringForColumn:@"longitude"] forKey:@"longitude"];
            [resultDict setObject:[rs stringForColumn:@"contactName"] forKey:@"contactName"];
            [resultDict setObject:[rs stringForColumn:@"contactNumber"] forKey:@"contactNumber"];
            [resultDict setObject:[rs stringForColumn:@"chatRoomType"] forKey:@"chatRoomType"];
            [resultDict setObject:[rs stringForColumn:@"groupId"] forKey:@"groupId"];
            
            [resultDict setObject:[rs stringForColumn:@"checkStar"] forKey:@"checkStar"];
            
            [resultArray addObject:resultDict];
        }
        [rs close];
        
        //  NSLog(@"%@",resultArray);
        
        return resultArray;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
}

-(NSMutableArray *) getSendingChatMessages;
{
    @try
    {
        NSString *query1 = [NSString stringWithFormat:@"select * from chatMessagesTable WHERE contentStatus = 'sending'"];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        FMResultSet         *rs1              = [db executeQuery:query1];
        
        while([rs1 next])
        {
            NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
            [resultDict setObject:[rs1 stringForColumn:@"id"] forKey:@"id"];
            [resultDict setObject:[rs1 stringForColumn:@"userId"] forKey:@"userId"];
            [resultDict setObject:[rs1 stringForColumn:@"content"] forKey:@"content"];
            [resultDict setObject:[rs1 stringForColumn:@"contentType"] forKey:@"contentType"];
            [resultDict setObject:[rs1 stringForColumn:@"contentStatus"] forKey:@"contentStatus"];
            [resultDict setObject:[rs1 stringForColumn:@"sender"] forKey:@"sender"];
            [resultDict setObject:[rs1 stringForColumn:@"sentTime"] forKey:@"sentTime"];
            [resultDict setObject:[rs1 stringForColumn:@"deliveredTime"] forKey:@"deliveredTime"];
            [resultDict setObject:[rs1 stringForColumn:@"seenTime"] forKey:@"seenTime"];
            [resultDict setObject:[rs1 stringForColumn:@"caption"] forKey:@"caption"];
            [resultDict setObject:[rs1 stringForColumn:@"isDownloaded"] forKey:@"isDownloaded"];
            [resultDict setObject:[rs1 stringForColumn:@"chatRoomType"] forKey:@"chatRoomType"];
            [resultDict setObject:[rs1 stringForColumn:@"groupId"] forKey:@"groupId"];
            [resultArray addObject:resultDict];
        }
        [rs1 close];
        
        NSString *query = [NSString stringWithFormat:@"select * from chatMessagesTable WHERE contentStatus = 'uploaded'"];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
            [resultDict setObject:[rs stringForColumn:@"id"] forKey:@"id"];
            [resultDict setObject:[rs stringForColumn:@"userId"] forKey:@"userId"];
            [resultDict setObject:[rs stringForColumn:@"content"] forKey:@"content"];
            [resultDict setObject:[rs stringForColumn:@"contentType"] forKey:@"contentType"];
            [resultDict setObject:[rs stringForColumn:@"contentStatus"] forKey:@"contentStatus"];
            [resultDict setObject:[rs stringForColumn:@"sender"] forKey:@"sender"];
            [resultDict setObject:[rs stringForColumn:@"sentTime"] forKey:@"sentTime"];
            [resultDict setObject:[rs stringForColumn:@"deliveredTime"] forKey:@"deliveredTime"];
            [resultDict setObject:[rs stringForColumn:@"seenTime"] forKey:@"seenTime"];
            [resultDict setObject:[rs stringForColumn:@"caption"] forKey:@"caption"];
            [resultDict setObject:[rs stringForColumn:@"isDownloaded"] forKey:@"isDownloaded"];
            [resultDict setObject:[rs stringForColumn:@"chatRoomType"] forKey:@"chatRoomType"];
            [resultDict setObject:[rs stringForColumn:@"groupId"] forKey:@"groupId"];
            [resultArray addObject:resultDict];
        }
        [rs close];

        
        return resultArray;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
}

-(NSDictionary *)getChatMessage:(NSString*)id;
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"select * from chatMessagesTable where id=\"%@\"",id];
        
        
        NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            //NSMutableArray *arrLocalList= [[NSMutableArray alloc] init];
            [resultDict setObject:[rs stringForColumn:@"content"] forKey:@"content"];
            [resultDict setObject:[rs stringForColumn:@"userId"] forKey:@"userId"];
            [resultDict setObject:[rs stringForColumn:@"chatRoomType"] forKey:@"chatRoomType"];
            [resultDict setObject:[rs stringForColumn:@"groupId"] forKey:@"groupId"];
            //[arrAccNo addObject:arrLocalList];
        }
        [rs close];
        
      //  NSLog(@"%@",resultDict);
        
        return resultDict;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
    
    
}

-(NSArray *)getOneToOneChatMessages:(NSString*)userId;
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"select * from chatMessagesTable where userId=\"%@\"",userId];
        
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            NSMutableDictionary *resultDict= [[NSMutableDictionary alloc] init];
            [resultDict setObject:[rs stringForColumn:@"content"] forKey:@"content"];
            [resultDict setObject:[rs stringForColumn:@"contentType"] forKey:@"contentType"];
            [resultDict setObject:[rs stringForColumn:@"userId"] forKey:@"userId"];
            [resultDict setObject:[rs stringForColumn:@"caption"] forKey:@"caption"];
            [resultDict setObject:[rs stringForColumn:@"chatRoomType"] forKey:@"chatRoomType"];
            [resultDict setObject:[rs stringForColumn:@"groupId"] forKey:@"groupId"];
            [resultDict setObject:[rs stringForColumn:@"sender"] forKey:@"sender"];
            [resultDict setObject:[rs stringForColumn:@"checkStar"] forKey:@"checkStar"];

            [resultDict setObject:[rs stringForColumn:@"contentStatus"] forKey:@"contentStatus"];
            [resultDict setObject:[rs stringForColumn:@"seenTime"] forKey:@"seenTime"];
            [resultDict setObject:[rs stringForColumn:@"deliveredTime"] forKey:@"deliveredTime"];

            [resultDict setObject:[rs stringForColumn:@"sentTime"] forKey:@"sentTime"];




            [resultArray addObject:resultDict];
        }
        [rs close];
        
       
        return resultArray;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
    
    
}
-(NSArray *)getOneToOneChatMessagesForMedia:(NSString*)userId;
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"select * from chatMessagesTable where userId=\"%@\" and contentType=\"%@\" and chatRoomType=\"%@\"",userId, @"image", @"0"];
        
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            NSMutableDictionary *resultDict= [[NSMutableDictionary alloc] init];
            [resultDict setObject:[rs stringForColumn:@"content"] forKey:@"content"];
            [resultDict setObject:[rs stringForColumn:@"userId"] forKey:@"userId"];
            [resultDict setObject:[rs stringForColumn:@"caption"] forKey:@"caption"];
            [resultDict setObject:[rs stringForColumn:@"chatRoomType"] forKey:@"chatRoomType"];
            [resultDict setObject:[rs stringForColumn:@"groupId"] forKey:@"groupId"];
            [resultArray addObject:resultDict];
        }
        [rs close];
        
        
        return resultArray;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
    
    
}
-(NSArray *)getGroupMessages:(NSString*)groupId
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"select * from chatMessagesTable where groupId=\"%@\"",groupId];
        
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            NSMutableDictionary *resultDict= [[NSMutableDictionary alloc] init];
            [resultDict setObject:[rs stringForColumn:@"content"] forKey:@"content"];
            [resultDict setObject:[rs stringForColumn:@"contentType"] forKey:@"contentType"];
            [resultDict setObject:[rs stringForColumn:@"userId"] forKey:@"userId"];
            [resultDict setObject:[rs stringForColumn:@"caption"] forKey:@"caption"];
            [resultDict setObject:[rs stringForColumn:@"chatRoomType"] forKey:@"chatRoomType"];
            [resultDict setObject:[rs stringForColumn:@"groupId"] forKey:@"groupId"];
            [resultDict setObject:[rs stringForColumn:@"sender"] forKey:@"sender"];
            [resultDict setObject:[rs stringForColumn:@"checkStar"] forKey:@"checkStar"];
            
            [resultDict setObject:[rs stringForColumn:@"contentStatus"] forKey:@"contentStatus"];
            [resultDict setObject:[rs stringForColumn:@"seenTime"] forKey:@"seenTime"];
            [resultDict setObject:[rs stringForColumn:@"deliveredTime"] forKey:@"deliveredTime"];
            
            
            
            
            
            [resultArray addObject:resultDict];
        }
        [rs close];
        
        
        return resultArray;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
    
    
}

-(NSArray *)getGroupChatMessagesForMedia:(NSString*)userId;
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"select * from chatMessagesTable where groupId=\"%@\" and contentType=\"%@\" and chatRoomType=\"%@\"",userId, @"image", @"1"];
        
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            NSMutableDictionary *resultDict= [[NSMutableDictionary alloc] init];
            [resultDict setObject:[rs stringForColumn:@"content"] forKey:@"content"];
            [resultDict setObject:[rs stringForColumn:@"userId"] forKey:@"userId"];
            [resultDict setObject:[rs stringForColumn:@"caption"] forKey:@"caption"];
            [resultDict setObject:[rs stringForColumn:@"chatRoomType"] forKey:@"chatRoomType"];
            [resultDict setObject:[rs stringForColumn:@"groupId"] forKey:@"groupId"];
            [resultArray addObject:resultDict];
        }
        [rs close];
        
        
        return resultArray;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
    
}

-(BOOL)deleteChatMessages:(NSString*)userId;
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"DELETE FROM chatMessagesTable WHERE userId = '%@'", userId];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
    
    
}
-(BOOL)deleteMessages:(NSString*)msgId
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"DELETE FROM chatMessagesTable WHERE id = '%@'", msgId];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;

}

//delete Group message

-(BOOL)deleteGroupMessages:(NSString*)grpId
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"DELETE FROM chatMessagesTable WHERE groupId = '%@'", grpId];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
    
}


-(BOOL)ExitChatTblGroup:(NSString*)grpId
{
    BOOL result = NO;
        NSString *query = [NSString stringWithFormat:@"DELETE FROM chatsTable WHERE chatRoomId = '%@'", grpId];
        result = [db executeUpdate:query];
    
        if ([db hadError])
        {
            NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
            return NO;
        }
        return result;

}
-(BOOL)ExitChatTblMsgGroup:(NSString*)grpId
{
    BOOL result = NO;
        NSString *query = [NSString stringWithFormat:@"DELETE FROM chatMessagesTable WHERE groupId = '%@'", grpId];
        result = [db executeUpdate:query];
    
        if ([db hadError])
        {
            NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
            return NO;
        }
        return result;

}

#pragma mark -
#pragma mark - Calls Table Insert (Update Delete Get)

-(BOOL)insertCalls:(NSString*)id zoeChatId:(NSString*)zoeChatId callTime:(NSString*)callTime callType:(NSString*)callType callLog:(NSString*)callLog callDuration:(NSString*)callDuration;
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"insert into callsTable (zoeChatId, callTime, callType, callLog, callDuration) values(\"%@\", \"%@\", \"%@\", \"%@\", \"%@\")", zoeChatId , callTime , callType , callLog, callDuration];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
    
    
}
-(NSMutableArray *) getCalls;
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"select * from callsTable"];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
            
            [resultDict setObject:[rs stringForColumn:@"zoeChatId"] forKey:@"zoeChatId"];
            [resultDict setObject:[rs stringForColumn:@"callType"] forKey:@"callType"];
            [resultDict setObject:[rs stringForColumn:@"callLog"] forKey:@"callLog"];
            [resultDict setObject:[rs stringForColumn:@"callDuration"] forKey:@"callDuration"];
            [resultDict setObject:[rs stringForColumn:@"callTime"] forKey:@"callTime"];
            [resultArray addObject:resultDict];
        }
        [rs close];
        
     //   NSLog(@"%@",resultArray);
        
        return resultArray;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
}

-(BOOL)deleteAllCalls;
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"DELETE FROM callsTable"];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
    
    
}


-(long long)convertDateToMilliseconds:(NSDate *)date
{
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    long long milliseconds=timeInSeconds*1000;
    return milliseconds;
}
-(NSDate *)convertMillisecondsToDate:(long long)milliSeconds
{
    NSDate *dat = [NSDate dateWithTimeIntervalSince1970:(milliSeconds / 1000.0)];
    return dat;
}

#pragma mark -
#pragma mark - Group Participants Table (Insert Update Delete Get)
-(BOOL)insertParticipants:(NSString *)id userId:(NSString *)userId groupId:(NSString *)groupId joinedAt:(NSString *)joinedAt addedBy:(NSString *)addedBy isAdmin:(NSString *)isAdmin
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"insert into GroupParticipants ( userId, groupId, joinedAt, addedBy, isAdmin) values(\"%@\", \"%@\", \"%@\", \"%@\", \"%@\")",userId, groupId, joinedAt, addedBy, isAdmin];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
}

-(NSArray *)getParticipants:(NSString*)groupId

{
    @try
    {
        //NSString *query = [NSString stringWithFormat:@"select * from GroupParticipants"];
        NSString *query = [NSString stringWithFormat:@"select * from GroupParticipants where groupId=\"%@\"",groupId];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
            [resultDict setObject:[rs stringForColumn:@"userId"] forKey:@"userId"];
            [resultDict setObject:[rs stringForColumn:@"groupId"] forKey:@"groupId"];
            [resultDict setObject:[rs stringForColumn:@"joinedAt"] forKey:@"joinedAt"];
            [resultDict setObject:[rs stringForColumn:@"addedBy"] forKey:@"addedBy"];
            [resultDict setObject:[rs stringForColumn:@"isAdmin"] forKey:@"isAdmin"];
            [resultArray addObject:resultDict];
        }
        [rs close];
        
        //   NSLog(@"%@",resultArray);
        
        return resultArray;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
    
}

-(BOOL)doesGroupExist:(NSString*)groupId
{
    BOOL result = NO;
    @try
    {
        NSString *query = [NSString stringWithFormat:@"select groupId from GroupParticipants where groupId=\"%@\"",groupId];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            result =YES;
        }
        [rs close];
        
        return result;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
    
}
-(NSDictionary *)getSingleGroup:(NSString*)groupId
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"select * from GroupParticipants where groupId=\"%@\"",groupId];
        
        
        NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            
            [resultDict setObject:[rs stringForColumn:@"userId"] forKey:@"userId"];
            [resultDict setObject:[rs stringForColumn:@"groupId"] forKey:@"groupId"];
            [resultDict setObject:[rs stringForColumn:@"joinedAt"] forKey:@"joinedAt"];
            [resultDict setObject:[rs stringForColumn:@"addedBy"] forKey:@"addedBy"];
            [resultDict setObject:[rs stringForColumn:@"isAdmin"] forKey:@"isAdmin"];
        }
        [rs close];
        
        //  NSLog(@"%@",resultDict);
        
        return resultDict;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
    
    
}

//-(BOOL)updateUserName:(NSString*)mobile name:(NSString*)name;
//{
//    
//    BOOL result = NO;
//    NSString *query = [NSString stringWithFormat:@"UPDATE UserTable set name = '%@' WHERE mobile = %@",name, mobile];
//    result = [db executeUpdate:query];
//    
//    if ([db hadError])
//    {
//        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
//        return NO;
//    }
//    return result;
//    
//    
//}

-(BOOL)deleteParticipant:(NSString*)userId
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"DELETE FROM GroupParticipants WHERE userId = %@", userId];
    
    //DELETE userId set name = '%@' WHERE mobile = %@",name, mobile
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
}
-(BOOL)deleteGroupParticipant:(NSString*)groupId
{
    BOOL result = NO;
    NSString *query = [NSString stringWithFormat:@"DELETE FROM GroupParticipants WHERE groupId =\"%@\"", groupId];
    
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
}

-(BOOL)updateAdminParticipant:(NSString*)isAdmin userId:(NSString*)userId
{
    BOOL result = NO;
    //    NSString *query = [NSString stringWithFormat:@"DELETE userId = '%@' FROM GroupParticipants WHERE groupId = %@", userId,groupId];
    NSString *query = [NSString stringWithFormat:@"UPDATE GroupParticipants set isAdmin = '%@' WHERE userId = %@",isAdmin, userId];
    
    //DELETE userId set name = '%@' WHERE mobile = %@",name, mobile
    //update Table1 set value=NULL where id='001'
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
}
-(NSString *)participant:(NSString*)groupId
{
    @try
    {
        //NSString *query = [NSString stringWithFormat:@"select * from GroupParticipants"];
        NSString *query = [NSString stringWithFormat:@"select userId from GroupParticipants where groupId=\"%@\"",groupId];
        NSString *resultString ;
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
            [resultDict setObject:[rs stringForColumn:@"userId"] forKey:@"userId"];
            
            resultString=[resultDict valueForKey:@"userId"];
        }
        [rs close];
        
        //   NSLog(@"%@",resultArray);
        
        return resultString;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
    
}

#pragma mark -
#pragma mark - Link Preview (Insert Update Delete Get)

-(BOOL)insertLinks:(NSString *)title description:(NSString *)description logo:(NSString *)logo url:(NSString *)url
{
    
    BOOL result = NO;
       NSString *query = [NSString stringWithFormat:@"insert into PreviewLinks (title,  description, logo, url) values(\"%@\",\"%@\", \"%@\", \"%@\")",title,description,logo,url];
    result = [db executeUpdate:query];
    
    if ([db hadError])
    {
        NSLog(@"lastErrorMessage:%@", [db lastErrorMessage]);
        return NO;
    }
    return result;
    
}
-(NSMutableArray *)getLinkurls
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"select * from PreviewLinks"];
        
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        FMResultSet         *rs              = [db executeQuery:query];
        
        while([rs next])
        {
            NSMutableDictionary *resultDict = [[NSMutableDictionary alloc]init];
            [resultDict setObject:[rs stringForColumn:@"title"] forKey:@"title"];
            [resultDict setObject:[rs stringForColumn:@"description"] forKey:@"description"];
            [resultDict setObject:[rs stringForColumn:@"logo"] forKey:@"logo"];
            [resultDict setObject:[rs stringForColumn:@"url"] forKey:@"url"];
                        
            
            [resultArray addObject:resultDict];
        }
        [rs close];
        
        //  NSLog(@"%@",resultArray);
        
        return resultArray;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    return 0;
}

@end
