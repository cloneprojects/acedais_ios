//
//  PrivacySettingVC.m
//  ZoeChat
//
//  Created by macmini on 10/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "PrivacySettingVC.h"
#import "PrivacyTblCell.h"
#import "DetailPrivacyVC.h"

@interface PrivacySettingVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *dataArray;
    NSInteger getindexValue;
}


@end

@implementation PrivacySettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
//    dataArray = [[NSArray alloc]initWithObjects:@"LastSeen",@"ProfilePhoto",@"About", nil];
    
    dataArray = [[NSArray alloc]initWithObjects:@"LastSeen", nil];
        [_privacyTblView setFrame:CGRectMake(_privacyTblView.frame.origin.x, _privacyTblView.frame.origin.y, _privacyTblView.frame.size.width,(60.0f*([dataArray count])))];


}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
        [self.privacyTblView reloadData];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  60;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    PrivacyTblCell *cell=(PrivacyTblCell *)[tableView dequeueReusableCellWithIdentifier:@"PrivacyTblCell"];
        cell.nameLbl.text=[dataArray objectAtIndex:indexPath.row];
    if (indexPath.row == 0)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *getStatus = [defaults objectForKey:@"seenStatus"];
    
        if (getStatus==nil)
        {
            cell.statusLbl.text = @"EveryOne";
        }
        else
        {
            cell.statusLbl.text = getStatus;
        }
        
        
    }
    else if (indexPath.row == 1)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *getStatus = [defaults objectForKey:@"profileStatus"];
        
        if (getStatus==nil)
        {
            cell.statusLbl.text = @"EveryOne";
        }
        else
        {
            cell.statusLbl.text = getStatus;
        }
    }
    else if (indexPath.row == 2)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *getStatus = [defaults objectForKey:@"aboutStatus"];
        if (getStatus==nil)
        {
            cell.statusLbl.text = @"EveryOne";
        }
        else
        {
            cell.statusLbl.text = getStatus;
        }
    }
    cell.selectionStyle=UITableViewCellSelectionStyleGray;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    getindexValue=indexPath.row;
    [self performSegueWithIdentifier:@"DetailPrivacy" sender:nil];
   
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"DetailPrivacy"])
    {
        DetailPrivacyVC *Single=[segue destinationViewController];
        Single.titleStr=[dataArray objectAtIndex:getindexValue];
    }
}

- (IBAction)clickOnBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
