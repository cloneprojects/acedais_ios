//
//  GettingProfileDetailsVC.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 29/12/16.
//  Copyright © 2016 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>




@interface GettingProfileDetailsVC : UIViewController<UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>


@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnProfile;
- (IBAction)onProfile:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
- (IBAction)onDone:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;
@end
