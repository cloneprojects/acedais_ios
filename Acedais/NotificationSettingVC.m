//
//  NotificationSettingVC.m
//  ZoeChat
//
//  Created by macmini on 30/05/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "NotificationSettingVC.h"
#import "Constants.h"
#import <AudioToolbox/AudioToolbox.h>
#import "AppDelegate.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

@interface NotificationSettingVC ()<UIActionSheetDelegate>
{
    NSMutableArray *audioFileList;
    NSString *savedSoundPath;
    NSString *checkString;
    AppDelegate *appdelegate;
    AVAudioPlayer *player;
    NSMutableArray *ListOFSounds;
    NSString *msgSound;
    NSString *groupSound;




    
}
@end

@implementation NotificationSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        
        self.msgNotiSwitch.onTintColor=color;
        self.groupNotiSwitch.onTintColor=color;

        
        
    }
    else
    {
        self.msgNotiSwitch.onTintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.groupNotiSwitch.onTintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        
        
        
    }

    
    self.msgNotiSwitch.transform = CGAffineTransformMakeScale(0.75, 0.75);
    self.groupNotiSwitch.transform = CGAffineTransformMakeScale(0.75, 0.75);
    self.previewSwitch.transform = CGAffineTransformMakeScale(0.75, 0.75);
    appdelegate =(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"YES" forKey:@"isNotificationPage"];
    [defaults synchronize];

    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.height == 568)
        {
            self.scrollview.contentSize = CGSizeMake(self.view.frame.size.width, 600);
        }
        else if(result.height == 667)
        {
            self.scrollview.contentSize = CGSizeMake(self.view.frame.size.width, 700);
        }
        else if(result.height == 736)
        {
            self.scrollview.contentSize = CGSizeMake(self.view.frame.size.width, 780);
        }
    }
    ListOFSounds = [[NSMutableArray alloc]initWithObjects:@"Default",@"Beep",@"Sound", nil];
    
    NSString *ismsgON = [[NSUserDefaults standardUserDefaults]stringForKey:@"msgON"];
    if ([ismsgON isEqualToString:@"1"])
    {
        _msgNotiSwitch.on = YES;
    }
    else if([ismsgON isEqualToString:@"0"])
    {
        _msgNotiSwitch.on = NO;
    }
    else
    {
        _msgNotiSwitch.on = YES;

    }

    NSString *isgrpON = [[NSUserDefaults standardUserDefaults]stringForKey:@"grpON"];
    if ([isgrpON isEqualToString:@"1"])
    {
        _groupNotiSwitch.on = YES;
    }
    else if([isgrpON isEqualToString:@"0"])
    {
        _groupNotiSwitch.on = NO;
    }
    else
    {
        _groupNotiSwitch.on = YES;
    }

    NSString *ispreON = [[NSUserDefaults standardUserDefaults]stringForKey:@"preON"];
    if ([ispreON isEqualToString:@"1"])
    {
        _previewSwitch.on = YES;
    }
    else if([ispreON isEqualToString:@"0"])
    {
        _previewSwitch.on = NO;
    }
    else
    {
        _previewSwitch.on = YES;
    }
    NSString *type1 = [[NSUserDefaults standardUserDefaults]stringForKey:@"CheckType1"];
    if ([type1 isEqualToString:@"Msg"])
    {
        NSString *soundName = [[NSUserDefaults standardUserDefaults]stringForKey:@"msgSOUND"];
        self.showSoundLbl.text = [NSString stringWithFormat:@"%@",soundName];
    }
    else{
        _showSoundLbl.text = @"Default";

    }
    NSString *type2 = [[NSUserDefaults standardUserDefaults]stringForKey:@"CheckType2"];

     if([type2 isEqualToString:@"Grp"])
    {
        NSString *soundName = [[NSUserDefaults standardUserDefaults]stringForKey:@"grpSOUND"];
        self.showGrpSoundLbl.text = [NSString stringWithFormat:@"%@",soundName];
        
    }
    else{
                _showGrpSoundLbl.text = @"Default";

    }

    

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"YES" forKey:@"isNotificationPage"];
    [defaults synchronize];
    
   

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (IBAction)ClickonMsgSound:(UIButton*)sender {
    checkString = @"Msg";

    [self showSoundListTable];
    [[NSUserDefaults standardUserDefaults] setObject:checkString forKey:@"CheckType1"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
}

- (IBAction)ClickOnGrpSound:(UIButton*)sender {
    checkString = @"Grp";

    [self showSoundListTable];
    [[NSUserDefaults standardUserDefaults] setObject:checkString forKey:@"CheckType2"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)ClickOnInappNoti:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];

}

- (IBAction)clickOnReset:(id)sender {
//    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Reset all notification settings,including custom notification settings for your chats?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
//                            @"Reset",
//                            nil];
//    popup.tag = 1;
//    [popup showInView:self.view];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Reset all notification settings,including custom notification settings for your chats?" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Reset" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"msgON"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"preON"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"grpON"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"msgSOUND"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"grpSOUND"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"CheckType1"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"CheckType2"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isNotificationPage"];
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"msgON"];
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"grpON"];
        [[NSUserDefaults standardUserDefaults] setObject:@"Default" forKey:@"msgSOUND"];
        [[NSUserDefaults standardUserDefaults] setObject:@"Default" forKey:@"grpSOUND"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"selectedCell1"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"selectedCell2"];
        
        
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        _msgNotiSwitch.on = YES;
        _groupNotiSwitch.on = YES;
        _previewSwitch.on = YES;
        _showSoundLbl.text = @"Default";
        _showGrpSoundLbl.text = @"Default";
        
        
    }]];
    
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        actionSheet.view.tintColor=color;
    }
    else
    {
        
        actionSheet.view.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        
    }
    
}
//- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
//    
//    switch (popup.tag) {
//        case 1: {
//            switch (buttonIndex) {
//                case 0:
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"msgON"];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"preON"];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"grpON"];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"msgSOUND"];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"grpSOUND"];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"CheckType1"];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"CheckType2"];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isNotificationPage"];
//                    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"msgON"];
//                    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"grpON"];
//                    [[NSUserDefaults standardUserDefaults] setObject:@"Default" forKey:@"msgSOUND"];
//                    [[NSUserDefaults standardUserDefaults] setObject:@"Default" forKey:@"grpSOUND"];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"selectedCell1"];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"selectedCell2"];
//
//
//                    
//                    [[NSUserDefaults standardUserDefaults] synchronize];
//
//                    [[NSUserDefaults standardUserDefaults] synchronize];
//                    _msgNotiSwitch.on = YES;
//                    _groupNotiSwitch.on = YES;
//                    _previewSwitch.on = YES;
//                    _showSoundLbl.text = @"Default";
//                    _showGrpSoundLbl.text = @"Default";
//
//                    break;
//                        default:
//                    break;
//            }
//            break;
//        }
//        default:
//            break;
//    }
//}
- (IBAction)ClickOnMsgNotiswitch:(UISwitch *)sender {
    if(self.msgNotiSwitch.isOn){
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"msgON"];
        [[NSUserDefaults standardUserDefaults] synchronize];

    }else{
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"msgON"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (IBAction)clickOnGroupNotiSwitch:(UISwitch *)sender {
    if(self.groupNotiSwitch.isOn){
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"grpON"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"grpON"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

}

- (IBAction)clickOnShowPreviewswitch:(UISwitch *)sender {
    //UISwitch *switchObject = (UISwitch *)sender;
    if(self.previewSwitch.isOn){
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"preON"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"preON"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

}

- (IBAction)ClickOnBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}

-(void)showSoundListTable
{
    UIView *viewForAudioOrVideo=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+self.navigationController.navigationBar.frame.size.height+50)];
    viewForAudioOrVideo.backgroundColor=[UIColor whiteColor];
    viewForAudioOrVideo.tag=9087650;
    [self.navigationController.view addSubview:viewForAudioOrVideo];
    
    
    
    UIView *TopView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width    , 60)];
    TopView.backgroundColor=[UIColor whiteColor];
    [viewForAudioOrVideo addSubview:TopView];
    
    UIView *separatorView = [[UIView alloc]initWithFrame:CGRectMake(0, 61, self.view.frame.size.width, 1)];
    separatorView.backgroundColor=[UIColor lightGrayColor];
    [viewForAudioOrVideo addSubview:separatorView];

    
    
    UILabel *SoundLbl=[[UILabel alloc]initWithFrame:CGRectMake(125, 23, 123, 30)];
    [SoundLbl setText:@"Sound"];
    SoundLbl.numberOfLines = 0;
    SoundLbl.textAlignment = NSTextAlignmentCenter;
    [SoundLbl setFont:[UIFont fontWithName:FONT_NORMAL size:14]];
    [TopView addSubview:SoundLbl];
    
    UIButton *cancel=[[UIButton alloc]initWithFrame:CGRectMake(15, 23, 80, 30)];
    [cancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancel setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    cancel.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:14];
    [cancel addTarget:self action:@selector(Clickcancle) forControlEvents:UIControlEventTouchUpInside];
    [TopView addSubview:cancel];
    
    UIButton *save=[[UIButton alloc]initWithFrame:CGRectMake(250, 23, 80, 30)];
    [save setTitle:@"Save" forState:UIControlStateNormal];
    [save setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    save.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:14];
    [save addTarget:self action:@selector(ClickSave) forControlEvents:UIControlEventTouchUpInside];
    [TopView addSubview:save];
    
    if ([checkString isEqualToString:@"Msg"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *lastRow = [defaults objectForKey:@"selectedCell1"];
        if (lastRow) {
            self.getMsgSoundIndexPath = [NSIndexPath indexPathForRow:lastRow.integerValue inSection:0];
        }
        else{
            self.getMsgSoundIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            
        }    }
    else if ([checkString isEqualToString:@"Grp"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *lastRow = [defaults objectForKey:@"selectedCell2"];
        if (lastRow) {
            self.getGrpSoundIndexPath = [NSIndexPath indexPathForRow:lastRow.integerValue inSection:0];
        }
        else{
            self.getGrpSoundIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            
        }
    }
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 62, self.view.frame.size.width, viewForAudioOrVideo.frame.size.height)];
       tableView.delegate = self;
    tableView.dataSource = self;
    

   // [self loadAudioFileList];
    [viewForAudioOrVideo addSubview:tableView];
    
}
-(void)loadAudioFileList{
    audioFileList = [[NSMutableArray alloc] init];
    
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    //    /Library/Ringtones
    NSURL *directoryURL = [NSURL URLWithString:@"/Library/Ringtones"];
    NSArray *keys = [NSArray arrayWithObject:NSURLIsDirectoryKey];
    // /System/Library/Audio/UISounds
    NSDirectoryEnumerator *enumerator = [fileManager
                                         enumeratorAtURL:directoryURL
                                         includingPropertiesForKeys:keys
                                         options:0
                                         errorHandler:^(NSURL *url, NSError *error) {
                                             // Handle the error.
                                             // Return YES if the enumeration should continue after the error.
                                             return YES;
                                         }];
    
    for (NSURL *url in enumerator) {
        NSError *error;
        NSNumber *isDirectory = nil;
        if (! [url getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:&error]) {
            // handle error
        }
        else if (! [isDirectory boolValue]) {
            [audioFileList addObject:url];
        }
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ListOFSounds count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    cell.textLabel.text = [ListOFSounds objectAtIndex:indexPath.row];
    if ([checkString isEqualToString:@"Msg"])
    {
    if([self.getMsgSoundIndexPath isEqual:indexPath])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    }
    else if ([checkString isEqualToString:@"Grp"])
    {
        if([self.getGrpSoundIndexPath isEqual:indexPath])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        
        cell.tintColor=color;
        
        
        
    }
    else
    {
        cell.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        
        
        
        
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    if ([checkString isEqualToString:@"Msg"])
    {
        msgSound = [ListOFSounds objectAtIndex:indexPath.row];
        
        if(self.getMsgSoundIndexPath)
        {
            UITableViewCell* uncheckCell = [tableView
                                            cellForRowAtIndexPath:self.getMsgSoundIndexPath];
            uncheckCell.accessoryType = UITableViewCellAccessoryNone;
        }
         if([self.getMsgSoundIndexPath isEqual:indexPath])
        {
            self.getMsgSoundIndexPath = nil;
        }
        else
        {
            UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            self.getMsgSoundIndexPath = indexPath;
        }
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[NSNumber numberWithInt:self.getMsgSoundIndexPath.row] forKey:@"selectedCell1"];
        [defaults synchronize];
        
        
    }
    else if([checkString isEqualToString:@"Grp"])
    {
        groupSound = [ListOFSounds objectAtIndex:indexPath.row];
        if(self.getGrpSoundIndexPath)
        {
            UITableViewCell* uncheckCell = [tableView
                                            cellForRowAtIndexPath:self.getGrpSoundIndexPath];
            uncheckCell.accessoryType = UITableViewCellAccessoryNone;
        }
        if([self.getGrpSoundIndexPath isEqual:indexPath])
        {
            self.getGrpSoundIndexPath = nil;
        }
        else
        {
            UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            self.getGrpSoundIndexPath = indexPath;
        }
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[NSNumber numberWithInt:self.getGrpSoundIndexPath.row] forKey:@"selectedCell2"];
        [defaults synchronize];
        

    }
    NSLog(@"%@",[audioFileList objectAtIndex:indexPath.row]);
  
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryNone;
    
}
-(void)Clickcancle{
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    [viewForAudioOrVideo removeFromSuperview];
    if ([checkString isEqualToString:@"Msg"])
    {
        if (self.getMsgSoundIndexPath == nil) {
            
            [[NSUserDefaults standardUserDefaults] setObject:@"Default" forKey:@"msgSOUND"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            self.showSoundLbl.text = [NSString stringWithFormat:@"Default"];
        }
        else{
            
            
            if (msgSound == nil) {
                [[NSUserDefaults standardUserDefaults] setObject:@"Default" forKey:@"msgSOUND"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                self.showSoundLbl.text = [NSString stringWithFormat:@"Default"];
                
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:msgSound forKey:@"msgSOUND"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                self.showSoundLbl.text = [NSString stringWithFormat:@"%@",msgSound];
            }
            
            
        }
    }
    else if([checkString isEqualToString:@"Grp"])
    {
        if (self.getGrpSoundIndexPath == nil) {
            
            [[NSUserDefaults standardUserDefaults] setObject:@"Default" forKey:@"grpSOUND"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            self.showGrpSoundLbl.text = [NSString stringWithFormat:@"Default"];
        }
        else{
            if (groupSound == nil) {
                [[NSUserDefaults standardUserDefaults] setObject:@"Default" forKey:@"grpSOUND"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                self.showGrpSoundLbl.text = [NSString stringWithFormat:@"Default"];

            }
            else
            {
            [[NSUserDefaults standardUserDefaults] setObject:groupSound forKey:@"grpSOUND"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            self.showGrpSoundLbl.text = [NSString stringWithFormat:@"%@",groupSound];
            }
        }
        
    }

    
}
-(void)ClickSave
{
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    [viewForAudioOrVideo removeFromSuperview];
    
        if ([checkString isEqualToString:@"Msg"])
        {
            if (self.getMsgSoundIndexPath == nil) {
                
                [[NSUserDefaults standardUserDefaults] setObject:@"Default" forKey:@"msgSOUND"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                self.showSoundLbl.text = [NSString stringWithFormat:@"Default"];
            }
            else{
                if (msgSound == nil) {
                    [[NSUserDefaults standardUserDefaults] setObject:@"Default" forKey:@"msgSOUND"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    self.showSoundLbl.text = [NSString stringWithFormat:@"Default"];
                    
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults] setObject:msgSound forKey:@"msgSOUND"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    self.showSoundLbl.text = [NSString stringWithFormat:@"%@",msgSound];
                }

            }
        }
        else if([checkString isEqualToString:@"Grp"])
        {
            if (self.getGrpSoundIndexPath == nil) {
                
                [[NSUserDefaults standardUserDefaults] setObject:@"Default" forKey:@"grpSOUND"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                self.showGrpSoundLbl.text = [NSString stringWithFormat:@"Default"];
            }
            else{
                if (groupSound == nil) {
                    [[NSUserDefaults standardUserDefaults] setObject:@"Default" forKey:@"grpSOUND"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    self.showGrpSoundLbl.text = [NSString stringWithFormat:@"Default"];
                    
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults] setObject:groupSound forKey:@"grpSOUND"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    self.showGrpSoundLbl.text = [NSString stringWithFormat:@"%@",groupSound];
                }

            }
      
        }
    
    
   // appdelegate.notiSoundPath = savedSoundPath;
}
@end
