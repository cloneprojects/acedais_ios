//
//  StarredMessageVC.m
//  ZoeChat
//
//  Created by macmini on 02/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "StarredMessageVC.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "MessageCell.h"
#import "TableArray.h"
#import "DBClass.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "DGActivityIndicatorView.h"
#import "MWPhotoBrowser.h"
#import <Photos/Photos.h>
#import "CustomAlbum.h"
#import "LoadDocumentVC.h"
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
@interface StarredMessageVC ()<UITableViewDelegate,UITableViewDataSource>
{
    DBClass *db_class;
    NSMutableArray *getSingleSentTime;
    NSMutableArray *arrWholetableArray;
    NSDateFormatter *dateFormatter;
    AVAudioPlayer *myAudioPlayer;
    NSTimer *sliderTime;
    NSTimer *Timer;
    NSMutableArray *arrPhotos;
    NSString *strLat, *strLong;
    NSString *strName, *strNameToSend, *isShowDistance, *strChatRoomType, *strPlayingTag;
    AppDelegate *appDelegate;
    NSMutableArray *arrChats;


}

@property (strong, nonatomic) TableArray *tableArray;
@end

@implementation StarredMessageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    db_class = [[DBClass alloc]init];
    arrWholetableArray=[[NSMutableArray alloc]init];
    arrChats=[[NSMutableArray alloc]init];

    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy hh:mm a"];
    self.tableArray = [[TableArray alloc] init];
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];

//    UIImageView *bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgimg.png"]];
//    [self.StarredTblView setBackgroundView:bg];
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.StarredTblView addGestureRecognizer:singleTap];

    [self getStarredMessage];
}
-(void)getStarredMessage
{
    //  self.tableArray = [[TableArray alloc] init];
    NSMutableArray *data=[[NSMutableArray alloc]init];
    data=[db_class getChatMessages];
    
    if (data.count!=0)
    {
        for (int i=0; i<data.count; i++)
        {
            getSingleSentTime = [[NSMutableArray alloc]init];
            NSMutableDictionary *dictSing=[data objectAtIndex:i];
            NSDictionary *dictInfo = [db_class getUserInfo:[dictSing valueForKey:@"userId"]];
            if (dictInfo.count!=0)
            {
                [ dictSing setObject:[dictInfo valueForKey:@"name"] forKey:@"name"];
            }
            else
            {
                NSString *strNme=[NSString stringWithFormat:@"%@",[dictSing valueForKey:@"userId"]];
                [ dictSing setObject:strNme forKey:@"name"];
            }
            NSLog(@"%@",dictSing);
            [data replaceObjectAtIndex:i withObject:dictSing];
            
        }
    }
    //
    
    NSString *strChatRoom;
    NSString *strGrpName;
    
    
    
        
    if (data.count!=0)
    {
        for (int i=0; i<data.count; i++)
        {
            NSDictionary *dict=[data objectAtIndex:i];
            NSString *CheckStar=[NSString stringWithFormat:@"%@",[dict valueForKey:@"checkStar"]];
            
            
            if ([CheckStar isEqualToString:@"1"])
            {
                NSString *strsndr=[NSString stringWithFormat:@"%@",[dict valueForKey:@"sender"]];
                strChatRoom=[NSString stringWithFormat:@"%@",[dict valueForKey:@"chatRoomType"]];

                
                    Message *message = [[Message alloc] init];
                if ([strChatRoom isEqualToString:@"1"])
                {
                    
                    arrChats=[db_class getChats];
                    
                    if (arrChats.count!=0)
                    {
                        for (int i=0; i<arrChats.count; i++)
                        {
                            NSMutableDictionary *dictSing=[arrChats objectAtIndex:i];
                            NSDictionary *dictInfo =[db_class getUserInfo:[dictSing valueForKey:@"chatRoomId"]];
                            strGrpName=[NSString stringWithFormat:@"%@",[dictSing valueForKey:@"groupName"]];
                            
                        }
                    }

                    
                    
                    message.name = strGrpName;

                }
                else
                {
                message.name = [dict valueForKey:@"name"];
                }
                    message.type=[dict valueForKey:@"contentType"];
                    if ([message.type isEqualToString:@"image"])
                    {
                        message.text = [dict valueForKey:@"content"];
                        
                        
                        
                            NSString *strContent=[NSString stringWithFormat:@"%@", [dict valueForKey:@"content"]];
                            NSArray *components = [strContent componentsSeparatedByString:@"/"];
                            NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                            
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
                            NSString *documentsDirectory = [paths objectAtIndex:0];
                            NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
                            message.image = [UIImage imageWithContentsOfFile:getImagePath];
                            
                    }
                        
                
                    else if ([message.type isEqualToString:@"location"])
                    {
                        NSString *strImageName=[NSString stringWithFormat:@"%@", [dict valueForKey:@"content"]];
                        
                        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
                        NSString *documentsDirectory = [paths objectAtIndex:0];
                        NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
                        message.image = [UIImage imageWithContentsOfFile:getImagePath];
                        message.text = [dict valueForKey:@"content"];
                        message.Latitude=[dict valueForKey:@"latitude"];
                        message.Longitude=[dict valueForKey:@"longitude"];
                    }
                    else if ([message.type isEqualToString:@"video"])
                    {
                        
                            NSString *strContent=[NSString stringWithFormat:@"%@", [dict valueForKey:@"content"]];
                            NSArray *components = [strContent componentsSeparatedByString:@"/"];
                            NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
                            NSString *documentsDirectory = [paths objectAtIndex:0];
                            NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
                            NSURL *assetURL=[NSURL fileURLWithPath:getImagePath];
                            message.image = [self thumbnailImageFromURL:assetURL];
                            message.text = [dict valueForKey:@"content"];
                        
                        
                    }
                    else if ([message.type isEqualToString:@"contact"])
                    {
                        message.contactName = [dict valueForKey:@"contactName"];
                        message.contactNo = [dict valueForKey:@"contactNumber"];
                    }
                    else if ([message.type isEqualToString:@"document"])
                    {
                        
                        message.text = [dict valueForKey:@"content"];
                    }
                    else
                    {
                        message.text = [dict valueForKey:@"content"];
                    }
                    
                    message.date=[self convertMillisecondsToDate:[[dict valueForKey:@"sentTime"]longLongValue]];
                    message.chat_id = [dict valueForKey:@"id"];
                    message.caption=[dict valueForKey:@"caption"];
                    
                
                if ([strsndr isEqualToString:@"0"])
                {
                    message.sender=MessageSenderMyself;
                    if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"sending"])
                    {
                        message.status = MessageStatusSending;
                    }
                    else if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"sent"])
                    {
                        message.status = MessageStatusSent;
                    }
                    else if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"delivered"])
                    {
                        message.status = MessageStatusReceived;
                    }
                    else if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"read"])
                    {
                        message.status = MessageStatusRead;
                    }
                    else if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"cancelled"])
                    {
                        message.status = MessageStatusCancelled;
                    }
                    else if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"uploaded"])
                    {
                        message.status = MessageStatusUploaded;
                    }
                    
                    
                }
                else{
                    message.sender=MessageSenderSomeone;
                    message.status = MessageStatusRead;
                    NSString *strSeenTime=[NSString stringWithFormat:@"%@",[dict valueForKey:@"seenTime"]];
                    //  BOOL  success=[db_class updateContentStatus:message.chat_id contentStatus:@"read"];
                    if ([strSeenTime isEqualToString:@"0"])
                    {
                        long long milli=[self convertDateToMilliseconds:[NSDate date]];
                        NSString *strMsgTime=[NSString stringWithFormat:@"%lld",milli];
                        NSString *strMsgId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"id"]];
                        NSString *strMsgSender=[NSString stringWithFormat:@"%@",[dict valueForKey:@"userId"]];
                        BOOL success=NO;
                        success=[db_class updateSeenTime:strMsgId seenTime:strMsgTime];
                        
                        
                        
                        NSDictionary *dictSing=[db_class getSingleChat:strMsgSender];
                        
                        if ([[dictSing valueForKey:@"lastMessageTime"]isEqualToString:[dict valueForKey:@"sentTime"]])
                        {
                            success=[db_class updateLastMsgStatus:strMsgSender lastMsgStatus:@"read"];
                        }
                        
                    }
                    if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"downloaded"])
                    {
                        message.status = MessageStatusDownloaded;
                    }
                    else if ([[dict valueForKey:@"contentStatus"]isEqualToString:@"downloading"])
                    {
                        message.status = MessageStatusDownloading;
                    }
                }
                [self.tableArray addObject:message];
                [arrWholetableArray addObject:message];
            }
            
            [getSingleSentTime addObject:[dict valueForKey:@"sentTime"]];
            
        }
        
        
    }
    //  [self.tableArray addObjectsFromArray:data];
    [self.StarredTblView reloadData];
    
   
    
   // [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_App_Badge_Count" object:self];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.tableArray numberOfSections];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tableArray numberOfMessagesInSection:section];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:[indexPath description]];
    NSInteger rowNumber = 0;
        for (NSInteger i = 0; i < indexPath.section; i++) {
            rowNumber += [self tableView:tableView numberOfRowsInSection:i];
        }
        rowNumber += indexPath.row;
        if (!cell)
        {
            cell = [[MessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        }
    //[cell.bubbleImage setHidden:YES];
    NSDictionary *getSender = [arrWholetableArray objectAtIndex:indexPath.row];
    NSLog(@"%@",getSender);
    NSString *sender = [getSender valueForKey:@"sender"];
//    if([sender isEqual:0])
//    {
//        cell.nameLabel.text = @"";
//
//    }
//    else{
//    //cell.nameLabel.text = cell.message.name;
//    }
        cell.message = [self.tableArray objectAtIndexPath:indexPath];
        cell.StarimgView.image = [UIImage imageNamed:@"FilledStar"];
        if ([cell.message.type isEqualToString:@"video"])
        {
            NSString *strContent=[NSString stringWithFormat:@"%@", cell.message.text];
            NSArray *components = [strContent componentsSeparatedByString:@"/"];
            NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
            NSString *name = strImageName;
            NSString *S1 = [name componentsSeparatedByString:@"."][0];
            NSString *S2 = [NSString stringWithFormat:@"%@.jpg",S1];
            NSLog(@"%@",S1);
            NSLog(@"%@",S2);
            
            strImageName=[NSString stringWithFormat:@"%@thumb_%@",FILE_AWS_IMAGE_BASE_URL, S2];
            UIImageView *getImg = [[UIImageView alloc]init];

            NSURL *imageURL = [NSURL URLWithString:strImageName];
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadImageWithURL:imageURL
                                  options:0
                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                     // progression tracking code, optional.
                                     // You can set the progress block to nil
                                 }
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (image) {
                                        // Resize your image here, then set it to UIImageView
                                        cell.message.image =image;
                                        // resize to 0.5x, function available since iOS 6
                                    }
                                }];
            cell.message.text = strContent;
            
        }
        NSDictionary *getlastInfo = [arrWholetableArray objectAtIndex:indexPath.row];
        NSLog(@"%@",getlastInfo);
        NSString *checkStar = [getlastInfo valueForKey:@"checkStar"];
    
        if ([cell.message.type isEqualToString:@"image"] || [cell.message.type isEqualToString:@"video"])
        {
            long status=cell.message.status;
            NSString *strStat=[NSString stringWithFormat:@"%ld",status];
            
            if (cell.message.sender==MessageSenderMyself)
            {
                if ([strStat isEqualToString:@"0"])
                {
                    
                    DGActivityIndicatorView *activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor whiteColor] size:50.0f];
                    activityIndicatorView.frame = CGRectMake((cell.imgView.frame.size.width/2)-25, (cell.imgView.frame.size.height/2)-25, 50, 50);
                    activityIndicatorView.backgroundColor=[UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:0.5];
                    activityIndicatorView.layer.cornerRadius=25;
                    activityIndicatorView.clipsToBounds=YES;
                    [cell.imgView addSubview:activityIndicatorView];
                    activityIndicatorView.tag=rowNumber+22000;
                    [activityIndicatorView startAnimating];
                    
                    UIButton *btnCancelUpload=[[UIButton alloc]initWithFrame:CGRectMake(cell.imgView.center.x-12.5, cell.imgView.center.y-12.5, 25, 25)];
                    btnCancelUpload.tag=rowNumber+25000;
                    [btnCancelUpload setBackgroundImage:[UIImage imageNamed:@"CloseIcon.png"] forState:UIControlStateNormal];
                    [btnCancelUpload addTarget:self action:@selector(cancelUpload:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.contentView addSubview:btnCancelUpload];
                }
                else if ([strStat isEqualToString:@"5"])
                {
                    UIButton *btnRetry=[[UIButton alloc]initWithFrame:CGRectMake(cell.imgView.center.x-25, cell.imgView.center.y-12.5, 50, 25)];
                    btnRetry.tag=rowNumber+28000;
                    [btnRetry setBackgroundImage:[UIImage imageNamed:@"retry.png"] forState:UIControlStateNormal];
                    [btnRetry addTarget:self action:@selector(RetryUpload:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.contentView addSubview:btnRetry];
                }
                else if ([strStat isEqualToString:@"6"])
                {
                    UIButton *btnCancelUpload=(UIButton *)[cell.contentView viewWithTag:rowNumber+25000];
                    [btnCancelUpload removeFromSuperview];
                    
                    UIButton *btnRetry=(UIButton *)[cell.contentView viewWithTag:rowNumber+28000];
                    [btnRetry removeFromSuperview];
                    DGActivityIndicatorView *activityIndicatorView = (DGActivityIndicatorView *)[cell.imgView viewWithTag:rowNumber+22000];
                    [activityIndicatorView stopAnimating];
                    
                }
                
                if (![strStat isEqualToString:@"0"] && ![strStat isEqualToString:@"4"] && ![strStat isEqualToString:@"5"])
                {
                    if ([cell.message.type isEqualToString:@"video"])
                    {
                        UIButton *btnPlay=[[UIButton alloc]initWithFrame:CGRectMake(cell.imgView.center.x-25, cell.imgView.center.y-25, 50 , 50)];
                        btnPlay.tag=rowNumber+35000;
                        [btnPlay setBackgroundImage:[UIImage imageNamed:@"Play.png"] forState:UIControlStateNormal];
                        [btnPlay addTarget:self action:@selector(onPlayVideo:) forControlEvents:UIControlEventTouchUpInside];
                        [cell.contentView addSubview:btnPlay];
                    }
                }
            }
            else
            {
                
                if ([strStat isEqualToString:@"8"])
                {
                    DGActivityIndicatorView *activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor whiteColor] size:50.0f];
                    activityIndicatorView.frame = CGRectMake((cell.imgView.frame.size.width/2)-25, (cell.imgView.frame.size.height/2)-25, 50, 50);
                    activityIndicatorView.backgroundColor=[UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:0.5];
                    activityIndicatorView.layer.cornerRadius=25;
                    activityIndicatorView.clipsToBounds=YES;
                    [cell.imgView addSubview:activityIndicatorView];
                    activityIndicatorView.tag=rowNumber+22000;
                    [activityIndicatorView startAnimating];
                    
                    
                    UIButton *btnCancelDownload=[[UIButton alloc]initWithFrame:CGRectMake(cell.imgView.center.x-12.5, cell.imgView.center.y-12.5, 25, 25)];
                    btnCancelDownload.tag=rowNumber+33000;
                    [btnCancelDownload setBackgroundImage:[UIImage imageNamed:@"CloseIcon.png"] forState:UIControlStateNormal];
                    [btnCancelDownload addTarget:self action:@selector(cancelDownload:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.contentView addSubview:btnCancelDownload];
                    
                }
                else if ([strStat isEqualToString:@"7"])
                {
                    if ([cell.message.type isEqualToString:@"video"])
                    {
                        UIButton *btnPlay=[[UIButton alloc]initWithFrame:CGRectMake(cell.imgView.center.x-25, cell.imgView.center.y-25, 50, 50)];
                        btnPlay.tag=rowNumber+35000;
                        [btnPlay setBackgroundImage:[UIImage imageNamed:@"Play.png"] forState:UIControlStateNormal];
                        [btnPlay addTarget:self action:@selector(onPlayVideo:) forControlEvents:UIControlEventTouchUpInside];
                        [cell.contentView addSubview:btnPlay];
                    }
                    
                }
                else
                {
                    UIButton *btnDownload=[[UIButton alloc]initWithFrame:cell.imgView.frame];
                    btnDownload.tag=rowNumber+30000;
                    [btnDownload setImage:[UIImage imageNamed:@"download.png"] forState:UIControlStateNormal];
                    [btnDownload addTarget:self action:@selector(downloadImageOrVideo:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.contentView addSubview:btnDownload];
                }
                
            }
        }
        else if ([cell.message.type isEqualToString:@"contact"])
        {
            if (cell.message.sender==MessageSenderMyself)
            {
                [cell.btnInviteOrMsg addTarget:self action:@selector(onInvite:) forControlEvents:UIControlEventTouchDown];
                cell.btnInviteOrMsg.tag=rowNumber+45000;
            }
            else
            {
                [cell.btnAddContact addTarget:self action:@selector(onAddNewContact:) forControlEvents:UIControlEventTouchDown];
                cell.btnAddContact.tag=rowNumber+48000;
                
                
                [cell.btnInviteOrMsg addTarget:self action:@selector(onInvite:) forControlEvents:UIControlEventTouchDown];
                cell.btnInviteOrMsg.tag=rowNumber+45000;
                
            }
        }
        else if ([cell.message.type isEqualToString:@"audio"])
        {
            long status=cell.message.status;
            NSString *strStat=[NSString stringWithFormat:@"%ld",status];
            
            if (cell.message.sender==MessageSenderMyself)
            {
                if ([strStat isEqualToString:@"0"])
                {
                    float xPos=cell.viewContact.frame.origin.x + cell.imgContact.frame.size.width;
                    
                    DGActivityIndicatorView *activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor blackColor] size:30.0f];
                    activityIndicatorView.frame = CGRectMake(xPos + 15, 12, 30, 30);
                    // activityIndicatorView.frame = CGRectMake(0, 12, 30, 30);
                    activityIndicatorView.backgroundColor=[UIColor clearColor];
                    activityIndicatorView.layer.cornerRadius=activityIndicatorView.frame.size.height/2;
                    activityIndicatorView.clipsToBounds=YES;
                    [cell.contentView addSubview:activityIndicatorView];
                    activityIndicatorView.tag=rowNumber+22000;
                    [activityIndicatorView startAnimating];
                    
                    [cell.btnAudioAction addTarget:self action:@selector(cancelUpload:) forControlEvents:UIControlEventTouchDown];
                    cell.btnAudioAction.tag=rowNumber+25000;
                    [cell.btnAudioAction setBackgroundImage:[UIImage imageNamed:@"CloseIcon.png"] forState:UIControlStateNormal];
                    
                    [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                    cell.slider.tag=rowNumber+25000;
                    NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                    
                }
                else if ([strStat isEqualToString:@"5"])
                {
                    [cell.btnAudioAction addTarget:self action:@selector(RetryUpload:) forControlEvents:UIControlEventTouchDown];
                    cell.btnAudioAction.tag=rowNumber+28000;
                    [cell.btnAudioAction setBackgroundImage:[UIImage imageNamed:@"UploadAudio.png"] forState:UIControlStateNormal];
                    
                    [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                    cell.slider.tag=rowNumber+54000;
                    NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                }
                
                else
                {
                    [cell.btnAudioAction addTarget:self action:@selector(onAudioPlay:) forControlEvents:UIControlEventTouchUpInside];
                    cell.btnAudioAction.tag=rowNumber+51000;
                    [cell.btnAudioAction setBackgroundImage:[UIImage imageNamed:@"PlayButton.png"] forState:UIControlStateNormal];
                    
                    [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                    cell.slider.tag=rowNumber+54000;
                    NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                }
            }
            
            else
            {
                
                if ([strStat isEqualToString:@"8"])
                {

                    DGActivityIndicatorView *activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor blackColor] size:30.0f];
                    activityIndicatorView.frame = CGRectMake(25 , 12, 30, 30);
                    activityIndicatorView.backgroundColor=[UIColor clearColor];
                    activityIndicatorView.layer.cornerRadius=activityIndicatorView.frame.size.height/2;
                    activityIndicatorView.clipsToBounds=YES;
                    [cell.contentView addSubview:activityIndicatorView];
                    activityIndicatorView.tag=rowNumber+22000;
                    [activityIndicatorView startAnimating];
                    
                    [cell.btnAudioAction addTarget:self action:@selector(cancelUpload:) forControlEvents:UIControlEventTouchDown];
                    cell.btnAudioAction.tag=rowNumber+25000;
                    [cell.btnAudioAction setBackgroundImage:[UIImage imageNamed:@"CloseIcon.png"] forState:UIControlStateNormal];
                    
                    [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                    cell.slider.tag=rowNumber+25000;
                    NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                    
                }
                else if ([strStat isEqualToString:@"7"])
                {
                    [cell.btnAudioAction addTarget:self action:@selector(onAudioPlay:) forControlEvents:UIControlEventTouchUpInside];
                    cell.btnAudioAction.tag=rowNumber+51000;
                    [cell.btnAudioAction setBackgroundImage:[UIImage imageNamed:@"PlayButton.png"] forState:UIControlStateNormal];
                    
                    [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                    cell.slider.tag=rowNumber+54000;
                    NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                    
                    
                }
                else
                {
                    
                    cell.btnAudioAction.tag=rowNumber+30000;
                    [cell.btnAudioAction setBackgroundImage:[UIImage imageNamed:@"DownloadAudio.png"] forState:UIControlStateNormal];
                    [cell.btnAudioAction addTarget:self action:@selector(downloadImageOrVideo:) forControlEvents:UIControlEventTouchDown];
                    [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                    cell.slider.tag=rowNumber+54000;
                    NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                    
                }
                
            }
        }
        else if ([cell.message.type isEqualToString:@"document"])
            
            
        {
            long status=cell.message.status;
            NSString *strStat=[NSString stringWithFormat:@"%ld",status];
            
            NSString *getdocument = [NSString stringWithFormat:@"%@",cell.message.text];
            NSLog(@"%@",getdocument);
            NSArray *components = [getdocument componentsSeparatedByString:@"/"];
            NSString *FileName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
            
            cell.documentName.textColor = [UIColor blackColor];
            [cell.documentName setFont: [cell.documentName.font fontWithSize: 10.0]];
            cell.documentName.text = FileName;
            
            
            
            if (cell.message.sender==MessageSenderMyself)
            {
                if ([strStat isEqualToString:@"0"])
                {
                    float xPos=cell.viewContact.frame.origin.x + cell.imgContact.frame.size.width;
                    
                    DGActivityIndicatorView *activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor blackColor] size:30.0f];
                    activityIndicatorView.frame = CGRectMake(xPos + 15, 12, 30, 30);
                    // activityIndicatorView.frame = CGRectMake(0, 12, 30, 30);
                    activityIndicatorView.backgroundColor=[UIColor clearColor];
                    activityIndicatorView.layer.cornerRadius=activityIndicatorView.frame.size.height/2;
                    activityIndicatorView.clipsToBounds=YES;
                    [cell.contentView addSubview:activityIndicatorView];
                    activityIndicatorView.tag=rowNumber+22000;
                    [activityIndicatorView startAnimating];
                    
                    //                    [cell.docBtn addTarget:self action:@selector(cancelUpload:) forControlEvents:UIControlEventTouchDown];
                    cell.docBtn.tag=rowNumber+25000;
                    //                    [cell.docBtn setBackgroundImage:[UIImage imageNamed:@"CloseIcon.png"] forState:UIControlStateNormal];
                    
                    //                    [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                    //                    cell.slider.tag=rowNumber+25000;
                    //                    NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                    
                }
                else if ([strStat isEqualToString:@"5"])
                {
                    //                    [cell.docBtn addTarget:self action:@selector(RetryUpload:) forControlEvents:UIControlEventTouchDown];
                    cell.docBtn.tag=rowNumber+28000;
                    //                    [cell.docBtn setBackgroundImage:[UIImage imageNamed:@"UploadAudio.png"] forState:UIControlStateNormal];
                    
                    //                    [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                    //                    cell.slider.tag=rowNumber+54000;
                    //                    NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                }
                
                else
                {
                    [cell.docBtn addTarget:self action:@selector(downloadDoc:) forControlEvents:UIControlEventTouchUpInside];
                    cell.docBtn.tag=rowNumber+51000;
                    //                    [cell.docBtn setBackgroundImage:[UIImage imageNamed:@"PlayButton.png"] forState:UIControlStateNormal];
                    
                    //                    [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                    //                    cell.slider.tag=rowNumber+54000;
                    //                    NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                }
            }
            else
            {
                
                if ([strStat isEqualToString:@"8"])
                {
                    float xPos=cell.viewContact.frame.origin.x + cell.imgContact.frame.size.width;
                    DGActivityIndicatorView *activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor blackColor] size:30.0f];
                    //                _viewContact.frame.origin.x + 10, 18, 20, 20
                    //                _btnAudioAction=[[UIButton alloc]initWithFrame:CGRectMake(_viewContact.frame.origin.x + 10, 18, 20, 20)];
                    activityIndicatorView.frame = CGRectMake(xPos-62, 5, 50, 50);
                    //activityIndicatorView.frame = CGRectMake(18, 12, 30, 30);
                    
                    activityIndicatorView.backgroundColor=[UIColor clearColor];
                    activityIndicatorView.layer.cornerRadius=activityIndicatorView.frame.size.height/2;
                    activityIndicatorView.clipsToBounds=YES;
                    [cell.contentView addSubview:activityIndicatorView];
                    activityIndicatorView.tag=rowNumber+22000;
                    [activityIndicatorView startAnimating];
                    
                    [cell.docBtn addTarget:self action:@selector(cancelUpload:) forControlEvents:UIControlEventTouchDown];
                    cell.docBtn.tag=rowNumber+25000;
                    [cell.docBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                    
                    //                [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                    //                cell.slider.tag=rowNumber+25000;
                    //                NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                    
                }
                else if ([strStat isEqualToString:@"7"])
                {
                    [cell.docBtn addTarget:self action:@selector(downloadDoc:) forControlEvents:UIControlEventTouchUpInside];
                    cell.docBtn.tag=rowNumber+51000;
                    //                [cell.docBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                    //
                    ////                [cell.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
                    ////                cell.slider.tag=rowNumber+54000;
                    //                NSLog(@"%ld",(long)cell.btnAudioAction.tag);
                }
                else
                {
                    
                    cell.docBtn.tag=rowNumber+30000;
                    //                [cell.docBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                    [cell.docBtn addTarget:self action:@selector(downloadImageOrVideo:) forControlEvents:UIControlEventTouchDown];
                    
                    NSLog(@"%ld",(long)cell.docBtn.tag);
                    
                }
                
            }
        }
        else if ([cell.message.type isEqualToString:@"sticker"])
        {
            [cell.bubbleImage setHidden:YES];
            
            NSLog(@"%@",cell.message.text);
            [cell fillWithStickerMessage: cell.message.text downloaded: [self.stickerController isStickerPackDownloaded: cell.message.text]];
        }
        
        
        return cell;
        
    }
- (BOOL) validateUrl: (NSString *) candidate {
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
}
#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Message *message = [self.tableArray objectAtIndexPath:indexPath];
    NSLog(@"%f",message.heigh);
    return message.heigh;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    return [self.tableArray titleForSection:section];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGRect frame = CGRectMake(0, 0, tableView.frame.size.width, 40);
    
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.backgroundColor = [UIColor clearColor];
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UILabel *label = [[UILabel alloc] init];
    label.text = [self tableView:tableView titleForHeaderInSection:section];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:FONT_NORMAL size:20.0];
    [label sizeToFit];
    label.center = view.center;
    label.font = [UIFont fontWithName:FONT_NORMAL size:13.0];
    label.backgroundColor = [UIColor colorWithRed:207/255.0 green:220/255.0 blue:252.0/255.0 alpha:1];
    label.layer.cornerRadius = 10;
    label.layer.masksToBounds = YES;
    label.autoresizingMask = UIViewAutoresizingNone;
    [view addSubview:label];
    return view;
}


    
- (UIImage *)thumbnailImageFromURL:(NSURL *)videoURL
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL: videoURL options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    NSError *err = NULL;
    CMTime requestedTime = CMTimeMake(1, 60);     // To create thumbnail image
    CGImageRef imgRef = [generator copyCGImageAtTime:requestedTime actualTime:NULL error:&err];
    NSLog(@"err = %@, imageRef = %@", err, imgRef);
    UIImage *thumbnailImage = [[UIImage alloc] initWithCGImage:imgRef];
    CGImageRelease(imgRef);    // MUST release explicitly to avoid memory leak
    return thumbnailImage;
}
-(long long)convertDateToMilliseconds:(NSDate *)date
{
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    long long milliseconds=timeInSeconds*1000;
    return milliseconds;
}
-(NSDate *)convertMillisecondsToDate:(long long)milliSeconds
{
    NSDate *dat = [NSDate dateWithTimeIntervalSince1970:(milliSeconds / 1000.0)];
    return dat;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)ClickOnBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)playSound :(NSString *)fName :(NSString *) ext{
    SystemSoundID audioEffect;
    NSString *path = [[NSBundle mainBundle] pathForResource : fName ofType :ext];
    if ([[NSFileManager defaultManager] fileExistsAtPath : path]) {
        NSURL *pathURL = [NSURL fileURLWithPath: path];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef) pathURL, &audioEffect);
        AudioServicesPlaySystemSound(audioEffect);
    }
    else {
        NSLog(@"error, file not found: %@", path);
    }
}

-(void)getplay:(UIButton*)sender
{
    
    //    if (myAudioPlayer.isPlaying)
    //    {
    //        PlayButton.png
    [_PlayPauseButton setBackgroundImage:[UIImage imageNamed:@"PlayButton.png"] forState:UIControlStateNormal];
    //    }
    //    else{
    //        [_PlayPauseButton setBackgroundImage:[UIImage imageNamed:@"PauseButton.png"] forState:UIControlStateNormal];
    //    }
    
    NSString *name = sender.accessibilityHint;
    
    
    NSString *S1 = [name componentsSeparatedByString:@"."][0];
    NSString *S2 = [name componentsSeparatedByString:@"."][1];
    NSLog(@"%@",S1);
    NSLog(@"%@",S2);
    
    //    if ([S2 isEqualToString:@"aac"])
    //    {
    //    NSString *appendString = @"m4a";
    //        name = [NSString stringWithFormat:@"%@.%@",S1,appendString];
    //    }
    //    NSLog(@"%@",name);
    NSError *error;
    
    
    
    
    
    
    
    
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *getPath = [documentsDirectory stringByAppendingPathComponent:@""];
    NSURL *audioURL = [NSURL fileURLWithPath:getPath];
    
    NSData *audioData = [NSData dataWithContentsOfURL:audioURL];
    NSString *docDirPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", docDirPath , name];
    [audioData writeToFile:filePath atomically:YES];
    
    NSURL *fileURL = [NSURL fileURLWithPath:filePath];
    //    myAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
    //    if (myAudioPlayer == nil) {
    //        NSLog(@"AudioPlayer did not load properly: %@", [error description]);
    //    } else {
    //        [myAudioPlayer play];
    //    }
    
    
    
    
    AVPlayer *player = [AVPlayer playerWithURL:fileURL];
    
    // create a player view controller
    AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
    [self presentViewController:controller animated:YES completion:nil];
    controller.player = player;
    [player play];
    
    
    
    //    myAudioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:audioURL error:&error];
    //    myAudioPlayer.delegate = self;
    //
    //    AVAudioSession* audioSession = [AVAudioSession sharedInstance];
    //    NSError *errorSession = nil;
    //    [audioSession setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionDuckOthers error:nil];
    //    [audioSession setActive:NO error:&errorSession];
    //
    //    [myAudioPlayer prepareToPlay];
    //    [myAudioPlayer setVolume:1.0];
    //    [myAudioPlayer play];
    //    if (error) {
    //        NSLog(@"error %@",[error localizedDescription]);
    //    }
    
    // Construct URL to sound file
    //    NSString *path = [NSString stringWithFormat:@"%@/20170522231600775.m4a", [[NSBundle mainBundle] resourcePath]];
    //    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    //
    //    // Create audio player object and initialize with URL to sound
    //    myAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    
    //    NSURL *soundURL = [[NSBundle mainBundle] URLForResource:S1
    //                                              withExtension:S2];
    //    myAudioPlayer = [[AVAudioPlayer alloc]
    //               initWithContentsOfURL:soundURL error:nil];
    //
    //    [myAudioPlayer play];
    
    
    
    
    //    NSError *error = nil;
    //
    //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    //    NSString *documentsDirectory = [paths objectAtIndex:0];
    //    NSString *getPath = [documentsDirectory stringByAppendingPathComponent:name];
    //
    //        //NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:getPath ];
    //    NSURL *fileURL = [[NSURL alloc]initWithString:getPath];
    //
    //
    //    myAudioPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
    //    if (!myAudioPlayer) {
    //        NSLog(@"failed playing SeriouslyFunnySound1, error: %@", error.description);
    //    }
    //    else{
    //        [[AVAudioSession sharedInstance] setDelegate: self];
    //        NSError *setCategoryError = nil;
    //        [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error: &setCategoryError];
    //        if (setCategoryError)
    //            NSLog(@"Error setting category! %@", setCategoryError);
    //    myAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
    //    [myAudioPlayer setVolume:5.0];
    //        [myAudioPlayer prepareToPlay];
    //
    //    [myAudioPlayer play];
    //    }
    //    self.movingslider.maximumValue = myAudioPlayer.duration;
    //    NSLog(@"%f",myAudioPlayer.duration);
    
    
    sliderTime = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                  target:self
                                                selector:@selector(updateSlider:)
                                                userInfo:nil
                                                 repeats:YES];
    Timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                             target:self
                                           selector:@selector(updateTimer)
                                           userInfo:nil
                                            repeats:YES];
    
    NSTimeInterval currentTime = myAudioPlayer.currentTime;
    NSInteger minutes = floor(currentTime/60);
    NSInteger seconds = trunc(currentTime - minutes * 60);
    NSLog(@"TimeLabel:%@",[NSString stringWithFormat:@"%d:%02d", minutes, seconds]);
    
    
}


-(void)SliderChanged:(UISlider*)sender
{
    [myAudioPlayer stop];
    myAudioPlayer.currentTime = sender.value;
    [myAudioPlayer prepareToPlay];
    [myAudioPlayer play];
}
//Audio
-(void)CreateAudioPreview:(NSString*)getSoundName
{
    //WholeView
    _WholeView=[[UIView alloc]initWithFrame:CGRectMake(self.StarredTblView.frame.origin.x, self.StarredTblView.frame.origin.y, self.StarredTblView.frame.size.width, self.StarredTblView.frame.size.height)];
    _WholeView.backgroundColor=[UIColor colorWithRed:179.0/255.0f green:179.0/255.0f blue:179.0/255.0f alpha:0.7];
    
    _WholeView.tag=9087650;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onRemoveViewAudioVideo:)]; // this is the current problem like a lot of people out there...
    [_WholeView addGestureRecognizer:tap];
    //AudioView
    _AudioView=[[UIView alloc]initWithFrame:CGRectMake(60, 258, 255, 150)];
    [_AudioView setBackgroundColor:[UIColor whiteColor]];
    _AudioView.layer.cornerRadius = 5;
    _AudioView.layer.masksToBounds = YES;
    _AudioView.center = _WholeView.center;
    //Audionamelbl
    _AudioNameLabel=[[UILabel alloc]initWithFrame:CGRectMake(9, 15, 216, 25)];
    [_AudioNameLabel setBackgroundColor:[UIColor clearColor]];
    [_AudioNameLabel setText:getSoundName];
    [_AudioNameLabel setFont: [_AudioNameLabel.font fontWithSize: 14.0]];
    [_AudioView addSubview:_AudioNameLabel];
    //AudioTimeLbl
    _TimeLabel=[[UILabel alloc]initWithFrame:CGRectMake(211, 70, 40, 20)];
    [_TimeLabel setBackgroundColor:[UIColor clearColor]];
    //[_TimeLabel setText:@"00:00"];
    [_TimeLabel setFont: [_TimeLabel.font fontWithSize: 12.0]];
    [_AudioView addSubview:_TimeLabel];
    
    //PlayPauseBtn
    //PauseButton.png
    _PlayPauseButton=[[UIButton alloc]initWithFrame:CGRectMake(15, 70, 20, 20)];
    [_PlayPauseButton setBackgroundImage:[UIImage imageNamed:@"PlayButton.png"] forState:UIControlStateNormal];
    [_PlayPauseButton addTarget:self action:@selector(getplay:) forControlEvents:UIControlEventTouchDown];
    _PlayPauseButton.accessibilityHint = getSoundName;
    [_AudioView addSubview:_PlayPauseButton];
    
    //CancelBtn
    _CancelButton=[[UIButton alloc]initWithFrame:CGRectMake(159, 116, 85, 30)];
    [_CancelButton setBackgroundColor:[UIColor clearColor]];
    [_CancelButton setTitle:@"CANCEL" forState:UIControlStateNormal];
    [_CancelButton.titleLabel setFont:[UIFont systemFontOfSize:13]];
    [_CancelButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [_CancelButton addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchDown];
    [_AudioView addSubview:_CancelButton];
    
    //Slider
    _movingslider=[[UISlider alloc]initWithFrame:CGRectMake(58, 70, 120, 20)];
    [_movingslider setBackgroundColor:[UIColor clearColor]];
    
    _movingslider.minimumTrackTintColor = [UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
    UIImage* _sliderBarImage = [UIImage imageNamed:@"25 x 5.png"];
    _sliderBarImage=[_sliderBarImage stretchableImageWithLeftCapWidth:8.0 topCapHeight:10.0];
    [_movingslider setMaximumTrackImage:_sliderBarImage forState:UIControlStateNormal];
    
    UIImage *_sliderThumb=[UIImage imageNamed:@"rec.png"];
    // _sliderThumb=[_sliderThumb stretchableImageWithLeftCapWidth:8.0 topCapHeight:10.0];
    [_movingslider setThumbImage:_sliderThumb forState:UIControlStateNormal];
    [_movingslider addTarget:self action:@selector(SliderChanged:) forControlEvents:UIControlEventValueChanged];
    [_AudioView addSubview:_movingslider];
    
    [_WholeView addSubview:_AudioView];
    [self.StarredTblView.superview addSubview:_WholeView];
    
}
-(void)updateSlider:(UISlider*)sender
{
    self.movingslider.value = myAudioPlayer.currentTime;
}
-(void)updateTimer
{
    
    NSTimeInterval currentTime = myAudioPlayer.currentTime;
    NSInteger minutes = floor(currentTime/60);
    NSInteger seconds = trunc(currentTime - minutes * 60);
    self.TimeLabel.text = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];
    NSLog(@"TimeLabel:%@",self.TimeLabel.text);
    double latdouble = [self.TimeLabel.text doubleValue];
    NSLog(@"latdouble: %f", latdouble);
    if (myAudioPlayer.duration == latdouble)
    {
        [_PlayPauseButton setBackgroundImage:[UIImage imageNamed:@"PlayButton.png"] forState:UIControlStateNormal];
    }
}

-(IBAction)onPlayVideo:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int nPos=btn.tag;
    nPos=nPos-35000;
    
    Message *message = [arrWholetableArray objectAtIndex:nPos];
    NSString *strContent= message.text;
    NSArray *components = [strContent componentsSeparatedByString:@"/"];
    NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
    NSURL *movieURL=[NSURL fileURLWithPath:getImagePath];
    
    AVPlayer *player = [AVPlayer playerWithURL:movieURL];
    AVPlayerViewController *playerViewController = [AVPlayerViewController new];
    playerViewController.player = player;
    [playerViewController.player play];//Used to Play On start
    [self presentViewController:playerViewController animated:YES completion:nil];
}
- (void)handleTap:(UITapGestureRecognizer *)tap
{
    if (UIGestureRecognizerStateEnded == tap.state)
    {
        UITableView *tableView = (UITableView *)tap.view;
        CGPoint p = [tap locationInView:tap.view];
        NSIndexPath* indexPath = [tableView indexPathForRowAtPoint:p];
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        MessageCell *cell = (MessageCell *)[self.StarredTblView cellForRowAtIndexPath:indexPath];
        CGPoint pointInCell = [tap locationInView:cell];
        if (CGRectContainsPoint(cell.imgView.frame, pointInCell))
        {
            // user tapped image
            NSInteger rowNumber = 0;
            
            for (NSInteger i = 0; i < indexPath.section; i++) {
                rowNumber += [self tableView:tableView numberOfRowsInSection:i];
            }
            
            rowNumber += indexPath.row;
            Message *msg = [arrWholetableArray objectAtIndex:rowNumber];
            if ([msg.type isEqualToString:@"image"])
            {
                int nSelectedIndex=0;
                arrPhotos=[[NSMutableArray alloc]init];
                for (int i=0; i<arrWholetableArray.count; i++)
                {
                    Message *message = [arrWholetableArray objectAtIndex:i];
                    if ([message.type isEqualToString:@"image"])
                    {
                        if (message.sender==MessageSenderMyself)
                        {
                            MWPhoto *photo = [MWPhoto photoWithImage:message.image];
                            photo.caption = message.caption;
                            [arrPhotos addObject:photo];
                        }
                        else
                        {
                            if (message.status==MessageStatusDownloaded)
                            {
                                MWPhoto *photo = [MWPhoto photoWithImage:message.image];
                                photo.caption = message.caption;
                                [arrPhotos addObject:photo];
                            }
                        }
                        
                        
                        if (rowNumber==i)
                        {
                            nSelectedIndex=arrPhotos.count-1;
                        }
                        
                    }
                }
                MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
                browser.displayActionButton = YES;
                browser.displaySelectionButtons = YES;
                browser.zoomPhotosToFill = NO;
                browser.alwaysShowControls = NO;
                browser.enableGrid = YES;
                browser.startOnGrid = NO;
                browser.hidesBottomBarWhenPushed=YES;
                [browser setCurrentPhotoIndex:nSelectedIndex];
                [self.navigationController pushViewController:browser animated:YES];
                
                
            }
            else if ([msg.type isEqualToString:@"location"])
            {
                strLat=msg.Latitude;
                strLong=msg.Longitude;
                
                if (msg.sender==MessageSenderMyself)
                {
                    strNameToSend=appDelegate.strName;
                    isShowDistance=@"no";
                }
                else
                {
                    strNameToSend=strName;
                    isShowDistance=@"yes";
                }
//                BOOL canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps:"]];
//                
//                if (canHandle) {
//                    // Google maps installed
//                } else {
//                    // Use Apple maps?
//                }
                
                
                
//                if ([[UIApplication sharedApplication] canOpenURL:
//                     [NSURL URLWithString:@"comgooglemaps://"]]) {
//                    [[UIApplication sharedApplication] openURL:
//                     [NSURL URLWithString:@"comgooglemaps://?center=%@,%@",strLat,strLong]];
//                } else {
//                    NSLog(@"Can't use comgooglemaps://");
//                }
                
                BOOL canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"http://maps.google.com/maps?z=8"]];
                if (canHandle)
                {
                    NSLog(@"GoogleMaps");
                    NSString *urlString = [NSString stringWithFormat:@"http://maps.google.com/maps?daddr=%@,%@", strLat,strLong];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
                }
                else {
                    [self performSegueWithIdentifier:@"SingleChat_ToShowLocation" sender:self];
                    
                }
            }
            else if ([msg.type isEqualToString:@"video"])
            {
                NSString *strContent= msg.text;
                NSArray *components = [strContent componentsSeparatedByString:@"/"];
                NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
                NSURL *movieURL=[NSURL fileURLWithPath:getImagePath];
                
                AVPlayer *player = [AVPlayer playerWithURL:movieURL];
                AVPlayerViewController *playerViewController = [AVPlayerViewController new];
                playerViewController.player = player;
                [playerViewController.player play];//Used to Play On start
                [self presentViewController:playerViewController animated:YES completion:nil];
                
            }
        }
        
        
        
    }
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if(![touch.view isMemberOfClass:[UITextField class]]) {
        [touch.view endEditing:YES];
    }
}
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < arrPhotos.count) {
        return [arrPhotos objectAtIndex:index];
    }
    return nil;
}

- (void)onAudioPlay:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int nPos=btn.tag;
    nPos=nPos-51000;
    
    Message *message = [arrWholetableArray objectAtIndex:nPos];
    NSString *strContent= message.text;
    
    NSArray *components = [strContent componentsSeparatedByString:@"/"];
    NSString *strAudioFileName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
    
    
    //[self CreateAudioPreview:strAudioFileName];
    
    [self playAudioFile:strAudioFileName: nPos];
    
    
}
- (void)playAudioFile :(NSString *)fName :(int)tag
{
    //    [myAudioPlayer stop];
    tag=tag+51000;
    strPlayingTag=[NSString stringWithFormat:@"%d",tag]; // 51000
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *getPath = [documentsDirectory stringByAppendingPathComponent:@""];
    NSURL *audioURL = [NSURL fileURLWithPath:getPath];
    
    NSData *audioData = [NSData dataWithContentsOfURL:audioURL];
    NSString *docDirPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", docDirPath , fName];
    [audioData writeToFile:filePath atomically:YES];
    
    NSURL *fileURL = [NSURL fileURLWithPath:filePath];
    
    AVPlayer *player = [AVPlayer playerWithURL:fileURL];
    
    // create a player view controller
    AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
    [self presentViewController:controller animated:YES completion:nil];
    controller.player = player;
    [player play];
    
    
    //       NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    //    NSString *documentsDirectory = [paths objectAtIndex:0];
    //    NSString *getPath = [documentsDirectory stringByAppendingPathComponent:fName];
    //
    //    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:getPath ];
    //
    //    myAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
    //     //myAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:NULL];
    //
    //    self.movingslider.maximumValue = myAudioPlayer.duration;
    //    NSLog(@"%f",myAudioPlayer.duration);
    //
    //
    //    sliderTime = [NSTimer scheduledTimerWithTimeInterval:1.0f
    //                                                  target:self
    //                                                selector:@selector(updateSlider:)
    //                                                userInfo:nil
    //                                                 repeats:YES];
    //    Timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
    //                                             target:self
    //                                           selector:@selector(updateTimer)
    //                                           userInfo:nil
    //                                            repeats:YES];
    //
    //    NSTimeInterval currentTime = myAudioPlayer.currentTime;
    //    NSInteger minutes = floor(currentTime/60);
    //    NSInteger seconds = trunc(currentTime - minutes * 60);
    //    //self.TimeLabel.text = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];
    //    NSLog(@"TimeLabel:%@",[NSString stringWithFormat:@"%d:%02d", minutes, seconds]);
    //
    //
    //    myAudioPlayer.delegate=self;
    //    NSError *sessionError = nil;
    ////    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    ////    [audioSession setCategory:AVAudioSessionCategoryMultiRoute error:&sessionError];
    ////    BOOL success= [audioSession overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&sessionError];
    ////    [audioSession setActive:YES error:&sessionError];
    ////    if(!success)
    ////    {
    ////        NSLog(@"error doing outputaudioportoverride - %@", [sessionError localizedDescription]);
    ////    }
    //    AVAudioSession *session = [AVAudioSession sharedInstance];
    //    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    //   // myAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    ////[myAudioPlayer setDelegate:self];
    //    [myAudioPlayer prepareToPlay];
    //    [myAudioPlayer play];
    ////    [myAudioPlayer setVolume:3.0f];
    ////    [myAudioPlayer play];
    
    
    
}

- (void)downloadDoc:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int nPos=btn.tag;
    nPos=nPos-51000;
    
    Message *msg = [arrWholetableArray objectAtIndex:nPos];
    NSLog(@"%@",msg);
    NSString *getdocument = msg.text;
    NSLog(@"%@",getdocument);
    UIStoryboard *storyboard =
    [UIStoryboard storyboardWithName:@"Main"
                              bundle:[NSBundle mainBundle]];
    LoadDocumentVC *yourViewController =
    [storyboard instantiateViewControllerWithIdentifier:@"LoadDocumentVC"];
    yourViewController.getWebStr = getdocument;
    [self.navigationController pushViewController:yourViewController animated:NO];
    
}
- (void)ShowPage:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int nPos=btn.tag;
    nPos=nPos-91000;
    
    Message *msg = [arrWholetableArray objectAtIndex:nPos];
    NSLog(@"%@",msg);
    NSString *getLink = msg.text;
    NSURL *url = [NSURL URLWithString:getLink];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
    
    
}

-(IBAction)onInvite:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int nPos=btn.tag;
    nPos=nPos-45000;
    
    Message *message = [arrWholetableArray objectAtIndex:nPos];
    NSLog(@"%@",message.contactName);
    NSLog(@"%@",message.contactNo);
    
    MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
    picker.messageComposeDelegate = self;
    picker.recipients = [NSArray arrayWithObjects:message.contactNo, nil];
    picker.body = @"Check out Acedais Messenger for your smartphone. Download it today from ";
    //https://www.pyramidions.com/
    [self presentViewController:picker animated:NO completion:NULL];
    
    
    
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            NSLog(@"Cancelled");
            break;
        case MessageComposeResultFailed:
            
            break;
        case MessageComposeResultSent:
            
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:NO completion:NULL];
}
-(IBAction)onAddNewContact:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int nPos=btn.tag;
    nPos=nPos-48000;
    
    Message *message = [arrWholetableArray objectAtIndex:nPos];
    
    
    
    CNContactStore *store = [[CNContactStore alloc] init];
    
    // create contact
    
    CNMutableContact *contact = [[CNMutableContact alloc] init];
    //  contact.familyName = @"Gopalsamy";
    contact.givenName = message.contactName;
    
    CNLabeledValue *homePhone = [CNLabeledValue labeledValueWithLabel:CNLabelHome value:[CNPhoneNumber phoneNumberWithStringValue:message.contactNo]];
    contact.phoneNumbers = @[homePhone];
    
    CNContactViewController *controller = [CNContactViewController viewControllerForUnknownContact:contact];
    controller.contactStore = store;
    controller.title=@"Add Contact";
    UIBarButtonItem *barBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"BackButton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onBack:)];
    [controller.navigationItem setLeftBarButtonItem:barBtn ];
    controller.delegate = self;
    
    [self.navigationController pushViewController:controller animated:TRUE];
    
    
   }

-(IBAction)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
