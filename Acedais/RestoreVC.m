//
//  RestoreVC.m
//  ZoeChat
//
//  Created by Pyramidions on 07/03/18.
//  Copyright © 2018 Pyramidions Solution. All rights reserved.
//

#import "RestoreVC.h"
#import "DBClass.h"
#import "MainTabBarVC.h"

@interface RestoreVC ()


@end

@implementation RestoreVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.minutesLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"messageTime"];

    
    NSString *msgCount = [NSString stringWithFormat:@"%@ messages received",[[NSUserDefaults standardUserDefaults] objectForKey:@"messageCount"]];
    
    self.messageLabel.text = msgCount;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
//    [UINavigationBar appearance].tintColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SegueHome"])
    {
        MainTabBarVC *lvc = segue.destinationViewController;
        [lvc setHidesBottomBarWhenPushed:YES];
        lvc.navigationItem.hidesBackButton = YES;
    }
}

- (IBAction)nextBtn:(id)sender
{
        [self performSegueWithIdentifier:@"SegueHome" sender:self];
}
@end
