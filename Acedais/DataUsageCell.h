//
//  DataUsageCell.h
//  ZoeChat
//
//  Created by macmini on 06/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataUsageCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *nameLbl;
@property (strong, nonatomic) IBOutlet UILabel *StatusLbl;

@end
