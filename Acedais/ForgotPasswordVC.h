//
//  ForgotPasswordVC.h
//  ZoeChat
//
//  Created by macmini on 13/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordVC : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *otpTxtFld;
@property (strong, nonatomic) IBOutlet UIButton *submitbtn;
- (IBAction)ClickOnSubmit:(id)sender;
- (IBAction)backBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *ForgotPasswordView;
@property (strong, nonatomic) IBOutlet UITextField *passwordTxt;
@property (strong, nonatomic) IBOutlet UITextField *conPasswordTxt;
@property (strong, nonatomic) IBOutlet UIButton *submit;
- (IBAction)submitBtn:(id)sender;
@property (strong,nonatomic)NSString *getChatId;

@end
