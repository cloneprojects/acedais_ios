//
//  SingleProfileVC.m
//  ZoeChat
//
//  Created by Pyramidions Solution on 27/03/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "SingleProfileVC.h"
#import "Constants.h"
#import "UIImageView+WebCache.h"
#import "SMCollectionViewFillLayout.h"
#import "MediaCollectionViewCell.h"
#import "DBClass.h"
#import "MWPhotoBrowser.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "AFNHelper.h"
#import "Constants.h"
#import "CreateGroupContactVC.h"
#import "TOCropViewController.h"
#import "AWSS3TransferUtility.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "CustomAlbum.h"
#import "MainTabBarVC.h"
#import "SingleChatVC.h"


@interface SingleProfileVC ()<SMCollectionViewFillLayoutDelegate, UICollectionViewDelegate, UICollectionViewDataSource, MWPhotoBrowserDelegate,UITableViewDelegate,UITableViewDataSource>
{
    DBClass *db_class;
    NSMutableArray *arrPhotos;
    AppDelegate *appDelegate;
    NSMutableArray *arrParticipants;
    //Check participents
    NSMutableArray *getPartIDs;
    NSURL *assetURL;
    NSString* UploadFileName;
    NSString *isPictureAdded;
    NSString *getgroupId;
    UIBarButtonItem *barBtn;
    UILabel *lblNav;
    NSArray *isAdmindataArray,*isnotAdminDataArray, *updatedAdminArray;
    NSString *ContactName, *passParticipantsId;
    UIView *viewForAudioOrVideo;
    UIView *viewForWhiteBG;
    UIView *viewForAudioOrVideo1;
    UIView *viewForWhiteBG1;
    UIView *viewForAudioOrVideo2;
    UIView *viewForWhiteBG2;
    NSMutableArray *getuserIDs;
    UIBarButtonItem *addnew;
    NSString *admin;
    //Viewcontact
    NSMutableArray *arrChats, *arrForTableView;
    NSMutableArray *arrContacts;
    NSDictionary *getMessageDict;
    NSDictionary *getViewDict;
    
    NSInteger buttonTag;





}
@property (weak, nonatomic) IBOutlet UICollectionView *aCollectionView;
@property (strong, nonatomic) SMCollectionViewFillLayout *aLayout;
@property (copy, nonatomic) NSArray *objects;
@property (weak, nonatomic) IBOutlet UISegmentedControl *directionSegmentedControl;

@property (nonatomic, strong) UIImage *image;           // The image we'll be cropping
@property (nonatomic, strong) UIImageView *imageView;   // The image view to present the cropped image

@property (nonatomic, assign) TOCropViewCroppingStyle croppingStyle; //The cropping style
@property (nonatomic, assign) CGRect croppedFrame;
@property (nonatomic, assign) NSInteger angle;
@property NSString*isAdmin;


- (void)setup;

- (void)setupLayout;
- (IBAction)directionChanged:(id)sender;

@end

@implementation SingleProfileVC
@synthesize dictDetails;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    db_class=[[DBClass alloc]init];
    
    arrChats=[[NSMutableArray alloc]init];
    arrContacts=[[NSMutableArray alloc]init];
    
    arrForTableView=[[NSMutableArray alloc]init];
    NSLog(@"%@",dictDetails);
    [self.Initiallbl setHidden:YES];
    if (dictDetails.count==nil)
    {
                [self.editImgBtn setHidden:YES];
    }
    getuserIDs=[[NSMutableArray alloc]init];
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSLog(@"%@",appDelegate.ViewDict);
    NSLog(@"%@",appDelegate.isfromView);
    
    _viewEnterNewSubject.hidden=YES;
    _btnEdit.hidden=YES;
    UIFont *fontname18 = [UIFont fontWithName:FONT_MEDIUM size:16];
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        
        self.lblStatusAndPhone.textColor=color;
        self.mediaLbl.textColor=color;
        [self.btnEdit setImage:[UIImage imageNamed:@"editButton.png"] forState:UIControlStateNormal];
        self.btnEdit.tintColor=color;
        self.txtNewSubject.tintColor=color;
        
    }
    else
    {
        self.lblStatusAndPhone.textColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.mediaLbl.textColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.btnEdit.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];;
        self.txtNewSubject.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        
    }

    barBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"BackButton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onBack)];
    [self.navigationItem setLeftBarButtonItem:barBtn ];
    
    lblNav=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-barBtn.width, 30)];
    lblNav.textColor=[UIColor whiteColor];
    lblNav.backgroundColor=[UIColor clearColor];
    [lblNav setFont:fontname18];
    if ([appDelegate.isfromView isEqualToString:@"Yes"])
    {
        NSString *strName=[NSString stringWithFormat:@"%@",[appDelegate.ViewDict valueForKey:@"name"]];
        
        if (![strName isEqualToString:@"(null)"])
        {
            lblNav.text=strName;
        }
        else
        {
            lblNav.text=[NSString stringWithFormat:@"+%@",[appDelegate.ViewDict valueForKey:@"chatRoomId"]];
        }
        
        
        
        lblNav.textAlignment=NSTextAlignmentLeft;
        self.navigationItem.titleView=lblNav;
        self.navigationController.navigationBar.translucent = YES;
        
        NSString *strImage=[dictDetails valueForKey:@"image"];
        
        if (![strImage isEqualToString:@"(null)"])
        {
            
            
            if(strImage.length==0)
            {
                NSString *strImage=[appDelegate.ViewDict valueForKey:@"image"];
                
                [_imgProfile sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"clear.png"]];
            }
            
            else
            {
                
                [_imgProfile sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"clear.png"]];
            }

        }
        else
        {
            
            [self.Initiallbl setHidden:NO];

            //lblImg
            NSString *InitialStr;
            UIColor *bgColor;
            if (![strName isEqualToString:@"(null)"])
            {
                InitialStr=[strName substringToIndex:1];
                if ([InitialStr isEqualToString:@"A"])
                {
                    bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"B"])
                {
                    bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"C"])
                {
                    bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"D"])
                {
                    bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"E"])
                {
                    bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"F"])
                {
                    bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"G"])
                {
                    bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"H"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"I"])
                {
                    bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"J"])
                {
                    bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"K"])
                {
                    bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"L"])
                {
                    bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"M"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"N"])
                {
                    bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"O"])
                {
                    bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"P"])
                {
                    bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"Q"])
                {
                    bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"R"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"S"])
                {
                    bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"T"])
                {
                    bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"U"])
                {
                    bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"V"])
                {
                    bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"W"])
                {
                    bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"X"])
                {
                    bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"Y"])
                {
                    bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"Z"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
                }
                
            }
            else
            {
                strName=[NSString stringWithFormat:@"+%@",[appDelegate.ViewDict valueForKey:@"chatRoomId"]];
                InitialStr=[strName substringToIndex:1];
                if ([InitialStr isEqualToString:@"0"])
                {
                    bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"1"])
                {
                    bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"2"])
                {
                    bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"3"])
                {
                    bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"4"])
                {
                    bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"5"])
                {
                    bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"6"])
                {
                    bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"7"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"8"])
                {
                    bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"9"])
                {
                    bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
                }
                
            }
            self.Initiallbl.text=InitialStr;
            self.Initiallbl.backgroundColor=bgColor;
//            _imgProfile.image=[UIImage imageNamed:@"clear.png"];
        }
        
        
        
        
        [_ViewForMedia.layer setCornerRadius:3.0f];
        [_ViewForMedia.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [_ViewForMedia.layer setBorderWidth:0.5f];
        [_ViewForMedia.layer setShadowColor:[UIColor colorWithRed:225.0/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1.0].CGColor];
        [_ViewForMedia.layer setShadowOpacity:1.0];
        [_ViewForMedia.layer setShadowRadius:3.0];
        [_ViewForMedia.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
        
        [self setup];
        
        NSString *StrZoe=[NSString stringWithFormat:@"%@",[appDelegate.ViewDict valueForKey:@"zoechatid"]];
        
        if ([StrZoe isEqualToString:@"(null)"])
        {
            StrZoe=[NSString stringWithFormat:@"%@",[appDelegate.ViewDict valueForKey:@"sentBy"]];
        }
        
        _lblMobileNo.text=[NSString stringWithFormat:@"%@",StrZoe];
        
        NSDictionary *getUserDict = [[NSDictionary alloc]init];
        //appDelegate.isContact=@"yes";
        NSLog(@"getUserDict: %@", getUserDict);
        if ([appDelegate.isContact isEqualToString:@"yes"])
        {
            getUserDict = [db_class getUserInfo:[appDelegate.ViewDict valueForKey:@"zoechatid"]];
            
        }
        else{
            getUserDict = [db_class getUserInfo:[appDelegate.ViewDict valueForKey:@"chatRoomId"]];
        }
        NSString *getStatus = [getUserDict valueForKey:@"status"];
        
        if ([[appDelegate.ViewDict valueForKey:@"chatRoomType"] isEqualToString:@"0"])
        {
            _objects=[db_class getOneToOneChatMessagesForMedia:StrZoe];
            _lblStatus.text = getStatus;
            [self.editImgBtn setHidden:YES];
        }
        else if ([[dictDetails valueForKey:@"chatRoomType"]isEqualToString:@"(null)"])
        {
            _objects=[db_class getOneToOneChatMessagesForMedia:StrZoe];
            _lblStatus.text = getStatus;
            [self.editImgBtn setHidden:YES];
        }

        else
        {
            _objects=[db_class getGroupChatMessagesForMedia:StrZoe];
            
        }
        
        
        NSLog(@"%@",_objects);
        if (_objects.count==0)
        {
            _lblNoMediaFiles.hidden=NO;
            _aCollectionView.hidden=YES;
        }
        else
        {
            _lblNoMediaFiles.hidden=YES;
            _aCollectionView.hidden=NO;
            _aCollectionView.dataSource=self;
            _aCollectionView.delegate=self;
        }
        
        CGRect contentRect = CGRectZero;
        for (UIView *view in self.scrollView.subviews) {
            contentRect = CGRectUnion(contentRect, view.frame);
        }
        self.scrollView.contentSize = CGSizeMake(contentRect.size.width, contentRect.size.height+10);
    }
    else if ([appDelegate.isfromView isEqualToString:@"No"]) {
        
        
        NSString *strName=[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"name"]];
        
        if (![strName isEqualToString:@"(null)"])
        {
            lblNav.text=strName;
        }
        else
        {
                     lblNav.text=[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"chatRoomId"]];
        }
        
        
        
        lblNav.textAlignment=NSTextAlignmentLeft;
        self.navigationItem.titleView=lblNav;
        self.navigationController.navigationBar.translucent = YES;
        
        NSString *strImage=[dictDetails valueForKey:@"image"];
        
        if (![strImage isEqualToString:@"(null)"])
        {
            [_imgProfile sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"clear.png"]];
        }
        else
        {
            
            [self.Initiallbl setHidden:NO];
            
            //lblImg
            NSString *InitialStr;
            UIColor *bgColor;
            if (![strName isEqualToString:@"(null)"])
            {
                InitialStr=[strName substringToIndex:1];
                if ([InitialStr isEqualToString:@"A"])
                {
                    bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"B"])
                {
                    bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"C"])
                {
                    bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"D"])
                {
                    bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"E"])
                {
                    bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"F"])
                {
                    bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"G"])
                {
                    bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"H"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"I"])
                {
                    bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"J"])
                {
                    bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"K"])
                {
                    bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"L"])
                {
                    bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"M"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"N"])
                {
                    bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"O"])
                {
                    bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"P"])
                {
                    bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"Q"])
                {
                    bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"R"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"S"])
                {
                    bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"T"])
                {
                    bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"U"])
                {
                    bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"V"])
                {
                    bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"W"])
                {
                    bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"X"])
                {
                    bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"Y"])
                {
                    bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"Z"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
                }
                
            }
            else
            {
                strName=[NSString stringWithFormat:@"+%@",[appDelegate.ViewDict valueForKey:@"chatRoomId"]];
                InitialStr=[strName substringToIndex:1];
                if ([InitialStr isEqualToString:@"0"])
                {
                    bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"1"])
                {
                    bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"2"])
                {
                    bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"3"])
                {
                    bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"4"])
                {
                    bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"5"])
                {
                    bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"6"])
                {
                    bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"7"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"8"])
                {
                    bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"9"])
                {
                    bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
                }
                
            }
            self.Initiallbl.text=InitialStr;
            self.Initiallbl.backgroundColor=bgColor;
//            _imgProfile.image=[UIImage imageNamed:@"clear.png"];
        }
        
        
        
        
        [_ViewForMedia.layer setCornerRadius:3.0f];
        [_ViewForMedia.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [_ViewForMedia.layer setBorderWidth:0.5f];
        [_ViewForMedia.layer setShadowColor:[UIColor colorWithRed:225.0/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1.0].CGColor];
        [_ViewForMedia.layer setShadowOpacity:1.0];
        [_ViewForMedia.layer setShadowRadius:3.0];
        [_ViewForMedia.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
        
        [self setup];
        
        NSString *StrZoe=[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"zoechatid"]];
        
        if ([StrZoe isEqualToString:@"(null)"])
        {
            StrZoe=[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"chatRoomId"]];
        }
        
            _lblMobileNo.text=[NSString stringWithFormat:@"%@",StrZoe];
        
        NSDictionary *getUserDict = [[NSDictionary alloc]init];
        //appDelegate.isContact=@"yes";
        if ([appDelegate.isContact isEqualToString:@"yes"])
        {
            getUserDict = [db_class getUserInfo:[dictDetails valueForKey:@"zoechatid"]];
            
        }
        else{
            getUserDict = [db_class getUserInfo:[dictDetails valueForKey:@"chatRoomId"]];
        }
        NSString *getStatus = [getUserDict valueForKey:@"status"];
        
        if ([[dictDetails valueForKey:@"chatRoomType"] isEqualToString:@"0"])
        {
            _objects=[db_class getOneToOneChatMessagesForMedia:StrZoe];
            _lblStatus.text = getStatus;
            [self.editImgBtn setHidden:YES];
        }
        else if ([[dictDetails valueForKey:@"chatRoomType"]isEqualToString:@"(null)"])
        {
            _objects=[db_class getOneToOneChatMessagesForMedia:StrZoe];
            _lblStatus.text = getStatus;
            [self.editImgBtn setHidden:YES];
        }
        
        
        else
        {
            _objects=[db_class getGroupChatMessagesForMedia:StrZoe];
            
        }
        
        
        NSLog(@"%@",_objects);
        if (_objects.count==0)
        {
            _lblNoMediaFiles.hidden=NO;
            _aCollectionView.hidden=YES;
        }
        else
        {
            _lblNoMediaFiles.hidden=YES;
            _aCollectionView.hidden=NO;
            _aCollectionView.dataSource=self;
            _aCollectionView.delegate=self;
        }
        
        CGRect contentRect = CGRectZero;
        for (UIView *view in self.scrollView.subviews) {
            contentRect = CGRectUnion(contentRect, view.frame);
        }
        self.scrollView.contentSize = CGSizeMake(contentRect.size.width, contentRect.size.height+10);
        
        
        
        if ([[dictDetails valueForKey:@"chatRoomType"] isEqualToString:@"1"])
        {
            [self.Initiallbl setHidden:YES];

            NSString *strImage=[dictDetails valueForKey:@"groupImage"];
            [self.editImgBtn setHidden:NO];
            
            if (![strImage isEqualToString:@"(null)"])
            {
                [_imgProfile sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"grp.png"]];
            }
            else
            {
                _imgProfile.image=[UIImage imageNamed:@"grp.png"];
            }
            
            lblNav.text=[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"groupName"]];
            _lblStatusAndPhone.text=@"Group Info";
            _lblStatus.text=[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"groupName"]];
            //checkAdmin to Add participants
//            NSArray *arrdata=[db_class getParticipants:[dictDetails valueForKey:@"chatRoomId"]];
//            for (int i=0; i<arrdata.count; i++)
//            {
//                NSDictionary *dictParti=[arrdata objectAtIndex:i];
//                
//                NSString *getParticipentID=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"userId"]];
//                NSString *strgrpID=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"groupId"]];
//                NSString *CheckAdmin=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"isAdmin"]];
//                
//                if ([strgrpID isEqualToString:[dictDetails valueForKey:@"chatRoomId"]])
//                {
//                    if([getParticipentID isEqualToString:appDelegate.strID])
//                    {
//                        if ([CheckAdmin isEqualToString:@"1"])
//                        {
//                             UIBarButtonItem *barBtn1 = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"addNew.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onAddParticipants)];
//                            [self.navigationItem setRightBarButtonItem:barBtn1];
//
//                        }
//                    }
//                    
//                }
//            }
            

            
            
            
         
            
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setValue:[dictDetails valueForKey:@"chatRoomId"] forKey:@"groupId"];
            
            MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDAnimationFade;
            hud.label.text = @"Loading";
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_GET_PARTICIPANTS withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                 if (response)
                 {
                     //   NSLog(@"Response: %@",response);
                     if ([[response valueForKey:@"error"] boolValue]==false)
                     {
                         NSDictionary *dictGroupDetails=[response valueForKey:@"groupDetails"];
                         NSString *strCreatedBy=[NSString stringWithFormat:@"%@",[dictGroupDetails valueForKey:@"createdBy"]];
                         NSDictionary *dictName=[db_class getUserNameForNotification:strCreatedBy];
                         getgroupId=[NSString stringWithFormat:@"%@",[dictGroupDetails valueForKey:@"groupId"]];
                         
                         BOOL isAvailinChatsTable=NO;
                         isAvailinChatsTable=[db_class doesGroupExist:getgroupId];
                         if (isAvailinChatsTable==NO)
                         {
                             if ([[dictName allKeys] containsObject:@"name"])
                             {
                                 strCreatedBy=[NSString stringWithFormat:@"%@",[dictName valueForKey:@"name"]];
                             }
                             else
                             {
                                 strCreatedBy=[NSString stringWithFormat:@"%@",[dictGroupDetails valueForKey:@"createdBy"]];
                             }
                             NSString *strMine=[NSString stringWithFormat:@"%@",appDelegate.strID];
                             if ([strCreatedBy isEqualToString:strMine])
                             {
                                 strCreatedBy=@"you";
                             }
                             
                             
                             
                             NSDate *createdDate= [self convertMillisecondsToDate:[[dictGroupDetails valueForKey:@"createdAt"]longLongValue]];
                             
                             NSDateFormatter *df = [[NSDateFormatter alloc] init]; //Will release itself for us
                             [df setDateFormat:@"dd-MM-yyyy"];
                             NSString *strDatee = [df stringFromDate:createdDate] ; //Simplified the code
                             
                             
                             _lblMobileNo.text=[NSString stringWithFormat:@"Created by %@, %@",strCreatedBy, strDatee];
                             _lblSmallMobile.hidden=YES;
                             _btnEdit.hidden=NO;
                             [arrParticipants removeAllObjects];
                             NSLog(@"%@",arrParticipants);
                             arrParticipants=[response valueForKey:@"participants"];
                             
                             BOOL success2 = [db_class deleteGroupParticipant:getgroupId];

                             for (int i=0; i<arrParticipants.count; i++)
                             {
                                 NSString *getuserId = [arrParticipants valueForKey:@"participantId"][i];
                                 NSString *getaddedby = [arrParticipants valueForKey:@"addedBy"][i];
                                 getgroupId = [arrParticipants valueForKey:@"groupId"][i];
                                 NSString *getJoinedat = [arrParticipants valueForKey:@"joinedAt"][i];
                                 NSString *getAdmin = [arrParticipants valueForKey:@"isAdmin"][i];
                                 admin = getAdmin;
                                 NSLog(@"getAdmin:%@",getAdmin);
                                 self.isAdmin = getAdmin;
                                 BOOL success = [db_class insertParticipants:@"" userId:getuserId groupId:getgroupId joinedAt:getJoinedat addedBy:getaddedby isAdmin:getAdmin];
                             }
                              [self showParticipants];
//                             if ([self.isAdmin isEqualToString:@"1"]) {
//                            [self showParticipants];
//                             }
//                             else{
////                               [self showParticipants];
//                             }
                         }
                         
                         else
                         {
                             
                             
                             if ([[dictName allKeys] containsObject:@"name"])
                             {
                                 strCreatedBy=[NSString stringWithFormat:@"%@",[dictName valueForKey:@"name"]];
                             }
                             else
                             {
                                 strCreatedBy=[NSString stringWithFormat:@"%@",[dictGroupDetails valueForKey:@"createdBy"]];
                             }
                             NSString *strMine=[NSString stringWithFormat:@"%@",appDelegate.strID];
                             if ([strCreatedBy isEqualToString:strMine])
                             {
                                 strCreatedBy=@"you";
                             }
                             NSDate *createdDate= [self convertMillisecondsToDate:[[dictGroupDetails valueForKey:@"createdAt"]longLongValue]];
                             
                             NSDateFormatter *df = [[NSDateFormatter alloc] init]; //Will release itself for us
                             [df setDateFormat:@"dd-MM-yyyy"];
                             NSString *strDatee = [df stringFromDate:createdDate] ; //Simplified the code
                             
                             
                             _lblMobileNo.text=[NSString stringWithFormat:@"Created by %@, %@",strCreatedBy, strDatee];
                             _lblSmallMobile.hidden=YES;
                             _btnEdit.hidden=NO;
                             [arrParticipants removeAllObjects];
                             NSLog(@"%@",arrParticipants);

                             arrParticipants=[response valueForKey:@"participants"];
                             BOOL success2 = [db_class deleteGroupParticipant:getgroupId];
                           //  BOOL success2 = [db_class deleteParticipant:userId];

                             NSString *getuserId;
                             NSString *getaddedby;
                             NSString *getJoinedat;
                             NSString *getAdmin;
                             for (int i=0; i<arrParticipants.count; i++)
                             {
                                 getuserId = [arrParticipants valueForKey:@"participantId"][i];
                                 getaddedby = [arrParticipants valueForKey:@"addedBy"][i];
                                 getgroupId = [arrParticipants valueForKey:@"groupId"][i];
                                 getJoinedat = [arrParticipants valueForKey:@"joinedAt"][i];
                                 getAdmin = [arrParticipants valueForKey:@"isAdmin"][i];
                                 [getuserIDs addObject:getuserId];
                                 admin = getAdmin;
                                NSLog(@"getAdmin:%@",getAdmin);
                                 
                                 NSString *checkuserId=[db_class participant:getgroupId];
                                 //BOOL success2 = [db_class deleteParticipant:getuserId];

                                 if (appDelegate.isPart==nil)
                                 {
                                     NSLog(@"%@",checkuserId);
                                     
                                         if ([getuserId isEqualToString:checkuserId])
                                         {
                                             
                                         }
                                         else
                                         {
                                             BOOL success = [db_class insertParticipants:@"" userId:getuserId groupId:getgroupId joinedAt:getJoinedat addedBy:getaddedby isAdmin:getAdmin];
                                         }
                                     
                                 }
                                 else
                                 {
                                     if ([getuserId isEqualToString:appDelegate.isPart])
                                     {
                                         BOOL success = [db_class insertParticipants:@"" userId:getuserId groupId:getgroupId joinedAt:getJoinedat addedBy:getaddedby isAdmin:getAdmin];
                                         appDelegate.isPart=@"";
                                     }
                                     else
                                     {
                                         BOOL success = [db_class insertParticipants:@"" userId:getuserId groupId:getgroupId joinedAt:getJoinedat addedBy:getaddedby isAdmin:getAdmin];
                                     }
                                 }
                             
                             }
                             [self showParticipants];
//
//                             if ([self.isAdmin isEqualToString:@"1"]) {
//                                 [self showParticipants];
//                             }
//                             else{
//                                 //                               [self showParticipants];
//                             }
                            
                         }
                         
                     }
                     
                     else
                     {
                         
                         MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                         
                         // Configure for text only and offset down
                         hud.mode = MBProgressHUDModeText;
                         hud.label.text = [NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                         hud.margin = 10.f;
                         hud.yOffset = 150.f;
                         hud.removeFromSuperViewOnHide = YES;
                         [hud hideAnimated:YES afterDelay:3];
                         
                         
                     }
                 }
                 
             }];
            
            
        }
        
        NSLog(@"Adminstatus: %@",admin);
        if ([admin isEqualToString:@"1"]);{
            addnew = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"addNew.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onAddParticipants)];
            [self.navigationItem setRightBarButtonItem:addnew ];
        }
        
        
        //Broadcast
        if ([[dictDetails valueForKey:@"chatRoomType"] isEqualToString:@"2"])
        {
            [self.Initiallbl setHidden:YES];

            NSString *strImage=[dictDetails valueForKey:@"groupImage"];
            [self.editImgBtn setHidden:YES];
            
            if (![strImage isEqualToString:@"(null)"])
            {
                [_imgProfile sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"megaphone.png"]];
            }
            else
            {
                _imgProfile.image=[UIImage imageNamed:@"megaphone.png"];
            }
            
            lblNav.text=[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"groupName"]];
            _lblStatusAndPhone.text=@"Group Info";
            _lblStatus.text=[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"groupName"]];
            
            UIBarButtonItem *barBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"addNew.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onAddParticipants)];
            [self.navigationItem setRightBarButtonItem:barBtn ];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setValue:[dictDetails valueForKey:@"chatRoomId"] forKey:@"groupId"];
            getgroupId=[dictDetails valueForKey:@"chatRoomId"];
            //            MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            //            hud.mode = MBProgressHUDAnimationFade;
            //            hud.label.text = @"Loading";
            
            
            _lblMobileNo.text=[NSString stringWithFormat:@"Created by You"];
            _lblSmallMobile.hidden=YES;
            _btnEdit.hidden=NO;
            
            
            [self BroadcastParticipants];
            
            
        }
    }
    
   
    
    appDelegate.isfromView=@"No";
    [appDelegate.ViewDict removeAllObjects];
    
}
-(void)BroadcastParticipants
{
    NSString *CheckAdmin;
    NSString *strName,*getStrName, *getParticipentID;
    int yPos=self.ViewForMedia.frame.origin.y + self.ViewForMedia.frame.size.height +20;
    NSArray *arrdata=[db_class getParticipants:getgroupId];
    
    UILabel *lblParti=[[UILabel alloc]initWithFrame:CGRectMake(16, yPos, self.view.frame.size.width-32, 20)];
    lblParti.text=[NSString stringWithFormat:@"Participants: %lu",(unsigned long)arrdata.count];
    lblParti.font=[UIFont fontWithName:FONT_MEDIUM size:14];
    [self.scrollView addSubview:lblParti];
    yPos+=30;
    
    for (int i=0; i<arrdata.count; i++)
    {
        NSDictionary *dictParti=[arrdata objectAtIndex:i];
        
        
        NSDictionary *dictInfo =[db_class getUserInfo:[dictParti valueForKey:@"userId"]];
        getParticipentID=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"userId"]];
        strName=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"name"]];
        NSString *strImage=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"image"]];
        NSString *strStatus=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"status"]];
        CheckAdmin=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"isAdmin"]];
        
        
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(16, yPos, 50, 50)];
        imgView.contentScaleFactor = UIViewContentModeScaleAspectFill;
        imgView.layer.cornerRadius = CGRectGetHeight(imgView.frame) / 2;
        imgView.clipsToBounds=YES;
        UILabel *initialLbl = [[UILabel alloc]initWithFrame:CGRectMake(16, yPos, 50, 50)];
        initialLbl.contentScaleFactor = UIViewContentModeScaleAspectFill;
        initialLbl.layer.cornerRadius = CGRectGetHeight(initialLbl.frame) / 2;
        initialLbl.clipsToBounds=YES;
        initialLbl.textAlignment=NSTextAlignmentCenter;
        initialLbl.textColor=[UIColor whiteColor];
        [initialLbl setHidden:YES];
        if (![strImage isEqualToString:@"(null)"])
        {
            [imgView sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"clear.png"]];
        }
        else
        {
            [initialLbl setHidden:NO];
        
        //lblImg
        NSString *InitialStr;
        UIColor *bgColor;
        if (![strName isEqualToString:@"(null)"])
        {
            
            NSString *getId=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"zoechatid"]];
            if ([getId isEqualToString:appDelegate.strID])
            {
                
                getStrName=@"You";
                [initialLbl setHidden:YES];
                if (![appDelegate.strProfilePic isEqualToString:@"(null)"])
                {
                     [imgView sd_setImageWithURL:[NSURL URLWithString:appDelegate.strProfilePic] placeholderImage:[UIImage imageNamed:@"clear.png"]];
                }
                else
                {
                    [initialLbl setHidden:NO];
                    initialLbl.text=@"Y";
                    initialLbl.backgroundColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
                   

                }
            }
            else
            {
                getStrName=strName;
            }

            
            
            InitialStr=[getStrName substringToIndex:1];
            if ([InitialStr isEqualToString:@"A"])
            {
                bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"B"])
            {
                bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"C"])
            {
                bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"D"])
            {
                bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"E"])
            {
                bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"F"])
            {
                bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"G"])
            {
                bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"H"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"I"])
            {
                bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"J"])
            {
                bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"K"])
            {
                bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"L"])
            {
                bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"M"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"N"])
            {
                bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"O"])
            {
                bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"P"])
            {
                bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"Q"])
            {
                bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"R"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"S"])
            {
                bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"T"])
            {
                bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"U"])
            {
                bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"V"])
            {
                bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"W"])
            {
                bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
            }else if ([InitialStr isEqualToString:@"X"])
            {
                bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"Y"])
            {
                bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"Z"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
            }
            
        }
        else
        {
            
            
            NSString *getId=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"userId"]];
            
            if ([getId isEqualToString:appDelegate.strID])
            {
                
                getStrName=@"You";
                [initialLbl setHidden:YES];

                if (![appDelegate.strProfilePic isEqualToString:@"(null)"])
                {
                    [imgView sd_setImageWithURL:[NSURL URLWithString:appDelegate.strProfilePic] placeholderImage:[UIImage imageNamed:@"clear.png"]];
                }
                else
                {
                    [initialLbl setHidden:NO];
                    initialLbl.text=@"Y";
                    initialLbl.backgroundColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
                    
                    
                }
            }
            
            else
            {
                NSString *trimmString=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"userId"]];
                NSCharacterSet *unwantedChars = [NSCharacterSet characterSetWithCharactersInString:@"userId""\"[]""{}"":"];
                getStrName = [[trimmString componentsSeparatedByCharactersInSet:unwantedChars] componentsJoinedByString: @""];
            }
            InitialStr=[getStrName substringToIndex:1];
            if ([InitialStr isEqualToString:@"0"])
            {
                bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"1"])
            {
                bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"2"])
            {
                bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"3"])
            {
                bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"4"])
            {
                bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"5"])
            {
                bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"6"])
            {
                bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"7"])
            {
                bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"8"])
            {
                bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
            }
            else if ([InitialStr isEqualToString:@"9"])
            {
                bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
            }
            
        }
        initialLbl.text=InitialStr;
        initialLbl.backgroundColor=bgColor;
        //            _imgProfile.image=[UIImage imageNamed:@"clear.png"];
        }
    
    

        [self.scrollView addSubview:imgView];
        [self.scrollView addSubview:initialLbl];
    
        UILabel *lblName=[[UILabel alloc]initWithFrame:CGRectMake(75, yPos+3, self.view.frame.size.width-100, 20)];
        if([CheckAdmin isEqualToString:@"1"])
        {
            UILabel *AdminLbl=[[UILabel alloc]initWithFrame:CGRectMake(200, yPos+3, 80, 20)];
            AdminLbl.text = @"Group Admin";
            AdminLbl.textAlignment = NSTextAlignmentCenter;
            AdminLbl.textColor=[[UIColor alloc] initWithRed:38/255.f green:186/255.f blue:154/255.f alpha:1];
            [AdminLbl.layer setBorderColor:[UIColor colorWithRed:38.0/255.0 green:186.0/255.0 blue:154.0/255.0 alpha:1.0].CGColor];
            [[AdminLbl layer] setCornerRadius:5]; // THIS IS THE RELEVANT LINE
            [AdminLbl.layer setMasksToBounds:YES];
            [AdminLbl.layer setBorderWidth:1.0];
            AdminLbl.font=[UIFont fontWithName:FONT_NORMAL size:10];
            //[lblName addSubview:AdminLbl];
            [self.scrollView addSubview:AdminLbl];
            
            if (![strName isEqualToString:@"(null)"])
            {
                lblName.text=strName;
                getStrName=lblName.text;
            }
            else
            {
                
                if ([[dictParti valueForKey:@"userId"]isEqual:[NSNull null]])
                {
                    NSLog(@"NULL");
                }
                else{
                    if ([[dictParti valueForKey:@"userId"]isEqualToString:appDelegate.strID])
                    {
                        lblName.text=@"You";
                        getStrName=lblName.text;
                        strStatus=appDelegate.strStatus;
                        [imgView sd_setImageWithURL:[NSURL URLWithString:appDelegate.strProfilePic] placeholderImage:[UIImage imageNamed:@"clear.png"]];
                    }
                    else
                    {
                        NSString *trimmString=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"userId"]];
                        NSCharacterSet *unwantedChars = [NSCharacterSet characterSetWithCharactersInString:@"userId""\"[]""{}"":"];
                        NSString *requiredString = [[trimmString componentsSeparatedByCharactersInSet:unwantedChars] componentsJoinedByString: @""];
                        NSLog(@"%@",requiredString);
                        lblName.text=[NSString stringWithFormat:@"%@",requiredString];
                        getStrName=lblName.text;
                        strStatus=@"";
                    }
                }
            }
        }
        else
        {
            
            if (![strName isEqualToString:@"(null)"])
            {
                lblName.text=strName;
                getStrName=lblName.text;
            }
            else
            {
                
                if ([[dictParti valueForKey:@"userId"]isEqual:[NSNull null]])
                {
                    NSLog(@"NULL");
                }
                else{
                    if ([[dictParti valueForKey:@"userId"]isEqualToString:appDelegate.strID])
                    {
                        lblName.text=@"You";
                        getStrName=lblName.text;
                        strStatus=appDelegate.strStatus;
                        [imgView sd_setImageWithURL:[NSURL URLWithString:appDelegate.strProfilePic] placeholderImage:[UIImage imageNamed:@"clear.png"]];
                    }
                    else
                    {
                        NSString *trimmString=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"userId"]];
                        NSCharacterSet *unwantedChars = [NSCharacterSet characterSetWithCharactersInString:@"userId""\"[]""{}"":"];
                        NSString *requiredString = [[trimmString componentsSeparatedByCharactersInSet:unwantedChars] componentsJoinedByString: @""];
                        NSLog(@"%@",requiredString);
                        lblName.text=[NSString stringWithFormat:@"%@",requiredString];
                        getStrName=lblName.text;
                        strStatus=@"";
                    }
                }
            }
        }
        lblName.font=[UIFont fontWithName:FONT_NORMAL size:14];
        [self.scrollView addSubview:lblName];
        
        UILabel *lblStatus=[[UILabel alloc]initWithFrame:CGRectMake(75, yPos+27, self.view.frame.size.width-100, 20)];
        lblStatus.text=strStatus;
        lblStatus.font=[UIFont fontWithName:FONT_NORMAL size:14];
        [self.scrollView addSubview:lblStatus];
        
        
        UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake(75, yPos+3, self.view.frame.size.width-100, 40)];
        [button addTarget:self action:@selector(CheckAdmin:) forControlEvents:UIControlEventTouchUpInside];
        button.accessibilityHint=CheckAdmin;
        button.accessibilityValue=getStrName;
        button.accessibilityIdentifier=getParticipentID;
        buttonTag=i;
        button.tag=buttonTag;
        [self.scrollView addSubview:button];
        
        
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(16,yPos+51, self.view.frame.size.width-32, 1)];
        lineView.backgroundColor = [UIColor lightGrayColor];
        [self.scrollView addSubview:lineView];
        
        yPos+=55;
        NSString *getpartidstr = [dictParti valueForKey:@"userId"];
        [getPartIDs addObject:getpartidstr];
        
    }
    NSLog(@"%@",getPartIDs);
    
    //Exit
    UIView *ExitView = [[UIView alloc] initWithFrame:CGRectMake(16,yPos, self.view.frame.size.width-32, 60)];
    
    [ExitView.layer setCornerRadius:3.0f];
    [ExitView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [ExitView.layer setBorderWidth:0.5f];
    [ExitView.layer setShadowColor:[UIColor colorWithRed:225.0/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1.0].CGColor];
    [ExitView.layer setShadowOpacity:1.0];
    [ExitView.layer setShadowRadius:3.0];
    [ExitView.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
    
    
    UIButton *exitBtn=[[UIButton alloc]initWithFrame:CGRectMake(78, 0,265, 60)];
    [exitBtn setBackgroundColor:[UIColor clearColor]];
    [exitBtn addTarget:self action:@selector(ExitGroup) forControlEvents:UIControlEventTouchDown];
    [ExitView addSubview:exitBtn];
    
    UILabel *exitLbl=[[UILabel alloc]initWithFrame:CGRectMake(78, 0, 265, 60)];
    [exitLbl setBackgroundColor:[UIColor clearColor]];
    [exitLbl setText:@"Delete Broadcast List"];
    [exitLbl setTextColor:[UIColor redColor]];
    [exitLbl setFont: [exitLbl.font fontWithSize: 17.0]];
    //exitLbl.textAlignment = NSTextAlignmentCenter;
    [ExitView addSubview:exitLbl];
    
    UIImageView *logoutImg = [[UIImageView alloc] initWithFrame:CGRectMake(19, 15, 30, 30)];
    logoutImg.image = [UIImage imageNamed:@"logout.png"];
    [logoutImg setTintColor:[UIColor redColor]];
    [ExitView addSubview:logoutImg];
    
    
    [self.scrollView addSubview:ExitView];
    
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.scrollView.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    self.scrollView.contentSize = CGSizeMake(contentRect.size.width, yPos+60);
}

-(long long)convertDateToMilliseconds:(NSDate *)date
{
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    long long milliseconds=timeInSeconds*1000;
    return milliseconds;
}
-(NSDate *)convertMillisecondsToDate:(long long)milliSeconds
{
    NSDate *dat = [NSDate dateWithTimeIntervalSince1970:(milliSeconds / 1000.0)];
    return dat;
}
-(void)viewWillAppear:(BOOL)animated
{
     appDelegate.strGroupIdToAddParticipants=@"";
    
//  
//    if ([appDelegate.isfromView isEqualToString:@"Yes"])
//    {
//        NSString *strName=[NSString stringWithFormat:@"%@",[appDelegate.ViewDict valueForKey:@"name"]];
//        
//        if (![strName isEqualToString:@"(null)"])
//        {
//            lblNav.text=strName;
//        }
//        else
//        {
//            lblNav.text=[NSString stringWithFormat:@"+%@",[appDelegate.ViewDict valueForKey:@"chatRoomId"]];
//        }
//        
//        
//        
//        lblNav.textAlignment=NSTextAlignmentLeft;
//        self.navigationItem.titleView=lblNav;
//        self.navigationController.navigationBar.translucent = YES;
//        
//        NSString *strImage=[dictDetails valueForKey:@"image"];
//        
//        if (![strImage isEqualToString:@"(null)"])
//        {
//            [_imgProfile sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"userImage.png"]];
//        }
//        else
//        {
//            _imgProfile.image=[UIImage imageNamed:@"userImage.png"];
//        }
//        
//        
//        
//        
//        [_ViewForMedia.layer setCornerRadius:3.0f];
//        [_ViewForMedia.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//        [_ViewForMedia.layer setBorderWidth:0.5f];
//        [_ViewForMedia.layer setShadowColor:[UIColor colorWithRed:225.0/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1.0].CGColor];
//        [_ViewForMedia.layer setShadowOpacity:1.0];
//        [_ViewForMedia.layer setShadowRadius:3.0];
//        [_ViewForMedia.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
//        
//        [self setup];
//        
//        NSString *StrZoe=[NSString stringWithFormat:@"%@",[appDelegate.ViewDict valueForKey:@"zoechatid"]];
//        
//        if ([StrZoe isEqualToString:@"(null)"])
//        {
//            StrZoe=[NSString stringWithFormat:@"%@",[appDelegate.ViewDict valueForKey:@"chatRoomId"]];
//        }
//        
//        _lblMobileNo.text=[NSString stringWithFormat:@"+%@",StrZoe];
//        
//        NSDictionary *getUserDict = [[NSDictionary alloc]init];
//        //appDelegate.isContact=@"yes";
//        if ([appDelegate.isContact isEqualToString:@"yes"])
//        {
//            getUserDict = [db_class getUserInfo:[appDelegate.ViewDict valueForKey:@"zoechatid"]];
//            
//        }
//        else{
//            getUserDict = [db_class getUserInfo:[appDelegate.ViewDict valueForKey:@"chatRoomId"]];
//        }
//        NSString *getStatus = [getUserDict valueForKey:@"status"];
//        
//        if ([[appDelegate.ViewDict valueForKey:@"chatRoomType"] isEqualToString:@"0"])
//        {
//            _objects=[db_class getOneToOneChatMessagesForMedia:StrZoe];
//            _lblStatus.text = getStatus;
//            [self.editImgBtn setHidden:YES];
//        }
//        
//        else
//        {
//            _objects=[db_class getGroupChatMessagesForMedia:StrZoe];
//            
//        }
//        
//        
//        NSLog(@"%@",_objects);
//        if (_objects.count==0)
//        {
//            _lblNoMediaFiles.hidden=NO;
//            _aCollectionView.hidden=YES;
//        }
//        else
//        {
//            _lblNoMediaFiles.hidden=YES;
//            _aCollectionView.hidden=NO;
//            _aCollectionView.dataSource=self;
//            _aCollectionView.delegate=self;
//        }
//        
//        CGRect contentRect = CGRectZero;
//        for (UIView *view in self.scrollView.subviews) {
//            contentRect = CGRectUnion(contentRect, view.frame);
//        }
//        self.scrollView.contentSize = CGSizeMake(contentRect.size.width, contentRect.size.height+10);
//        
//        
//        
//        if ([[appDelegate.ViewDict valueForKey:@"chatRoomType"] isEqualToString:@"1"])
//        {
//            NSString *strImage=[appDelegate.ViewDict valueForKey:@"groupImage"];
//            [self.editImgBtn setHidden:NO];
//            
//            if (![strImage isEqualToString:@"(null)"])
//            {
//                [_imgProfile sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"userImage.png"]];
//            }
//            else
//            {
//                _imgProfile.image=[UIImage imageNamed:@"userImage.png"];
//            }
//            
//            lblNav.text=[NSString stringWithFormat:@"%@",[appDelegate.ViewDict valueForKey:@"groupName"]];
//            _lblStatusAndPhone.text=@"Group Info";
//            _lblStatus.text=[NSString stringWithFormat:@"%@",[appDelegate.ViewDict valueForKey:@"groupName"]];
//            
//            UIBarButtonItem *barBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"AddContact.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onAddParticipants)];
//            barBtn.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
//            [self.navigationItem setRightBarButtonItem:barBtn ];
//            
//            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
//            [dictParam setValue:[appDelegate.ViewDict valueForKey:@"chatRoomId"] forKey:@"groupId"];
//            
//            MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//            hud.mode = MBProgressHUDAnimationFade;
//            hud.label.text = @"Loading";
//            
//            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
//            [afn getDataFromPath:FILE_GET_PARTICIPANTS withParamData:dictParam withBlock:^(id response, NSError *error)
//             {
//                 [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
//                 if (response)
//                 {
//                     //   NSLog(@"Response: %@",response);
//                     if ([[response valueForKey:@"error"] boolValue]==false)
//                     {
//                         NSDictionary *dictGroupDetails=[response valueForKey:@"groupDetails"];
//                         NSString *strCreatedBy=[NSString stringWithFormat:@"%@",[dictGroupDetails valueForKey:@"createdBy"]];
//                         NSDictionary *dictName=[db_class getUserNameForNotification:strCreatedBy];
//                         getgroupId=[NSString stringWithFormat:@"%@",[dictGroupDetails valueForKey:@"groupId"]];
//                         
//                         BOOL isAvailinChatsTable=NO;
//                         isAvailinChatsTable=[db_class doesGroupExist:getgroupId];
//                         if (isAvailinChatsTable==NO)
//                         {
//                             if ([[dictName allKeys] containsObject:@"name"])
//                             {
//                                 strCreatedBy=[NSString stringWithFormat:@"%@",[dictName valueForKey:@"name"]];
//                             }
//                             else
//                             {
//                                 strCreatedBy=[NSString stringWithFormat:@"+%@",[dictGroupDetails valueForKey:@"createdBy"]];
//                             }
//                             NSString *strMine=[NSString stringWithFormat:@"+%@",appDelegate.strID];
//                             if ([strCreatedBy isEqualToString:strMine])
//                             {
//                                 strCreatedBy=@"you";
//                             }
//                             
//                             
//                             
//                             NSDate *createdDate= [self convertMillisecondsToDate:[[dictGroupDetails valueForKey:@"createdAt"]longLongValue]];
//                             
//                             NSDateFormatter *df = [[NSDateFormatter alloc] init]; //Will release itself for us
//                             [df setDateFormat:@"dd-MM-yyyy"];
//                             NSString *strDatee = [df stringFromDate:createdDate] ; //Simplified the code
//                             
//                             
//                             _lblMobileNo.text=[NSString stringWithFormat:@"Created by %@, %@",strCreatedBy, strDatee];
//                             _lblSmallMobile.hidden=YES;
//                             _btnEdit.hidden=NO;
//                             
//                             arrParticipants=[response valueForKey:@"participants"];
//                             
//                             
//                             for (int i=0; i<arrParticipants.count; i++)
//                             {
//                                 NSString *getuserId = [arrParticipants valueForKey:@"participantId"][i];
//                                 NSString *getaddedby = [arrParticipants valueForKey:@"addedBy"][i];
//                                 getgroupId = [arrParticipants valueForKey:@"groupId"][i];
//                                 NSString *getJoinedat = [arrParticipants valueForKey:@"joinedAt"][i];
//                                 NSString *getAdmin = [arrParticipants valueForKey:@"isAdmin"][i];
//                                 BOOL success = [db_class insertParticipants:@"" userId:getuserId groupId:getgroupId joinedAt:getJoinedat addedBy:getaddedby isAdmin:getAdmin];
//                             }
//                             
//                             [self showParticipants];
//                             
//                         }
//                         
//                         else
//                         {
//                             [self showParticipants];
//                         }
//                         
//                     }
//                     
//                     else
//                     {
//                         
//                         MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//                         
//                         // Configure for text only and offset down
//                         hud.mode = MBProgressHUDModeText;
//                         hud.label.text = [NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
//                         hud.margin = 10.f;
//                         hud.yOffset = 150.f;
//                         hud.removeFromSuperViewOnHide = YES;
//                         [hud hideAnimated:YES afterDelay:3];
//                         
//                         
//                     }
//                 }
//                 
//             }];
//            
//            
//        }
//        
//    }
//    else{
//        
//        
//        NSString *strName=[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"name"]];
//        
//        if (![strName isEqualToString:@"(null)"])
//        {
//            lblNav.text=strName;
//        }
//        else
//        {
//            lblNav.text=[NSString stringWithFormat:@"+%@",[dictDetails valueForKey:@"chatRoomId"]];
//        }
//        
//        
//        
//        lblNav.textAlignment=NSTextAlignmentLeft;
//        self.navigationItem.titleView=lblNav;
//        self.navigationController.navigationBar.translucent = YES;
//        
//        NSString *strImage=[dictDetails valueForKey:@"image"];
//        
//        if (![strImage isEqualToString:@"(null)"])
//        {
//            [_imgProfile sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"userImage.png"]];
//        }
//        else
//        {
//            _imgProfile.image=[UIImage imageNamed:@"userImage.png"];
//        }
//        
//        
//        
//        
//        [_ViewForMedia.layer setCornerRadius:3.0f];
//        [_ViewForMedia.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//        [_ViewForMedia.layer setBorderWidth:0.5f];
//        [_ViewForMedia.layer setShadowColor:[UIColor colorWithRed:225.0/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1.0].CGColor];
//        [_ViewForMedia.layer setShadowOpacity:1.0];
//        [_ViewForMedia.layer setShadowRadius:3.0];
//        [_ViewForMedia.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
//        
//        [self setup];
//        
//        NSString *StrZoe=[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"zoechatid"]];
//        
//        if ([StrZoe isEqualToString:@"(null)"])
//        {
//            StrZoe=[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"chatRoomId"]];
//        }
//        
//        _lblMobileNo.text=[NSString stringWithFormat:@"+%@",StrZoe];
//        
//        NSDictionary *getUserDict = [[NSDictionary alloc]init];
//        //appDelegate.isContact=@"yes";
//        if ([appDelegate.isContact isEqualToString:@"yes"])
//        {
//            getUserDict = [db_class getUserInfo:[dictDetails valueForKey:@"zoechatid"]];
//            
//        }
//        else{
//            getUserDict = [db_class getUserInfo:[dictDetails valueForKey:@"chatRoomId"]];
//        }
//        NSString *getStatus = [getUserDict valueForKey:@"status"];
//        
//        if ([[dictDetails valueForKey:@"chatRoomType"] isEqualToString:@"0"])
//        {
//            _objects=[db_class getOneToOneChatMessagesForMedia:StrZoe];
//            _lblStatus.text = getStatus;
//            [self.editImgBtn setHidden:YES];
//        }
//        
//        else
//        {
//            _objects=[db_class getGroupChatMessagesForMedia:StrZoe];
//            
//        }
//        
//        
//        NSLog(@"%@",_objects);
//        if (_objects.count==0)
//        {
//            _lblNoMediaFiles.hidden=NO;
//            _aCollectionView.hidden=YES;
//        }
//        else
//        {
//            _lblNoMediaFiles.hidden=YES;
//            _aCollectionView.hidden=NO;
//            _aCollectionView.dataSource=self;
//            _aCollectionView.delegate=self;
//        }
//        
//        CGRect contentRect = CGRectZero;
//        for (UIView *view in self.scrollView.subviews) {
//            contentRect = CGRectUnion(contentRect, view.frame);
//        }
//        self.scrollView.contentSize = CGSizeMake(contentRect.size.width, contentRect.size.height+10);
//        
//        
//        
//        if ([[dictDetails valueForKey:@"chatRoomType"] isEqualToString:@"1"])
//        {
//            NSString *strImage=[dictDetails valueForKey:@"groupImage"];
//            [self.editImgBtn setHidden:NO];
//            
//            if (![strImage isEqualToString:@"(null)"])
//            {
//                [_imgProfile sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"userImage.png"]];
//            }
//            else
//            {
//                _imgProfile.image=[UIImage imageNamed:@"userImage.png"];
//            }
//            
//            lblNav.text=[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"groupName"]];
//            _lblStatusAndPhone.text=@"Group Info";
//            _lblStatus.text=[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"groupName"]];
//            
//            UIBarButtonItem *barBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"AddContact.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onAddParticipants)];
//            barBtn.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
//            [self.navigationItem setRightBarButtonItem:barBtn ];
//            
//            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
//            [dictParam setValue:[dictDetails valueForKey:@"chatRoomId"] forKey:@"groupId"];
//            
//            MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//            hud.mode = MBProgressHUDAnimationFade;
//            hud.label.text = @"Loading";
//            
//            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
//            [afn getDataFromPath:FILE_GET_PARTICIPANTS withParamData:dictParam withBlock:^(id response, NSError *error)
//             {
//                 [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
//                 if (response)
//                 {
//                     //   NSLog(@"Response: %@",response);
//                     if ([[response valueForKey:@"error"] boolValue]==false)
//                     {
//                         NSDictionary *dictGroupDetails=[response valueForKey:@"groupDetails"];
//                         NSString *strCreatedBy=[NSString stringWithFormat:@"%@",[dictGroupDetails valueForKey:@"createdBy"]];
//                         NSDictionary *dictName=[db_class getUserNameForNotification:strCreatedBy];
//                         getgroupId=[NSString stringWithFormat:@"%@",[dictGroupDetails valueForKey:@"groupId"]];
//                         
//                         BOOL isAvailinChatsTable=NO;
//                         isAvailinChatsTable=[db_class doesGroupExist:getgroupId];
//                         if (isAvailinChatsTable==NO)
//                         {
//                             if ([[dictName allKeys] containsObject:@"name"])
//                             {
//                                 strCreatedBy=[NSString stringWithFormat:@"%@",[dictName valueForKey:@"name"]];
//                             }
//                             else
//                             {
//                                 strCreatedBy=[NSString stringWithFormat:@"+%@",[dictGroupDetails valueForKey:@"createdBy"]];
//                             }
//                             NSString *strMine=[NSString stringWithFormat:@"+%@",appDelegate.strID];
//                             if ([strCreatedBy isEqualToString:strMine])
//                             {
//                                 strCreatedBy=@"you";
//                             }
//                             
//                             
//                             
//                             NSDate *createdDate= [self convertMillisecondsToDate:[[dictGroupDetails valueForKey:@"createdAt"]longLongValue]];
//                             
//                             NSDateFormatter *df = [[NSDateFormatter alloc] init]; //Will release itself for us
//                             [df setDateFormat:@"dd-MM-yyyy"];
//                             NSString *strDatee = [df stringFromDate:createdDate] ; //Simplified the code
//                             
//                             
//                             _lblMobileNo.text=[NSString stringWithFormat:@"Created by %@, %@",strCreatedBy, strDatee];
//                             _lblSmallMobile.hidden=YES;
//                             _btnEdit.hidden=NO;
//                             
//                             arrParticipants=[response valueForKey:@"participants"];
//                             
//                             
//                             for (int i=0; i<arrParticipants.count; i++)
//                             {
//                                 NSString *getuserId = [arrParticipants valueForKey:@"participantId"][i];
//                                 NSString *getaddedby = [arrParticipants valueForKey:@"addedBy"][i];
//                                 getgroupId = [arrParticipants valueForKey:@"groupId"][i];
//                                 NSString *getJoinedat = [arrParticipants valueForKey:@"joinedAt"][i];
//                                 NSString *getAdmin = [arrParticipants valueForKey:@"isAdmin"][i];
//                                 BOOL success = [db_class insertParticipants:@"" userId:getuserId groupId:getgroupId joinedAt:getJoinedat addedBy:getaddedby isAdmin:getAdmin];
//                             }
//                             
//                             [self showParticipants];
//                             
//                         }
//                         
//                         else
//                         {
//                             [self showParticipants];
//                         }
//                         
//                     }
//                     
//                     else
//                     {
//                         
//                         MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//                         
//                         // Configure for text only and offset down
//                         hud.mode = MBProgressHUDModeText;
//                         hud.label.text = [NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
//                         hud.margin = 10.f;
//                         hud.yOffset = 150.f;
//                         hud.removeFromSuperViewOnHide = YES;
//                         [hud hideAnimated:YES afterDelay:3];
//                         
//                         
//                     }
//                 }
//                 
//             }];
//            
//            
//        }
//    }
}

-(void)onBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)showParticipants
{
    getPartIDs = [[NSMutableArray alloc]init];
    NSString *CheckAdmin;
    NSString *strName,*getStrName, *getParticipentID;
    int yPos=self.ViewForMedia.frame.origin.y + self.ViewForMedia.frame.size.height +20;
    NSArray *arrdata=[db_class getParticipants:getgroupId];

    UILabel *lblParti=[[UILabel alloc]initWithFrame:CGRectMake(16, yPos, self.view.frame.size.width-32, 20)];
    lblParti.text=[NSString stringWithFormat:@"Participants: %lu",(unsigned long)arrdata.count];
    lblParti.font=[UIFont fontWithName:FONT_MEDIUM size:14];
    [self.scrollView addSubview:lblParti];
    
    yPos+=30;
    
    
    bool flag = false;
    
    for (int i=0; i<arrdata.count; i++)
    {
        NSDictionary *dictParti=[arrdata objectAtIndex:i];
        
        NSDictionary *dictInfo =[db_class getUserInfo:[dictParti valueForKey:@"userId"]];
        
        
        getParticipentID=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"userId"]];
        
        
        
        strName=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"name"]];
        NSString *strImage=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"image"]];
        NSString *strStatus=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"status"]];
        CheckAdmin=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"isAdmin"]];
        admin = CheckAdmin;
         NSLog(@"FromName:%@", strName);
//         NSString *StrZoe=[NSString stringWithFormat:@"%@",[appDelegate.ViewDict valueForKey:@"zoechatid"]];
        
        
         NSString *StrZoe=[NSString stringWithFormat:@"%@",appDelegate.strID];
        
        
        
        if([StrZoe isEqualToString:getParticipentID])
        {
                if([CheckAdmin isEqualToString:@"1"])
                {
                    flag = true;
                    strName=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"name"]];
                    NSLog(@"FromName:%@", strName);
                }
        }
    
    }
    
    
    
    
    
    if (flag)
    {
       
    for (int i=0; i<arrdata.count; i++)
    {
        NSDictionary *dictParti=[arrdata objectAtIndex:i];
        
    NSDictionary *dictInfo =[db_class getUserInfo:[dictParti valueForKey:@"userId"]];
        
        
        getParticipentID=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"userId"]];
        
        
        
        strName=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"name"]];
        NSString *strImage=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"image"]];
        NSString *strStatus=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"status"]];
        CheckAdmin=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"isAdmin"]];
        
     
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(16, yPos, 50, 50)];
        imgView.contentScaleFactor = UIViewContentModeScaleAspectFill;
        imgView.layer.cornerRadius = CGRectGetHeight(imgView.frame) / 2;
        imgView.clipsToBounds=YES;
        UILabel *initialLbl = [[UILabel alloc]initWithFrame:CGRectMake(16, yPos, 50, 50)];
        initialLbl.contentScaleFactor = UIViewContentModeScaleAspectFill;
        initialLbl.layer.cornerRadius = CGRectGetHeight(initialLbl.frame) / 2;
        initialLbl.clipsToBounds=YES;
        initialLbl.textAlignment=NSTextAlignmentCenter;
        initialLbl.textColor=[UIColor whiteColor];
        [initialLbl setHidden:YES];

        if (![strImage isEqualToString:@"(null)"])
        {
            [imgView sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"clear.png"]];
        }
        else
        {
            [initialLbl setHidden:NO];
            
            //lblImg
            NSString *InitialStr;
            UIColor *bgColor;
            if (![strName isEqualToString:@"(null)"])
            {
                
                NSString *getId=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"zoechatid"]];
                if ([getId isEqualToString:appDelegate.strID])
                {
                    
                    getStrName=@"You";
                    [initialLbl setHidden:YES];

                    if (![appDelegate.strProfilePic isEqualToString:@"(null)"])
                    {
                        [imgView sd_setImageWithURL:[NSURL URLWithString:appDelegate.strProfilePic] placeholderImage:[UIImage imageNamed:@"clear.png"]];
                    }
                    else
                    {
                        [initialLbl setHidden:NO];
                        initialLbl.text=@"Y";
                        initialLbl.backgroundColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
                        
                        
                    }
                    
                }
                else
                {
                    getStrName=strName;
                }
                
                
                
                InitialStr=[getStrName substringToIndex:1];
                if ([InitialStr isEqualToString:@"A"])
                {
                    bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"B"])
                {
                    bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"C"])
                {
                    bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"D"])
                {
                    bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"E"])
                {
                    bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"F"])
                {
                    bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"G"])
                {
                    bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"H"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"I"])
                {
                    bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"J"])
                {
                    bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"K"])
                {
                    bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"L"])
                {
                    bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"M"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"N"])
                {
                    bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"O"])
                {
                    bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"P"])
                {
                    bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"Q"])
                {
                    bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"R"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"S"])
                {
                    bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"T"])
                {
                    bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"U"])
                {
                    bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"V"])
                {
                    bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"W"])
                {
                    bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"X"])
                {
                    bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"Y"])
                {
                    bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"Z"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
                }
                
            }
            else
            {
                NSString *getId=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"userId"]];

                if ([getId isEqualToString:appDelegate.strID])
                {
                    
                    getStrName=@"You";
                    [initialLbl setHidden:YES];

                    if (![appDelegate.strProfilePic isEqualToString:@"(null)"])
                    {
                        [imgView sd_setImageWithURL:[NSURL URLWithString:appDelegate.strProfilePic] placeholderImage:[UIImage imageNamed:@"clear.png"]];
                    }
                    else
                    {
                        [initialLbl setHidden:NO];
                        initialLbl.text=@"Y";
                        initialLbl.backgroundColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
                        
                        
                    }
                }
 
                else
                {
                NSString *trimmString=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"userId"]];
                NSCharacterSet *unwantedChars = [NSCharacterSet characterSetWithCharactersInString:@"userId""\"[]""{}"":"];
                getStrName = [[trimmString componentsSeparatedByCharactersInSet:unwantedChars] componentsJoinedByString: @""];
                }
                InitialStr=[getStrName substringToIndex:1];
                if ([InitialStr isEqualToString:@"A"])
                {
                    bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"B"])
                {
                    bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"C"])
                {
                    bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"D"])
                {
                    bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"E"])
                {
                    bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"F"])
                {
                    bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"G"])
                {
                    bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"H"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"I"])
                {
                    bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"J"])
                {
                    bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"K"])
                {
                    bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"L"])
                {
                    bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"M"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"N"])
                {
                    bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"O"])
                {
                    bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"P"])
                {
                    bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"Q"])
                {
                    bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"R"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"S"])
                {
                    bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"T"])
                {
                    bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"U"])
                {
                    bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"V"])
                {
                    bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"W"])
                {
                    bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
                }else if ([InitialStr isEqualToString:@"X"])
                {
                    bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"Y"])
                {
                    bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"Z"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
                }

               else if ([InitialStr isEqualToString:@"0"])
                {
                    bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"1"])
                {
                    bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"2"])
                {
                    bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"3"])
                {
                    bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"4"])
                {
                    bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"5"])
                {
                    bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"6"])
                {
                    bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"7"])
                {
                    bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"8"])
                {
                    bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
                }
                else if ([InitialStr isEqualToString:@"9"])
                {
                    bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
                }
                
            }
            initialLbl.text=InitialStr;
            initialLbl.backgroundColor=bgColor;
            //            _imgProfile.image=[UIImage imageNamed:@"clear.png"];
        }
        [self.scrollView addSubview:imgView];
        [self.scrollView addSubview:initialLbl];
        
        UILabel *lblName=[[UILabel alloc]initWithFrame:CGRectMake(75, yPos+3, self.view.frame.size.width-100, 20)];
        if([CheckAdmin isEqualToString:@"1"])
        {
            UILabel *AdminLbl=[[UILabel alloc]initWithFrame:CGRectMake(200, yPos+3, 80, 20)];
            AdminLbl.text = @"Group Admin";
            AdminLbl.textAlignment = NSTextAlignmentCenter;
            NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
            UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
            if (color != nil) {
                AdminLbl.textColor=color;
                [AdminLbl.layer setBorderColor:color.CGColor];
            }
            else{
                AdminLbl.textColor=[[UIColor alloc] initWithRed:38/255.f green:186/255.f blue:154/255.f alpha:1];
                [AdminLbl.layer setBorderColor:[UIColor colorWithRed:38.0/255.0 green:186.0/255.0 blue:154.0/255.0 alpha:1.0].CGColor];
                
            }

            [[AdminLbl layer] setCornerRadius:5]; // THIS IS THE RELEVANT LINE
            [AdminLbl.layer setMasksToBounds:YES];
            [AdminLbl.layer setBorderWidth:1.0];
            AdminLbl.font=[UIFont fontWithName:FONT_NORMAL size:10];
            //[lblName addSubview:AdminLbl];
            [self.scrollView addSubview:AdminLbl];
        
        if (![strName isEqualToString:@"(null)"])
        {
            NSString *getId=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"zoechatid"]];
            if ([getId isEqualToString:appDelegate.strID])
            {
                
                lblName.text=@"You";
                getStrName=lblName.text;
            }
            else
            {
                lblName.text=strName;
                getStrName=lblName.text;
            }
        }
        else
        {
            
            if ([[dictParti valueForKey:@"userId"]isEqual:[NSNull null]])
            {
                NSLog(@"NULL");
            }
            else{
            if ([[dictParti valueForKey:@"userId"]isEqualToString:appDelegate.strID])
            {
                lblName.text=@"You";
                getStrName=lblName.text;
                strStatus=appDelegate.strStatus;
                 [imgView sd_setImageWithURL:[NSURL URLWithString:appDelegate.strProfilePic] placeholderImage:[UIImage imageNamed:@"clear.png"]];
            }
            else
            {
                NSString *trimmString=[NSString stringWithFormat:@"+%@",[dictParti valueForKey:@"userId"]];
                NSCharacterSet *unwantedChars = [NSCharacterSet characterSetWithCharactersInString:@"userId""\"[]""{}"":"];
                NSString *requiredString = [[trimmString componentsSeparatedByCharactersInSet:unwantedChars] componentsJoinedByString: @""];
                NSLog(@"%@",requiredString);
                lblName.text=[NSString stringWithFormat:@"%@",requiredString];
                getStrName=lblName.text;
                strStatus=@"";
            }
        }
        }
    }
        else
        {
            
            if (![strName isEqualToString:@"(null)"])
            {
                NSString *getId=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"zoechatid"]];
                if ([getId isEqualToString:appDelegate.strID])
                {

                lblName.text=@"You";
                getStrName=lblName.text;
                }
                else
                {
                lblName.text=strName;
                getStrName=lblName.text;
                }
            }
            else
            {
                
                if ([[dictParti valueForKey:@"userId"]isEqual:[NSNull null]])
                {
                    NSLog(@"NULL");
                }
                else{
                    if ([[dictParti valueForKey:@"userId"]isEqualToString:appDelegate.strID])
                    {
                        lblName.text=@"You";
                        getStrName=lblName.text;
                        strStatus=appDelegate.strStatus;
                        [imgView sd_setImageWithURL:[NSURL URLWithString:appDelegate.strProfilePic] placeholderImage:[UIImage imageNamed:@"clear.png"]];
                    }
                    else
                    {
                        NSString *trimmString=[NSString stringWithFormat:@"+%@",[dictParti valueForKey:@"userId"]];
                        NSCharacterSet *unwantedChars = [NSCharacterSet characterSetWithCharactersInString:@"userId""\"[]""{}"":"];
                        NSString *requiredString = [[trimmString componentsSeparatedByCharactersInSet:unwantedChars] componentsJoinedByString: @""];
                        NSLog(@"%@",requiredString);
                        lblName.text=[NSString stringWithFormat:@"%@",requiredString];
                        getStrName=lblName.text;
                        strStatus=@"";
                    }
                }
            }
        }
        lblName.font=[UIFont fontWithName:FONT_NORMAL size:14];
        [self.scrollView addSubview:lblName];

        UILabel *lblStatus=[[UILabel alloc]initWithFrame:CGRectMake(75, yPos+27, self.view.frame.size.width-100, 20)];
        lblStatus.text=strStatus;
        lblStatus.font=[UIFont fontWithName:FONT_NORMAL size:14];
        [self.scrollView addSubview:lblStatus];
        
        
        UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake(75, yPos+3, self.view.frame.size.width-100, 40)];
        [button addTarget:self action:@selector(CheckAdmin:) forControlEvents:UIControlEventTouchUpInside];
        button.accessibilityHint=CheckAdmin;
        button.accessibilityValue=getStrName;
        button.accessibilityIdentifier=getParticipentID;
        buttonTag=i;
        button.tag=buttonTag;
        [self.scrollView addSubview:button];

        
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(16,yPos+51, self.view.frame.size.width-32, 1)];
        lineView.backgroundColor = [UIColor lightGrayColor];
        [self.scrollView addSubview:lineView];
        
        yPos+=55;
        NSString *getpartidstr = [dictParti valueForKey:@"userId"];
        [getPartIDs addObject:getpartidstr];
        
    }
        
    }
    NSLog(@"%@",getPartIDs);
    
    //Exit
    UIView *ExitView = [[UIView alloc] initWithFrame:CGRectMake(16,yPos, self.view.frame.size.width-32, 60)];
    
    [ExitView.layer setCornerRadius:3.0f];
    [ExitView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [ExitView.layer setBorderWidth:0.5f];
    [ExitView.layer setShadowColor:[UIColor colorWithRed:225.0/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1.0].CGColor];
    [ExitView.layer setShadowOpacity:1.0];
    [ExitView.layer setShadowRadius:3.0];
    [ExitView.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
    

    UIButton *exitBtn=[[UIButton alloc]initWithFrame:CGRectMake(78, 0,265, 60)];
    [exitBtn setBackgroundColor:[UIColor clearColor]];
    [exitBtn addTarget:self action:@selector(ExitGroup) forControlEvents:UIControlEventTouchDown];
    [ExitView addSubview:exitBtn];
    
    UILabel *exitLbl=[[UILabel alloc]initWithFrame:CGRectMake(78, 0, 265, 60)];
    [exitLbl setBackgroundColor:[UIColor clearColor]];
    [exitLbl setText:@"Exit Group"];
    [exitLbl setTextColor:[UIColor redColor]];
    [exitLbl setFont: [exitLbl.font fontWithSize: 17.0]];
    //exitLbl.textAlignment = NSTextAlignmentCenter;
    [ExitView addSubview:exitLbl];

    UIImageView *logoutImg = [[UIImageView alloc] initWithFrame:CGRectMake(19, 15, 30, 30)];
    logoutImg.image = [UIImage imageNamed:@"logout.png"];
    [logoutImg setTintColor:[UIColor redColor]];
    [ExitView addSubview:logoutImg];

    
    [self.scrollView addSubview:ExitView];
    
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.scrollView.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    self.scrollView.contentSize = CGSizeMake(contentRect.size.width, yPos+60);
}
- (IBAction)CheckAdmin:(UIButton*)sender
{
    NSLog(@"%@",sender.accessibilityHint);
    NSLog(@"%ld",(long)sender.tag);
    //buttonTag=sender.tag;
    
//    
    NSArray *arrdata=[db_class getParticipants:getgroupId];
    NSDictionary *dictParti=[arrdata objectAtIndex:sender.tag];
    NSLog(@"%@",dictParti);
    NSString *userId=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"userId"]];
    NSString *addedById=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"addedBy"]];

    NSString *getGID=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"groupId"]];
    NSString *checkAdminStr=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"isAdmin"]];
    NSLog(@"%@",checkAdminStr);

    if ([getGID isEqualToString:getgroupId])
    {
        if ([userId isEqualToString:appDelegate.strID])
        {
            
            if ([sender.accessibilityValue isEqualToString:@"You"] && [checkAdminStr isEqualToString:@"1"])
            {
                
            }
            else if ([sender.accessibilityValue isEqualToString:@"You"] && [checkAdminStr isEqualToString:@"0"])
            {
                
            }
            
            else if ([checkAdminStr isEqualToString:@"1"])
            {
                [self UpdatedAdminView:sender.accessibilityValue getpartID:sender.accessibilityIdentifier];

            }
            else
            {
                [self AdminView:sender.accessibilityValue getpartID:sender.accessibilityIdentifier];
            }
        }
        else if ([addedById isEqualToString:appDelegate.strID])
        {
            if ([sender.accessibilityValue isEqualToString:@"You"] && [checkAdminStr isEqualToString:@"1"])
            {
                
            }
            else if ([sender.accessibilityValue isEqualToString:@"You"] && [checkAdminStr isEqualToString:@"0"])
            {
                
            }

            else if ([checkAdminStr isEqualToString:@"1"])
            {
                [self UpdatedAdminView:sender.accessibilityValue getpartID:sender.accessibilityIdentifier];
                
            }
            else
            {
                [self AdminView:sender.accessibilityValue getpartID:sender.accessibilityIdentifier];
            }
        }
        else
        {
            [self NoAdminView:sender.accessibilityValue getpartID:sender.accessibilityIdentifier];
        }

        }
    

    
/*
    if ([sender.accessibilityValue isEqualToString:@"You"] && [sender.accessibilityHint isEqualToString:@"1"])
    {
        
    }
    else if ([sender.accessibilityValue isEqualToString:@"You"] && [sender.accessibilityHint isEqualToString:@"0"])
    {
        
    }


    else if ([sender.accessibilityHint isEqualToString:@"1"])
    {
        [self UpdatedAdminView:sender.accessibilityValue getpartID:sender.accessibilityIdentifier];
    }
    else if (![sender.accessibilityValue isEqualToString:@"You"] && [sender.accessibilityHint isEqualToString:@"1"])
    {
        
        [self AdminView:sender.accessibilityValue getpartID:sender.accessibilityIdentifier];

    }
    else if ([sender.accessibilityHint isEqualToString:@"0"])
    {
        [self AdminView:sender.accessibilityValue getpartID:sender.accessibilityIdentifier];

    }
 */
    
//        }
//    else
//    {
//        if ([sender.accessibilityValue isEqualToString:@"You"])
//        {
//            
//        }
//        else if ([sender.accessibilityHint isEqualToString:@"1"])
//        {
//            [self UpdatedAdminView:sender.accessibilityValue getpartID:sender.accessibilityIdentifier];
//        }
//
//        else if (![sender.accessibilityValue isEqualToString:@"You"] && [sender.accessibilityHint isEqualToString:@"1"])
//        {
//            
//            [self AdminView:sender.accessibilityValue getpartID:sender.accessibilityIdentifier];
//            
//        }
//        else if ([sender.accessibilityHint isEqualToString:@"0"])
//        {
//            [self AdminView:sender.accessibilityValue getpartID:sender.accessibilityIdentifier];
//            
//        }
//        
//        else{
//         [self NoAdminView:sender.accessibilityValue getpartID:sender.accessibilityIdentifier];
//        }
//    }

}
-(void)AdminView:(NSString*)getContactName getpartID:(NSString*)getpartID
{
    viewForAudioOrVideo=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+self.navigationController.navigationBar.frame.size.height+50)];
    viewForAudioOrVideo.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7f];
    viewForAudioOrVideo.tag=9087650;
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onRemoveViewAudioVideo:)]; // this is the current problem like a lot of people out there...
//    [viewForAudioOrVideo addGestureRecognizer:tap];
    
    
    
    
    viewForWhiteBG=[[UIView alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)-140, (viewForAudioOrVideo.frame.size.height/2)-50, 280, (44*isAdmindataArray.count))];
    viewForWhiteBG.backgroundColor=[UIColor whiteColor];
    viewForWhiteBG.layer.cornerRadius=3;
    viewForWhiteBG.clipsToBounds=YES;
    [viewForAudioOrVideo addSubview:viewForWhiteBG];
     ContactName=getContactName;
    isAdmindataArray=[[NSArray alloc]initWithObjects:[NSString stringWithFormat:@"Message %@",ContactName],[NSString stringWithFormat:@"View %@",ContactName],@"Make Admin",[NSString stringWithFormat:@"Remove %@",ContactName], nil];

    UITableView *infoTblView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, viewForWhiteBG.frame.size.width, viewForWhiteBG.frame.size.height) style:UITableViewStylePlain];
    infoTblView.delegate = self;
    infoTblView.dataSource = self;
    infoTblView.tag=11;
    viewForWhiteBG.center=viewForAudioOrVideo.center;
   
    passParticipantsId=getpartID;
    
    
    
    UIButton *transparencyButton = [[UIButton alloc] initWithFrame:viewForAudioOrVideo.bounds];
    transparencyButton.backgroundColor = [UIColor clearColor];
    [transparencyButton addTarget:self action:@selector(dismissHelper:) forControlEvents:UIControlEventTouchUpInside];
    [viewForWhiteBG addSubview:infoTblView];
    [infoTblView reloadData];
    [viewForAudioOrVideo insertSubview:transparencyButton belowSubview:viewForWhiteBG];

    [self.navigationController.view addSubview:viewForAudioOrVideo];

    }
//1
-(void)UpdatedAdminView:(NSString*)getContactName getpartID:(NSString*)getpartID
{
    viewForAudioOrVideo1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+self.navigationController.navigationBar.frame.size.height+50)];
    viewForAudioOrVideo1.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7f];
    viewForAudioOrVideo1.tag=9087651;
    
    //    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onRemoveViewAudioVideo:)]; // this is the current problem like a lot of people out there...
    //    [viewForAudioOrVideo addGestureRecognizer:tap];
    
    
    
    
    viewForWhiteBG1=[[UIView alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)-140, (viewForAudioOrVideo1.frame.size.height/2)-50, 280, (44*updatedAdminArray.count))];
    viewForWhiteBG1.backgroundColor=[UIColor whiteColor];
    viewForWhiteBG1.layer.cornerRadius=3;
    viewForWhiteBG1.clipsToBounds=YES;
    [viewForAudioOrVideo1 addSubview:viewForWhiteBG1];
    ContactName=getContactName;
    passParticipantsId=getpartID;

    updatedAdminArray=[[NSArray alloc]initWithObjects:[NSString stringWithFormat:@"Message %@",ContactName],[NSString stringWithFormat:@"View %@",ContactName],[NSString stringWithFormat:@"Remove %@",ContactName], nil];

    UITableView *infoTblView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, viewForWhiteBG1.frame.size.width, viewForWhiteBG1.frame.size.height) style:UITableViewStylePlain];
    infoTblView.delegate = self;
    infoTblView.dataSource = self;
    infoTblView.tag=111;

    viewForWhiteBG1.center=viewForAudioOrVideo1.center;
    
    
    
    
    
    UIButton *transparencyButton = [[UIButton alloc] initWithFrame:viewForAudioOrVideo1.bounds];
    transparencyButton.backgroundColor = [UIColor clearColor];
    [transparencyButton addTarget:self action:@selector(dismissHelper1:) forControlEvents:UIControlEventTouchUpInside];
    [viewForWhiteBG1 addSubview:infoTblView];
    [infoTblView reloadData];
    [viewForAudioOrVideo1 insertSubview:transparencyButton belowSubview:viewForWhiteBG1];

    [self.navigationController.view addSubview:viewForAudioOrVideo1];

    
    
}
-(void)NoAdminView:(NSString*)getContactName getpartID:(NSString*)getpartID
{
    viewForAudioOrVideo2=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+self.navigationController.navigationBar.frame.size.height+50)];
    viewForAudioOrVideo2.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7f];
    viewForAudioOrVideo2.tag=9087652;
    
    //    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onRemoveViewAudioVideo:)]; // this is the current problem like a lot of people out there...
    //    [viewForAudioOrVideo addGestureRecognizer:tap];
    
    
    
    
    viewForWhiteBG2=[[UIView alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)-140, (viewForAudioOrVideo2.frame.size.height/2)-50, 280, (44*isnotAdminDataArray.count))];
    viewForWhiteBG2.backgroundColor=[UIColor whiteColor];
    viewForWhiteBG2.layer.cornerRadius=3;
    viewForWhiteBG2.clipsToBounds=YES;
    [viewForAudioOrVideo2 addSubview:viewForWhiteBG2];
    ContactName=getContactName;
    passParticipantsId=getpartID;
    isnotAdminDataArray=[[NSArray alloc]initWithObjects:[NSString stringWithFormat:@"Message %@",ContactName],[NSString stringWithFormat:@"View %@",ContactName], nil];
    
    UITableView *infoTblView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, viewForWhiteBG2.frame.size.width, viewForWhiteBG2.frame.size.height) style:UITableViewStylePlain];
    infoTblView.delegate = self;
    infoTblView.dataSource = self;
    infoTblView.tag=1111;
    viewForWhiteBG2.center=viewForAudioOrVideo2.center;
    
    
    
    
    
    UIButton *transparencyButton = [[UIButton alloc] initWithFrame:viewForAudioOrVideo2.bounds];
    transparencyButton.backgroundColor = [UIColor clearColor];
    [transparencyButton addTarget:self action:@selector(dismissHelper2:) forControlEvents:UIControlEventTouchUpInside];
    [viewForWhiteBG2 addSubview:infoTblView];
    [infoTblView reloadData];
    [viewForAudioOrVideo2 insertSubview:transparencyButton belowSubview:viewForWhiteBG2];

    [self.navigationController.view addSubview:viewForAudioOrVideo2];

    
}

- (void)dismissHelper:(UIButton *)sender
{
    [viewForAudioOrVideo removeFromSuperview];
    sender.hidden = YES;
    // or [sender removeFromSuperview]
}
- (void)dismissHelper1:(UIButton *)sender
{
    [viewForAudioOrVideo1 removeFromSuperview];
    sender.hidden = YES;
    // or [sender removeFromSuperview]
}
- (void)dismissHelper2:(UIButton *)sender
{
    [viewForAudioOrVideo2 removeFromSuperview];
    sender.hidden = YES;
    // or [sender removeFromSuperview]
}

-(void)ExitGroup
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:[dictDetails valueForKey:@"chatRoomId"] forKey:@"groupId"];
    [dictParam setValue:appDelegate.strID forKey:@"from"];
    NSString *getGID = [dictParam valueForKey:@"groupId"];
    [[appDelegate.socket emitWithAck:@"exitGroup" with:@[@{@"groupId": getGID,@"from":appDelegate.strID}]] timingOutAfter:2 callback:^(NSArray* data) {
        NSLog(@"%@",data);
        NSString *checkAck=[data objectAtIndex:0];
        NSInteger getack=[checkAck integerValue];
        NSLog(@"%ld",(long)getack);
        if (getack == 1)
        {
            NSArray *arrdata=[db_class getParticipants:getgroupId];
            for (int i=0; i<arrdata.count; i++)
            {
                NSDictionary *dictParti=[arrdata objectAtIndex:i];
                NSString *userId=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"userId"]];
                if ([getgroupId isEqualToString:getGID])
                {
                    if ([appDelegate.strID isEqualToString:userId])
                    {
                        BOOL success2 = [db_class deleteParticipant:userId];
                        BOOL success = [db_class ExitChatTblGroup:getgroupId];
                        BOOL success1 = [db_class ExitChatTblMsgGroup:getgroupId];
                    }
                }
                
                
                // BOOL success2 = [db_class deleteParticipant:appDelegate.strID groupId:getGID];
                
            }
            
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            // Configure for text only and offset down
            hud.mode = MBProgressHUDModeText;
            hud.label.text = @"You are left from group";
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hideAnimated:YES afterDelay:3];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
            UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            MainTabBarVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainTabBarVC"];
            [self.navigationController pushViewController:hme animated:YES];
        }
        else
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            // Configure for text only and offset down
            hud.mode = MBProgressHUDModeText;
            hud.label.text = @"No Ack";
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hideAnimated:YES afterDelay:3];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
            
            
        }
    }];

   
    
}
-(void)onAddParticipants
{
    appDelegate.strGroupIdToAddParticipants=[dictDetails valueForKey:@"chatRoomId"];
    appDelegate.strGroupName =[dictDetails valueForKey:@"groupName"];
    NSLog(@"Dictdetails : %@",dictDetails);
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CreateGroupContactVC *Create = (CreateGroupContactVC *)[sb instantiateViewControllerWithIdentifier:@"CreateGroupContactVC"];
    Create.CheckPartiIDS = getPartIDs;
    NSLog(@"grpids",getPartIDs);
    Create.getGroupName = [dictDetails valueForKey:@"groupName"];
    Create.getGroupImg = [dictDetails valueForKey:@"groupImage"];
    [self.navigationController pushViewController:Create animated:YES];
}
-(void)viewWillDisappear:(BOOL)animated
{
     self.navigationController.navigationBar.translucent = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Setup

- (void)setup
{
   
    [self setupLayout];
}

- (void)setupLayout
{
    _aLayout = [[SMCollectionViewFillLayout alloc] init];
    _aLayout.delegate = self;
    _aLayout.stretchesLastItems = NO;
    _aLayout.direction = SMCollectionViewFillLayoutVertical;
    _aCollectionView.collectionViewLayout = _aLayout;
    
    
     _aLayout.direction = SMCollectionViewFillLayoutHorizontal;
    
}

-(IBAction)onEdit:(id)sender
{
    _viewEnterNewSubject.hidden=NO;
    [_txtNewSubject becomeFirstResponder];
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _objects.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict=[_objects objectAtIndex:indexPath.row];
    NSString *strContent=[NSString stringWithFormat:@"%@", [dict valueForKey:@"content"]];
    NSArray *components = [strContent componentsSeparatedByString:@"/"];
    NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
    

    
    MediaCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.5];
    cell.imgMedia.image=[UIImage imageWithContentsOfFile:getImagePath];
    
    [cell.layer setCornerRadius:3.0f];
    [cell.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [cell.layer setBorderWidth:0.5f];
    [cell.layer setShadowColor:[UIColor colorWithRed:225.0/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1.0].CGColor];
    [cell.layer setShadowOpacity:1.0];
    [cell.layer setShadowRadius:3.0];
    [cell.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];

    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    arrPhotos=[[NSMutableArray alloc]init];
    for (int i=0; i<_objects.count; i++)
    {
        
        NSDictionary *dict=[_objects objectAtIndex:i];
        NSString *strContent=[NSString stringWithFormat:@"%@", [dict valueForKey:@"content"]];
        NSArray *components = [strContent componentsSeparatedByString:@"/"];
        NSString *strImageName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:strImageName];
         UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
        
        MWPhoto *photo = [MWPhoto photoWithImage:img];
        NSString *strCaption=[NSString stringWithFormat:@"%@",[dictDetails valueForKey:@"caption"]];
        if (![strCaption isEqualToString:@"(null)"])
        {
             photo.caption = strCaption;
        }
        [arrPhotos addObject:photo];
            
        
    }
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = YES;
    browser.displaySelectionButtons = YES;
    browser.zoomPhotosToFill = NO;
    browser.alwaysShowControls = YES;
    browser.enableGrid = YES;
    browser.startOnGrid = NO;
    [browser setCurrentPhotoIndex:indexPath.row];
    [self.navigationController pushViewController:browser animated:YES];
    
    
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < arrPhotos.count) {
        return [arrPhotos objectAtIndex:index];
    }
    return nil;
}

#pragma mark - SMCollectionViewFillLayoutDelegate

- (NSInteger)numberOfItemsInSide
{
    return 1;
}

- (CGFloat)itemLength
{
    return 100.0f;
}

- (CGFloat)itemSpacing
{
    return 5.0f;
}

-(void)getChatMessages
{
    NSArray *data=[db_class getChatMessages];
    
    if (data.count!=0)
    {
        for (int i=0; i<data.count; i++)
        {
            NSDictionary *dict=[data objectAtIndex:i];
            NSString *strsndr=[NSString stringWithFormat:@"%@",[dict valueForKey:@"sender"]];
           // if ([[dict valueForKey:@"userId"]isEqualToString:strZoeChatId])
           // {
           // }
        }
    }
    
}


- (IBAction)onCancel:(id)sender
{
    _viewEnterNewSubject.hidden=YES;
    [_txtNewSubject resignFirstResponder];
}

- (IBAction)onOk:(id)sender
{
    [self.txtNewSubject resignFirstResponder];
    if ([[dictDetails valueForKey:@"chatRoomType"] isEqualToString:@"1"])
    {
    if ([appDelegate.strInternetMode isEqualToString:@"online"])
    {
        
        
       
        if ([self.txtNewSubject.text isEqualToString:@""])
        {
            
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:@"Please enter group subject." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            [self.txtNewSubject becomeFirstResponder];
            NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
            UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
            if (color != nil) {
                alert.view.tintColor=color;
            }
            else
            {
                
                alert.view.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
                
            }
            return;
        }
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        NSLog(@"dictparam:%@",dictParam);
            [dictParam setValue:[dictDetails valueForKey:@"chatRoomId"] forKey:@"groupId"];
            [dictParam setValue:_txtNewSubject.text forKey:@"groupName"];
            [dictParam setValue:appDelegate.strID forKey:@"from"];
        NSString *getGID = [dictParam valueForKey:@"groupId"];
        
        
        NSString *getoldname = [dictDetails valueForKey:@"groupName"];

        [[appDelegate.socket emitWithAck:@"editGroupName" with:@[@{@"groupId": getGID,@"groupName": _txtNewSubject.text,@"from":appDelegate.strID,@"oldName":getoldname }]] timingOutAfter:2 callback:^(NSArray* data) {
            NSLog(@"%@",data);
            NSString *checkAck=[data objectAtIndex:0];
            NSInteger getack=[checkAck integerValue];
            NSLog(@"%ld",(long)getack);
            if (getack == 1)
            {
                BOOL success2=[db_class updateGroupName:[dictDetails valueForKey:@"chatRoomId"] groupName:_txtNewSubject.text];
                
                _viewEnterNewSubject.hidden=YES;
                [_txtNewSubject resignFirstResponder];
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                // Configure for text only and offset down
                hud.mode = MBProgressHUDModeText;
                hud.label.text = @"Groupname changed Successfully";
                hud.margin = 10.f;
                hud.yOffset = 150.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hideAnimated:YES afterDelay:3];
                
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
                UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                MainTabBarVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainTabBarVC"];
                [self.navigationController pushViewController:hme animated:YES];
            }
            else
            {
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                // Configure for text only and offset down
                hud.mode = MBProgressHUDModeText;
                hud.label.text = @"No Ack";
                hud.margin = 10.f;
                hud.yOffset = 150.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hideAnimated:YES afterDelay:3];
            }
        }];
    }
        
        else
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            // Configure for text only and offset down
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [NSString stringWithFormat:@"Please check your internet connection"];
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.label.adjustsFontSizeToFitWidth=YES;
            hud.removeFromSuperViewOnHide = YES;
            [hud hideAnimated:YES afterDelay:3];
            
        }
    }

        else
        {
            BOOL success2=[db_class updateGroupName:[dictDetails valueForKey:@"chatRoomId"] groupName:_txtNewSubject.text];
            
            _viewEnterNewSubject.hidden=YES;
            [_txtNewSubject resignFirstResponder];
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            // Configure for text only and offset down
            hud.mode = MBProgressHUDModeText;
            hud.label.text = @"BroadCastName changed Successfully";
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hideAnimated:YES afterDelay:2];
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
            UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            MainTabBarVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainTabBarVC"];
            [self.navigationController pushViewController:hme animated:YES];

        }
    
    
    
}

- (IBAction)ClickOnEditImg:(id)sender {
    
//    Emit - editGroupImage
//    groupId
//    groupImage
//    from
    
    NSLog(@"%@",appDelegate.strInternetMode);
    if ([appDelegate.strInternetMode isEqualToString:@"online"])
    {
        UIButton *btnProf=(UIButton * )sender;
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:appDelegate.strAlertTitle
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction* Camera = [UIAlertAction
                                 actionWithTitle:@"Take Picture"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                                     
                                     //Use camera if device has one otherwise use photo library
                                     if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                                     {
                                         [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
                                     }
                                     else
                                     {
                                         [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                                     }
                                     
                                     [imagePicker setDelegate:self];
                                     
                                     //Show image picker
                                     [self presentViewController:imagePicker animated:YES completion:nil];
                                 }];
        
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Choose From Camera Roll"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
                                 imagePickerController.delegate = self;
                                 [imagePickerController setAllowsEditing:NO];
                                 imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
                                 [self presentViewController:imagePickerController animated:YES completion:nil];
                                 
                             }];
        
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleCancel
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:Camera];
        [alert addAction:ok];
        [alert addAction:cancel];
        
        
        UIPopoverPresentationController *popPresenter1 = [alert
                                                          popoverPresentationController];
        popPresenter1.sourceView=btnProf;
        popPresenter1.sourceRect=btnProf.bounds;
        [alert setModalPresentationStyle:UIModalPresentationPopover];
        
        [self presentViewController:alert animated:YES completion:nil];
        NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
        UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
        if (color != nil) {
            alert.view.tintColor=color;
        }
        else
        {
            
            alert.view.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
            
        }
        
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Please check your internet connection"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.label.adjustsFontSizeToFitWidth=YES;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:3];
        
    }
    
    }
#pragma mark
#pragma mark - ImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:chosenImage];
    cropController.delegate = self;
    cropController.rotateButtonsHidden=YES;
    cropController.rotateClockwiseButtonHidden=YES;
    //  cropController.aspectRatioPickerButtonHidden=YES;
    
    // -- Uncomment these if you want to test out restoring to a previous crop setting --
    //cropController.angle = 90; // The initial angle in which the image will be rotated
    //cropController.imageCropFrame = CGRectMake(0,0,2848,4288); //The
    
    // -- Uncomment the following lines of code to test out the aspect ratio features --
    cropController.aspectRatioPreset = TOCropViewControllerAspectRatioPresetSquare; //Set the initial aspect ratio as a square
    cropController.aspectRatioLockEnabled = YES; // The crop box is locked to the aspect ratio and can't be resized away from it
    cropController.resetAspectRatioEnabled = NO; // When tapping 'reset', the aspect ratio will NOT be reset back to default
    
    // -- Uncomment this line of code to place the toolbar at the top of the view controller --
    // cropController.toolbarPosition = TOCropViewControllerToolbarPositionTop;
    
    
    self.image = chosenImage;
    
    //If profile picture, push onto the same navigation stack
    if (self.croppingStyle == TOCropViewCroppingStyleCircular) {
        [picker pushViewController:cropController animated:YES];
    }
    else { //otherwise dismiss, and then present from the main controller
        [picker dismissViewControllerAnimated:YES completion:^{
            [self presentViewController:cropController animated:YES completion:nil];
        }];
    }
    
    
    
    assetURL = [info objectForKey:UIImagePickerControllerReferenceURL];
    
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark
#pragma mark - Cropper Delegate -
- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    self.imgProfile.image = image;
//    [self.btnProfile setImage:image forState:UIControlStateNormal];
//    
//    self.btnProfile.clipsToBounds = YES;
//    self.btnProfile.layer.cornerRadius = (self.btnProfile.frame.size.width / 2);//half of the width
//    self.btnProfile.layer.borderColor=[UIColor blackColor].CGColor;
//    self.btnProfile.layer.borderWidth=1.0f;
    isPictureAdded=@"1";
    
    MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.imgProfile animated:YES];
    // hud.mode = MBProgressHUDAnimationFade;
    // hud.label.text = @"";
    
    
    __block NSString *fileName = nil;
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library assetForURL:assetURL resultBlock:^(ALAsset *asset)  {
        fileName = asset.defaultRepresentation.filename;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyyMMddHHmmssSSS"];
        NSString *localDateString = [dateFormatter stringFromDate:[NSDate date]];
        // NSLog(@"%@",localDateString);
        
        NSArray* arrExt = [fileName componentsSeparatedByString: @"."];
        NSString* strExt = [arrExt objectAtIndex: arrExt.count-1];
        
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setValue:[dictDetails valueForKey:@"chatRoomId"] forKey:@"groupId"];
        [dictParam setValue:appDelegate.strID forKey:@"from"];
        NSString *getGID = [dictParam valueForKey:@"groupId"];
        UploadFileName=[NSString stringWithFormat:@"%@.%@",getGID,strExt];
        
        
        //image you want to upload
        UIImage *imageToUpload=[self compressImage:image];
        
        //convert uiimage to
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",UploadFileName]];
        [UIImagePNGRepresentation(imageToUpload) writeToFile:filePath atomically:YES];
        
        NSURL* fileUrl = [NSURL fileURLWithPath:filePath];
        
        AWSS3TransferUtilityUploadCompletionHandlerBlock completionHandler = ^(AWSS3TransferUtilityUploadTask *task, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // Do something e.g. Alert a user for transfer completion.
                // On failed uploads, `error` contains the error object.
                [MBProgressHUD hideHUDForView:self.imgProfile animated:YES];
                
                NSLog(@"%@",error);
                [CustomAlbum addNewAssetWithImage:imageToUpload toAlbum:[CustomAlbum getMyAlbumWithName:CSAlbum] onSuccess:^(NSString *ImageId) {
                    NSLog(@"image saved");
                    
                    //
                    NSString *strImageURL;
                    
                    if ([isPictureAdded isEqualToString:@"1"])
                    {
                        strImageURL=[NSString stringWithFormat:@"%@%@",FILE_AWS_IMAGE_BASE_URL,UploadFileName];
                       
                        [[appDelegate.socket emitWithAck:@"editGroupImage" with:@[@{@"groupId": getGID,@"groupImage": strImageURL,@"from":appDelegate.strID }]] timingOutAfter:2 callback:^(NSArray* data) {
                            NSLog(@"%@",data);
                            NSString *checkAck=[data objectAtIndex:0];
                            NSInteger getack=[checkAck integerValue];
                            NSLog(@"%ld",(long)getack);
                            if (getack == 1)
                            {
                                NSString *strContent=[NSString stringWithFormat:@"You have changed groupImage"];
                                long long milliSeconds=[self convertDateToMilliseconds:[NSDate date]];
                                NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
                                
                                NSUUID *uuid=[NSUUID UUID];
                                NSString *strMgsId=[NSString stringWithFormat:@"%@",uuid];
                                
                                
                                
                                
                                
                                BOOL succ = [db_class updateGroupImage:[dictDetails valueForKey:@"chatRoomId"]  groupImage:strImageURL];                            [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];
                                UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                MainTabBarVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainTabBarVC"];
                                [self.navigationController pushViewController:hme animated:YES];
                                
                            }
                            
                            
                            
                            else
                            {
                                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                                // Configure for text only and offset down
                                hud.mode = MBProgressHUDModeText;
                                hud.label.text = @"No Ack";
                                hud.margin = 10.f;
                                hud.yOffset = 150.f;
                                hud.removeFromSuperViewOnHide = YES;
                                [hud hideAnimated:YES afterDelay:3];
                            }
                        }];

                        
                        
                    }
                    else
                    {
                        strImageURL=@"";
                        
                    }

                    
                    
                    
                    
                    
                }
                                          onError:^(NSError *error)
                 {
                     NSLog(@"probelm in saving image");
                 }];
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                
                // Configure for text only and offset down
                hud.mode = MBProgressHUDModeText;
                hud.label.text = @"Profile picture uploaded";
                hud.margin = 10.f;
                hud.yOffset = 150.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hideAnimated:YES afterDelay:3];
            });
        };
        
        AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility defaultS3TransferUtility];
        [[transferUtility uploadFile:fileUrl bucket:FILE_BUCKET_NAME key:UploadFileName contentType:@"image/png" expression:nil completionHandler:completionHandler]
         continueWithBlock:^id(AWSTask *task) {
            if (task.error) {
                [MBProgressHUD hideHUDForView:self.imgProfile animated:YES];
                NSLog(@"Error: %@", task.error);
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                
                // Configure for text only and offset down
                hud.mode = MBProgressHUDModeText;
                hud.label.text = @"Profile picture not uploaded";
                hud.margin = 10.f;
                hud.yOffset = 150.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hideAnimated:YES afterDelay:3];
                
            }
//            if (task.exception) {
//                [MBProgressHUD hideHUDForView:self.imgProfile animated:YES];
//                NSLog(@"Exception: %@", task.exception);
//            }
            if (task.result) {
                AWSS3TransferUtilityUploadTask *uploadTask = task.result;
                // Do something with uploadTask.
                NSLog(@"%@",uploadTask);
            }
            
            return nil;
        }];
        
    }failureBlock:^(NSError *error)
     {
         NSLog(@"%@",error);
     }];
    
    [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (UIImage *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth) {
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
}
//tableview

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger getRow;
    NSLog(@"isadminArray:%@",isAdmindataArray);
    NSLog(@"updateAdminArray:%@",updatedAdminArray);
    NSLog(@"isnotAdmindataArray:%@",isnotAdminDataArray);
    
    if (tableView.tag==11)
    {
        getRow=isAdmindataArray.count;
    }
    else if (tableView.tag==111)
    {
        getRow=updatedAdminArray.count;
    }
    else if (tableView.tag==1111)
    {
        getRow=isnotAdminDataArray.count;
    }
    return getRow;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyCellIdentifier = @"MyCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyCellIdentifier];
    
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyCellIdentifier];
    }
    if (tableView.tag==11)
    {
        cell.textLabel.text=[isAdmindataArray objectAtIndex:indexPath.row];
    }
    else if (tableView.tag==111)
    {
        cell.textLabel.text=[updatedAdminArray objectAtIndex:indexPath.row];
        
    }
    else if (tableView.tag==1111)
    {
        cell.textLabel.text=[isnotAdminDataArray objectAtIndex:indexPath.row];
        
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     [self RefreshContacts];
    [self refreshChats];

    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:[dictDetails valueForKey:@"chatRoomId"] forKey:@"groupId"];
    [dictParam setValue:appDelegate.strID forKey:@"from"];
    NSString *getGID = [dictParam valueForKey:@"groupId"];
    NSLog(@"%@",passParticipantsId);
    if (tableView.tag==11)
    {
     if (indexPath.row==0)
     {
         viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
         [viewForAudioOrVideo removeFromSuperview];
         for (int i =0; i<arrContacts.count; i++)
         {
             NSDictionary *checkZoeidDict=[arrContacts objectAtIndex:i];
             NSString *strid=[checkZoeidDict valueForKey:@"zoechatid"];
             if ([strid isEqualToString:passParticipantsId] )
             {
                 NSDictionary *dictToSendSingleChat=[db_class getOneChat:passParticipantsId];
                 NSLog(@"%@",dictToSendSingleChat);
                 if (dictToSendSingleChat.count==0)
                 {
                     getMessageDict=[arrContacts objectAtIndex:i];
                 }
                 else
                 {
                     getMessageDict=[db_class getOneChat:passParticipantsId];
                     [getMessageDict setValue:[checkZoeidDict valueForKey:@"name"] forKey:@"name"];
                     [getMessageDict setValue:[checkZoeidDict valueForKey:@"image"] forKey:@"image"];
                     NSLog(@"%@",getMessageDict);
                 }
                 
             }
             
         }
         [self performSegueWithIdentifier:@"show_message" sender:nil];
     }
    else if (indexPath.row==1)
    {
        NSDictionary *dictToSendSingleChat=[db_class getOneChat:passParticipantsId];
        viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
        [viewForAudioOrVideo removeFromSuperview];
        
        for (int i =0; i<arrContacts.count; i++)
        {
            NSDictionary *checkZoeidDict=[arrContacts objectAtIndex:i];
            //NSDictionary *dictArt=[arrForTableView objectAtIndex:i];
            // NSString *strid1=[dictArt valueForKey:@"chatRoomId"];
            
            appDelegate.isfromView=@"Yes";
            NSString *strid=[checkZoeidDict valueForKey:@"zoechatid"];
            if ([strid isEqualToString:passParticipantsId] )
            {
                //NSDictionary *dictToSendSingleChat=[db_class getOneChat:passParticipantsId];
                NSLog(@"%@",dictToSendSingleChat);
                if (dictToSendSingleChat.count==0)
                {
                    // getViewDict=[arrContacts objectAtIndex:i];
                    getViewDict=[db_class getOneChat:passParticipantsId];
                    [getViewDict setValue:[checkZoeidDict valueForKey:@"name"] forKey:@"name"];
                    [getViewDict setValue:[checkZoeidDict valueForKey:@"image"] forKey:@"image"];
                    [getViewDict setValue:[checkZoeidDict valueForKey:@"zoechatid"] forKey:@"chatRoomId"];
                    [getViewDict setValue:@"0" forKey:@"chatRoomType"];
                    [getViewDict setValue:@"" forKey:@"groupImage"];
                    [getViewDict setValue:@"" forKey:@"groupName"];
                    NSLog(@"%@",getViewDict);
                    
                    
                }
                else
                {
                    getViewDict=[db_class getOneChat:passParticipantsId];
                    [getViewDict setValue:[checkZoeidDict valueForKey:@"name"] forKey:@"name"];
                    [getViewDict setValue:[checkZoeidDict valueForKey:@"image"] forKey:@"image"];
                    NSLog(@"%@",getViewDict);
                }
                
            }
            
        }
        
        UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SingleProfileVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"SingleProfileVC"];
        appDelegate.ViewDict=getViewDict;
        [self.navigationController pushViewController:hme animated:YES];
        
    }
    else if (indexPath.row==2)
    {
        [[appDelegate.socket emitWithAck:@"makeAdmin" with:@[@{@"groupId": getGID,@"participantId":passParticipantsId ,@"from":appDelegate.strID }]] timingOutAfter:2 callback:^(NSArray* data) {
            NSLog(@"%@",data);
            
            
            NSString *checkAck=[data objectAtIndex:0];
            NSInteger getack=[checkAck integerValue];
            NSLog(@"%ld",(long)getack);
            if (getack == 1)
            {
                NSArray *arrdata=[db_class getParticipants:getgroupId];
                for (int i=0; i<arrdata.count; i++)
                {
                    NSDictionary *dictParti=[arrdata objectAtIndex:i];
                    NSString *userId=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"userId"]];
                    if ([getgroupId isEqualToString:getGID])
                    {
                        if ([passParticipantsId isEqualToString:userId])
                        {
                            BOOL success = [db_class updateAdminParticipant:@"1" userId:passParticipantsId];
                            viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
                            [viewForAudioOrVideo removeFromSuperview];
                            
                            
                            
                            
//                            NSString *strContent=[NSString stringWithFormat:@"Now you are Admin"];
//                            long long milliSeconds=[self convertDateToMilliseconds:[NSDate date]];
//                            NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
//                            
//                            NSUUID *uuid=[NSUUID UUID];
//                            NSString *strMgsId=[NSString stringWithFormat:@"%@",uuid];
//                            
//                            BOOL isAvailinChatsTable=NO;
//                            isAvailinChatsTable=[db_class doesChatExist:appDelegate.strGroupIdToAddParticipants];
//                            
//                            BOOL success1=NO;
//                            if (isAvailinChatsTable==NO)
//                            {
//                                success=[db_class insertChats:@"" chatRoomId:appDelegate.strGroupIdToAddParticipants chatRoomType:@"1" sender:@"" lastMessage:strContent lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"header" unReadCount:@"1" groupName:lblNav.text  groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
//                                if (success==YES)
//                                {
//                                    isAvailinChatsTable=YES;
//                                }
//                            }
//                            else
//                            {
//                                BOOL success1 = [db_class updateLastMessage: appDelegate.strGroupIdToAddParticipants sender:@"" lastMessage:strContent lastMessageStatus:@"delivered" lastMessageTime:strMilliSeconds lastMessagesType:@"header" unReadCount:@"1"];
//                                NSLog(@"success");
//                            }
//                            
//                            
//                            BOOL succe=[db_class insertChatMessages:strMgsId userId:appDelegate.strID groupId:appDelegate.strGroupIdToAddParticipants chatRoomType:@"1" content:strContent contentType:@"header" contentStatus:@"delivered" sender:@"" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0"];

                            
                        }
                    }
                }
                
                
            }
            
            
            
            else
            {
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                // Configure for text only and offset down
                hud.mode = MBProgressHUDModeText;
                hud.label.text = @"No Ack";
                hud.margin = 10.f;
                hud.yOffset = 150.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hideAnimated:YES afterDelay:3];
            }
        }];

    }
    else if (indexPath.row==3)
    {
        [[appDelegate.socket emitWithAck:@"removeParticipant" with:@[@{@"groupId": getGID,@"participantId":passParticipantsId}]] timingOutAfter:2 callback:^(NSArray* data) {
            NSLog(@"%@",data);
            
            NSString *checkAck=[data objectAtIndex:0];
            NSInteger getack=[checkAck integerValue];
            NSLog(@"%ld",(long)getack);
            if (getack == 1)
            {
                NSArray *arrdata=[db_class getParticipants:getgroupId];
                for (int i=0; i<arrdata.count; i++)
                {
                    NSDictionary *dictParti=[arrdata objectAtIndex:i];
                    NSString *userId=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"userId"]];
                    if ([getgroupId isEqualToString:getGID])
                    {
                        if ([passParticipantsId isEqualToString:userId])
                        {
                            BOOL success = [db_class deleteParticipant:userId];
                            viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
                            [viewForAudioOrVideo removeFromSuperview];
                            
                            
                        }
                    }
                }
                
                
            }
            
            
            
            else
            {
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                // Configure for text only and offset down
                hud.mode = MBProgressHUDModeText;
                hud.label.text = @"No Ack";
                hud.margin = 10.f;
                hud.yOffset = 150.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hideAnimated:YES afterDelay:3];
            }
        }];
     }
    }
    else if (tableView.tag==111)
    {
        if (indexPath.row==0)
        {
            //NSDictionary *dictToSendSingleChat=[db_class getOneChat:passParticipantsId];
            viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087651];
            [viewForAudioOrVideo removeFromSuperview];
            // dictDetails=dictToSendSingleChat;
            for (int i =0; i<arrContacts.count; i++)
            {
                NSDictionary *checkZoeidDict=[arrContacts objectAtIndex:i];
                //NSDictionary *dictArt=[arrForTableView objectAtIndex:i];
                // NSString *strid1=[dictArt valueForKey:@"chatRoomId"];
                
                
                NSString *strid=[checkZoeidDict valueForKey:@"zoechatid"];
                if ([strid isEqualToString:passParticipantsId] )
                {
                    NSDictionary *dictToSendSingleChat=[db_class getOneChat:passParticipantsId];
                    NSLog(@"%@",dictToSendSingleChat);
                    if (dictToSendSingleChat.count==0)
                    {
                        getMessageDict=[arrContacts objectAtIndex:i];
                    }
                    else
                    {
                        getMessageDict=[db_class getOneChat:passParticipantsId];
                        [getMessageDict setValue:[checkZoeidDict valueForKey:@"name"] forKey:@"name"];
                        [getMessageDict setValue:[checkZoeidDict valueForKey:@"image"] forKey:@"image"];
                        NSLog(@"%@",getMessageDict);
                    }
                    
                }
                
            }
            [self performSegueWithIdentifier:@"show_message" sender:nil];
        }
        else if (indexPath.row==1)
        {
            NSDictionary *dictToSendSingleChat=[db_class getOneChat:passParticipantsId];
            viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087651];
            [viewForAudioOrVideo removeFromSuperview];
            
            for (int i =0; i<arrContacts.count; i++)
            {
                NSDictionary *checkZoeidDict=[arrContacts objectAtIndex:i];
                //NSDictionary *dictArt=[arrForTableView objectAtIndex:i];
                // NSString *strid1=[dictArt valueForKey:@"chatRoomId"];
                
                appDelegate.isfromView=@"Yes";
                NSString *strid=[checkZoeidDict valueForKey:@"zoechatid"];
                if ([strid isEqualToString:passParticipantsId] )
                {
                    //NSDictionary *dictToSendSingleChat=[db_class getOneChat:passParticipantsId];
                    NSLog(@"%@",dictToSendSingleChat);
                    if (dictToSendSingleChat.count==0)
                    {
                        // getViewDict=[arrContacts objectAtIndex:i];
                        getViewDict=[db_class getOneChat:passParticipantsId];
                        [getViewDict setValue:[checkZoeidDict valueForKey:@"name"] forKey:@"name"];
                        [getViewDict setValue:[checkZoeidDict valueForKey:@"image"] forKey:@"image"];
                        [getViewDict setValue:[checkZoeidDict valueForKey:@"zoechatid"] forKey:@"chatRoomId"];
                        [getViewDict setValue:@"0" forKey:@"chatRoomType"];
                        [getViewDict setValue:@"" forKey:@"groupImage"];
                        [getViewDict setValue:@"" forKey:@"groupName"];
                        NSLog(@"%@",getViewDict);
                        
                        
                    }
                    else
                    {
                        getViewDict=[db_class getOneChat:passParticipantsId];
                        [getViewDict setValue:[checkZoeidDict valueForKey:@"name"] forKey:@"name"];
                        [getViewDict setValue:[checkZoeidDict valueForKey:@"image"] forKey:@"image"];
                        NSLog(@"%@",getViewDict);
                    }
                    
                }
                
            }
            
            UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SingleProfileVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"SingleProfileVC"];
            appDelegate.ViewDict=getViewDict;
            [self.navigationController pushViewController:hme animated:YES];
            
        }
        else if (indexPath.row==2)
        {
            [[appDelegate.socket emitWithAck:@"removeParticipant" with:@[@{@"groupId": getGID,@"participantId":passParticipantsId}]] timingOutAfter:2 callback:^(NSArray* data) {
                NSLog(@"%@",data);
                NSString *checkAck=[data objectAtIndex:0];
                NSInteger getack=[checkAck integerValue];
                NSLog(@"%ld",(long)getack);
                if (getack == 1)
                {
                    NSArray *arrdata=[db_class getParticipants:getgroupId];
                    for (int i=0; i<arrdata.count; i++)
                    {
                        NSDictionary *dictParti=[arrdata objectAtIndex:i];
                        NSString *userId=[NSString stringWithFormat:@"%@",[dictParti valueForKey:@"userId"]];
                        if ([getgroupId isEqualToString:getGID])
                        {
                            if ([passParticipantsId isEqualToString:userId])
                            {
                                BOOL success = [db_class deleteParticipant:userId];
                                viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087651];
                                [viewForAudioOrVideo removeFromSuperview];
                                
                                
                            }
                        }
                    }
                    
                    
                }
                
                
                
                else
                {
                    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                    // Configure for text only and offset down
                    hud.mode = MBProgressHUDModeText;
                    hud.label.text = @"No Ack";
                    hud.margin = 10.f;
                    hud.yOffset = 150.f;
                    hud.removeFromSuperViewOnHide = YES;
                    [hud hideAnimated:YES afterDelay:3];
                }
            }];
        }

    }
    else if(tableView.tag==1111)
    {
        if (indexPath.row==0)
        {
            //NSDictionary *dictToSendSingleChat=[db_class getOneChat:passParticipantsId];
            viewForAudioOrVideo2=(UIView *)[self.navigationController.view viewWithTag:9087652];
            [viewForAudioOrVideo2 removeFromSuperview];
           // dictDetails=dictToSendSingleChat;
            for (int i =0; i<arrContacts.count; i++)
            {
                NSDictionary *checkZoeidDict=[arrContacts objectAtIndex:i];
                //NSDictionary *dictArt=[arrForTableView objectAtIndex:i];
               // NSString *strid1=[dictArt valueForKey:@"chatRoomId"];


                NSString *strid=[checkZoeidDict valueForKey:@"zoechatid"];
                if ([strid isEqualToString:passParticipantsId] )
                {
                    NSDictionary *dictToSendSingleChat=[db_class getOneChat:passParticipantsId];
                    NSLog(@"%@",dictToSendSingleChat);
                    if (dictToSendSingleChat.count==0)
                    {
                        getMessageDict=[arrContacts objectAtIndex:i];
                    }
                    else
                    {
                    getMessageDict=[db_class getOneChat:passParticipantsId];
                        [getMessageDict setValue:[checkZoeidDict valueForKey:@"name"] forKey:@"name"];
                        [getMessageDict setValue:[checkZoeidDict valueForKey:@"image"] forKey:@"image"];
                        NSLog(@"%@",getMessageDict);
                    }

                }
                
            }
            [self performSegueWithIdentifier:@"show_message" sender:nil];
        }
        else if (indexPath.row==1)
        {
            NSDictionary *dictToSendSingleChat=[db_class getOneChat:passParticipantsId];
            viewForAudioOrVideo2=(UIView *)[self.navigationController.view viewWithTag:9087652];
            [viewForAudioOrVideo2 removeFromSuperview];
            
            for (int i =0; i<arrContacts.count; i++)
            {
                NSDictionary *checkZoeidDict=[arrContacts objectAtIndex:i];
                //NSDictionary *dictArt=[arrForTableView objectAtIndex:i];
                // NSString *strid1=[dictArt valueForKey:@"chatRoomId"];
                
                appDelegate.isfromView=@"Yes";
                NSString *strid=[checkZoeidDict valueForKey:@"zoechatid"];
                if ([strid isEqualToString:passParticipantsId] )
                {
                    //NSDictionary *dictToSendSingleChat=[db_class getOneChat:passParticipantsId];
                    NSLog(@"%@",dictToSendSingleChat);
                    if (dictToSendSingleChat.count==0)
                    {
                       // getViewDict=[arrContacts objectAtIndex:i];
                        getViewDict=[db_class getOneChat:passParticipantsId];
                        [getViewDict setValue:[checkZoeidDict valueForKey:@"name"] forKey:@"name"];
                        [getViewDict setValue:[checkZoeidDict valueForKey:@"image"] forKey:@"image"];
                        [getViewDict setValue:[checkZoeidDict valueForKey:@"zoechatid"] forKey:@"chatRoomId"];
                        [getViewDict setValue:@"0" forKey:@"chatRoomType"];
                        [getViewDict setValue:@"" forKey:@"groupImage"];
                        [getViewDict setValue:@"" forKey:@"groupName"];
                        NSLog(@"%@",getViewDict);


                    }
                    else
                    {
                        getViewDict=[db_class getOneChat:passParticipantsId];
                        [getViewDict setValue:[checkZoeidDict valueForKey:@"name"] forKey:@"name"];
                        [getViewDict setValue:[checkZoeidDict valueForKey:@"image"] forKey:@"image"];
                        NSLog(@"%@",getViewDict);
                    }
                    
                }
                
            }

            UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SingleProfileVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"SingleProfileVC"];
            appDelegate.ViewDict=getViewDict;
            [self.navigationController pushViewController:hme animated:YES];
            
        }
        
        
    }

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"show_message"])
    {
        SingleChatVC *Single=[segue destinationViewController];
        Single.dictDetails=getMessageDict;
    }
}

-(void)RefreshContacts
{
    arrContacts=[[NSMutableArray alloc]init];
    NSMutableArray *arrZoeCont=[[NSMutableArray alloc]init];
    NSArray *arrCont=[db_class getUsers];
    if (arrCont.count!=0)
    {
        for (int i=0; i<arrCont.count; i++)
        {
            NSDictionary *dictCont=[arrCont objectAtIndex:i];
            NSString *strShowInContacts=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"showincontactspage"]];
            NSString *strzoe=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"zoechatid"]];
            if ([strShowInContacts boolValue]==true)
            {
                if (![appDelegate.strID isEqualToString:strzoe])
                {
                    [arrZoeCont addObject:dictCont];
                }
                
            }
        }
    }
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    arrContacts=[arrZoeCont sortedArrayUsingDescriptors:@[sort]];
    
    
}
-(void)refreshChats
{
    arrChats=[db_class getChats];
    
    if (arrChats.count!=0)
    {
        for (int i=0; i<arrChats.count; i++)
        {
            NSMutableDictionary *dictSing=[arrChats objectAtIndex:i];
            NSDictionary *dictInfo =[db_class getUserInfo:[dictSing valueForKey:@"chatRoomId"]];
            NSString *strName=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"name"]];
            NSString *strImage=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"image"]];
            [dictSing setObject:strName forKey:@"name"];
            [dictSing setObject:strImage forKey:@"image"];
            NSLog(@"%@",dictSing);
            
            [arrChats replaceObjectAtIndex:i withObject:dictSing];
        }
    }
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"dateTime" ascending:NO];
    arrChats=[arrChats sortedArrayUsingDescriptors:@[sort]];
    
    arrForTableView=[NSMutableArray arrayWithArray:arrChats];
    
}

@end
