//
//  NotificationSettingVC.h
//  ZoeChat
//
//  Created by macmini on 30/05/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationSettingVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) IBOutlet UIView *MessageInfoView;
@property (strong, nonatomic) IBOutlet UIView *GroupInfoView;
@property (strong, nonatomic) IBOutlet UIView *InappNotiView;
@property (strong, nonatomic) IBOutlet UIView *PreviewView;
@property (strong, nonatomic) IBOutlet UIView *resetNotiView;
@property (strong, nonatomic) IBOutlet UISwitch *msgNotiSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *groupNotiSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *previewSwitch;
@property (strong, nonatomic) IBOutlet UIButton *soundBtn;
@property (strong, nonatomic) IBOutlet UIButton *groupSoundBtn;
@property (strong, nonatomic) IBOutlet UIButton *inappNotiBtn;
@property (strong, nonatomic) IBOutlet UIButton *resetBtn;
- (IBAction)ClickonMsgSound:(id)sender;
- (IBAction)ClickOnGrpSound:(id)sender;
- (IBAction)ClickOnInappNoti:(id)sender;
- (IBAction)clickOnReset:(id)sender;
- (IBAction)ClickOnMsgNotiswitch:(id)sender;
- (IBAction)clickOnGroupNotiSwitch:(id)sender;
- (IBAction)clickOnShowPreviewswitch:(id)sender;
- (IBAction)ClickOnBackBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *showSoundLbl;
@property (strong, nonatomic) IBOutlet UILabel *showGrpSoundLbl;
@property (strong, nonatomic)NSIndexPath *getMsgSoundIndexPath;
@property (strong, nonatomic)NSIndexPath *getGrpSoundIndexPath;


@end
