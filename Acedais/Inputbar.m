//
//  Inputbar.h
//  Whatsapp
//
//  Created by Rafael Castro on 7/11/15.
//  Copyright (c) 2015 HummingBird. All rights reserved.
//

#import "Inputbar.h"
#import "HPGrowingTextView.h"
#import "Constants.h"
#import <AVFoundation/AVFoundation.h>
#import "AppDelegate.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <AudioToolbox/AudioServices.h>
#define DOCUMENTS_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]

@interface Inputbar() <HPGrowingTextViewDelegate, AVAudioRecorderDelegate,UIGestureRecognizerDelegate>
{
    NSMutableDictionary *recordSetting;
    NSString *recorderFilePath;
    AVAudioRecorder *recorder;
    AppDelegate *appDelegate;
    
   
    BOOL isCalled;
    UIPanGestureRecognizer *pan;
    BOOL AudioEnabled;

    
    
    

}
@property (nonatomic, strong) HPGrowingTextView *textView;
@property (nonatomic, strong) UIButton *rightButton;
@property (nonatomic, strong) UIButton *leftButton;
@end

#define RIGHT_BUTTON_SIZE 45
#define LEFT_BUTTON_SIZE 45

@implementation Inputbar

-(id)init
{
    self = [super init];
    if (self)
    {
        [self addContent];
    }
    return self;
}
-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    {
        [self addContent];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        [self addContent];
    }
    return self;
}
-(void)addContent
{
    [self addTextView];
    [self addRightButton];
    [self addLeftButton];
    isCalled=NO;
    AudioEnabled=NO;
   
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
}
-(void)addTextView
{
    CGSize size = self.frame.size;
    _textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(LEFT_BUTTON_SIZE,
                                                                    5,
                                                                    size.width - LEFT_BUTTON_SIZE - RIGHT_BUTTON_SIZE,
                                                                    size.height)];
    _textView.isScrollable = NO;
    _textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    
    _textView.minNumberOfLines = 1;
    _textView.maxNumberOfLines = 6;
    // you can also set the maximum height in points with maxHeight
    // textView.maxHeight = 200.0f;
    _textView.returnKeyType = UIReturnKeyGo; //just as an example
    _textView.font = [UIFont systemFontOfSize:15.0f];
    _textView.delegate = self;
    _textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    _textView.backgroundColor = [UIColor whiteColor];
    _textView.placeholder = _placeholder;
    
    //textView.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    _textView.keyboardType = UIKeyboardTypeDefault;
    _textView.returnKeyType = UIReturnKeyDefault;
    _textView.enablesReturnKeyAutomatically = YES;
    //textView.scrollIndicatorInsets = UIEdgeInsetsMake(0.0, -1.0, 0.0, 1.0);
    //textView.textContainerInset = UIEdgeInsetsMake(8.0, 4.0, 8.0, 0.0);
    _textView.layer.cornerRadius = 5.0;
    _textView.layer.borderWidth = 0.5;
    _textView.layer.borderColor =  [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:205.0/255.0 alpha:1.0].CGColor;
    
    _textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    // view hierachy
    [self addSubview:_textView];
}
-(void)addRightButton
{
    CGSize size = self.frame.size;
    self.rightButton = [[UIButton alloc] init];
    self.rightButton.frame = CGRectMake(size.width - RIGHT_BUTTON_SIZE, 0, RIGHT_BUTTON_SIZE, size.height);
    self.rightButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
     [self.rightButton setImage:[UIImage imageNamed:@"send button.png"] forState:UIControlStateNormal];//mic.png
    [self.rightButton setImage:[UIImage imageNamed:@"audio_mic"] forState:UIControlStateSelected];
    
    [self.rightButton addTarget:self action:@selector(didPressRightButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.rightButton];
    
    [self.rightButton setSelected:YES];
    
    
    [self.rightButton setImage:[UIImage imageNamed:@"audio_mic"] forState:UIControlStateSelected | UIControlStateHighlighted];
  
  
    
   
    pan = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(move:)];
    pan.delegate = self;
    [self.rightButton addGestureRecognizer:pan];
    
    
    [self.rightButton addTarget:self action:@selector(holdDown:) forControlEvents:UIControlEventTouchDown];
   
    
    
   
    
}
- (void)holdDown:(UIButton *)sender
{
    
   
    NSString *trimmedString = [self.textView.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    NSLog(@"%@",trimmedString);
    if ([trimmedString isEqualToString:@""])

{
    isCalled=YES;
     AudioEnabled=YES;
    [self startRecording];
    recorder.delegate=self;
}
    else
    {
        
    }

}



-(void)move:(UIPanGestureRecognizer*)sender {
    [self bringSubviewToFront:sender.view];
    CGPoint translatedPoint = [sender translationInView:sender.view.superview];
    
    if (sender.state == UIGestureRecognizerStateBegan) {
      // CGFloat firstX = sender.view.center.x;
       // CGFloat firstY = sender.view.center.y;
        
     

    }
    
    else if (sender.state == UIGestureRecognizerStateChanged) {

    translatedPoint = CGPointMake(sender.view.center.x+translatedPoint.x, sender.view.center.y);
    
        if (translatedPoint.x >= 285)
        {
            isCalled=YES;
        }
        else
        {
            isCalled=NO;
           
        }
        
    [sender.view setCenter:translatedPoint];
    [sender setTranslation:CGPointZero inView:sender.view];
               RecordLbl.frame=CGRectMake(RecordLbl.frame.origin.x - 1.0, RecordLbl.frame.origin.y, RecordLbl.frame.size.width, RecordLbl.frame.size.height);
    }
    
    else if (sender.state == UIGestureRecognizerStateEnded) {
       // isCalled=NO;

         [self stopRecording];
        CGSize size = self.frame.size;
        
       
         self.rightButton.frame=CGRectMake(size.width - RIGHT_BUTTON_SIZE , self.rightButton.frame.origin.y, self.rightButton.frame.size.width, self.rightButton.frame.size.height);
        
        [sliderView removeFromSuperview];
        self.rightButton.backgroundColor=[UIColor clearColor];
        
        
       
    }
}
-(void)addLeftButton
{
    CGSize size = self.frame.size;
    self.leftButton = [[UIButton alloc] init];
    self.leftButton.frame = CGRectMake(0, 0, LEFT_BUTTON_SIZE, size.height);
    self.leftButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin;
    [self.leftButton setImage:[UIImage imageNamed:@"Emoji.png"] forState:UIControlStateNormal];
    
    [self.leftButton addTarget:self action:@selector(didPressLeftButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.leftButton];
}
-(void)resignFirstResponder
{
    [_textView resignFirstResponder];
}
-(NSString *)text
{
    return _textView.text;
}


#pragma mark - Delegate

-(void)didPressRightButton:(UIButton *)sender
{

    
    NSString *trimmedString = [self.textView.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    NSLog(@"%@",trimmedString);
    if ([trimmedString isEqualToString:@""])
    {
        [self stopRecording];

    }
    else
    {
        if (self.rightButton.isSelected) return;
        if ([trimmedString isEqualToString:@"\n"])
        {
            self.textView.text = @"";
            return;
        }
        
        [self.delegate inputbarDidPressRightButton:self];
        self.textView.text = @"";
    }
    
    
}
-(void)didPressLeftButton:(UIButton *)sender
{
    [self.delegate inputbarDidPressLeftButton:self];
}

#pragma mark - Set Methods

-(void)setPlaceholder:(NSString *)placeholder
{
    _placeholder = placeholder;
    _textView.placeholder = placeholder;
}
-(void)setLeftButtonImage:(UIImage *)leftButtonImage
{
    [self.leftButton setImage:leftButtonImage forState:UIControlStateNormal];
}
-(void)setRightButtonTextColor:(UIColor *)righButtonTextColor
{
    [self.rightButton setTitleColor:righButtonTextColor forState:UIControlStateNormal];
}
-(void)setRightButtonText:(NSString *)rightButtonText
{
    [self.rightButton setTitle:rightButtonText forState:UIControlStateNormal];
}

#pragma mark - TextViewDelegate




-(void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    AppDelegate *app = [[UIApplication sharedApplication] delegate];
    float diff = (growingTextView.frame.size.height - height);
    NSLog(@"%f",diff);
    CGRect r = self.frame;
    r.size.height -= diff;
    r.origin.y += diff;
    self.frame = r;
    app.dynamicRowHeight = r.size.height;
    NSLog(@"%f",app.dynamicRowHeight);

   // app.heightStr = @"TRUE";
    if (self.delegate && [self.delegate respondsToSelector:@selector(inputbarDidChangeHeight:)])
    {
        [self.delegate inputbarDidChangeHeight:self.frame.size.height];
    }
}
-(void)growingTextViewDidBeginEditing:(HPGrowingTextView *)growingTextView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(inputbarDidBecomeFirstResponder:)])
    {
        [self.delegate inputbarDidBecomeFirstResponder:self];
    }
}
- (void)growingTextViewDidChange:(HPGrowingTextView *)growingTextView
{
    NSString *text = [growingTextView.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    const char * _char = [text cStringUsingEncoding:NSUTF8StringEncoding];
    int isBackSpace = strcmp(_char, "\b");
    
    if (isBackSpace == -8) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ClearView" object:nil];
            }
    if ([text isEqualToString:@""] || [text isEqualToString:@"\n"])
    {
        [self.rightButton setSelected:YES];
        
       
    }
    else
        [self.rightButton setSelected:NO];
    
    NSString *strLength=[NSString stringWithFormat:@"%lu",(unsigned long)text.length];
//    if (text.length-1)
//    {
//        NSLog(@"Backspace was pressed");
//
//    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"InputBar_Length" object:strLength];
    
    // if a timer is already active, prevent it from firing
    if (self.searchTimer != nil) {
        [self.searchTimer invalidate];
        self.searchTimer = nil;
    }
    
    // reschedule the search: in 1.0 second, call the searchForKeyword: method on the new textfield content
    self.searchTimer = [NSTimer scheduledTimerWithTimeInterval: 2.0
                                                        target: self
                                                      selector: @selector(searchForURL:)
                                                      userInfo: text
                                                       repeats: NO];

    }
- (void) searchForURL:(NSTimer *)timer
{
    // retrieve the keyword from user info
    NSString *keyword = (NSString*)timer.userInfo;
    
    // perform your search (stubbed here using NSLog)
    NSLog(@"Searching for keyword %@", keyword);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LinkURL" object:keyword];

    
    
}



-(void)buttonPosition
{
    CGFloat vallX= self.rightButton.frame.origin.x - 2;
    NSLog(@"self.rightButton.frame.origin.x: %f\n", self.rightButton.frame.origin.x);
    self.rightButton.frame=CGRectMake(vallX , self.rightButton.frame.origin.y, self.rightButton.frame.size.width, self.rightButton.frame.size.height);
    NSLog(@"value of vallX: %f\n", vallX);
    NSString* numberA = [NSString stringWithFormat:@"%.6f", vallX];
    if ([numberA isEqualToString:@"95.000000"])
    {
        /*CGSize size = self.frame.size;
        self.rightButton.frame=CGRectMake(size.width - RIGHT_BUTTON_SIZE , self.rightButton.frame.origin.y, self.rightButton.frame.size.width, self.rightButton.frame.size.height);
        self.rightButton.backgroundColor=[UIColor clearColor];
            [buttonTimer invalidate];
            buttonTimer=nil;
            */
        
       [self performSelector:@selector(SetButtonPosition) withObject:self afterDelay:1.0];
    }
}

-(void)SetButtonPosition
{
    
    CGSize size = self.frame.size;
    self.rightButton.frame=CGRectMake(size.width - RIGHT_BUTTON_SIZE , self.rightButton.frame.origin.y, self.rightButton.frame.size.width, self.rightButton.frame.size.height);
    //self.rightButton.backgroundColor=[UIColor clearColor];
    //[buttonTimer invalidate];
    //buttonTimer=nil;
}

- (void) startRecording{
    if (AudioEnabled==YES)
    {
        [self vibratePhone];
        self.rightButton.layer.cornerRadius=self.rightButton.frame.size.width/2;
        self.rightButton.clipsToBounds=YES;
        
        NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
        UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
        if (color != nil) {
            
            self.rightButton.backgroundColor=color;
            
            
        }
        else
        {
            self.rightButton.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
            
        }
        
        
        sliderView=[[UIView alloc]initWithFrame:CGRectMake(0, 5, 275, 35)];
        CATransition *transition = [CATransition animation];
        transition.duration = 0.2;
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [sliderView.layer addAnimation:transition forKey:nil];
        micImg=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
        [micImg setImage:[UIImage imageNamed:@"audio_mic"]];
        [self blinkForever:micImg];
        timelabel=[[UILabel alloc]initWithFrame:CGRectMake(26, 5, 50, 20)];
        RecordLbl=[[UILabel alloc]initWithFrame:CGRectMake(130, 7, 120, 20)];
        RecordLbl.font = [UIFont fontWithName:FONT_HEAVY size:15.0];
        RecordLbl.text=@"<Slide to Cancel";
        
        RecordLbl.textColor=[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.3];
        sliderView.layer.cornerRadius=10;
        sliderView.clipsToBounds=YES;
        //sliderView.layer.borderColor=[UIColor blackColor].CGColor;
        //sliderView.layer.borderWidth=0.5;
        sliderView.backgroundColor=[UIColor lightGrayColor];
        [sliderView addSubview:micImg];
        [sliderView addSubview:RecordLbl];
        [sliderView addSubview:timelabel];
        //[sliderView bringSubviewToFront:RecordLbl];
        
        
        [self addSubview:sliderView];
        
        
        myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateTime) userInfo:nil repeats:YES];
        
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        NSError *err = nil;
        //[audioSession setCategory :AVAudioSessionCategoryPlayAndRecord error:&err];
        [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];

        if(err){
            NSLog(@"audioSession: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
            return;
        }
        [audioSession setActive:YES error:&err];
        err = nil;
        if(err){
            NSLog(@"audioSession: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
            return;
        }
        
        recordSetting = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                         [NSNumber numberWithInt: kAudioFormatMPEG4AAC], AVFormatIDKey,
                         [NSNumber numberWithFloat:16000.0], AVSampleRateKey,
                         [NSNumber numberWithInt: 1], AVNumberOfChannelsKey,
                         nil];
        
        // Create a new dated file
        NSDate *now=[NSDate date];
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"yyyyMMddHHmmssSSS"];
        NSString *localDateString = [dateFormatter1 stringFromDate:now];
        
        recorderFilePath = [NSString stringWithFormat:@"%@/%@.m4a", DOCUMENTS_FOLDER, localDateString];
       // recorderFilePath = nil;
        if([recorderFilePath length] == 0)
        {
            NSLog(@"audioSession");
        }
        else
        {
        NSURL *url = [NSURL fileURLWithPath:recorderFilePath];
        err = nil;
        recorder = [[ AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:&err];
        if(!recorder){
            NSLog(@"recorder: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
            
            UIAlertView *alert =
            [[UIAlertView alloc] initWithTitle: @"Warning"
                                       message: [err localizedDescription]
                                      delegate: nil
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
            [alert show];
            
            return;
        }
        
        
        [recorder prepareToRecord];
        recorder.meteringEnabled = YES;
        
        BOOL audioHWAvailable = audioSession.inputIsAvailable;
        if (! audioHWAvailable) {
            UIAlertView *cantRecordAlert =
            [[UIAlertView alloc] initWithTitle: @"Warning"
                                       message: @"Audio input hardware not available"
                                      delegate: nil
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
            [cantRecordAlert show];
            
            return;
        }
        
        // start recording
        [recorder recordForDuration:(NSTimeInterval) 5000000];
        AudioEnabled=NO;

        
    }
    }
    else{
        AudioEnabled=YES;
    }
    
    
}

- (void) stopRecording{
     self.rightButton.backgroundColor=[UIColor clearColor];
    [myTimer invalidate];
    [sliderView removeFromSuperview];
    [recorder stop];
    if([recorderFilePath length] == 0)
    {
    }
    else
    {
    NSURL *url = [NSURL fileURLWithPath: recorderFilePath];
    NSError *err = nil;
    NSData *audioData = [NSData dataWithContentsOfFile:[url path] options: 0 error:&err];
    if(!audioData)
        NSLog(@"audio data: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
    
 }
}

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *) aRecorder successfully:(BOOL)flag
{
    if (isCalled == YES)
    {
      
    NSLog (@"audioRecorderDidFinishRecording:successfully:");
   
    NSArray *components = [recorderFilePath componentsSeparatedByString:@"/"];
    NSString *strAudioFileName = ((![components count])? nil :[components objectAtIndex:components.count-1] );
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:strAudioFileName forKey:@"audioFileName"];
    
    NSDictionary *theInfo =[NSDictionary dictionaryWithObjectsAndKeys:dictParam,@"AudioDict", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Audio_File_Created"
                                                        object:self
                                                      userInfo:theInfo];
        isCalled=NO;

    }
    else if (isCalled == NO)
    {
        NSLog (@"FailedAudio");
        isCalled=YES;
    }
    
    
}
-(void)blinkForever:(UIView*)view
{
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionAutoreverse|UIViewAnimationOptionRepeat animations:^{
        
        micImg.alpha = 0.0;
        
    } completion:nil];
}
- (void)updateTime {
    if([recorder isRecording])
    {
        
        float minutes = floor(recorder.currentTime/60);
        float seconds = recorder.currentTime - (minutes * 60);
        
        NSString *time = [[NSString alloc]
                          initWithFormat:@"%0.0f:%0.0f",
                          minutes, seconds];
       
        timelabel.text=time;
    }
}
- (void)vibratePhone;
{
    if([[UIDevice currentDevice].model isEqualToString:@"iPhone"])
    {
        AudioServicesPlaySystemSound (1352); //works ALWAYS as of this post
    }
    else
    {
        // Not an iPhone, so doesn't have vibrate
        // play the less annoying tick noise or one of your own
        AudioServicesPlayAlertSound (1105);
    }
}

@end
