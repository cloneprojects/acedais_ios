//
//  InviteFriendsVC.m
//  ZoeChat
//
//  Created by macmini on 03/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "InviteFriendsVC.h"
#import "Constants.h"
#import "DBClass.h"
#import "AppDelegate.h"
#import "InviteTblCell.h"
#import <MessageUI/MessageUI.h>
#import <SDWebImage/UIImageView+WebCache.h>


@interface InviteFriendsVC ()<MFMessageComposeViewControllerDelegate>
{
    NSMutableArray *arrContacts;
    DBClass *db_class;
    AppDelegate *appDelegate;
    NSIndexPath *getindepath;
}
@end

@implementation InviteFriendsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    db_class = [[DBClass alloc]init];
    arrContacts = [[NSMutableArray alloc]init];
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self RefreshContacts];
}
-(void)RefreshContacts
{
    
    arrContacts=[[NSMutableArray alloc]init];
    
    NSMutableArray *arrZoeCont=[[NSMutableArray alloc]init];
    NSArray *arrCont=[db_class getUsers];
    if (arrCont.count!=0)
    {
        for (int i=0; i<arrCont.count; i++)
        {
            NSDictionary *dictCont=[arrCont objectAtIndex:i];
            NSString *strShowInContacts=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"showincontactspage"]];
            NSString *strzoe=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"zoechatid"]];
            if ([strShowInContacts boolValue]==false)
            {
                if (![appDelegate.strID isEqualToString:strzoe])
                {
                    [arrZoeCont addObject:dictCont];
                }
                
            }
            
            
        }
    }
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    arrContacts=[arrZoeCont sortedArrayUsingDescriptors:@[sort]];
    
    [self.InviteFriendsTblView reloadData];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  60;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return arrContacts.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    InviteTblCell *cell=(InviteTblCell *)[tableView dequeueReusableCellWithIdentifier:@"InviteTblCell"];
    if (cell==nil) {
        cell=[[InviteTblCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InviteTblCell"];
    }
    NSString *InitialStr;
    UIColor *bgColor;
    [cell.LblImg setHidden:YES];
    NSDictionary *dictArt=[arrContacts objectAtIndex:indexPath.row];
    NSString *strName=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"name"]];
    cell.NameLbl.text=[strName capitalizedString];
    cell.NameLbl.font=[UIFont fontWithName:FONT_NORMAL size:16];
    InitialStr=[cell.NameLbl.text substringToIndex:1];
    if ([InitialStr isEqualToString:@"A"])
    {
        bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"B"])
    {
        bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"C"])
    {
        bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"D"])
    {
        bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"E"])
    {
        bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"F"])
    {
        bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"G"])
    {
        bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"H"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"I"])
    {
        bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"J"])
    {
        bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"K"])
    {
        bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"L"])
    {
        bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"M"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"N"])
    {
        bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"O"])
    {
        bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"P"])
    {
        bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"Q"])
    {
        bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"R"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"S"])
    {
        bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"T"])
    {
        bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"U"])
    {
        bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"V"])
    {
        bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"W"])
    {
        bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"X"])
    {
        bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"Y"])
    {
        bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"Z"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
    }

    NSString *strImageURL1=[dictArt valueForKey:@"image"];
    
        if ([strImageURL1 isEqualToString:@""])
        {
           
            
            [cell.LblImg setHidden:NO];
            
            [cell.imgview sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
            
            cell.LblImg.text=InitialStr;
            cell.LblImg.backgroundColor=bgColor;
        }
        else
    {
         [cell.imgview sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
    }
    

    cell.imgview.layer.cornerRadius=cell.imgview.frame.size.height/2;
    cell.imgview.clipsToBounds=YES;
    cell.LblImg.layer.cornerRadius=cell.imgview.frame.size.height/2;
    cell.LblImg.clipsToBounds=YES;
    cell.inviteBtn.layer.cornerRadius = 5; // this value vary as per your desire
    cell.inviteBtn.clipsToBounds = YES;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    [cell.inviteBtn addTarget:self action:@selector(ClickOnInvite:) forControlEvents:UIControlEventTouchUpInside];
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        
        cell.inviteBtn.backgroundColor=color;
        
        
        
    }
    else
    {
        cell.inviteBtn.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        
        
        
        
    }
    return cell;
}

-(IBAction)ClickOnInvite:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.InviteFriendsTblView];
    NSIndexPath *indexPath = [self.InviteFriendsTblView indexPathForRowAtPoint:buttonPosition];
    getindepath = indexPath;
    [self showPopView];
    
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            NSLog(@"Cancelled");
            break;
        case MessageComposeResultFailed:
            
            break;
        case MessageComposeResultSent:
            
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:NO completion:NULL];
}
-(void)showPopView
{
    UIView *viewForAudioOrVideo=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+self.navigationController.navigationBar.frame.size.height+50)];
    viewForAudioOrVideo.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.7f];
    viewForAudioOrVideo.tag=9087650;
    [self.navigationController.view addSubview:viewForAudioOrVideo];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onRemoveViewAudioVideo:)]; // this is the current problem like a lot of people out there...
    [viewForAudioOrVideo addGestureRecognizer:tap];
    
    UIView *viewForWhiteBG=[[UIView alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)-140, (viewForAudioOrVideo.frame.size.height/2)-50, 280, 100)];
    viewForWhiteBG.backgroundColor=[UIColor whiteColor];
    viewForWhiteBG.layer.cornerRadius=3;
    viewForWhiteBG.clipsToBounds=YES;
    [viewForAudioOrVideo addSubview:viewForWhiteBG];
    
    UILabel *InfoLbl=[[UILabel alloc]initWithFrame:CGRectMake(20, 0, viewForWhiteBG.frame.size.width-40, 50)];
    [InfoLbl setText:@"Are you sure you want to invite this contact?"];
    InfoLbl.numberOfLines = 0;
    InfoLbl.textAlignment = NSTextAlignmentCenter;
    [InfoLbl setFont:[UIFont fontWithName:FONT_NORMAL size:14]];
    [viewForWhiteBG addSubview:InfoLbl];
    
    UIButton *cancel=[[UIButton alloc]initWithFrame:CGRectMake(35, 51, 50, 30)];
    [cancel setTitle:@"No" forState:UIControlStateNormal];
    [cancel setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    cancel.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:14];
    [cancel addTarget:self action:@selector(ClickOnCancel:) forControlEvents:UIControlEventTouchUpInside];
    [viewForWhiteBG addSubview:cancel];
    
    UIButton *ok=[[UIButton alloc]initWithFrame:CGRectMake(215, 51, 50, 30)];
    [ok setTitle:@"Yes" forState:UIControlStateNormal];
    [ok setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    ok.titleLabel.font=[UIFont fontWithName:FONT_NORMAL size:14];
    [ok addTarget:self action:@selector(clickOnOk:) forControlEvents:UIControlEventTouchUpInside];
    [viewForWhiteBG addSubview:ok];
}
- (IBAction)ClickOnCancel:(id)sender {
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    [viewForAudioOrVideo removeFromSuperview];
}

- (IBAction)clickOnOk:(id)sender {
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    [viewForAudioOrVideo removeFromSuperview];
    NSDictionary *dictArt=[arrContacts objectAtIndex:getindepath.row];
    NSString *strnumber=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"mobile"]];
    MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
    picker.messageComposeDelegate = self;
    picker.recipients = [NSArray arrayWithObjects:strnumber, nil];
    picker.body = @"Hey, Check out Acedais Messenger to connect with your friends,family and Colleagues . Download it today from:  ";
    //https://itunes.apple.com/us/app/zoechat/id1231021028
    [self presentViewController:picker animated:NO completion:NULL];
    
    
}
- (void) onRemoveViewAudioVideo: (UIGestureRecognizer *) gesture
{
    UIView *viewForAudioOrVideo=(UIView *)[self.navigationController.view viewWithTag:9087650];
    [viewForAudioOrVideo removeFromSuperview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (IBAction)clickonBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
