//
//  DetailDataVC.m
//  ZoeChat
//
//  Created by macmini on 06/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "DetailDataVC.h"
#import "AppDelegate.h"

@interface DetailDataVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *dataArray;
    AppDelegate *appdelegate;
}

@end

@implementation DetailDataVC
@synthesize titleStr,photoIndexPath,audioIndexPath,videoIndexPath,documentIndexPath;
- (void)viewDidLoad {
    [super viewDidLoad];
//    dataArray = [[NSArray alloc]initWithObjects:@"Never",@"Wi-fi",@"Wi-fi and Cellular", nil];
    dataArray = [[NSArray alloc]initWithObjects:@"Never",@"Wi-fi and Cellular",@"Wi-fi", nil];

    self.title = titleStr;
    appdelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"YES" forKey:@"isDataPage"];
    [defaults synchronize];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"YES" forKey:@"isDataPage"];
    [defaults synchronize];

    
    
    if ([titleStr isEqualToString:@"Photo"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *lastRow = [defaults objectForKey:@"selected1"];
        if (lastRow) {
            self.photoIndexPath = [NSIndexPath indexPathForRow:lastRow.integerValue inSection:0];
        }
        else{
            self.photoIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];

        }
    }
    if ([titleStr isEqualToString:@"Audio"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *lastRow = [defaults objectForKey:@"selected2"];
        if (lastRow) {
            self.audioIndexPath = [NSIndexPath indexPathForRow:lastRow.integerValue inSection:0];
        }
        else{
            self.audioIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            
        }
    }
    if ([titleStr isEqualToString:@"Video"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *lastRow = [defaults objectForKey:@"selected3"];
        if (lastRow) {
            self.videoIndexPath = [NSIndexPath indexPathForRow:lastRow.integerValue inSection:0];
        }
        else{
            self.videoIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            
        }

    }
    if ([titleStr isEqualToString:@"Document"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *lastRow = [defaults objectForKey:@"selected4"];
        if (lastRow) {
            self.documentIndexPath = [NSIndexPath indexPathForRow:lastRow.integerValue inSection:0];
        }
        else{
            self.documentIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            
        }

    }

    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  44;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    UITableViewCell *cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.textLabel.text=[dataArray objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if ([titleStr isEqualToString:@"Photo"])
    {
    if([self.photoIndexPath isEqual:indexPath])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    }
    if ([titleStr isEqualToString:@"Audio"])
    {
        if([self.audioIndexPath isEqual:indexPath])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    if ([titleStr isEqualToString:@"Video"])
    {
        if([self.videoIndexPath isEqual:indexPath])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }if ([titleStr isEqualToString:@"Document"])
    {
        if([self.documentIndexPath isEqual:indexPath])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        
        cell.tintColor=color;
        
        
        
    }
    else
    {
        cell.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        
        
        
        
    }

    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if ([titleStr isEqualToString:@"Photo"])
    {
        if(self.photoIndexPath)
        {
            UITableViewCell* uncheckCell = [tableView
                                            cellForRowAtIndexPath:self.photoIndexPath];
            uncheckCell.accessoryType = UITableViewCellAccessoryNone;
        }
        if([self.photoIndexPath isEqual:indexPath])
        {
            self.photoIndexPath = nil;
        }
        else
        {
            UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            self.photoIndexPath = indexPath;
        }
        
    
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[NSNumber numberWithInt:self.photoIndexPath.row] forKey:@"selected1"];
        [defaults setObject:[dataArray objectAtIndex:indexPath.row] forKey:@"photoStatus"];
        [defaults synchronize];
        }
    else if ([titleStr isEqualToString:@"Video"])
    {
        if(self.videoIndexPath)
        {
            UITableViewCell* uncheckCell = [tableView
                                            cellForRowAtIndexPath:self.videoIndexPath];
            uncheckCell.accessoryType = UITableViewCellAccessoryNone;
        }
        if([self.videoIndexPath isEqual:indexPath])
        {
            self.videoIndexPath = nil;
        }
        else
        {
            UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            self.videoIndexPath = indexPath;
        }
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[NSNumber numberWithInt:self.videoIndexPath.row] forKey:@"selected3"];
        [defaults setObject:[dataArray objectAtIndex:indexPath.row] forKey:@"videoStatus"];
        [defaults synchronize];
    }
    else if ([titleStr isEqualToString:@"Audio"])
    {
        if(self.audioIndexPath)
        {
            UITableViewCell* uncheckCell = [tableView
                                            cellForRowAtIndexPath:self.audioIndexPath];
            uncheckCell.accessoryType = UITableViewCellAccessoryNone;
        }
        if([self.audioIndexPath isEqual:indexPath])
        {
            self.audioIndexPath = nil;
        }
        else
        {
            UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            self.audioIndexPath = indexPath;
        }
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[NSNumber numberWithInt:self.audioIndexPath.row] forKey:@"selected2"];
        [defaults setObject:[dataArray objectAtIndex:indexPath.row] forKey:@"audioStatus"];
        [defaults synchronize];
    }
    else if ([titleStr isEqualToString:@"Document"])
    {
        if(self.documentIndexPath)
        {
            UITableViewCell* uncheckCell = [tableView
                                            cellForRowAtIndexPath:self.documentIndexPath];
            uncheckCell.accessoryType = UITableViewCellAccessoryNone;
        }
        if([self.documentIndexPath isEqual:indexPath])
        {
            self.documentIndexPath = nil;
        }
        else
        {
            UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            self.documentIndexPath = indexPath;
        }
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[NSNumber numberWithInt:self.documentIndexPath.row] forKey:@"selectedCell4"];
        [defaults setObject:[dataArray objectAtIndex:indexPath.row] forKey:@"documentStatus"];
        [defaults synchronize];
    }
    
    }



-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryNone;
}


@end
