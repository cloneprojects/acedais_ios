//
//  StarredMessageVC.h
//  ZoeChat
//
//  Created by macmini on 02/06/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Stickerpipe/Stickerpipe.h>


@interface StarredMessageVC : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *StarredTblView;
- (IBAction)ClickOnBtn:(id)sender;
//Audio
@property (strong, nonatomic) UIButton *PlayPauseButton;
@property (strong, nonatomic) UIButton *CancelButton;
@property (strong, nonatomic) UILabel *TimeLabel;
@property (strong, nonatomic) UILabel *AudioNameLabel;
@property(strong, nonatomic)UIView *AudioView;
@property(strong, nonatomic)UIView *WholeView;
@property(strong,nonatomic)UISlider *movingslider;
@property (strong, nonatomic) STKStickerController *stickerController;

@end
