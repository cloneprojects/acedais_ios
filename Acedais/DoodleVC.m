//
//  DoodleVC.m
//  Wohoo
//
//  Created by Pyramidions on 19/07/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "DoodleVC.h"
#import "AppDelegate.h"
#import "DBClass.h"
#import "Constants.h"

@interface DoodleVC ()<ACEDrawingViewDelegate>
{
    UIImage *imageToUpload;
    AppDelegate *appdelegate;
    BOOL isAvailinChatsTable;
    DBClass *db_class;
    BOOL clickDoodle;


}
@end

@implementation DoodleVC
@synthesize strZoeChatId,strChatRoomType;
- (void)viewDidLoad {
    [super viewDidLoad];
    clickDoodle=YES;

    self.title=@"Draw Doodle";
    appdelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    db_class=[[DBClass alloc]init];
    isAvailinChatsTable=NO;
    isAvailinChatsTable=[db_class doesChatExist:strZoeChatId];
    self.doodleView.delegate = self;
    self.doodleView.drawTool = ACEDrawingToolTypePen;
    self.doodleView.draggableTextFontName = @"MarkerFelt-Thin";
    [self.colorView setHidden:YES];
    self.cl.layer.cornerRadius=self.cl.frame.size.width/2;
    self.cl.clipsToBounds=YES;
    self.c2.layer.cornerRadius=self.c2.frame.size.width/2;
    self.c2.clipsToBounds=YES;
    self.c3.layer.cornerRadius=self.c3.frame.size.width/2;
    self.c3.clipsToBounds=YES;
    self.c4.layer.cornerRadius=self.c4.frame.size.width/2;
    self.c4.clipsToBounds=YES;
    self.c5.layer.cornerRadius=self.c5.frame.size.width/2;
    self.c5.clipsToBounds=YES;
    self.c6.layer.cornerRadius=self.c6.frame.size.width/2;
    self.c6.clipsToBounds=YES;
    self.c7.layer.cornerRadius=self.c7.frame.size.width/2;
    self.c7.clipsToBounds=YES;
    self.c8.layer.cornerRadius=self.c8.frame.size.width/2;
    self.c8.clipsToBounds=YES;
    self.c9.layer.cornerRadius=self.c9.frame.size.width/2;
    self.c9.clipsToBounds=YES;
    self.c10.layer.cornerRadius=self.c10.frame.size.width/2;
    self.c10.clipsToBounds=YES;
    self.c11.layer.cornerRadius=self.c11.frame.size.width/2;
    self.c11.clipsToBounds=YES;
    self.c12.layer.cornerRadius=self.c12.frame.size.width/2;
    self.c12.clipsToBounds=YES;
    
    self.clearBtn.layer.cornerRadius = 5;
    self.clearBtn.clipsToBounds = YES;
    self.colorBtn.layer.cornerRadius = 5;
    self.colorBtn.clipsToBounds = YES;
    self.sendBtn.layer.cornerRadius = 5;
    self.sendBtn.clipsToBounds = YES;

    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        
        self.clearBtn.backgroundColor=color;
        self.colorBtn.backgroundColor=color;
        self.sendBtn.backgroundColor=color;
    }
    else
    {
        self.clearBtn.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.colorBtn.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.sendBtn.backgroundColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
           
    }
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)ClickOnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}
- (IBAction)clickOnClrBtn:(id)sender {
    
    [self.doodleView clear];
    
}

- (IBAction)ClickOnColorBtn:(id)sender {
    
    if (clickDoodle)
    {
        [self.colorView setHidden:NO];
        clickDoodle=NO;
        
    }
    else if(clickDoodle==NO)
    {
        [self.colorView setHidden:YES];
        clickDoodle=YES;
        
    }
    

}
-(UIImage*)getUIImageFromView:(UIView*)yourView
{
    UIGraphicsBeginImageContext(_doodleView.bounds.size);
    [_doodleView.layer renderInContext:UIGraphicsGetCurrentContext()];
    imageToUpload = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return imageToUpload;
}
- (IBAction)ClickOnsendBtn:(id)sender {
    [self getUIImageFromView:_doodleView];
    [self SendImage];
}
-(void)SendImage
{
    NSDate *now=[NSDate date];
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyyMMddHHmmssSSS"];
    NSString *localDateString = [dateFormatter1 stringFromDate:now];
    //  NSLog(@"%@",localDateString);
    
    
    NSString* strExt = @"PNG";
    
    NSString *UploadFileName=[NSString stringWithFormat:@"%@.%@",localDateString,strExt];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",UploadFileName]];
    [UIImagePNGRepresentation(imageToUpload) writeToFile:filePath atomically:YES];
    NSString * strImageURL=[NSString stringWithFormat:@"%@%@",FILE_AWS_IMAGE_BASE_URL,UploadFileName];
    NSString *strMsgId = [[NSUUID UUID] UUIDString]; //id
    
    
    long long milliSeconds=[self convertDateToMilliseconds:now];
    NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
    appdelegate.isDoodle=@"YES";
    BOOL success=NO;
    if (isAvailinChatsTable==NO)
    {
        success=[db_class insertChats:@"" chatRoomId:strZoeChatId chatRoomType:strChatRoomType sender:@"0" lastMessage:strImageURL lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"image" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appdelegate.strID isDelete:@"0" isLock:@"0" password:@""];
        if (success==YES)
        {
            isAvailinChatsTable=YES;
        }
    }
    else
    {
        success=[db_class updateLastMessage:strZoeChatId sender:@"0" lastMessage:strImageURL lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"image" unReadCount:@"0"];
        NSLog(@"success");
    }
    
    success=[db_class insertChatMessages:strMsgId userId:strZoeChatId groupId:strZoeChatId chatRoomType:strChatRoomType content:strImageURL contentType:@"image" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
    appdelegate.isFrmLocationPage=@"yes";

    [self.navigationController popViewControllerAnimated:YES];

}
-(long long)convertDateToMilliseconds:(NSDate *)date
{
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    long long milliseconds=timeInSeconds*1000;
    return milliseconds;
}
-(NSDate *)convertMillisecondsToDate:(long long)milliSeconds
{
    NSDate *dat = [NSDate dateWithTimeIntervalSince1970:(milliSeconds / 1000.0)];
    return dat;
}
- (IBAction)clickOnC1:(id)sender {
    self.doodleView.lineColor = [UIColor colorWithRed:26/255.0 green:186/255.0 blue:156/255.0 alpha:1.0];
    [self.colorView setHidden:YES];
    clickDoodle=YES;


}

- (IBAction)clickOnC2:(id)sender {
    self.doodleView.lineColor = [UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
    [self.colorView setHidden:YES];
    clickDoodle=YES;



}

- (IBAction)clickOnC3:(id)sender {
    self.doodleView.lineColor =  [UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
    [self.colorView setHidden:YES];


}

- (IBAction)clickOnC4:(id)sender {
    self.doodleView.lineColor = [UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
    [self.colorView setHidden:YES];
    clickDoodle=YES;



}

- (IBAction)clickOnC5:(id)sender {
    self.doodleView.lineColor = [UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
    [self.colorView setHidden:YES];
    clickDoodle=YES;



}

- (IBAction)clickOnC6:(id)sender {
    self.doodleView.lineColor = [UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
    [self.colorView setHidden:YES];
    clickDoodle=YES;



}

- (IBAction)clickOnC7:(id)sender {
    self.doodleView.lineColor = [UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
    [self.colorView setHidden:YES];
    clickDoodle=YES;



}

- (IBAction)clickOnC8:(id)sender {
    self.doodleView.lineColor = [UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
    [self.colorView setHidden:YES];
    clickDoodle=YES;



}

- (IBAction)clickOnC9:(id)sender {
    self.doodleView.lineColor = [UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
    [self.colorView setHidden:YES];
    clickDoodle=YES;

}

- (IBAction)clickOnC10:(id)sender {
    self.doodleView.lineColor = [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
    [self.colorView setHidden:YES];
    clickDoodle=YES;

}

- (IBAction)clickOnC11:(id)sender {
    self.doodleView.lineColor = [UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
    [self.colorView setHidden:YES];
    clickDoodle=YES;

}

- (IBAction)clickOnC12:(id)sender {
    self.doodleView.lineColor = [UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
    [self.colorView setHidden:YES];
    clickDoodle=YES;

}
@end
