//
//  ShowAllContactsVC.m
//  ZoeChat
//
//  Created by Pyramidions Solution on 28/04/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "ShowAllContactsVC.h"
#import "DBClass.h"
#import "AppDelegate.h"
#import "KTSContactsManager.h"
#import "Constants.h"

@interface ShowAllContactsVC ()<KTSContactsManagerDelegate>
{
    DBClass *db_class;
    AppDelegate *appDelegate;
     BOOL isAvailinChatsTable;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *tableData;
@property (strong, nonatomic) KTSContactsManager *contactsManager;
@end

@implementation ShowAllContactsVC
@synthesize strZoeChatId, strChatRoomType;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    db_class=[[DBClass alloc]init];
    
    UIBarButtonItem *barBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"BackButton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onBack)];
    [self.navigationItem setLeftBarButtonItem:barBtn ];

    self.title=@"Contacts To Send";
    self.contactsManager = [KTSContactsManager sharedManager];
    self.contactsManager.delegate = self;
    self.contactsManager.sortDescriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"firstName" ascending:YES] ];
    [self loadData];
    
     [self setFontFamily:FONT_NORMAL forView:self.view andSubViews:YES];

    isAvailinChatsTable=NO;
    isAvailinChatsTable=[db_class doesChatExist:strZoeChatId];
  appDelegate.isFrmLocationPage=@"no";
}
- (void)loadData
{
    [self.contactsManager importContacts:^(NSArray *contacts)
     {
         self.tableData = contacts;
         [self.tableView reloadData];
         NSLog(@"contacts: %@",contacts);
     }];
}

-(void)addressBookDidChange
{
    NSLog(@"Address Book Change");
    [self loadData];
}

-(BOOL)filterToContact:(NSDictionary *)contact
{
    NSArray *phones = contact[@"phones"];
    NSString *phNumber=@"";
    if ([phones count] > 0) {
        NSDictionary *phoneItem = phones[0];
       phNumber = phoneItem[@"value"];
    }
    
    return (![contact[@"firstName"] isEqualToString:@""] || ![contact[@"middleName"] isEqualToString:@""] || ![contact[@"lastName"] isEqualToString:@""]) && ![phNumber isEqualToString:@""] ;
}

#pragma mark - TableView Methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contactCell"];
    NSString *InitialStr;
    UIColor *bgColor;
    NSDictionary *contact = [self.tableData objectAtIndex:indexPath.row];
    NSLog(@"%@",contact);
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    NSString *strContName=@"";
    NSString *strFirstName = contact[@"firstName"];
    if ([[strFirstName stringByTrimmingCharactersInSet: set] length] != 0)
    {
        strContName=[NSString stringWithFormat:@"%@",strFirstName];
    }

    NSString *strMidName = contact[@"middleName"];
    if ([[strMidName stringByTrimmingCharactersInSet: set] length] != 0)
    {
        strContName=[NSString stringWithFormat:@"%@ %@",strContName,strMidName];
    }
    
    NSString *strLastName = contact[@"lastName"];
    if ([[strLastName stringByTrimmingCharactersInSet: set] length] != 0)
    {
         strContName=[NSString stringWithFormat:@"%@ %@",strContName,strLastName];
    }
    
    
    UILabel *nameLabel = (UILabel *)[cell viewWithTag:1];
    nameLabel.frame = CGRectMake(66,10, 175, 20);
    nameLabel.text = strContName;
    InitialStr=[nameLabel.text substringToIndex:1];
    if ([InitialStr isEqualToString:@"A"])
    {
        bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"B"])
    {
        bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"C"])
    {
        bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"D"])
    {
        bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"E"])
    {
        bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"F"])
    {
        bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"G"])
    {
        bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"H"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"I"])
    {
        bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"J"])
    {
        bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"K"])
    {
        bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"L"])
    {
        bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"M"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"N"])
    {
        bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"O"])
    {
        bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"P"])
    {
        bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"Q"])
    {
        bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"R"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"S"])
    {
        bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"T"])
    {
        bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"U"])
    {
        bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"V"])
    {
        bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"W"])
    {
        bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"X"])
    {
        bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"Y"])
    {
        bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"Z"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
    }


    
 /*   UILabel *phoneNumber = (UILabel *)[cell viewWithTag:2];
    NSArray *phones = contact[@"phones"];
  
    if ([phones count] > 0) {
        NSDictionary *phoneItem = phones[0];
        phoneNumber.text = phoneItem[@"value"];
    } */
    
    UIImageView *cellIconView = (UIImageView *)[cell.contentView viewWithTag:888];
//    UILabel *lblImg = (UILabel *)[cell viewWithTag:2];
    UIImage *image;
    
    UILabel* lblImg = [[UILabel alloc] initWithFrame:cellIconView.frame];
    lblImg.textColor = [UIColor whiteColor];
    lblImg.layer.cornerRadius=lblImg.frame.size.height/2;
    lblImg.clipsToBounds=YES;
    lblImg.textAlignment = UITextAlignmentCenter;
    [cell.contentView addSubview:lblImg];
    [lblImg setHidden:YES];

    if ([[contact allKeys] containsObject:@"image"])
    {
        image = contact[@"image"];
    }
    else
    {
        [lblImg setHidden:NO];
        image=[UIImage imageNamed:@"clear"];
        lblImg.text=InitialStr;
        lblImg.backgroundColor=bgColor;
    }
    cellIconView.contentScaleFactor = UIViewContentModeScaleAspectFill;
    cellIconView.layer.cornerRadius = CGRectGetHeight(cellIconView.frame) / 2;
    
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tableData count];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *cont=[self.tableData objectAtIndex:indexPath.row];
    NSString *firstName = cont[@"firstName"];
    NSString *strContactName=[firstName stringByAppendingString:[NSString stringWithFormat:@" %@", cont[@"lastName"]]];
    
    NSArray *arrContactNos=[cont valueForKey:@"phones"];
    NSDictionary *dict=[arrContactNos objectAtIndex:0];
    NSString *strContactNo=[NSString stringWithFormat:@"%@",[dict valueForKey:@"value"]];
    NSString *trimmedCon = [strContactNo stringByReplacingOccurrencesOfString:@"\u00a0" withString:@""];
    NSString *strMsgId = [[NSUUID UUID] UUIDString]; //id

    long long milliSeconds=[self convertDateToMilliseconds:[NSDate date]];
    NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
    
    BOOL success=NO;
    if (isAvailinChatsTable==NO)
    {
        success=[db_class insertChats:@"" chatRoomId:strZoeChatId chatRoomType:strChatRoomType sender:@"0" lastMessage:@"Contact" lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"contact" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
        if (success==YES)
        {
            isAvailinChatsTable=YES;
        }
    }
    else
    {
        success=[db_class updateLastMessage:strZoeChatId sender:@"0" lastMessage:@"Contact" lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"contact" unReadCount:@"0"];
        NSLog(@"success");
    }
    
    success=[db_class insertChatMessages:strMsgId userId:strZoeChatId groupId:strZoeChatId chatRoomType:strChatRoomType content:@"Contact" contentType:@"contact" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:strContactName contactNumber:trimmedCon checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
    
    
    
    
    if ([strChatRoomType isEqualToString:@"2"])
    {
        NSMutableArray *arrPartis=[[NSMutableArray alloc]init];
        NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
        NSArray *arrdata=[db_class getParticipants:strZoeChatId];
        for (int i=0; i<arrdata.count; i++)
        {
            NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
            NSDictionary *dictCont=[arrdata objectAtIndex:i];
            NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"userId"]];
            [dictParti setObject:strPhone forKey:@"participantId"];
            [arrPartis addObject:dictParti];
//            NSString *BCstrMsgId = [[NSUUID UUID] UUIDString]; //id
//            
//            BOOL isBroadcastChat;
//            isBroadcastChat=NO;
//            isBroadcastChat=[db_class doesChatExist:strPhone];
//            
//            if (isBroadcastChat==NO)
//            {
//                
//                 success=[db_class insertChats:@"" chatRoomId:strPhone chatRoomType:@"0" sender:@"0" lastMessage:@"Contact" lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"contact" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
//                
//                if (success==YES)
//                {
//                    isBroadcastChat=YES;
//                }
//            }
//            else
//            {
//                
//                 success=[db_class updateLastMessage:strPhone sender:@"0" lastMessage:@"Contact" lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"contact" unReadCount:@"0"];
//                NSLog(@"success");
//                
//                
//            }
//            
//            success=[db_class insertChatMessages:BCstrMsgId userId:strPhone groupId:strPhone chatRoomType:@"0" content:@"Contact" contentType:@"contact" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:strContactName contactNumber:strContactNo checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];

        }
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrPartis options:0 error:&error];
        NSString *jsonString;
        
        if (! jsonData)
        {
            NSLog(@"Got an error: %@", error);
        }
        else
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setValue:appDelegate.strID forKey:@"from"];
        [dictParam setValue:jsonString forKey:@"participantId"];
        
        
        [appDelegate.socket emit:@"sendBroadcast" with:@[@{@"from": appDelegate.strID,@"participants": jsonString,@"content":@"Contact",@"contentType": @"contact",@"messageId": strMsgId,@"time":strMilliSeconds,@"caption":@"", @"contactName":strContactName, @"contactNumber":trimmedCon, @"chatRoomType":@"0",@"groupId":@""}]] ;
        
        
        for (int i=0; i<arrdata.count; i++)
        {
            NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
            NSDictionary *dictCont=[arrdata objectAtIndex:i];
            NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"userId"]];
            [dictParti setObject:strPhone forKey:@"participantId"];
            [arrPartis addObject:dictParti];
            NSString *BCstrMsgId = [[NSUUID UUID] UUIDString]; //id
            
            BOOL isBroadcastChat;
            isBroadcastChat=NO;
            isBroadcastChat=[db_class doesChatExist:strPhone];
            
            if (isBroadcastChat==NO)
            {
                
                success=[db_class insertChats:@"" chatRoomId:strPhone chatRoomType:@"0" sender:@"0" lastMessage:@"Contact" lastMessageStatus:@"sending" lastMessagesTime:strMilliSeconds lastMessagesType:@"contact" unReadCount:@"0" groupName:@"" groupImage:@"" sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
                
                if (success==YES)
                {
                    isBroadcastChat=YES;
                }
            }
            else
            {
                
                success=[db_class updateLastMessage:strPhone sender:@"0" lastMessage:@"Contact" lastMessageStatus:@"sending" lastMessageTime:strMilliSeconds lastMessagesType:@"contact" unReadCount:@"0"];
                NSLog(@"success");
                
                
            }
            
            success=[db_class insertChatMessages:BCstrMsgId userId:strPhone groupId:strPhone chatRoomType:@"0" content:@"Contact" contentType:@"contact" contentStatus:@"sending" sender:@"0" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:strContactName contactNumber:trimmedCon checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
            
        }
    }
    
    else
        
    {
    [appDelegate.socket emit:@"sendMessage" with:@[@{@"from": appDelegate.strID,@"to": strZoeChatId,@"content": @"Contact",@"contentType":@"contact",@"messageId": strMsgId,@"time":strMilliSeconds, @"caption":@"", @"contactName":strContactName, @"contactNumber":trimmedCon ,@"chatRoomType":strChatRoomType,@"groupId":@""}]];
    }
    
    appDelegate.isFrmLocationPage=@"yes";
    
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)onBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(long long)convertDateToMilliseconds:(NSDate *)date
{
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    long long milliseconds=timeInSeconds*1000;
    return milliseconds;
}
-(NSDate *)convertMillisecondsToDate:(long long)milliSeconds
{
    NSDate *dat = [NSDate dateWithTimeIntervalSince1970:(milliSeconds / 1000.0)];
    return dat;
}


#pragma mark -
#pragma mark - Font
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)view;
        [btn setFont:[UIFont fontWithName:fontFamily size:[[btn font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *txtFld = (UITextField *)view;
        [txtFld setFont:[UIFont fontWithName:fontFamily size:[[txtFld font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextView class]])
    {
        UITextView *txtVw = (UITextView *)view;
        [txtVw setFont:[UIFont fontWithName:fontFamily size:[[txtVw font] pointSize]]];
    }
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}

@end
