//
//  CallCell.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 20/04/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CallCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *CallImage;
@property (weak, nonatomic) IBOutlet UILabel *CallName;
@property (weak, nonatomic) IBOutlet UILabel *CallTime;
@property (weak, nonatomic) IBOutlet UIImageView *ImgStatusIcon;
@property (strong, nonatomic) IBOutlet UILabel *LblImg;
@property (weak, nonatomic) IBOutlet UIImageView *ImgCallType;
@end
