//
//  BackupVC.h
//  ZoeChat
//
//  Created by veena on 2/28/18.
//  Copyright © 2018 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CloudKit/CloudKit.h>
#import <SSZipArchive/SSZipArchive.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "AFNHelper.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "Constants.h"
@interface BackupVC : UIViewController<UIDocumentPickerDelegate>
@property NSMutableArray *notes;
@end
