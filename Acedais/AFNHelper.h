//
//  AFNHelper.h
//  gHealth Doctor
//
//  Created by Ramesh P on 16/11/16.
//  Copyright © 2016 provenlogic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define POST_METHOD @"POST"
#define GET_METHOD  @"GET"

typedef void (^RequestCompletionBlock)(id response, NSError *error);

@interface AFNHelper : NSObject
{
//blocks
    RequestCompletionBlock dataBlock;
}

@property(nonatomic,copy)NSString *strReqMethod;

-(id)initWithRequestMethod:(NSString *)method;

-(void)getDataFromPath:(NSString *)path withParamData:(NSMutableDictionary *)dictParam withBlock:(RequestCompletionBlock)block;

-(void)getDataFromPath:(NSString *)path withParamDataImage:(NSMutableDictionary *)dictParam andImage:(UIImage *)image withBlock:(RequestCompletionBlock)block;

-(void)getAddressFromGooglewithParamData:(NSMutableDictionary *)dictParam withBlock:(RequestCompletionBlock)block;

-(void)getAddressFromGooglewAutoCompletewithParamData:(NSMutableDictionary *)dictParam withBlock:(RequestCompletionBlock)block;

@end
