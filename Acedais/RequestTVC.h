//
//  RequestTVC.h
//  ZoeChat
//
//  Created by admin on 16/05/18.
//  Copyright © 2018 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestTVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imggrp;
@property (weak, nonatomic) IBOutlet UILabel *lblgrpname;
@property (weak, nonatomic) IBOutlet UILabel *lblgrptime;
@property (weak, nonatomic) IBOutlet UIButton *btnYes;
@property (weak, nonatomic) IBOutlet UIButton *btnNo;

@end
