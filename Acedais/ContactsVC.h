//
//  ContactsVC.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 31/12/16.
//  Copyright © 2016 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ContactsVC : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableContacts;
@property (strong, nonatomic) IBOutlet UIView *passwordView;
@property (strong, nonatomic) IBOutlet UITextField *passwordTxt;
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
- (IBAction)ClickOnSubmitBtn:(id)sender;
- (IBAction)ClickOnForgotPassword:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *InviteFriendsTblView;

@end
