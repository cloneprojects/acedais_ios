//
//  GetNameImageVC.m
//  ZoeChat
//
//  Created by Pyramidions Solution on 22/04/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "GetNameImageVC.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "GetNameImageTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MBProgressHUD.h"
#import "AFNHelper.h"
#import "TOCropViewController.h"
#import "AWSS3TransferUtility.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "CustomAlbum.h"
#import "MainTabBarVC.h"
#import "DBClass.h"

@interface GetNameImageVC ()<TOCropViewControllerDelegate>
{
    UITextField *currentTextView;
    UITextView *currentTextView1;
    bool keyboardIsShown;
    UITapGestureRecognizer *tapGesture;
    AppDelegate *appDelegate;
    NSString *isPictureAdded;
    UIPopoverController *popoverController;
    NSURL *assetURL;
    NSString* UploadFileName;
     DBClass *db_class;
    NSString *strGroupId;

}
@property (nonatomic, strong) UIImage *image;           // The image we'll be cropping
@property (nonatomic, strong) UIImageView *imageView;   // The image view to present the cropped image

@property (nonatomic, assign) TOCropViewCroppingStyle croppingStyle; //The cropping style
@property (nonatomic, assign) CGRect croppedFrame;
@property (nonatomic, assign) NSInteger angle;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
@property (nonatomic, strong) UIPopoverController *activityPopoverController;
#pragma clang diagnostic pop

- (void)showCropViewController;
- (void)sharePhoto;

- (void)layoutImageView;
- (void)didTapImageView;

- (void)updateImageViewWithImage:(UIImage *)image fromCropViewController:(TOCropViewController *)cropViewController;


@end

@implementation GetNameImageVC
@synthesize arrParticipants;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    isPictureAdded=@"0";
    UploadFileName=@"";
    _txtGroupName.delegate=self;
     db_class=[[DBClass alloc]init];
    [self setFontFamily:FONT_NORMAL forView:self.view andSubViews:YES];
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    UIBarButtonItem *barBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"BackButton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onBack)];
    [self.navigationItem setLeftBarButtonItem:barBtn ];
    
    self.title=@"New Group";
     strGroupId = [[NSUUID UUID] UUIDString];
    _btnProfile.layer.cornerRadius=_btnProfile.frame.size.height/2;
    _btnProfile.clipsToBounds=YES;

    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 0.75;
    border.borderColor = [UIColor darkGrayColor].CGColor;
    border.frame = CGRectMake(0, _txtGroupName.frame.size.height - borderWidth, _txtGroupName.frame.size.width, _txtGroupName.frame.size.height);
    border.borderWidth = borderWidth;
    [_txtGroupName.layer addSublayer:border];
    _txtGroupName.layer.masksToBounds = YES;
    
    _lblNoOfParticipants.text=[NSString stringWithFormat:@"Participants: %lu",(unsigned long)arrParticipants.count];
    
    
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        
        self.txtGroupName.tintColor=color;
        
        
    }
    else
    {
        
       
        self.txtGroupName.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        
        
    }

}
-(void)onBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)onNext:(id)sender
{
    if ([appDelegate.strInternetMode isEqualToString:@"online"])
    {
        if ([self.txtGroupName.text isEqualToString:@""])
        {
            
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:@"Please enter your group name." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            [self.txtGroupName becomeFirstResponder];
            NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
            UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
            if (color != nil) {
                alert.view.tintColor=color;
            }
            else
            {
                
                alert.view.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
                
            }

            return;
        }
        
        
        NSMutableArray *arrPartis=[[NSMutableArray alloc]init];
         NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
         [dictParti setObject:appDelegate.strID forKey:@"participantId"];
        [dictParti setObject:@"1" forKey:@"isAdmin"];

        [arrPartis addObject:dictParti];
        
        for (int i=0; i<arrParticipants.count; i++)
        {
            NSMutableDictionary *dictParti=[[NSMutableDictionary alloc]init];
            NSDictionary *dictCont=[arrParticipants objectAtIndex:i];
            NSString *strPhone=[NSString stringWithFormat:@"%@",[dictCont valueForKey:@"zoechatid"]];
            [dictParti setObject:strPhone forKey:@"participantId"];
            [dictParti setObject:@"0" forKey:@"isAdmin"];
            [arrPartis addObject:dictParti];
        }
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrPartis options:0 error:&error];
        NSString *jsonString;
        
        if (! jsonData)
        {
            NSLog(@"Got an error: %@", error);
        }
        else
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
        NSString* encodedString = [jsonString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        
        NSString *strImageURL;
       
        if ([isPictureAdded isEqualToString:@"1"])
        {
            strImageURL=[NSString stringWithFormat:@"%@%@",FILE_AWS_IMAGE_BASE_URL,UploadFileName];
            [dictParam setValue:appDelegate.strID forKey:@"from"];
            [dictParam setValue:_txtGroupName.text forKey:@"groupName"];
            [dictParam setValue:strGroupId forKey:@"groupId"];
            [dictParam setValue:strImageURL forKey:@"groupImage"];
            [dictParam setValue:jsonString forKey:@"participants"];
        }
        else
        {
            strImageURL=@"";
            [dictParam setValue:appDelegate.strID forKey:@"from"];
            [dictParam setValue:_txtGroupName.text forKey:@"groupName"];
            [dictParam setValue:strGroupId forKey:@"groupId"];
            [dictParam setValue:jsonString forKey:@"participants"];

        }
        //
        
//         [appDelegate.socket emit:@"createGroup" with:@[@{@"from": appDelegate.strID,@"groupId": strGroupId,@"groupImage": strImageURL,@"groupName":_txtGroupName.text,@"participants":jsonString }]];
        [[appDelegate.socket emitWithAck:@"createGroup" with:@[@{@"from": appDelegate.strID,@"groupId": strGroupId,@"groupImage": strImageURL,@"groupName":_txtGroupName.text,@"participants":jsonString }]] timingOutAfter:2 callback:^(NSArray* data) {
            NSLog(@"%@",data);
        }];


        NSString *strContent=[NSString stringWithFormat:@"You have created group"];
        long long milliSeconds=[self convertDateToMilliseconds:[NSDate date]];
        NSString *strMilliSeconds=[NSString stringWithFormat:@"%lld",milliSeconds];
        
        NSUUID *uuid=[NSUUID UUID];
        NSString *strMgsId=[NSString stringWithFormat:@"%@",uuid];
        
        BOOL success=[db_class insertChats:@"" chatRoomId:strGroupId chatRoomType:@"1" sender:@"" lastMessage:strContent lastMessageStatus:@"delivered" lastMessagesTime:strMilliSeconds lastMessagesType:@"header" unReadCount:@"1" groupName:_txtGroupName.text groupImage:strImageURL sentBy:appDelegate.strID isDelete:@"0" isLock:@"0" password:@""];
        
        BOOL success1=[db_class insertChatMessages:strMgsId userId:appDelegate.strID groupId:strGroupId chatRoomType:@"1" content:strContent contentType:@"header" contentStatus:@"delivered" sender:@"" sentTime:strMilliSeconds deliveredTime:@"0" seenTime:@"0" caption:@"" isDownloaded:@"0" latitude:@"" longitude:@"" contactName:@"" contactNumber:@"" checkStar:@"0" showPreview:@"0" linkTitle:@"" linkLogo:@"" linkDescription:@""];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Refresh_Chats" object:self];

       
        UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MainTabBarVC *hme = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainTabBarVC"];
        [self.navigationController pushViewController:hme animated:YES];

    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Please check your internet connection"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.label.adjustsFontSizeToFitWidth=YES;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:3];
        
    }
    
}
-(IBAction)onPhoto:(id)sender
{
    NSLog(@"%@",appDelegate.strInternetMode);
    if ([appDelegate.strInternetMode isEqualToString:@"online"])
    {
        UIButton *btnProf=(UIButton * )sender;
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:appDelegate.strAlertTitle
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction* Camera = [UIAlertAction
                                 actionWithTitle:@"Take Picture"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                                     
                                     //Use camera if device has one otherwise use photo library
                                     if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                                     {
                                         [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
                                     }
                                     else
                                     {
                                         [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                                     }
                                     
                                     [imagePicker setDelegate:self];
                                     
                                     //Show image picker
                                     [self presentViewController:imagePicker animated:YES completion:nil];
                                 }];
        
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Choose From Camera Roll"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
                                 imagePickerController.delegate = self;
                                 [imagePickerController setAllowsEditing:NO];
                                 imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
                                 [self presentViewController:imagePickerController animated:YES completion:nil];
                                 
                             }];
        
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleCancel
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:Camera];
        [alert addAction:ok];
        [alert addAction:cancel];
        
        
        UIPopoverPresentationController *popPresenter1 = [alert
                                                          popoverPresentationController];
        popPresenter1.sourceView=btnProf;
        popPresenter1.sourceRect=btnProf.bounds;
        [alert setModalPresentationStyle:UIModalPresentationPopover];
        
        [self presentViewController:alert animated:YES completion:nil];
        NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
        UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
        if (color != nil) {
            alert.view.tintColor=color;
        }
        else
        {
            
            alert.view.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
            
        }

    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Configure for text only and offset down
        hud.mode = MBProgressHUDModeText;
        hud.label.text = [NSString stringWithFormat:@"Please check your internet connection"];
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.label.adjustsFontSizeToFitWidth=YES;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:3];
        
    }
    
    
    
    
}
#pragma mark -
#pragma mark - TableView Delagate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  60;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return arrParticipants.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    GetNameImageTableViewCell *cell=(GetNameImageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"GetNameImageTableViewCell"];
    if (cell==nil) {
        cell=[[GetNameImageTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"GetNameImageTableViewCell"];
    }
    NSString *InitialStr;
    UIColor *bgColor;
    [cell.lblInitialImg setHidden:YES];
    NSDictionary *dictArt=[arrParticipants objectAtIndex:indexPath.row];
    NSString *strName=[NSString stringWithFormat:@"%@",[dictArt valueForKey:@"name"]];
    cell.Name.text=[strName capitalizedString];
    InitialStr=[cell.Name.text substringToIndex:1];
    if ([InitialStr isEqualToString:@"A"])
    {
        bgColor=[UIColor colorWithRed:26/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"B"])
    {
        bgColor=[UIColor colorWithRed:46/255.0 green:204/255.0 blue:113/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"C"])
    {
        bgColor=[UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"D"])
    {
        bgColor=[UIColor colorWithRed:155/255.0 green:89/255.0 blue:182/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"E"])
    {
        bgColor=[UIColor colorWithRed:52/255.0 green:73/255.0 blue:94/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"F"])
    {
        bgColor=[UIColor colorWithRed:241/255.0 green:196/255.0 blue:15/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"G"])
    {
        bgColor=[UIColor colorWithRed:230/255.0 green:126/255.0 blue:34/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"H"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:200/255.0 blue:113/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"I"])
    {
        bgColor=[UIColor colorWithRed:149/255.0 green:165/255.0 blue:166/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"J"])
    {
        bgColor=[UIColor colorWithRed:67/255.0 green:53/255.0 blue:61/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"K"])
    {
        bgColor=[UIColor colorWithRed:137/255.0 green:127/255.0 blue:186/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"L"])
    {
        bgColor=[UIColor colorWithRed:123/255.0 green:176/255.0 blue:166/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"M"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:102/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"N"])
    {
        bgColor=[UIColor colorWithRed:214/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"O"])
    {
        bgColor=[UIColor colorWithRed:134/255.0 green:255/255.0 blue:103/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"P"])
    {
        bgColor=[UIColor colorWithRed:103/255.0 green:189/255.0 blue:255/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"Q"])
    {
        bgColor=[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"R"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:103/255.0 blue:138/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"S"])
    {
        bgColor=[UIColor colorWithRed:170/255.0 green:182/255.0 blue:155/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"T"])
    {
        bgColor=[UIColor colorWithRed:74/255.0 green:226/255.0 blue:221/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"U"])
    {
        bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"V"])
    {
        bgColor=[UIColor colorWithRed:249/255.0 green:174/255.0 blue:116/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"W"])
    {
        bgColor=[UIColor colorWithRed:74/255.0 green:78/255.0 blue:77/255.0 alpha:1.0];
    }else if ([InitialStr isEqualToString:@"X"])
    {
        bgColor=[UIColor colorWithRed:129/255.0 green:165/255.0 blue:230/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"Y"])
    {
        bgColor=[UIColor colorWithRed:231/255.0 green:76/255.0 blue:60/255.0 alpha:1.0];
    }
    else if ([InitialStr isEqualToString:@"Z"])
    {
        bgColor=[UIColor colorWithRed:255/255.0 green:113/255.0 blue:196/255.0 alpha:1.0];
    }
    

        NSString *strImageURL1=[dictArt valueForKey:@"image"];
    cell.Image.image=[UIImage imageNamed:@"userImage.png"];
    if (![strImageURL1 isEqualToString:@"(null)"])

    {
        if (![strImageURL1 isEqualToString:@""])
        {
            [cell.Image sd_setImageWithURL:[NSURL URLWithString:strImageURL1] placeholderImage:[UIImage imageNamed:@"clear.png"]options:SDWebImageRefreshCached];
        }
    }
    else
    {
        [cell.lblInitialImg setHidden:NO];
    
   
    cell.lblInitialImg.text=InitialStr;
    cell.lblInitialImg.backgroundColor=bgColor;
    }
   
    
    cell.Image.layer.cornerRadius=cell.Image.frame.size.height/2;
    cell.Image.clipsToBounds=YES;
    cell.lblInitialImg.layer.cornerRadius=cell.lblInitialImg.frame.size.height/2;
    cell.lblInitialImg.clipsToBounds=YES;
  
    cell.Name.font=[UIFont fontWithName:FONT_NORMAL size:16];
    

    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark
#pragma mark - ImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:chosenImage];
    cropController.delegate = self;
    cropController.rotateButtonsHidden=YES;
    cropController.rotateClockwiseButtonHidden=YES;
    //  cropController.aspectRatioPickerButtonHidden=YES;
    
    // -- Uncomment these if you want to test out restoring to a previous crop setting --
    //cropController.angle = 90; // The initial angle in which the image will be rotated
    //cropController.imageCropFrame = CGRectMake(0,0,2848,4288); //The
    
    // -- Uncomment the following lines of code to test out the aspect ratio features --
    cropController.aspectRatioPreset = TOCropViewControllerAspectRatioPresetSquare; //Set the initial aspect ratio as a square
    cropController.aspectRatioLockEnabled = YES; // The crop box is locked to the aspect ratio and can't be resized away from it
    cropController.resetAspectRatioEnabled = NO; // When tapping 'reset', the aspect ratio will NOT be reset back to default
    
    // -- Uncomment this line of code to place the toolbar at the top of the view controller --
    // cropController.toolbarPosition = TOCropViewControllerToolbarPositionTop;
    
    
    self.image = chosenImage;
    
    //If profile picture, push onto the same navigation stack
    if (self.croppingStyle == TOCropViewCroppingStyleCircular) {
        [picker pushViewController:cropController animated:YES];
    }
    else { //otherwise dismiss, and then present from the main controller
        [picker dismissViewControllerAnimated:YES completion:^{
            [self presentViewController:cropController animated:YES completion:nil];
        }];
    }
    
    
    
    assetURL = [info objectForKey:UIImagePickerControllerReferenceURL];
    
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark
#pragma mark - Cropper Delegate -
- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    [self.btnProfile setImage:image forState:UIControlStateNormal];
    
    self.btnProfile.clipsToBounds = YES;
    self.btnProfile.layer.cornerRadius = (self.btnProfile.frame.size.width / 2);//half of the width
    self.btnProfile.layer.borderColor=[UIColor blackColor].CGColor;
    self.btnProfile.layer.borderWidth=1.0f;
    isPictureAdded=@"1";
    
    MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.btnProfile animated:YES];
    // hud.mode = MBProgressHUDAnimationFade;
    // hud.label.text = @"";
    
    
    __block NSString *fileName = nil;
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library assetForURL:assetURL resultBlock:^(ALAsset *asset)  {
        fileName = asset.defaultRepresentation.filename;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyyMMddHHmmssSSS"];
        NSString *localDateString = [dateFormatter stringFromDate:[NSDate date]];
        // NSLog(@"%@",localDateString);
        
        NSArray* arrExt = [fileName componentsSeparatedByString: @"."];
        NSString* strExt = [arrExt objectAtIndex: arrExt.count-1];
        
        
        
        UploadFileName=[NSString stringWithFormat:@"%@.%@",strGroupId,strExt];
        
        
        //image you want to upload
        UIImage *imageToUpload=[self compressImage:image];
        
        //convert uiimage to
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",UploadFileName]];
        [UIImagePNGRepresentation(imageToUpload) writeToFile:filePath atomically:YES];
        
        NSURL* fileUrl = [NSURL fileURLWithPath:filePath];
        
        AWSS3TransferUtilityUploadCompletionHandlerBlock completionHandler = ^(AWSS3TransferUtilityUploadTask *task, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // Do something e.g. Alert a user for transfer completion.
                // On failed uploads, `error` contains the error object.
                [MBProgressHUD hideHUDForView:self.btnProfile animated:YES];
               
                NSLog(@"%@",error);
                [CustomAlbum addNewAssetWithImage:imageToUpload toAlbum:[CustomAlbum getMyAlbumWithName:CSAlbum] onSuccess:^(NSString *ImageId) {
                    NSLog(@"image saved");
                    
                }
                                          onError:^(NSError *error)
                 {
                     NSLog(@"probelm in saving image");
                 }];
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                
                // Configure for text only and offset down
                hud.mode = MBProgressHUDModeText;
                hud.label.text = @"Profile picture uploaded";
                hud.margin = 10.f;
                hud.yOffset = 150.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hideAnimated:YES afterDelay:3];
            });
        };
        
        AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility defaultS3TransferUtility];
        [[transferUtility uploadFile:fileUrl bucket:FILE_BUCKET_NAME key:UploadFileName contentType:@"image/png" expression:nil completionHandler:completionHandler]
        continueWithBlock:^id(AWSTask *task) {
            if (task.error) {
                [MBProgressHUD hideHUDForView:self.btnProfile animated:YES];
                NSLog(@"Error: %@", task.error);
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                
                // Configure for text only and offset down
                hud.mode = MBProgressHUDModeText;
                hud.label.text = @"Profile picture not uploaded";
                hud.margin = 10.f;
                hud.yOffset = 150.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hideAnimated:YES afterDelay:3];
                
            }
           
            if (task.result) {
                AWSS3TransferUtilityUploadTask *uploadTask = task.result;
                // Do something with uploadTask.
                NSLog(@"%@",uploadTask);
            }
            
            return nil;
        }];
        
    }failureBlock:^(NSError *error)
     {
         NSLog(@"%@",error);
     }];
    
    [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (UIImage *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth) {
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
}




#pragma mark -
#pragma mark - Keyboard

-(void)dismissKeyBoard
{
    // [tableTexture removeFromSuperview];
    
    for(UIView *subView in self.view.subviews)
    {
        if([subView isKindOfClass:[UITextField class]])
        {
            if([subView isFirstResponder])
            {
                [subView resignFirstResponder];
                break;
            }
        }
        else if([subView isKindOfClass:[UITextView class]])
        {
            if([subView isFirstResponder])
            {
                [subView resignFirstResponder];
                break;
            }
        }
        
    }
    
    
    
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}




#pragma mark -
#pragma mark - Font


-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)view;
        [btn setFont:[UIFont fontWithName:fontFamily size:[[btn font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *txtFld = (UITextField *)view;
        [txtFld setFont:[UIFont fontWithName:fontFamily size:[[txtFld font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextView class]])
    {
        UITextView *txtVw = (UITextView *)view;
        [txtVw setFont:[UIFont fontWithName:fontFamily size:[[txtVw font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}

-(long long)convertDateToMilliseconds:(NSDate *)date
{
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    long long milliseconds=timeInSeconds*1000;
    return milliseconds;
}
-(NSDate *)convertMillisecondsToDate:(long long)milliSeconds
{
    NSDate *dat = [NSDate dateWithTimeIntervalSince1970:(milliSeconds / 1000.0)];
    return dat;
}

@end
