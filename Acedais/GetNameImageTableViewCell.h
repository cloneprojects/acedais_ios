//
//  GetNameImageTableViewCell.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 22/04/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GetNameImageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *Image;
@property (strong, nonatomic) IBOutlet UILabel *lblInitialImg;
@property (weak, nonatomic) IBOutlet UILabel *Name;
@end
