//
//  GetNameImageVC.h
//  ZoeChat
//
//  Created by Pyramidions Solution on 22/04/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GetNameImageVC : UIViewController<UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnProfile;
@property (weak, nonatomic) IBOutlet UITextField *txtGroupName;
@property (strong, nonatomic) IBOutlet UILabel *lblNoOfParticipants;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UITableView *tableParticipant;
@property (weak, nonatomic)  NSArray *arrParticipants;
@end
