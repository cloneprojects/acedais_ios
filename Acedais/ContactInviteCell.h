//
//  ContactInviteCell.h
//  BitChat
//
//  Created by Pyramidions on 11/11/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactInviteCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *NamLbl;
@property (strong, nonatomic) IBOutlet UIButton *invitBtn;
@property (strong, nonatomic) IBOutlet UIImageView *imgiview;
@property (strong, nonatomic) IBOutlet UILabel *LbImg;
@end
