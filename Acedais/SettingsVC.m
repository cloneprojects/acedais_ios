//
//  SettingsVC.m
//  ZoeChat
//
//  Created by Pyramidions Solution on 07/01/17.
//  Copyright © 2017 Pyramidions Solution. All rights reserved.
//

#import "SettingsVC.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "AFNHelper.h"
#import "TOCropViewController.h"
#import "MBProgressHUD.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "AWSS3TransferUtility.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIButton+WebCache.h"
#import <Photos/Photos.h>
#import "CustomAlbum.h"

@interface SettingsVC ()<TOCropViewControllerDelegate>
{
    UITextField *currentTextView;
    UITextView *currentTextView1;
    bool keyboardIsShown;
    UITapGestureRecognizer *tapGesture;
    AppDelegate *appDelegate;
    NSString *isPictureAdded;
    UIPopoverController *popoverController;
    NSURL *assetURL;
    NSString* UploadFileName;
}
@property (nonatomic, strong) UIImage *image;           // The image we'll be cropping
@property (nonatomic, strong) UIImageView *imageView;   // The image view to present the cropped image

@property (nonatomic, assign) TOCropViewCroppingStyle croppingStyle; //The cropping style
@property (nonatomic, assign) CGRect croppedFrame;
@property (nonatomic, assign) NSInteger angle;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
@property (nonatomic, strong) UIPopoverController *activityPopoverController;
#pragma clang diagnostic pop

- (void)showCropViewController;
- (void)sharePhoto;

- (void)layoutImageView;
- (void)didTapImageView;

- (void)updateImageViewWithImage:(UIImage *)image fromCropViewController:(TOCropViewController *)cropViewController;


@end

@implementation SettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
     [self setFontFamily:FONT_NORMAL forView:self.view andSubViews:YES];
    // [self.navigationController.navigationBar setTranslucent:NO];

    UploadFileName=@"";
    isPictureAdded=@"1";
    _txtName.delegate=self;
    _txtName.text=appDelegate.strName;
    _txtStatus.delegate=self;
    _txtStatus.text=appDelegate.strStatus;
    _btnEdit.clipsToBounds=YES;
    _btnEdit.layer.cornerRadius=4;
    [_btnEdit setFont:[UIFont fontWithName:FONT_MEDIUM size:[[_btnEdit font] pointSize]]];
  
    [self.camImgView setHidden:YES];
    [self.nameEditImg setHidden:YES];
    [self.statusEditImg setHidden:YES];
    [self.camBgView setHidden:YES];

    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        
        [self.btnEdit setBackgroundColor:color];
        self.txtName.tintColor=color;
        self.txtStatus.tintColor=color;
        
        
    }
    else
    {
        
        [self.btnEdit setBackgroundColor:[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0]];
        self.txtName.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        self.txtStatus.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        
        
    }

    _btnProfile.clipsToBounds=YES;
    _btnProfile.layer.cornerRadius=_btnProfile.frame.size.height/2;
    
    {
    CALayer *btnUpperBorder = [CALayer layer];
    btnUpperBorder.frame = CGRectMake(0.0f
                                      , 0.0f, _txtName.frame.size.width, 1.5f);
    btnUpperBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                       alpha:1.0f].CGColor;
    [_txtName.layer addSublayer:btnUpperBorder];
    
    CALayer *btnbottomBorder = [CALayer layer];
    btnbottomBorder.frame = CGRectMake(0.0f
                                       , _txtName.frame.size.height-1.5, _txtName.frame.size.width, 1.5f);
    btnbottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                        alpha:1.0f].CGColor;
    [_txtName.layer addSublayer:btnbottomBorder];
    
    }
    {
        CALayer *btnUpperBorder = [CALayer layer];
        btnUpperBorder.frame = CGRectMake(0.0f
                                          , 0.0f, _txtStatus.frame.size.width, 1.5f);
        btnUpperBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                           alpha:1.0f].CGColor;
        [_txtStatus.layer addSublayer:btnUpperBorder];
        
        CALayer *btnbottomBorder = [CALayer layer];
        btnbottomBorder.frame = CGRectMake(0.0f
                                           , _txtStatus.frame.size.height-1.5, _txtStatus.frame.size.width, 1.5f);
        btnbottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                            alpha:1.0f].CGColor;
        [_txtStatus.layer addSublayer:btnbottomBorder];
        
    }
    
    //ScrollView moving
    
    //  _scrollView.contentSize=CGSizeMake(self.view.frame.size.width, self.btnRegister.frame.origin.y+70);
    
    
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.scrollView.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    self.scrollView.contentSize = contentRect.size;
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyBoard)];
    
    [self.view addGestureRecognizer:tap];
    
    _btnProfile.userInteractionEnabled=NO;
    _txtName.userInteractionEnabled=NO;
    _txtStatus.userInteractionEnabled=NO;
    
    [self updateProfilePicture];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



-(void)updateProfilePicture
{
    NSString *strImage=appDelegate.strProfilePic;
    NSURL *imageURL = [NSURL URLWithString:strImage];
    // [self.btnProfile setImage:[UIImage imageNamed:@"user.png"] forState:UIControlStateNormal];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:SDWebImageRefreshCached
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                // do something with image
                                [_btnProfile setImage:image forState:UIControlStateNormal];
                                
                            }
                        }];

}


- (IBAction)onProfile:(id)sender
{
    
    
    UIButton *btnProf=(UIButton * )sender;
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:appDelegate.strAlertTitle
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* Camera = [UIAlertAction
                             actionWithTitle:@"Take Picture"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                                 
                                 //Use camera if device has one otherwise use photo library
                                 if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                                 {
                                     [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
                                 }
                                 else
                                 {
                                     [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                                 }
                                 
                                 [imagePicker setDelegate:self];
                                 
                                 //Show image picker
                                 [self presentViewController:imagePicker animated:YES completion:nil];
                             }];
    
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Choose From Camera Roll"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
                             imagePickerController.delegate = self;
                             imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
                             [self presentViewController:imagePickerController animated:YES completion:nil];
                             
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:Camera];
    [alert addAction:ok];
    [alert addAction:cancel];
    
    
    UIPopoverPresentationController *popPresenter1 = [alert
                                                      popoverPresentationController];
    popPresenter1.sourceView=btnProf;
    popPresenter1.sourceRect=btnProf.bounds;
    [alert setModalPresentationStyle:UIModalPresentationPopover];
    
    [self presentViewController:alert animated:YES completion:nil];
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    if (color != nil) {
        alert.view.tintColor=color;
    }
    else
    {
        
        alert.view.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
        
    }

    
    
}

- (IBAction)onEdit:(id)sender
{
    UIButton *btnEditProfile=(UIButton *)sender;
    btnEditProfile.selected=!btnEditProfile.selected;
    
    if (btnEditProfile.selected==YES)
    {
        _btnProfile.userInteractionEnabled=YES;
        _txtName.userInteractionEnabled=YES;
        _txtStatus.userInteractionEnabled=YES;
        [self.camImgView setHidden:NO];
        [self.camBgView setHidden:NO];
        [self.nameEditImg setHidden:NO];
        [self.statusEditImg setHidden:NO];
        self.camBgView.clipsToBounds=YES;
        self.camBgView.layer.cornerRadius=self.camBgView.frame.size.height/2;
        
        self.camImgView.image = [self.camImgView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.camImgView setTintColor:[UIColor whiteColor]];
        self.nameEditImg.image = [self.nameEditImg.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        
        NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
        UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
        if (color != nil) {
            [self.nameEditImg setTintColor:color];
            [self.camBgView setBackgroundColor:color];

            
        }
        else
        {
            [self.nameEditImg setTintColor:[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0]];
            [self.camBgView setBackgroundColor:[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0]];
           

            
        }
        self.statusEditImg.image = [self.statusEditImg.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
       
        if (color != nil) {
            [self.statusEditImg setTintColor:color];
            
        }
        else
        {
            [self.statusEditImg setTintColor:[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0]];
            
        }
    }
    else
    {
        [self.camImgView setHidden:YES];
        [self.nameEditImg setHidden:YES];
        [self.statusEditImg setHidden:YES];
        [self.camBgView setHidden:YES];
        
        
        if ([self.txtName.text isEqualToString:@""])
        {
            btnEditProfile.selected=!btnEditProfile.selected;
            
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:@"Please enter your name." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
            UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
            if (color != nil) {
                alert.view.tintColor=color;
            }
            else
            {
                
                alert.view.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
                
            }
            
            [self.txtName becomeFirstResponder];
            return;
        }
        else if ([self.txtStatus.text isEqualToString:@""])
        {
            btnEditProfile.selected=!btnEditProfile.selected;
            
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:@"Please enter your status." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            [self.txtName becomeFirstResponder];
            NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"myColor"];
            UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
            if (color != nil) {
                alert.view.tintColor=color;
            }
            else
            {
                
                alert.view.tintColor=[UIColor colorWithRed:72/255.0 green:217/255.0 blue:194/255.0 alpha:1.0];
                
            }
            return;
        }
        NSString *strImageUrl=@"";
        if ([UploadFileName isEqualToString:@""])
        {
            strImageUrl=appDelegate.strProfilePic;
        }
        else
        {
            strImageUrl=[NSString stringWithFormat:@"%@%@",FILE_AWS_IMAGE_BASE_URL,UploadFileName];
        }
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setValue:_txtName.text forKey:@"name"];
        [dictParam setValue:appDelegate.strID forKey:@"zoechatid"];
        [dictParam setValue:strImageUrl forKey:@"image"];
        [dictParam setValue:self.txtStatus.text forKey:@"status"];
        
        
        MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDAnimationFade;
        hud.label.text = @"Loading";
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_UPDATE_PROFILE withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
             if (response)
             {
                 //  NSLog(@"Response: %@",response);
                 if ([[response valueForKey:@"error"] boolValue]==false)
                 {
                     _btnProfile.userInteractionEnabled=NO;
                     _txtName.userInteractionEnabled=NO;
                     _txtStatus.userInteractionEnabled=NO;
                     
                     appDelegate.strName=[NSString stringWithFormat:@"%@",[response valueForKey:@"name"]];
                     appDelegate.strStatus=[NSString stringWithFormat:@"%@",[response valueForKey:@"status"]];
                     appDelegate.strProfilePic=[NSString stringWithFormat:@"%@",[response valueForKey:@"image"]];
                     
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setValue:appDelegate.strName forKey:@"name"];
                     [defaults setValue:appDelegate.strStatus forKey:@"status"];
                     [defaults setValue:appDelegate.strProfilePic forKey:@"image"];
                     
                 }
                 else
                 {
                     btnEditProfile.selected=!btnEditProfile.selected;
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     // Configure for text only and offset down
                     hud.mode = MBProgressHUDModeText;
                     hud.label.text = [NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                     hud.margin = 10.f;
                     hud.yOffset = 150.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hideAnimated:YES afterDelay:3];
                     
                     
                 }
             }
             
         }];
        
    }
    
    
}




#pragma mark
#pragma mark - ImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
   
    
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:chosenImage];
    cropController.delegate = self;
    cropController.rotateButtonsHidden=YES;
    cropController.rotateClockwiseButtonHidden=YES;
    //  cropController.aspectRatioPickerButtonHidden=YES;
    
    // -- Uncomment these if you want to test out restoring to a previous crop setting --
    //cropController.angle = 90; // The initial angle in which the image will be rotated
    //cropController.imageCropFrame = CGRectMake(0,0,2848,4288); //The
    
    // -- Uncomment the following lines of code to test out the aspect ratio features --
    cropController.aspectRatioPreset = TOCropViewControllerAspectRatioPresetSquare; //Set the initial aspect ratio as a square
    cropController.aspectRatioLockEnabled = YES; // The crop box is locked to the aspect ratio and can't be resized away from it
    cropController.resetAspectRatioEnabled = NO; // When tapping 'reset', the aspect ratio will NOT be reset back to default
    
    // -- Uncomment this line of code to place the toolbar at the top of the view controller --
    // cropController.toolbarPosition = TOCropViewControllerToolbarPositionTop;
    
    
    self.image = chosenImage;
    
    //If profile picture, push onto the same navigation stack
    if (self.croppingStyle == TOCropViewCroppingStyleCircular) {
        [picker pushViewController:cropController animated:YES];
    }
    else { //otherwise dismiss, and then present from the main controller
        [picker dismissViewControllerAnimated:YES completion:^{
            [self presentViewController:cropController animated:YES completion:nil];
        }];
    }
    
    
     assetURL = [info objectForKey:UIImagePickerControllerReferenceURL];
   
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark
#pragma mark - Cropper Delegate -
- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    [self.btnProfile setImage:image forState:UIControlStateNormal];
    
    self.btnProfile.clipsToBounds = YES;
    self.btnProfile.layer.cornerRadius = (self.btnProfile.frame.size.width / 2);//half of the width
    self.btnProfile.layer.borderColor=[UIColor blackColor].CGColor;
    self.btnProfile.layer.borderWidth=1.0f;
    isPictureAdded=@"1";
    
    MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
     hud.mode = MBProgressHUDAnimationFade;
     hud.label.text = @"Uploading Image";
    
    
    __block NSString *fileName = nil;
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library assetForURL:assetURL resultBlock:^(ALAsset *asset)  {
        fileName = asset.defaultRepresentation.filename;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyyMMddHHmmssSSS"];
        NSString *localDateString = [dateFormatter stringFromDate:[NSDate date]];
        // NSLog(@"%@",localDateString);
        
        NSArray* arrExt = [fileName componentsSeparatedByString: @"."];
        NSString* strExt = [arrExt objectAtIndex: arrExt.count-1];
        
        UploadFileName=[NSString stringWithFormat:@"%@.%@",appDelegate.strID,strExt];
        appDelegate.strProfilePic=[NSString stringWithFormat:@"%@%@",FILE_AWS_IMAGE_BASE_URL,UploadFileName];
        
        //image you want to upload
     //   UIImage* imageToUpload = image;
        UIImage *imageToUpload=[self compressImage:image];
        //convert uiimage to
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",UploadFileName]];
        [UIImagePNGRepresentation(imageToUpload) writeToFile:filePath atomically:YES];
        
        NSURL* fileUrl = [NSURL fileURLWithPath:filePath];
        
        AWSS3TransferUtilityUploadCompletionHandlerBlock completionHandler = ^(AWSS3TransferUtilityUploadTask *task, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // Do something e.g. Alert a user for transfer completion.
                // On failed uploads, `error` contains the error object.
                [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                NSLog(@"%@",error);
                [self updateProfilePicture];
                [CustomAlbum addNewAssetWithImage:imageToUpload toAlbum:[CustomAlbum getMyAlbumWithName:CSAlbum] onSuccess:^(NSString *ImageId) {
                    NSLog(@"image saved");
                    
                }
                                          onError:^(NSError *error)
                 {
                     NSLog(@"probelm in saving image");
                 }];

                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                
                // Configure for text only and offset down
                hud.mode = MBProgressHUDModeText;
                hud.label.text = @"Profile picture uploaded";
                hud.margin = 10.f;
                hud.yOffset = 150.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hideAnimated:YES afterDelay:3];
                 [self.btnProfile setImage:image forState:UIControlStateNormal];
            });
        };
        
        AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility defaultS3TransferUtility];
          [[transferUtility uploadFile:fileUrl bucket:FILE_BUCKET_NAME key:UploadFileName contentType:@"image/png" expression:nil completionHandler:completionHandler] continueWithBlock:^id(AWSTask *task) {
            if (task.error) {
                [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                 NSLog(@"Error: %@", task.error);
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                
                // Configure for text only and offset down
                hud.mode = MBProgressHUDModeText;
                hud.label.text = @"Profile picture not uploaded";
                hud.margin = 10.f;
                hud.yOffset = 150.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hideAnimated:YES afterDelay:3];
                
            }
           
            if (task.result) {
                AWSS3TransferUtilityUploadTask *uploadTask = task.result;
                // Do something with uploadTask.
                NSLog(@"%@",uploadTask);
            }
            
            return nil;
        }];
        
    }failureBlock:^(NSError *error)
     {
         NSLog(@"%@",error);
     }];
    
    [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (UIImage *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth) {
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
}




#pragma mark -
#pragma mark - Keyboard

-(void)dismissKeyBoard
{
    // [tableTexture removeFromSuperview];
    
    for(UIView *subView in self.scrollView.subviews)
    {
        if([subView isKindOfClass:[UITextField class]])
        {
            if([subView isFirstResponder])
            {
                [subView resignFirstResponder];
                break;
            }
        }
        else if([subView isKindOfClass:[UITextView class]])
        {
            if([subView isFirstResponder])
            {
                [subView resignFirstResponder];
                break;
            }
        }
        
    }
    
    
    
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
-(void) textFieldDidBeginEditing:(UITextField *)textField
{
    
    currentTextView=textField;
    [self moveScrollView:textField.frame];
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
    currentTextView1=textView;
    [self moveScrollView:textView.frame];
}
- (void)keyboardDidHide:(NSNotification *)n
{
    keyboardIsShown = NO;
    [self.view removeGestureRecognizer:tapGesture];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardDidShow:(NSNotification *)n
{
    if (keyboardIsShown) {
        return;
    }
    
    keyboardIsShown = YES;
    [self.scrollView addGestureRecognizer:tapGesture];
}

- (void) moveScrollView:(CGRect) rect
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    
    CGSize kbSize = CGSizeMake(32, 260); // keyboard height
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.scrollView.frame;
    aRect.size.height -= (kbSize.height+55);
    if (!CGRectContainsPoint(aRect, rect.origin) )
    {
        CGPoint scrollPoint = CGPointMake(0.0, rect.origin.y-kbSize.height+100);
        
        if(screenHeight==480)
            scrollPoint = CGPointMake(0.0, rect.origin.y-kbSize.height+100+90);
        
        if(scrollPoint.y>0)
            [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

- (void) hideKeyBoard
{
    
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    //  NSLog(@"Keyboard is hidden");
    //  UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    UIEdgeInsets content=UIEdgeInsetsMake(0, 0, 0, 0);
    self.scrollView.contentInset = content;
    self.scrollView.scrollIndicatorInsets = content;
}


#pragma mark -
#pragma mark - Font


-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)view;
        [btn setFont:[UIFont fontWithName:fontFamily size:[[btn font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *txtFld = (UITextField *)view;
        [txtFld setFont:[UIFont fontWithName:fontFamily size:[[txtFld font] pointSize]]];
    }
    if ([view isKindOfClass:[UITextView class]])
    {
        UITextView *txtVw = (UITextView *)view;
        [txtVw setFont:[UIFont fontWithName:fontFamily size:[[txtVw font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}



@end
